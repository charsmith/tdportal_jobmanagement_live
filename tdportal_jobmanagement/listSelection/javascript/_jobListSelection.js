﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var initialStoreData = [];
var gData;
var gDataAll;
var searchData;
var selectedStoreId;
var prevSelectedStoreId;
//var serviceURL = '../listSelection/JSON/_jobListSelection.JSON';
//var serviceURL = myProtocol + 'maps.tribunedirect.com/api/ZipSelectList/';
var gStateServiceUrl = '../JSON/_listStates.JSON';
var serviceURL = mapsUrl + 'api/ZipSelectList_post/';
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var gDemograhicsLoadUrl = mapsUrl + 'api/ZipSelectList_demo/' + jobCustomerNumber;
var tempStaticJsonUrl = "JSON/_demo_staticInfo.JSON";
var tempStaticJson = [];
var storesInOrder = [];
var gOutputData;
var gDataStartIndex = 0;
var gInnerData = [];
var gSelectedZips = [];
var jobNumber = "";
var facilityId = "";
var totalAvailable = 0;
var jsonStoreLocations = "";
var selectedCRRTS = [];
var gDataStartIndex = 0;
var gPageSize = 7;
var pagePrefs;
var mappingDemographics = {};
var selectedDemographics = {};
var demo_groups = {};
var addedDemographics = {};
var eventLocationsInfo = {};
var prev_demos = [];
var isDemosChanged = false;
var isStoreChanged = false;
var isTargetChanged = false;
var isRadiusChanged = false;
var isEnterPress = false;
var isDemosDefaulted = false;
var isMapRefreshed = false;
var isZipsChanged = false;
var min_max_change = false;
var inputMinNumber;
var inputMaxNumber;
var inputMinMNumber;
var inputMaxMNumber;
var inputMinUNumber;
var inputMaxUNumber;
var prevSelectedMode;
var pageObj;
var isJobSaved = false;
jobNumber = getSessionData("jobNumber");
facilityId = getSessionData("facilityId");
caseysStaicJobNumber = sessionStorage.jobNumber;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
/*if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
    window.location.href = "../listSelectionCaseys/jobListSelection.html";
}*/
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobListSelection').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    //$.mobile.selectmenu.prototype.options.nativeMenu = false;
    displayMessage('_jobListSelection');
    createConfirmMessage('_jobListSelection');
    confirmNavigationMessage('_jobListSelection');
    saveConfirmMessage('_jobListSelection');
    showStoreChangeMessage('_jobListSelection');
    displayCompetitorConfirmMessage('_jobListSelection');
    radiusZipCRRTChangeMessage('_jobListSelection');
    $('#btnContinue').attr('onclick', 'continueToOrder()');

    //test for mobility...
    //loadMobility();
    //var over = '<div id="overlay" style="z-index:100;"><img id="loading" alt="Loading...." src="../images/kaleidoscope_wait.gif"></img><span id="loadingText">Loading Map....</span></div>';
    //$(over).appendTo('body');
    loadingImg("_jobListSelection");
    sessionStorage.removeItem('updatedCRRTs');
    createPlaceHolderforIE();

    //Event 1, Event 2 locations for two events
    eventLocationsInfo = {
        "locationList": {},
        "isEnabled": true,
    };
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        if (eventLocationsInfo.locationList["Event 1"] == undefined && eventLocationsInfo.locationList["Event 1"] == null)
            eventLocationsInfo.locationList["Event 1"] = {};
    }
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true)) {
        if (eventLocationsInfo.locationList["Event 2"] == undefined && eventLocationsInfo.locationList["Event 2"] == null)
            eventLocationsInfo.locationList["Event 2"] = {};
        $("label[for='chkReplicate']").text("Replicate List Selections For All Events");
    }

    if (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER)
        loadDemographics();
    createMapPreferences(false);
    $('#dvFindAvblEmails').css('display', 'none');
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    displayJobInfo();
    if (appPrivileges.roleName == "admin") {
        $('#btnMapPrefs').addClass('ui-first-child');   
        //$('#aCustomerConfig').css('display', 'block');
        //$('#aCustomerConfig').addClass('ui-first-child');
    }
    if (appPrivileges.roleName != "admin")
        $('#btnCreateStatic').css('display', 'none');

    //if ((jobCustomerNumber == "99997") && ((sessionStorage.isDemoSelected != undefined && sessionStorage.isDemoSelected != null && sessionStorage.isDemoSelected == "1") || (sessionStorage.username.toLowerCase().indexOf('demo') > -1)) &&
    //            (sessionStorage.isDemoInsDisplayed == undefined || sessionStorage.isDemoInsDisplayed == null || sessionStorage.isDemoInsDisplayed == "" || sessionStorage.isDemoInsDisplayed == "false")) {
    //    sessionStorage.isDemoInsDisplayed = true;
    //    $('#btnDemoInstructions').trigger('click');
    //}
    // else {
    //        $('#btnDemoInstructions').css('display', 'none');
    window.setTimeout(function applyStyles() {
        if (appPrivileges.roleName == "admin")
            //$('#aCustomerConfig').addClass('ui-first-child');
                $('#btnMapPrefs').addClass('ui-first-child');   
        else
            $('#btnMapPrefs').addClass('ui-first-child');
        //$('#btnPieChart').addClass('ui-first-child');
        //            $('#btnMapPrefs').addClass('ui-last-child');
    }, 1000);
    //}
    if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
        //        $('#divBottom').css('display', 'block');
        $('#tdShowHints').attr('style', 'width:95px;height:55px;');
        //        $('#btnSave').css('display', 'none');
        //        $('#btnContinue').css('display', 'none');
    }
    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        window.setTimeout(function () { setDemoHintsSliderValue(); }, 2000);
        //setDemoHintsSliderValue();
        //window.setTimeout(function loadHints() {
        createDemoHints("jobListSelection");
        // }, 100);
    } else if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
        setDemoHintsSliderValue();
    }

    $("#listControls").bind({
        popupafterclose: function (event, ui) {
            if (sessionStorage.showDemoHints != "off")
                $('#targetSelection').popup('open', { positionTo: '#map' });
        }
    });
    $("#targetSelection").bind({
        popupafterclose: function (event, ui) {
            if (sessionStorage.showDemoHints != "off")
                $('#mapPreference').popup('open', { positionTo: '#map' });
        }
    });
});

$(document).bind('pageshow', '#_jobListSelection', function (event) {
    mapVerticalResize();
    //makeCompetitorSearchMap();
    //persistNavPanelState();
    $("#dvMailingTypes div[data-role=collapsible]").bind('collapsiblecollapse', function (event) {
        var evt_list = ['dvAcqMailingList', 'dvEmailList', 'dvUSMailingList']
        if (evt_list.indexOf(event.target.id) > -1) {
            //$('#dvLeftPanel').css('height', '680px');
            $('#map').empty();
            $('#map').css('transform', '');
            $('#map').css('background-color', 'rgb(255, 255, 255)');
        }
    });
    $("#dvMailingTypes div[data-role=collapsible]").bind('collapsibleexpand', function (event) {
        var collapsibles = $("#dvMailingTypes div[data-role=collapsible]");
        var collapsible_open_count = 0;
        $.each(collapsibles, function () {
            if (!$(this).hasClass('ui-collapsible-collapsed')) {
                collapsible_open_count++;
                return false;
            }
        });

        var evt_list = ['dvAcqMailingList', 'dvEmailList', 'dvUSMailingList']
        if ($('#ddlStoreLocations').val() == "") {
            $(this).collapsible('collapse');
            $('#alertmsg').html("Please select a location.");
            $('#popupDialog').popup('open');
            return false;
        }

        if (evt_list.indexOf(event.target.id) > -1) {
            $('#btnMapPrefs').addClass('ui-disabled');
            $('#btnMapPrefs')[0].disabled = false;
            if (collapsible_open_count == 0 || prevSelectedStoreId == undefined || prevSelectedStoreId == null || prevSelectedStoreId == "") {
                $("#dvMailingTypes div[data-role=collapsible]").find('div[id^=dvContentHolder]').empty();
                var temp = $('#selectionMethodTemplate').html();
                $(this).find('div[id^=dvContentHolder]').html(temp).trigger('create');
                $('#dvDemoSlider').bind('change', function () {
                    $('#dvDemoSlider div.ui-rangeslider-sliders .ui-slider-handle').removeClass('ui-btn-a').removeClass('ui-btn-d').removeClass('ui-btn-null').addClass('ui-btn-null');
                    $('#dvDemoSlider div.ui-rangeslider-sliders div.ui-slider-bg').css('background-color', '');
                    $('#dvDemoSlider div.ui-rangeslider-sliders div.ui-slider-track').removeClass('ui-bar-d').removeClass('ui-bar-a').removeClass('ui-bar-a');
                    $('#dvDemoSlider div.ui-rangeslider-sliders div.ui-slider-track').removeClass('ui-bar-d').removeClass('ui-bar-a').removeClass('ui-bar-c');
                    if ($('#rdoIncludeSelected').is(':checked')) {
                        $('#rdoIncludeSelected').trigger('change');
                    }
                    else {
                        $('#rdoExcludeSelected').trigger('change');
                    }
                });
                $('#dvCollapsibleDemos').collapsibleset('refresh'); //.trigger('create');
                $('#dvSectionMethod').collapsible("collapse");
                $('#dvSectionMethod').addClass("ui-disabled");
                $('#dvSectionMethod')[0].disabled = true;
                $('#dvSectionMethod').on('collapsibleexpand', function (event) {
                    if ($('#ddlStoreLocations').val() == "") {
                        event.preventDefault();
                        event.stopPropagation();
                        $('#dvSectionMethod').collapsible("collapse");
                    }
                });
                $("#dvSelectAllPostalBoundaries").on("collapsibleexpand", function (e) {
                    $("#dvSelectAllPostalBoundaries").collapsible('collapse');
                    e.stopPropagation();
                    e.preventDefault();
                    $('#chkSelectAllPostalBoundaries').attr('checked', ($('#chkSelectAllPostalBoundaries').is(':checked')) ? false : true).checkboxradio('refresh').trigger('change');
                    //selectAllPostalBoundaries($('#chkSelectAllPostalBoundaries')[0]);
                });

                $('#dvSelectByRadius').find('.ui-collapsible-content.ui-body-d').css('padding-left', '2px');
                $('#dvSelectByRadius').find('.ui-collapsible-content.ui-body-d').css('padding-right', '2px');


                if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                    if ((gOutputData != undefined && gOutputData != null && gOutputData != '') && gOutputData.templateName.toLowerCase().indexOf('delivery') > -1) {
                        //$('input[type=radio][name=rdoCounts]').attr('checked', false).checkboxradio('refresh');
                        $('#rdoPolygon').attr('checked', true).checkboxradio('refresh').trigger('change');
                    }
                    else {
                        $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh').trigger('change');
                    }
                    if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER) {// || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                        $('#sldrRange').attr('max', '50');
                    }
                    $('#sldrRange').slider('disable');
                    $('#sldrRange')[0].disabled = true;
                    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                        $('#txtRadiusTargeted').val('3000');
                        $('#spnRadiusTargeted').val('3000');
                    }
                }
                else {
                    $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
                    $('#sldrRange').slider('enable');
                    $('#sldrRange')[0].disabled = false;
                }

                //ddlSearchStates
                $('#ddlSearchStates').on('change', function (event) {
                    $(this).focus();
                });

                if (appPrivileges.roleName == "admin") {
                    $('#dvPreSelectDemo').css('display', "block");
                    $('#dvSelectDemographcs').css('display', "block");
                    $('#dvExtendedDemographs').css('display', "block");
                }
                else {
                    $('#dvPreSelectDemo').css('display', "none");
                    $('#dvSelectDemographcs').css('display', "block");
                    $('#dvExtendedDemographs').css('display', "none");
                    $('#dvSelectDemographcs').css("border-bottom-right-radius", "inherit");
                    $('#dvSelectDemographcs').css("border-bottom-left-radius", "inherit");
                }

                //$('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
                if (prevSelectedStoreId == undefined)
                    $('#chkViewByHouseHold').attr('checked', false).checkboxradio('refresh');


                if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
                    $('#dvCollapsibleDemos').css('display', 'none');
                    $('#aCustomerConfig').css('display', 'none');
                }

                if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
                    $("#dvSelectByRadius").trigger('collapsibleexpand');
                else
                    $("#dvSelectByZipCrrt").trigger('collapsibleexpand');

                if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER) {
                    $('#btnRevert').css('display', "none");
                    $('#btnSaveSelections').css('display', "none");
                    $('#btnRefresh').css('display', "none");
                }
                else {
                    $('#btnRevert').addClass('ui-disabled');
                    $('#btnRevert')[0].disabled = true;
                    $('#btnSaveSelections').addClass('ui-disabled');
                    $('#btnSaveSelections')[0].disabled = true;
                    $('#btnRefresh').addClass('ui-disabled');
                    $('#btnRefresh')[0].disabled = true;

                    //click events.
                    $('#btnRefresh').click(function () {
                        if ($("#rdoPolygon").is(':checked')) {
                            var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
                            if (Object.keys(temp_store_list_selection).length > 0 && (temp_store_list_selection.polygonInfo == undefined || temp_store_list_selection.polygonInfo == null || temp_store_list_selection.polygonInfo == "")) {
                                $('#alertmsg').text('Please draw polygon before refreshing map.');
                                $('#popupDialog').popup('open');
                                return false;
                            }
                        }
                        if (isMapMove) {
                            showResult();
                        }
                        else {
                            //validateTargetQty();
                            $('#waitPopUp').popup('open', { positionTo: 'window' });
                            var diff_demos = [];
                            var selected_demos = [];
                            selected_demos = makeSelectedDemosObject();
                            // if (gData != undefined && gData != null && gData != "" && selected_demos.length > 0) {
                            diff_demos = DiffObjects(prev_demos, selected_demos);
                            //}
                            if (isDemosChanged || diff_demos.length > 0) {
                                //getSelectedStoreData();
                                //$('#ddlStoreLocations').trigger('change');
                                sessionStorage.removeItem('updatedCRRTs');
                                selectedStoreChanged();
                                isDemosChanged = false;
                                //getSelectedStoreData();
                                //showResult();
                            }
                            else {
                                //Need to write code for radius
                                window.setTimeout(function makeDelay() {
                                    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                                        //                         var txt_qty = ($('#txtRadiusTargeted').val() != "") ? parseInt($('#txtRadiusTargeted').val()) : "";
                                        //                            if ((gData.targetedAsk != $('#txtRadiusTargeted').val() || (($('#rdoTargetCounts')[0].checked && $('#hdnZipsChanged').val() == "") && (sessionStorage.updatedCRRTs == undefined && sessionStorage.updatedCRRTs == null || sessionStorage.updatedCRRT == "")) || ((!$('#sldrRange')[0].disabled) && $('#sldrRange').attr('prevValue') != $('#sldrRange').val()))) {
                                        var temp_target_qty = "";
                                        var curr_mode = $('input[type=radio][name=rdoCounts]:checked').val();
                                        temp_target_qty = ($('#txtRadiusTargeted').val() != "") ? $('#txtRadiusTargeted').val().replace(',', '') : '';
                                        //if ((gData.targetedQty != temp_target_qty) || (($('#rdoTargetCounts')[0].checked && $('#hdnZipsChanged').val() == "") && (sessionStorage.updatedCRRTs == undefined && sessionStorage.updatedCRRTs == null || sessionStorage.updatedCRRT == "")) || ((!$('#sldrRange')[0].disabled) && $('#sldrRange').attr('prevValue') != $('#sldrRange').val())) {
                                        if ((gData != undefined && gData != null && gData.targetedAsk != temp_target_qty) || (($('#rdoTargetCounts')[0].checked && $('#hdnZipsChanged').val() == "" && (prevSelectedMode == "p" || curr_mode == "p")) && (sessionStorage.updatedCRRTs == undefined && sessionStorage.updatedCRRTs == null || sessionStorage.updatedCRRT == "")) || ((!$('#sldrRange')[0].disabled) && $('#sldrRange').attr('prevValue') != $('#sldrRange').val()) || isZipsChanged) {

                                            //if (gData.targetedQty != temp_target_qty) {
                                            if (gData.targetedAsk != temp_target_qty) {
                                                isTargetChanged = true;
                                                isEnterPress = false;
                                            }
                                            if (!isZipsChanged)
                                                sessionStorage.removeItem('updatedCRRTs');
                                            getSelectedStoreData();
                                            //showResult();
                                        }
                                        else {
                                            calculateTargetedCnt();
                                            //showResult();
                                        }

                                    }
                                    else if (!$('#dvSelectByZipCrrt').hasClass('ui-collapsible-collapsed')) {
                                        calculateTargetedCnt();
                                        //showResult();
                                    }
                                }, 1000);
                            }
                        }
                    });

                    $('#btnSaveSelections').click(function (event) {
                        saveSelections(event);
                    });
                }
                $('#loadingText1').text("Loading Map.....");

                $('#dvPostalBoundarySelections').on('collapsibleexpand', function (event) {
                    if (!$("#dvLocations div[data-role=collapsible]").hasClass('ui-collapsible-collapsed')) {
                        $('#dvLocations').css('display', 'none');
                        event.preventDefault();
                        event.stopPropagation();
                        $('#loadingText1').html("Loading <br />Postal Boundaries.....")
                        $('#waitPopUp').popup('open', { positionTo: 'window' });
                        window.setTimeout(function () {
                            $("#dvLocations").trigger('create');
                            $('#dvLocations').find('.ui-collapsible-content').css('padding-top', '0px');
                            openPostalBoundaryToggleDemoHint();

                            $('#dvLocations div[data-role=collapsible]').on('collapsibleexpand', function (event) {
                                event.preventDefault();
                                //window.setTimeout(function () {
                                //$(this).find('fieldset div.ui-controlgroup-controls').css('width', '100%');
                                $('#dvLocations div[data-role=collapsible] fieldset div.ui-controlgroup-controls').css('width', '100%');
                                //}, 1000);
                            });
                        }, 500);
                    }
                });

                $('#dvDemographicFilters').on('collapsibleexpand', function (event) {
                    openDemographicFilterToggleDemoHint();
                });

                $('#dvSelectByZipCrrt').on('collapsibleexpand', function (event) {
                    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
                        openCarrierRouteToggleDemoHint();
                    else
                        carrierRouteSelected();
                });

                $('#dvSelectByRadius').on('collapsibleexpand', function (event, ui) {
                    if ($('#hdnMode').val() == 'zipcrrt') {
                        //var temp_ctrls = $('input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked:[selectedType=f]');
                        var temp_ctrls = $('input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked[selectedType=f]');
                        var msg = "";
                        var ctrl = null;
                        if (temp_ctrls.length > 0) {
                            ctrl = $('#rdoTargetCounts');
                            msg = "Resetting the targeted count will remove all of your current carrier route selections. Click \"Continue\" to reset the targeted count or click \"Cancel\" to keep your current selections.";
                        }
                        else {
                            ctrl = $('#rdoRadius');
                            msg = "Resetting the radius will remove all of your current carrier route selections. Click \"Continue\" to reset the radius or click \"Cancel\" to keep your current selections.";
                        }

                        //$('#rdoTargetCounts')[0].checked
                        //             Resetting the targeted count will remove all of your current carrier route selections. Click "Continue" to reset the targeted count or click "Cancel" to keep your current selections.
                        //Resetting the radius will remove all of your current carrier route selections. Click \"Continue\" to reset the radius or click \"Cancel\" to keep your current selections.
                        //var msg = "Resetting the targeted count will remove all of your current carrier route selections. Click \"Continue\" to reset the targeted count or click \"Cancel\" to keep your current selections.";
                        $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'getCRRTSelectionByRadius(\'' + ctrl[0].id + '\');');
                        $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnSelectedZip\').val(\'test\');$(\'#dvSelectByZipCrrt\').collapsible(\'expand\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');');
                        $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
                        $('#popupradiusZipCRRTConfirmDialog').popup('open');
                        $('#hdnMode').val('radius');
                        $('#hdnSelectedZip').val('');
                        //}
                    }
                });

                $('#sldrRange').bind('slidestop blur', function (e, ui) {
                    if ($('#sldrRange').attr('prevValue') != $('#sldrRange').val())
                        if (validateTargets()) {
                            if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed') && $('#rdoRadius')[0].checked && (!$('#sldrRange')[0].disabled)) {
                                $('#waitPopUp').popup('open', { positionTo: 'window' });
                                window.setTimeout(function getDelay() {
                                    isRadiusChanged = true;
                                    sessionStorage.removeItem('updatedCRRTs');
                                    getSelectedStoreData();
                                    //showResult();
                                }, 1000);

                            }
                        }
                });

                $('#txtRadiusTargeted').bind('blur', function () {
                    if (gData != undefined && gData != null && gData != "" && gData.targetedQty != $('#txtRadiusTargeted').val() && !isEnterPress) {
                        if (validateTargets()) {
                            $('#spnPheripheralTargets').html(0);
                            $('#spnTotalTargets').html(0);
                            isTargetChanged = true;
                            $('#btnRefresh').trigger('click');
                        }
                    }
                });

                if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                    $('#sldrRange').slider('disable');
                    $('#sldrRange')[0].disabled = true;
                    $('label[for="rdoRadius"]').removeClass('ui-last-child');
                    $('#rdoPolygon').parent().css('display', 'block');
                    updateFooterInfo(jobCustomerNumber);
                }
                else {
                    $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
                    $('#sldrRange').slider('enable');
                    $('#sldrRange')[0].disabled = false;
                    //$('#rdoPolygon').parent().css('display', 'block');
                    //$('label[for="rdoRadius"]').addClass('ui-last-child');
                }
            }
            var tempSelectedStoreId = $('#ddlStoreLocations').attr('value');
            updateSelectedStore(tempSelectedStoreId);
            //updateSelectedStore(self.selectedLocation);
        }
    });


    $("[data-role=footer]").toolbar({ tapToggleBlacklist: "#mapHolder" });

    //$('#overlay').css('display', 'none');
    if (sessionStorage.ListSelectionPrefs == undefined || sessionStorage.ListSelectionPrefs == null || sessionStorage.ListSelectionPrefs == "")
        if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
            getPagePreferences('ListSelection.html', sessionStorage.facilityId, CASEYS_CUSTOMER_NUMBER);
        else
            getPagePreferences('ListSelection.html', sessionStorage.facilityId, jobCustomerNumber);
    else
        managePagePrefs(jQuery.parseJSON(sessionStorage.ListSelectionPrefs));
    pagePrefs = jQuery.parseJSON(sessionStorage.ListSelectionPrefs);

    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;
    //if (jobCustomerNumber == "99997")
    //  $('#btnNavLinks').addClass('ui-disabled');
    if (pagePrefs != null) {
        displayListFileInfo();
        getBubbleCounts(gOutputData);
        //$('#ddlStoreLocations').change(function () {

        //});
        //eval(getURLDecode(pagePrefs.scriptBlockDict.pageshow));

        var is_source_found = false;
        if (gOutputData.standardizeAction != undefined && gOutputData.standardizeAction != null && gOutputData.standardizeAction != "") {
            //if (gOutputData.standardizeAction.standardizeFileList != undefined && gOutputData.standardizeAction.standardizeFileList != null && Object.keys(gOutputData.standardizeAction.standardizeFileList).length == 0) {
            //    $('#dvMailingListType li input[type=radio][id=rdoUploadedSourceList]').parent().css('display', 'none');
            //    $('#dvMailingListType li input[type=radio][id=rdoDoNotMailList]').parent().css('display', 'none');
            //}
            //else {
            $.each(gOutputData.standardizeAction.standardizeFileList, function (key, val) {
                if (key != "type") {
                    if (val.sourceType == 1)
                        is_source_found = true;
                }
            });
            //}
        }
        if (!is_source_found)
            $('#dvUSMailingList').css('display', 'none');
    }

    ddlKeyboardSelection('ddlSearchStates');
    if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        if (jobCustomerNumber != AAG_CUSTOMER_NUMBER) {
            $.getJSON(tempStaticJsonUrl, function (data) {
                tempStaticJson = data;
            });
        }
        window.setTimeout(function () {
            pageObj = new loadNavBar();
            ko.applyBindings(pageObj);
            if ((jobCustomerNumber != AAG_CUSTOMER_NUMBER) || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true))) {
                $('#dvNavBar').trigger('create');
                $('#dvNavBar').css('display', 'block');
                $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
                $('#sldrShowHints').slider();//.trigger('create');
                //$('#dvReplicate').css('padding-top', '35px');
                $('#dvReplicate').css('display', 'block');
                //$('#mapHolder').css('padding-top', '35px');
            }
        }, 500);
    }
    else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
        $('#sldrShowHints').slider();//.trigger('create');
        $('#dvNavBar').css('display', 'none');
        $('#dvReplicate').css('display', 'none');

        $('#dvEmailList').css('display', 'none');
        $('#dvUSMailingList').css('display', 'none');
    }
    else {
        $('#dvEmailList').css('display', 'none');
        $('#dvUSMailingList').css('display', 'none');

        $('#dvNavBar').css('display', 'none');
        $('#dvReplicate').css('display', 'none');
    }

    $('#divBottom').trigger('create');

    if (!dontShowHintsAgain && (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isListSelectionDemoHintsDisplayed == undefined || sessionStorage.isListSelectionDemoHintsDisplayed == null || sessionStorage.isListSelectionDemoHintsDisplayed == "false")) {
        sessionStorage.isListSelectionDemoHintsDisplayed = true;
        $('#storeSelection').popup('close');
        window.setTimeout(function loadHints() {
            $('#storeSelection').popup('open', { positionTo: '#map' });
        }, 1000);
    }
    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
        $('#storeSelection p a').text('Turn Hints Off');
        $('#selectionMethod p a').text('Turn Hints Off');
        $('#postalBoundarySelections p a').text('Turn Hints Off');
    }
    window.setTimeout(function loadHints() {
        $('#sldrShowHints').slider();
    }, 3000);

    if (isHideLoadOfficers() == "true") {
        $('#btnLoanOfficers').hide();
    }
    $('#btnSaveTop').css('display', 'block');
    $('#btnContinue').css('display', 'block');

    $('#btnMapPrefs').addClass('ui-disabled');
    $('#btnMapPrefs')[0].disabled = true;
    getNextPageInOrder();
});

function validateTargets() {
    var peri_targets = $('#spnPheripheralTargets').text();
    var mode = "";
    var target_mode_cnts = false;
    if ($('#rdoTargetCounts').is(":checked") && gData) {
        target_mode_cnts = ($('#txtRadiusTargeted').val().replace(/,/g, '') != gData.targetedAsk);
        mode = "Targeted quantity";
    }
    else if ($('#rdoRadius').is(':checked') && gData) {
        target_mode_cnts = ($('#sldrRange').val() != gData.radius);
        mode = "Radius";
    }
    else if ($('#rdoPolygon').is(':checked')) {
        target_mode_cnts = true;
        mode = "Polygon";
    }

    //$('#txtRadiusTargeted').val().replace(/,/g, '') != gData.targetedQty
    if (parseInt(peri_targets) > 0 && target_mode_cnts) {
        var msg = "Changes to the " + mode + " will clear all Peripheral Targets selected.";
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').bind('click', function () {
            $('#spnPheripheralTargets').html(0);
            $('#spnTotalTargets').html(0);
            isTargetChanged = true;
            $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').unbind('click');
            $('#popupradiusZipCRRTConfirmDialog').popup('open');
            $('#popupradiusZipCRRTConfirmDialog').popup('close');
            if (!$('#rdoPolygon').is(':checked')) {
                $('#btnRefresh').trigger('click');
            }
            else {
                changeToPolygonMode($('#rdoPolygon')[0]);
            }
        });
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').bind('click', function () {
            $('#popupradiusZipCRRTConfirmDialog').popup('open');
            $('#popupradiusZipCRRTConfirmDialog').popup('close');
            $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').unbind('click');
            if ($('#rdoPolygon').is(':checked')) {
                $('#rdoPolygon').attr('checked', false).checkboxradio('refresh');
                $('input[type=radio][value="' + prevSelectedMode + '"]').attr('checked', true).checkboxradio('refresh');
            }
            else if ($('#rdoRadius').is(':checked')) {
                $('#sldrRange').val($('#sldrRange').attr('prevValue')).slider('refresh');
            }
            else if ($('#rdoTargetCounts').is(":checked")) {
                $('#txtRadiusTargeted').val(gData.targetedAsk.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            }
            isEnterPress = false;
            return false;
        });
        $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
        $('#popupradiusZipCRRTConfirmDialog').popup('open');
        return false;
    }
    else return true;
}

String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};

function isHideLoadOfficers() {
    return jQuery.inArray(jobCustomerNumber, [JETS_CUSTOMER_NUMBER, KUBOTA_CUSTOMER_NUMBER, DCA_CUSTOMER_NUMBER, SK_CUSTOMER_NUMBER, CASEYS_CUSTOMER_NUMBER, REGIS_CUSTOMER_NUMBER, GWA_CUSTOMER_NUMBER]) > -1 ? "true" : "false";
}

function openCarrierRouteToggleDemoHint() {
    if (!dontShowHintsAgain && (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isCarrierRouteSelectedFirstTime == undefined || sessionStorage.isCarrierRouteSelectedFirstTime == null || sessionStorage.isCarrierRouteSelectedFirstTime == "false")) {
        sessionStorage.isCarrierRouteSelectedFirstTime = true;
        $('#carrierRouteSelection').popup('close');
        window.setTimeout(function loadHints() {
            $('#carrierRouteSelection').popup('open', { positionTo: '#map' });
        }, 1000);
    }
    else {
        carrierRouteSelected();
    }
}

function carrierRouteSelected() {
    if ($('#hdnMode').val() != "rtoc") {
        var msg = "Clicking on \"Select Only By Zip/Carrier Route\" will create Zip/Carrier Route selection instead of Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection."
        //var msg = "Removing zips/carrier routes or adding zips/carrier routes that intersect with the existing radius will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection.";
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'convertRadiusToZipCRSelections()');
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnMode\').val(\'radius\');$(\'#dvSelectByRadius\').trigger(\'expand\');$(\'#hdnSelectedZip\').val(\'\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');');
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnMode\').val(\'radius\');$(\'#hdnSelectedZip\').val(\'\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');$(\'#dvSelectByRadius\').collapsible(\'expand\');');
        $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
        $('#popupradiusZipCRRTConfirmDialog').popup('open');
    }
    $('#hdnMode').val('zipcrrt');
    $('#hdnSelectedZip').val('');
}

function openPostalBoundaryToggleDemoHint() {
    $('#waitPopUp').popup('close');
    $('#loadingText1').text("Loading Map.....")
    $('#dvLocations').css('display', 'block');
    if (!dontShowHintsAgain && (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isPostalBoundarySelectedFirstTime == undefined || sessionStorage.isPostalBoundarySelectedFirstTime == null || sessionStorage.isPostalBoundarySelectedFirstTime == "false")) {
        sessionStorage.isPostalBoundarySelectedFirstTime = true;
        $('#postalBoundarySelection').popup('close');
        window.setTimeout(function loadHints() {
            $('#postalBoundarySelection').popup('open', { positionTo: '#map' });
        }, 1000);
    }
}

function openDemographicFilterToggleDemoHint() {
    if (!dontShowHintsAgain && (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isDemographicFilterSelectedFirstTime == undefined || sessionStorage.isDemographicFilterSelectedFirstTime == null || sessionStorage.isDemographicFilterSelectedFirstTime == "false")) {
        sessionStorage.isDemographicFilterSelectedFirstTime = true;
        $('#demographicSelection').popup('close');
        window.setTimeout(function loadHints() {
            $('#demographicSelection').popup('open', { positionTo: '#map' });
        }, 1000);
    }
}

function closeInitialPopup(popup_ctrl) {
    $('#' + popup_ctrl).popup('close');
    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
        //$('#selectionMethod').popup();
        //$('#selectionMethod').popup('open', { positionTo: "#map" });
        if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isPostalBoundarySelectionsHintsDisplayed == undefined || sessionStorage.isPostalBoundarySelectionsHintsDisplayed == null || sessionStorage.isPostalBoundarySelectionsHintsDisplayed == "" || sessionStorage.isPostalBoundarySelectionsHintsDisplayed == "false") && (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER)) {
            sessionStorage.isPostalBoundarySelectionsHintsDisplayed = true;
            $('#postalBoundarySelections').popup('open', { positionTo: '#map' });
        }
    }
}
//******************** Public Functions Start **************************

var loanOfficer = function (user_id, first_name, last_name) {
    var self = this;
    self.loanOfficerId = user_id;
    self.loanOfficerName = first_name + ' ' + last_name;
};

var loadNavBar = function () {
    var self = this;
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.loanOffiersList = ko.observableArray([]);
    self.selectedLoanOfficer = ko.observable({});
    //Loading Inhome dates nav bar.
    if (gOutputData != undefined && gOutputData != null && jobCustomerNumber != AAG_CUSTOMER_NUMBER) {
        var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
        var cnt = -1;
        $.each(gOutputData, function (key, val) {
            if (key.toLowerCase().indexOf('inhome') > -1) {
                var temp_list = {};
                temp_list[key] = val;
                self.inHomeDatesList.push({
                    "showCheckIcon": false,
                    'dateValue': val
                });
                cnt++;
                if (cnt == 0)
                    nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                else
                    nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
            }
        });
        nav_bar_items += '</ul></div>';
        self.currentInHome(self.inHomeDatesList()[0]);
    }
    else if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
        $.each(eventLocationsInfo, function (key, val) {
            if (key.toLowerCase().indexOf('locationlist') > -1) {
                $.each(val, function (key1, val1) {
                    self.inHomeDatesList.push({
                        "showCheckIcon": false,
                        'dateValue': key1
                    });
                    nav_bar_items += '<li><a href="#" val="">' + key1 + '</a></li>';
                });
            }
        });
        nav_bar_items += '</ul></div>';
        self.currentInHome(self.inHomeDatesList()[0]);
    }

    self.navBarItemClick = function (data, event) {
        var ele = event.target || event.targetElement;
        $('#dvNavBar div ul li a').removeClass('ui-btn-active');
        //Need to call the validations here before setting the new inhome date selection
        $(ele).addClass('ui-btn-active');
        self.currentInHome(ko.dataFor(ele));
        //self.getSelectedInfo();
        $('#ddlStoreLocations').val('').selectmenu('refresh').trigger('change');
    };
    if (sessionStorage.loanOffiersList != undefined && sessionStorage.loanOffiersList != null && sessionStorage.loanOffiersList != "") {
        var lo_list = [];
        lo_list = $.parseJSON(sessionStorage.loanOffiersList);
        $.each(lo_list, function (key, val) {
            if (key == 0) {
                self.selectedLoanOfficer(val.userId);
            }
            self.loanOffiersList.push(new loanOfficer(val.userId, val.firstName, val.lastName));
        });
    }
}

function convertRadiusToZipCRSelections() {
    $('#popupradiusZipCRRTConfirmDialog').popup('close');
    if ($('#dvPostalBoundarySelections').css('display') == 'block') {
        $('#dvPostalBoundarySelections').collapsible("collapse");
        $('#dvSectionMethod').collapsible("expand");
    }
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function getDelay1() {
        $('#dvSelectByRadius').collapsible('collapse');
        if (gData != null && gData.zips != undefined) {
            var r_crrts = [];
            var total_targets_selected = 0;
            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
                var zip = key.zipCode;
                var index = 0;
                var targeted_counts = 0;
                $.each(key.crrts, function (i, j) {
                    //if (j.selectType == 'r') {
                    j.isChecked = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 1 : 0;
                    j.selectType = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 'f' : '';
                    $('#' + 'check_' + zip + '_' + index).attr('selectedType', j.selectType);
                    j.targeted = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? j.available : 0;
                    $('#spn_' + zip + '_' + index).text(((j.isChecked) ? j.available : 0));
                    targeted_counts = parseInt(targeted_counts) + parseInt(j.targeted);
                    //}
                    index++;
                });
                $('#spnCheckAll' + zip).text(targeted_counts);
                $('#spnTargetedZipCnt' + zip).text(targeted_counts);
                key.targeted = targeted_counts;
                total_targets_selected = parseInt(total_targets_selected) + parseInt(targeted_counts);
            });
            $('#spnTargeted').text(total_targets_selected.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
        sessionStorage.removeItem('updatedCRRTs');
        sessionStorage.removeItem('storeListSelection');
        gSelectedZips = [];
        $('#dvLocations').empty();
        buildZips(selectedStoreId);
        var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
        if ((Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0) && temp_store_list_selection.polygonInfo != undefined && temp_store_list_selection.polygonInfo != null && temp_store_list_selection.polygonInfo != "") {
            delete temp_store_list_selection.polygonInfo;
            sessionStorage.storeListSelection = JSON.stringify(temp_store_list_selection);
        }
        if (gData.polygonInfo != undefined && gData.polygonInfo != null && gData.polygonInfo != "") delete gData.polygonInfo;
        if (gSelectedZips.polygonInfo != undefined && gSelectedZips.polygonInfo != null && gSelectedZips.polygonInfo != "") {
            delete gData.polygonInfo;
        }
        $('#btnPolygon').addClass('ui-disabled');
        if (drawingManager)
            drawingManager.setDrawingMode(null);
        calculateTargetedCnt();
        $('#waitPopUp').popup('close');
        $('#btnSaveSelections').removeClass('ui-disabled');
        $('#btnSaveSelections')[0].disabled = false;
        $('#btnSaveTop').removeClass('ui-disabled');
        $('#btnSaveTop')[0].disabled = false;
        $('#btnSave').removeClass('ui-disabled');
        $('#btnSave')[0].disabled = false;
        $('#rdoPolygon').removeAttr('data-selectedType');
        $('#rdoTargetCounts').removeAttr('data-selectedType');
        $('#rdoRadius').removeAttr('data-selectedType');
        //$("#dvLocations").enhanceWithin();
    }, 1000);
}

function getCRRTSelectionByRadius(ctrl) {
    $('#popupradiusZipCRRTConfirmDialog').popup('open');
    $('#popupradiusZipCRRTConfirmDialog').popup('close');
    if ($('#dvPostalBoundarySelections').css('display') == 'block') {
        $('#dvPostalBoundarySelections').collapsible("collapse");
        $('#dvSectionMethod').collapsible("expand");
    }
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    $('#rdoRadius').attr('checked', false).checkboxradio('refresh');
    $('#rdoPolygon').attr('checked', false).checkboxradio('refresh');
    $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh');
    window.setTimeout(function getDelay2() {
        ctrl = $('#' + ctrl);
        $('#dvSelectByZipCrrt').trigger('collapsiblecollapse');
        if (gData != null && gData.zips != undefined) {
            var r_crrts = [];
            var total_targets_selected = 0;
            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
                var zip = key.zipCode;
                var index = 0;
                var targeted_counts = 0;
                $.each(key.crrts, function (i, j) {
                    j.isChecked = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 1 : 0;
                    j.selectType = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 'r' : '';

                    $('#' + 'check_' + zip + '_' + index).attr('selectedType', j.selectedType);
                    targeted_counts = parseInt(targeted_counts) + parseInt(j.targeted);
                    index++;
                });
                key.targeted = targeted_counts;
                total_targets_selected = parseInt(total_targets_selected) + parseInt(targeted_counts);
            });
            buildZips(selectedStoreId);
            $('#txtRadiusTargeted').val(total_targets_selected.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#spnRadiusTargeted').text(total_targets_selected.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            gData.radius = 0;
        }
        sessionStorage.removeItem('updatedCRRTs');
        sessionStorage.removeItem('storeListSelection');
        gSelectedZips = [];

        if (ctrl.attr('id') == "rdoRadius") {
            $('#rdoTargetCounts').attr('checked', false).checkboxradio('refresh');
            $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('onchange');
        }
        else {
            $('#rdoRadius').attr('checked', false).checkboxradio('refresh');
            $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh').trigger('onchange');
        }

        getSelectedStoreData();
        //calculateTargetedCnt();
        //$('#dvSelectByRadius').trigger('expand');
        $('#waitPopUp').popup('close');
        //$("#dvLocations").enhanceWithin();
    }, 1000);
}

function openHint() {
    if (!dontShowHintsAgain && (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isStoreSelectedFirstTime == undefined || sessionStorage.isStoreSelectedFirstTime == null || sessionStorage.isStoreSelectedFirstTime == "false")) {
        sessionStorage.isStoreSelectedFirstTime = true;
        $('#listControls').popup('close');
        window.setTimeout(function loadHints() {
            $('#listControls').popup('open', { positionTo: '#map' });
        }, 1000);
    }
}

function makeGData() {
    //var zip_select_url = serviceURL + jobCustomerNumber + "/1/" + ((jobCustomerNumber == CW_CUSTOMER_NUMBER) ? jobNumber : caseysStaicJobNumber + '/' + selectedStoreId);
    //    if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER) {
    //        //if (jobNumber == "-1") jobNumber = "-13502"
    //        var zip_select_url = serviceURL + jobCustomerNumber + "/1/" + jobNumber + "/" + selectedStoreId;
    //    }
    //    else
    $('#chkSelectAllPostalBoundaries').attr('checked', false).checkboxradio('refresh');
    var zip_select_url = serviceURL + jobCustomerNumber + "/" + facilityId + "/" + ((jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER) ? jobNumber : caseysStaicJobNumber); //  + '/' + selectedStoreId;
    //zip_select_url += (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) ? '/' + pageObj.currentInHome().dateValue.replace(/\//g, '_') : '';
    var radius_selected = ($('#sldrRange')[0].disabled) ? 0 : $('#sldrRange').val();
    var target_selected = (!$('#sldrRange')[0].disabled && ($('#rdoPolygon').is(':checked') || $('#rdoRadius').is(':checked'))) ? 0 : $('#txtRadiusTargeted').val();
    if (typeof (target_selected) == "string" && target_selected.indexOf(',') > -1)
        target_selected = target_selected.replace(',', '');
    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
        //if (jobCustomerNumber != "99997")
        $('#btnSaveSelections').removeClass('ui-disabled');
        $('#btnSaveSelections')[0].disabled = false;
        var selected_crrts = $('span[id^=spn_]');
        var selected_target = 0;
        //if ($('#hdnZipsChanged').val() == "") {
        //        if ($('#hdnZipsChanged').val() != "") {
        //            if (selected_crrts.length == 0) {
        //                selected_target = target_selected;
        //            }
        //            else {
        //                //$.each($('#hdnZipsChanged').val().split('|'), function (a, b) {
        //                $.each(selected_crrts, function (a, b) {
        //                    //selected_target = parseInt(selected_target) + parseInt($('#spnCheckAll' + b).text());
        //                    selected_target = parseInt(selected_target) + parseInt($(b).text());
        //                    sessionStorage.removeItem('updatedCRRTs');
        //                });
        //                radius_selected = 0;
        //            }
        //        }
        //        else {
        selected_target = target_selected;
        //       }


        //        if ((prevSelectedStoreId == "" || prevSelectedStoreId == 0) || prevSelectedStoreId != selectedStoreId)
        //            zip_select_url += "/0/0";
        //        else
        //            zip_select_url += "/" + radius_selected + "/" + selected_target;

    }
    mapCenter = undefined;
    var temp_g_select_obj = {};
    var selected_demos = [], demo_info = {}, demo_values = [];
    //temp_g_select_obj["needsNewCache"] = (gData != undefined && gData != null && gData != "" && !isStoreChanged) ? 0 : 1;

    //var temp_check_for_saved_store_data = {};
    //if (sessionStorage.checkForSavedStoreData != undefined && sessionStorage.checkForSavedStoreData != null && sessionStorage.checkForSavedStoreData != "") {
    //    temp_check_for_saved_store_data = JSON.parse(sessionStorage.checkForSavedStoreData);
    //}
    var selected_store_id = $('#ddlStoreLocations').find('option:selected').attr('data-storeId');
    temp_g_select_obj["checkForSavedStoreData"] = (selectedStoreId != prevSelectedStoreId) ? 1 : 0; //(temp_check_for_saved_store_data[selected_store_id] != undefined && temp_check_for_saved_store_data[selected_store_id] != null) ? temp_check_for_saved_store_data[selected_store_id] : 1;

    temp_g_select_obj["needsNewCache"] = (selectedStoreId != prevSelectedStoreId) ? 1 : 0; //(temp_check_for_saved_store_data[selected_store_id] != undefined && temp_check_for_saved_store_data[selected_store_id] != null) ? temp_check_for_saved_store_data[selected_store_id] : 1;

    if (prevSelectedStoreId != 0 && selectedStoreId != prevSelectedStoreId)
        selectedDemographics = {};

    selected_demos = makeSelectedDemosObject();
    temp_g_select_obj["demos"] = selected_demos;
    var diff_demos = DiffObjects(prev_demos, selected_demos);
    if (diff_demos.length > 0) {
        temp_g_select_obj["needsNewCache"] = 1;
    }

    prev_demos = selected_demos;
    //if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER)
    //    temp_g_select_obj["inHomeDate"] = pageObj.currentInHome().dateValue;
    temp_g_select_obj["storeId"] = $('#ddlStoreLocations').find('option:selected').attr('data-storeId');
    if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) temp_g_select_obj["storeId"] = selectedStoreId;
    temp_g_select_obj["storePk"] = selectedStoreId;
    temp_g_select_obj["available"] = 0;
    temp_g_select_obj["customerNumber"] = jobCustomerNumber;

    var other_stores_in_job = $.extend(true, [], storesInOrder);
    var other_stores_in_job = $.grep(other_stores_in_job, function (v) {
        return v !== selectedStoreId;
    });

    temp_g_select_obj["otherStoresInOrder"] = other_stores_in_job.join(',');

    if ($('#chkMailAroundCompetitor').is(':checked') && $('#ulSelectedCompetitor li').length > 0) {
        temp_g_select_obj["compLatitude"] = $('#ulSelectedCompetitor li').data('lat');
        temp_g_select_obj["compLongitude"] = $('#ulSelectedCompetitor li').data('lng');
    }
    temp_g_select_obj["targetedAsk"] = ($('#rdoTargetCounts').is(':checked')) ? target_selected : 0;
    temp_g_select_obj["targeted"] = 0; //((prevSelectedStoreId == "" || prevSelectedStoreId == 0) || prevSelectedStoreId != selectedStoreId) ? 0 : selected_target;
    //temp_g_select_obj["radius"] = ($('#rdoRadius').is(':checked')) ? radius_selected : 0; // ((prevSelectedStoreId == "" || prevSelectedStoreId == 0) || prevSelectedStoreId != selectedStoreId) ? 0 : radius_selected;
    temp_g_select_obj["radius"] = ($('#rdoTargetCounts').is(':checked') || $('#rdoPolygon').is(':checked')) ? 0 : (((prevSelectedStoreId == "" || prevSelectedStoreId == 0) || prevSelectedStoreId != selectedStoreId) ? 1 : radius_selected); // ((prevSelectedStoreId == "" || prevSelectedStoreId == 0) || prevSelectedStoreId != selectedStoreId) ? 0 : radius_selected;
    temp_g_select_obj["latitude"] = $('#ddlStoreLocations').find('option:selected').data('lat');
    temp_g_select_obj["longitude"] = $('#ddlStoreLocations').find('option:selected').data('lng');

    //temp_g_select_obj["zoomLevel"] = (map != null && map != undefined && map != '') ? map.getZoom() : this.defaultMapZoom;
    temp_g_select_obj["zoomLevel"] = (isStoreChanged) ? this.defaultMapZoom : ((map != null && map != undefined && map != '') ? map.getZoom() : this.defaultMapZoom);
    temp_g_select_obj["wantsScatter"] = (($('#chkViewByHouseHold').length > 0) && $('#chkViewByHouseHold')[0].checked) ? 1 : 0;
    temp_g_select_obj["wantsRadius"] = ($('#chkViewRadius')[0].checked) ? 1 : 0;

    //temp_g_select_obj["wantsCrrtMarkers"] = ($('#chkViewCRM').length > 0 && $('#chkViewCRM')[0].checked) ? 1 : 0;

    temp_g_select_obj["wantsCrrtBorders"] = ($('#chkViewCRB')[0].checked) ? 1 : 0;
    temp_g_select_obj["wantsZipBorders"] = ($('#chkViewZCB')[0].checked) ? 1 : 0;
    temp_g_select_obj["wantsZipCentroids"] = (appPrivileges.roleName == "admin" && $('#chkViewZipCentroids')[0].checked) ? 1 : 0;
    temp_g_select_obj["wantsCrrtCentroids"] = (appPrivileges.roleName == "admin" && $('#chkViewCRRTCentroids')[0].checked) ? 1 : 0;
    temp_g_select_obj["wantsSurroundingLocations"] = (($('#chkViewSurroundingLocations').length > 0) && $('#chkViewSurroundingLocations')[0].checked) ? 1 : 0;

    if ($("#rdoPolygon").is(':checked')) {
        var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
        if (Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0 && temp_store_list_selection.polygonInfo != undefined && temp_store_list_selection.polygonInfo != null && temp_store_list_selection.polygonInfo != "") {
            temp_g_select_obj["polygonInfo"] = temp_store_list_selection.polygonInfo;
        }
    }

    temp_g_select_obj["zips"] = ((prevSelectedStoreId == "" || prevSelectedStoreId == 0) || prevSelectedStoreId != selectedStoreId) ? [] : gSelectedZips.zips;
    makePeripheralSelections();
    temp_g_select_obj["zips"] = gSelectedZips.zips;

    temp_g_select_obj["jobTypeId"] = gOutputData.jobTypeId;
    //var peri_targets = $('#spnPheripheralTargets').text();
    //var temp_ver = "";
    //if (selectedShape)
    //    temp_ver = google.maps.geometry.encoding.encodePath(selectedShape.getPath().getArray());
    //if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed') && parseInt(peri_targets) > 0) {
    //    if (($('#rdoTargetCounts').is(':checked') && gSelectedZips.targetedAsk != undefined && gSelectedZips.targetedAsk != null && gSelectedZips.targetedAsk != "" && target_selected != "" && target_selected > 0 && target_selected != gSelectedZips.targetedAsk) ||
    //        ($('#rdoRadius').is(':checked') && gSelectedZips.radius != undefined && gSelectedZips.radius != null && gSelectedZips.radius != "" && radius_selected != "" && radius_selected > 0 && radius_selected != gSelectedZips.radius) ||
    //        ($('#rdoPolygon').is(':checked') && gSelectedZips.polygonInfo != undefined && gSelectedZips.polygonInfo != null && gSelectedZips.polygonInfo != "" && temp_ver != "" && temp_ver != gSelectedZips.polygonInfo)) {
    //        temp_g_select_obj["zips"] = [];
    //    }
    //}
    if (!isZipsChanged)
        temp_g_select_obj["zips"] = [];
    // }
    //getCORS(zip_select_url, null, function (data) {

    //THE below has to be opend if it is required to set the REGIS auth string to ALLIED customer at the below mentioned commented numbers
    //#1 - START
    //var temp_auth_string = sessionStorage.authString;
    //if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
    //    sessionStorage.authString = "Basic dXNlcl9yZWcxOnVzZXJfcmVnMQ==";
    //}
    //#1 - END
    var temp_demos = [];

    postCORS(zip_select_url, JSON.stringify(temp_g_select_obj), function (data) {
        //temp_check_for_saved_store_data[selected_store_id] = 0;
        //sessionStorage.checkForSavedStoreData = JSON.stringify(temp_check_for_saved_store_data);
        if (data.demos == undefined || data.demos == null || data.demos == "")
            delete data["demos"];
        if (data.polygonInfo == undefined || data.polygonInfo == null || data.polygonInfo == "")
            delete data["polygonInfo"];
        if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
            //#2 - START
            //sessionStorage.authString = temp_auth_string;
            //#2 - END
            if (data.zips.length == 0 && Object.keys(tempStaticJson).length > 0 && tempStaticJson.zips.length > 0) {
                temp_g_select_obj.zips = tempStaticJson.zips;
                data = temp_g_select_obj;
            }
        }
        //data["targetedAsk"] = target_selected;
        //data["storeId"] = $('#ddlStoreLocations').find('option:selected').attr('data-storeId');
        //if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) data["storeId"] = selectedStoreId;
        //data["storePk"] = selectedStoreId;
        if (data.compLongitude == 0)
            delete data["compLongitude"];
        if (data.compLatitude == 0)
            delete data["compLatitude"];
        temp_demos = (data.demos != undefined && data.demos != null) ? data.demos : [];
        if (data.checkForSavedStoreData != undefined && data.checkForSavedStoreData != null && data.checkForSavedStoreData == 1) {
            makeDemosObject(temp_demos);
            isDemosDefaulted = true;
            // createDemographicsList();
            //createSelectedDemographics();
        }
        else {
            makeDemosObject(temp_demos)
        }

        //if(sessionStorage)

        gDataAll = data;
        gData = data;

        $('#dvPostalBoundarySelections').css('display', 'block');
        $('#sldrRange').attr('hasChanged', false);
        $('#sldrRange').val(data.radius).slider('refresh');
        $('#sldrRange').attr('prevValue', $('#sldrRange').val());
        if (data.zips.length > 0)
            $('#dvPostalBoundarySelections').css('display', 'block');
        else
            $('#dvPostalBoundarySelections').css('display', 'none');
        var temp_poly_options = [];
        //var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
        //if (Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0) {
        //    if (temp_store_list_selection.polygonInfo != undefined && temp_store_list_selection.polygonInfo != null && temp_store_list_selection.polygonInfo != "" && temp_store_list_selection.polygonInfo.length > 0) {
        //        if (gData.polygonInfo == undefined || gData.polygonInfo == null || gData.polygonInfo == "")
        //            gData["polygonInfo"] = temp_store_list_selection.polygonInfo;
        //    }
        //}
        sessionStorage.storeListSelection = JSON.stringify(gData);

        if (prevSelectedStoreId != selectedStoreId)
            initialStoreData = data;
        $('#hdnSelectedZip').val('');
        gSelectedZips.radius = data.radius;
        prevSelectedStoreId = selectedStoreId;
        var mode = verifyCRRTsMode();

        if (mode == "radius") {
            $('#dvSelectByRadius').collapsible('expand');
            //$('#dvSelectByRadius').trigger('collapsibleexpand');
        }
        else if (mode == "crrt") {
            $('#dvSelectByZipCrrt').collapsible('expand');
            //$('#dvSelectByZipCrrt').trigger('collapsibleexpand');
        }

        $('#waitPopUp').popup('close');
        $('input[type=radio][name=rdoCounts]').attr('checked', false).checkboxradio('refresh');
        if (gData.polygonInfo != undefined && gData.polygonInfo != null && gData.polygonInfo != "") {
            window.setTimeout(function () {
                $('#rdoPolygon').attr('checked', true).checkboxradio('refresh');//.trigger('change');
            }, 500);
            getCountsBy($('#rdoPolygon')[0]);
            $('#btnPolygon').attr('disabled', true);
            $('#btnPolygon').addClass('ui-disabled');
        }
        else if (parseInt(data.targetedAsk) > 0 || $('#rdoTargetCounts').is('checked')) {
            window.setTimeout(function () {
                $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh');//.trigger('change');
            }, 500);
            getCountsBy($('#rdoTargetCounts')[0]);
        }
        else {
            window.setTimeout(function () {
                $('#rdoRadius').attr('checked', true).checkboxradio('refresh');//.trigger('change');
            }, 500);
            getCountsBy($('#rdoRadius')[0]);
        }

        //if (jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER && !$('#chkAcquisitionTargets').attr('checked')) {
        //    $('#dvPostalBoundarySelections').css('display', 'none');
        //}
        isZipsChanged = false;
        isStoreChanged = false;
        isEnterPress = false;
    }, function (error_response) {
        if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
            //#3 - START
            //sessionStorage.authString = temp_auth_string;
            //#3 - END
        }
        $('#dvLocations').empty();
        $('#dvPostalBoundarySelections').css('display', 'none');
        $('#waitPopUp').popup('close');
        showErrorResponseText(error_response, false);
        //$('#alertmsg').text($('#divError').text());
        //$('#divError').css('display', 'none');
        //$('#popupDialog').popup('open');
    });
}

function makeDemosObject(temp_demos) {
    demo_groups = {};
    $.each(mappingDemographics, function (key, val) {
        temp_key = key;
        if (!demo_groups[val.columnDescription]) {
            demo_groups[val.columnDescription] = {};
            demo_groups[val.columnDescription]["menuType"] = val.menuType;
            demo_groups[val.columnDescription]["values"] = {};
        }

        demo_groups[val.columnDescription].values[val.valueDescription] = $.extend(true, {}, val);
        if (temp_demos.length > 0) {
            $.each(temp_demos, function (key1, val1) {
                if (val1.demoName == val.columnDescription) {
                    $.each(val1.demoValues, function (key2, val2) {
                        if (val2.name == val.valueDescription) {
                            demo_groups[val.columnDescription].values[val.valueDescription].selector = (val1.shouldExclude == 0 || val1.shouldExclude == null) ? "in" : ((val1.shouldExclude == 1) ? "not in" : "");
                            return false;
                        }
                    });
                    demo_groups[val.columnDescription]["isDefaultValue"] = val1.isDefaultValue;
                }
            });
        }

    });
}

function verifyCRRTsMode() {
    var select_mode = "";
    if (gData.zips.length > 0) {
        $.each(gData.zips, function (a, b) {
            var is_r_found = false;
            var is_f_found = false;
            if (b.crrts.length > 0) {
                $.each(b.crrts, function (c, d) {
                    if (d.selectType == "r") {
                        is_r_found = true;
                        return false;
                    }
                    else if (!is_r_found && d.selectType == "f") {
                        is_f_found = true;
                    }
                });
            }
            if (is_r_found) {
                select_mode = "radius";
                return false;
            }
            if (!is_r_found && is_f_found)
                select_mode = "crrt";
        });
    }
    return select_mode;
}

function displayListFileInfo() {
    if (sessionStorage.jobSetupOutput != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if (gOutputData != null) {
            getUploadedFilesCount();
        }
        loadStoreLocations();
        if (gOutputData.modulesAction != undefined && gOutputData.modulesAction != null && gOutputData.modulesAction != "" && gOutputData.modulesAction.modulesDict != null && gOutputData.modulesAction.modulesDict != null && gOutputData.modulesAction.modulesDict != "") {
            getCustNavLinks();
            //createNavLinks();
            displayNavLinks();
        }
        else {
            displayNavLinks();
        }
    }
}
//load the store locations that are selected in the "select location" job page.
function loadStoreLocations() {
    //var loaded_stores_data = [];
    //if (gOutputData != undefined && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
    //    loaded_stores_data = gOutputData.selectedLocationsAction.selectedLocationsList;
    //    loadStoresOld(loaded_stores_data);
    //} else {
    //    if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
    //        $('#alertmsg').html("List Selection requires locations to be added on the Select Locations page. You may continue to add locations before continuing with List Selection.");
    //        //$('#okBut').attr('data-mini', 'true');
    //        //            $('#okBut').bind('click', function () {
    //        //                window.location.href = "../jobSelectLocations/jobSelectLocations.html";
    //        //            });
    //        //            $('#okBut').find('.ui-btn-text').text('Select Locations');
    //        $('#popupDialog').popup('open');
    //    }
    //    else {//if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER) {
    //        $('#alertmsg').html("List Selection requires locations to be added on the Select Locations page. You may either continue to Upload List or return to Select Locations to add locations before continuing with List Selection.");
    //        $('<a href="#" data-role="button" data-inline="true" data-theme="a" id="okBut1">Upload List</a>').insertBefore('#okBut');
    //        $('#popupDialog').find('div[data-role="content"]').trigger('create');
    //        $('#okBut').bind('click', function () {
    //            window.location.href = "../jobSelectLocations/jobSelectLocations.html";
    //        });
    //        $('#okBut1').bind('click', function () {
    //            window.location.href = "../jobUploadList/jobUploadList.html";
    //        });
    //        $('#okBut').text('Select Locations');
    //        $('#popupDialog').popup('open');
    //    }
    getCORS(serviceURLDomain + "api/Locations_dropDown/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + ((appPrivileges.customerNumber == CW_CUSTOMER_NUMBER) ? jobNumber : caseysStaicJobNumber) + "/0/2/" + gOutputData.jobTypeId, null, function (data) {
        if (data.locationsSelected != undefined && data.locationsSelected != undefined != null && data.locationsSelected != "" && Object.keys(data.locationsSelected).length > 0) {
            locationsList = data.locationsSelected;
            loadStoresOld(locationsList);
        }
        else {
            if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                $('#alertmsg').html("List Selection requires locations to be added on the Select Locations page. You may continue to add locations before continuing with List Selection.");
                //$('#okBut').attr('data-mini', 'true');
                //            $('#okBut').bind('click', function () {
                //                window.location.href = "../jobSelectLocations/jobSelectLocations.html";
                //            });
                //            $('#okBut').find('.ui-btn-text').text('Select Locations');
                $('#popupDialog').popup('open');
            }
            else {//if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER) {
                $('#alertmsg').html("List Selection requires locations to be added on the Select Locations page. You may either continue to Upload List or return to Select Locations to add locations before continuing with List Selection.");
                $('<a href="#" data-role="button" data-inline="true" data-theme="a" id="okBut1">Upload List</a>').insertBefore('#okBut');
                $('#popupDialog').find('div[data-role="content"]').trigger('create');
                $('#okBut').bind('click', function () {
                    window.location.href = "../jobSelectLocations/jobSelectLocations.html";
                });
                $('#okBut1').bind('click', function () {
                    window.location.href = "../jobUploadList/jobUploadList.html";
                });
                $('#okBut').text('Select Locations');
                $('#popupDialog').popup('open');
            }
        }
    }, function (error_response) {
        showErrorResponseText(error_response);
    });
    // }


    //if (gOutputData != undefined && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
    //    loaded_stores_data = gOutputData.selectedLocationsAction.selectedLocationsList;
    //    loadStores(loaded_stores_data);
    //}
    //else {
    //    if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER) {
    //        $('#alertmsg').html("List Selection requires locations to be added on the Select Locations page. You may continue to add locations before continuing with List Selection.");
    //        //$('#okBut').attr('data-mini', 'true');
    //        //            $('#okBut').bind('click', function () {
    //        //                window.location.href = "../jobSelectLocations/jobSelectLocations.html";
    //        //            });
    //        //            $('#okBut').find('.ui-btn-text').text('Select Locations');
    //        $('#popupDialog').popup('open');
    //    }
    //    else {//if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER) {
    //        $('#alertmsg').html("List Selection requires locations to be added on the Select Locations page. You may either continue to Upload List or return to Select Locations to add locations before continuing with List Selection.");
    //        $('<a href="#" data-role="button" data-inline="true" data-theme="a" id="okBut1">Upload List</a>').insertBefore('#okBut');
    //        $('#popupDialog').find('div[data-role="content"]').trigger('create');
    //        $('#okBut').bind('click', function () {
    //            window.location.href = "../jobSelectLocations/jobSelectLocations.html";
    //        });
    //        $('#okBut1').bind('click', function () {
    //            window.location.href = "../jobUploadList/jobUploadList.html";
    //        });
    //        $('#okBut').text('Select Locations');
    //        $('#popupDialog').popup('open');
    //    }
    //}
    //    else {
    //        getCORS(serviceURLDomain + "api/StoreStatus/" + jobCustomerNumber + "/1/" + ((jobCustomerNumber == CW_CUSTOMER_NUMBER) ? jobNumber : caseysStaicJobNumber) + "/0/all", null, function (data) {
    //            if (data["opted-in"] != undefined && data["opted-in"] != null) {
    //                if (gOutputData != null && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != null) {
    //                    gOutputData.selectedLocationsAction.selectedLocationsList = data["opted-in"];
    //                    gOutputData.selectedLocationsAction.selectedLocationsList = gOutputData.selectedLocationsAction.selectedLocationsList.sort(function (a, b) {
    //                        return parseInt(a.storeId) > parseInt(b.storeId) ? 1 : -1;
    //                    });
    //                    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
    //                    loadStores(gOutputData.selectedLocationsAction.selectedLocationsList);
    //                }
    //            }
    //        }, function (error_response) {
    //            showErrorResponseText(error_response);
    //        });
    //    }
}
function loadStores(loaded_stores_data) {
    var stores_data = '<select name="ddlStoreLocations" id="ddlStoreLocations" data-theme="a" data-mini="true" onchange="selectedStoreChanged();">';
    stores_data += '<option value="">Select Store</option>';
    var store_display_name = "";
    $.each(loaded_stores_data, function (key, val) {
        store_display_name = key + ' - ' + val;
        stores_data += '<option value="' + key + '">' + store_display_name + '</option>';
    });
    stores_data += '</select>';
    //stores_data += '* <span style="font-size:12px">Pending Approval</span>';
    $('#dvSelectedLocations').empty();
    $('#dvSelectedLocations').html(stores_data);
    $('#dvSelectedLocations select').each(function (i) {
        $(this).selectmenu();
    });
}

function loadStoresOld(loaded_stores_data) {
    var loc_config = jQuery.parseJSON(sessionStorage.locationConfig)
    var needsCount = false;
    var stores_data = '<select name="ddlStoreLocations" id="ddlStoreLocations" data-theme="a" data-mini="true" onchange="selectedStoreChanged();">';
    stores_data += '<option value="">Select Store</option>';
    var store_display_name = "";

    var saved_stores = (sessionStorage.savedStores != undefined && sessionStorage.savedStores != null && sessionStorage.savedStores != "") ? $.parseJSON(sessionStorage.savedStores) : [];
    $.each(loaded_stores_data, function (key, val) {
        var display_name = (val.storeId != undefined && val.storeId != null && val.storeId != "") ? val.storeId + ' - ' + val.city + ', ' + val.state : val.city + ', ' + val.state;
        store_display_name = (val.pendingApproval != undefined && val.pendingApproval != null && val.pendingApproval == 1) ? display_name + ' *' : display_name;
        var store_id = "";
        var store_type = "";
        if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
            store_id = (($.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == null) && val.storeId != undefined && val.storeId != null && val.storeId != "") ? val.storeId : '';
            store_type = (($.parseJSON(sessionStorage.locationConfig).hidden["storeType"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["storeType"] == null) && val.storeType != undefined && val.storeType != null && val.storeType != "") ? val.storeType : '';
            display_name = (store_id !== "") ? store_id : "";
            display_name += (store_type != "") ? (((display_name != "") ? ' - ' : '') + store_type) : ((store_id == "") ? val.storeName : '');
            display_name += (display_name != "") ? ' - ' + val.city + ', ' + val.state : val.city + ', ' + val.state;
            //display_name = (val.storeId != undefined && val.storeId != null && val.storeId != "") ? val.storeId + ' - ' + val.storeName + ' - ' + val.city + ', ' + val.state : val.storeName + ' - ' + val.city + ', ' + val.state;
            //store_display_name = display_name + ((val.needsCount != undefined && val.needsCount != null && JSON.parse(val.needsCount.toString().toLowerCase())) ? ' *' : '');
            store_display_name = display_name;
        } else if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
            store_id = (($.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == null) && val.storeId != undefined && val.storeId != null && val.storeId != "") ? val.storeId : '';
            display_name = (store_id !== "") ? store_id : "";
            display_name += (display_name != "") ? ' - ' + val.location.substring(0, 17) + '...' + ' (' + val.city + ')' : val.location; // ' ('+val.city +')'
        }
        store_display_name = (saved_stores.length > 0 && saved_stores.indexOf(val.pk) > -1) ? store_display_name + ' *' : store_display_name;
        store_display_name = display_name;
        var last_update_date = (val.lastUpdateDate) ? val.lastUpdateDate : "";
        if (saved_stores.length > 0 && saved_stores.indexOf(val.pk) > -1) {
            var style_info = "background-color:lightblue;";
            stores_data += '<option value="' + val.pk + '" data-storeId="' + val.storeId + '" brandName="' + store_type + '" data-lat="' + val.latitude + '" data-lng="' + val.longitude + '" data-lastUpdated="' + last_update_date + '" style="' + style_info + '" title="' + (store_id + (store_id != "" ? ' - ' : '') + val.location + ' (' + val.city + ')') + '">' + store_display_name + '</option>';
        }
        else
            stores_data += '<option value="' + val.pk + '" data-storeId="' + val.storeId + '" brandName="' + store_type + '" data-lat="' + val.latitude + '" data-lng="' + val.longitude + '" data-lastUpdated="' + last_update_date + '"  title="' + (store_id + (store_id != "" ? ' - ' : '') + val.location + ' (' + val.city + ')') + '">' + store_display_name + '</option>';
        storesInOrder.push(val.pk);
    });
    stores_data += '</select>';
    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) stores_data += '<div align="left" style="padding-left:12px;font-size:x-small;text-align:left">* List Saved</div>';

    if ((jobCustomerNumber == REGIS_CUSTOMER_NUMBER))
        stores_data += '<br />* <span style="font-size:12px">Location needs to be reviewed and saved</span>';

    $('#dvSelectedLocations').empty();
    $('#dvSelectedLocations').html(stores_data);
    $('#dvSelectedLocations select').each(function (i) {
        $(this).selectmenu();
    });
}

function getMailCompititors() {
    if ($('#chkMailAroundCompetitor').attr('checked') == 'checked') {
        $('#lblMailAroundCompetitor').attr('data-theme', 'c');
        $('#lblMailAroundCompetitor').trigger('create');
        //refreshMap();
        $('#dvSearchCompititor').css('display', 'block');
    }
    else {
        $('#lblMailAroundCompetitor').attr('data-theme', '');
        $('#lblMailAroundCompetitor').trigger('create');
        $('#chkMailAroundCompetitor').attr('data-theme', '');
        $('#dvSearchCompititor').css('display', 'none');
        resetMarkersArray();
        clearSelectedMarkers();
        resetCircles();
        //drawMarkerForSelecetedStore();
        delete gSelectedZips.compLatitude;
        delete gSelectedZips.compLongitude;
        sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
        $('#ulSelectedCompetitor').empty();
        $('#spnCompLocWarning').remove();
        $('#btnRefresh').trigger('click');
    }
}

function getAcquisitionTargets() {
    if ($('#chkAcquisitionTargets').attr('checked') == 'checked') {
        $('#dvSectionMethod').css('display', 'block');
        $('#dvEmailSelections').css('display', 'block');
        $('#dvPostalBoundarySelections').css('display', 'block');
        $('#dvAcquisitionTargets').css('display', 'block');
        $('#postalSec .ui-btn').text("Acquisition Selections");
    }
    else {
        $('#dvSectionMethod').css('display', 'none');
        $('#dvEmailSelections').css('display', 'none');
        $('#dvPostalBoundarySelections').css('display', 'none');
    }
}

function competitorStoreChange() {
    var element = document.getElementById('ddlSearchStates');
    if ($.browser.msie) {
        element.focus();
        setTimeout(function () {
            element.focus();
        }, 1);
    }
    else {
        element.focus();
        //var select = $(this).val();
    }
}

function selectedStoreChanged() {
    if ($('#ddlStoreLocations').val() == "") {
        $('#sldrRange').slider('disable');
        if ($('#sldrRange')[0] != undefined)
            $('#sldrRange')[0].disabled = true;
        isDemosChanged = false;
        initialStoreData = {};
        selectedDemographics = {};
        prevSelectedStoreId = "";
        $('#popupMapPrefs').panel('close');
        resetSelections();
    }
    else {
        var tempSelectedStoreId = $('#ddlStoreLocations').attr('value');
        if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && eventLocationsInfo.locationList != undefined && pageObj != undefined && pageObj != null && pageObj != "") {
            var invalid_select_location = false;
            $.each(eventLocationsInfo.locationList, function (key, val) {
                if (key == pageObj.currentInHome().dateValue && eventLocationsInfo.locationList[key].length != undefined && eventLocationsInfo.locationList[key] != tempSelectedStoreId) {
                    invalid_select_location = true;
                }
            });
            if (invalid_select_location == true) {
                error_msg = 'You may only select one location per event. If you want to change the location for this event, click Change Event Location. If you want to select the location for another event, click Cancel then switch events using the buttons at the top of the screen.';
                $('#confirmMsg').html(error_msg);
                $('#okButConfirm').text('Change Event Location');
                $('#cancelButConfirm').text('Cancel');
                $('#cancelButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    cancelSelections();
                    $('#popupConfirmDialog').popup('close');
                });
                $('#okButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#okButConfirm').unbind('click');
                    $('#popupConfirmDialog').popup('close');
                    $.each(eventLocationsInfo.locationList, function (key, val) {
                        if (key != pageObj.currentInHome().dateValue && eventLocationsInfo.locationList[key] != undefined && eventLocationsInfo.locationList[key].length > 0) {
                            eventLocationsInfo.locationList[key] = {};
                        }
                    });
                    window.setTimeout(function () {
                        $('#dvAcqMailingList').collapsible('expand');
                        //updateSelectedStore(tempSelectedStoreId);
                    }, 1000);
                });
                $('#popupConfirmDialog').popup('open');
                return false;
            }
        }
        else {
            //if (prevSelectedStoreId == undefined)
            $('#dvAcqMailingList').collapsible('expand');
            //else
            //    updateSelectedStore(tempSelectedStoreId);
            //$('#dvAcqMailingList').trigger('collapsibleexpand');
        }
        //if ($("#dvMailingTypes div[data-role=collapsible]").hasClass('ui-collapsible-collapsed')) {
        //    //self.selectedMailingType('acquisition');
        //    $("#dvAcqMailingList").collapsible("expand");
        //}
        //updateSelectedStore(tempSelectedStoreId);
        $('#dvLeftPanel').css('height', 'auto');
    }
}

function updateSelectedStore(selected_storeId) {
    //$('#sldrRange').val('1').slider('refresh');
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && pageObj != undefined && pageObj != null && pageObj != "")
        eventLocationsInfo.locationList[pageObj.currentInHome().dateValue] = selected_storeId;
    if (jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER) {
        $('#dvMailAroundCompitor').css('display', 'block');
        $('#dvMailAroundCompitor').attr('data-theme', 'b');
        $('#dvMailAroundCompitor').trigger('refresh', 'b');
        $('#dvMailAroundCompitor').trigger('create');
    } else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
        $('#dvEmailList').css('display', 'none');
        $('#dvUSMailingList').css('display', 'none');
    }

    //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');

    //To be opened when the compitior selection is completed
    //$('#chkViewByHouseHold').attr('checked', false).checkboxradio('refresh');
    var selected_crrts = ($('#hdnSelectedZip').val() != "") ? $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked') : [];
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length > 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        //diff = getDifferences(selectedCRRTS, selected_crrts);
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        //diff = getDifferences(selected_crrts, selectedCRRTS);
        diff = getDifferences(selectedCRRTS, selected_crrts);

    if (diff.length == undefined || diff.length == 0) {
        if (sessionStorage.storeListSelection != undefined) {
            var selected_text = $('#hdnSelectedStore').val();
            selected_text = $('#ddlStoreLocations option:selected').text();
            var message = "";
            //initialStoreData
            if (gSelectedZips["zoomLevel"]) delete gSelectedZips["zoomLevel"];
            var diff_obj = DiffObjects(gSelectedZips, initialStoreData);
            var temp_diff = 0;
            temp_diff = ($('#hdnSelectedZip').val() != "") ? selected_crrts.length : diff_obj.length;
            if ((selected_text != "" && selectedCRRTS.length == 0) && !isTargetChanged && !isRadiusChanged && !isDemosChanged && temp_diff > 0 && prevSelectedStoreId != selected_storeId) {//|| selected_crrts.length > 0
                message = "The list for this location has not been saved.<br /><br />";
                $('#storeMsg').html(message);
                $('#btnModelSave').text('Save');
                $('#btnModelRevert').text('Continue');
                $('#popupStoreDialog').popup('open');
            }
            else if ((selected_text != "" && selectedCRRTS.length > 0) || (isTargetChanged || isRadiusChanged || isDemosChanged) && prevSelectedStoreId != selected_storeId) {//|| selected_crrts.length > 0
                //message = "The Targeted Selections have been changed for Store '" + selected_text + "' " +
                //          "To select another location you must either Save Selections or Revert to the previously saved selections.";
                message = "The list for this location has not been saved. If you would like to continue without saving, you will need to Revert the changes you have made first.<br /><br />";
                $('#storeMsg').html(message);
                $('#btnModelSave').text('Save');
                $('#btnModelRevert').text('Revert');
                $('#popupStoreDialog').popup('open');
            }
            else {
                $('#waitPopUp').popup('open', { positionTo: 'window' });

                $('#hdnZipsChanged').val('');

                $('#rdoPolygon').removeAttr('data-selectedType');
                $('#rdoTargetCounts').removeAttr('data-selectedType');
                $('#rdoRadius').removeAttr('data-selectedType');

                if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                    if ((gOutputData != undefined && gOutputData != null && gOutputData != '') && gOutputData.templateName.toLowerCase().indexOf('delivery') > -1) {
                        $('#rdoPolygon').attr('checked', true).checkboxradio('refresh');
                    }
                    else {
                        $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh');
                    }
                    if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER) {// || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                        $('#sldrRange').attr('max', '50');
                    }
                    $('#sldrRange').slider('disable');
                    $('#sldrRange')[0].disabled = true;
                    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                        $('#txtRadiusTargeted').val('3000');
                        $('#spnRadiusTargeted').val('3000');
                    }
                }
                else {
                    $('#rdoRadius').attr('checked', true).checkboxradio('refresh');
                    $('#sldrRange').slider('enable');
                    $('#sldrRange')[0].disabled = false;
                }
                //$('#txtRadiusTargeted').val('3000');
                //$('#spnRadiusTargeted').val('3000');

                //if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER )
                if ($("input[name*=rdo]:checked").length == 0)
                    $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh').trigger('change');
                //else {
                //    $('#sldrRange').attr('hasChanged', false);
                //    $('#sldrRange').val('1').slider('refresh');
                //    $('#rdoTargetCounts').attr('checked', false).checkboxradio('refresh');
                //    $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
                //}
                if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                    $('#rdoRadius').attr('checked', $('#rdoRadius').is(':checked')).checkboxradio('refresh');
                    $('#rdoPolygon').attr('checked', $('#rdoPolygon').is(':checked')).checkboxradio('refresh');
                    $('#rdoTargetCounts').attr('checked', ((!$('#rdoRadius').is(':checked') && !$('#rdoPolygon').is(':checked') && !$('#rdoTargetCounts').is(':checked')) || $('#rdoTargetCounts').is(':checked'))).checkboxradio('refresh');
                    //$('#txtRadiusTargeted').val('3000');
                    $('#txtRadiusTargeted').val((selectedStoreId != prevSelectedStoreId) ? "3000".toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : $('#txtRadiusTargeted').val().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $('#spnPheripheralTargets').html(0);
                    $('#spnRadiusTargeted').html(0);
                    $('#spnTotalTargets').html(0);
                    $('#trPheripheralTargets').css('display', 'none');
                    $('#trTargetsSeparator').css('display', 'none');
                    $('#trTotalTargets').css('display', 'none');
                }
                window.setTimeout(function setDelay() {
                    if ($('#rdoRadius')[0].checked) {
                        $('#sldrRange').slider('enable');
                        $('#sldrRange')[0].disabled = false;
                    }
                    getSelectedStoreData();
                }, 1000);

                selectedCRRTS = [];
            }
        }
        else {
            $('#waitPopUp').popup('open', { positionTo: 'window' });
            $('#hdnZipsChanged').val('');
            $('#sldrRange').attr('hasChanged', false);
            //$('#sldrRange').val('1').slider('refresh');
            //if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER)
            if ($("input[name*=rdo]:checked").length == 0)
                $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh').trigger('change');



            //else {
            //    $('#rdoTargetCounts').attr('checked', false).checkboxradio('refresh');
            //    $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
            //}
            if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                $('#rdoRadius').attr('checked', $('#rdoRadius').is(':checked')).checkboxradio('refresh');
                $('#rdoPolygon').attr('checked', $('#rdoPolygon').is(':checked')).checkboxradio('refresh');
                $('#rdoTargetCounts').attr('checked', ((!$('#rdoRadius').is(':checked') && !$('#rdoPolygon').is(':checked') && !$('#rdoTargetCounts').is(':checked')) || $('#rdoTargetCounts').is(':checked'))).checkboxradio('refresh');
                //$('#txtRadiusTargeted').val('3000');
                $('#txtRadiusTargeted').val((selectedStoreId != prevSelectedStoreId) ? "3000".toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : $('#txtRadiusTargeted').val().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $('#spnPheripheralTargets').html(0);
                $('#spnRadiusTargeted').html(0);
                $('#spnTotalTargets').html(0);
                $('#trPheripheralTargets').css('display', 'none');
                $('#trTargetsSeparator').css('display', 'none');
                $('#trTotalTargets').css('display', 'none');
            }
            window.setTimeout(function setPause() {
                if ($('#rdoRadius')[0].checked) {
                    $('#sldrRange').slider('enable');
                    $('#sldrRange')[0].disabled = false;
                }
                getSelectedStoreData();
            }, 1000);
            selectedCRRTS = [];
        }
        //prevSelectedStoreId = $('#hdnSelectedStore').val();//($('#hdnSelectedStore').val() != "") ? $('#hdnSelectedStore').val().substring(0, $('#hdnSelectedStore').val().indexOf(' ')) : 0;
        //$('#hdnSelectedStore').val($('#ddlStoreLocations option:selected').text());
        $('#hdnSelectedStore').val($('#ddlStoreLocations option:selected').val());
    }
    else {

        if (selected_crrts.length > 0 || selectedCRRTS.length > 0) {
            var selected_text = $('#hdnSelectedStore').val();
            selected_text = $('#ddlStoreLocations option:selected').text();
            var msg = "The Targeted Selections have been changed for Store '" + selected_text + "'. " +
                  "To select another location you must refresh the map. Click 'Ok' to Refresh Map";
            $('#confirmMsg2').text(msg);
            $('#popupConfirmMessage').popup('open');
        }
    }
    $('#btnRefresh').removeClass('ui-disabled');
    $('#btnRefresh')[0].disabled = false;

    $('#dvSectionMethod').removeClass("ui-disabled");
    $('#dvSectionMethod')[0].disabled = false;
    $('#dvSectionMethod').collapsible("expand");
    makeReadOnlyZips();
    openHint();
}

function getSelectedStoreData() {
    //if (jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER) {
    //    $('#map').html(((jobCustomerNumber == AAG_CUSTOMER_NUMBER) ? '<img src="images/aag_map_image.png" width="100%"/>' : '<img src="images/dca_map_image.png"/>'));
    //    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
    //        $('#txtRadiusTargeted').val('2500');
    //        $('#spnRadiusTargeted').val('2500');
    //    }
    //    $('#waitPopUp').popup('close');
    //    $('#map').show();
    //}
    //else {
    if ($('#dvPostalBoundarySelections').css('display') == 'block') {
        $('#dvPostalBoundarySelections').collapsible("collapse");
        $('#dvSectionMethod').collapsible("expand");
    }
    selectedStoreId = $('#ddlStoreLocations').attr('value');
    isStoreChanged = true;
    if ($('#ddlStoreLocations').val() != "select") {
        var selected_store = $('#ddlStoreLocations').find(":selected")
        if (selected_store.length > 0 && jobCustomerNumber == REGIS_CUSTOMER_NUMBER)
            sessionStorage.brandName = $('#ddlStoreLocations').find(":selected").attr('brandName').replace(/ /g, '');
    }
    //demo_groups = {};
    //
    /*$.each(mappingDemographics, function (key, val) {
        temp_key = key;
        if (!demo_groups[val.columnDescription]) {
            demo_groups[val.columnDescription] = {};
            demo_groups[val.columnDescription]["menuType"] = val.menuType;
            demo_groups[val.columnDescription]["values"] = {};
        }
        demo_groups[val.columnDescription].values[val.valueDescription] = val;

    });*/
    $('#ulDemographics').empty();

    //selectedStoreId = "2218"; // TO BE Removed and used above commented line when the service url becomes dynamic.
    makeGData();
    createDemographicsList();
    createSelectedDemographics();

    if (gData == undefined || gData == null || gData == "" || gData.zips == undefined || gData.zips == null || gData.zips == "" || gData.zips.length == 0) {
        $('#map').empty();
        $('#map').hide();

        $('#dvLocations').empty();
        $('#divAvailable').empty();
        $('#spnRadiusAvblCount').empty();
        $('#txtTargeted').empty();
        $('#spnTargeted').text('0');
        $('#btnSaveSelections').addClass('ui-disabled');
        $('#btnSaveSelections')[0].disabled = true;
        $('#btnRefresh').addClass('ui-disabled');
        $('#btnRefresh')[0].disabled = true;
        $('#alertmsg').html('No data available for selected combination.');
        $('#popupDialog').popup('open');
        return;
    } else {
        var list_saved_info = "";
        //list_saved_info = $('#ddlStoreLocations option:selected').attr('data-lastUpdated');
        //list_saved_info = (list_saved_info != "") ? '<span>List Saved:' + list_saved_info + '</span>' : '<span style="color:red">Unsaved List</span>';

        list_saved_info = (gData.listSaved != undefined && gData.listSaved != null) ? gData.listSaved : "";
        list_saved_info = (list_saved_info != "") ? '<span>Saved: ' + list_saved_info + '</span>' : '<span>Unsaved List</span>';
        //list_saved_info = (gData.listSaved != undefined && gData.listSaved != null && gData.listSaved != "") ? '<span>List Saved:' + gData.listSaved + '</span>' : '<span style="color:red">Unsaved List</span>';
        var source_id = "";
        $.each($('#dvMailingTypes div[data-role=collapsible]'), function () {
            if (!$(this).hasClass('ui-collapsible-collapsed')) {
                source_id = $(this).attr('id');
                return false;
            }
        });
        if (source_id.toLowerCase().indexOf('acq') > -1) {
            var src_id = source_id;
            src_id = src_id.replace('dv', 'spn') + 'Heading';
            var new_heading = "";
            new_heading = $('#' + src_id).text().substring(0, +($('#' + src_id).text().indexOf(' (') > -1 ? $('#' + src_id).text().indexOf(' (') : $('#' + src_id).text().length)) + ' (' + (([71, 72, 73].indexOf(gOutputData.jobTypeId) > -1) ? 'Resident' : 'Acxiom') + ')';
            $('#' + src_id).text(new_heading);
            $('#' + src_id).css('font-size', '15px');
        }
        $('#btnMapPrefs').removeClass('ui-disabled');
        $('#btnMapPrefs')[0].disabled = false;
        $('#' + source_id).find('span[id^=spnSaveListInfo]').html(list_saved_info);
        window.setTimeout(function () {
            $('#' + source_id).find('span[id^=spnAvailableTotal]').html($('#spnRadiusAvblCount').text().replace(/\B(?=(\d{3})+(?!\d))/g, ",") || $('#divAvailable').text().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#' + source_id).find('span[id^=spnTargetedTotal]').html($('#spnRadiusTargeted').text().replace(/\B(?=(\d{3})+(?!\d))/g, ",") || $('#txtRadiusTargeted').val().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#' + source_id).find('span[id^=spnDnmUSource]').html(62);
        }, 1500);
        $('#' + source_id).find('p[id^=pCounts]').css('display', 'block');
        $('#' + source_id).find('p[id^=pSavedInfo]').css('display', 'block');
        //$('#dvSaveListInfo').html(list_saved_info);
        if (Object.keys(selectedDemographics).length > 0) {
            $('#spnDemoAppliedText').text('(Demographic Filters Applied)');
            $('#spnDemoAppliedText').css('display', 'block');
            $('#btnPieChart').removeClass('ui-disabled');
            $('#btnPieChart')[0].disabled = false;
        }
        $('#map').show();
    }


    //eval(getURLDecode(pagePrefs.scriptBlockDict.getSelectedStoreData));

    if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER) {
        makePageSelect(false)
        $('#pageSelect').trigger('onchange');
    }
    else
        buildZips(selectedStoreId);

    getDataFromSession();
    ////$('#waitPopUp').popup('open', { positionTo: 'window' });
    ////$('#overlay').css('display', 'block');
    calculateTargetedCnt();
    ////$('#overlay').css('display', 'none');

    //$('#waitPopUp').popup('close');
    //}
    //$("#dvLocations").trigger("create");
    //$("#dvLocations").enhanceWithin();
   // generatePieChart();
}

function getDataFromSession() {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) {
        var job_info = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if (job_info.listSelection != undefined && job_info.listSelection != null) {
            var store_info = jQuery.grep(job_info.listSelection, function (obj) {
                return obj.storeId === selectedStoreId;
            });
            if (store_info.length > 0) {
                gSelectedZips = store_info[0];
                buildSelectedZips();
                //calculateTargetedCnt();
            }
        }
    }
}

//Prevents the collapsible set item expansion when clicked on checkbox.
function chkClick(e) {
    e.stopPropagation();
}

function chkSingleCRRT() {
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    selectedCRRTS = [];//$.extend(true, {}, selected_crrts);
    isMapRefreshed = false;
}

function makeEntireZipSelection(is_check_all, ctrl, avlb, also_in_store, selected_zip, status) {
    $('#checkAll' + selected_zip)[0].checked = status;
    $('#checkAll' + selected_zip).attr("checked", status).checkboxradio("refresh");
    $('#chkEntireZip' + selected_zip).attr("checked", status).checkboxradio("refresh");
    $('#hdnSelectedZip').val(selected_zip);
    selectAllZips(is_check_all, ctrl, avlb, also_in_store);
    if (is_check_all)
        $('div.ui-collapsible-content', "#" + $('#hdnSelectedZip').val()).trigger('collapsiblecollapse');
    //selectAllZips(true, $('#checkAll' + selected_zip), 0);
    var ctrls = $('#dvLocations input[type=checkbox]:not([id^=chkEntireZip]):not([id^=checkAll]):not(:checked)');
    $('#chkSelectAllPostalBoundaries').attr('checked', ($(ctrls).length == 0) ? true : false).checkboxradio('refresh');
}

function checkEntireZip(is_check_all, ctrl, avlb, also_in_store, selected_zip) {
    isZipsChanged = true;
    if (ctrl.checked) {
        var ctrls = $('#dvLocations input[type=checkbox]:not([id^=chkEntireZip]):not([id^=checkAll]):not(:checked)');
        if ($(ctrls).length == 0)
            $('#chkSelectAllPostalBoundaries').attr('checked', true).checkboxradio('refresh');
        if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
            var is_allowed = true;
            if (is_check_all) {
                $.each($('input[id^=check_' + selected_zip + '_]'), function (a, b) {
                    if ($(b).attr('selectedType') != "null" && $(b).attr('selectedType') != "" && $(b).attr('selectedType') == "r") {
                        is_allowed = false;
                        return false;
                    }
                });
            }
            else {
                if ($('#' + ctrl.id).attr('selectedType') == "r")
                    is_allowed = false;
            }
            if (!is_allowed) {
                //var msg = "Adding/Removing zips or carrier routes will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to stay with the Radius selection.";
                var msg = "Removing zips/carrier routes or adding zips/carrier routes that intersect with the existing radius will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection.";
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'convertToZipCRRTMode(' + is_check_all + ',\'' + ctrl.id + '\',\'' + avlb + '\',\'' + also_in_store + '\',\'' + selected_zip + '\',true)');
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnSelectedZip\').val(\'\');$(\'#' + ctrl.id + '\').attr("checked",false).checkboxradio("refresh");$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');');
                $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
                $('#popupradiusZipCRRTConfirmDialog').popup('open');
            }
            else {
                makeEntireZipSelection(is_check_all, ctrl.id, avlb, also_in_store, selected_zip, true);
            }
        }
        else {
            makeEntireZipSelection(is_check_all, ctrl.id, avlb, also_in_store, selected_zip, true);
        }

    }
    else {
        $('#chkSelectAllPostalBoundaries').attr('checked', false).checkboxradio('refresh');
        if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
            var is_allowed = true;
            if (is_check_all) {//.attr('selectedType');
                $.each($('input[id^=check_' + selected_zip + '_]'), function (a, b) {
                    if ($(this).attr('selectedType') != "null" && $(this).attr('selectedType') != "" && $(this).attr('selectedType') == "r") {
                        is_allowed = false;
                        return false;
                    }
                });
            }
            else {
                if ($('#' + ctrl.id).attr('selectedType') == "r")
                    is_allowed = false;
            }
            if (!is_allowed) {
                //var msg = "Adding/Removing zips or carrier routes will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to stay with the Radius selection.";
                var msg = "Removing zips/carrier routes or adding zips/carrier routes that intersect with the existing radius will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection.";
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'convertToZipCRRTMode(' + is_check_all + ',\'' + ctrl.id + '\',\'' + avlb + '\',\'' + also_in_store + '\',\'' + selected_zip + '\',false)');
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnSelectedZip\').val(\'\');$(\'#' + ctrl.id + '\').attr("checked",true).checkboxradio("refresh");$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');');
                $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
                $('#popupradiusZipCRRTConfirmDialog').popup('open');
            }
            else {
                makeEntireZipSelection(is_check_all, ctrl.id, avlb, also_in_store, selected_zip, false);
            }
        }
        else {
            makeEntireZipSelection(is_check_all, ctrl.id, avlb, also_in_store, selected_zip, false);
        }
    }
}

if (typeof String.prototype.startsWith != 'function') {
    // see below for better implementation!
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) == 0;
    };
}

function convertToZipCRRTMode(is_check_all, ctrl, avlb, also_in_store, selected_zip, status) {
    $('#popupradiusZipCRRTConfirmDialog').popup('open');
    $('#popupradiusZipCRRTConfirmDialog').popup('close');
    if ($('#dvPostalBoundarySelections').css('display') == 'block') {
        $('#dvPostalBoundarySelections').collapsible("collapse");
        $('#dvSectionMethod').collapsible("expand");
    }
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    $('#rdoPolygon').removeAttr('data-selectedType');
    $('#rdoTargetCounts').removeAttr('data-selectedType');
    $('#rdoRadius').removeAttr('data-selectedType');
    $('#rdoRadius').attr('checked', false).checkboxradio('refresh');
    $('#rdoPolygon').attr('checked', false).checkboxradio('refresh');
    $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh');
    window.setTimeout(function getDelay3() {
        makeEntireZipSelection(is_check_all, ctrl, avlb, also_in_store, selected_zip, status);
        $('#dvSelectByRadius').collapsible('collapse');
        if (gData != null && gData.zips != undefined) {
            var r_crrts = [];
            var total_targets_selected = 0;
            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
                var zip = key.zipCode;
                var index = 0;
                var targeted_counts = 0;
                $.each(key.crrts, function (i, j) {
                    //if (j.selectType == 'r') {
                    j.isChecked = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 1 : 0;
                    j.selectType = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 'f' : '';
                    $('#' + 'check_' + zip + '_' + index).attr('selectedType', j.selectType);
                    j.targeted = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? j.available : 0;
                    $('#spn_' + zip + '_' + index).text(((j.isChecked) ? j.available : 0));
                    targeted_counts = parseInt(targeted_counts) + parseInt(j.targeted);
                    //}
                    index++;
                });
                $('#spnCheckAll' + zip).text(targeted_counts);
                $('#spnTargetedZipCnt' + zip).text(targeted_counts);
                key.targeted = targeted_counts;
                total_targets_selected = parseInt(total_targets_selected) + parseInt(targeted_counts);
            });
            $('#spnTargeted').text(total_targets_selected.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
        sessionStorage.removeItem('updatedCRRTs');
        sessionStorage.removeItem('storeListSelection');
        gSelectedZips = [];
        $('#dvLocations').empty();
        buildZips(selectedStoreId);
        var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
        if ((Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0) && temp_store_list_selection.polygonInfo != undefined && temp_store_list_selection.polygonInfo != null && temp_store_list_selection.polygonInfo != "") {
            delete temp_store_list_selection.polygonInfo;
            sessionStorage.storeListSelection = JSON.stringify(temp_store_list_selection);
        }
        if (gData.polygonInfo != undefined && gData.polygonInfo != null && gData.polygonInfo != "") delete gData.polygonInfo;
        if (gSelectedZips.polygonInfo != undefined && gSelectedZips.polygonInfo != null && gSelectedZips.polygonInfo != "") {
            delete gData.polygonInfo;
        }
        $('#btnPolygon').addClass('ui-disabled');
        if (drawingManager != undefined && drawingManager != null)
            drawingManager.setDrawingMode(null);
        calculateTargetedCnt();
        $('#hdnMode').val('rtoc');
        $('#dvSelectByZipCrrt').collapsible('expand');
        $('#waitPopUp').popup('close');
        //$("#dvLocations").enhanceWithin();
    }, 1000);
}

//to display the "All zips" for the selected store location.
function buildZips(selectedStoreId) {
    var li_zips = "";
    var li_all_routes = "";
    var li_chk_routes = "";
    var index = 0;
    var allzip = 0;
    var i = 0;
    var is_all_checkbox_selected = true;
    var totalAvailable = 0;
    var totalTargeted = 0;
    var target_selected = 0;
    var data_theme = 'e';

    //if (jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER)
    //    data_theme = 'i'; //green theme
    var radius_target_selected = 0;
    if (selectedStoreId != undefined) {
        li_zips = '<div data-theme="c" data-role="collapsible-set"  data-inset="false" data-theme="' + data_theme + '" style="padding-left: 10px" data-collapsed-icon="carat-r" data-expanded-icon="carat-d">';
        $('#dvLocations').empty();
        if (gData != null) {
            gInnerData = gData;
            var li_zips_data = "";
            $.each(gInnerData.zips, function (zip_key, zip_val) {
                if (gInnerData.zips[index] != undefined) {
                    var city_name = zip_val.zipName;
                    var zipCode = zip_val.zipCode;
                    if (zip_val.available != null && zip_val.available > 0) {
                        var avail = zip_val.available;
                        //                        var targeted = zip_val.targeted;
                        //                        totalTargeted = eval(parseInt(totalTargeted) + parseInt(targeted));
                        totalAvailable = eval(parseInt(totalAvailable) + parseInt(avail));
                    }
                    if (zip_val.alsoInStore == null)
                        li_zips = '<div data-theme="' + data_theme + '" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:100%"><div class="ui-grid-a ui-responsive"><div class="ui-block-a" style="width:75%">' + zipCode + ' - ' + city_name + '';
                    else
                        li_zips = '<div data-theme="c" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:100%"><div class="ui-grid-a ui-responsive"><div class="ui-block-a" style="width:75%">' + zipCode + ' - ' + city_name + '';
                    li_all_routes = '<div class="ui-field-contain" data-inset="false" data-theme="c"><fieldset id="fieldset" data-role="controlgroup" data-mini="true" data-inset="false" data-theme="c">';
                    var selected_routes_count = 0;
                    var len = 0;
                    var is_Crrt_Starts_With_B = false;
                    if (zip_val.crrts != null || zip_val.crrts.length > 0) {
                        li_chk_routes = "";
                        i = 0;
                        len = zip_val.crrts.length;
                        var zip_targeted_cnt = 0;
                        target_selected = 0;
                        var zip_avbl_cnt = 0;
                        $.each(zip_val.crrts, function (key, val) {
                            allzip += eval(parseInt(val.available));

                            var is_checked = '';
                            zip_targeted_cnt += parseInt(val.targeted);
                            target_selected += parseInt(val.targeted);
                            if (val.selectType == 'r')
                                radius_target_selected += parseInt(val.targeted);
                            zip_avbl_cnt += parseInt(val.available);
                            if (val.isChecked == 1) {
                                selected_routes_count++;
                                //zip_targeted_cnt += parseInt(val.available);
                                //target_selected += parseInt(val.available);
                                is_checked = ' checked="checked" ';
                            } else {
                                is_checked = '';
                            }
                            if (is_checked == '')
                                is_all_checkbox_selected = false;
                            if (!is_Crrt_Starts_With_B)
                                is_Crrt_Starts_With_B = (val.crrtName.toUpperCase().startsWith('B'));
                            //li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="selectAllZips(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\')" targeted ="' + val.targeted + '" value="' + val.available + '" "' + is_checked + '"/>';
                            li_chk_routes += '<input data-theme="c" type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="checkEntireZip(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" selectedType="' + val.selectType + '" targeted ="' + val.targeted + '" value="' + val.available + '" ' + is_checked + '/>';

                            li_chk_routes += '<label for="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();"><span style="font-weight:normal;font-size:12px;color:#369">' + val.crrtName + ' | <span  id="spn_' + zipCode + '_' + i + '">' + val.targeted + '</span> of ' + val.available + '</span></label>';
                            i = i + 1;
                        });
                        totalTargeted += target_selected;
                    }
                    //if (selected_routes_count == len)
                    if (zip_avbl_cnt == target_selected)
                        //li_all_routes += '<input type="checkbox" name="checkAll" onchange="selectAllZips(true,this,0, \'' + zip_val.alsoInStore + '\')" id="checkAll' + zipCode + '" checked/>';
                        li_all_routes += '<input data-theme="c" type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '" checked />';
                    else
                        li_all_routes += '<input data-theme="c" type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '"/>';
                    //li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | Avlb: ' + zip_val.available + '</span></label>';
                    li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | <span id="spnCheckAll' + zipCode + '">' + target_selected + '</span> of <span id="spnCheckAllAvbl' + zipCode + '">' + zip_val.available + '</span> targeted</span></label>';
                    $('#divAvailable').html(zip_val.available.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $('#spnRadiusAvblCount').html(zip_val.available.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                    if (is_Crrt_Starts_With_B && (jobCustomerNumber === CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER)) {
                        li_chk_routes += '<div style="font-size:12px;padding-top:8px">Carrier Routes starting with "B" are P.O. Boxes and will not display on the map.</div>';
                        is_Crrt_Starts_With_B = false;
                    }

                    li_all_routes += li_chk_routes + '</fieldset></div>';
                    var checked_status = '';
                    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                        if (selected_routes_count > 0)
                            checked_status = 'checked';
                    }
                    else {
                        if (selected_routes_count == len) {
                            checked_status = 'checked';
                        }
                    }
                    var entire_zip_option = "";
                    if (checked_status != "") {
                        if (zip_val.alsoInStore == null)
                            //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true" checked/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                            entire_zip_option += '<div class="ui-block-b" style="width:25%"><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" data-theme="' + data_theme + '" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;">&nbsp;</label><input class="my-list" type="checkbox" data-theme="' + data_theme + '" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-mini="true" ' + checked_status + ' style="left: 0.28em;top: 43%;" /></div>';
                        else
                            //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true" checked/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                            entire_zip_option += '<div class="ui-block-b" style="width:25%"><label data-theme="' + data_theme + '" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;">&nbsp;</label><input class="my-list" type="checkbox" data-theme="' + data_theme + '"  name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-mini="true" ' + checked_status + ' style="left: 0.28em;top: 43%;" /></div>';
                    }

                    if (checked_status == "") {
                        if (zip_val.alsoInStore == null)
                            //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                            entire_zip_option += '<div class="ui-block-b" style="width:25%"><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" data-theme="' + data_theme + '" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;">&nbsp;</label><input class="my-list" type="checkbox" data-theme="' + data_theme + '"  name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-mini="true" style="left: 0.28em;top: 43%;" /></div>';
                        else
                            //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
                            entire_zip_option += '<div class="ui-block-b" style="width:25%"><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" data-theme="' + data_theme + '" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;">&nbsp;</label><input class="my-list" type="checkbox" data-theme="' + data_theme + '"  name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-mini="true" style="left: 0.28em;top: 43%;" /></div>';
                    }

                    li_zips += '<br/><span style="font-weight:normal;font-size:12px;">Available: ' + zip_val.available + ' | Targeted: <span style="font-weight:normal;font-size:12px;" id="spnTargetedZipCnt' + zipCode + '">' + zip_targeted_cnt + '</span></span><br/>';

                    var zip_status = "";
                    if (selected_routes_count == len && target_selected == zip_val.available)
                        zip_status = "Entire Zip Selected"
                    else if (selected_routes_count == len && target_selected < zip_val.available)
                        zip_status = ""
                    else if (selected_routes_count < len)
                        zip_status = selected_routes_count + ' of ' + len + ' Carrier Routes Selected';

                    if (selected_routes_count == len) {
                        if (zip_val.alsoInStore == null)
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span>';
                        else
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span>';
                    }
                    else {
                        if (zip_val.alsoInStore == null)
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span>';
                        else
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span>';
                    }
                    li_zips += '</div>' + entire_zip_option + '</div></h3>';
                    li_zips += li_all_routes;
                    li_zips += '</div>';
                    if (is_all_checkbox_selected)
                        $('#checkAll').attr("checked", true);
                    index++;
                    li_zips_data += li_zips;
                }
            });
            $('#dvLocations').append(li_zips_data);
            //$("#dvLocations").trigger("create");
            $($('div[class=ui-checkbox]').find('label[for^=chkEntireZip]')).each(function (i) {
                $(this).find('span').css('padding-left', '1px');
            });
        }
        $('#divAvailable').html(totalAvailable.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#spnRadiusAvblCount').html(totalAvailable.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        //$('#txtRadiusTargeted').val(totalTargeted.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        window.setTimeout(function () {
            if ($('#rdoTargetCounts').is(':checked') || $('#rdoRadius').is(':checked') || $('#rdoPolygon').is(':checked')) {
                $('#txtRadiusTargeted').val(radius_target_selected.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $('#spnRadiusTargeted').html(radius_target_selected.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            }
            else {
                $('#txtRadiusTargeted').val(gData.targetedAsk.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $('#spnRadiusTargeted').html(gData.targetedAsk.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            }
        }, 1000);
        var ctrls = $('#dvLocations input[type=checkbox]:not([id^=chkEntireZip]):not([id^=checkAll]):not(:checked)');
        $('#chkSelectAllPostalBoundaries').attr('checked', ($(ctrls).length == 0 && totalAvailable == totalTargeted) ? true : false).checkboxradio('refresh');
		
        //$('#spnRadiusTargeted').html(totalTargeted.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

        //if (jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER) {
        //    var temp_items = $(li_zips_data);
        //    $.each(temp_items, function (a, b) {
        //        $(b).attr('data-theme', 'a');
        //        $(b).find('input').attr('data-theme', 'a');
        //        $(b).find('label').attr('data-theme', 'a');
        //    });
        //    //var temp_data = '<div id="dvTemp">' + li_zips_data + '</div>';
        //    //$(temp_data).find('div[data-role=collapsible]').removeAttr('data-theme').attr('data-theme','j');
        //    $('#dvEmailSelectionsLocations').append(temp_items);
        //    $("#dvEmailSelectionsLocations").trigger("create");

        //    var temp_items_uploaded = $(li_zips_data);
        //    $.each(temp_items_uploaded, function (a, b) {
        //        $(b).attr('data-theme', 'e');
        //        $(b).find('input').attr('data-theme', 'e');
        //        $(b).find('label').attr('data-theme', 'e');
        //    });
        //    $('#dvUploadedTargetsLocations').append(temp_items_uploaded);
        //    $("#dvUploadedTargetsLocations").trigger("create");
        //    //buildEmailZips();
        //    //buildUploadZips();
        //}
    }
    else {
        $('#hdnSelectedStore').val('');
    }
    if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isSelectionMethodHintsDisplayed == undefined || sessionStorage.isSelectionMethodHintsDisplayed == null || sessionStorage.isSelectionMethodHintsDisplayed == "" || sessionStorage.isSelectionMethodHintsDisplayed == "false") && (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER)) {
        sessionStorage.isSelectionMethodHintsDisplayed = true;
        $('#selectionMethod').popup('open', { positionTo: '#map' });
    }   
}

function buildEmailZips() {
    var li_zips = "";
    var li_all_routes = "";
    var li_chk_routes = "";
    var index = 0;
    var allzip = 0;
    var i = 0;
    var is_all_checkbox_selected = true;
    var totalAvailable = 0;
    var totalTargeted = 0;
    var target_selected = 0;
    li_zips = '<div data-role="collapsible-set"  data-inset="false" data-theme="j" style="padding-left: 10px" data-collapsed-icon="carat-r" data-expanded-icon="carat-d">';
    $('#dvEmailSelectionsLocations').empty();
    if (gData != null) {
        gInnerData = gData;
        $.each(gInnerData.zips, function (zip_key, zip_val) {
            if (gInnerData.zips[index] != undefined) {
                var city_name = zip_val.zipName;
                var zipCode = zip_val.zipCode;
                if (zip_val.available != null && zip_val.available > 0) {
                    var avail = zip_val.available;
                    //                        var targeted = zip_val.targeted;
                    //                        totalTargeted = eval(parseInt(totalTargeted) + parseInt(targeted));
                    totalAvailable = eval(parseInt(totalAvailable) + parseInt(avail));
                }
                if (zip_val.alsoInStore == null)
                    li_zips = '<div data-theme="j" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:105%">' + zipCode + ' - ' + city_name + '';
                else
                    li_zips = '<div data-theme="e" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:105%">' + zipCode + ' - ' + city_name + '';
                li_all_routes = '<div data-role="fieldcontain" data-inset="false"><fieldset id="fieldset" data-role="controlgroup" data-mini="true" data-inset="false">';
                var selected_routes_count = 0;
                var len = 0;
                var is_Crrt_Starts_With_B = false;
                if (zip_val.crrts != null || zip_val.crrts.length > 0) {
                    li_chk_routes = "";
                    i = 0;
                    len = zip_val.crrts.length;
                    var zip_targeted_cnt = 0;
                    target_selected = 0;
                    var zip_avbl_cnt = 0;
                    $.each(zip_val.crrts, function (key, val) {
                        allzip += eval(parseInt(val.available));

                        var is_checked = '';
                        zip_targeted_cnt += parseInt(val.targeted);
                        target_selected += parseInt(val.targeted);
                        zip_avbl_cnt += parseInt(val.available);
                        if (val.isChecked == 1) {
                            selected_routes_count++;
                            //zip_targeted_cnt += parseInt(val.available);
                            //target_selected += parseInt(val.available);
                            is_checked = ' checked="checked" ';
                        } else {
                            is_checked = '';
                        }
                        if (is_checked == '')
                            is_all_checkbox_selected = false;
                        if (!is_Crrt_Starts_With_B)
                            is_Crrt_Starts_With_B = (val.crrtName.toUpperCase().startsWith('B'));
                        //li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="selectAllZips(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\')" targeted ="' + val.targeted + '" value="' + val.available + '" "' + is_checked + '"/>';
                        li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="checkEntireZip(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" selectedType="' + val.selectType + '" targeted ="' + val.targeted + '" value="' + val.available + '" ' + is_checked + '/>';

                        li_chk_routes += '<label for="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();"><span style="font-weight:normal;font-size:12px;color:#369">' + val.crrtName + ' | <span  id="spn_' + zipCode + '_' + i + '">' + val.targeted + '</span> of ' + val.available + '</span></label>';
                        i = i + 1;
                    });
                    totalTargeted += target_selected;
                }
                //if (selected_routes_count == len)
                if (zip_avbl_cnt == target_selected)
                    //li_all_routes += '<input type="checkbox" name="checkAll" onchange="selectAllZips(true,this,0, \'' + zip_val.alsoInStore + '\')" id="checkAll' + zipCode + '" checked/>';
                    li_all_routes += '<input type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '" checked />';
                else
                    li_all_routes += '<input type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '"/>';
                //li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | Avlb: ' + zip_val.available + '</span></label>';
                li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | <span id="spnCheckAll' + zipCode + '">' + target_selected + '</span> of <span id="spnCheckAllAvbl' + zipCode + '">' + zip_val.available + '</span> targeted</span></label>';
                $('#divAvailable').html(zip_val.available.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $('#spnRadiusAvblCount').html(zip_val.available.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                if (is_Crrt_Starts_With_B && (jobCustomerNumber === CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER)) {
                    li_chk_routes += '<div style="font-size:12px;padding-top:8px">Carrier Routes starting with "B" are P.O. Boxes and will not display on the map.</div>';
                    is_Crrt_Starts_With_B = false;
                }

                li_all_routes += li_chk_routes + '</fieldset></div>';
                var checked_status = '';
                if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                    if (selected_routes_count > 0)
                        checked_status = 'checked';
                }
                else {
                    if (selected_routes_count == len) {
                        checked_status = 'checked';
                    }
                }
                if (checked_status != "") {
                    if (zip_val.alsoInStore == null)
                        //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true" checked/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                        li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="j" data-mini="true" ' + checked_status + '/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                    else
                        //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true" checked/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                        li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="e" data-mini="true" ' + checked_status + '/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                }

                if (checked_status == "") {
                    if (zip_val.alsoInStore == null)
                        //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                        li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="j" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                    else
                        //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
                        li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
                }

                li_zips += '<br/><span style="font-weight:normal;font-size:12px;">Available: ' + zip_val.available + ' | Targeted: <span style="font-weight:normal;font-size:12px;" id="spnTargetedZipCnt' + zipCode + '">' + zip_targeted_cnt + '</span></span><br/>';

                var zip_status = "";
                if (selected_routes_count == len && target_selected == zip_val.available)
                    zip_status = "Entire Zip Selected"
                else if (selected_routes_count == len && target_selected < zip_val.available)
                    zip_status = ""
                else if (selected_routes_count < len)
                    zip_status = selected_routes_count + ' of ' + len + ' Carrier Routes Selected';

                if (selected_routes_count == len) {
                    if (zip_val.alsoInStore == null)
                        li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span></h3>';
                    else
                        li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span></h3>';
                }
                else {
                    if (zip_val.alsoInStore == null)
                        li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span></h3>';
                    else
                        li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span></h3>';
                }
                li_zips += li_all_routes;
                li_zips += '</div>';
                if (is_all_checkbox_selected)
                    $('#checkAll').attr("checked", true);
                index++;
                $('#dvEmailSelectionsLocations').append(li_zips);


            }
        });
        $("#dvEmailSelectionsLocations").trigger("create");
        $($('div[class=ui-checkbox]').find('label[for^=chkEntireZip]')).each(function (i) {
            $(this).find('span').css('padding-left', '1px');
        });
    }

}

function buildUploadZips() {
    var li_zips = "";
    var li_all_routes = "";
    var li_chk_routes = "";
    var index = 0;
    var allzip = 0;
    var i = 0;
    var is_all_checkbox_selected = true;
    var totalAvailable = 0;
    var totalTargeted = 0;
    var target_selected = 0;
    li_zips = '<div data-role="collapsible-set"  data-inset="false" data-theme="b" style="padding-left: 10px" data-collapsed-icon="carat-r" data-expanded-icon="carat-d">';
    $('#dvUploadedTargetsLocations').empty();
    if (gData != null) {
        gInnerData = gData;
        $.each(gInnerData.zips, function (zip_key, zip_val) {
            if (gInnerData.zips[index] != undefined) {
                var city_name = zip_val.zipName;
                var zipCode = zip_val.zipCode;
                if (zip_val.available != null && zip_val.available > 0) {
                    var avail = zip_val.available;
                    //                        var targeted = zip_val.targeted;
                    //                        totalTargeted = eval(parseInt(totalTargeted) + parseInt(targeted));
                    totalAvailable = eval(parseInt(totalAvailable) + parseInt(avail));
                }
                if (zip_val.alsoInStore == null)
                    li_zips = '<div data-theme="b" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:105%">' + zipCode + ' - ' + city_name + '';
                else
                    li_zips = '<div data-theme="e" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:105%">' + zipCode + ' - ' + city_name + '';
                li_all_routes = '<div data-role="fieldcontain" data-inset="false"><fieldset id="fieldset" data-role="controlgroup" data-mini="true" data-inset="false">';
                var selected_routes_count = 0;
                var len = 0;
                var is_Crrt_Starts_With_B = false;
                if (zip_val.crrts != null || zip_val.crrts.length > 0) {
                    li_chk_routes = "";
                    i = 0;
                    len = zip_val.crrts.length;
                    var zip_targeted_cnt = 0;
                    target_selected = 0;
                    var zip_avbl_cnt = 0;
                    $.each(zip_val.crrts, function (key, val) {
                        allzip += eval(parseInt(val.available));

                        var is_checked = '';
                        zip_targeted_cnt += parseInt(val.targeted);
                        target_selected += parseInt(val.targeted);
                        zip_avbl_cnt += parseInt(val.available);
                        if (val.isChecked == 1) {
                            selected_routes_count++;
                            //zip_targeted_cnt += parseInt(val.available);
                            //target_selected += parseInt(val.available);
                            is_checked = ' checked="checked" ';
                        } else {
                            is_checked = '';
                        }
                        if (is_checked == '')
                            is_all_checkbox_selected = false;
                        if (!is_Crrt_Starts_With_B)
                            is_Crrt_Starts_With_B = (val.crrtName.toUpperCase().startsWith('B'));
                        //li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="selectAllZips(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\')" targeted ="' + val.targeted + '" value="' + val.available + '" "' + is_checked + '"/>';
                        li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="checkEntireZip(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" selectedType="' + val.selectType + '" targeted ="' + val.targeted + '" value="' + val.available + '" ' + is_checked + '/>';

                        li_chk_routes += '<label for="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();"><span style="font-weight:normal;font-size:12px;color:#369">' + val.crrtName + ' | <span  id="spn_' + zipCode + '_' + i + '">' + val.targeted + '</span> of ' + val.available + '</span></label>';
                        i = i + 1;
                    });
                    totalTargeted += target_selected;
                }
                //if (selected_routes_count == len)
                if (zip_avbl_cnt == target_selected)
                    //li_all_routes += '<input type="checkbox" name="checkAll" onchange="selectAllZips(true,this,0, \'' + zip_val.alsoInStore + '\')" id="checkAll' + zipCode + '" checked/>';
                    li_all_routes += '<input type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '" checked />';
                else
                    li_all_routes += '<input type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '"/>';
                //li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | Avlb: ' + zip_val.available + '</span></label>';
                li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | <span id="spnCheckAll' + zipCode + '">' + target_selected + '</span> of <span id="spnCheckAllAvbl' + zipCode + '">' + zip_val.available + '</span> targeted</span></label>';
                $('#divAvailable').html(zip_val.available.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $('#spnRadiusAvblCount').html(zip_val.available.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                if (is_Crrt_Starts_With_B && (jobCustomerNumber === CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER)) {
                    li_chk_routes += '<div style="font-size:12px;padding-top:8px">Carrier Routes starting with "B" are P.O. Boxes and will not display on the map.</div>';
                    is_Crrt_Starts_With_B = false;
                }

                li_all_routes += li_chk_routes + '</fieldset></div>';
                var checked_status = '';
                if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                    if (selected_routes_count > 0)
                        checked_status = 'checked';
                }
                else {
                    if (selected_routes_count == len) {
                        checked_status = 'checked';
                    }
                }
                if (checked_status != "") {
                    if (zip_val.alsoInStore == null)
                        //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true" checked/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                        li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="b" data-mini="true" ' + checked_status + '/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                    else
                        //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true" checked/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                        li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="e" data-mini="true" ' + checked_status + '/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                }

                if (checked_status == "") {
                    if (zip_val.alsoInStore == null)
                        //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                        li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="b" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                    else
                        //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
                        li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
                }

                li_zips += '<br/><span style="font-weight:normal;font-size:12px;">Available: ' + zip_val.available + ' | Targeted: <span style="font-weight:normal;font-size:12px;" id="spnTargetedZipCnt' + zipCode + '">' + zip_targeted_cnt + '</span></span><br/>';

                var zip_status = "";
                if (selected_routes_count == len && target_selected == zip_val.available)
                    zip_status = "Entire Zip Selected"
                else if (selected_routes_count == len && target_selected < zip_val.available)
                    zip_status = ""
                else if (selected_routes_count < len)
                    zip_status = selected_routes_count + ' of ' + len + ' Carrier Routes Selected';

                if (selected_routes_count == len) {
                    if (zip_val.alsoInStore == null)
                        li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span></h3>';
                    else
                        li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span></h3>';
                }
                else {
                    if (zip_val.alsoInStore == null)
                        li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span></h3>';
                    else
                        li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span></h3>';
                }
                li_zips += li_all_routes;
                li_zips += '</div>';
                if (is_all_checkbox_selected)
                    $('#checkAll').attr("checked", true);
                index++;
                $('#dvUploadedTargetsLocations').append(li_zips);


            }
        });
        $("#dvUploadedTargetsLocations").trigger("create");
        $($('div[class=ui-checkbox]').find('label[for^=chkEntireZip]')).each(function (i) {
            $(this).find('span').css('padding-left', '1px');
        });
    }

}


function getZipInfo(zip_code) {
    $('#hdnSelectedZip').val(zip_code);
}

function getDifferences(oldObj, newObj) {
    var diff = {};

    for (var k in oldObj) {
        if (!(k in newObj))
            diff[k] = undefined;  // property gone so explicitly set it undefined
        else if (oldObj[k] !== newObj[k])
            //else if (!isEqual(oldObj[k], newObj[k]))
            diff[k] = newObj[k];  // property in both but has changed
    }

    for (k in newObj) {
        if (!(k in oldObj))
            diff[k] = newObj[k]; // property is new
    }

    return diff;
}

//when clicked on "Save Selections" button..
function saveSelections(event) {
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        diff = getDifferences(selectedCRRTS, selected_crrts);

    //if (diff.length == undefined || diff.length == 0) {
    if (isMapRefreshed) {
        //Get the complete jon info.
        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        }
        var target_cnt = 0;
        if (gOutputData.minimumQuantityType == "store") {

            if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                if ($('#trRadiusSelectBy').is(':visible')) {
                    if ($('#rdoTargetCounts').is(':checked'))
                        target_cnt = $('#txtRadiusTargeted').val();
                    else if ($('#rdoRadius').is(':checked'))
                        target_cnt = $('#spnRadiusTargeted').text();
                }
                else if (!$('#trRadiusSelectBy').is(':visible'))
                    target_cnt = $('#txtRadiusTargeted').val();
            }
            else {
                target_cnt = $('#spnTargeted').text();
            }

            if (target_cnt < gOutputData.minimumQuantity) {
                var msg = "The Targeted Quantity you have selected is below the required minimum of " + gOutputData.minimumQuantity + " pieces. Please adjust your selection to meet this requirement.";
                $('#alertmsg').html(msg);
                $('#popupDialog').popup('open');
                return false;
            }
        }
        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
            if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                if ($('#trRadiusSelectBy').is(':visible')) {
                    if ($('#rdoTargetCounts').is(':checked'))
                        target_cnt = $('#txtRadiusTargeted').val();
                    else if ($('#rdoRadius').is(':checked') || $('#rdoPolygon').is(':checked'))
                        target_cnt = $('#spnRadiusTargeted').text();
                }
                else if (!$('#trRadiusSelectBy').is(':visible'))
                    target_cnt = $('#txtRadiusTargeted').val();
            }
            else {
                target_cnt = $('#spnTargeted').text();
            }
            var target_cnt = (typeof (target_cnt) == "string" && target_cnt.indexOf(',') > -1) ? target_cnt.replace(/,/g, '') : target_cnt;
            var available_count = parseInt($('#spnRadiusAvblCount').text().replace(/,/g, ""));

            var alert_msg = '';
            if (available_count < parseInt(target_cnt))
                alert_msg = "The current selections cannot be saved because the targeted quantity cannot be met using the available quantity found. The available quantity may be increased by removing or widening your demographic selections.";
            else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && gOutputData.jobTypeId == 73 && parseInt(target_cnt) < 500)
                alert_msg = "The required minimum value per location is 500 targets. Please adjust your targeted count to meet this requirement.";
            else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && gOutputData.jobTypeId != 73 && parseInt(target_cnt) < 50)
                alert_msg = "The required minimum value per location is 50 targets. Please adjust your targeted count to meet this requirement.";
            else if (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && parseInt(target_cnt) < 3000)
                alert_msg = "The required minimum value per location is 3,000 targets. Please adjust your targeted count to meet this requirement.";

            if (alert_msg.length > 0) {
                $('#popupStoreDialog').popup('close');
                $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
                $('#alertmsg').html(alert_msg);
                window.setTimeout(function () {
                    $('#popupDialog').popup('open');
                }, 300);

                $('#btnRefresh').addClass('ui-disabled');
                $('#btnRefresh')[0].disabled = true;
                return false;
            }
        }

        //get the selected zips info for the selected store.
        if (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null) {
            gSelectedZips = jQuery.parseJSON(sessionStorage.storeListSelection);
        }

        if (gSelectedZips != undefined && gSelectedZips != null) {
            if (gOutputData.listSelection != undefined && gOutputData.listSelection != "") {
                var zip_index = 0;
                var selected_zip = jQuery.grep(gOutputData.listSelection, function (key, obj) {
                    zip_index = obj;
                    return key.storeId === selectedStoreId;
                });
                if (selected_zip != undefined && selected_zip.length == 0) {
                    selected_zip = gSelectedZips;
                    gOutputData.listSelection.push(gSelectedZips);
                }
                else {
                    selected_zip = gSelectedZips;
                    gOutputData.listSelection[zip_index] = selected_zip;
                }
            } else {
                gOutputData["listSelection"] = [];
                gOutputData.listSelection.push(gSelectedZips);
            }

            if (gOutputData != undefined && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
                $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                    if (selectedStoreId == val.pk) {
                        val.needsCount = false;
                        return false;
                    }
                });
            }
        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        sessionStorage.removeItem('storeListSelection');
        $('#hdnSelectedStore').val('');
        var post_json_zips = (selected_zip != undefined) ? selected_zip : gSelectedZips;
        var temp_selected_zips = $.extend(true, {}, post_json_zips);

        if ($('#rdoPolygon').is(':checked')) {
            temp_selected_zips.radius = 0;
            temp_selected_zips.targetedAsk = 0;
        }
        else if ($('#rdoTargetCounts').is(':checked')) {
            temp_selected_zips.radius = 0;
            if (temp_selected_zips.polygonInfo != undefined || temp_selected_zips.polygonInfo != null) delete temp_selected_zips.polygonInfo;
            //temp_selected_zips.targetedAsk = temp_selected_zips.targetedQty;
        }
        else if ($('#rdoRadius').is(':checked')) {
            temp_selected_zips.targetedAsk = 0;
            if (temp_selected_zips.polygonInfo != undefined || temp_selected_zips.polygonInfo != null) delete temp_selected_zips.polygonInfo;
        }
        if ($('#trRadiusSelectBy').is(':visible')) {
            if ($('#rdoTargetCounts').is(':checked')) {
                var tmp_cnt = (typeof ($('#txtRadiusTargeted').val()) == "string" && $('#txtRadiusTargeted').val().indexOf(',') > -1) ? $('#txtRadiusTargeted').val().replace(',', '') : $('#txtRadiusTargeted').val();
                temp_selected_zips.targetedAsk = tmp_cnt;
            }
        }

        temp_selected_zips.demos = makeSelectedDemosObject();
        temp_selected_zips["jobTypeId"] = gOutputData.jobTypeId;
        //if (temp_selected_zips.demos != undefined)
        //  delete temp_selected_zips.demos;
        //postCORS(myProtocol + "maps.tribunedirect.com/api/ZipSelectList/1/" + (jobCustomerNumber == CW_CUSTOMER_NUMBER) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId + "/store_select", JSON.stringify(post_json_zips), function (response) {
        //postCORS(myProtocol + "maps.tribunedirect.com/api/ZipSelectList/" + jobCustomerNumber + "/1/" + ((jobCustomerNumber == CW_CUSTOMER_NUMBER) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId), JSON.stringify(post_json_zips), function (response) {
        //postCORS(mapsUrl + "api/ZipSelectList_save/" + jobCustomerNumber + "/1/" + ((jobCustomerNumber == CW_CUSTOMER_NUMBER) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId), JSON.stringify(temp_selected_zips), function (response) {
        var save_selections_url = mapsUrl + "api/ZipSelectList_save/" + jobCustomerNumber + "/" + facilityId + "/" + jobNumber + "/" + selectedStoreId;
        if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER)
            save_selections_url = mapsUrl + "api/ZipSelectList_save/" + jobCustomerNumber + "/" + facilityId + "/" + jobNumber + '/' + selectedStoreId + "/" + pageObj.currentInHome().dateValue.replace(/\//g, '_');
        postCORS(save_selections_url, JSON.stringify(temp_selected_zips), function (response) {
            var msg = (response.toLowerCase() == "\"success\"") ? 'Your selections have been saved for <b>' + $('#ddlStoreLocations option[value=' + gSelectedZips.storePk + ']').text().replace(/\*/g, '') + '.</b>' : 'Your selections have not been saved.';
            $('#alertmsg').html(msg);
            $('#popupDialog').popup('open');

            $('#ddlStoreLocations option[value=' + gSelectedZips.storePk + ']').css('background-color', 'lightblue');
            $('#ddlStoreLocations option[value=' + gSelectedZips.storePk + ']').css('font-weight', 'bolder');
            $('#ddlStoreLocations option[value=' + gSelectedZips.storePk + ']').css('font-style', 'italic');
            $('#ddlStoreLocations option[value=' + gSelectedZips.storePk + ']').text($('#ddlStoreLocations option[value=' + gSelectedZips.storePk + ']').text().replace(/\*/g, '') + '*');
            var saved_stores = (sessionStorage.savedStores != undefined && sessionStorage.savedStores != null && sessionStorage.savedStores != "") ? $.parseJSON(sessionStorage.savedStores) : [];
            saved_stores.push(selectedStoreId);
            sessionStorage.savedStores = JSON.stringify(saved_stores);
            gSelectedZips = [], selectedCRRTS = [], prev_demos = [];
            totalAvailable = 0;
            selectedDemographics = {};
            isDemosChanged = false, isStoreChanged = false, isTargetChanged = false, isRadiusChanged = false, isEnterPress = false, isDemosDefaulted = false, isMapRefreshed = false;
            map = null, gCrrtsA = null, ctaLayer = null, infowindow = null, service = null, mapCenter = null, myCircle = null;
            selectedStoreId = "", prevSelectedStoreId = "";
            gCrrtMarkersA = [], defaultMapZoom = 13, isMapMove = false;
            sessionStorage.removeItem('storeListSelection');
            resetSelections();
            /* RESET Demographics START*/
            demo_groups = {};

            $.each(mappingDemographics, function (key, val) {
                temp_key = key;
                if (!demo_groups[val.columnDescription]) {
                    demo_groups[val.columnDescription] = {};
                    demo_groups[val.columnDescription]["menuType"] = val.menuType;
                    demo_groups[val.columnDescription]["values"] = {};
                }
                demo_groups[val.columnDescription].values[val.valueDescription] = val;

            });
            createDemographicsList();
            createSelectedDemographics();
            /* RESET Demographics END*/

            if (event != undefined) {
                //Save the job.
                if (!isJobSaved) {
                    postJobSetUpData('save');
                    isJobSaved = true;
                }
            }
            else {
                if (!isJobSaved) {
                    isJobSaved = true;
                    return true;
                }
                else { return false; }
            }

        }, function (response_error) {
            //var msg = 'Your selections have not been saved.';
            //var msg = response_error.responseText;
            //$('#alertmsg').text(msg);
            //$('#popupDialog').popup('open');
            showErrorResponseText(response_error, false);
        });
        if (event != undefined || !isJobSaved)
            return true;
    }
    else {
        if ($('#ddlStoreLocations').val() != "") {
            //var msg = "The Targeted Selections have been changed for Store '" + $('#hdnSelectedStore').val() + "'. " +
            //     "To save selections you must refresh the map. Click 'Ok' to Refresh Map.";
            $('#popupStoreDialog').popup('close');
            var msg = "You have made changes to selections that will affect your targeted count. In order to save, you will need to click Refresh first."
            $('#confirmMsg2').text(msg);
            $('#popupConfirmMessage a[id=okBut]').text('Refresh');
            $('#popupConfirmMessage').popup('open');
        }
        return false;
    }
}

function resetSelections() {
    $('#btnMapPrefs').addClass('ui-disabled');
    $('#btnMapPrefs')[0].disabled = true;
    $('#btnRevert').addClass('ui-disabled');
    $('#btnRevert')[0].disabled = true;
    sessionStorage.removeItem('updatedCRRTs');
    selectedCRRTS = [];
    initialStoreData = [];
    gSelectedZips = [];
    isTargetChanged = false;
    isRadiusChanged = false;
    $('#ddlStoreLocations').val("").selectmenu('refresh');
    //$('#dvMapPrefOptions input[type=checkbox]:not([id=chkViewRadius])').attr('checked', false).checkboxradio('refresh');
    $('#chkViewZCB').attr('checked', true).checkboxradio('refresh');
    $('#dvLocations').empty();
    $('#dvLeftPanel').css('height', '680px');
    $('#map').empty();
    $('#map').css('transform', '');
    $('#map').css('background-color', 'rgb(255, 255, 255)');
    $('#dvSaveListInfo').empty();
    $('#divAvailable').empty();
    $('#spnRadiusAvblCount').empty();
    $('#spnRadiusTargeted').text('');
    $('#sldrRadius').val('0').slider('refresh');
    $('#rdoPolygon').removeAttr('data-selectedType');
    $('#rdoTargetCounts').removeAttr('data-selectedType');
    $('#rdoRadius').removeAttr('data-selectedType');
    $('#rdoPolygon').attr('checked', false).checkboxradio('refresh');
    $('#rdoRadius').attr('checked', false).checkboxradio('refresh');
    $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh');
    $('#txtTargeted').empty();
    $('#spnTargeted').text('0');

    $('#spnPheripheralTargets').html(0);
    $('#spnRadiusTargeted').html(0);
    $('#spnTotalTargets').html(0);
    $('#trPheripheralTargets').css('display', 'none');
    $('#trTargetsSeparator').css('display', 'none');
    $('#trTotalTargets').css('display', 'none');

    $('#btnSaveSelections').addClass('ui-disabled');
    $('#btnSaveSelections')[0].disabled = true;
    $('#btnRefresh').addClass('ui-disabled');
    $('#btnRefresh')[0].disabled = true;
    $('#dvSectionMethod').collapsible("collapse");
    $('#dvSectionMethod').collapsible("disable");
    $('#dvSectionMethod').addClass('ui-disabled');
    $('#btnPieChart').addClass('ui-disabled');
    $('#btnPieChart')[0].disabled = true;
    if ($('#dvSectionMethod')[0] != undefined) {
        $('#dvSectionMethod')[0].disabled = true;
    }
    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        $('#txtRadiusTargeted').val('3000');
        $('#spnRadiusTargeted').val('3000');
    }
    $("#dvSelectByRadius").collapsible('expand');
    $('#dvPostalBoundarySelections').collapsible('collapse');
    $('#dvPostalBoundarySelections').css('display', 'none');

    var source_id = "";
    $.each($('#dvMailingTypes div[data-role=collapsible]'), function () {
        if (!$(this).hasClass('ui-collapsible-collapsed')) {
            source_id = $(this).attr('id');
            return false;
        }
    });
    $('#' + source_id).find('span[id^=spnSaveListInfo]').html("");
    window.setTimeout(function () {
        $('#' + source_id).find('span[id^=spnAvailableTotal]').html("");
        $('#' + source_id).find('span[id^=spnTargetedTotal]').html("");
        $('#' + source_id).find('span[id^=spnDnmUSource]').html("");
    }, 1500);
    $('#' + source_id).find('p[id^=pCounts]').css('display', 'none');
    $('#' + source_id).find('p[id^=pSavedInfo]').css('display', 'none');
    $('#' + source_id).collapsible('collapse');
}
//when clicked on "Refresh Map" button to validate the targeted quantity. If the entered quantity is greater than total targeted quanity, show message.
function validateTargetQty() {
    //    var entered_targeted_qty = parseInt($("#txtTargeted").val());
    //    alert("entered_targeted_qty:" + entered_targeted_qty);
    //    var total_targeted = parseInt(getTargetedCounts());
    //    alert("total_targeted:" + total_targeted);
    //    $("#txtTargeted").val(total_targeted);
    //    var radius = parseInt($("#txtRadius").val());
    //    var error_text = "The targeted quantity of " + entered_targeted_qty + " cannot be found within a " + radius + " mile radius of this location. ";
    //    error_text += "Either increase the radius selection or decrease the targeted quantity.";
    //    if (entered_targeted_qty > total_targeted) {
    //        $('#alertmsg').text(error_text);
    //        $('#popupDialog').popup('open');
    //        $("#txtTargeted").val(entered_targeted_qty);
    //        return false;
    //    }
    //    else {
    //        refreshMap();
    //    }
    var targeted_qty = parseInt($("#txtTargeted").val());
    if (targeted_qty <= 0) {
        $('#alertmsg').text("Please select at least One Zip route.");
        $('#popupDialog').popup('open');
        $("#txtTargeted").val(targeted_qty);
        $("#spnTargeted").html(targeted_qty);
        return false;
    }
    else {
        return true;
    }
}


//when user checks "Select Entire Zip" / "Individual Zip codes"..
function selectAllZips(is_check_all, ctrl, avlb, also_in_store) {
    ctrl = $('#' + ctrl)[0];
    //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip])');
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip])');
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    if (is_check_all)
        selectedCRRTS = $.extend(true, {}, selected_crrts);

    var selected_zip = $('#hdnSelectedZip').val();
    var is_select_all = $('#checkAll' + selected_zip)[0].checked;
    var targeted_value = 0;
    $('#hdnSelectedStore').val($('#ddlStoreLocations option:selected').text());
    //    var store_info = jQuery.grep(gData, function (obj) {
    //        return obj.storeId === selectedStoreId;
    //    });
    store_info = gData;
    var zip_routes = jQuery.grep(store_info.zips, function (key, obj) {
        return key.zipCode === parseInt(selected_zip);
    });
    if (!is_select_all) {
        targeted_value = zip_routes[0].targeted;
    }

    //getting all crrt checkboxes in that selected zip.
    var selected_zip_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '"):not("#chkEntireZip' + selected_zip + '")');
    if (is_check_all) {
        $.each(selected_zip_crrts, function (i, j) {
            if (is_select_all)
                $(this).attr('checked', true).checkboxradio('refresh');
            else
                $(this).attr('checked', false).checkboxradio('refresh');
        });
        if (is_select_all)
            $('#chkEntireZip' + selected_zip).attr('checked', true).checkboxradio('refresh');
        else
            //
            $('#chkEntireZip' + selected_zip).attr('checked', false).checkboxradio('refresh');
    }

    //getting previous selections in the selected zip.
    var selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '"):not("#chkEntireZip' + selected_zip + '")');
    var prev_targetted_value = 0;
    var index = 0;

    var udpated_crrts = (sessionStorage.updatedCRRTs != undefined && sessionStorage.updatedCRRTs != null && sessionStorage.updatedCRRTs != "") ? jQuery.parseJSON(sessionStorage.updatedCRRTs) : [];
    $.each(selected_crrts, function (a, b) {
        var ctrl_id = $(this)[0].id;
        var current_crrt = $.grep(udpated_crrts, function (obj) {
            return obj.crrtID === ctrl_id;
        });

        if ($(this)[0].checked) {
            if (ctrl.id.split('_').length > 2 && $(this)[0].id != ctrl.id) {
                prev_targetted_value += parseInt($(b).attr('targeted'));
                $('#spn_' + zip_routes[0].zipCode + '_' + a).text($(b).attr('targeted'));
            }
            else {
                prev_targetted_value += parseInt(b.value);
                $('#spn_' + zip_routes[0].zipCode + '_' + a).text(b.value);
                $(b).attr('targeted', b.value)
                $(b).attr('isCrrtChanged', true);
                if (current_crrt.length > 0) {
                    current_crrt[0].crrtID = $(this)[0].id;
                    current_crrt[0].status = 'checked';
                }
                else {
                    udpated_crrts.push({
                        crrtID: $(this)[0].id,
                        status: 'checked'
                    });
                }
            }
        }
        else {
            $('#spn_' + zip_routes[0].zipCode + '_' + a).text(0);
            if (current_crrt.length > 0) {
                current_crrt[0].crrtID = $(this)[0].id;
                current_crrt[0].status = 'unchecked';
            }
            else {
                udpated_crrts.push({
                    crrtID: $(this)[0].id,
                    status: 'unchecked'
                });
            }
        }
        index++;
    });
    var the_spn = '';
    $('#spnCheckAll' + zip_routes[0].zipCode).text(prev_targetted_value);
    var zip_selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '"):not("#chkEntireZip' + selected_zip + '"):checked');

    var checked_status = false;
    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
        if (zip_selected_crrts.length > 0)
            checked_status = true;
    }
    else {
        if (zip_selected_crrts.length == selected_crrts.length) {
            checked_status = true;
        }
    }
    if (checked_status) {
        $('#chkEntireZip' + selected_zip).attr('checked', checked_status).checkboxradio('refresh');
        if (zip_selected_crrts.length == selected_crrts.length)
            $('#checkAll' + selected_zip).attr('checked', checked_status).checkboxradio('refresh');
        else {
            $('#checkAll' + selected_zip).attr('checked', false).checkboxradio('refresh');
            the_spn = zip_selected_crrts.length + ' of ' + selected_crrts.length + ' Carrier Routes Selected';
        }

        if (parseInt($('#spnCheckAllAvbl' + zip_routes[0].zipCode).text()) == prev_targetted_value)
            the_spn = 'Entire Zip Selected';
    }
    else {
        $('#chkEntireZip' + selected_zip).attr('checked', checked_status).checkboxradio('refresh');
        $('#checkAll' + selected_zip).attr('checked', checked_status).checkboxradio('refresh');
        the_spn = zip_selected_crrts.length + ' of ' + selected_crrts.length + ' Carrier Routes Selected';
    }

    if (also_in_store != 'null' && also_in_store != null) {
        the_spn += '<br/>Also in Store: ' + also_in_store;
    }
    $('#spn' + selected_zip).html(the_spn);

    $('#spnTargetedZipCnt' + selected_zip).html(prev_targetted_value);
    $('div.ui-collapsible-content', "#" + $('#hdnSelectedZip').val()).trigger('collapsibleexpand');

    //var selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '):not("#chkEntireZip' + selected_zip + '")');
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip])');
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip])');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        //diff = getDifferences(selectedCRRTS, selected_crrts);
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        //diff = getDifferences(selected_crrts, selectedCRRTS);
        diff = getDifferences(selectedCRRTS, selected_crrts);
    if (diff.length != undefined && diff.length > 0)
        //if (jobCustomerNumber != "99997")
        $('#btnSaveSelections').removeClass('ui-disabled');
    $('#btnSaveSelections')[0].disabled = false;
    $('#btnRefresh').removeClass('ui-disabled');
    $('#btnRefresh')[0].disabled = false;
    var zips_changed = $('#hdnZipsChanged').val();
    if (zips_changed.indexOf(zip_routes[0].zipCode) == -1)
        zips_changed += (zips_changed != "") ? '|' + zip_routes[0].zipCode : zip_routes[0].zipCode;
    $('#hdnZipsChanged').val(zips_changed);
    sessionStorage.updatedCRRTs = JSON.stringify(udpated_crrts);
    if (udpated_crrts.length > 0) {
        //$('#btnRefresh').css('display', 'block');
        //$('#dvRevisedDemosZipsText').html('<span style="color:red;font-size:13px;">Click the Refresh button to update the map and counts with your revised postal boundary selections.</span>');
        //$('#dvRevisedDemosZipsText').css('display', 'block');
        $('#btnRefresh').removeClass('ui-btn-e').addClass('ui-btn-h');
        $('#btnRefresh').removeClass('ui-disabled');
        $('#pMapRefreshMsg').css('display', 'block');
        $('#pMapRefreshMsg').css('display', '');
        $('#btnRefresh')[0].disabled = false;
    }
    selectedCRRTS = [];
    isMapRefreshed = false;
}
function generatePieChart() {
    if (gData != undefined && gData.demos != undefined && gData.demos.length > 0) {
        var available_count = parseInt($('#spnRadiusAvblCount').text().replace(",", ""));
        available_count = available_count - gData.targetedQty;
        var temp_selected_demos = {};
        $.each(gData.demos, function (key, val) {
            if (temp_selected_demos[val.demoName] == undefined) {
                temp_selected_demos[val.demoName] = {};
                $.each(val.demoValues, function (key1, val1) {
                    if (key1 == 0)
                        temp_selected_demos[val.demoName] = val1.name;
                    else
                        temp_selected_demos[val.demoName] = temp_selected_demos[val.demoName] + ' | ' + val1.name;
                });
            }
        });
        if (temp_selected_demos != undefined) {
            var targeted_demographics = '';
            var available_demographics = '';
            $.each(temp_selected_demos, function (key, val) {
                targeted_demographics = targeted_demographics + ((targeted_demographics.length > 0 ? ' , ' : '') + (key + '(' + val + ')'));
                available_demographics = available_demographics + ((available_demographics.length > 0 ? ' , ' : '') + key);
            });
            //var fusion_chart_data = '[{"label":' + getDisplayDemographicName(targeted_demographics) + ',"highlight": "#5AD3D1","value":"' + gData.targetedQty + '"},';
            //fusion_chart_data += '{"label":' + getDisplayDemographicName(available_demographics) + ',"highlight": "#FF5A5E","value":"' + available_count + '"}]';

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Demographics');
            data.addColumn('number', 'Value');
            data.addRows([[getDisplayDemographicName(targeted_demographics), gData.targetedQty], [getDisplayDemographicName(available_demographics), available_count]]);
            // var google_chart_data = '[["Subject", "Number of Enroles"],["' + targeted_demographics + '",' + gData.targetedQty + '],' + '["' + available_demographics + '",' + available_count + ']]';

            window.setTimeout(function () {
                google.setOnLoadCallback(loadGooglePieChart(data));
                //createPieChart(fusion_chart_data);
            }, 3000);
        }
    }
}

createPieChart = function (pie_chart_data) {
    FusionCharts.ready(function () {
        var pieChart = new FusionCharts({
            type: "pie3d",
            renderAt: "fusionChart",
            width: "500",
            height: "300",
            dataFormat: "json",
            dataSource: {
                "chart": {
                    "animation": "1",
                    "caption": "Monthly revenue for last year",
                    "subCaption": "Harry's SuperMart",
                    "xAxisName": "Month",
                    "yAxisName": "Revenues (In USD)",
                    "theme": "zune"
                },

                "data": pie_chart_data
            }

        }).render();
    });
};

var loadGooglePieChart = function (google_chart_data) {
    var options = {
        title: 'Demographics',
        is3D: true,
        animation: {
            duration: 5000,
            easing: 'out',
            startup: true
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('googleChart'));
    chart.draw(google_chart_data, options);
};

function displayChart() {
    if ($('#ddlStoreLocations').val() != "")
        $('#popUpPieChart').popup('open');
}
//when user checks "Select Entire Zip" / "Individual Zip codes"..
function calculateTargetedCnt() {

    //eval(getURLDecode(pagePrefs.scriptBlockDict.calculateTargetedCnt));
    if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER) {
        //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]');
        var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]');
        $.each(selected_crrts, function (key1, val1) {
            $(val1).attr('checked', true).checkboxradio('refresh');
        });
        //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]:checked');
        //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]');
        var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]');
        //        $.each(gDataAll.zips, function (key, val) {
        //            $('#spnTargetedZipCnt' + val.zipCode).html(val.available);
        //            //$('#spnTargetedZipCnt' + val.zipCode).html(val.available);
        //        });
        gSelectedZips = $.extend(true, {}, gDataAll);
        $.each(gSelectedZips.zips, function (key, val) {
            val.crrts = [];
        });
        sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
        refreshMap();
    }
    else {
        makePeripheralSelections();
        refreshMap();
    }
    $('#hdnZipsChanged').val('');
    $('#waitPopUp').popup('close');
}
function makePeripheralSelections() {
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip])');
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        //diff = getDifferences(selectedCRRTS, selected_crrts);
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        //diff = getDifferences(selected_crrts, selectedCRRTS);
        diff = getDifferences(selectedCRRTS, selected_crrts);

    //var targed_cnt = 0;
    //        $.each(selected_crrts, function (i, j) {
    //            if ($(this)[0].id.toLowerCase().indexOf('checkall') == -1 && $(this)[0].id.toLowerCase().indexOf('chkentirezip') == -1 && $(this)[0].checked) {
    //                //                var tar_zip_cnt = ($(this)[0].targeted != undefined) ? $(this)[0].targeted : 0;
    //                //                var avbl_zip_cnt = ($(this)[0].value != undefined) ? $(this)[0].value : 0;
    //                //                targed_cnt = ((!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? parseInt(tar_zip_cnt) : parseInt(avbl_zip_cnt)) + parseInt(targed_cnt);
    //                targed_cnt = parseInt(targed_cnt) + parseInt($(this)[0].value);
    //            }
    //        });
    //$('#txtTargeted').html(targed_cnt);
    //if (validateTargetQty()) {
    var temp_g_data = $.extend(true, {}, gData);
    var temp_g_data = gData;
    var targeted_qty = 0;
    var phrl_targets = 0;
    var udpated_crrts = (sessionStorage.updatedCRRTs != undefined && sessionStorage.updatedCRRTs != null && sessionStorage.updatedCRRTs != "") ? jQuery.parseJSON(sessionStorage.updatedCRRTs) : [];
    if (temp_g_data != null && temp_g_data.zips != undefined) {
        var zip_routes = jQuery.grep(temp_g_data.zips, function (key, obj) {
            var zip = key.zipCode;
            var is_selected = false;
            var index = 0;
            var selected_crrts_cnt = 0;
            $.each(key.crrts, function (i, j) {
                var current_crrt = $.grep(udpated_crrts, function (obj) {
                    return obj.crrtID === 'check_' + zip + '_' + index;
                });
                if ($('#check_' + zip + '_' + index)[0] != undefined && $('#check_' + zip + '_' + index)[0].checked) {
                    is_selected = true;
                    j.isChecked = 1;
                    if (current_crrt.length == 0) {
                        selected_crrts_cnt = ((!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? parseInt(j.targeted) : parseInt(j.available)) + parseInt(selected_crrts_cnt);
                        if (j.selectType == 'f' && !$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed'))
                            phrl_targets += ((!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? parseInt(j.targeted) : parseInt(j.available));
                    }
                }
                else {
                    j.isChecked = 0;
                }
                if (current_crrt.length > 0)
                    if (current_crrt[0].status == 'checked') {
                        j.selectType = 'f';
                        j.targeted = j.available;
                        selected_crrts_cnt = ((!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? parseInt(j.targeted) : parseInt(j.available)) + parseInt(selected_crrts_cnt);
                        phrl_targets += ((!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? parseInt(j.targeted) : parseInt(j.available));
                    }
                    else {
                        j.selectType = '';
                        j.targeted = 0;
                    }
                index++;
            });
            if (is_selected) {
                key.targeted = selected_crrts_cnt;
                key.isChecked = 1;
                targeted_qty = parseInt(targeted_qty) + parseInt(selected_crrts_cnt);
                //return key;
            }
            else {
                if (key.isChecked) {
                    delete key["targeted"];
                    key.isChecked = 0;
                }
            }
            return key;
        });
        $('#spnTargeted').html(targeted_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        var source_id = "";
        $.each($('#dvMailingTypes div[data-role=collapsible]'), function () {
            if (!$(this).hasClass('ui-collapsible-collapsed')) {
                source_id = $(this).attr('id');
                return false;
            }
        });
        window.setTimeout(function () {
            //$('#' + source_id).find('span[id^=spnAvailableTotal]').html($('#spnRadiusAvblCount').text().replace(/\B(?=(\d{3})+(?!\d))/g, ",") || $('#divAvailable').text().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            //var targeted_value = (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? ($('#spnRadiusTargeted').text().replace(/\B(?=(\d{3})+(?!\d))/g, ",") || $('#txtRadiusTargeted').val().replace(/\B(?=(\d{3})+(?!\d))/g, ",")) : ($('#spnTargeted').html().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            //$('#' + source_id).find('span[id^=spnTargetedTotal]').html($('#spnRadiusTargeted').text().replace(/\B(?=(\d{3})+(?!\d))/g, ",") || $('#txtRadiusTargeted').val().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#' + source_id).find('span[id^=spnTargetedTotal]').html(targeted_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#' + source_id).find('span[id^=spnDnmUSource]').html(62);
        }, 1500);

        if (phrl_targets != 0 && parseInt(phrl_targets) > 0 && !isZipsChanged) {
            $('#spnRadiusTargeted').html((parseInt(targeted_qty) - parseInt(phrl_targets)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#spnPheripheralTargets').html(phrl_targets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#spnTotalTargets').html(targeted_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#trPheripheralTargets').css('display', '');
            $('#trTargetsSeparator').css('display', '');
            $('#trTotalTargets').css('display', '');
        }
        else {
            $('#spnPheripheralTargets').html(0);
            $('#spnRadiusTargeted').html((parseInt(targeted_qty) - parseInt(phrl_targets)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#spnTotalTargets').html(targeted_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#trPheripheralTargets').css('display', 'none');
            $('#trTargetsSeparator').css('display', 'none');
            $('#trTotalTargets').css('display', 'none');
        }
        if ($('#hdnZipsChanged').val() != "") {
            //$('#spnRadiusTargeted').html(targeted_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            ////$('#txtRadiusTargeted').val(targeted_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            //$('#spnPheripheralTargets').html(phrl_targets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            //$('#spnTotalTargets').html(targeted_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
        temp_g_data.targetedQty = targeted_qty;
        //gData = $.extend(true, {}, temp_g_data);
        temp_g_data.zips = zip_routes;
        gSelectedZips = temp_g_data;

        gSelectedZips["demos"] = makeSelectedDemosObject();
        var other_stores_in_job = $.extend(true, [], storesInOrder);
        var other_stores_in_job = $.grep(other_stores_in_job, function (v) {
            return v !== gSelectedZips.storePk;
        });

        gSelectedZips["otherStoresInOrder"] = other_stores_in_job.join(',');

        sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);



        if (gSelectedZips["zoomLevel"]) delete gSelectedZips["zoomLevel"];
        var diff_obj = DiffObjects(gSelectedZips, initialStoreData);

        if (udpated_crrts.length > 0 || diff_obj.length > 0) {
            //selectedCRRTS = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
            //selectedCRRTS = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
            //if (diff.length != undefined && diff.length > 0)
            $('#btnRevert').removeClass('ui-disabled');
            $('#btnRevert')[0].disabled = false;
        }
    }
}
function buildSelectedZips() {

    var e = 0;
    var zip_code = 0;
    var index = 0;
    var selected_crrts_cnt = 0;
    var target_selected = 0;

    $.each(gSelectedZips.zips, function (a, b) {
        e = 0;
        zip_code = b.zipCode;
        index = 0;
        selected_crrts_cnt = 0;
        target_selected = 0;
        $.each(b.crrts, function (c, d) {
            if (d.isChecked) {
                $('#check_' + zip_code + '_' + index).attr('checked', true).checkboxradio('refresh');
                $('#spn_' + zip_code + '_' + index).text(d.targeted);
                target_selected += parseInt(d.targeted);
                selected_crrts_cnt++;
            }
            index++;
        });
        var the_spn = '';


        var zip_status = "";
        if (selected_crrts_cnt == b.crrts.length && target_selected == b.available)
            zip_status = "Entire Zip Selected"
        else if (selected_crrts_cnt == b.crrts.length && target_selected < b.available)
            zip_status = ""
        else if (selected_crrts_cnt < b.crrts.length)
            zip_status = selected_crrts_cnt + ' of ' + b.crrts.length + ' Carrier Routes Selected';

        if (index == selected_crrts_cnt) {
            if (target_selected == b.available)
                $('#checkAll' + zip_code).attr('checked', true).checkboxradio('refresh');
            else
                $('#checkAll' + zip_code).attr('checked', false).checkboxradio('refresh');
            $('#chkEntireZip' + zip_code).attr('checked', true).checkboxradio('refresh');
            //the_spn = 'Entire Zip Selected';
        }
        the_spn = zip_status;
        $('#spnTargetedZipCnt' + b.zipCode).text(b.targeted);
        //        else {
        //            the_spn = selected_crrts_cnt + ' of ' + index + ' Carrier Routes Selected';
        //        }

        //
        if (b.alsoInStore != 'null' && b.alsoInStore != null) {
            the_spn += '<br/>Also in Store: ' + b.alsoInStore;
        }
        $('#spn' + zip_code).html(the_spn);

    });
}
function refreshMap() {
    gSelectedZips = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != "") ? jQuery.parseJSON(sessionStorage.storeListSelection) : [];
    if (sessionStorage.mapPrefsChanged == undefined || sessionStorage.mapPrefsChanged == null || sessionStorage.mapPrefsChanged == "")
        createMapPreferences(true);
    sessionStorage.removeItem('mapPrefsChanged');
    //set map preferences START ---
    if (Object.keys(gSelectedZips).length > 0) {
        if ($('#chkViewByHouseHold').length > 0)
            gSelectedZips.wantsScatter = ($('#chkViewByHouseHold')[0].checked) ? 1 : 0;
        else if ($('#ddlViewByHousehold').length > 0)
            gSelectedZips.wantsScatter = ($('#ddlViewByHousehold').val() == 0) ? 0 : 1;
        else
            gSelectedZips.wantsScatter = 0;

        //gSelectedZips.wantsCrrtMarkers = ($('#chkViewCRM')[0].checked) ? 1 : 0;
        gSelectedZips.wantsCrrtBorders = ($('#chkViewCRB')[0].checked) ? 1 : 0;
        gSelectedZips.wantsZipBorders = ($('#chkViewZCB')[0].checked) ? 1 : 0;
        gSelectedZips.wantsRadius = ($('#chkViewRadius')[0].checked) ? 1 : 0;
        gSelectedZips.wantsSurroundingLocations = (($('#chkViewSurroundingLocations').length > 0) && $('#chkViewSurroundingLocations')[0].checked) ? 1 : 0;
        gSelectedZips.customerNumber = jobCustomerNumber;
        //gSelectedZips.wantsNeighboringMarkers = ($('#chkViewNLM')[0].checked) ? 1 : 0;

        gSelectedZips.zoomLevel = (isStoreChanged) ? this.defaultMapZoom : ((map != null && map != undefined && map != '') ? map.getZoom() : this.defaultMapZoom);

        if (appPrivileges.roleName == "admin") {
            gSelectedZips.wantsZipCentroids = ($('#chkViewZipCentroids')[0].checked) ? 1 : 0;
            gSelectedZips.wantsCrrtCentroids = ($('#chkViewCRRTCentroids')[0].checked) ? 1 : 0;
        }
        //        gSelectedZips.viewPort = [];
        //        gSelectedZips.paddedViewPort = [];


        // if ($('#ulSelectedCompetitor li').length > 0) {
        if ($('#chkMailAroundCompetitor').is(':checked') && $('#ulSelectedCompetitor li').length > 0) {
            gSelectedZips.compLatitude = $('#ulSelectedCompetitor li').data('lat');
            gSelectedZips.compLongitude = $('#ulSelectedCompetitor li').data('lng');
        }

        if (viewPorts.viewPort != undefined && viewPorts.viewPort != null)
            gSelectedZips.viewPort = viewPorts.viewPort;
        if (viewPorts.paddedViewPort != undefined && viewPorts.paddedViewPort != null)
            gSelectedZips.paddedViewPort = viewPorts.paddedViewPort;
        sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
    }

    //set map preferences END ---
    isStoreChanged = false;
    isMapRefreshed = true;
    window.setTimeout(function () {
        makeGoogleMap();
    }, 800);

    $("#dvMapMarker").css('display', 'block');
    var selected_demos = [];
    selected_demos = makeSelectedDemosObject();
    var diff_demos = DiffObjects(prev_demos, selected_demos);
    if ((diff_demos.length == 0) || (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)) {  //to be removed second conditions later when static loading is removed.
        $('#btnRefresh').addClass('ui-disabled');
        $('#btnRefresh').removeClass('ui-btn-h').addClass('ui-btn-e');
        $('#btnRefresh')[0].disabled = true;
        $('#pMapRefreshMsg').css('display', 'none');
        $('#spnRadiusAvblCount').css('color', '#484848');
        $('#spnRadiusAvblCount').css('font-weight', 'normal');
        $('#divAvailable').css('color', '#484848');
        $('#divAvailable').css('font-weight', 'normal');
        //$('#dvRevisedDemosZipsText').empty();
        //$('#dvRevisedDemosZipsText').css('display', 'none');
    }
}

function showStoreChangeMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupStoreDialog" data-overlay-theme="d" data-history="false" data-theme="c" data-dismissible="false" style="max-width:500px;" class="ui-corner-all">' +
                    '<div data-role="header" data-theme="d" class="ui-corner-top">' +
                    '	<h1>Confirm Changes?</h1>' +
                    '</div>' +
                    '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
                    '<div id="storeMsg"></div>' +
                    '	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnModelSave" data-theme="a" onclick="savaSelectedInfo(event);">Save Selections</a>' +
                    '	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnModelRevert" data-transition="flow" data-theme="a" onclick="revertSelections();">Revert</a>' +
                    '	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnCancel" data-transition="flow" data-theme="a" onclick="cancelSelections()">Cancel</a>' +
                    '</div>' +
                '</div>';
    $("#" + page_name).append(msg_box);
}

function savaSelectedInfo(event) {
    var temp_selected_store_id = $('#ddlStoreLocations').val();
    //if (isDemosChanged) {
    //    getSelectedStoreData();
    //}
    if (saveSelections(event)) {
        $('#ddlStoreLocations').val(temp_selected_store_id).selectmenu('refresh');
        //getSelectedStoreData();
        $('#popupStoreDialog').popup('open');
        $('#popupStoreDialog').popup('close');
        $('#dvAcqMailingList').collapsible('expand');
        $('#sldrRange').slider('enable');
        $('#sldrRange')[0].disabled = false;

        $('#dvSectionMethod').removeClass("ui-disabled");
        $('#dvSectionMethod')[0].disabled = false;
        $('#dvSectionMethod').collapsible("expand");
    }
}

function revertSelections() {
    $('#popupStoreDialog').popup('close');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function getDelay5() {
        gSelectedZips = [];
        gData.zips = [];
        //$('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');

        $('#hdnSelectedStore').val('');
        //    var checked_items = $('input:checked');
        //    $.each(checked_items, function (a, b) {
        //        $(this).attr('checked', false).checkboxradio('refresh');
        //    });
        //getDataFromSession();

        isTargetChanged = false;
        isRadiusChanged = false;
        isMapRefreshed = false;

        if (gSelectedZips["zoomLevel"]) delete gSelectedZips["zoomLevel"];
        var diff_obj = DiffObjects(gSelectedZips, initialStoreData);

        if (diff_obj.length > 0) {
            prevSelectedStoreId = "";
            selectedDemographics = {};
        }
        else if (isDemosChanged) {
            selectedDemographics = {};
            if ($('#ddlStoreLocations').val() == prevSelectedStoreId) {
                makeDemosObject(prev_demos);
                createDemographicsList();
                createSelectedDemographics();
            }
        }
        else {
            if (prevSelectedStoreId != selectedStoreId) {
                selectedDemographics = {};
                $('#ddlStoreLocations').val('').selectmenu('refresh');
                $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
                prevSelectedStoreId = "";
            }
        }
        isDemosChanged = false;
        selectedCRRTS = [];
        sessionStorage.removeItem('storeListSelection');
        sessionStorage.removeItem('updatedCRRTs');
        //buildSelectedZips();
        //refreshMap();
        $('#sldrRange').attr('hasChanged', false);
        // if ($('#ddlStoreLocations').val() != prevSelectedStoreId) {
        if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
            $("#dvSelectByRadius").collapsible('expand');
        else
            $("#dvSelectByZipCrrt").collapsible('expand');

        if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
            $('#rdoRadius').attr('checked', true).checkboxradio('refresh');
            $('#sldrRange').val('1').slider('refresh');
            $('#sldrRange').slider('enable');
            $('#sldrRange')[0].disabled = false;
        }
        else {
            $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh');
            //$('#ddlStoreLocations').trigger('change');
            $('#txtRadiusTargeted').val("3000");
            $('#spnRadiusTargeted').text("3000");
        }
        //}
        //prevSelectedStoreId = "";
        $('#rdoPolygon').removeAttr('data-selectedType');
        $('#rdoTargetCounts').removeAttr('data-selectedType');
        $('#rdoRadius').removeAttr('data-selectedType');
        getSelectedStoreData();
        $('#btnRevert').addClass('ui-disabled');
        $('#btnRevert')[0].disabled = true;
        $('#btnSaveSelections').addClass('ui-disabled');
        $('#btnSaveSelections')[0].disabled = true;
        if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed'))
            //if (jobCustomerNumber != "99997")
            $('#btnSaveSelections').removeClass('ui-disabled');
        $('#btnSaveSelections')[0].disabled = false;
        $('#waitPopUp').popup('close');
    }, 1000);
}
function cancelSelections() {
    $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
    $('#popupStoreDialog').popup('close');
    $('#sldrRange').slider('enable');
    $('#sldrRange')[0].disabled = false;
    $('#btnRefresh').addClass('ui-disabled');
    $('#btnRefresh')[0].disabled = true;
}

function refreshCurrentSelections() {
    $('#popupConfirmMessage').popup('open');
    $('#popupConfirmMessage').popup('close');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function getDelay4() {
        if (!isRadiusChanged && !isTargetChanged && !isDemosChanged && !isZipsChanged) {
            calculateTargetedCnt();
            //selectedCRRTS = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
            selectedCRRTS = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
            //selectedCRRTS = [];
            $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
            //$('#ddlStoreLocations').trigger('onchange');
            //saveSelections();
        }
        else {
            $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
            getSelectedStoreData();
        }
        $('#sldrRange').slider('enable');
        $('#sldrRange')[0].disabled = false;
        $('#waitPopUp').popup('close');
    }
    , 1000);
}

function revertCurrentSelections() {
    $('#popupConfirmMessage').popup('close');
    $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
    $('#sldrRange').slider('enable');
    $('#sldrRange')[0].disabled = false;
}

function saveConfirmMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupConfirmMessage" data-history="false" data-overlay-theme="d" data-theme="c" style="max-width:400px;" class="ui-corner-all">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg2"></div>';
    msg_box += '<div id="colId"></div>';
    msg_box += '<div id="valList"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelBut" onclick="revertCurrentSelections();">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a"  id="okBut" onclick="refreshCurrentSelections();">Ok</a>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

function radiusZipCRRTChangeMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupradiusZipCRRTConfirmDialog" data-history="false" data-overlay-theme="d" data-theme="c" style="max-width:400px;" class="ui-corner-all">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg1"></div>';
    msg_box += '<div id="colId1"></div>';
    msg_box += '<div id="valList1"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-mini="true" data-theme="a" id="cancelBut1" onclick="">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-mini="true" data-theme="a"  id="okBut1" onclick="">Continue</a>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

function makePageSelect(has_search) {
    var zip_data = ((has_search) ? searchData : gDataAll);
    var page_size = Math.round(zip_data.zips.length / gPageSize);
    //    if (gDataAll.zips.length % gPageSize > 0) {
    //        page_size++;
    //    }
    var str = '';
    //var str = '<li>';
    //str += '<select name="pageSelect" id="pageSelect" data-theme="b" data-overlay-theme="d" data-native-menu="false" data-mini="true" onchange="updateStartIndex()">';
    for (var i = 0; i < page_size; i++) {
        str += '<option value="' + i + '">Page ' + (1 + i) + '</option>';
    }
    //str += '</select>';
    //str += '</li>';
    if ($('#search').length == 0) {
        var search_ctrl = '<br/><input type="search" width="20%" maxlength="25" name="search" id="search" value="" onkeyup="searchZips();" onchange="searchZips();"/>';
        $(search_ctrl).insertAfter('#dvPageNumbers');
    }
    if (page_size == 0)
        $('#dvPageNumbers').css('display', 'none');
    else {
        $('#pageSelect option').remove();
        $(str).appendTo('#pageSelect');
        $('#pageSelect').selectmenu('refresh');
        $('#dvPageNumbers').css('display', 'block');
    }
    $('.ui-page').trigger('create');
}

function updateStartIndex(has_search) {
    var multiplier = parseInt($('#pageSelect').prop("selectedIndex"));
    gDataStartIndex = multiplier * gPageSize;
    var upper_value = (gDataStartIndex + gPageSize <= gDataAll.zips.length) ? gDataStartIndex + gPageSize : gDataAll.zips.length;
    var temp_gdata = [];
    temp_gdata = $.extend(true, {}, ((has_search || (searchData != undefined && searchData != null && searchData.zips != undefined && searchData.zips.length > 0)) ? searchData : gDataAll));
    var zips_in_page = temp_gdata.zips.splice(gDataStartIndex, upper_value - gDataStartIndex);
    temp_gdata.zips = zips_in_page;
    gData = temp_gdata;
    $('#dvLocations').empty();
    buildZips(selectedStoreId);
    $.each(gData.zips, function (key, val) {
        $('#spnTargetedZipCnt' + val.zipCode).html(val.available);
    });
    var avbl_cnt = 0;
    $.each(gDataAll.zips, function (key, val) {
        avbl_cnt = parseInt(avbl_cnt) + parseInt(val.available);
    });
    $('#divAvailable').html(avbl_cnt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $('#spnRadiusAvblCount').html(avbl_cnt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $('#txtTargeted').val($('#divAvailable').text().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $('#spnTargeted').html($('#divAvailable').text().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    makeReadOnlyZips();
    //$("#dvLocations").enhanceWithin();
}

function makeReadOnlyZips() {

    eval(getURLDecode(pagePrefs.scriptBlockDict.makeReadOnlyZips));
    //if (jobCustomerNumber == CW_CUSTOMER_NUMBER) {
    //    $($('div[class=ui-checkbox]')).each(function (i) {
    //        $(this).addClass('ui-disabled')
    //        $(this).css('opacity', '0.8');
    //    });
    //    $.each($('#dvLocations div'), function (key, val) {
    //        if (val.id != undefined && val.id != null && val.id != "") {
    //            $('#' + val.id + ' h3 a').click(function (event) {
    //                return false;
    //            });
    //            $('#' + val.id + ' h3 a').css('cursor', 'default');
    //            //$('#' + val.id + ' h3 a').removeClass('ui-btn-icon-left');
    //        }
    //    });
    //}
}

function searchZips() {
    var filterd_list = [];
    var search_value = $('#search').val();

    if (search_value != "" && search_value != undefined) {
        var searched_zips = jQuery.grep(gDataAll.zips, function (a, b) {
            return (a.zipCode.toString().indexOf(search_value) > -1 || a.zipName.toLowerCase().indexOf(search_value.toLowerCase()) > -1 || a.available.toString().indexOf(search_value) > -1 || (a.zipCode.toString() + ' - ' + a.zipName.toLowerCase()).indexOf(search_value.toLowerCase()) > -1);
        });
        searchData = $.extend(true, {}, gDataAll);
        searchData.zips = searched_zips;
        makePageSelect(true);
        updateStartIndex(true);
    }
    else {
        searchData = [];
        makePageSelect(false);
        updateStartIndex(false);
    }

}

function getCountsByConfirm(ctrl) {
    var prev_selected_type = $('input[type=radio][data-selectedType]:not([data-selectedType=""])');
    if ((drawingManager && prev_selected_type.length > 0 && prev_selected_type.attr('data-selectedType') == "p") && ($(ctrl).val() == 'r' || $(ctrl).val() == 't')) {
        //if ((drawingManager && drawingManager.drawingControl) && ($(ctrl).val() == 'r' || $(ctrl).val() == 't')) {
        $('#confirmMsg').html("This will remove all of your current polygon selections. Click \"Ok\" to remove or click \"Cancel\" to keep your current selections.");
        $('#okButConfirm').text('Ok');
        $('#okButConfirm').bind('click', function (event) {
            $('#waitPopUp').popup('open', { positionTo: 'window' });
            $('#okButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
            $('#spnPheripheralTargets').html(0);
            $('#spnTotalTargets').html(0);
            sessionStorage.removeItem("updatedCRRTs");
            prevSelectedMode = prev_selected_type.attr('data-selectedType');
            prev_selected_type.removeAttr('data-selectedType');
            $(ctrl).attr('data-selectedType', $(ctrl).val());
            if ($(ctrl).val() == "r") {
                $('#sldrRange').val('1').slider('refresh');
                gSelectedZips.radius = 1;
            }
            getCountsBy(ctrl);
            var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
            if ((Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0) && temp_store_list_selection.polygonInfo != undefined && temp_store_list_selection.polygonInfo != null && temp_store_list_selection.polygonInfo != "") {
                delete temp_store_list_selection.polygonInfo;
                sessionStorage.storeListSelection = JSON.stringify(temp_store_list_selection);
            }
            if (gSelectedZips.polygonInfo != undefined && gSelectedZips.polygonInfo != null && gSelectedZips.polygonInfo != "") delete gSelectedZips.polygonInfo;
            if (gData.polygonInfo != undefined && gData.polygonInfo != null && gData.polygonInfo != "") delete gData.polygonInfo;
            window.setTimeout(function getRadius() {
                $('#btnRefresh').trigger('click');
            }, 1000);
        });
        $('#cancelButConfirm').bind('click', function (event) {
            $('#cancelButConfirm').unbind('click');
            $('input[type=radio]').attr('checked', false).checkboxradio('refresh');
            $('input[type=radio][value="p"]').attr('checked', true).checkboxradio('refresh');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            $('#popupConfirmDialog').popup('close');
        });
        $('#popupConfirmDialog').popup('open');
    }
    else {
        prevSelectedMode = prev_selected_type.attr('data-selectedType');
        var crr_mode = ctrl.value;
        if (prevSelectedMode != crr_mode && validateTargets()) {
            prev_selected_type.removeAttr('data-selectedType');
            $(ctrl).attr('data-selectedType', $(ctrl).val());
            getCountsBy(ctrl);
            if ($(ctrl).val() != 'p')
                $('#btnRefresh').trigger('click')
        } else {
            if ($(ctrl).val() == 'p')
              changeToPolygonMode(ctrl);
        }
    }
}

function selectAllPostalBoundaryZips(is_checked) {
    $('#popupradiusZipCRRTConfirmDialog').popup('open');
    $('#popupradiusZipCRRTConfirmDialog').popup('close');
    if ($('#dvPostalBoundarySelections').css('display') == 'block') {
        //$('#dvPostalBoundarySelections').collapsible("collapse");
        $('#dvSectionMethod').collapsible("expand");
    }
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    $('#rdoRadius').attr('checked', false).checkboxradio('refresh');
    $('#rdoPolygon').attr('checked', false).checkboxradio('refresh');
    $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh');
    window.setTimeout(function () {
        $('#dvSelectByRadius').collapsible('collapse');
        if (gData != null && gData.zips != undefined) {
            var r_crrts = [];
            var total_targets_selected = 0;
            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
                var zip = key.zipCode;
                var index = 0;
                var targeted_counts = 0;
                $.each(key.crrts, function (i, j) {
                    //if (j.selectType == 'r') {
                    j.isChecked = (is_checked) ? 1 : 0;
                    j.selectType = 'f';
                    $('#' + 'check_' + zip + '_' + index).attr('selectedType', j.selectType);
                    j.targeted = (is_checked) ? j.available : 0;
                    $('#spn_' + zip + '_' + index).text(((j.isChecked) ? j.available : 0));
                    targeted_counts = parseInt(targeted_counts) + parseInt(j.targeted);
                    //}
                    index++;
                });
                $('#spnCheckAll' + zip).text(targeted_counts);
                $('#spnTargetedZipCnt' + zip).text(targeted_counts);
                key.targeted = targeted_counts;
                total_targets_selected = parseInt(total_targets_selected) + parseInt(targeted_counts);
            });
            $('#spnTargeted').text(total_targets_selected.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
        sessionStorage.removeItem('updatedCRRTs');
        sessionStorage.removeItem('storeListSelection');
        gSelectedZips = [];
        $('#dvLocations').empty();
        buildZips(selectedStoreId);
        var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
        if ((Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0) && temp_store_list_selection.polygonInfo != undefined && temp_store_list_selection.polygonInfo != null && temp_store_list_selection.polygonInfo != "") {
            delete temp_store_list_selection.polygonInfo;
            sessionStorage.storeListSelection = JSON.stringify(temp_store_list_selection);
        }
        if (gData.polygonInfo != undefined && gData.polygonInfo != null && gData.polygonInfo != "") delete gData.polygonInfo;
        if (gSelectedZips.polygonInfo != undefined && gSelectedZips.polygonInfo != null && gSelectedZips.polygonInfo != "") {
            delete gData.polygonInfo;
        }
        $('#btnPolygon').addClass('ui-disabled');
        if (drawingManager != undefined && drawingManager != null)
            drawingManager.setDrawingMode(null);
        calculateTargetedCnt();
        $('#hdnMode').val('rtoc');
        $('#dvSelectByZipCrrt').collapsible('expand');
        $('#waitPopUp').popup('close');
        //$("#dvLocations").enhanceWithin();
    }, 1000);
}

function selectAllPostalBoundaries(ctrl) {
    if ($('#dvLocations input[type=checkbox]:not([id^=chkEntireZip]):not([id^=checkAll]):checked').filter('[selectedType = "r"]').length > 0) {
        var msg = "Removing zips/carrier routes or adding zips/carrier routes that intersect with the existing radius will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection.";
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'selectAllPostalBoundaryZips(' + $(ctrl).is(':checked') + ')');
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', "$('#" + ctrl.id + "').attr('checked'," + !ctrl.checked + ").checkboxradio('refresh');  $('#popupradiusZipCRRTConfirmDialog').popup('close');return false;");
        $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
        $('#popupradiusZipCRRTConfirmDialog').popup('open');
    }
    else {
        selectAllPostalBoundaryZips($(ctrl).is(':checked'));
    }

    //if ($(ctrl).is(':checked')) {
    //    ctrls = $('#dvLocations input[type=checkbox]:not([id^=chkEntireZip]):not([id^=checkAll]):not([checked=true])');
    //    $.each(ctrls, function () {
    //        if (!$(this).is(':checked')) {
    //            $(this).attr('checked', true).trigger('change');
    //        }
    //    });
    //}
    //else if (!$(ctrl).is(':checked')) {
    //    ctrls = $('#dvLocations input[type=checkbox]:not([id^=chkEntireZip]):not([id^=checkAll]):not([checked=false])');
    //    $.each(ctrls, function () {
    //        if ($(this).is(':checked')) {
    //            $(this).attr('checked', false).trigger('change');
    //        }
    //    });
    //}
    //$(ctrls).checkboxradio('refresh');
}

function getCountsBy(ctrl) {
    // $('#chkViewByHouseHold').attr('checked', false).checkboxradio('refresh');
    //if (drawingManager && drawingManager.drawingControl) {
    //    $('#confirmMsg').html("This will remove all of your current polygon selections. Click \"Ok\" to remove or click \"Cancel\" to keep your current selections.");
    //    $('#popupConfirmDialog').popup('open');
    //}
    var prev_selected_type = $('input[type=radio][data-selectedType]:not([data-selectedType=""])');
    if ($(ctrl).val() == 'r') {
        toggleDrawingManager(false);
        $('#trRadius').css('display', 'block');
        $('#trRadius').css('display', '');
        $('#sldrRange').slider('enable');
        $('#sldrRange')[0].disabled = false;
        if ($('#ddlStoreLocations').val() == "") {
            $('#sldrRange').slider('disable');
            $('#sldrRange')[0].disabled = true;
        }
        if (gSelectedZips.radius == 0)
            $('#sldrRange').val('1').slider('refresh');
        $('#sldrRange').css('color', 'black');
        $('#spnRadiusTargeted').css('display', 'block');
        $('#txtRadiusTargeted').css('display', 'none');
        $('#spnTextRadiusTargeted').css('display', 'none');
        $('#fldStDrawTools').css('display', 'none');
        if (prev_selected_type.attr('data-selectedType') != 'r') {
            var target_cnt = $('#txtRadiusTargeted').val();
            target_cnt = (typeof (target_cnt) == "string" && target_cnt.indexOf(',') > -1) ? target_cnt.replace(',', '') : target_cnt;
            gSelectedZips.targetedAsk = 0;
            gSelectedZips.radius = $('#sldrRange').val();
            sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
        }
        //$('#btnRefresh').trigger('click');
    }
    else if ($(ctrl).val() == 't') {

        toggleDrawingManager(false);

        $('#trRadius').css('display', 'block');
        $('#trRadius').css('display', '');
        $('#sldrRange').slider('disable');
        $('#sldrRange')[0].disabled = true;
        $('#sldrRange').css('color', 'lightgray');
        $('#txtRadiusTargeted').css('display', 'block');
        $('#spnTextRadiusTargeted').css('display', 'block');
        $('#spnRadiusTargeted').css('display', 'none');
        $('#fldStDrawTools').css('display', 'none');

        if ($('#spnRadiusTargeted').text() != "0") {
            if ($('#txtRadiusTargeted').val() == "" || $('#txtRadiusTargeted').val() == "0")
                $('#txtRadiusTargeted').val($('#spnRadiusTargeted').text().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            //$('#txtRadiusTargeted').val($('#spnRadiusTargeted').text().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
        else {
            $('#txtRadiusTargeted').val("3000".toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            $('#spnRadiusTargeted').text("3000".toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
        if (prev_selected_type.attr('data-selectedType') != 't') {
            var target_cnt = $('#txtRadiusTargeted').val();
            target_cnt = (typeof (target_cnt) == "string" && target_cnt.indexOf(',') > -1) ? target_cnt.replace(',', '') : target_cnt;
            if (gData)
                gData.targetedAsk = target_cnt;
            gSelectedZips.targetedAsk = target_cnt;
            sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
        }
        //$('#btnRefresh').trigger('click');
    }
    else if ($(ctrl).val() == 'p') {
        changeToPolygonMode(ctrl);
        //$('#trRadius').css('display', 'none');
        //$('#spnTextRadiusTargeted').css('display', 'none');
        //$('#spnRadiusTargeted').css('display', 'block');
        //if (myCircle) myCircle.setMap(null);
        //if (map) {
        //    if (!drawingManager) {
        //        $('#fldStDrawTools').css('display', 'block');
        //        createDrawingManager();
        //        $('#btnSaveSelections').addClass('ui-disabled');
        //        $('#btnSaveSelections')[0].disabled = true;
        //    }
        //    else {
        //        toggleDrawingManager(true);
        //        if (gSelectedZips.polygonInfo != undefined && gSelectedZips.polygonInfo != null && gSelectedZips.polygonInfo != "") {
        //            drawingManager.setDrawingMode(null);
        //            $('#btnSaveSelections').removeClass('ui-disabled');
        //            $('#btnSaveSelections')[0].disabled = false;
        //        }
        //        else {
        //            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
        //            $('#btnSaveSelections').addClass('ui-disabled');
        //            $('#btnSaveSelections')[0].disabled = true;
        //        }
        //        drawingManager.setMap(map);
        //        $('#fldStDrawTools').css('display', 'block');
        //    }
        //    gSelectedZips.targetedAsk = 0;
        //    gSelectedZips.radius = 0;
        //    sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
        //}
    }
}

function changeToPolygonMode(ctrl) {
    $('#trRadius').css('display', 'none');
    $('#spnTextRadiusTargeted').css('display', 'none');
    $('#spnRadiusTargeted').css('display', 'block');
    if (myCircle) myCircle.setMap(null);
    if (map) {
        if (!drawingManager) {
            $('#fldStDrawTools').css('display', 'block');
            createDrawingManager();
            $('#btnSaveSelections').addClass('ui-disabled');
            $('#btnSaveSelections')[0].disabled = true;
            $('#btnSaveTop').addClass('ui-disabled');
            $('#btnSaveTop')[0].disabled = true;
            $('#btnSave').addClass('ui-disabled');
            $('#btnSave')[0].disabled = true;
        }
        else {
            toggleDrawingManager(true);
            //if (gSelectedZips.polygonInfo != undefined && gSelectedZips.polygonInfo != null && gSelectedZips.polygonInfo != "") {
            if (gData.polygonInfo != undefined && gData.polygonInfo != null && gData.polygonInfo != "") {
                drawingManager.setDrawingMode(null);
                $('#btnSaveSelections').removeClass('ui-disabled');
                $('#btnSaveSelections')[0].disabled = false;
                $('#btnSaveTop').removeClass('ui-disabled');
                $('#btnSaveTop')[0].disabled = false;
                $('#btnSave').removeClass('ui-disabled');
                $('#btnSave')[0].disabled = false;
            }
            else {
                drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
                $('#fldStDrawTools').css('display', 'block');
                $('#btnSaveSelections').addClass('ui-disabled');
                $('#btnSaveSelections')[0].disabled = true;

                $('#btnSaveTop').addClass('ui-disabled');
                $('#btnSaveTop')[0].disabled = true;
                $('#btnSave').addClass('ui-disabled');
                $('#btnSave')[0].disabled = true;
            }
            drawingManager.setMap(map);
            $('#fldStDrawTools').css('display', 'block');
        }
        gSelectedZips.targetedAsk = 0;
        gSelectedZips.radius = 0;
        sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
    }
}

function createDrawingManager() {
    //if (!drawingManager) {
    var polyOptions = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#2f51fd',
        fillOpacity: 0.4
    };
    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: false,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
            //google.maps.drawing.OverlayType.MARKER,
            //google.maps.drawing.OverlayType.CIRCLE,
              google.maps.drawing.OverlayType.POLYGON//,
            //google.maps.drawing.OverlayType.POLYLINE,
            //google.maps.drawing.OverlayType.RECTANGLE
            ]
        },
        polygonOptions: polyOptions
    });
    drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
        if (e.type != google.maps.drawing.OverlayType.MARKER) {
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setDrawingMode(null);
            drawingManager.setOptions({
                drawingControl: false
            });
            $('#btnPolygon').attr('disabled', true);
            $('#btnPolygon').addClass('ui-disabled');

            $('#btnDeletePolygon')[0].disabled = false;
            $('#btnDeletePolygon').attr('disabled', false);
            $('#btnDeletePolygon').removeClass('ui-disabled');
            $('#btnMovePolygon')[0].disabled = false;
            $('#btnMovePolygon').attr('disabled', false);
            $('#btnMovePolygon').removeClass('ui-disabled');
            //$('#btnPolygon').css('opacity', '0.7');
            // Add an event listener that selects the newly-drawn shape when the user
            // mouses down on it.
            var newShape = e.overlay;
            newShape.type = e.type;
            google.maps.event.addListener(newShape, 'click', function () {
                setSelection(newShape, e);
            });
            google.maps.event.addListener(newShape, 'dragend', function () {
                // Polygon was dragged
                //alert('polygon dragged');
            });
            showArrays(e);
            drawingManager.setDrawingMode(null);
        }
    });

    //    var homeControlDiv = document.createElement('div');
    //    var homeControl = new HomeControl(homeControlDiv, map);

    //    homeControlDiv.index = 1;
    //    map.controls[google.maps.ControlPosition.TOP_CENTER].push(homeControlDiv);
    //}
}

function toggleDrawingManager(can_show) {
    if (drawingManager) {
        // drawingManager.setOptions({
        //     drawingControl: can_show
        // });
        //        $('#dvClearShape').css('display', ((can_show) ? 'block' : 'none'));
    }
}

//function setCRRTSelection(is_clicked) {
//    if (is_clicked) {
//        $('#chkViewByHouseHold').attr('checked', false).checkboxradio('refresh');
//        $('#dvSelectByRadius').trigger('collapse');
//        if (gData != null && gData.zips != undefined) {
//            var r_crrts = [];
//            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
//                var zip = key.zipCode;
//                var is_selected = false;
//                var index = 0;
//                $.each(key.crrts, function (i, j) {
//                    if (j.selectType == 'r') {
//                        is_selected = false;
//                        j.isChecked = 0;
//                        j.selectType = '';
//                        j.targeted = 0;
//                        //r_crrts[i] = 'check_' + zip + '_' + index;
//                        $('#' + 'check_' + zip + '_' + index).attr('checked', false).checkboxradio('refresh');
//                        $('#spn_' + zip + '_' + index).text(j.targeted);
//                        $('#spnCheckAll' + zip).text(j.targeted);
//                    }
//                    index++;
//                });
//                key.targeted = 0;
//                //if (!is_selected) {
//                $('#chkEntireZip' + zip).attr('checked', false).checkboxradio('refresh');
//                $('#checkAll' + zip).attr('checked', false).checkboxradio('refresh');
//                //}
//                $('#spn' + zip).text('0 of ' + key.crrts.length + ' Carrier Routes Selected');
//                $('#spnTargetedZipCnt' + key.zipCode).text(key.targeted);
//                $('#spnTargeted').text(0);
//            });
//        }
//        sessionStorage.removeItem('updatedCRRTs');
//        sessionStorage.removeItem('storeListSelection');
//        gSelectedZips = [];
//    }
//}
function getDemographicsUrl() {
    var demographics_url = "JSON/mappingDemographics.JSON";
    switch (jobCustomerNumber) {
        case DCA_CUSTOMER_NUMBER:
            demographics_url = "JSON/mappingDemographics_DCA.JSON"
            break;
        case KUBOTA_CUSTOMER_NUMBER:
        case GWA_CUSTOMER_NUMBER:
            demographics_url = "JSON/mappingDemographics_KUBOTA.JSON"
            break;
        case AAG_CUSTOMER_NUMBER:
            demographics_url = "JSON/mappingDemographics_AAG.JSON"
            break;
        case SK_CUSTOMER_NUMBER:
            demographics_url = "JSON/mappingDemographics_SK.JSON"
            break;
    }

    return demographics_url;
}

function loadDemographics() {
    var demographics_url = getDemographicsUrl();
    var temp_auth_string = sessionStorage.authString;
    var avblDemos = [];
    var selDemos = [];
    if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        //sessionStorage.authString = "Basic dXNlcl9yZWcxOnVzZXJfcmVnMQ==";
        var temp_key = "";
        var is_demo_found = false;
        $.getJSON(demographics_url, function (demographics) {
            //$.getJSON("JSON/defaultDemographics_DCA.JSON", function (selected_demos) {
            mappingDemographics = demographics;
            $.each(mappingDemographics, function (key, val) {
                //mappingDemographics.forEach(function (a) {
                temp_key = key;
                if (!demo_groups[val.columnDescription]) {
                    //if (val.columnDescription == s_d_val)
                    demo_groups[val.columnDescription] = {};
                    demo_groups[val.columnDescription]["menuType"] = val.menuType;
                    demo_groups[val.columnDescription]["values"] = {};
                }
                //$.each(selected_demos, function (s_d_key, s_d_val) {
                //    if (s_d_val[val.columnDescription] != undefined && s_d_val[val.columnDescription] != null && s_d_val[val.columnDescription] == val.valueDescription) {
                //        val.selector = "in";
                //        return false;
                //    }
                //    else if (s_d_val[val.columnDescription] != undefined && s_d_val[val.columnDescription] != null && typeof (s_d_val[val.columnDescription]) == "object" && s_d_val[val.columnDescription].length > 0) {
                //        var is_avbl = false;
                //        $.each(s_d_val[val.columnDescription], function (temp_key, temp_val) {
                //            if (val.valueDescription == temp_val) {
                //                val.selector = "in";
                //                is_avbl = true;
                //                return false;
                //            }
                //        });
                //        if (is_avbl == true)
                //            return false;
                //    }
                //});
                demo_groups[val.columnDescription].values[val.valueDescription] = val;
            });
            createDemographicsList();
            createSelectedDemographics();
            //});
        });

        //$.getJSON("JSON/demographicsListToDisplay.JSON", function (result) {
        //    avblDemos = result;
        //});
        //$.getJSON("JSON/selectedDemographics.JSON", function (result) {
        //    selDemos = result;
        //});
    } else {
        getCORS(gDemograhicsLoadUrl, null, function (data) {
            if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
                sessionStorage.authString = temp_auth_string;
                //mappingDemographics = avblDemos;
            }
            //else {
            mappingDemographics = data;
            //}

            mappingDemographics.forEach(function (a) {
                if (!demo_groups[a.columnDescription]) {
                    demo_groups[a.columnDescription] = {};
                    demo_groups[a.columnDescription]["menuType"] = a.menuType;
                    demo_groups[a.columnDescription]["values"] = {};
                }
                demo_groups[a.columnDescription].values[a.valueDescription] = a;
            });
            createDemographicsList();
            createSelectedDemographics();
        }, function (error_response) {
            if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
                sessionStorage.authString = temp_auth_string;
            }
            showErrorResponseText(error_response, false);
        });
    }
}

function createDemographicsList() {
    var demo_list_item = '';
    var index = 0;
    $('#dvDemographics').css('display', 'block');
    var is_default_demo = false
    var temp_demos = {};
    var temp_sorted = Object.keys(demo_groups).sort();
    var temp = [];
    $.each(temp_sorted, function (key, val) {
        if (val == "Roll-Up Code - E-Tech") {
            temp.push("Ethinicity");
        }
        else if (val == "Income - Estimated Household") {
            temp.push("Household Estimated Income");
        }
        else if (val == "Home Property Type Detail Rp") {
            temp.push("Home Property Type");
        }
        else if (val == "Age In Two-Year Incr - Input Ind - Reseller") {
            temp.push("Two Year Age Range");
        }
        else if (val == "Presence Of Children - Reseller") {
            temp.push("Presence Of Children");
        }
        else
            temp.push(val);

    });
    temp_sorted = temp.sort();
    $.each(temp_sorted, function (key, val) {
        $.each(demo_groups, function (key1, val1) {
            if (val == "Ethinicity" && key1 == "Roll-Up Code - E-Tech") {
                temp_demos[key1] = val1;
                return false;
            }
            else if (val == "Two Year Age Range" && key1 == "Age In Two-Year Incr - Input Ind - Reseller") {
                temp_demos[key1] = val1;
                return false;
            }
            else if (val == "Presence Of Children" && key1 == "Presence Of Children - Reseller") {
                temp_demos[key1] = val1;
                return false;
            }
            else if (val == "Household Estimated Income" && key1 == "Income - Estimated Household") {
                temp_demos[key1] = val1;
                return false;
            }
            else if (val == "Home Property Type" && key1 == "Home Property Type Detail Rp") {
                temp_demos[key1] = val1;
                return false;
            }
            else if (key1 == val) {
                temp_demos[key1] = val1;
                return false;
            }
        });
    });
    demo_groups = temp_demos;
    $.each(demo_groups, function (key, val) {
        is_default_demo = false;
        $.each(val.values, function (demo_key, demo_value) {
            var k = demo_key;
            if (demo_value.selector != undefined && demo_value.selector != null && demo_value.selector != "") {
                is_default_demo = true;
                return false;
            }
        });
        if (is_default_demo) {
            //val.values["Default Demographic"] = true;
            selectedDemographics[key] = val;
            delete demo_groups[key];
            is_default_demo = false;
        }
        else {
            //val.values["Default Demographic"] = false;
            demo_list_item += '<li data-icon="plus" data-theme="c"><a title="' + getDisplayDemographicName(key) + '"href="#" style="font-size: 12px;" id="' + key.replace(/ /g, '_') + '" value="' + index + '" data-options=\'' + JSON.stringify(val).replace(/'/g, "\\'") + '\' onclick="addDemographics(this);">' + getDisplayDemographicName(key) + '</a></li>';
        }
        index++;
    });
    $('#ulDemographics').empty();
    window.setTimeout(function makeDelay() {
        $('#ulDemographics').append(demo_list_item).listview('refresh');
    }, 100);
    if (Object.keys(demo_groups).length == 0)
        $('#dvDemographics').css('display', 'none');
    if (isDemosDefaulted) {
        prev_demos = [];
        prev_demos = makeSelectedDemosObject();
        isDemosDefaulted = false;
    }
}

function createDemographicsList1() {
    var demo_list_item = '';
    var index = 0;
    $('#dvDemographics').css('display', 'block');
    $.each(mappingDemographics, function (key, val) {
        demo_list_item += '<li data-icon="plus" ui-tooltip="Select Filter"  ><a href="#" style="font-size: 12px;" id="' + key.replace(/ /g, '_') + '" value="' + index + '" data-options=\'' + JSON.stringify(val).replace(/'/g, "\\'") + '\' onclick="addDemographics(this);">' + key + '</a></li>';
        index++;
    });
    $('#ulDemographics').append(demo_list_item).listview('refresh');
    if (Object.keys(demo_groups).length == 0)
        $('#dvDemographics').css('display', 'none');
}

function getDisplayDemographicName(key) {
    var temp_key = "";
    temp_key = key;
    return temp_key.replace('Childrens Age - 1 Year Incr - Reseller', 'Children’s Age')
             .replace('Income - Estimated Household', 'Household Estimated Income')
             .replace('Home Property Type Detail Rp', 'Home Property Type')
             .replace('Roll-Up Code - E-Tech', 'Ethnicity')
             .replace("Childrens Age Ranges Present in Hhld - Reseller", "Children's Age Ranges in Household")
             .replace("Adult Age Ranges Present in Household", "Adult Age Ranges in Household")
             .replace("Age In Two-Year Incr - Input Ind - Reseller", "Two Year Age Range")
             .replace("Presence Of Children - Reseller", "Presence Of Children");
}

function addDemographics(ctrl) {
    var options = $(ctrl).data("options");
    var item_to_add = $(ctrl).attr('id').replace(/_/g, ' ');
    var item_index = $(ctrl).attr('index');
    createAndOpenDemographicsPopup(options, item_to_add, ctrl)
}

function createSelectedDemographics() {
    var selected_demo_items = "";
    var index = 0;

    $.each(selectedDemographics, function (key, val) {
        if (index == 0)
            selected_demo_items = '<li data-role="list-divider" style="font-size: 12px;" value="0" data-theme="g">Selected Demographic Filters</li>';

        var selected_options = "";
        var demo_type = '';
        var is_pre_select_demo = false;
        if (Object.keys(val).length > 0) {
            $.each(val.values, function (a, b) {
                //if (b.selector != "" && b != true) selected_options += ((selected_options != "") ? ', '+ a : a);
                //if (b == true && selected_options != "")
                if (a.indexOf('Default Demographic') > -1 && (b == true))
                    is_pre_select_demo = true;

                if (b.selector != "") selected_options += ((selected_options != "") ? ((a.indexOf('Default Demographic') > -1 && (b == true)) ? ', ' + a : ((a.indexOf('Default Demographic') == -1) ? ', ' + a : '')) : a);

                if (demo_type == '' && b.selector != "" && b.selector == "in")
                    //demo_type = 'Target - ';
                    demo_type = '';
                else if (demo_type == "" && b.selector != "" && b.selector == "not in")
                    //demo_type = 'Remove - ';
                    demo_type = 'Exclude : ';
            });

        }

        if (((jobCustomerNumber == AAG_CUSTOMER_NUMBER) && appPrivileges.roleName == "power") || appPrivileges.roleName == "admin" || !is_pre_select_demo)
            selected_demo_items += '<li data-icon="delete" data-theme="c"><a href="#" data-rel="popup" id="' + key.replace(/ /g, '_') + '" value="' + index + '" onclick="populateEditDemographics(this)"  data-options=\'' + JSON.stringify(val).replace(/'/g, "\\'") + '\'>';
        else
            selected_demo_items += '<li data-icon="false" data-theme="c"><a href="#" data-rel="popup" id="' + key.replace(/ /g, '_') + '" value="' + index + '" data-options=\'' + JSON.stringify(val).replace(/'/g, "\\'") + '\'>';

        selected_demo_items += '<h3 title="' + getDisplayDemographicName(key) + '"style="font-size: 14px;">' + getDisplayDemographicName(key) + '</h3>';


        selected_demo_items += '<p title="' + selected_options + '" class="word_wrap">' + demo_type + selected_options + '</p></a>';

        if (((jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName == "power")) || (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) || ((jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin") || !is_pre_select_demo)
            selected_demo_items += '<a href="#" onclick="getRemoveFilterConfirmation(\'' + key + '\');" title="Remove Filter">Remove Filter</a></li>';

        index++;
    });
    if (selected_demo_items == "") {
        selected_demo_items = '<li data-role="list-divider" style="font-size: 12px;" value="0">Selected Demographic Filters</li>';
        selected_demo_items += '<li style="font-size: 12px;" value="0">No Selections</li>';
        $('#btnPieChart').addClass('ui-disabled');
        $('#btnPieChart')[0].disabled = true;
    }
    window.setTimeout(function makeDelaySelected() {
        $('#ulSelectedDemographics').empty();
        $('#ulSelectedDemographics').append(selected_demo_items).listview('refresh');
    }, 100);

    $('#dvSelectedDemographics').css('display', 'block');
}

function getRemoveFilterConfirmation(key) {
    $('#confirmMsg').html('Are you sure you want to remove this demographic filter?');
    $('#okButConfirm').text('Continue');
    $('#okButConfirm').attr('onclick', 'removeSelectedDemographic(\'' + key + '\')');
    $('#popupConfirmDialog').popup('open');
}
var min_val = '', max_val = '', step = '', default_min = '', default_max = '';
function populateEditDemographics(ctrl) {
    var item_name = $(ctrl).attr('id').replace(/_/g, ' ');
    var options = $(ctrl).data("options");
    createAndOpenDemographicsPopup(options, item_name, ctrl)
}

function setRangeSliderLimitations(item_name, radio_val, is_update) {
    $('#dvDemoSlider').css('opacity', '1.0');
    if (inputMinNumber && inputMaxNumber) {
        inputMinNumber.disabled = false;
        inputMaxNumber.disabled = false;
    }
    $('#dvDemoSliderM').css('opacity', '1.0');
    if (inputMinMNumber && inputMaxMNumber) {
        inputMinMNumber.disabled = false;
        inputMaxMNumber.disabled = false;
    }
    $('#dvDemoSliderU').css('opacity', '1.0');
    if (inputMinUNumber && inputMaxUNumber) {
        inputMinUNumber.disabled = false;
        inputMaxUNumber.disabled = false;
    }
    switch (item_name) {
        case "Two Year Age Range":
        case "Age In Two-Year Incr - Input Ind - Reseller":
            min_val = '2';
            max_val = '99';
            step = '2';
            default_min = '34';
            default_max = '99';
            $('#spnSldrtext').html('<b>Note:</b> A selection below 18 will include all values less than 18.');
            $('#dvDemoSlider').css('display', 'block');
            $('#spnSldrHeading').css('display', 'block');
            $('#dvDemoSliderM').css('display', 'none');
            $('#dvDemoSliderU').css('display', 'none');
            $('#dvLblF,#dvLblM,#dvLblU').css('display', 'none');
            $('#dvDemoSlider div.ui-block-c').css('width', '60%');
            break;
        case "Income - Estimated Household":
            min_val = '5000';
            max_val = '125000';
            step = '5000';
            default_min = '75000';
            default_max = '125000';
            $('#spnSldrtext').html('<b>Note:</b> A selection below $15,000 will include all values less than $15,000 <br/> and a selection of $125,000 will include all values greater that $125,000.');
            $('#dvDemoSlider').css('display', 'block');
            $('#spnSldrHeading').css('display', 'block');
            $('#dvDemoSliderM').css('display', 'none');
            $('#dvDemoSliderU').css('display', 'none');
            $('#dvLblF,#dvLblM,#dvLblU').css('display', 'none');
            $('#dvDemoSlider div.ui-block-c').css('width', '60%');
            break;
        case "Childrens Age - 1 Year Incr - Reseller":
            min_val = '0';
            max_val = '18';
            default_min = min_val;
            default_max = max_val;
            step = '1';
            $('#spnSldrtext').html('');
            $('#dvDemoSlider').css('display', 'block');
            $('#spnSldrHeading').css('display', 'block');
            $('#dvDemoSliderM').css('display', 'none');
            $('#dvDemoSliderU').css('display', 'none');
            $('#dvLblF,#dvLblM,#dvLblU').css('display', 'none');
            $('#dvDemoSlider div.ui-block-c').css('width', '60%');
            break;
        case "Home Market Value - Estimated":
            min_val = '1000';
            max_val = '1000000';
            default_min = min_val;
            default_max = max_val;
            step = '25000';
            $('#spnSldrtext').html('<b>Note:</b> A selection of $1,000,000 will include all values greater that $1,000,000.');
            $('#dvDemoSlider').css('display', 'block');
            $('#spnSldrHeading').css('display', 'block');
            $('#dvDemoSliderM').css('display', 'none');
            $('#dvDemoSliderU').css('display', 'none');
            $('#dvLblF,#dvLblM,#dvLblU').css('display', 'none');
            $('#dvDemoSlider div.ui-block-c').css('width', '60%');
            break;
        case "Adult Age Ranges Present in Household":
            $('#dvDemoSlider').css('display', 'block');
            $('#dvDemoSliderM').css('display', 'block');
            $('#dvDemoSliderU').css('display', 'block');
            $('#spnSldrtext').html('<b>Note:</b> A selection of 75 will include all values above 75.');
            $('#dvLblF,#dvLblM,#dvLblU').css('display', 'block');
            $('#dvDemoSlider div.ui-block-c').css('width', '56%');
            min_val = '18', max_val = '75', step = '1', default_min = min_val, default_max = max_val;
            break;
        case "Childrens Age Ranges Present in Hhld - Reseller":
            $('#dvDemoSlider').css('display', 'block');
            $('#dvDemoSliderM').css('display', 'block');
            $('#dvDemoSliderU').css('display', 'block');
            $('#spnSldrtext').html('');
            $('#dvLblF,#dvLblM,#dvLblU').css('display', 'block');
            $('#dvDemoSlider div.ui-block-c').css('width', '56%');
            min_val = '0', max_val = '17', step = '1', default_min = min_val, default_max = max_val;
            break;
        default:
            $('#dvDemoSlider').css('display', 'none');
            $('#dvDemoSliderM').css('display', 'none');
            $('#dvDemoSliderU').css('display', 'none');
            $('#spnSldrHeading').css('display', 'none');
            $('#dvLblF,#dvLblM,#dvLblU').css('display', 'none');
            $('#dvDemoSlider div.ui-block-c').css('width', '60%');
            $('#spnSldrtext').html('');
            min_val = '', max_val = '', step = '', default_min = '', default_max = '';
            break;
    }

    if (min_val != '') {
        var slider = document.getElementById('sldr');
        var sliderM = document.getElementById('sldrM');
        var sliderU = document.getElementById('sldrU');
        if (slider.noUiSlider)
            slider.noUiSlider.destroy();
        if (sliderM.noUiSlider)
            sliderM.noUiSlider.destroy();
        if (sliderU.noUiSlider)
            sliderU.noUiSlider.destroy();
        noUiSlider.create(slider, {
            start: [parseInt(default_min), parseInt(default_max)],
            connect: true,
            step: parseInt(step),
            range: {
                'min': parseInt(min_val),
                //'10%': [parseInt(min_val), parseInt(step)],
                //'50%': [parseInt(max_val), parseInt(step)],
                'max': parseInt(max_val)
            }
        });
        if (item_name == "Adult Age Ranges Present in Household" || item_name == "Childrens Age Ranges Present in Hhld - Reseller") {
            noUiSlider.create(sliderM, {
                start: [parseInt(default_min), parseInt(default_max)],
                connect: true,
                step: parseInt(step),
                range: {
                    'min': parseInt(min_val),
                    //'10%': [parseInt(min_val), parseInt(step)],
                    //'50%': [parseInt(max_val), parseInt(step)],
                    'max': parseInt(max_val)
                }
            });

            inputMinMNumber = document.getElementById('sldrMinM');
            inputMaxMNumber = document.getElementById('sldrMaxM');
            inputMinMNumber.addEventListener('change', function () {
                this.value = this.value.replace(/[^0-9-,]+/g, '')
                if (parseInt(this.value.replaceAll(',', '')) < parseInt(min_val) || parseInt(this.value.replaceAll(',', '')) > max_val || this.value == "") this.value = min_val;
                if (validateAndEnableRange(this.value.replace(/,/g, ''), inputMaxMNumber.value.replace(/,/g, ''), 'M')) {
                    min_max_change = true;
                    sliderM.noUiSlider.set([this.value.replace(/,/g, ''), null]);
                }
            });
            inputMaxMNumber.addEventListener('change', function () {
                this.value = this.value.replace(/[^0-9-,]+/g, '')
                if (parseInt(this.value.replaceAll(',', '')) < parseInt(min_val) || parseInt(this.value.replaceAll(',', '')) > max_val || this.value == "") this.value = max_val;
                if (validateAndEnableRange(inputMinMNumber.value.replace(/,/g, ''), this.value.replace(/,/g, ''), 'M')) {
                    min_max_change = true;
                    sliderM.noUiSlider.set([null, this.value.replace(/,/g, '')]);
                }
            });

            sliderM.noUiSlider.on('update', function (values, handle) {
                if ($('#dvDemoSliderM').css('opacity') != '0.5') {
                    if (!min_max_change) {
                        inputMinMNumber.value = (parseInt(values[0])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        inputMaxMNumber.value = (parseInt(values[1])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    }
                }
                else {
                    validateAndEnableRange(inputMinMNumber.value.replaceAll(',', ''), inputMaxMNumber.value.replaceAll(',', ''), 'M');
                }
            });
            sliderM.noUiSlider.on('set', function (values, handle) {
                if ($('#dvDemoSliderM').css('opacity') != '0.5') {
                    var is_household_income = ($('#spnDemoName').text() == "Income - Estimated Household" || $('#spnDemoName').text() == "Home Market Value - Estimated" || $('#spnDemoName').text() == "Age In Two-Year Incr - Input Ind - Reseller") ? "true" : "false";
                    if (values[0] != inputMinMNumber.value.replace(/,/g, '') + ".00" && JSON.parse(is_household_income) && min_max_change) values[0] = inputMinMNumber.value.replace(/,/g, '') + ".00";//values[0] = (values[0] - 1) + ".00";
                    if (values[1] != inputMaxMNumber.value.replace(/,/g, '') + ".00" && JSON.parse(is_household_income) && min_max_change) values[1] = inputMaxMNumber.value.replace(/,/g, '') + ".00"; //values[1] = (values[1] - 1) + ".00";
                    inputMinMNumber.value = (parseInt(values[0])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    inputMaxMNumber.value = (parseInt(values[1])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    updateDemoItemsOnSliderChange(values[0].replace(/,/g, ''), values[1].replace(/,/g, ''), 'M');
                }
                window.setTimeout(function () { min_max_change = false; }, 500);
            });

            noUiSlider.create(sliderU, {
                start: [parseInt(default_min), parseInt(default_max)],
                connect: true,
                step: parseInt(step),
                range: {
                    'min': parseInt(min_val),
                    //'10%': [parseInt(min_val), parseInt(step)],
                    //'50%': [parseInt(max_val), parseInt(step)],
                    'max': parseInt(max_val)
                }
            });
            inputMinUNumber = document.getElementById('sldrMinU');
            inputMaxUNumber = document.getElementById('sldrMaxU');
            inputMinUNumber.addEventListener('change', function () {
                this.value = this.value.replace(/[^0-9-,]+/g, '')
                if (parseInt(this.value.replaceAll(',', '')) < parseInt(min_val) || parseInt(this.value.replaceAll(',', '')) > max_val || this.value == "") this.value = min_val;
                if (validateAndEnableRange(this.value.replace(/,/g, ''), inputMaxUNumber.value.replace(/,/g, ''), 'U')) {
                    min_max_change = true;
                    sliderU.noUiSlider.set([this.value.replace(/,/g, ''), null]);
                }
            });
            inputMaxUNumber.addEventListener('change', function () {
                this.value = this.value.replace(/[^0-9-,]+/g, '')
                if (parseInt(this.value.replaceAll(',', '')) < parseInt(min_val) || parseInt(this.value.replaceAll(',', '')) > max_val || this.value == "") this.value = max_val;
                if (validateAndEnableRange(inputMinUNumber.value.replace(/,/g, ''), this.value.replace(/,/g, ''), 'U')) {
                    min_max_change = true;
                    sliderU.noUiSlider.set([null, this.value.replace(/,/g, '')]);
                }
            });

            sliderU.noUiSlider.on('update', function (values, handle) {
                if ($('#dvDemoSliderU').css('opacity') != '0.5') {
                    if (!min_max_change) {
                        inputMinUNumber.value = (parseInt(values[0])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        inputMaxUNumber.value = (parseInt(values[1])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    }
                }
                else {
                    validateAndEnableRange(inputMinUNumber.value.replaceAll(',', ''), inputMaxUNumber.value.replaceAll(',', ''), 'U');
                }
            });
            sliderU.noUiSlider.on('set', function (values, handle) {
                if ($('#dvDemoSliderU').css('opacity') != '0.5') {
                    var is_household_income = ($('#spnDemoName').text() == "Income - Estimated Household" || $('#spnDemoName').text() == "Home Market Value - Estimated" || $('#spnDemoName').text() == "Age In Two-Year Incr - Input Ind - Reseller") ? "true" : "false";
                    if (values[0] != inputMinUNumber.value.replace(/,/g, '') + ".00" && JSON.parse(is_household_income) && min_max_change) values[0] = inputMinUNumber.value.replace(/,/g, '') + ".00";//values[0] = (values[0] - 1) + ".00";
                    if (values[1] != inputMaxUNumber.value.replace(/,/g, '') + ".00" && JSON.parse(is_household_income) && min_max_change) values[1] = inputMaxUNumber.value.replace(/,/g, '') + ".00"; //values[1] = (values[1] - 1) + ".00";
                    inputMinUNumber.value = (parseInt(values[0])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    inputMaxUNumber.value = (parseInt(values[1])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    updateDemoItemsOnSliderChange(values[0].replace(/,/g, ''), values[1].replace(/,/g, ''), 'U');
                }
                window.setTimeout(function () { min_max_change = false; }, 500);
            });
        }
        inputMinNumber = document.getElementById('sldrMin');
        inputMaxNumber = document.getElementById('sldrMax');
        inputMinNumber.addEventListener('change', function () {
            this.value = this.value.replace(/[^0-9-,]+/g, '')
            if (parseInt(this.value.replaceAll(',', '')) < parseInt(min_val) || parseInt(this.value.replaceAll(',', '')) > max_val || this.value == "") this.value = min_val;
            if (validateAndEnableRange(this.value.replace(/,/g, ''), inputMaxNumber.value.replace(/,/g, ''), '')) {
                min_max_change = true;
                slider.noUiSlider.set([this.value.replace(/,/g, ''), null]);
            }
        });
        inputMaxNumber.addEventListener('change', function () {
            this.value = this.value.replace(/[^0-9-,]+/g, '')
            if (parseInt(this.value.replaceAll(',', '')) < parseInt(min_val) || parseInt(this.value.replaceAll(',', '')) > max_val || this.value == "") this.value = max_val;
            if (validateAndEnableRange(inputMinNumber.value.replace(/,/g, ''), this.value.replace(/,/g, ''), '')) {
                min_max_change = true;
                slider.noUiSlider.set([null, this.value.replace(/,/g, '')]);
            }
        });
        slider.noUiSlider.on('update', function (values, handle) {
            if ($('#dvDemoSlider').css('opacity') != '0.5') {
                if (!min_max_change) {
                    inputMinNumber.value = (parseInt(values[0])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    inputMaxNumber.value = (parseInt(values[1])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            }
            else {
                validateAndEnableRange(inputMinNumber.value.replaceAll(',', ''), inputMaxNumber.value.replaceAll(',', ''), '');
            }
        });
        slider.noUiSlider.on('set', function (values, handle) {
            if ($('#dvDemoSlider').css('opacity') != '0.5') {
                var is_household_income = ($('#spnDemoName').text() == "Income - Estimated Household" || $('#spnDemoName').text() == "Home Market Value - Estimated" || $('#spnDemoName').text() == "Age In Two-Year Incr - Input Ind - Reseller") ? "true" : "false";
                if (values[0] != inputMinNumber.value.replace(/,/g, '') + ".00" && JSON.parse(is_household_income) && min_max_change) values[0] = inputMinNumber.value.replace(/,/g, '') + ".00";//values[0] = (values[0] - 1) + ".00";
                if (values[1] != inputMaxNumber.value.replace(/,/g, '') + ".00" && JSON.parse(is_household_income) && min_max_change) values[1] = inputMaxNumber.value.replace(/,/g, '') + ".00"; //values[1] = (values[1] - 1) + ".00";
                inputMinNumber.value = (parseInt(values[0])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                inputMaxNumber.value = (parseInt(values[1])).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                updateDemoItemsOnSliderChange(values[0].replace(/,/g, ''), values[1].replace(/,/g, ''), '');
            }
            window.setTimeout(function () { min_max_change = false; }, 500);
        });
        //slider.noUiSlider.on('slide', function (values, handle) {
        //});
    }
}

function validateAndEnableRange(min_val, max_val, src) {
    var demo_ctrl;
    var selected_demo = $('#spnDemoName').text();
    if ($('#dvDemoSlider' + src).css('opacity') == '0.5') {
        var error_msg = 'Selecting a range will remove any selections made that are outside the range or that have been removed from the range.';
        $('#popupDemos').popup('close');
        $('#confirmMsg').html(error_msg);
        $('#okButConfirm').text('Continue');
        $('#cancelButConfirm').text('Cancel');
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            //cancelSelections();
            $('#popupConfirmDialog').popup('close');
            window.setTimeout(function () {
                $('#popupDemos').popup('open');
            }, 300);
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            $('#okButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
            $('#dvDemoSlider' + src).css('opacity', '1.0');
            if (src == "M") {
                if (inputMinMNumber && inputMaxMNumber) {
                    inputMinMNumber.disabled = false;
                    inputMaxMNumber.disabled = false;
                }
            }
            else if (src == 'U') {
                if (inputMinUNumber && inputMaxUNumber) {
                    inputMinUNumber.disabled = false;
                    inputMaxUNumber.disabled = false;
                }
            }
            else {
                if (inputMinNumber && inputMaxNumber) {
                    inputMinNumber.disabled = false;
                    inputMaxNumber.disabled = false;
                }
            }
            window.setTimeout(function () {
                $('#popupDemos').popup('open');
            }, 300);
            window.setTimeout(function getDelay() {
                min_max_change = true;
                var slider = document.getElementById('sldr' + src);
                if (slider.noUiSlider) slider.noUiSlider.set([parseInt(min_val), parseInt(max_val)]);
            }, 300);
        });
        $('#popupConfirmDialog').popup('open');
        return false;
    }
    return true;
}

function createAndOpenDemographicsPopup(options, item_name, ctrl) {
    var popup_options = "";
    //item_name = getDisplayDemographicName(item_name);   
    var is_update_mode = false;
    $('#spnDemoName').text(item_name);
    var display_name = getDisplayDemographicName(item_name);
    var br_line = (display_name.split(' ').length > 6) ? '<br />' : ' ';
    popup_options = "";//'<h3>' + item_name + br_line + 'Demo Settings</h3>';
    $('#dvHeading').html(display_name + br_line + 'Settings');
    if (Object.keys(options.values).length > 0)
        popup_options += '<fieldset data-role="controlgroup" data-mini="true">';
    //if (Object.keys(options.values).length > 5)
    //popup_options += '<div data-role="controlgroup" class="ui-corner-all" data-iscroll="true" style="overflow-y:scroll;height:155px;padding:5px" data-theme="c">';
    popup_options += '<div class="ui-corner-all" data-theme="c">';
    var index = 0;
    var default_demo = '';
    var radio_value = '';
    var is_default_checked = false;
    $.each(options.values, function (a, b) {
        if (a.indexOf('Default Demographic') > -1) {
            if (((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && appPrivileges.roleName == "power") || appPrivileges.roleName == "admin") {
                var is_checked = (b) ? 'checked="checked"' : "";
                default_demo = '<fieldset data-role="controlgroup" data-mini="true" data-theme="a">';
                default_demo += '<input data-theme="c" type="checkbox" name="chkDefault_Demographic" id="chkDefault_Demographic" ' + is_checked + '"><label for="chkDefault_Demographic">Set As A Default Demo for Client<br/><span style="font-size: 12px; font-weight: normal;"> (Applies to all jobs for current client)</span></label>';
                default_demo += '</fieldset>';
            }
        }
        else {
            //$.each(d, function (a, b) {
            if (typeof (b) == "object" && Object.keys(b).length > 0) {
                if (b.selector != "") radio_value = b.selector;
                var is_checked = (b.selector != "") ? 'checked' : "";
                if (is_checked == 'checked') {
                    is_update_mode = true;
                }

                //            if (item_name == "Female Present") {
                //                popup_options += '<input type="radio" name="rdo' + item_name.replace(/ /, '_') + '" id="rdo' + a.replace(/ /, '_') + '" value="' + b.value + '" ' + is_checked + ' data-mini="true"/>';
                //                popup_options += '<label for="rdo' + a.replace(/ /, '_') + '">' + a + '</label>';
                //            }
                //            else {
                //                popup_options += '<input type="checkbox" name="chk' + b.valueDescription.replace(/ /, '_') + '" id="chk' + b.valueDescription.replace(/ /, '_') + '" value="' + b.value + '" ' + is_checked + ' data-mini="true"/>';
                //                popup_options += '<label for="chk' + b.valueDescription.replace(/ /, '_') + '">' + b.valueDescription + '</label>';
                //            }
                if (options.menuType == "checkbox" || options.menuType == "multiselect") {//
                    popup_options += '<input data-theme="c" type="checkbox" name="chk' + b.valueDescription.replace(/ /, '_') + '" id="chk' + b.valueDescription.replace(/ /, '_') + '" value="' + b.value + '" ' + is_checked + ' data-mini="true" onchange="validateSldr(this);"/>';
                    popup_options += '<label for="chk' + b.valueDescription.replace(/ /, '_') + '">' + b.valueDescription + '</label>';
                }
                else if (options.menuType == "multiselect") {
                    if (index == 0)
                        popup_options += '<select name="ddl' + b.valueDescription.replace(/ /, '_') + '" id="ddl' + b.valueDescription.replace(/ /, '_') + '" data-theme="c" data-mini="true" multiselect="multiselect" data-native-menu="false">';

                    popup_options += '<option value="' + b.value + '">' + b.valueDescription + '</option>';
                    if ((index + 1) == Object.keys(options.values).length)
                        popup_options += '</select>';
                }
                else if (options.menuType == "radio") {

                }

            }
            if ((index + 1) < Object.keys(options.values).length)
                index++;
            //});
        }
    });
    //if (Object.keys(options.values).length > 5)
    popup_options += '</div>';
    popup_options += '</fieldset>';

    var include_exclude_options = '<fieldset data-role="controlgroup" data-mini="true" data-theme="c">';
    include_exclude_options += '<input type="radio" name="rdoIncludeExcludes" id="rdoIncludeSelected" value="in" onchange="changeSliderColor(this);"><label for="rdoIncludeSelected">Include Selected</label>';
    include_exclude_options += '<input type="radio" name="rdoIncludeExcludes" id="rdoExcludeSelected" value="not in" onchange="changeSliderColor(this);"><label for="rdoExcludeSelected">Exclude Selected</label>';
    include_exclude_options += '</fieldset>';

    //popup_options += include_exclude_options;
    //if (default_demo != '')popup_options += default_demo;

    $('#dvDemoOptions').empty();
    $('#dvDemoOptions').append(popup_options);
    if (Object.keys(options.values).length > 5)
        $('#dvDemoOptions').css('height', '155px');
    else
        $('#dvDemoOptions').css('height', 'auto');
    $('#dvIncludeExclude').empty();
    $('#dvIncludeExclude').append(include_exclude_options);
    $('#dvDefaultDemo').empty();
    if (default_demo != '') {
        $('#dvDefaultDemo').append(default_demo);
    }
    $('#btnPopUpDemoSubmit').attr('onclick', 'submitDemograpghicOptions(\'' + item_name + '\');');
    $('#btnPopUpDemoSubmit').data('options', options);

    setRangeSliderLimitations(item_name, radio_value, is_update_mode);
    $('#dvDemoOptions').trigger('create');
    $('#dvIncludeExclude').trigger('create');
    $('#dvDefaultDemo').trigger('create');

    //if (radio_value == "") radio_value = "in";
    //$("input[name=rdoIncludeExcludes][value='" + radio_value + "']").attr('checked', 'checked').checkboxradio('refresh');

    $('#popupDemos').popup('open');
    $('#dvDemoSlider').trigger('change');
    $('#ulSelectedDemographics').listview('refresh');
    if (radio_value == "") radio_value = "in";
    $("input[name=rdoIncludeExcludes][value='" + radio_value + "']").attr('checked', 'checked').checkboxradio('refresh').trigger('change');
    var option_type = 'include';

    // if (!is_update_mode) {
    min_max_change = true;
    var selected_items;
    if ($('#spnDemoName').text() != "Adult Age Ranges Present in Household" && $('#spnDemoName').text() != "Childrens Age Ranges Present in Hhld - Reseller") {
        var slider = document.getElementById('sldr');
        selected_items = $('#popupDemos input[type=checkbox]:checked');
        if (selected_items.length > 0)
            validateSldr(selected_items[0]);
        else
            if (slider.noUiSlider) slider.noUiSlider.set([parseInt(inputMinNumber.value.replace(/,/g, '')), parseInt(inputMaxNumber.value.replace(/,/g, ''))]);
    } else if ($('#spnDemoName').text() == "Adult Age Ranges Present in Household" || $('#spnDemoName').text() == "Childrens Age Ranges Present in Hhld - Reseller") {
        var slider = document.getElementById('sldr');
        selected_items = $('#popupDemos input[type=checkbox][id*="F"]:checked');
        if (selected_items.length > 0)
            validateSldr(selected_items[0]);
        else
            if (slider.noUiSlider) slider.noUiSlider.set([parseInt(inputMinNumber.value.replace(/,/g, '')), parseInt(inputMaxNumber.value.replace(/,/g, ''))]);
        var sliderM = document.getElementById('sldrM');
        selected_items = $('#popupDemos input[type=checkbox][id*="M"]:checked');
        if (selected_items.length > 0)
            validateSldr(selected_items[0]);
        else
            if (sliderM.noUiSlider) sliderM.noUiSlider.set([parseInt(inputMinMNumber.value.replace(/,/g, '')), parseInt(inputMaxMNumber.value.replace(/,/g, ''))]);
        var sliderU = document.getElementById('sldrU');
        selected_items = $('#popupDemos input[type=checkbox][id*="U"]:checked');
        if (selected_items.length > 0)
            validateSldr(selected_items[0]);
        else
            if (sliderU.noUiSlider) sliderU.noUiSlider.set([parseInt(inputMinUNumber.value.replace(/,/g, '')), parseInt(inputMaxUNumber.value.replace(/,/g, ''))]);
    }
    // }

}

function changeSliderColor(element) {
    var selected_theme = (element.id == 'rdoIncludeSelected') ? 'd' : 'a';
    var selected_track_theme = (element.id == 'rdoIncludeSelected') ? 'a' : 'd';
    var bgTheme = $('#aBkgrnd').attr('data-theme');
    var bkgrnd = $('.ui-overlay-' + bgTheme).attr('bgColor');
    var bgTrTheme = $('#aTrackerBkgrnd').attr('data-theme');
    var trBkgrnd = $('.ui-overlay-' + bgTheme).attr('bgColor');
    $('#sldr,#sldrM,#sldrU').find('div.noUi-handle').removeClass('ui-btn-a').removeClass('ui-btn-d').removeClass('ui-btn-null').addClass('ui-btn-null');

    if (element.id == 'rdoIncludeSelected') {
        $('#sldr,#sldrM,#sldrU').find('div.noUi-base').css('background-color', 'black');
        $('#sldr,#sldrM,#sldrU').find('div.noUi-background').css('background-color', 'black');
        $('#sldr,#sldrM,#sldrU').find('div.noUi-connect').css('background-color', 'orange');
    }
    else {
        $('#sldr,#sldrM,#sldrU').find('div.noUi-base').css('background-color', 'orange');
        $('#sldr,#sldrM,#sldrU').find('div.noUi-background').css('background-color', 'orange');
        $('#sldr,#sldrM,#sldrU').find('div.noUi-connect').css('background-color', 'black');
    }
}

function validateSldr(element) {
    var id = '';
    var is_valid = false;
    var is_uncheck_found = false;
    var is_increment = false;
    var update_val = '';
    var min_value = '';
    var max_value = '';

    switch ($('#spnDemoName').text()) {
        case "Home Market Value - Estimated":
        case "Childrens Age - 1 Year Incr - Reseller":
        case "Income - Estimated Household":
        case "Age In Two-Year Incr - Input Ind - Reseller":
        case "Adult Age Ranges Present in Household":
        case "Childrens Age Ranges Present in Hhld - Reseller":
            var selected_items = $('#popupDemos input[type=checkbox]');
            if ($(element).parent().text().indexOf('Females') > -1 || $(element).parent().text().indexOf('Female') > -1)
                selected_items = $('#popupDemos input[type=checkbox][id*="F"]');
            else if ($(element).parent().text().indexOf('Males') > -1 || $(element).parent().text().indexOf('Male') > -1)
                selected_items = $('#popupDemos input[type=checkbox][id*="M"]');
            else if ($(element).parent().text().indexOf('Unknown Gender') > -1)
                selected_items = $('#popupDemos input[type=checkbox][id*="U"]');

            if (selected_items.length > 0) {
                for (i = 0; i < selected_items.length; i++) {
                    if (selected_items[i].checked) {
                        is_valid = (is_valid && is_uncheck_found) ? false : true;
                        if (is_uncheck_found) {
                            break;
                        }
                    }
                    else {
                        is_uncheck_found = is_valid ? true : false;
                    }
                }
                if (is_valid) {

                    selected_items = $('#popupDemos input[type=checkbox]:checked');
                    if ($(element).parent().text().indexOf('Females') > -1 || $(element).parent().text().indexOf('Female') > -1)
                        selected_items = $('#popupDemos input[type=checkbox][id*="F"]:checked');
                    else if ($(element).parent().text().indexOf('Males') > -1 || $(element).parent().text().indexOf('Male') > -1)
                        selected_items = $('#popupDemos input[type=checkbox][id*="M"]:checked');
                    else if ($(element).parent().text().indexOf('Unknown Gender') > -1)
                        selected_items = $('#popupDemos input[type=checkbox][id*="U"]:checked');

                    if ($(element).parent().text().indexOf('Males') > -1 || $(element).parent().text().indexOf('Male') > -1) {
                        $('#dvDemoSliderM').css('opacity', '1.0');
                        if (inputMinMNumber && inputMaxMNumber) {
                            inputMinMNumber.disabled = false;
                            inputMaxMNumber.disabled = false;
                        }
                    }
                    else if ($(element).parent().text().indexOf('Unknown Gender') > -1) {
                        $('#dvDemoSliderU').css('opacity', '1.0');
                        if (inputMinUNumber && inputMaxUNumber) {
                            inputMinUNumber.disabled = false;
                            inputMaxUNumber.disabled = false;
                        }
                    }
                    else {
                        $('#dvDemoSlider').css('opacity', '1.0');
                        if (inputMinNumber && inputMaxNumber) {
                            inputMinNumber.disabled = false;
                            inputMaxNumber.disabled = false;
                        }
                    }

                    for (i = 0; i < selected_items.length; i++) {
                        if (selected_items[i].checked) {
                            if (min_value == '') {
                                min_value = selected_items[i].defaultValue;
                                if ($('#spnDemoName').text() == "Home Market Value - Estimated" || $('#spnDemoName').text() == "Income - Estimated Household" || $('#spnDemoName').text() == "Age In Two-Year Incr - Input Ind - Reseller" || $('#spnDemoName').text() == "Adult Age Ranges Present in Household" || $('#spnDemoName').text() == "Childrens Age Ranges Present in Hhld - Reseller") {
                                    min_value = $('label[for="' + selected_items[i].id + '"]').text();
                                    if (min_value.indexOf('-') > -1) {
                                        min_value = min_value.toLowerCase().split('-')[0].replace('$', '').replace('age', '').replace('females', '').replace('female', '').replace('males', '').replace('male', '').replace('unknown gender', '').replaceAll(',', '');
                                    }
                                    else {
                                        if (min_value.toLowerCase().indexOf('less than') > -1) {
                                            min_value = min_val;
                                        }
                                        else {
                                            if (min_value.toLowerCase().indexOf('plus') > -1 || min_value.toLowerCase().indexOf('and up') > -1) {
                                                min_value = min_value.substring(min_value.indexOf('$')).replace('Plus', '').replace('and up', '').replace('$', '').replaceAll(',', '');
                                            }
                                            else {
                                                min_value = min_value.substring(min_value.indexOf('$')).replace('$', '').replaceAll(',', '');
                                            }
                                        }
                                    }
                                    min_value = min_value.trim();
                                }
                            }
                            if (i <= (selected_items.length - 1) && ((i == (selected_items.length - 1)) || !selected_items[i + 1].checked)) {
                                max_value = selected_items[i].defaultValue;
                                if ($('#spnDemoName').text() == "Home Market Value - Estimated" || $('#spnDemoName').text() == "Income - Estimated Household" || $('#spnDemoName').text() == "Age In Two-Year Incr - Input Ind - Reseller" || $('#spnDemoName').text() == "Adult Age Ranges Present in Household" || $('#spnDemoName').text() == "Childrens Age Ranges Present in Hhld - Reseller") {
                                    max_value = $('label[for="' + selected_items[i].id + '"]').text();
                                    if (max_value.indexOf('-') > -1) {
                                        max_value = max_value.toLowerCase().split('-')[1].replace('$', '').replace('age', '').replace('females', '').replace('female', '').replace('males', '').replace('male', '').replace('unknown gender', '').replaceAll(',', '');
                                    }
                                    else {
                                        if (max_value.toLowerCase().indexOf('plus') > -1 || max_value.toLowerCase().indexOf('greater than') > -1 || max_value.toLowerCase().indexOf('and up') > -1) {
                                            max_value = max_value.substring(max_value.indexOf('$')).replace('Plus', '').replace('and up', '').replace('$', '').replaceAll(',', '');
                                            max_value = max_val;
                                        }
                                        else if (max_value.toLowerCase().indexOf('less than') > -1) {
                                            max_value = min_val;
                                        }
                                        else {
                                            max_value = max_value.substring(max_value.indexOf('$')).replace('$', '').replaceAll(',', '');
                                        }
                                    }
                                    max_value = max_value.trim();
                                }
                            }
                            if (min_value != '' && max_value != '') {
                                break;
                            }
                        }
                    }
                }
            }
            if (!is_valid && !(element.checked && $(element).parent().text() == "unknown" && $('#spnDemoName').text() == "Adult Age Ranges Present in Household" && $('#spnDemoName').text() == "Childrens Age Ranges Present in Hhld - Reseller")) {
                if ($(element).parent().text().indexOf('Males') > -1 || $(element).parent().text().indexOf('Male') > -1) {
                    $("#dvDemoSliderM").css('opacity', '0.5');
                    inputMinMNumber.disabled = true;
                    inputMaxMNumber.disabled = true;
                }
                else if ($(element).parent().text().indexOf('Unknown Gender') > -1) {
                    $("#dvDemoSliderU").css('opacity', '0.5');
                    inputMinUNumber.disabled = true;
                    inputMaxUNumber.disabled = true;
                }
                else {
                    $("#dvDemoSlider").css('opacity', '0.5');
                    inputMinNumber.disabled = true;
                    inputMaxNumber.disabled = true;
                }
            }
            else if (!(element.checked && $(element).parent().text() == "unknown")) {
                if ($(element).parent().text().indexOf('Males') > -1 || $(element).parent().text().indexOf('Male') > -1) {
                    var slider = document.getElementById('sldrM');
                    min_max_change = true;
                    inputMinMNumber.value = (parseInt(min_value)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    inputMaxMNumber.value = (parseInt(max_value)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    if (slider.noUiSlider) slider.noUiSlider.set([parseInt(min_value), parseInt(max_value)]);
                }
                else if ($(element).parent().text().indexOf('Unknown Gender') > -1) {
                    var slider = document.getElementById('sldrU');
                    min_max_change = true;
                    inputMinUNumber.value = (parseInt(min_value)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    inputMaxUNumber.value = (parseInt(max_value)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    if (slider.noUiSlider) slider.noUiSlider.set([parseInt(min_value), parseInt(max_value)]);
                } else {
                    var slider = document.getElementById('sldr');
                    min_max_change = true;
                    inputMinNumber.value = (parseInt(min_value)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    inputMaxNumber.value = (parseInt(max_value)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    if (slider.noUiSlider) slider.noUiSlider.set([parseInt(min_value), parseInt(max_value)]);
                }
            }
            break;
    }
}

function updateDemoItemsOnSliderChange(min_value, max_value, src) {
    var tmp_min_val = 0, tmp_max_val = 0;
    var is_household_income = ($('#spnDemoName').text() == "Income - Estimated Household" || $('#spnDemoName').text() == "Home Market Value - Estimated") ? "true" : "false";
    //if (is_household_income == "true") {
    var selected_items;
    if (($('#spnDemoName').text() == "Adult Age Ranges Present in Household" || $('#spnDemoName').text() == "Childrens Age Ranges Present in Hhld - Reseller") && src == "") src = "F";
    if (src != "") {
        $('#popupDemos input[type=checkbox][id*="' + src + '"]').attr('checked', false).checkboxradio('refresh');
        selected_items = $('#popupDemos input[type=checkbox][id*="' + src + '"]')
    }
    else {
        $('#popupDemos input[type=checkbox]').attr('checked', false).checkboxradio('refresh');
        selected_items = $('#popupDemos input[type=checkbox]');
    }
    var chk_filter = ((src == "M") ? 'males' : ((src == "F") ? "females" : ((src == "U") ? 'unknown gender' : '')));
    selected_items.filter(function (a, b) {
        var myVal_range = $(this).parent().text().toLowerCase().replaceAll('$', '').replaceAll(',', '').replace('age', '').replace('less than', '').replace('greater than', '').replace('plus', '').replace(chk_filter, '').trim();//.replace('less than', '').replace('greater than', '').replace('age', '').replace('plus', '').trim();//.replace('unknown');
        var tmp_min = 0, tmp_max = 0;
        if (myVal_range.indexOf('-') > -1) {
            var demo_item_values = myVal_range.split('-');
            tmp_min = demo_item_values[0];
            tmp_max = demo_item_values[1];
            return ((parseInt(tmp_min) >= parseInt(min_value) && parseInt(tmp_min) <= parseInt(max_value)) || (parseInt(tmp_max) >= parseInt(min_value) && parseInt(tmp_max) <= parseInt(max_value)));
        }
        else {
            tmp_min = myVal_range;
            if ($(this).parent().text().toLowerCase().indexOf('less than') > -1)
                tmp_min = min_val;
            //tmp_min = myVal_range - 1;
            if ($(this).parent().text().toLowerCase().indexOf('greater than') > -1 || $(this).parent().text().toLowerCase().indexOf('plus') > -1)
                tmp_min = max_val;
            //tmp_min = myVal_range + 1;
            return ($(this).parent().text().toLowerCase().indexOf('less than') > -1 && (parseInt(tmp_min) >= parseInt(min_value) && parseInt(tmp_min) <= parseInt(max_value)) ||
                $(this).parent().text().toLowerCase().indexOf('greater than') > -1 && (parseInt(tmp_min) <= parseInt(min_value) && parseInt(tmp_min) <= parseInt(max_value)) ||
                $(this).parent().text().toLowerCase().indexOf('plus') > -1 && (parseInt(tmp_min) <= parseInt(min_value) && parseInt(tmp_min) <= parseInt(max_value)) ||
                (parseInt(tmp_min) >= parseInt(min_value) && parseInt(tmp_min) <= parseInt(max_value) ||
                                      (a == 0 && parseInt(tmp_min) >= parseInt(min_value) && parseInt(tmp_min) >= parseInt(max_value))) ||
                                      (a == (selected_items.length - 1) && parseInt(tmp_min) <= parseInt(min_value) && parseInt(tmp_min) <= parseInt(max_value)));
        }
    }).attr('checked', true).checkboxradio('refresh');
}

function validateDemosWIthCounts(item_name) {
    $('#popupDemos').popup('close');
    var peri_targets = $('#spnPheripheralTargets').text();
    if (!isDemosChanged && !$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed') && parseInt(peri_targets) > 0) {
        var msg = "Changes to the Demographics will clear all Peripheral Targets selected.";
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').bind('click', function () {
            isDemosChanged = true;
            submitDemograpghicOptions(item_name);
            $('#popupradiusZipCRRTConfirmDialog').popup('open');
            $('#popupradiusZipCRRTConfirmDialog').popup('close');
            return false;
        });
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').bind('click', function () {
            $('#popupradiusZipCRRTConfirmDialog').popup('open');
            $('#popupradiusZipCRRTConfirmDialog').popup('close');
            return false;
        });
        $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
        $('#popupradiusZipCRRTConfirmDialog').popup('open');
        return false;
    }
    else return true;
}

function submitDemograpghicOptions(item_name) {
    if (!validateDemosWIthCounts(item_name)) return false;
    var post_demographics = { "shouldExclude": "", "columnName": "", "values": "" };
    var selected_field_value = '';
    var selected_items = (item_name == "Female Present") ? $('#popupDemos input[type=radio]') : $('#popupDemos input[type=checkbox]');
    var selected_options = {};
    var is_demographics_checked = false;
    var radio_value = $('#dvIncludeExclude input[name=rdoIncludeExcludes]:checked').val();
    var checked_count = 0;
    var item = '';
    if (selected_items.length > 0) {
        $.each(selected_items, function (a, b) {
            item = (item_name == "Female Present") ? $(b).attr('id').replace(/_/g, ' ').replace('rdo', '') : $(b).attr('id').replace(/_/g, ' ').replace('chk', '');
            if (Object.keys(selectedDemographics).length > 0 && selectedDemographics[item_name] != undefined && selectedDemographics[item_name] != null && selectedDemographics[item_name].values[item] != undefined && selectedDemographics[item_name].values[item] != null) {
                if ($(b)[0].checked) {
                    selectedDemographics[item_name].values[item].selector = (radio_value != '') ? radio_value : "";
                    if (item.indexOf('Default Demographic') != -1) {
                        selectedDemographics[item_name].values[item] = $(b)[0].checked;
                        is_demographics_checked = true;
                    }
                    else {
                        post_demographics.shouldExclude = (selectedDemographics[item_name].values[item].selector != undefined && selectedDemographics[item_name].values[item].selector != '' && selectedDemographics[item_name].values[item].selector == 'not in') ? "1" : "0";
                        post_demographics.columnName = (selectedDemographics[item_name].values[item].columnName != undefined && selectedDemographics[item_name].values[item].columnName != '') ? selectedDemographics[item_name].values[item].columnName : '';
                        selected_field_value += (selectedDemographics[item_name].values[item].value != undefined && selectedDemographics[item_name].values[item].value != '') ? selectedDemographics[item_name].values[item].value + ',' : '';
                    }
                    checked_count++;
                }
                else {
                    selectedDemographics[item_name].values[item].selector = "";
                    if (item.indexOf('Default Demographic') != -1)
                        selectedDemographics[item_name].values[item] = $(b)[0].checked;
                }
            }
            else if (Object.keys(demo_groups).length > 0 && demo_groups[item_name] != undefined && demo_groups[item_name] != null && demo_groups[item_name].values[item] != undefined && demo_groups[item_name].values[item] != null)
                if ($(b)[0].checked) {
                    demo_groups[item_name].values[item].selector = (radio_value != '') ? radio_value : "";
                    if (item.indexOf('Default Demographic') > -1) {
                        demo_groups[item_name].values[item] = $(b)[0].checked;
                        is_demographics_checked = true;
                    }
                    else {
                        //if (item.indexOf('Default Demographics') == -1)
                        post_demographics.shouldExclude = (demo_groups[item_name].values[item].selector != undefined && demo_groups[item_name].values[item].selector != '' && demo_groups[item_name].values[item].selector == 'not in') ? "1" : "0";
                        post_demographics.columnName = (demo_groups[item_name].values[item].columnName != undefined && demo_groups[item_name].values[item].columnName != '') ? demo_groups[item_name].values[item].columnName : '';
                        selected_field_value += (demo_groups[item_name].values[item].value != undefined && demo_groups[item_name].values[item].value != '') ? demo_groups[item_name].values[item].value + ',' : '';
                    }
                    checked_count++;
                }
                else
                    demo_groups[item_name].values[item].selector = "";
        });
    }

    if (is_demographics_checked) {
        selected_field_value = selected_field_value.substring(0, selected_field_value.length - 1);
        post_demographics.values = selected_field_value;

        var gcust_config_data_post_url = mapsUrl + "api/ZipSelectList_demo_post/" + jobCustomerNumber;
        postCORS(gcust_config_data_post_url, JSON.stringify(post_demographics), function (data) {
            isDemosChanged = true;
        }, function (error_response) {
            showErrorResponseText(error_response, false);
            //if (!(jQuery.browser.msie)) {
            //    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
            //}
            //else {
            //    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
            //}
            //$('#popupDialog').popup('open');
        });
    }


    if (Object.keys(demo_groups).length > 0 && demo_groups[item_name] != undefined && demo_groups[item_name] != null) {
        if (checked_count > 0) {
            selectedDemographics[item_name] = demo_groups[item_name];
            delete demo_groups[item_name];
        }
    }
    else if (Object.keys(selectedDemographics).length > 0 && selectedDemographics[item_name] != undefined && selectedDemographics[item_name] != null) {
        if (checked_count == 0) {
            removeSelectedDemographic(item_name);
        }
    }


    if (checked_count > 0) {
        $('#ulDemographics').empty();
        $('#ulSelectedDemographics').empty();
        createSelectedDemographics();
        createDemographicsList();
        var temp_selected_demos = [];
        temp_selected_demos = makeSelectedDemosObject();
        var diff_demos = DiffObjects(prev_demos, temp_selected_demos);
        if (diff_demos.length > 0) {
            isDemosChanged = true;
            isMapRefreshed = false;
        }
        //$('#dvRevisedDemosZipsText').html('<span style="color:red;;font-size:13px;">Click the Refresh button to update the map and counts with your revised demographic filter selections.</span>');
        //$('#dvRevisedDemosZipsText').css('display', 'block');
        $('#divAvailable').css('color', '#484848');
        $('#divAvailable').css('font-weight', 'normal');
        $('#spnRadiusAvblCount').css('color', '#b82f1a');
        $('#spnRadiusAvblCount').css('font-weight', 'bold');
        $('#pMapRefreshMsg').css('display', 'block');
        $('#pMapRefreshMsg').css('display', '');
        $('#btnRefresh').removeClass('ui-disabled');
        $('#btnRefresh').removeClass('ui-btn-e').addClass('ui-btn-h');
        $('#btnRefresh')[0].disabled = false;
    }
    $('#popupDemos').popup('close');
}

function removeSelectedDemographic(demographic_name) {
    var item = demographic_name;
    $.each(selectedDemographics[demographic_name].values, function (a, b) {
        b.selector = "";
    });
    demo_groups[demographic_name] = selectedDemographics[demographic_name];
    delete selectedDemographics[demographic_name];
    $('#ulDemographics').empty();
    $('#ulSelectedDemographics').empty();
    $('#popupConfirmDialog').popup('close');
    isDemosChanged = true;
    isMapRefreshed = false;
    createDemographicsList();
    createSelectedDemographics();
    //$('#dvRevisedDemosZipsText').html('<span style="color:red;;font-size:13px;">Click the Refresh button to update the map and counts with your revised demographic filter selections.</span>');
    //$('#dvRevisedDemosZipsText').css('display', 'block');
    $('#btnRefresh').removeClass('ui-btn-e').addClass('ui-btn-h');
    $('#btnRefresh').removeClass('ui-disabled');
    $('#pMapRefreshMsg').css('display', 'block');
    $('#pMapRefreshMsg').css('display', '');
    $('#btnRefresh')[0].disabled = false;
}

function makeSelectedDemosObject() {
    var selected_demos = [], demo_info = {}, demo_values = [];
    if (Object.keys(selectedDemographics).length > 0) {
        var temp_demos = {};

        $.each(selectedDemographics, function (key, val) {
            temp_values = {};
            var is_default_demo = 0;
            //if (val.values["Default Demographic"] != undefined && val.values["Default Demographic"] != null && val.values["Default Demographic"] != "")
            //is_default_demo = (JSON.parse(val.values["Default Demographic"])) ? 1 : 0;
            is_default_demo = (val["isDefaultValue"]) ? JSON.parse(val["isDefaultValue"]) : 0;
            demo_info["isDefaultValue"] = is_default_demo;
            demo_info["demoName"] = key;
            var demo_selector;
            demo_selector = "";
            var demo_column;
            demo_column = "";
            $.each(val.values, function (key1, val1) {
                //if (val1.selector != "" && key1 != "Default Demographic") {
                if (val1.selector != "") {
                    //demo_info["demoSelector"] = val1.selector;
                    demo_info["shouldExclude"] = (val1.selector == "not in" ? "1" : "0");
                    if ((val1.columnName.indexOf('_') > -1 && val1.columnName.split('_').length > 2)) {
                        demo_info["demoColumn"] = val1.columnName.substring(0, val1.columnName.lastIndexOf('_') + 1) + 'column';
                        demo_values.push({
                            "demoColumnName": val1.columnName,
                            "name": key1,
                            "value": val1.value
                        });
                    } else {
                        demo_info["demoColumn"] = val1.columnName;
                        demo_values.push({
                            "name": key1,
                            "value": val1.value
                        });
                    }
                    demo_info["demoValues"] = demo_values;
                }
            });
            selected_demos.push(demo_info);
            demo_info = {};
            demo_values = [];
        });
    }
    return selected_demos;
}

function getSelectedZipsList() {
    var selected_zips = [];
    if (gData != undefined && gData != null && gData.zips != undefined && gData.zips != null) {
        $.each(gData.zips, function (zipKey, zipVal) {
            var is_selected = false;
            $.each(zipVal.crrts, function (crrtKey, crrtVal) {
                if (crrtVal.isChecked) {
                    is_selected = true;
                    return false;
                }
            });
            if (is_selected) {
                selected_zips.push({
                    "zipName": zipVal.zipCode + ' - ' + zipVal.zipName,
                    "zipCode": zipVal.zipCode
                });
            }
        });

        if (selected_zips.length > 0) {
            var map_prefs_options = '<option value="0">Off</option>';
            $.each(selected_zips, function (map_pref_key, map_pref_val) {
                map_prefs_options += '<option value="' + map_pref_val.zipCode + '">' + map_pref_val.zipName + '</option>';
            });
            $('#ddlViewByHousehold').empty();
            $('#ddlViewByHousehold').append(map_prefs_options).selectmenu('refresh');
        }
    }
}

function manageCRRTRadiusSelection(ctrl) {
    if ($('#chkCrrtRadiusSelection').is(':checked')) {
        $('#trRadiusSelectBy').css('display', 'block');
        $('#trRadiusSelectBy').removeAttr('style', '');
        $('#trRadius').css('display', 'block');
        $('#trRadius').removeAttr('style', '');
    }
    else {
        $('#trRadiusSelectBy').css('display', 'none');
        $('#trRadius').css('display', 'none');

    }
}

function manageCRCentroid(ctrl) {
    if ($(ctrl).is(':checked')) {
        //$('#chkViewCRRTCentroids')[0].disabled = false;
        $('#chkViewCRRTCentroids').parent().removeClass('ui-state-disabled');
        $('#chkViewCRRTCentroids').checkboxradio('enable');
        //$('#chkViewCRRTCentroids').removeAttr('disabled').checkboxradio('refresh');
    }
    else {
        //$('#chkViewCRRTCentroids')[0].disabled = true;
        //$('#chkViewCRRTCentroids').attr('disabled','disabled');
        $('#chkViewCRRTCentroids').checkboxradio('disable');
        $('#chkViewCRRTCentroids').attr('checked', false);
    }
    $('#chkViewCRRTCentroids').checkboxradio('refresh');
    refreshMapWithPrefs();
}

function refreshMapWithPrefs() {
    sessionStorage.mapPrefsChanged = true;
    refreshMap();
}

function createMapPreferences(is_map_target_genereated) {
    //var common_prefs = '<input type="checkbox" name="chkViewCRM" id="chkViewCRM" ' + ((is_map_target_genereated && $('#chkViewCRM')[0].checked) ? 'checked' : '') + '><label for="chkViewCRM">View Carrier Route Markers</label>';
    var common_prefs = '<input data-theme="c"  type="checkbox" name="chkViewCRB" id="chkViewCRB" ' + ((is_map_target_genereated && $('#chkViewCRB')[0].checked) ? 'checked' : '') + ' onchange="manageCRCentroid(this);"/><label for="chkViewCRB" style="margin-top:0px;margin-bottom:0px;">View Carrier Route Borders</label>';
    if (appPrivileges.roleName == "admin") {
        var is_checked = $('#chkViewCRRTCentroids').is(':checked');
        common_prefs += '<input data-theme="c" type="checkbox" name="chkViewCRRTCentroids" id="chkViewCRRTCentroids" ' + ((is_map_target_genereated && $('#chkViewCRRTCentroids')[0].checked) ? 'checked' : '') + ' ' + ((is_checked) ? '' : 'disabled') + ' onchange="refreshMapWithPrefs();"/><label for="chkViewCRRTCentroids" style="margin-top:0px;margin-bottom:0px;">View CRRT Centroids</label>';
    }
    common_prefs += '<input data-theme="c" type="checkbox" name="chkViewZCB" id="chkViewZCB" ' + ((is_map_target_genereated && $('#chkViewZCB')[0].checked) ? 'checked' : ((!is_map_target_genereated) ? 'checked' : '')) + ' onchange="refreshMapWithPrefs();"/><label for="chkViewZCB" style="margin-top:0px;margin-bottom:0px;">View Zip Code Boundaries</label>';

    if (appPrivileges.roleName == "admin") {
        common_prefs += '<input data-theme="c" type="checkbox" name="chkViewZipCentroids" id="chkViewZipCentroids" ' + ((is_map_target_genereated && $('#chkViewZipCentroids')[0].checked) ? 'checked' : '') + ' onchange="refreshMapWithPrefs();"/><label for="chkViewZipCentroids" style="margin-top:0px;margin-bottom:0px;">View Zip Centroids</label>';
    }

    common_prefs += '<input data-theme="c" type="checkbox" name="chkViewRadius" id="chkViewRadius" ' + ((is_map_target_genereated && $('#chkViewRadius')[0].checked) ? 'checked' : ((!is_map_target_genereated) ? 'checked' : '')) + ' onchange="refreshMapWithPrefs();"/><label for="chkViewRadius" style="margin-top:0px;margin-bottom:0px;">View Radius/Polygon</label>';
    //common_prefs += '<input type="checkbox" name="chkViewNLM" id="chkViewNLM" ' + ((is_map_target_genereated && $('#chkViewNLM')[0].checked) ? 'checked' : ((!is_map_target_genereated) ? 'checked' : '')) + '><label for="chkViewNLM">View Neighboring Location Markers</label>';


    //if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER)
        common_prefs += '<input data-theme="c" type="checkbox" name="chkViewSurroundingLocations" id="chkViewSurroundingLocations" ' + ((is_map_target_genereated && $('#chkViewZCB')[0].checked) ? 'checked' : ((!is_map_target_genereated) ? 'checked' : '')) + ' onchange="refreshMapWithPrefs();"/><label for="chkViewSurroundingLocations" style="margin-top:0px;margin-bottom:0px;">View Surrounding Location Markers</label>';
    //common_prefs += '<input data-theme="c" type="checkbox" name="chkViewSurroundingLocations" id="chkViewSurroundingLocations" ' + ((is_map_target_genereated && $('#chkViewSurroundingLocations')[0].checked) ? 'checked' : '') + ' onchange="refreshMapWithPrefs();"/><label for="chkViewSurroundingLocations" style="margin-top:0px;margin-bottom:0px;">View Surrounding Location Markers</label>';
    //

    //$('#popupMapPrefs').prepend('<a id="btnMapPrefsClose" href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right" onclick="$(\'#popupMapPrefs\').popup(\'close\');">Close</a>');

    //    if (parseInt($('#spnRadiusTargeted').text()) > 10000) {
    //        $('#popupMapPrefs fieldset').empty();
    //        $('#popupMapPrefs fieldset').append(common_prefs);
    //        $('#dvMapPrefOptions').trigger('create');
    //        $('#liViewByHousehold').css('display', 'block');
    //        getSelectedZipsList();
    //    }
    //    else {
    var view_hh_option = '<input data-theme="c" type="checkbox" name="chkViewByHouseHold" id="chkViewByHouseHold" ' + ((is_map_target_genereated && $('#chkViewByHouseHold')[0].checked) ? 'checked' : '') + ' onchange="refreshMapWithPrefs();"/><label for="chkViewByHouseHold" style="margin-top:0px;margin-bottom:0px;">View By Household<br /><span style="font-size: 12px; font-weight: normal;">(Scatter Map up to 10k targets)</span></label>';
    view_hh_option += common_prefs;
    var fld_st = '<fieldset data-role="controlgroup" data-mini="true">' + view_hh_option + '</fieldset>';
    $('#dvMapPrefOptions').empty();
    $('#dvMapPrefOptions').html(fld_st);
    $('#dvMapPrefOptions').trigger('create');
    $('#liViewByHousehold').css('display', 'none');
    //}
}

function openMapPrefs() {
    $('#popupMapPrefs').panel('open', 'optionsHash');
}

var customconfigData = [];
function loadCustomerConfigData() {
    //#1 - START
    var temp_auth_string = sessionStorage.authString;
    if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
        sessionStorage.authString = "Basic dXNlcl9yZWcxOnVzZXJfcmVnMQ==";
    }
    //#1
    var gcust_config_data_url = mapsUrl + "/api/ZipSelectList_config/" + jobCustomerNumber;
    getCORS(gcust_config_data_url, null, function (data) {
        //#2 - START
        if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
            sessionStorage.authString = temp_auth_string;
        }
        customconfigData = data;
        bindConfigData(data)
    }, function (error_response) {
        //#3 - START
        if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
            sessionStorage.authString = temp_auth_string;
        }
        //#3 - END
        showErrorResponseText(error_response, false);
    });
}


function bindConfigData(data) {
    var config_Json = data;
    //var fset = '<fieldset data-role="controlgroup">';
    var config_controls = '';
    //var div_count = Math.round(config_Json.length / 4);
    var div_count = Math.round(Object.keys(config_Json).length / 2);
    var col_count_id = 97;
    var count = div_count;
    var inner_container = '';
    var group_index = 0;
    $("#dvConfigOptions").empty();
    if (Object.keys(config_Json).length > 0) {
        $.each(config_Json, function (key, val) {
            if (group_index == 0) {
                inner_container += '<div data-role="fieldcontain" align="center" style="margin: 0em 0; padding:15px"><div data-role="collapsible-set" id="dvConigDemos" class="ui-corner-all" data-content-theme="c" data-inset="false" data-theme="c" data-collapsed-icon="carat-r" data-expanded-icon="carat-d">';
            }
            for (var i = 0; i < val.length; i++) {
                if (i == 0) {
                    inner_container += '<div data-theme="e" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d"><h3>' + key + '</h3>';
                    inner_container += '<fieldset id="fieldset" data-role="controlgroup" data-mini="true" data-inset="false" style="overflow-y:scroll;height:155px;">';
                }
                config_controls += '<input type="checkbox" name="chk' + val[i].value + '" id="chk' + val[i].value + '" ' + ((val[i].isChecked != undefined && val[i].isChecked != null && val[i].isChecked != "" && val[i].isChecked == "true") ? "checked" : '') + '>';
                config_controls += '<label for="chk' + val[i].value + '"><span style="font-size:12px; font-weight:normal;">' + val[i].name + '</span></label>';
                //if ((i + 1) == count || ((i + 1) == val.length)) {
                if (((i + 1) == val.length)) {
                    //inner_container += '<div class="ui-block-' + String.fromCharCode(col_count_id) + '"><div style="padding: 0px 4px 0px 4px">' + fset;
                    inner_container += config_controls;
                    inner_container += '</fieldset></div>'; //</div>
                    config_controls = '';
                    //                    if ((i + 1) < val.length) {
                    //                        col_count_id++;
                    //                        count = count + div_count;
                    //                    }
                }
            }

            if ((group_index + 1) == Object.keys(config_Json).length) {
                inner_container += '</div></div>';
            }
            group_index++;
        });
    }
    //var config_div = '<div class="ui-grid-' + String.fromCharCode(col_count_id) + ' ui-responsive">' + inner_container + '</div>';
    //$('#dvConfigOptions').append(config_div);
    $('#dvConfigOptions').append(inner_container);
    $('#dvConfigOptions').trigger('create');
    //$(".ui-responsive").trigger("create");
    //$('#popupCustomerConfig').find('.ui-block-a,.ui-block-b,.ui-block-c,.ui-block-d,.ui-block-e').addClass('ui-block-new');
    $('#dvConfigOptions').find('.ui-collapsible-content,.ui-body-d').css('border-left-width', '1px');
    $('#dvConfigOptions').find('.ui-collapsible-content,.ui-body-d').css('border-right-width', '1px');
    //$("#dvConigDemos").collapsibleset().collapsibleset('refresh');
    //$('#popupCustomerConfig-popup').css('left', '169px');
    //$('#popupCustomerConfig').css('width', '500px');
    $('#popupCustomerConfig').popup('open', { positionTo: 'window' });
    //$("#popupCustomerConfig").popup({ positionTo: "window" }).popup('open').popup('reposition', 'positionTo: window');
}

function saveCustomerConfigData() {
    var saveConfigInfo = [];
    var gcust_config_data_post_url = mapsUrl + "api/ZipSelectList_postConfig/" + jobCustomerNumber;
    if (Object.keys(customconfigData).length > 0) {
        $.each(customconfigData, function (key, val) {
            $.each(val, function (key1, val1) {
                if ($('#chk' + val1.value).attr('checked')) {
                    var temp_val2 = $.extend(true, {}, val1);
                    temp_val2.isChecked = "true";
                    saveConfigInfo.push(temp_val2);
                }
            });
        });
    }
    postCORS(gcust_config_data_post_url, JSON.stringify(saveConfigInfo), function (data) {
        $('#ulDemographics').empty();
        $('#ulSelectedDemographics').empty();
        if (jobCustomerNumber != AAG_CUSTOMER_NUMBER)
            loadDemographics();
        $('#popupCustomerConfig').popup('close');
        $('#alertmsg').text("Configuration saved successfully.");
        $('#popupDialog a[id=okBut]').attr('onclick', '$(\'#popupDialog\').popup(\'open\');$(\'#popupDialog\').popup(\'close\');');
        $('#popupDialog').popup('open');
    }, function (error_response) {
        $('#popupCustomerConfig').popup('close');
        showErrorResponseText(error_response, false);
        //if (!(jQuery.browser.msie)) {
        //    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
        //}
        //else {
        //    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
        //}
        //$('#popupDialog').popup('open');
    });
}

function loadStates() {
    $.getJSON(gStateServiceUrl, function (result) {
        statesJSON = result;
        $("#" + statesTemplate).tmpl(statesJSON).appendTo("#" + ddlSearchStates).selectmenu('refresh');
        //$('#ddlProfileState').selectmenu('refresh');
    });
}

function openNavPanel() {
    //    $("#ulNavLinks").empty();
    //    $("#ulNavLinks").append(displayNavLinks()).listview('refresh');
    //    if (jobCustomerNumber == "99997") {
    //        $('#alertmsg').html("Menu locked for the purpose of this demo.");
    //        $('#popupDialog').popup('open');
    //    }
    //    else {
    $('#navLinksPanel').panel('open', 'optionsHash');
    // }
}
function navLinksClick(path) {
    var a = path;
    window.location.href = path;
}

function confirmNavigationMessage(page) {
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupNavConfirmDialog" data-overlay-theme="d" data-theme="c" style="text-align:left;max-width:500px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="navdialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmNavMsg"></div>';
    msg_box += '<div id="colId1"></div>';
    msg_box += '<div id="valList1"></div>';
    msg_box += '<div align="right">';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="dontSaveNavBut" data-mini="true">Don\'t Save</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelNavBut" data-mini="true">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="saveNavBut" data-mini="true">Save</a>';
    msg_box += '</div>';
    msg_box += '</div></div>';
    $("#" + page).append(msg_box);
}

function validateAndNavigate() {
    $('#btnContinueTop').click();
}

function validateForChangesAndSave(next_page_name, click_type) {
    if (click_type != "save") {
        var selected_crrts = ($('#hdnSelectedZip').val() != "") ? $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked') : [];
        var diff = [];
        if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length > 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
            //diff = getDifferences(selectedCRRTS, selected_crrts);
            diff = getDifferences(selected_crrts, selectedCRRTS);
        else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
            //diff = getDifferences(selected_crrts, selectedCRRTS);
            diff = getDifferences(selectedCRRTS, selected_crrts);

        if (diff.length == undefined || diff.length == 0) {
            if (sessionStorage.storeListSelection != undefined) {
                var selected_text = $('#hdnSelectedStore').val();
                selected_text = $('#ddlStoreLocations option:selected').text();
                if (gSelectedZips["zoomLevel"]) delete gSelectedZips["zoomLevel"];
                var temp_diff = DiffObjects(gSelectedZips, initialStoreData)
                //Save changes to the List Selections for Store 101 – Allentown, PA before continuing?
                if (selected_text != "" && (selectedCRRTS.length > 0 || selected_crrts.length > 0 || isTargetChanged || isRadiusChanged || isDemosChanged || temp_diff.length > 0)) {//|| selected_crrts.length > 0
                    var message = 'Save changes to the List Selections for Store <b>' + selected_text + '</b> before continuing?';
                    $('#confirmNavMsg').html(message);
                    $('#dontSaveNavBut').bind('click', function () {
                        $('#dontSaveNavBut').unbind('click');
                        $('#popupNavConfirmDialog').popup('close');
                        window.location.href = next_page_name;
                    });
                    $('#cancelNavBut').bind('click', function () {
                        var lnk_id = next_page_name.substring(next_page_name.lastIndexOf('\/') + 1, next_page_name.lastIndexOf('.')).replace('job', '');
                        $('#li' + lnk_id).attr('data-theme', 'c');
                        $('#li' + lnk_id + ' a').removeClass('ui-btn-active');
                        $('#li' + lnk_id).parent().listview('refresh');
                        $('#cancelNavBut').unbind('click');
                        $('#popupNavConfirmDialog').popup('close');
                    });
                    $('#saveNavBut').bind('click', function () {
                        $('#saveNavBut').unbind('click');
                        $('#popupNavConfirmDialog').popup('close');
                        window.setTimeout(function () {
                            $('#btnSaveSelections').trigger('click');
                            window.location.href = next_page_name;
                        }, 50);
                    });
                    $('#popupNavConfirmDialog').popup('open');
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
        }
        else {
            var selected_text = $('#hdnSelectedStore').val();
            selected_text = $('#ddlStoreLocations option:selected').text();
            if (selected_text != "" && selectedCRRTS.length > 0) {
                var message = 'Save changes to the List Selections for Store <b>' + selected_text + '</b> before continuing?';
                $('#confirmNavMsg').html(message);
                $('#dontSaveNavBut').bind('click', function () {
                    $('#dontSaveNavBut').unbind('click');
                    $('#popupNavConfirmDialog').popup('close');
                    window.location.href = next_page_name;
                });
                $('#cancelNavBut').bind('click', function () {
                    var lnk_id = next_page_name.substring(next_page_name.lastIndexOf('\/') + 1, next_page_name.lastIndexOf('.')).replace('job', '');
                    $('#li' + lnk_id).attr('data-theme', 'c');
                    $('#li' + lnk_id + ' a').removeClass('ui-btn-active');
                    $('#li' + lnk_id).parent().listview('refresh');
                    $('#cancelNavBut').unbind('click');
                    $('#popupNavConfirmDialog').popup('close');
                });
                $('#saveNavBut').bind('click', function () {
                    $('#saveNavBut').unbind('click');
                    $('#popupNavConfirmDialog').popup('close');
                    window.setTimeout(function () {
                        $('#btnSaveSelections').trigger('click');
                        window.location.href = next_page_name;
                    }, 50);
                });
                $('#popupNavConfirmDialog').popup('open');
                return false;
            }
            else {
                return true;
            }
        }
    }
    else
        return saveSelections(); //We shoud not pass any parameter.
    //return true;
}

//foundKeys = Object.keys(associativeArray).filter(function (key) {
//    return associativeArray[key] == value;
//})
function allowNumbersOnly(event) {
    event.value = event.value.replace(/[^0-9]+/g, '');
    //var tempVal = $('#txtRadiusTargeted').val();
    //if (/^[0-9]{1,10}$/.test(+tempVal)) // OR if (/^[0-9]{1,10}$/.test(+tempVal) && tempVal.length<=10) 
    //    return true;
    //else
    //    return false;
    if (event.value != undefined && event.value != "") {
        $('#btnRefresh').removeClass('ui-disabled');
        $('#btnRefresh')[0].disabled = false;
    }
}
//******************** Public Functions End **************************
