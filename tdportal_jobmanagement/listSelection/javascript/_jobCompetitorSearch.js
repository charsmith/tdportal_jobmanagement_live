﻿var service;
var overlays = [];
var resultList = [];
var markersArray = [];
var marker;
var is_map_move = false;
var circles = [];
var resultSet = [];
var isSelectedFromList = false;
var lastSelectedLocation = [];
var markersForSelectedLocation = [];
var infoWindows = [];
var viewPorts = [];
function makeCompetitorSearchMap() {
    //var latlng = new google.maps.LatLng(geoip_latitude(), geoip_longitude());
    //    var latlng = new google.maps.LatLng(43.1462421, -104.9929722);
    //    var mapOptions = {
    //        zoom: 12,
    //        center: latlng
    //    }
    //    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    //    var service = new google.maps.places.PlacesService(map);
    //    google.maps.event.addListener(map, 'idle', function () {
    //        //is_map_move = true;
    //        var NewMapCenter = map.getCenter();

    //        var request = {
    //            location: NewMapCenter,
    //            radius: '100',
    //            query: document.getElementById("txtBusinessName").value
    //        };
    //        resultList = [];
    //        if (document.getElementById("txtBusinessName").value != '' && document.getElementById("txtBusinessName").value != null) {
    //            service.textSearch(request, function (results, status, pagination) {
    //                if (status == google.maps.places.PlacesServiceStatus.OK) {
    //                    resultList = resultList.concat(results);
    //                    plotResultList();
    //                }
    //            });
    //        }

    //   });
}
function createPlaceHolderforIE() {
    if ($.browser.msie) {
        $('input[placeholder]').each(function () {
            var input = $(this);
            if ($(input).val() == "") $(input).val(input.attr('placeholder'));
            if (input.val() == '' || input.val() == input.attr('placeholder'))
                $(input).css('color', 'grey');
            else
                $(input).css('color', 'black');

            $(input).focus(function () {
                if (input.val() == input.attr('placeholder')) {
                    input.val('').css('color', 'black');
                }
                else
                    $(input).css('color', 'black');
            });
            $(input).blur(function () {
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.val(input.attr('placeholder')).css('color', 'grey');
                }
                else
                    $(input).css('color', 'black');
            });
        });
    };
}

function showResult() {
    init();
}

function plotResultList() {
    $('#ulSearchedLocations').empty();
    $('#ulSelectedCompetitor').empty();
    var li_list = '';
    resultSet = [];


    for (var i = 0; i < resultList.length; i++) {
        resultSet[i + 1] = resultList[i];
    }

    for (var i = 1; i < resultSet.length; i++) {
        //if (resultSet[i].formatted_address != undefined && resultSet[i].formatted_address != null && resultSet[i].formatted_address != "") {
        //    address_parts = resultSet[i].formatted_address.split(',');
        //    var address = "";
        //    var city = "";
        //    var state_zip = "";
        //    var zip = "";
        //    var is_match = (resultSet[i].name == address_parts[0])

        //    address = ((is_match) ? ((address_parts[1] != undefined && address_parts[1] != null) ? address_parts[1] : "") : ((address_parts[0] != undefined && address_parts[0] != null) ? address_parts[0] : ""));
        //    city = ((is_match) ? ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : "") : ((address_parts[1] != undefined && address_parts[1] != null) ? address_parts[1] : ""));
        //    state_zip = ((is_match) ? ((address_parts[3] != undefined && address_parts[3] != null) ? address_parts[3] : "") : ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : ""));

        //    zip = state_zip.match(/\b\d{5}\b/g);
        //    var state = state_zip.replace(/\d/g, '');
        //}

        if (resultSet[i].vicinity != undefined && resultSet[i].vicinity != null && resultSet[i].vicinity != "") {
            address_parts = resultSet[i].vicinity.split(',');
            var address = "";
            var city = "";
            var state_zip = "";
            var zip = "";
            var is_match = (resultSet[i].name == address_parts[0])

            address = ((is_match) ? ((address_parts[1] != undefined && address_parts[1] != null) ? address_parts[1] : "") : ((address_parts[0] != undefined && address_parts[0] != null) ? address_parts[0] : ""));
            city = ((is_match) ? ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : "") : ((address_parts[1] != undefined && address_parts[1] != null) ? address_parts[1] : ""));
            state_zip = ((is_match) ? ((address_parts[3] != undefined && address_parts[3] != null) ? address_parts[3] : "") : ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : ""));

            zip = state_zip.match(/\b\d{5}\b/g);
            var state = state_zip.replace(/\d/g, '');
        }

        //li_list += '<li><a onclick="show(\'' + i + '\',\'' + resultList[i].geometry.location.k + '\',\'' + resultList[i].geometry.location.D + '\',\'' + resultList[i].formatted_address + '\',\'' + resultList[i].name + '\');" style="font-family:Verdana;font-size:small" href="#">' + resultList[i].name + '<br/>' + resultList[i].formatted_address + '<br/>' + resultList[i].geometry.location.lat() + ' | ' + resultList[i].geometry.location.lng() + '</a></li>'
        li_list += '<li><a onclick="addCompetitorLocation(\'' + i + '\',\'' + resultSet[i].name.replace(/'/g, "") + '\',\'' + resultSet[i].geometry.location.lat() + '\',\'' + resultSet[i].geometry.location.lng() + '\',\'' + address + '\'|\'' + city + '\'|\'' + state + '\');" style="font-family:Verdana;font-size:small" href="#"><img src="' + logoPath + 'mapMarkers/marker_red' + i + '.png" alt="" class="ui-li-icon ui-corner-none" style="max-height:60px;max-width:60px;"><h2><span style="font-size: 14px">' + resultSet[i].name + '</span></h2>';
        li_list += '<p>' + address + ' ' + city + ', ' + state + '</p></a></li>'
        //createMarker(JSON.stringify(resultSet[i]), i);
        createMarker(resultSet[i].geometry.location.lat(),resultSet[i].geometry.location.lng(),resultSet[i].reference,(j+1));
    }

//    if (markersArray != undefined && markersArray.length > 0)
//        makeClusters();

    $('#ulSearchedLocations').append(li_list);
    $('#ulSearchedLocations').listview('refresh');
}

function plotResultLists(resultList) {
    var iterator = 0;
    var j = 0;
    overlays = [];
    clearSelectedMarkers();
    resetMarkersArray();
    clearLastSelectedMarkers();
    $('#ulSearchedLocations').empty();
    $('#ulSelectedCompetitor').empty();
    $('#spnCompLocWarning').remove();
    for (var i = 0; i < resultList.length; i++) {
        setTimeout(function () {
            //overlays.push(marker);
            var place = '';
            var request = {
                reference: resultList[iterator].reference
            };
            var li_list = '';
            service.getDetails(request, function (place, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    place.formatted_address;
                    if (place.formatted_address.split(',')[3] == ' United States' || place.formatted_address.split(',')[2] == ' United States' || place.formatted_address.split(',')[2] == ' USA') {
                        //overlays.push(place);


                        if (place.formatted_address != undefined && place.formatted_address != null && place.formatted_address != "") {
                            address_parts = place.formatted_address.split(',');
                            var address = "";
                            var city = "";
                            var state_zip = "";
                            var zip = "";
                            var is_match = (place.name == address_parts[0])

                            address = ((is_match) ? ((address_parts[1] != undefined && address_parts[1] != null) ? address_parts[1] : "") : ((address_parts[0] != undefined && address_parts[0] != null) ? address_parts[0] : ""));
                            city = ((is_match) ? ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : "") : ((address_parts[1] != undefined && address_parts[1] != null) ? address_parts[1] : ""));
                            state_zip = ((is_match) ? ((address_parts[3] != undefined && address_parts[3] != null) ? address_parts[3] : "") : ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : ""));

                            zip = state_zip.match(/\b\d{5}\b/g);
                            var state = state_zip.replace(/\d/g, '');
                        }


                        li_list += '<li data-theme="c"><a id="aSearchedLocation' + j + '" onclick="addCompetitorLocation(' + (j + 1) + ',\'' + place.name.replace(/'/g, "") + '\',\'' + place.geometry.location.lat() + '\',\'' + place.geometry.location.lng() + '\',\'' + address.replace(/'/g, '`') + '|' + city + '|' + state + '\');" style="font-family:Verdana;font-size:small" href="#"><img src="' + logoPath + 'mapMarkers/marker_red' + (j + 1) + '.png" alt="" class="ui-li-icon ui-corner-none" style="max-height:60px;max-width:60px;"><h2><span style="font-size: 14px">' + place.name + '</span></h2>';
                        li_list += '<p>' + address + ' ' + city + ', ' + state + '</p></a></li>'

                        //createMarker(JSON.stringify(place), (j + 1));
                        createMarker(place.geometry.location.lat(),place.geometry.location.lng(),place.reference,(j+1));

                        $('#ulSearchedLocations').append(li_list);
                        $('#ulSearchedLocations').listview('refresh');
                        j++;
                    }

                    $('#dvSearchCompititor').css('display', 'block');
                    $('#ulSelectedCompetitor').css('display', 'block');
//                    if (markersArray != undefined && markersArray.length > 0)
//                        makeClusters();
                }

            });

            iterator++;
        }, i * 280);
    }

}

function addCompetitorLocation(id, name, lat, lng, address) {
    var LatLng_selected_store = new google.maps.LatLng(gDataAll.latitude, gDataAll.longitude);
    var LatLng_competitor_store = new google.maps.LatLng(lat, lng);

    var distance_between_two = google.maps.geometry.spherical.computeDistanceBetween(LatLng_selected_store, LatLng_competitor_store);  //In meters
    var distance_in_miles = distance_between_two / 1609.344;
    //google.maps.geometry.spherical.computeDistanceBetween();

    if (distance_in_miles > 6) {
        $('#confirmMsgCompetitor').text('The competitor selected to mail around is over 6 miles from the location in this order.');
        $('#btnUseSelected').bind('click',function(){
        $('#btnUseSelected').unbind('click');
        $('#btnUseSelected').removeAttr('onclick');
        addLocationToCompetitorList(id , name , lat , lng, address , distance_in_miles);
        });
        //$('#btnUseSelected').attr('onclick', 'addLocationToCompetitorList("' + id + '","' + name + '","' + lat + '","' + lng + '","' + address + '","' + distance_in_miles + '");');
        $('#popupCompetitorConfirmDialog').popup('open');
    }
    else {
        addLocationToCompetitorList(id, name, lat, lng, address);
    }
}


function addLocationToCompetitorList(id, name, lat, lng, address, distance) {
    var old_marker;
    isSelectedFromList = true;
    //setTimeout(function () {

    if (lastSelectedLocation.length > 0)
        old_marker = lastSelectedLocation[0].marker;

    lastSelectedLocation = [];
    clearSelectedMarkers();
    clearLastSelectedMarkers();
    if (infoWindows.length > 0) {
        var info_win = infoWindows[0];
        info_win.close();
        infoWindows = [];
    }

    var markerNew = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        zindex: 9
        //icon: logoPath + 'mapMarkers/marker_red.png'
    });

    var marker_selected = { 'marker': markerNew, 'id': id };
    markersForSelectedLocation.push(marker_selected);

    var contentStr = '<a href="#" onClick="addCompetitorLocation(' + id + ',\'' + name + '\',\'' + lat + '\',\'' + lng + '\',\'' + address + '\');">Mail Around this Competitor</a><br/><b>' + name + '</b><br/>' + ((address.split('|').length > 0) ? address.split('|')[0] : '') + '<br/>' + ((address.split('|').length > 1) ? address.split('|')[1] : '') + '<br/>' + ((address.split('|').length > 2) ? address.split('|')[2] : '');
    var infowindowNew = new google.maps.InfoWindow({
        content: contentStr,
        zIndex: 2
    });

    infoWindows.push(infowindowNew);
    infowindowNew.open(map, markerNew);

    drawNewCircle(lat, lng, markerNew);


    $('#ulSelectedCompetitor').empty();
    $('#spnCompLocWarning').remove();
    var li_list = "";
    var spl_address = address.split('|');


    li_list += '<li data-lat="' + lat + '" data-lng="' + lng + '"><a style="font-family:Verdana;font-size:small" href="#" onclick="editSelectedCompetitor();"><h3>Competitor Location</h2>';
    li_list += '<p>' + spl_address[0] + '</p><p>' + spl_address[1] + ' ' + ((spl_address[2].length > 0) ? spl_address[2] : '') + '</p>';
    //li_list += (distance > 6) ? '<p style="color:red">Over 6 miles from store location.</p>' : '';
    li_list += '</a></li>';

    $('#ulSelectedCompetitor').append(li_list);
    $('#ulSelectedCompetitor').listview('refresh');
    $('#ulSelectedCompetitor').parent().append(((distance > 6) ? '<span id="spnCompLocWarning" style="font-family:Verdana;font-size:small;color:red">Over 6 miles from store location.</span>' : ''));
    $('#ulSelectedCompetitor').css('margin-bottom','0em');
    $('#popupCompetitorConfirmDialog').popup('close');
    //}, 550);
    
    $('#dvSearchCompititor').css('display', 'none');
    $('#lblMailAroundCompetitor').attr('data-theme', '');
    //$('#chkMailAroundCompetitor').attr('checked', false).checkboxradio('refresh');
    //$('#lblMailAroundCompetitor').trigger('create');

    resetMarkersArray();
    //drawMarkerForSelecetedStore();
    $('#btnUseSelected').unbind('click');
    //$('#btnUseSelected').removeAttr('onclick');
    $('#btnRefresh').trigger('click');
}

function editSelectedCompetitor() {
    //$('#chkMailAroundCompetitor').attr('data-theme', '');
    $('#lblMailAroundCompetitor').attr('data-theme', 'b');
    //$('#lblMailAroundCompetitor').trigger('create');
    $('#dvSearchCompititor').css('display', 'block');
    $('#chkMailAroundCompetitor').attr('checked', true).checkboxradio('refresh');
    //showResult($('#btnRefreshMap'));
    showResult();
    $('#ulSelectedCompetitor').css('display', 'block');
    clearLastSelectedMarkers();
    clearSelectedMarkers();
    resetCircles();
}

function clearMarker(markers) {
    if (markers) {
        for (i in markers) {
            markers[i].setMap(null);
        }
    }
}

//function createMarker(place, i) {
function createMarker(lat, lng,reference, i) {
    //place = JSON.parse(place);
    var circle;
    var selected_marker = '';

    var LatLng = new google.maps.LatLng(lat, lng);
    var infowindow = new google.maps.InfoWindow();
    if (is_map_move) {
        resetMarkersArray();
        is_map_move = false;
    }

    if (i != 0) {
        if (markersForSelectedLocation.length > 0 && markersForSelectedLocation[i] != undefined)
         selected_marker = markersForSelectedLocation[i].marker.icon;

        var marker = new google.maps.Marker({
            map: map,
            position: LatLng,
            icon: (selected_marker != '') ? selected_marker : logoPath + 'mapMarkers/marker_red' + i + '.png'
        });

        markersArray.push(marker);
        var selected_state = $("#ddlSearchStates option:selected").val();
        if (document.getElementById("txtAddress").value != '' || document.getElementById("txtCity").value != '' || selected_state != '') {
            //map.setCenter(LatLng);
            marker.icon = logoPath + 'mapMarkers/marker_red' + i + '.png'
            
        }
    }
    else if (i == 0 || isSelectedFromList) {
        var marker = new google.maps.Marker({
            map: map,
            position: LatLng
        });
        drawNewCircle(lat, lng, marker);
        if (lastSelectedLocation.length > 0) {
            var markerNew = new google.maps.Marker({
                position: new google.maps.LatLng(lastSelectedLocation[0].lat(), lastSelectedLocation[0].lng()),
                map: map,
                icon: logoPath + 'mapMarkers/marker_red' + i + '.png'
            });
            clearMarker(markersArray);
            drawNewCircle(circles[0].getCenter().lat(), circles[0].getCenter().lng(), markerNew);
        }
    }

    var request = {
        reference: reference
    };
    google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
            infowindow.setContent(markers[i][0]);
            infowindow.open(map, marker);
        }
    }));

    google.maps.event.addListener(marker, 'click', function () {
        service.getDetails(request, function (place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                clearLastSelectedMarkers();
                var contentStr = '<a href="#" onClick="addCompetitorLocation(' + i + ',\'' + place.name + '\',\'' + place.geometry.location.lat() + '\',\'' + place.geometry.location.lng() + '\',\'' + place.formatted_address.replace(/,/g,'|') + '\');">Mail Around this Competitor</a><br/><b>' + place.name + '</b><br/>' + ((place.formatted_address.split(',').length > 0) ? place.formatted_address.split(',')[0] : '') + '<br/>' + ((place.formatted_address.split(',').length > 1) ? place.formatted_address.split(',')[1] : '') + '<br/>' + ((place.formatted_address.split(',').length > 2) ? place.formatted_address.split(',')[2] : '');
                //if (!!place.formatted_phone_number) contentStr += '<br>Phone: ' + place.formatted_phone_number;
                if (infoWindows.length > 0) {
                    var info_win = infoWindows[0];
                    info_win.close();
                    infoWindows = [];
                }
                infowindow = new google.maps.InfoWindow();
                infowindow.setContent(contentStr);
                infowindow.open(map, marker);
                infoWindows.push(infowindow);
                var markerNew = new google.maps.Marker({
                    position: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
                    map: map,
                    //icon: logoPath + 'mapMarkers/marker_red.png'
                });
                var latlng = { 'lat': place.geometry.location.lat(), 'lng': place.geometry.location.lng(), 'marker': markerNew };
                lastSelectedLocation.push(latlng);

                var marker_selected = { 'marker': markerNew, 'id': i };
                markersForSelectedLocation.push(marker_selected);

                drawNewCircle(place.geometry.location.lat(), place.geometry.location.lng(), markerNew);

            } else {
                var contentStr = "<h5>No Result, status=" + status + "</h5>";
                infowindow.setContent(contentStr);
                infowindow.open(map, marker);
            }
        });
        
    });


}

function clearSelectedMarkers() {
    if (markersForSelectedLocation.length > 0) {
        var marker_old = markersForSelectedLocation[0];
        marker_old.marker.setMap(null);
        markersForSelectedLocation = [];
    }
}
function clearLastSelectedMarkers() {
    if (lastSelectedLocation.length > 0) {
        for (k = 0 ; k < lastSelectedLocation.length; k++) {
            var marker_old = lastSelectedLocation[k];
            marker_old.marker.setMap(null);
        }
        lastSelectedLocation = [];
    }
}

function resetMarkersArray() {
    clearLastSelectedMarkers();
    if (markersArray.length > 0) {
        for (l = 0 ; l < markersArray.length; l++) {
            var marker_old = markersArray[l];
            marker_old.setMap(null);
        }
        markersArray = [];
    }
}

function resetCircles() {
    if (circles.length > 0) {
        for (i = 0; i < circles.length; i++) {
            var circle = circles[i];
            circle.setMap(null);
        }
        circles = [];
    }
}

function drawNewCircle(lat, lng, marker) {

    var latlng = new google.maps.LatLng(lat, lng);
    //map.setCenter(latlng);
    var radius_value_default = $('#sldrRange').val(); //currently setting 2 miles radius
    var new_radius_value = radius_value_default * 1609.344

    if (gSelectedZips.wantsRadius == 1) {
        resetCircles();
        circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#2f51fd',
            fillOpacity: 0.4,
            map: map,
            //center: map.getCenter(),
            clickable: false,
            radius: new_radius_value

        });
        circle.bindTo('center', marker, 'position');
        circles.push(circle);
    }
}

function searchExistingLocation() {
    var selected_state = document.getElementById("ddlSearchStates").selectedIndex;
    selected_state = $("#ddlSearchStates option:selected").val();
   
    var search_business = ($('#txtBusinessName').val() != "") ? $('#txtBusinessName').val() : "";

    var store_address = ($('#txtAddress').val() != "") ? $('#txtAddress').val() : "";
    var search_state = (selected_state != "") ? selected_state : "";
    var search_city = ($('#txtCity').val() != "") ? $('#txtCity').val() : "";
    
    var msg = "";
    var search_json = {};
    if (search_city == "")
        msg += 'Please enter the city';
    if (search_state == "")
        msg += (msg != "") ? '<br/> Please enter the state' : 'Please enter the state';
    return msg;
}

function displayCompetitorConfirmMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupCompetitorConfirmDialog" data-history="false" data-overlay-theme="d" data-theme="c" style="max-width:500px;" class="ui-corner-all">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsgCompetitor"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-mini="true" data-theme="a" id="btnSelectNew" onclick="$(\'#popupCompetitorConfirmDialog\').popup(\'close\');">Select New Competitor Location</a><a href="#" data-role="button" data-inline="true" data-mini="true" data-theme="a"  id="btnUseSelected" >Use the Selected Location</a>';
    //msg_box += '<a href="#" data-role="button" data-inline="true" data-mini="true" data-theme="b"  id="btnUseSelected" >Use the Selected Location</a>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

function makeClusters() {
    var markerCluster = new MarkerClusterer(map, markersArray);
}

function addTiles(){
var bounds = map.getBounds();
var min_lat =  bounds.getSouthWest().lat(); // south - bottom
var max_lat = bounds.getNorthEast().lat(); //north - top
var min_lng =  bounds.getSouthWest().lng(); //west - left
var max_lng = bounds.getNorthEast().lng(); //east - right
//alert(" OLD : South/Bottom :" + min_lat + "North/Top :" + max_lat + "East/Right : " + max_lng + "West/Left : " + min_lng);
viewPorts.viewPort = {
    "right":max_lng,
    "left" : min_lng,
    "top" :max_lat,
    "bottom" : min_lat
};

//sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
//setBounds(bounds);
//var new_bounds = map.getBounds();
//var new_min_lat =  new_bounds.getSouthWest().lat(); // south
//var new_max_lat = new_bounds.getNorthEast().lat(); //north
//var new_min_lng =  new_bounds.getSouthWest().lng(); //west
//var new_max_lng = new_bounds.getNorthEast().lng(); //east
////alert("NEW : South/Bottom :" + min_lat1 + "North/Top :" + max_lat1 + "East/Right : " + max_lng1 + "West/Left : " + min_lng1);
// viewPorts.paddedViewPort = {
//    "right":new_max_lng,
//    "left" : new_min_lng,
//    "top" : new_max_lat,
//    "bottom" : new_min_lat
//};
 //sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
}

function setBounds(bnd, cb)
{
    var prj = map.getProjection();
    if(!bnd) bnd = map.getBounds();
    var ne = prj.fromLatLngToPoint(bnd.getNorthEast()),
        sw = prj.fromLatLngToPoint(bnd.getSouthWest());
    if(cb) ne.x += (300 / Math.pow(2, map.getZoom()));
    if(cb) sw.x -= (300 / Math.pow(2, map.getZoom()));
    else google.maps.event.addListenerOnce(map,'bounds_changed',
        function(){setBounds(bnd,1)});
    map.fitBounds(new google.maps.LatLngBounds(
        prj.fromPointToLatLng(sw), prj.fromPointToLatLng(ne)));
}

function createStaticImage() {
    ctaLayer;
    var baseUrl = myProtocol+"maps.google.com/maps/api/staticmap?";
    var params = [];
    var markers_list = [];
    var mapx = 600;
    var mapy = 300;
    var map_scale = 2;
    params.push("center=" + map.getCenter().lat().toFixed(6) + "," + map.getCenter().lng().toFixed(6));
    params.push("zoom=" + map.getZoom());
    params.push("maptype=roadmap");
    params.push("size=" + mapx +"x"+mapy);
    params.push("scale=" + map_scale);

    var bounds = map.getBounds();
    var ne = bounds.getNorthEast(); 
    var sw = bounds.getSouthWest(); 
    var nw = new google.maps.LatLng(ne.lat(), sw.lng());
    var se = new google.maps.LatLng(sw.lat(), ne.lng());

    
    var G = google.maps;
    var lat_lng1 = new G.LatLng(ne.lat().toFixed(6), sw.lng().toFixed(6));
    var lat_lng2 = new G.LatLng(nw.lat().toFixed(6), se.lng().toFixed(6));

    var center_point = new GMaps.LatLng(map.getCenter().lat().toFixed(6), map.getCenter().lng().toFixed(6));
//    var marker = new google.maps.Marker({
//        position: center_point,
//        map: map,
//        title: 'Center'
//    });
//    
//    var center_point1 = new G.LatLng(ne.lat().toFixed(6), ne.lng().toFixed(6));
//    var marker = new google.maps.Marker({
//        position: center_point1,
//        map: map,
//        title: 'Click to zoom1'
//    });
//    var center_point2 = new G.LatLng(sw.lat().toFixed(6), sw.lng().toFixed(6));
//    var marker = new google.maps.Marker({
//        position: center_point2,
//        map: map,
//        title: 'Click to zoom2'
//    });


//    var center_point3 = new G.LatLng(nw.lat().toFixed(6), nw.lng().toFixed(6));
//    var marker = new google.maps.Marker({
//        position: center_point3,
//        map: map,
//        title: 'Click to zoom3'
//    });

//    var center_point4 = new G.LatLng(se.lat().toFixed(6), se.lng().toFixed(6));
//    var marker = new google.maps.Marker({
//        position: center_point4,
//        map: map,
//        title: 'Click to zoom4'
//    });


    var zoom = map.getZoom();
    // var static_coordinates = getCoordinates(centerPoint, zoom, mapx, mapy, map_scale);
    var static_coordinates = getCoordinates(center_point, lat_lng1, lat_lng2, zoom, mapx, mapy, map_scale);

    for(var i=0;i<markersArray.length;i++){
        markers_list.push("markers=color:red|label:" + (i+1) + "|" + markersArray[i].position.lat().toFixed(6) + "," + markersArray[i].position.lng().toFixed(6));
    }
    //params = params.concat(markers_list);
    var imgSource = baseUrl + params.join('&') + "&sensor=false&path=color:red|weight:1|fillcolor:white|" + ctaLayer.getDefaultViewport();
    $('#imgStatic').attr("src", imgSource);
    $('#dvNWSEPoints').html(static_coordinates);

    $('#popUpLoadImage').popup().popup('open');
}


function getCoordinates(center,latlng1,latlng2, zoom, mapWidth, mapHeight, mapScale) {
    var corner_points;
    var scale = Math.pow(2, zoom);
    var center_px1 = proj.fromLatLngToPoint(latlng1);
    var SWPoint = { x: (center_px1.x - (mapWidth / 2) / scale), y: (center_px1.y + (mapHeight / 2) / scale) };
    var SWLatLon = proj.fromPointToLatLng(SWPoint);
    var NEPoint = { x: (center_px1.x + (mapWidth / 2) / scale), y: (center_px1.y - (mapHeight / 2) / scale) };
    var NELatLon = proj.fromPointToLatLng(NEPoint);
    var center_px2 = proj.fromLatLngToPoint(latlng2);
    var SEPoint = { x: (center_px2.x - (mapWidth / 2) / scale), y: (center_px2.y + (mapHeight / 2) / scale) };
    var SELatLon = proj.fromPointToLatLng(SEPoint);
    var NWPoint = { x: (center_px2.x + (mapWidth / 2) / scale), y: (center_px2.y - (mapHeight / 2) / scale) };
    var NWLatLon = proj.fromPointToLatLng(NWPoint);
    corner_points = 'CENTER: ' + center + '<br/><br/>' + 'SW: ' + SWLatLon + '<br/><br/>' + 'NE: ' + NELatLon + '<br/><br/>' + '<br/><br/>' + 'SE: ' + SELatLon + '<br/><br/>' + 'NW: ' + NWLatLon + '<br/><br/>' +
                    'Pixels per Inch (PPI): ' + (mapWidth) * mapScale + "x" + (mapHeight) * mapScale + '<br/><br/>';
    return corner_points;
    //alert('CENTER: ' + centerPx + '\n' + 'SW: ' + SWLatLon + '\n' + ' NE: ' + NELatLon);    
}

