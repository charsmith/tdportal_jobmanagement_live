﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var urlString = unescape(window.location)
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
var params = queryString.split("&");

var gServiceUrl = "";
var gServicePostGetUrl = "";

var gData;
var gMappedData;
var gOutputData;
var custListFieldsData = {};

var currentLineNumber = 0;
var startLineNumber = 0;
var endLineNumber = 10;
var numberOfRecords = 1;
var method_type = "";
var fileType = "";
var fileName = "";
var jobNumber = "", facilityId = "";
var file_data;
var file_name_to_map = "";
var file_type_to_map = "";
var fileType = params[0].substring(params[0].indexOf('=') + 1);
var fileName = params[1].substring(params[1].indexOf('=') + 1);
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************

if (params[1] != null && params[1].split("=")[1] != null) {
    file_name_to_map = params[1].split("=")[1];
    file_name_type_map = params[0].split("=")[1];
}
jobNumber = getSessionData("jobNumber");
facilityId = (appPrivileges.customerNumber == CW_CUSTOMER_NUMBER) ? 1 : getSessionData("facilityId");
gServiceUrl = serviceURLDomain + 'api/SampleLine/' + facilityId + '/' + jobNumber + '/source^' + fileType + '/' + fileName + '/';
gServicePostGetUrl = serviceURLDomain + 'api/FileMap/' + facilityId + '/' + jobNumber + '/' + fileName;


$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobFileMapper').live('pagebeforecreate', function (event) {
    displayMessage('_jobFileMapper');
    confirmMessage();
    displayNavLinks();

    $('#popupDialog').attr("data-rel", "dialog");
    $('#popupDialog').attr("data-dismissible", "false");
    $('#popupConfirmDialog a[id=okBut]').html("Continue");
    $('#popupConfirmDialog a[id=cancelBut]').attr('onclick', '$("#popupConfirmDialog").popup("close");');
    createControl('spnControl');
    $("div[data-role='header']").eq(0).find('a').eq(2).addClass('ui-btn-right');
    //$("#aReturnJob").attr("href", ((sessionStorage.continueToOrder.toLowerCase().indexOf('management') > -1 && appPrivileges.customerNumber == 1) ? "../multipleFileUpload/" + sessionStorage.continueToOrder : "../" + sessionStorage.continueToOrder.substring(0, sessionStorage.continueToOrder.indexOf('.')) + "/" + sessionStorage.continueToOrder));
    //$("#aReturnJob").attr("href", "../" + sessionStorage.continueToOrder.substring(0, sessionStorage.continueToOrder.indexOf('.')) + "/" + sessionStorage.continueToOrder);
    $("#aReturnJob").bind('click', function () {
        $("#aReturnJob").unbind('click');
        window.location.href = "../" + sessionStorage.continueToOrder.substring(0, sessionStorage.continueToOrder.indexOf('.')) + "/" + sessionStorage.continueToOrder;
    });
    //test for mobility...
    //loadMobility();
    makeGetPostData();
    loadCustomFieldsList('fieldDDL');
    //$('#fieldDDL').selectmenu('refresh');
    //$('#btnSaveTop').attr('onclick', "postFileMapData();");
    //$('#btnContinueTop').attr('data-icon', 'delete');
    //$('#btnContinueTop').attr('onclick', 'cancelData();');
    //$('#btnContinueTop').attr('title', 'Cancel');
});

$(document).on('pageshow', '#_jobFileMapper', function (event) {
    if ($.browser.msie) {
        $('input[placeholder]').each(function () {
            var input = $(this);
            $(input).val(input.attr('placeholder'));
            $(input).focus(function () {
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            });
            $(input).blur(function () {
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.val(input.attr('placeholder'));
                }
            });
        });
    };
    makeGData();
    populateFieldNameDropDown(false);
    $('#dvFieldsDDL').css('display', 'none');
});
//******************** Page Load Events End **************************

//******************** Public Functions Start **************************
function createControl(parent_name) {
    $('#' + parent_name).append("<select data-mini='true' id='fieldName'  data-theme='e'></select>");
}

function populateFieldNameDropDown(is_extended_file_map) {
    $('#spnControl').find('select').get(0).options.length = 0;
    if (is_extended_file_map) {
        $('#spnControl').find('select').empty();
        $('#spnControl').find('select').append($('#fieldDDL')[0].innerHTML);
        $('#spnControl').find('select').val('-1').selectmenu('refresh');
    }
    else {
        $.getJSON('../JSON/_listSourceBasicSet.JSON', function (data) {
            if (typeof (data) != 'undefined') {
                $.each(data[0].FieldName, function (i, option) {
                    $('#spnControl').find('select').eq(0).append($('<option/>').attr("value", option.value).text(option.name));
                });
                //$('#spnControl').find('select').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').text('Select');
                $('#spnControl').find('select').val('').selectmenu('refresh');
            }
        });
    }
}

function makeGData() {
    getCORS(gServiceUrl + startLineNumber + "/" + endLineNumber, null, function (data) {
        gData = data;
        //if (sessionStorage.jsonMappedData == undefined)
        gMappedData = gData;
        //else
        //    gMappedData = jQuery.parseJSON(sessionStorage.jsonMappedData);
        updateCustomFieldsWhenRowChanges();
        updateCustomFieldSampleData();
        buildLists();
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
    //setTimeout(function a() { }, 10000);
}

function fnDelimiterChange() {
    $.each($('div[id^=dvSwitchMain]'), function (a, b) {
        var ctrl = $(this).find('input[id^=txtSwitchOccurs]');
        switch ($("#ddlDelimiter").val().toLowerCase()) {
            case "comma":
                $("#" + ctrl[0].id).attr("placeholder", "Enter switch value(s) separated by Comma");
                break;
            case "piped":
                $("#" + ctrl[0].id).attr("placeholder", "Enter switch value(s) separated by Piped");
                break;
            case "semi-colon":
                $("#" + ctrl[0].id).attr("placeholder", "Enter switch value(s) separated by Semi-Colon");
                break;
            case "tab":
                $("#" + ctrl[0].id).attr("placeholder", "Enter switch value(s) separated by Tab");
                break;
        }
    });
    $('.ui-page').trigger('create');
}

function fnBuildSwitchStatements() {
    var count = 0;
    count = parseInt($("#hdnSwitchCount").val()) + 1;
    var actual_count = parseInt($("#hdnSwitchActualCount").val()) + 1;
    $("#hdnSwitchActualCount").val(actual_count);
    $("#hdnSwitchCount").val(count);
    var switch_statement = '<div id="dvSwitchMain' + count + '"><hr style="border-color:#6FACD5; border-width:1px; border-style:dotted" /><div class="ui-grid-a"><div class="ui-block-a" align="left">';
    switch_statement += '<input type="text" id="txtSwitchName' + count + '" name="txtSwitchName' + count + '" value="Switch ' + count + '" data-mini="true" style="font-size:17px;font-weight:bold;color:#6facd5;"/></div>';

    switch_statement += '<div id="dvArrows' + count + '" class="ui-block-b" align="right"></div>';
    switch_statement += '</div>';

    switch_statement += '<div data-role="fieldcontain"><label >A Switch occurs if the entered values below are found:</label>';
    switch_statement += '<input type="text" name="txtSwitchOccurs' + count + '" id="txtSwitchOccurs' + count + '" data-mini="true" value="" /></div>';

    switch_statement += '<div data-role="fieldcontain"><label >The value is output using:</label><select data-theme="e" name="ddlFieldName' + count + '" id="ddlFieldName' + count + '" data-mini="true" onchange="fnShowControl(this, ' + count + ');">';
    switch_statement += '<option value="-1">Select Switch Type</option><option value="text">Text</option><option value="field">Field</option></select></div>';

    switch_statement += '<div data-role="fieldcontain" id="dvOriginalValue' + count + '">';
    switch_statement += '<div id="dvSwitchOutputText' + count + '" style="display:none;"><label >The Output Text below replaces the original value:</label><input type="text" name="txtSwitchOutput' + count + '" id="txtSwitchOutput' + count + '" placeholder="Enter Output Text" data-mini="true" /></div>';
    switch_statement += '<div id="dvSwitchOutputDDL' + count + '" style="display:none;"><label >The output value in the field below replaces the original value:</label><select data-theme="e" name="ddlSwitchOutput' + count + '" id="ddlSwitchOutput' + count + '" data-mini="true"></select><div>';
    switch_statement += '</div></div>';

    $('#dvSwitchStatement').append(switch_statement);

    fnForArrows(count);

    $("div select").each(function (i) {
        $(this).selectmenu();
    });

    fnDelimiterChange();
    loadMappedColumnDropdown('ddlSwitchOutput' + count);
    $('#txtSwitchName' + count).addClass('text-label');
    $('.ui-page').trigger('create');
}

function fnSwapSwitchsMoveUp(count) {
    $("#dvSwitchMain" + count).insertBefore($("#dvSwitchMain" + count).prev());
    fnForArrows(parseInt($("#hdnSwitchCount").val()));

    $("div select").each(function (i) {
        $(this).selectmenu();
    });

    $('.ui-page').trigger('create');
}

function fnSwapSwitchsMoveDown(count) {
    $("#dvSwitchMain" + count).insertAfter($("#dvSwitchMain" + count).next());
    fnForArrows(parseInt($("#hdnSwitchCount").val()));

    $("div select").each(function (i) {
        $(this).selectmenu();
    });

    $('.ui-page').trigger('create');
}


function fnForArrows(count) {
    var arrows = '';
    var cnt_first = 0;
    var cnt_single_icon = true;
    var div_count = $('div[id^=dvSwitchMain]').length;

    for (var j = 0; j < div_count; j++) {
        var div_id = $('div[id^=dvSwitchMain]')[j].id;
        var div_number = div_id.substring(12); //12- dvSwitchMain length

        arrows = '';
        if (j == 0) {
            if (div_count > 1)
                arrows += '<a href="#" data-role="button" data-icon="arrow-d" data-iconpos="notext" data-theme="e" data-inline="true" onclick="fnSwapSwitchsMoveDown(' + div_number + ');">Move Down</a>';
            arrows += '<a href="#" data-role="button" data-icon="delete" data-iconpos="notext" data-theme="e" data-inline="true" onclick="fnRemoveSwitch(' + div_number + ');">Remove</a>';
            cnt_single_icon = false;
        }
        else if (j == div_count - 1) {
            arrows += '<a href="#" data-role="button" data-icon="arrow-u" data-iconpos="notext" data-theme="e" data-inline="true" onclick="fnSwapSwitchsMoveUp(' + div_number + ');">Move Up</a>';
            arrows += '<a href="#" data-role="button" data-icon="delete" data-iconpos="notext" data-theme="e" data-inline="true" onclick="fnRemoveSwitch(' + div_number + ');">Remove</a>';
        }
        else {
            arrows += '<a href="#" data-role="button" data-icon="arrow-d" data-iconpos="notext" data-theme="e" data-inline="true" onclick="fnSwapSwitchsMoveDown(' + div_number + ');">Move Down</a>';
            arrows += '<a href="#" data-role="button" data-icon="arrow-u" data-iconpos="notext" data-theme="e" data-inline="true" onclick="fnSwapSwitchsMoveUp(' + div_number + ');">Move Up</a>';
            arrows += '<a href="#" data-role="button" data-icon="delete" data-iconpos="notext" data-theme="e" data-inline="true" onclick="fnRemoveSwitch(' + div_number + ');">Remove</a>';
        }
        if ($("#dvSwitchMain" + div_number) != undefined) {
            $("#dvArrows" + div_number).empty();
            $("#dvArrows" + div_number).append(arrows);
        }

    }
}

function fnShowControl(ctrl, count) {
    if ($("#" + ctrl.id).val() == "-1") {
        $("#dvSwitchOutputDDL" + count).hide();
        $("#dvSwitchOutputText" + count).hide();
    }
    else if ($("#" + ctrl.id).val() == "text") {
        $("#dvSwitchOutputDDL" + count).hide();
        $("#dvSwitchOutputText" + count).show();
    }
    else {
        $("#dvSwitchOutputDDL" + count).show();
        $("#dvSwitchOutputText" + count).hide();
    }
}

function fnDefaultSwitch(ctrl) {
    if ($("#" + ctrl.id).val() == "-1") {
        $("#dvDDLDefault").hide();
        $("#dvTxtDefault").hide();
    }
    else if ($("#" + ctrl.id).val() == "text") {
        $("#dvDDLDefault").hide();
        $("#dvTxtDefault").show();
    }
    else {
        $("#dvDDLDefault").show();
        $("#dvTxtDefault").hide();
    }
}

function fnRemoveSwitch(count) {
    $("#dvSwitchMain" + count).remove();

    fnForArrows(parseInt($("#hdnSwitchCount").val()));
    $("div select").each(function (i) {
        $(this).selectmenu();
    });
    $('.ui-page').trigger('create');
}

function makeGetPostData() {
    if (sessionStorage.jsonMappedPostData != undefined && gOutputData != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jsonMappedPostData);
        /* the below values need to be updated with the values from the session */
        gOutputData.schemaName = "standard";
        gOutputData.delimiterP = "tab";
        gOutputData.mapDirection = "inbound";
        gOutputData.format = "delimited";
        /*----------------------------------------------------------------------*/
    }
    else {
        getCORS(gServicePostGetUrl, null, function (data) {
            if (data != null) {
                gOutputData = data;
                /* the below values need to be updated with the values from the session */
                gOutputData.schemaName = "standard";
                gOutputData.delimiterP = "tab";
                gOutputData.mapDirection = "inbound";
                gOutputData.format = "delimited";
            }
            /*----------------------------------------------------------------------*/
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    }
}

function buildLists() {
    var file_data = [];
    if (gMappedData != null && gMappedData != undefined && gMappedData.length > 0) {
        file_data = jQuery.grep(gMappedData, function (obj) {
            return obj.lineNumber === currentLineNumber;
        });

        var map_info;
        var file_mapping_data;
        if (file_data.length > 0) {
            var file_mappings = "";
            bindCustomFields();
            var count = 1;
            var is_switch = false;
            $.each(file_data[0].sampleFieldList, function (key2, val2) {
                is_switch = false;
                if (gOutputData != undefined && gOutputData.mapFieldList != undefined) {
                    map_info = jQuery.grep(gOutputData.mapFieldList, function (obj) {
                        //if (obj.column == parseInt(val2.column) + 1)
                        //return obj.column === parseInt(val2.column) + 1;
                        if (obj.column == parseInt(val2.column))
                            return parseInt(obj.column) === parseInt(val2.column);
                    });
                    if (map_info != undefined && map_info.length > 0) {
                        if (val2.fieldName == undefined) val2["fieldName"] = map_info[0].name;
                        map_info[0].value = val2.value;
                    }
                    else
                        if (val2.fieldName == undefined) val2["fieldName"] = "";

                    map_info = jQuery.grep(gOutputData.mapFieldList, function (obj) {
                        //if (obj.column == parseInt(val2.column) + 1)
                        //return obj.column === parseInt(val2.column) + 1;
                        if (obj.type == 'switch' && obj.column == parseInt(val2.column)) {
                            is_switch = true;
                            return parseInt(obj.column) === parseInt(val2.column);
                        }
                    });
                }
                if (!is_switch)
                    file_mappings += '<li data-icon="edit" name="' + val2.column + '" id="edit"><a href="#popupLogin" onclick="populateMapper(' + val2.column + ', ' + count + ',\'' + val2.value + '\');" data-rel="popup" data-position-to="window"><h3>' + val2.value + '</h3><p>' + val2.fieldName + '</p><span class="ui-li-count">Col ' + count + '</span></a></li>';
                count++;
            });
            $('#ulFileMapper').empty();
            $('#ulFileMapper').append(file_mappings);
            $('#ulFileMapper').listview('refresh');

            $('#spnFileMapName').text(gOutputData.mapName); // TO BE discussed updated and accordingly.
        };
    }
}

function bindData() {
    if ($("#hdnMapperPk").val() != "") {
        var file_data = [];
        file_data = jQuery.grep(gMappedData, function (obj) {
            return obj.lineNumber === currentLineNumber;
        });

        var col_field_name = [];
        col_field_name = jQuery.grep(file_data[0].sampleFieldList, function (obj) {
            return obj.fieldName === $("#fieldName").val();
        });

        if (col_field_name.length > 0) {
            $('#alertmsg').text('Field Name already selected. Please select another one.');
            $("#popupDialog").popup('open');
            return false;
        }
        var col_data = [];
        col_data = jQuery.grep(file_data[0].sampleFieldList, function (obj) {
            return obj.column === parseInt($("#hdnMapperPk").val());
        });
        var mapper_name = $("#fieldName").val();
        col_data[0].fieldName = mapper_name;

        sessionStorage.jsonMappedData = JSON.stringify(gMappedData);

        var column_data = [];
        column_data = jQuery.grep(gMappedData, function (obj) {
            return obj.lineNumber === currentLineNumber;
        });
        var map_column_info;
        map_column_info = jQuery.grep(column_data[0].sampleFieldList, function (obj) {
            return obj.column === parseInt($("#hdnMapperPk").val());
        });

        if (gOutputData != undefined) {
            if ($("#hdnMapperPk").val() != "" || $("#hdnMapperPk").val() != undefined) {
                var temp_column_data = [];
                temp_column_data = jQuery.grep(gOutputData.mapFieldList, function (obj) {
                    return obj.column === parseInt($("#hdnMapperPk").val()); // +1;
                });
                if (temp_column_data.length > 0) {
                    temp_column_data[0].value = map_column_info[0].value;
                }
                else {
                    gOutputData.mapFieldList.push({
                        "name": mapper_name,
                        "value": map_column_info[0].value,
                        //"column": parseInt(map_column_info[0].column) + 1
                        "column": parseInt(map_column_info[0].column)
                    });
                }
            }
        }
    }

}

function fnClearSwitch() {
    $('div[id^=dvSwitchMain]').remove();
    $('#ddlSwitchType').val("-1").selectmenu('refresh').trigger('onchange');
    $('#txtDefaultOutput').val('');
    $('#ddlDefaultOutput').val("-1").selectmenu('refresh').trigger('onchange');
}

function fnCancel(type) {
    if (type == "changemap") {
        $('#popupChangeMap').popup('close');
    }
    else if (type == "savemap") {
        $('#textMapFileName').val('');
        $('#ddlSaveTo').val('-1').selectmenu('refresh');
        $('#txtCustomerId').val('');
        $('#dvCustomerId').css('display', 'none');
        $('#ddlLayoutTemplate').val('-1').selectmenu('refresh');
        $('#dvLayoutTemplate').css('display', 'none');
        $('#popupSaveMap').popup('close');

    }
    else if (type.toLowerCase() == 'custom') {
        $('#ddlCustomField').val('-1').selectmenu('refresh');
        $('#customFieldName').val('-1').selectmenu('refresh');
        $('#popupCustom').popup('close');
    }
    else if (type.toLowerCase() == 'login') {
        $('#popupLogin').popup('close');
        $('#fieldName').val('-1').selectmenu('refresh');
        $('#sliderExtendedMap').val('off').slider('refresh');
        $('div.ui-collapsible-content', "#dvAdvPref").trigger('collapse');
    }
    else if (type.toLowerCase() == 'text') {
        $('#custTextField').val('');
        $('#editTextField').popup('close');
    }
    else if (type.toLowerCase() == 'switch') {
        $('div[id^=dvSwitchMain]').remove();
        $('#ddlSwitchType').val("-1").selectmenu('refresh').trigger('onchange');
        $('#txtDefaultOutput').val('');
        $('#ddlDefaultOutput').val("-1").selectmenu('refresh').trigger('onchange');
        $('#popupEditCustomSwitch').popup('close');
    }
    else {
        $('#popupEditCustom').popup('close');
    }
}

function validateMappedFieldsExistence() {
    if (gOutputData.mapFieldList.length == 0) {
        $('#popupCustom').popup('close');
        $('#alertmsg').text('This Custom Field requires the file to have at least one mapped field. Please cancel the "Add Custom Field" dialog and map the field you want to associate the Custom Field with.');
        $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');$(\'#popupCustom\').popup(\'open\');');
        window.setTimeout(function getDelay() { $('#popupDialog').popup('open'); }, 100);
        return false;
    }
    return true;
}

function fnDescCustomField(ctrl) {
    var selection = $('#' + ctrl.id);
    var desc = $('#descCustomField');
    desc.empty();
    switch (selection.val().toLowerCase()) {
        case "concat":
            desc.html('Concatenation is </br> the operation of joining two </br> character strings end-to-end. For </br> example, the concatenation of </br> "John" and "Doe" is "JohnDoe".');
            validateMappedFieldsExistence();
            $('#customFieldName').empty();
            $('#customFieldName').append($('#fieldDDL')[0].innerHTML);
            $('#customFieldName').selectmenu('refresh');
            break;
        case "switch":
            desc.html('A Switch allows you to switch </br> text or field values with the field </br> that the switch is assigned to.');
            if (!validateMappedFieldsExistence()) break;
            loadMappedColumnDropdown('customFieldName');
            loadMappedColumnDropdown('ddlDefaultOutput');
            break;
        case "text":
            desc.html('A Text field will allow you to enter </br> a text value and assign it to a </br> field value.');
            $('#customFieldName').empty();
            $('#customFieldName').append($('#fieldDDL')[0].innerHTML);
            $('#customFieldName').selectmenu('refresh');
            break;
        default:
            desc.html('Please select Field Type.');
            $('#customFieldName').empty();
            $('#customFieldName').append($('#fieldDDL')[0].innerHTML);
            $('#customFieldName').selectmenu('refresh');
            break;
    }
}

function updateFileMapper() {
    bindData();
    buildLists();
    $("#popupLogin").popup("close");
    return;
}

function populateMapper(pk, col_id, value) {
    $("#hdnMapperPk").val(pk);
    $("#hdnColId").val(col_id);
    $("#lblColData").html(value);
    populateFieldNameDropDown(false);
}

function fnAddLine() {
    if ($("#hdnMapperPk").val() != "") {
        var temp_file_mapper = [];
        var column_data = [];
        column_data = jQuery.grep(gMappedData, function (obj) {
            return obj.lineNumber === currentLineNumber;
        });
        if (column_data.length > 0) {
            var insert_column_at;
            insert_column_at = jQuery.grep(column_data[0].sampleFieldList, function (obj) {
                return obj.column === parseInt($("#hdnMapperPk").val());
            });
            if (insert_column_at.length > 0) {
                temp_file_mapper.fieldName = "";
                temp_file_mapper.value = "";
                temp_file_mapper.column = column_data[0].sampleFieldList.length + 1;
                column_data[0].sampleFieldList.splice($("#hdnColId").val(), 0, temp_file_mapper);
            }
        }
        sessionStorage.jsonMappedData = JSON.stringify(gMappedData);
        bindData();
        buildLists();
    }
    else {
        $('#alertmsg').text('Please select a row to add.');
        $("#popupDialog").popup('open');
    }
}

function fnRemoveCustomField(field_name) {

    var file_data = [];
    var column_data;
    if (gOutputData.mapFieldList.length > 0) {
        var cust_col_count = 0;
        column_data = jQuery.grep(gOutputData.mapFieldList, function (obj) {
            cust_col_count++;
            //return obj.name === $("#hdnCustColName").val();
            return obj.name === field_name;
        });

        if (column_data.length > 0) {
            var returnVal = $('#confirmMsg').text("Are you sure you want to remove this field? All custom settings associated with this field will be removed.");
            $('#valList').val(gOutputData);
            $('#colId').val(cust_col_count - 1);
            $('#popupConfirmDialog').popup('open');
            return false;
        }
    }
}

function fnConfirmRemoveRow() {
    $('#popupConfirmDialog').popup('close');
    var val = $('#valList').val();
    var col_id = $('#colId').val();
    if (val.mapFieldList[col_id].type == "switch") {
        val.mapFieldList.push({
            "column": val.mapFieldList[col_id].column,
            "name": val.mapFieldList[col_id].name,
            "type": 'varchar',
            "value": val.mapFieldList[col_id].value
        });
        var removed_column_info = { "column": val.mapFieldList[col_id].column, "value": val.mapFieldList[col_id].value, "fieldName": val.mapFieldList[col_id].name };

        var col_index = '';
        map_data = jQuery.grep(gMappedData, function (obj) {
            return obj.lineNumber === currentLineNumber;
        });
        $.each(map_data[0].sampleFieldList, function (a, b) {
            if (b.column > val.mapFieldList[col_id].column) {
                col_index = a;
                return false;
            }
        });
        //gMappedData[currentLineNumber].sampleFieldList.splice(col_index, 0, removed_column_info);
    }
    val.mapFieldList.splice(col_id, 1);
    sessionStorage.jsonMappedPostData = JSON.stringify(gOutputData);
    buildLists();
}

function fnPreviousPage() {
    currentLineNumber--;
    if (currentLineNumber < 0) currentLineNumber = 0;
    if (currentLineNumber >= startLineNumber) {
        buildLists();
        updateCustomFieldSampleData();
    }
    else {
        endLineNumber = endLineNumber - 10;
        //currentLineNumber = endLineNumber;
        //endLineNumber = currentLineNumber + 1;
        startLineNumber = endLineNumber - 10;
        if (startLineNumber < 0)
            startLineNumber = 0;
        makeGData();
    }
}

function fnNextPage() {

    currentLineNumber++;
    if (currentLineNumber >= endLineNumber) {
        currentLineNumber = endLineNumber;
        startLineNumber = endLineNumber;
        endLineNumber = endLineNumber + 10;
        makeGData();
    }
    else {
        buildLists();
        updateCustomFieldSampleData();
    }

}

function updateCustomFieldsWhenRowChanges() {
    $.each(gOutputData.mapFieldList, function (a, b) {
        if (b.type == undefined) {
            var mapped_column = [];
            $.each(gMappedData, function (c, d) {
                var is_found = false;
                if (d.lineNumber == currentLineNumber) {
                    mapped_column = $.grep(d.sampleFieldList, function (obj) {
                        if (obj.column == b.column) {
                            is_found = true;
                            return obj.column === b.column;
                        }
                    });
                }
                if (is_found)
                    return false;
            });
            //$.each(gMappedData, function (c, d) {
            if (mapped_column.length > 0) {
                b.value = mapped_column[0].value;
            }
            //});

        }
    });
    //[currentLineNumber].sampleFieldList,
}

function updateCustomFieldSampleData() {
    var map_data;

    if (gOutputData.mapFieldList.length > 0) {
        if (gMappedData != null && gMappedData != undefined && gMappedData.length > 0) {
            map_data = jQuery.grep(gMappedData, function (obj) {
                return obj.lineNumber === currentLineNumber;
            });
        }
        $.each(gOutputData.mapFieldList, function (a, b) {
            //gMappedData[currentLineNumber].sampleFieldList[        
            if (b.type == "concat") {
                if (b.fieldList != undefined && b.fieldList.length > 0) {
                    var custom_value = "";
                    $.each(b.fieldList, function (c, d) {
                        if (d.type == "field" && d.column != undefined) {
                            $.each(map_data[0].sampleFieldList, function (key, val) {
                                if (d.column == val.column) {
                                    d.value = val.value;
                                    custom_value += val.value;
                                    return false;
                                }
                            });
                        }
                        else if (d.type == "text" && d.column == undefined) {
                            custom_value += d.value;
                        }
                    });
                    b.value = custom_value;
                }
            }
            else if (b.type == "switch") {
                var custom_value = "";
                //b.value = gOutputData.mapFieldList[b.column].value;
                //var map_data;
                //                if (gMappedData != null && gMappedData != undefined && gMappedData.length > 0) {
                //                    map_data = jQuery.grep(gMappedData, function (obj) {
                //                        return obj.lineNumber === currentLineNumber;
                //                    });
                //                }
                if (map_data.length > 0) {
                    $.each(map_data[0].sampleFieldList, function (c, d) {
                        if (d.column == b.column) {
                            b.value = d.value;
                            return false;
                        }
                    });
                }
            }
        });
        bindCustomFields()
    }
}

function postFileMapData() {
    var url = serviceURLDomain + 'api/FileMap/true';
    var post_data = jQuery.grep(gOutputData.mapFieldList, function (obj) {
        return obj.value != "";
    });
    gOutputData.mapFieldList = post_data;
    postCORS(url, JSON.stringify(gOutputData), function (response) {
        $('#alertmsg').text('File Map changes saved successfully.');
        $("#popupDialog").popup('open');
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
}

function switchValidations() {
    var msg = "<ul>";
    $.each($('div[id^=dvSwitchMain]'), function (a, b) {
        var delimiter = "";
        var switch_data = $(this).find('input[id^=txtSwitchOccurs]').val();
        switch ($("#ddlDelimiter").val().toLowerCase()) {
            case "comma":
                if (switch_data.indexOf("|") != -1 || switch_data.indexOf(";") != -1 || switch_data.indexOf("\\t") != -1)
                    delimiter = "Commas";
                break;
            case "piped":
                if (switch_data.indexOf(",") != -1 || switch_data.indexOf(";") != -1 || switch_data.indexOf("\\t") != -1)
                    delimiter = "Piped";
                break;
            case "semi-colon":
                if (switch_data.indexOf("|") != -1 || switch_data.indexOf(",") != -1 || switch_data.indexOf("\\t") != -1)
                    delimiter = "Semi-Colon";
                break;
            case "tab":
                if (switch_data.indexOf("|") != -1 || switch_data.indexOf(";") != -1 || switch_data.indexOf(",") != -1)
                    delimiter = "Tab";
                break;
        }
        if (delimiter != "")
            msg += '<li>Please enter values separated by ' + delimiter + ' for <b>' + $(this).find('input[id^=txtSwitchName]').val() + '</b> or change the delimiter in the Advanced Preferences.</li>';

        if ($(this).find('select[id^=ddlFieldName]').val() == "-1")
            msg += '<li>The Switch Type has not been selected for <b>' + $(this).find('input[id^=txtSwitchName]').val() + '</b>. Please make a selection or remove <b>' + $(this).find('input[id^=txtSwitchName]').val() + '</b>.</li>';

        if ($(this).find('select[id^=ddlFieldName]').val() == "text" && $.trim($(this).find('input[id^=txtSwitchOutput]').val()) == "")
            msg += '<li>Output Text has not been entered for <b>' + $(this).find('input[id^=txtSwitchName]').val() + '</b>. Please enter a value or remove <b>' + $(this).find('input[id^=txtSwitchName]').val() + '</b>.</li>';

        if ($(this).find('select[id^=ddlFieldName]').val() == "field" && $(this).find('select[id^=ddlSwitchOutput]').val() == "-1")
            msg += '<li>The field providing the Output Value in <b>' + $(this).find('input[id^=txtSwitchName]').val() + '</b> has not been selected. Please select a field or remove <b>' + $(this).find('input[id^=txtSwitchName]').val() + '</b>.</li>';
    });

    //default switch validations
    if ($('#ddlSwitchType').val() == "-1")
        msg += '<li>The Default Switch Type has not been selected. Please make a selection.</li>';
    if ($('#ddlSwitchType').val() == "text" && $.trim($('#txtDefaultOutput').val()) == "")
        msg += '<li>Default Output Text has not been entered. Please enter a value.</li>';
    if ($('#ddlSwitchType').val() == "field" && $('#ddlDefaultOutput').val() == "-1")
        msg += '<li>The field providing the Default Output Value has not been selected. Please select a field.</li>';

    msg = msg + "</ul>";
    if (msg != "<ul></ul>") {
        $('#alertmsg').html(msg);
        $('#popupEditCustomSwitch').popup('close');
        $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');window.setTimeout(function () { $(\'#popupEditCustomSwitch\').popup(\'open\');}, 100);');
        window.setTimeout(function () { $('#popupDialog').popup('open'); }, 100);
        return false;
    }
    return true;
}

function updateCustomFields() {
    if ($('#hidCustColType').val() == "text") {
        var insert_fieldList_at;
        insert_fieldList_at = jQuery.grep(gOutputData.mapFieldList, function (obj) {
            return obj.type === $('#hidCustColType').val();
        });

        if (insert_fieldList_at.length > 0) {
            insert_fieldList_at[0].value = $('#custTextField').val();
            insert_fieldList_at[0].fieldList.value = $('#custTextField').val();
        }
        fnCancel('text');
    }
    else if ($('#hidCustColType').val() == "switch") {
        if (!switchValidations()) return false;
        updateSwitchStatements();
        fnCancel('switch');
    }
    else {
        $("#popupCustom").popup("close");
        $("#popupEditCustom").popup("open");
        $("#popupEditCustom").popup("close");
        var field_data = {};
        var field_list = [];
        var $inputs = $('#ulControls').find('li').find('input,select');
        var display_text = "";
        var lbl_id = "";
        $inputs.each(function () {
            if ($(this).attr('id').indexOf('custom') != -1) {
                if ($(this)[0].type == 'select-one') {

                    lbl_id = "customLblColValue" + ($(this)[0].id).substring($(this)[0].id.length - 1, $(this)[0].id.length);

                    field_data["type"] = "field";
                    field_data["value"] = $("#" + lbl_id).html();
                    field_data["column"] = $(this).val();
                    //field_data["name"] = $("#" + lbl_id).html();
                    display_text += $("#" + lbl_id).html();
                }
                else if ($(this)[0].type == 'text') {
                    field_data["type"] = "text";
                    field_data["value"] = $(this).val();
                    display_text += $(this).val();
                }
            }
            field_list.push(field_data);
            field_data = {};
        });

        var insert_fieldList_at;
        insert_fieldList_at = jQuery.grep(gOutputData.mapFieldList, function (obj) {
            return obj.name === $('#hdnCustColName').val();
        });

        if (insert_fieldList_at.length > 0) {
            insert_fieldList_at[0].fieldList = field_list;
            insert_fieldList_at[0].value = display_text;
        }
    }
    sessionStorage.jsonMappedPostData = JSON.stringify(gOutputData);
    bindCustomFields();
}

function updateSwitchStatements() {

    var switch_blocks = $('div[id^=dvSwitchMain]');

    //    {"name": "switch","value": "", “column”:"0", "fieldList":[
    //       {"name":"switch1","occurs":"Test Data1","type": "field","outputUsing": "fullName", “column”:"0"},
    //       {"name":"switch2","occurs":"Test Data2","type": "text","outputUsing": "sample text", “column”:"0"},
    //       {"name":"default","occurs":"","type": "text","outputUsing": "default sample text", “column”:"0"}
    //    ]}

    var switch_info = {};
    var switch_fields_List = {};
    switch_info["name"] = "switch";
    switch_info["value"] = "";
    switch_info["column"] = "0";
    switch_info["fieldList"] = [];

    var insert_fieldList_at;
    insert_fieldList_at = jQuery.grep(gOutputData.mapFieldList, function (obj) {
        return obj.name === $('#hdnCustColName').val() && obj.type === $('#hidCustColType').val();
    });
    insert_fieldList_at[0].fieldList = [];
    $.each($('div[id^=dvSwitchMain]'), function (a, b) {
        switch_fields_List = {};
        switch_fields_List["name"] = $(this).find('input[id^=txtSwitchName]').val();
        switch_fields_List["occurs"] = $(this).find('input[id^=txtSwitchOccurs]').val();
        switch_fields_List["type"] = $(this).find('select[id^=ddlFieldName]').val();
        if ($(this).find('select[id^=ddlFieldName]').val() == "text")
            switch_fields_List["outputUsing"] = $(this).find('input[id^=txtSwitchOutput]').val();
        else if ($(this).find('select[id^=ddlFieldName]').val() == "field")
            switch_fields_List["outputUsing"] = $(this).find('select[id^=ddlSwitchOutput]').val();
        switch_fields_List["column"] = "0";
        insert_fieldList_at[0].fieldList.push(switch_fields_List);
    });

    //Default Values persistence
    switch_fields_List = {};
    switch_fields_List["name"] = "default";
    switch_fields_List["occurs"] = "";
    switch_fields_List["type"] = $('#ddlSwitchType').val();
    if ($('#ddlSwitchType').val() == "text")
        switch_fields_List["outputUsing"] = $('#txtDefaultOutput').val();
    else if ($('#ddlSwitchType').val() == "field")
        switch_fields_List["outputUsing"] = $('#ddlDefaultOutput').val();
    switch_fields_List["column"] = "0";
    insert_fieldList_at[0].fieldList.push(switch_fields_List);
}

function openAddCustomFieldPopUp() {
    fnCancel('custom')
    $('#customFieldName').empty();
    $('#customFieldName').append($('#fieldDDL')[0].innerHTML);
    $('#customFieldName').selectmenu('refresh');
    $('#popupCustom').popup('open');
}

function createCustomFields(field_type) {
    if (!validateMappedFieldsExistence()) return false;
    var ctrls = $('#popupCustom').find("select");
    var temp_data = {};
    var field_list = [];
    var str = "";
    var selected_column = '';
    $.each(ctrls, function (key, val) {
        str = "";
        if (val.id.toLowerCase() == "ddlcustomfield")
            temp_data["type"] = val.value;
        else {
            if (field_type == 'concat')
                temp_data["name"] = val.value;
            else if (field_type == 'switch') {
                temp_data["column"] = val.value;
                temp_data["name"] = $(val)[0].options[$(val)[0].selectedIndex].text;
                selected_column = val.value;
            }
        }
    });

    if (field_type == 'concat')
        temp_data["value"] = "";
    else if (field_type == 'switch') {
        var col_index = '';
        var map_data;
        if (gMappedData != null && gMappedData != undefined && gMappedData.length > 0) {
            map_data = jQuery.grep(gMappedData, function (obj) {
                return obj.lineNumber === currentLineNumber;
            });
        }
        $.each(map_data[0].sampleFieldList, function (a, b) {
            if (b.column == selected_column) {
                col_index = a;
                return false;
            }
        });
        //gMappedData[currentLineNumber].sampleFieldList.splice(col_index, 1);
        $.each(gOutputData.mapFieldList, function (key, val) {
            if (val.column == selected_column) {
                temp_data["value"] = val.value;
                selected_column = key;
                return false;
            }
            //            if ((val.type = "switch" || val.type == "concat")) {
            //                temp_data["value"] = val.value;
            //                selected_column = key;
            //                return false;
            //            }
        });
        //if (typeof (selected_column) == "number")
        gOutputData.mapFieldList.splice(selected_column, 1);
    }
    else if (field_type == 'text') {
        //temp_data["name"] = 'text';
        temp_data["name"] = $('#customFieldName').val();
        temp_data["value"] = '';
        temp_data["column"] = '0';
        field_list["type"] = 'text';
        field_list["value"] = '';
        field_list["column"] = $('#descCustomField').val();
    }
    temp_data["fieldList"] = field_list;
    gOutputData.mapFieldList.push(temp_data);
    sessionStorage.jsonMappedPostData = JSON.stringify(gOutputData);
    if (field_type == "switch")
        buildLists();
    else if (field_type == "concat" || field_type == "text")
        bindCustomFields();
    $("#popupCustom").popup("close");
    return;
}

function loadMappedColumnDropdown(ddl_id) {
    $('#' + ddl_id).empty();

    var options = '';

    if (ddl_id.indexOf('Output') != "-1")
        options = '<option value="-1">Select Field</option>';
    else
        options = '<option value="-1">Select</option>';

    $.each(gOutputData.mapFieldList, function (key, val) {
        if (val.column != undefined && val.type != "text")
            options += '<option value="' + val.column + '">' + val.name + '</option>';
        else if (val.type != undefined && (val.type == "concat" || val.type == "switch"))
            options += '<option value="' + val.name + '">' + val.name + '</option>';
    });
    $('#' + ddl_id).append(options); //.selectmenu('refresh');
    loadDefaultColumns(ddl_id);
}

function loadDefaultColumns(ddl_id) {
    var options = '';
    options += '<option value="customerNumber">Customer Number</option>';
    options += '<option value="jobNumber">Job Number</option>';
    options += '<option value="clientProject">Client Project</option>';
    options += '<option value="sourcefile">Source File</option>';
    options += '<option value="sourcetype">Source Type</option>';
    options += '<option value="priority">Priority</option>';
    options += '<option value="batchCode">Batch Code</option>';
    options += '<option value="versionCode">Version Code</option>';
    $('#' + ddl_id).append(options).selectmenu('refresh');
}

function bindCustomFields() {
    var column_data;
    $('#ulCustomFields').empty();
    if (gOutputData != undefined) {
        column_data = jQuery.grep(gOutputData.mapFieldList, function (obj) {
            return (((obj.column === undefined || obj.column > -1) && (obj.fieldList != undefined && obj.fieldList.length === 0)) || (obj.fieldList != undefined && obj.fieldList.length > 0));
        });

        column_data.push();
        if (column_data.length > 0) {
            var file_mappings = "";
            file_mappings = '<li data-role="list-divider" data-theme="f">Custom Fields</li>';
            var cust_col_count = 0;
            $.each(column_data, function (key, val) {
                file_mappings += '<li data-icon="delete" id="edit"><a data-rel="popup" data-position-to="window" onclick="return editCustomFields(\'' + val.name + '\',\'' + val.type + '\')"><h3>';
                if (val.type == "concat" || val.type == "text")
                    file_mappings += val.value + '</h3><p>' + val.name + '</p><span class="ui-li-count">' + (val.type) + '</span></a><a href="#" onclick="fnRemoveCustomField(\'' + val.name + '\');">Remove List</a></li>';
                else if (val.type == "switch")
                    file_mappings += val.value + '</h3><p>' + val.name + '</p><span class="ui-li-count">' + (val.type) + '</span></a><a href="#" onclick="fnRemoveCustomField(\'' + val.name + '\');">Remove List</a></li>';
                cust_col_count++;
            });
        }
        $('#ulCustomFields').append(file_mappings);
        $('#ulCustomFields').listview('refresh');
    }
}

function editCustomFields(col_name, field_type) {

    if (field_type == 'text') {
        $('#hidCustColType').val(field_type);
        $("#hdnCustColName").val(col_name);
        var cust_col_count = 0;
        column_data = jQuery.grep(gOutputData.mapFieldList, function (obj) {
            cust_col_count++;
            return (obj.name === $("#hdnCustColName").val() && obj.type === $("#hidCustColType").val());
        });
        $.each(column_data, function (key, val) {
            fnLoadCustomTextFieldData(val.value);
        });

        $("#editTextField").popup("open");
    }
    else if (field_type == 'switch') {
        $("#hdnCustColName").val(col_name);
        $('#hidCustColType').val(field_type);
        $("#popupEditCustomSwitch").popup("open");
        var cust_col_count = 0;
        column_data = jQuery.grep(gOutputData.mapFieldList, function (obj) {
            cust_col_count++;
            return obj.name === $("#hdnCustColName").val();
        });
        $.each(column_data, function (key, val) {
            if (val.fieldList != undefined) {
                var is_default_set = false;
                $.each(val.fieldList, function (a, b) {
                    if (b.name != 'default')
                        fnBuildSwitchStatements();
                    is_default_set = (b.name == 'default') ? true : false;
                    bindSwitchStatementsData(b, a, is_default_set);
                });
            }
        });
    }
    else {
        $('#ulControls').empty();
        $('#hidCustColType').val(field_type);
        $("#hdnCustColName").val(col_name);
        var cust_col_count = 0;
        column_data = jQuery.grep(gOutputData.mapFieldList, function (obj) {
            cust_col_count++;
            return obj.name === $("#hdnCustColName").val();
        });
        $.each(column_data, function (key, val) {
            if (val.fieldList != undefined)
                fnLoadCustomFieldData(val.value, val.fieldList);
        });
    }
}

function fnLoadCustomTextFieldData(value) {
    $('#custTextField').val(value);
}

function bindSwitchStatementsData(switch_statement, index, is_default_set) {
    //$('#ddlDefaultOutput').val("-1").selectmenu('refresh').trigger('onchange');
    if (is_default_set) {
        //Default Values persistence
        $('#ddlSwitchType').val(switch_statement.type).selectmenu('refresh').trigger('onchange');
        if ($('#ddlSwitchType').val() == "text")
            $('#txtDefaultOutput').val(switch_statement.outputUsing);
        else if ($('#ddlSwitchType').val() == "field")
            $('#ddlDefaultOutput').val(switch_statement.outputUsing).selectmenu('refresh');
    }
    else {
        $.each($('div[id^=dvSwitchMain]'), function (a, b) {
            if (a == index) {
                $(this).find('input[id^=txtSwitchName]').val(switch_statement.name);
                $(this).find('input[id^=txtSwitchOccurs]').val(switch_statement.occurs);
                $(this).find('select[id^=ddlFieldName]').val(switch_statement.type).selectmenu('refresh').trigger('onchange');
                if ($(this).find('select[id^=ddlFieldName]').val() == "text")
                    $(this).find('input[id^=txtSwitchOutput]').val(switch_statement.outputUsing);
                else if ($(this).find('select[id^=ddlFieldName]').val() == "field")
                    $(this).find('select[id^=ddlSwitchOutput]').val(switch_statement.outputUsing).selectmenu('refresh');
                return false;
            }
        });
    }
}

function fnLoadCustomFieldData(field_value, field_list) {
    var control_to_add = "";
    var dropdown_count = "";
    var textfield_count = "";
    if (field_list.length > 0 || field_value != "") {
        for (i = 0; i < field_list.length; i++) {
            if (field_list[i].type == 'field') {
                dropdown_count = $('#ulControls').find('li').find("select[name^=customSelect]").length;
                var lbl_id = "customLblColValue" + (dropdown_count + 1);
                control_to_add = '<li><div data-role="fieldcontain"><h5><label id=' + lbl_id + '>' + field_list[i].value + '</label></h5></div></li>';
                control_to_add += '<li><div data-role="fieldcontain"><select data-theme="e" name="customSelect' + (dropdown_count + 1) + '" id="customSelect' + (dropdown_count + 1) + '" data-mini="true" onchange="assignColumnData(this, ' + lbl_id + ')">';
                var column_data = getCurrentRowData();
                //                for (j = 1; j <= column_data[0].sampleFieldList.length; j++) {
                //                    if (field_list[i].column == j)
                //                        control_to_add += '<option value="' + j + '" selected>Col ' + j + '</option>';
                //                    else
                //                        control_to_add += '<option value="' + j + '">Col ' + j + '</option>';
                //                                }
                control_to_add += '</select></li>';
            }
            else {
                textfield_count = $('#ulControls').find('li').find("input[type='text']").length;
                control_to_add = '<br /><li><input class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset ui-mini" type="text" name="name" id="customText' + (textfield_count + 1) + '" value="' + field_list[i].value + '" data-mini="true" /></li>';
            }
            if (control_to_add != "") {
                $('#ulControls').append(control_to_add);
                if (field_list[i].type == 'field') {
                    $("div select").each(function (i) {
                        $(this).selectmenu();
                    });
                    loadMappedColumnDropdown('customSelect' + (dropdown_count + 1));
                    $('#customSelect' + (dropdown_count + 1)).val(field_list[i].column).selectmenu('refresh');
                }
            }
        }

    }

    $("#popupEditCustom").popup("open");
}

function fnCreateDropDown(field_type) {
    var dropdown_count = $('#ulControls').find('li').find("select[name^=customSelect]").length;
    var textfield_count = $('#ulControls').find('li').find("input[type='text']").length;

    var column_data = getCurrentRowData();
    var control_to_add = "";
    if (column_data.length > 0) {
        if (field_type == 'field') {
            var lbl_id = "customLblColValue" + (dropdown_count + 1);
            control_to_add += '<li><div data-role="fieldcontain"><h5><label id=' + lbl_id + '></label></h5></div></li>';
            //control_to_add += '<li><h5><label id=' + lbl_id + '></label></h5></li>';

            //control_to_add += '<li><div class=ui-select><div class="ui-btn ui-btn-up-a ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-right" data-theme="a" data-iconpos="right" data-icon="arrow-d" data-mini="true" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperEls="span"><span class="ui-btn-inner ui-btn-corner-all">';
            //control_to_add += '<span class=ui-btn-text ><span>Select Column</span></span><span class="ui-icon ui-icon-arrow-d ui-icon-shadow">&nbsp;</span></span>';
            control_to_add += '<li><select data-theme="e" name="customSelect' + (dropdown_count + 1) + '" id="customSelect' + (dropdown_count + 1) + '" data-mini="true" onchange="assignColumnData(this, ' + lbl_id + ')">';
            //            control_to_add += '<option value="-1">Select Column</option>';
            //            for (i = 1; i <= column_data[0].sampleFieldList.length; i++) {
            //                control_to_add += '<option value="' + i + '">Col ' + i + '</option>';
            //            }
            //control_to_add += '</select></span></div></div></li>';
            control_to_add += '</select></li>';
        }
        else {
            control_to_add = '<br /><li><input class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset ui-mini" type="text" name="name" id="customText' + (textfield_count + 1) + '" value=" " data-mini="true" /></li>';
        }
    }
    $('#ulControls').append(control_to_add);
    $("#ulControls select").each(function (i) {
        $(this).selectmenu();
    });
    loadMappedColumnDropdown('customSelect' + (dropdown_count + 1));
    $("#popupEditCustom").popup("open");
}

function getCurrentRowData() {
    var column_data = [];
    column_data = jQuery.grep(gMappedData, function (obj) {
        return obj.lineNumber === currentLineNumber;
    });
    return column_data;
}

function assignColumnData(ctrl, lbl) {
    var current_control = $('#' + ctrl.id);
    var selectedText = current_control.get(0).options[$('#' + ctrl.id).get(0).selectedIndex].text;
    //to set the selected value in the drop down.
    current_control.closest('.ui-select').find('.ui-btn-text').find('span').html(selectedText);

    var selected_value = $('#' + ctrl.id).val();
    var column_data = getCurrentRowData();

    if (column_data.length > 0) {
        var insert_column_at;
        insert_column_at = jQuery.grep(column_data[0].sampleFieldList, function (obj) {
            //return obj.column === parseInt(selected_value) - 1;
            return obj.column === parseInt(selected_value);
        });
    }
    if (insert_column_at.length > 0)
        lbl.innerHTML = insert_column_at[0].value;

    // $('#ulControls').listview('refresh');
}

function fnSubmitPreview() {
    window.location.href = '../jobFileMappingPreview/jobFileMappingPreview.html?ft=' + fileType + '&fn=' + fileName;
}

function cancelData() {

    gMappedData = [];
    gOutputData = undefined;
    currentLineNumber = 0;
    startLineNumber = 0;
    endLineNumber = 10;
    numberOfRecords = 1;
    sessionStorage.removeItem("custListFieldsData");
    sessionStorage.removeItem("gOutputData");
    sessionStorage.removeItem("jsonMappedData");
    sessionStorage.removeItem("fileMapPostData");
    sessionStorage.removeItem("jsonMappedPostData");
    makeGetPostData();
    makeGData();
    buildLists();
    bindCustomFields();
}

function extendedMapOnOff(ctrl_slider_extended_map) {
    var is_file_map_on = $('#' + ctrl_slider_extended_map).val();
    if (is_file_map_on == 'off') {
        populateFieldNameDropDown(false);
    }
    else {
        populateFieldNameDropDown(true);
    }
}

function loadCustomFieldsList(ctrl) {
    var serviceUrl = '../JSON/_listSource.JSON';
    var ddl_control = $('#' + ctrl);
    var options_list = '';
    setTimeout(
    $.getJSON(serviceUrl, function (data) {
        if (typeof (data) != 'undefined') {
            $.each(data[0].FieldName, function (i, option) {
                options_list += '<option value="' + option.value + '">' + option.name + '</option>';
            });
            $(options_list).appendTo('#' + ctrl);
            $('#fieldDDL').val('-1').selectmenu('refresh');
        }
    }), 2000);
}

function resetFileMap() {
    cancelData();
    gOutputData.mapName = $("#ddlFileMap :selected").text();
    $('#spnFileMapName').text(gOutputData.mapName);
    $('#popupChangeMap').popup('close');
}

function openChangeMapPopUp() {
    $('#ddlFileMap').val('-1').selectmenu('refresh');;
    $("#ddlFileMap option:contains(" + gOutputData.mapName + ")").attr('selected', true).parent().selectmenu('refresh');;
    $('#popupChangeMap').popup('open');

}

function openSaveMapPopUp() {
    $('#ddlSaveTo').val('-1').selectmenu('refresh');;
    $('#popupSaveMap').popup('open');
}


function getSaveToInfo() {
    var selected_option = $('#ddlSaveTo').val();
    if (selected_option == "customer") {
        $('#dvCustomerId').css('display', 'block');
        $('#dvLayoutTemplate').css('display', 'none');
        $('#txtCustomerNumber').val(appPrivileges.customerNumber);
    }
    else if (selected_option == "layoutTemplate") {
        $('#dvCustomerId').css('display', 'none');
        $('#dvLayoutTemplate').css('display', 'block');
    }
}

function saveFileMapInfo() {

    gOutputData.mapName = $('#textMapFileName').val();

    var temp_save_map_info = {};
    temp_save_map_info["saveToType"] = $('#ddlSaveTo').val();
    temp_save_map_info["value"] = ($('#ddlSaveTo').val() == "customer") ? $('#txtCustomerNumber').val() : $('#ddlLayoutTemplate').val();
    gOutputData["saveTo"] = temp_save_map_info;
    fnCancel('savemap');
    $('#popupSaveMap').popup('close');
}


//******************** Public Functions End **************************
