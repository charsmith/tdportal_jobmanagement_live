﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
//var processStatusURL = 'JSON/_processStatus.JSON';
var processStatusURL = serviceURLDomain + "api/MediaworksStatus";
//******************** Global Variables End **************************
// Define a "PublisherSet" class that tracks its own name and children, and has a method to add a new child
var PublisherSet = function (category_name, children) {
    this.categoryName = category_name;
    this.id = category_name.replace(/ /g, '');
    this.children = ko.observableArray(children);
}

function createViewModel() {
    var status_model = [];
    $.each(gData, function (key, val) {
        $.each(val, function (key1, val1) {
            if (val1.status.toLowerCase() === 'declined')
                val1['dataTheme'] = 'h';
            else if (val1.status.toLowerCase() === 'pending approval')
                val1['dataTheme'] = 'e';
            if (val1.status.toLowerCase() === 'approved')
                val1['dataTheme'] = 'g';
            else
                val['dataTheme'] = 'c';
        });
        var publisher_set = new PublisherSet(key, val);
        status_model.push(publisher_set);
    });
    return status_model;
}
//******************** Page Load Events Start **************************
$('#_mediaworksProcessStatus').live('pagebeforecreate', function (event) {
    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage 
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }

    if (sessionStorage.desktop) // desktop storage 
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    else if (localStorage.mobile) // mobile storage
        return true;

    if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
        alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
        window.open('', '_self', '');
        window.close();
        return false;
    }
    //if (appPrivileges.roleName != "admin")
    //    $("#divFileMap").hide();
    displayMessage('_mediaworksProcessStatus');
	$('#okBut').attr('data-theme','i');
    fnCreateMWNavLinks('status');
   
});

$(document).on('pageshow', '#_mediaworksProcessStatus', function (event) {
    makeGData();
});

//******************** Public Functions Start **************************
function makeGData() {
    getCORS(processStatusURL, null, function (data) {
        gData = data;
        $.each(gData, function (key, val) {
            val = val.sort(function (a, b) {
                var x = parseInt(a.approvalStatus), y = parseInt(b.approvalStatus);
                if (x == y)
                    return (a.publisherName < b.publisherName) ? -1 : 1;
                else
                    return x < y ? -1 : 1;
            });
        });
        ko.applyBindings(createViewModel(), $('#divList')[0]);
        $("#divList ul").each(function (i) {
            $(this).listview();
        });

        $.each($('#divList'), function (obj) {
            $(this).collapsibleset();
        });
    }, function (error_response) {
        showErrorResponseText(error_response,true);
    });
}

function fnStatus(key, publisher_name, publisher_id, job_number) {
    appPrivileges.publisherName = publisher_name;
    saveSessionData("appPrivileges", JSON.stringify(appPrivileges));

    saveSessionData("publisherId", publisher_id);
    saveSessionData("jobNumber", job_number);

    if (key.toLowerCase().indexOf('approval') > -1) {
        //popUpApprovalUnAvailable();
        document.location.href = '../fileApproval/mediaworksFileApproval.html';
    }
    else {
        document.location.href = '../fileUpload/mediaworksFileUpload.html';
    }
}
//******************** Public Functions End **************************
