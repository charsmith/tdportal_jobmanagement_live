﻿
var manageComments = function (self) {

    self.addedComments = [];
    self.gJobComments = [];
    //Validates whether user selected at least one filter type.        
    self.filterValidate = function () {
        $("#popupFilterComments").popup('close');
        if (!($('#filterChkIS')[0].checked || $('#filterChkJobIns')[0].checked || $('#filterChkProductionReport')[0].checked)) {
            var msg = "Please select at least one filter option"
            $('#alertmsg').html(msg);
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupFilterComments').popup('open');");
            return false;
        }
        return true;
    };
    //Filters the comments and displayed over the page.
    self.filterComments = function () {
        if (!(self.filterValidate()))
            return false;
        $("#popupFilterComments").popup('close');
        self.loadComments();
        window.setTimeout(function getCommentsDelay() {
            self.createComments(self.addedComments);
        }, 1000);
    }
    //Loads the comments over the page based on the selected filters.
    self.loadComments = function () {
        var filterComments = {};
        filterComments["wantsIS"] = ($('#filterChkIS').attr('checked') == 'checked') ? 1 : 0;
        filterComments["wantsJobInstructions"] = ($('#filterChkJobIns').attr('checked') == 'checked') ? 1 : 0;
        filterComments["wantsProductionReport"] = ($('#filterChkProductionReport').attr('checked') == 'checked') ? 1 : 0;
        postCORS(serviceURLDomain + "api/Comments/" + self.facilityId + "/" + self.jobNumber, JSON.stringify(filterComments), function (comments_data) {
            //getCORS(serviceURLDomain + "api/Comments/" + facilityId + "/" + jobNumber, null, function (comments_data) {
            if (comments_data != "" && comments_data.length > 0) {
                self.addedComments = comments_data;
            }
        }, function (error_response) {
            showErrorResponseText(error_response,true);
            self.addedComments = [];
        });
    };

    //Change event for filter milestones.
    self.selectFilters = function (ctrl) {
        if ($('#' + ctrl.id).is(':checked'))
            $('#filterChkViewAllComments').attr('checked', false).checkboxradio('refresh');

        if ($('#popupFilterComments input[type=checkbox][id!=filterChkViewAllComments]:checked').length == 0)
            $('#filterChkViewAllComments').attr('checked', true).checkboxradio('refresh');
    };
    //Creates the comment items.
    self.createComments = function (comments_info) {
        $('#ulComments').empty();
        var li_comments = "";
        if (appPrivileges.customerNumber == "1") {
            li_comments += '<li data-theme="f" data-icon="myapp-filter"><a href="#" id="btnFilterComments" data-role="button"   data-iconpos="notext" data-mini="true" data-inline="true" data-theme="e" onclick="$(\'#popupFilterComments\').popup(\'open\');">Comments</a></li>';
            li_comments += '<li data-theme="f" data-icon="myapp-comment"><a href="#popupComments" data-rel="popup" data-position-to="window" data-inline="true">Add Comments</a></li>';
        }
        //    li_comments += '<li data-theme="b" data-icon="myapp-comment"><a href="#popupComments" data-rel="popup" data-position-to="window" data-inline="true">Add Comments</a><a href="#" id="btnFilterComments" data-role="button" data-icon="myapp-filter"  data-iconpos="notext" data-mini="true" data-inline="true" data-theme="b" onclick="$(\'#popupFilterComments\').popup(\'open\');">Filter Comments</a></li>';
        //else
        //    li_comments += '<li data-theme="b" data-icon="myapp-comment"><a href="#popupComments" data-rel="popup" data-position-to="window" data-inline="true">Add Comments</a></li>';
        comments_info.reverse();
        comments_info = comments_info.sort(function (a, b) {
            return new Date(b.created) - new Date(a.created);
        });
        $.each(comments_info, function (keyComment, valComment) {
            var img_icons;
            img_icons = (valComment.wantsIS == "1") ? '<img src="' + logoPath + 'mileStoneIcons/iconList.png" width="16" height="16" title=" IS "/>' : "";
            img_icons += (valComment.wantsJobInstructions == "1") ? '<img src="' + logoPath + 'mileStoneIcons/iconWhitepaper.png" width="16" height="16" title="Job Instructions" />' : "";
            img_icons += (valComment.wantsProductionReport == "1") ? '<img src="' + logoPath + 'mileStoneIcons/iconProduction.png" width="16" height="16" title="Production Schedule"  />' : "";
            li_comments += '<li><h3><span style="font-size:12px; font-weight:normal;">' + valComment.userName + ' - ' + valComment.created + '</span></h3><p><span class="wrapword" style="font-size:14px">' + valComment.comment + '</span></p><span><p>' + img_icons + '</p></span></li>';
        });
        $('#ulComments').append(li_comments).listview('refresh');
    };
    //Adds the new comments to the job.
    self.addJobComments = function () {
        //    var new_comment = {
        //        "emailId": "ajaysaptaputre@yahoo.com",
        //        "date": setDateFormatAMPM(new Date()),
        //        "comment": $('#txtJobComments').val()
        //    }
        //    addedComments.push(new_comment);
        var msg = '';
        if ($('#txtJobComments').val() == "" || jQuery.trim($('#txtJobComments').val()) == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter the comment";
            else
                msg = "Please enter the comment";
        }
        if (appPrivileges.customerNumber == "1" && appPrivileges.roleName == "admin") {
            if (!($('#chkIS')[0].checked || $('#chkJobIns')[0].checked || $('#chkProductionReport')[0].checked)) {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please select at least one flag";
                else
                    msg = "Please select at least one flag";

            }
        }

        $('#popupComments').popup('open');
        $('#popupComments').popup('close');
        if (msg != "") {
            $('#alertmsg').html(msg);
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupComments').popup('open');");
            return false;
        }
        $('#txtJobComments').attr('style', 'height:50px');

        var flag_comments = {};
        flag_comments["wantsIS"] = ($('#chkIS').attr('checked') == 'checked') ? 1 : 0;
        flag_comments["wantsJobInstructions"] = ($('#chkJobIns').attr('checked') == 'checked') ? 1 : 0;
        flag_comments["wantsProductionReport"] = ($('#chkProductionReport').attr('checked') == 'checked') ? 1 : 0;

        var new_comment = {
            "userName": sessionStorage.username,
            "comment": $('#txtJobComments').val(),
            "flagComments": flag_comments
        }
        self.addedComments.push(new_comment);

        if (Object.keys(new_comment).length > 0) {
            //var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
            var post_comments_url = serviceURLDomain + 'api/Comments_post/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
            postCORS(post_comments_url, JSON.stringify(new_comment), function (response) {
                if (response != "") {
                    msg = 'Comments data was saved.';
                    self.loadComments();
                    window.setTimeout(function getCommentsDelay() {
                        self.createComments(self.addedComments);
                    }, 1000);
                }
                else
                    msg = "Your comments data was not saved.";
                $('#txtJobComments').val('');
                $('#chkIS').attr('checked', false).checkboxradio('refresh');
                $('#chkJobIns').attr('checked', false).checkboxradio('refresh');
                $('#chkProductionReport').attr('checked', false).checkboxradio('refresh');
                $('#alertmsg').text(msg);
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                $('#popupDialog').popup('open');
            }, function (response_error) {
                ////var msg = 'Error in saving.';
                //var msg = response_error.responseText;
                //msg = "Error: Contact Administrator."
                //if (msg != "") {
                //    $('#alertmsg').text(msg);
                //    $('#popupDialog').popup('open');
                //}
                showErrorResponseText(error_response, false);
            });
        }
    };
};