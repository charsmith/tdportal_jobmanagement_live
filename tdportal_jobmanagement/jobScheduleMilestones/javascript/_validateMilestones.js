﻿var validateMilestones = function (self) {
    //Validates the user entered date and persists.
    self.dateValidationAndPersist = function (ctrl, number, val) {
        if (fnDateFormatMMDDYYYY(ctrl, number)) {
            if ($("#txtinhome").val() != "") {
                self.fnSaveMilestones(val);
            }
        }
        else
            return false;
    };
    //Validates the milestone dates entered by user in a popup.
    self.popupValidation = function (val, sched_date, cmpl_date, type) {
        var msg = "";
        var in_home_date;
        var list_arrival_date;
        var art_arrival_date;
        var sale_start_date;
        var sale_end_date;
        //eval(getURLDecode(pagePrefs.scriptBlockDict.popupValidation));
        var sel_date = (type == "scheduledDate") ? sched_date : cmpl_date;
        $.each(val, function (key1, val1) {
            if (key1.toLowerCase().indexOf('inhome') > -1) {
                in_home_date = ($("#hdnSelectedMilestone").val() == "inhome") ? new Date(sel_date) : ((val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "");
            }
            if (key1.toLowerCase().indexOf('list') > -1) {
                list_arrival_date = ($("#hdnSelectedMilestone").val() == "list") ? new Date(sel_date) : ((val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "");
            }
            if (key1.toLowerCase().indexOf('artarrival') > -1) {
                art_arrival_date = ($("#hdnSelectedMilestone").val() == "artarrival") ? new Date(sel_date) : ((val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "");
            }
            if (key1.toLowerCase().indexOf('salestart') > -1) {
                sale_start_date = ($("#hdnSelectedMilestone").val() == "salestart") ? new Date(sel_date) : ((val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "");
            }
            if (key1.toLowerCase().indexOf('saleend') > -1) {
                sale_end_date = ($("#hdnSelectedMilestone").val() == "saleend") ? new Date(sel_date) : ((val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "");
            }
        });
        var current_date = new Date();

        var tmp_in_home_date = new Date(($("#hdnSelectedMilestone").val() == "inhome") ? sel_date : val.inhome[type]);

        var list_date = (list_arrival_date != "") ? new Date(list_arrival_date).setHours(0, 0, 0, 0) : "";
        var curr_date = (current_date != "") ? new Date(current_date).setHours(0, 0, 0, 0) : "";
        var art_date = (art_arrival_date != "") ? new Date(art_arrival_date).setHours(0, 0, 0, 0) : "";

        if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) { //cw        

            if (tmp_in_home_date != "" && tmp_in_home_date != "Invalid Date") { //In-home date is not compulsory field, it may or may not exists based on the customer
                tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 :4));

                var tmp_in_date = (tmp_in_home_date != "") ? new Date(tmp_in_home_date).setHours(0, 0, 0, 0) : "";

                switch (sessionStorage.userRole.toLowerCase()) {
                    case "admin":
                        if (in_home_date != "" && in_home_date < current_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should be always greater than current date.";
                        }
                        if ((list_date != "" && !isNaN(list_date)) && (list_date < curr_date || !(list_date <= tmp_in_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Data/List arrival date should be set between the current date and at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }

                        if ((art_date != "" && !isNaN(art_date)) && (art_date < curr_date || !(art_date <= tmp_in_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Art arrival date should be set between the current date and at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }
                        break;
                    case "power":
                    case "user":
                        var tmp_current_date = new Date();
                        var tmp_in_home_date = new Date(($("#hdnSelectedMilestone").val() == "inhome") ? sel_date : val.inhome[type]);
                        var tmp_current_with_inhome_date = new Date();
                        tmp_current_with_inhome_date.setDate(new Date().getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 7 : ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? 15 : 21)));
                        tmp_current_with_inhome_date.setHours(0, 0, 0, 0); //only date part need to compare

                        tmp_current_date.setDate(tmp_current_date.getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10));
                        tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10));
                        tmp_in_date = (tmp_in_home_date != "") ? new Date(tmp_in_home_date).setHours(0, 0, 0, 0) : "";

                        if (in_home_date != "" && in_home_date < tmp_current_with_inhome_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should always be greater than " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 7 : ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? 15 : 21)) + " days from current date.";
                        }

                        if (in_home_date != "" && in_home_date <= tmp_current_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should be always no closer than " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days from the date provided for when Art Arrival date & Data/List arrival date.";
                        }

                        if (art_date != "" && (art_date < curr_date || art_date > tmp_in_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Art Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days prior to the in-home date.";
                        }

                        if (list_date != "" && (list_date < curr_date || list_date > tmp_in_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Data/List Arrival date should be set between the current date and  " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days prior to the in-home date.";
                        }
                        break;
                }
            }
            if (sale_start_date != "" && sale_end_date != "" && sale_start_date > sale_end_date) {
                if (msg != "")
                    msg += "</br></br>";
                msg += "Sale end date should be greater than or equal to Sale start date.";
            }
        }
        else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) { //cgs
            switch (sessionStorage.userRole.toLowerCase()) {
                case "admin":
                    if (in_home_date != "" && in_home_date < current_date) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "In-Home date should be always greater than current date.";
                    }
                    break;
                case "power":
                case "user":
                    var tmp_current_date = new Date();
                    var tmp_in_home_date = new Date(($("#hdnSelectedMilestone").val() == "inhome") ? sel_date : val.inhome[type]);
                    var tmp_current_with_inhome_date = new Date();
                    tmp_current_with_inhome_date.setDate(new Date().getDate() + 21);
                    tmp_current_with_inhome_date.setHours(0, 0, 0, 0); //only date part need to compare

                    if (in_home_date != "" && in_home_date < tmp_current_with_inhome_date) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "In-Home date should always be greater than 21 days from current date.";
                    }
                    break;
            }
        }
        else if (jobCustomerNumber == "1") { //TD        
            if (tmp_in_home_date != "" && tmp_in_home_date != "Invalid Date") { //In-home date is not compulsory field, it may or may not exists based on the customer
                tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4));

                var tmp_in_date = (tmp_in_home_date != "") ? new Date(tmp_in_home_date).setHours(0, 0, 0, 0) : "";

                switch (sessionStorage.userRole.toLowerCase()) {
                    case "admin":
                        if (in_home_date != "" && in_home_date < current_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should be always greater than current date.";
                        }
                        if ((list_date != "" && !isNaN(list_date)) && (list_date < curr_date || !(list_date <= tmp_in_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Data/List arrival date should be set between the current date and at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }

                        if ((art_date != "" && !isNaN(art_date)) && (art_date < curr_date || !(art_date <= tmp_in_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Art arrival date should be set between the current date and at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }
                        break;
                    case "power":
                    case "user":
                        var tmp_current_date = new Date();
                        var tmp_in_home_date = new Date(($("#hdnSelectedMilestone").val() == "inhome") ? sel_date : val.inhome[type]);
                        var tmp_current_with_inhome_date = new Date();
                        tmp_current_with_inhome_date.setDate(new Date().getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 :10)); //10 days for TD normal user
                        tmp_current_with_inhome_date.setHours(0, 0, 0, 0); //only date part need to compare

                        tmp_current_date.setDate(tmp_current_date.getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7)); //7 days for TD normal user
                        tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7)); //7 days for TD normal user
                        tmp_in_date = (tmp_in_home_date != "") ? new Date(tmp_in_home_date).setHours(0, 0, 0, 0) : "";

                        if (in_home_date != "" && in_home_date < tmp_current_with_inhome_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should always be greater than " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days from current date.";
                        }

                        if (in_home_date != "" && in_home_date <= tmp_current_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should be always no closer than " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7) + " days from the date provided for when Art Arrival date & Data/List arrival date.";
                        }

                        if (art_date != "" && (art_date < curr_date || art_date > tmp_in_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Art Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7) + " days prior to the in-home date.";
                        }

                        if (list_date != "" && (list_date < curr_date || list_date > tmp_in_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Data/List Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7) + " days prior to the in-home date.";
                        }
                        break;
                }
            }
            if (sale_start_date != "" && sale_end_date != "" && sale_start_date > sale_end_date) {
                if (msg != "")
                    msg += "</br></br>";
                msg += "Sale end date should be greater than or equal to Sale start date.";
            }
        }
        //if (appPrivileges.customerNumber == "1") { //TD        
        //    $.each(val, function (key, value) {
        //        if (value.isRequired == 1 && $("#" + type).val() == "" && type == "scheduledDate") {
        //            if (msg != "")
        //                msg += "</br>";
        //            msg += value.description + " date is mandatory!";
        //        }
        //    });
        //}
        return msg;
    };
    //validates the milestones in the list item with respect to inhome date.
    self.fnMilestonesValidation = function () {
        var msg = "";
        //eval(getURLDecode(pagePrefs.scriptBlockDict.fnMilestonesValidation));

        var in_home_date = ($("#txtinhome").val() == undefined || $("#txtinhome").val() == "") ? "" : new Date($("#txtinhome").val());
        var list_arrival_date = ($("#txtlist").val() == undefined || $("#txtlist").val() == "") ? "" : new Date($("#txtlist").val());
        var art_arrival_date = ($("#txtartarrival").val() == undefined || $("#txtartarrival").val() == "") ? "" : new Date($("#txtartarrival").val());
        var sale_start_date = ($("#txtsalestart").val() == undefined || $("#txtsalestart").val() == "") ? "" : new Date($("#txtsalestart").val());
        var sale_end_date = ($("#txtsaleend").val() == undefined || $("#txtsaleend").val() == "") ? "" : new Date($("#txtsaleend").val());
        var current_date = new Date();
        var sign_up_start = new Date($("#txtsignupstart").val());
        var sign_up_end = new Date($("#txtsignupend").val());
        var approvals_start = new Date($("#txtapprovalsstart").val());
        var approvals_end = new Date($("#txtapprovalsend").val());
        var in_home_date = new Date($("#txtinhome").val());

        var tmp_in_home_date = ($("#txtinhome").val() == undefined || $("#txtinhome").val() == "") ? "" : new Date($("#txtinhome").val());
        var list_date = (list_arrival_date != "") ? new Date(list_arrival_date).setHours(0, 0, 0, 0) : "";
        var curr_date = (current_date != "") ? new Date(current_date).setHours(0, 0, 0, 0) : "";
        var art_date = (art_arrival_date != "") ? new Date(art_arrival_date).setHours(0, 0, 0, 0) : "";

        if (self.gOutputData != undefined && self.gOutputData != null && self.gOutputData != "") {
            $.each(self.gOutputData["milestoneAction"].milestoneDict, function (key, value) {
                key = key.toLowerCase();
                if ($("#hdnMilestoneFilter").val() != 'express') {
                    if (value.isRequired == 1 && ($("#txt" + key).val() == "")) {
                        if (msg != "")
                            msg += "</br>";
                        msg += value.description + " date is mandatory!";
                    }
                }
            });
        }

        if ((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) && $("#hdnMilestoneFilter").val() != 'standard') { //cw
            if ($("#hdnMilestoneFilter").val() != 'express') {
                if (tmp_in_home_date != "") { //In-home date is not compulsory field, it may or may not exists based on the customer
                    tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4));

                    var tmp_in_date = (tmp_in_home_date != "") ? new Date(tmp_in_home_date).setHours(0, 0, 0, 0) : "";
                    switch (sessionStorage.userRole.toLowerCase()) {
                        case "admin":
                            if (in_home_date != "" && in_home_date < current_date) {
                                if (msg != "")
                                    msg += "</br></br>";
                                msg += "In-Home date should be always greater than current date.";
                            }

                            if (list_date != "" && (list_date < curr_date || !(list_date <= tmp_in_date))) {
                                if (msg != "")
                                    msg += "</br></br>";
                                msg += "Data/List arrival date should be set between the current date and at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                            }

                            if (art_date != "" && (art_date < curr_date || !(art_date <= tmp_in_date))) {
                                if (msg != "")
                                    msg += "</br></br>";
                                msg += "Art arrival date should be set between the current date and at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                            }
                            break;
                        case "power":
                        case "user":
                            var tmp_current_date = new Date();
                            var tmp_in_home_date = ($("#txtinhome").val() == undefined) ? "" : new Date($("#txtinhome").val());
                            var tmp_current_with_inhome_date = new Date();

                            tmp_current_with_inhome_date.setDate(new Date().getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 7 : ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? 15 : 21)));
                            tmp_current_with_inhome_date.setHours(0, 0, 0, 0); //only date part need to compare

                            tmp_current_date.setDate(tmp_current_date.getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10));
                            tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10));
                            tmp_in_date = (tmp_in_home_date != "") ? new Date(tmp_in_home_date).setHours(0, 0, 0, 0) : "";

                            if (in_home_date != "" && in_home_date < tmp_current_with_inhome_date) {
                                if (msg != "")
                                    msg += "</br></br>";
                                msg += "In-Home date should always be greater than " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 7 : ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? 15 : 21)) + " days from current date.";
                            }

                            if (in_home_date != "" && in_home_date <= tmp_current_date) {
                                if (msg != "")
                                    msg += "</br></br>";
                                msg += "In-Home date should be always no closer than  " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + "  days from the date provided for when Art Arrival date & Data/List arrival date.";
                            }

                            if (list_date != "" && (list_date < curr_date || list_date > tmp_in_date)) {
                                if (msg != "")
                                    msg += "</br></br>";
                                msg += "The Data/List Arrival date should be set between the current date and  " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + "  days prior to the in-home date.";
                            }

                            if (art_date != "" && (art_date < curr_date || art_date > tmp_in_date)) {
                                if (msg != "")
                                    msg += "</br></br>";
                                msg += "The Art Arrival date should be set between the current date and  " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + "  days prior to the in-home date.";
                            }
                            break;
                    }
                }

                if (sale_start_date != "" && sale_end_date != "" && sale_start_date > sale_end_date) {
                    if (msg != "")
                        msg += "</br></br>";
                    msg += "Sale end date should be greater than or equal to Sale start date.";
                }
            }
        }
        else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) { //cgs    

            // var current_date = new Date();
            if ($("#hdnMilestoneFilter").val() != 'express') {
                var tmp_in_home_date = new Date($("#inHomeDate").val());
                tmp_in_home_date.setDate(tmp_in_home_date.getDate() - 4);

                switch (sessionStorage.userRole.toLowerCase()) {
                    case "admin":
                        if (in_home_date != "" && in_home_date < current_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should be always greater than current date.";
                        }
                        break;
                    case "power":
                    case "user":
                        var tmp_current_with_inhome_date = new Date();
                        tmp_current_with_inhome_date.setDate(new Date().getDate() + 21);
                        tmp_current_with_inhome_date.setHours(0, 0, 0, 0); //only date part need to compare
                        if (in_home_date != "" && in_home_date < tmp_current_with_inhome_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should always be greater than 21 days from current date.";
                        }

                        var sign_start_date_diff = dateDifference(sign_up_start, in_home_date);
                        if (sign_start_date_diff < 42)
                            msg += "The sign up start date can be no less than 6 weeks prior to the In-Home.</br></br>";

                        var approvals_start_date_diff = dateDifference(approvals_start, in_home_date);
                        if (approvals_start_date_diff < 28)
                            msg += "The approval start date can be no less than 4 weeks prior to the In-Home.</br></br>";

                        var sign_approvals_start_date_diff = dateDifference(sign_up_start, approvals_start);
                        if (sign_approvals_start_date_diff < 14)
                            msg += "The sign up start date should be at least 2 weeks from the approvals start date.</br></br>";

                        var sign_end_date_diff = dateDifference(sign_up_start, sign_up_end);
                        if (sign_end_date_diff < 7)
                            msg += "Sign up end date should be at least be 1 week from Sign up start date.</br></br>";

                        var approvals_end_date_diff = dateDifference(approvals_start, approvals_end);
                        if (approvals_end_date_diff < 7)
                            msg += "Approvals end date should be at least be 1 week from Approvals start date.";
                }
            }
        }
        else if (jobCustomerNumber == "1" && $("#hdnMilestoneFilter").val() == 'rush') { //TD
            //$.each(self.gOutputData["milestoneAction"].milestoneDict, function (key, value) {
            //    if (value.isRequired == 1 && $("#txt" + key).val() == "") {
            //        if (msg != "")
            //            msg += "</br>";
            //        msg += value.description + " date is mandatory!";
            //    }
            //});

            if (tmp_in_home_date != "") { //In-home date is not compulsory field, it may or may not exists based on the customer
                tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4));

                var tmp_in_date = (tmp_in_home_date != "") ? new Date(tmp_in_home_date).setHours(0, 0, 0, 0) : "";
                switch (sessionStorage.userRole.toLowerCase()) {
                    case "admin":
                        if (in_home_date != "" && in_home_date < current_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should be always greater than current date.";
                        }

                        if (list_date != "" && (list_date < curr_date || !(list_date <= tmp_in_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Data/List arrival date should be set between the current date and at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }

                        if (art_date != "" && (art_date < curr_date || !(art_date <= tmp_in_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Art arrival date should be set between the current date and at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }
                        break;
                    case "power":
                    case "user":
                        var tmp_current_date = new Date();
                        var tmp_in_home_date = ($("#txtinhome").val() == undefined) ? "" : new Date($("#txtinhome").val());
                        var tmp_current_with_inhome_date = new Date();

                        tmp_current_with_inhome_date.setDate(new Date().getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10)); //10 days for TD normal user
                        tmp_current_with_inhome_date.setHours(0, 0, 0, 0); //only date part need to compare

                        tmp_current_date.setDate(tmp_current_date.getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7)); //7 days for TD normal user
                        tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7)); //7 days for TD normal user
                        tmp_in_date = (tmp_in_home_date != "") ? new Date(tmp_in_home_date).setHours(0, 0, 0, 0) : "";

                        if (in_home_date != "" && in_home_date < tmp_current_with_inhome_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should always be greater than " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days from current date.";
                        }

                        if (in_home_date != "" && in_home_date <= tmp_current_date) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "In-Home date should be always no closer than " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7) + " days from the date provided for when Art Arrival date & Data/List arrival date.";
                        }

                        if (list_date != "" && (list_date < curr_date || list_date > tmp_in_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Data/List Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7) + " days prior to the in-home date.";
                        }

                        if (art_date != "" && (art_date < curr_date || art_date > tmp_in_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Art Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7) + " days prior to the in-home date.";
                        }
                        break;
                }
            }

            if (sale_start_date != "" && sale_end_date != "" && sale_start_date > sale_end_date) {
                if (msg != "")
                    msg += "</br></br>";
                msg += "Sale end date should be greater than or equal to Sale start date.";
            }
        }

        if (jobCustomerNumber == "1") { //TD        
            if ($("#hdnMilestoneFilter").val() == 'standard') {
                $.each(self.gOutputData["milestoneAction"].milestoneDict, function (key, value) {
                    if (value.isRequired == 1 && value["scheduledDate"] == "") {
                        if (msg != "")
                            msg += "</br>";
                        msg += value.description + " date is mandatory!";
                    }
                });
            }
        }
        if (msg != undefined && msg != "" && msg != null) {
            $('#alertmsg').html(msg);
            $("#popupDialog").popup('open');
            return false;
        }
        else {
            //        //persisting comments section - START
            //        if (addedComments.length > 0) {
            //            gOutputData.commentsAction.commentsDict = addedComments.reverse();
            //            addedComments = [];
            //        } if (gOutputData.commentsAction.commentsDict.length > 0)
            //            sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
            //        //persisting comments section - END

            return true;
        }
    }
};
