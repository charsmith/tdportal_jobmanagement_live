﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
//var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;

var pageObj;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
$('#_jobMilestones').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobMilestones');
    if (sessionStorage.scheduleMilestonesPrefs == undefined || sessionStorage.scheduleMilestonesPrefs == null || sessionStorage.scheduleMilestonesPrefs == "")
        if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == SUNTIMES_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
            getPagePreferences('scheduleMilestones.html', sessionStorage.facilityId, CW_CUSTOMER_NUMBER);
        else
            getPagePreferences('scheduleMilestones.html', sessionStorage.facilityId, jobCustomerNumber);
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.scheduleMilestonesPrefs));
    }
    
    //test for mobility...
    //loadMobility();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    pageObj = new milestonesSetup();
    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        setDemoHintsSliderValue();
        //window.setTimeout(function loadHints() {
        createDemoHints("jobMilestones");
        // }, 100);
    }
    window.setTimeout(function loadHints() {
        $('#sldrShowHints').trigger('create');
    }, 3000);
});

$(document).on('pageshow', '#_jobMilestones', function (event) {
    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        $('#mileStoneHints p a').text('Turn Hints Off');
    }
    if (!dontShowHintsAgain && (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isMileStonesDemoHintsDisplayed == undefined || sessionStorage.isMileStonesDemoHintsDisplayed == null || sessionStorage.isMileStonesDemoHintsDisplayed == "false")) {
        sessionStorage.isMileStonesDemoHintsDisplayed = true;
        $('#mileStoneHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#mileStoneHints').popup('open', { positionTo: '#ulMileStones' });
        }, 500);
    }
    pageObj.loadData();
    if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) && sessionStorage.userRole == "admin") {
        $('#btnSaveTop').css('visibility', 'visible');
        $('#btnSave').css('visibility', 'visible');
        //$('#selMilestones').css('visibility', 'visible');

        $('#btnSaveTop').css("display", "");
        $('#btnSave').css("display", "");
        $('#containDiv div').css("display", "block");
    }

    if (appPrivileges.customerNumber != "1") {
        $('#dvFlagComments').css('display', 'none');
        $('#dvCommentsDisplay').css('display', 'none');
    }
    $('#filterChkViewAllComments').attr('checked', true).checkboxradio('refresh');
    $("#popupComments").bind({
        popupafterclose: function (event, ui) {
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
        }
    });
    persistNavPanelState();
    $('#sldrShowHints').slider("refresh");
    //$('#divBottom td').first().find('label').remove();
    //$('#divBottom td').first().css('width', '100px');
    //$('#tdShowHints select').remove();
});