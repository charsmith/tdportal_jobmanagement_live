﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************


//******************** Global Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_uploadArt').live('pagebeforecreate', function (event) {
    if (sessionStorage["headerValue"] != undefined && sessionStorage["headerValue"] != null && sessionStorage["headerValue"] != "")
        var headerVal = sessionStorage.headerValue;
    $("#headerH2").text(headerVal);
    displayNavLinks();
});

$(document).on('pageshow', '#_uploadArt', function (event) {
    
});

//******************** Page Load Events End **************************

//******************** Public Functions Start **************************

//******************** Public Functions End **************************