﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gCompanyInfoServiceUrl = "../adminTemplateSetup/JSON/_companyInfoConfig.JSON";
var gServiceCompanyData;
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_templateSetup').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_templateSetup');
    createConfirmMessage("_templateSetup");
    getCompanyInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
});

$(document).on('pageshow', '#_templateSetup', function (event) {
    window.setTimeout(function () {
        pageObj = new loadCompanyInfo();
        ko.applyBindings(pageObj);
        //window.setTimeout(function () {
        $('input[type=checkbox]').trigger('create');//.checkboxradio();
        $('div').trigger('create');
        //}, 500);

    }, 500);
});

var getCompanyInfo = function () {
    if (sessionStorage.templateSetupConfigJSON != undefined && sessionStorage.templateSetupConfigJSON != null && sessionStorage.templateSetupConfigJSON != "") {
        gServiceCompanyData = $.parseJSON(sessionStorage.templateSetupConfigJSON);
    }
    else {
        $.getJSON(gCompanyInfoServiceUrl, function (data) {
            gServiceCompanyData = data;
        });
    }
};

var fieldInfo = function (label, value, is_required, show_chkbox) {
    var self = this;
    self.label = ko.observable('');
    self.label(label);
    self.value = ko.observable('');
    self.value(value);
    self.isRequired = ko.observable('');
    self.isRequired(is_required);
    self.showCheckBox = ko.observable('');
    self.showCheckBox(show_chkbox);
};

var loadCompanyInfo = function () {
    var self = this;
    self.varCompanyInfo = ko.observable({});
    //ko.mapping.fromJS(gServiceCompanyData, {}, self.companyInfo);
    var temp_arr = [];
    var temp_arr1 = ko.observableArray([]);
    var temp_key = "";
    var index = 0;
    var temp_company_info = {};
    var show_chk = false;

    $.each(gServiceCompanyData, function (key, val) {
        temp_key = key;
        temp_company_info[temp_key] = ko.observableArray([]);
        if (temp_key == "companyInfo")
            show_chk = true;
        else
            show_chk = false;
        $.each(val, function (key1, val1) {
            temp_company_info[temp_key].push(new fieldInfo(val1.label, val1.value, val1.isRequired, show_chk));
        });

    });
    if (Object.keys(temp_company_info).length > 0) {
        self.varCompanyInfo(temp_company_info);
        temp_company_info = {};
    }

    self.showOrHideField = function (data, event) {
        var ele = event.targetElement || event.target;
        data.isRequired($(ele).is(':checked'));
        $('input[type=checkbox]').checkboxradio('refresh');
    };
    self.addCustomFieldClick = function (data, event) {
        //$('#popupNewCustomField').popup('open');
    };
    self.addCustomField = function (data, event) {
        var label_name = $('#txtLabel').val();
        var temp_field = {
            "label": label_name,
            "value": "",
            "isRequired": "true"
        };
        var my_cust_field = {};
        ko.mapping.fromJS(temp_field, {}, my_cust_field);
        data.varCompanyInfo().customInfo.push(new fieldInfo(temp_field.label, temp_field.value, temp_field.isRequired, "true"));
        $('div').trigger('create');
        $('#popupNewCustomField').popup('close');
    };
    self.deleteCustomField = function (data, event) {
        var ele  = event.targetElement || event.target;
        var parent_context = ko.contextFor(ele).$parent;
        parent_context.varCompanyInfo().customInfo.remove(data);
    };

    self.continueToNextPage = function (data,event) {

        var config_json = ko.mapping.toJS(self.varCompanyInfo);
        sessionStorage.templateSetupConfigJSON = JSON.stringify(config_json);

        window.location.href = "variableCompanyInfo.html";
    };
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if ((type == "template")) {
        window.location.href = " ../adminTemplateSetup/templateSetup.html";
    }
    else if ((type == "vcc")) {
        window.location.href = " ../adminTemplateSetup/variableCompanyInfo.html";
    }
};


