﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var serviceURL = "../jobSubmitOrder/JSON/_submitOrder.JSON";
var customerEmailsURL = "../jobSubmitOrder/JSON/_customerEmails.JSON";
var gCheckoutServiceURL = serviceURLDomain + "api/CheckOut/";
//var gCheckoutServiceURL = serviceURLDomainInternal + "api/CheckOut/";
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var pageObj;
//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobSubmitOrder').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobSubmitOrder');
    loadingImg("_jobSubmitOrder");
    pageObj = new jobSubmitOrder();
    pageObj.getApproversList();
    pageObj.makeEmails();
    pageObj.showExecuteConfirmationToAdmin();
    //pageObj.displayOrderSummary();
    if (sessionStorage.approvalCheckoutPrefs == undefined || sessionStorage.approvalCheckoutPrefs == null || sessionStorage.approvalCheckoutPrefs == "")
        if (appPrivileges.customerNumber == "203" || appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || appPrivileges.customerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == "6000140" || appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER)
            getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, CW_CUSTOMER_NUMBER);
        else
            getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, appPrivileges.customerNumber);
    else
        managePagePrefs(jQuery.parseJSON(sessionStorage.approvalCheckoutPrefs));

    if (sessionStorage.userRole != 'admin') {
        $('#btnCountApprovalClear').hide();
        $('#btnArtApprovalClear').hide();
        $('#btnExecute').css('visibility', 'hidden');
        $('#btnExecute').css("display", "");
        $('#btnContinueTop').css('display', 'none');
    }
    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == "6000140" || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER) {
        $('#dvConfirmationEmailData').css('display', 'none');
        if ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER) && appPrivileges.roleName == "user")
            $('#btnSubmitOrder').css('display', 'none');
        //$('#pSubmitInfoMsg').text('Please review your order below. If both artwork and list upload sections have been completed you are ready to submit your order. Click the Submit Order button below and a confirmation and approval email will be sent.');
        window.setTimeout(function loadHints() {
            createDemoHints("jobSubmitOrder");
        }, 50);
    }
    else {
        $('#dvOrderSummary').css('display', 'none');
        if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
            $('#dvConfirmationEmailData').css('display', 'none');
        }

    }

    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });

    if (sessionStorage.userRole == 'admin') {
        $('#fsApprovalTypes').css("display", "block"); //display approval checkboxes only for admins
    }
});

$(document).on('pageshow', '#_jobSubmitOrder', function (event) {
    pageObj.makeGData();
    persistNavPanelState();
    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == "6000140" || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER) {
        $("#tblOrderGrid").css("display", "none");
        $('#tblOrderGrid thead').css('display', 'none');
    }
    if (sessionStorage.userRole != 'admin') {
        $('#btnSaveTop').addClass('ui-first-child');
    } 
    var submit_order_msg = "Please review your order below. If both artwork and list upload sections have been completed you are ready to submit your order. Click the Submit Order button below and a confirmation and approval email will be sent.";
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == "6000140") {
        submit_order_msg  = 'Please review your order below. If the list upload section has been completed you are ready to submit your order. Click the Submit Order button and wait for the success message. Once submitted you will receive a confirmation email.';}
    $('#pSubmitInfoMsg').html(submit_order_msg);
//    var hint_text = 'When you submit your order, by clicking the "Submit Order" button, an email will be sent to the user placing the order. This will include a Job Summary Report';
//    hint_text += (jobCustomerNumber == BBB_CUSTOMER_NUMBER) ? ' and link to the Proofing Module to review your artwork' : '.';
//    $('#hintText').html(hint_text);
    if ((jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == "6000140" || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isSubmitOrderDemoHintsDisplayed == undefined || sessionStorage.isSubmitOrderDemoHintsDisplayed == null || sessionStorage.isSubmitOrderDemoHintsDisplayed == "false")) {
        $('#approveEmailDemoHints').popup('open', { positionTo: '#txtEmails' });
        sessionStorage.isSubmitOrderDemoHintsDisplayed = true;
    }
});
//******************** Page Load Events End **************************