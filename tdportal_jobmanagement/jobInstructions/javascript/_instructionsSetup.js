﻿

var InstructionVM = function (self) {
    var component = function () {
        var self = this;
        self.component = ko.observable('');
        self.itemNumber = ko.observable('');
        self.inventoryCode = ko.observable('');
        self.description = ko.observable('');
        self.comments = ko.observable('');
    };

    var componentRow = function (component_data) {
        var self = this;
        self.component = ko.observable(component_data.component);
        self.itemNumber = ko.observable(component_data.itemNumber);
        self.inventoryCode = ko.observable(component_data.inventoryCode);
        self.description = ko.observable(component_data.description);
        self.comments = ko.observable(component_data.comments);
    };
    var componentProcessRow = function (component_process) {
        var self = this;
        self.process = ko.observable(component_process.process);
        self.component = ko.observable(component_process.component);
        self.fileNames = ko.observable(component_process.fileNames);
        self.adressBlock = ko.observable(component_process.adressBlock);
        self.Define = ko.observable(component_process.Define);
        self.Line1 = ko.observable(component_process.Line1);
        self.Line2 = ko.observable(component_process.Line2);
        self.Line3 = ko.observable(component_process.Line3);
        self.Line4 = ko.observable(component_process.Line4);
        self.Line5 = ko.observable(component_process.Line5);
        self.Line6 = ko.observable(component_process.Line6);
        self.Line7 = ko.observable(component_process.Line7);
        self.processDescription = ko.computed(function () {
            return "Line 1 = " + self.Line1() + "\nLine 2 = " + self.Line2() + "\nLine 3 = " + self.Line3() + "\nLine 4 = " + self.Line4() +
         "\nLine 5 = " + self.Line5() + "\nLine 6 = " + self.Line6() + "\nLine 7 = " + self.Line7();
        }, self);
    };

    var componentFinishing = function () {
        var self = this;
        self.finishProcess = ko.observable('');
        self.quantity = ko.observable('');
        self.finishDesc = ko.observable('');
        self.additionalInstructions = ko.observable('');
    };

    var componentFinishingRow = function (component_finish) {
        var self = this;
        self.finishProcess = ko.observable(component_finish.process);
        self.quantity = ko.observable(component_finish.quantity);
        self.finishDesc = ko.observable(component_finish.finishDesc);
        self.additionalInstructions = ko.observable(component_finish.additionalInstructions);
    };
    self.title = ko.observable(self.jobInstructionServiceData[0].title);

    self.componentData = ko.observableArray();
    self.componentDataRows = ko.observableArray();

    self.componentFinishingData = ko.observableArray();
    self.componentFinishingDataRows = ko.observableArray();

    self.componentProcessDataRows = ko.observableArray();

    self.editProcessInfo = ko.observableArray();
    self.editFinishProcessInfo = ko.observableArray();

    self.componentDataSource = ko.observableArray();
    self.processDataSource = ko.observableArray();
    self.finishDataSource = ko.observableArray();

    $.each(self.jobInstructionServiceData[0].componentDict, function (a, b) {
        self.componentDataRows.push(new componentRow(b)); //main object with all data
        if (self.componentDataSource.indexOf(b.component) == -1) {
            self.componentDataSource.push({
                "compValue": b.component.replace(/ /g, ' '),
                "compText": b.component
            });
        }
    });

    $.each(self.jobInstructionServiceData[0].componentProcessDict, function (a, b) {
        self.componentProcessDataRows.push(new componentProcessRow(b));
        if (self.processDataSource.indexOf(b.process) == -1) {
            self.processDataSource.push({
                "processValue": b.process.replace(/ /g, ' '),
                "processText": b.process
            });
        }
    });


    $.each(self.jobInstructionServiceData[0].componentFinishing, function (a, b) {
        self.componentFinishingDataRows.push(new componentFinishingRow(b));
        if (self.finishDataSource.indexOf(b.process) == -1) {
            self.finishDataSource.push({
                "finishValue": b.process.replace(/ /g, ' '),
                "finishText": b.process
            });
        }
    });

    self.removeComponent = function () {
        self.componentData.remove(this);
    }


    self.addComponent = function () {
        self.componentData.push(new component());
        $('select[id^=ddlComponent]').selectmenu().selectmenu('refresh', true);
    }

    self.changeComponent = function (data, event) {
        var selectedVal = $(event.srcElement).val() == undefined ? $(event.target).val() : $(event.srcElement).val();
        var selectedObj = $.grep(self.componentDataRows(), function (i, j) {
            return selectedVal.toLowerCase() == i.component().toLowerCase();
        });
        if (selectedObj != undefined && selectedObj != null && selectedObj.length > 0) {
            data.comments(selectedObj[0].comments);
            data.itemNumber(selectedObj[0].itemNumber);
            data.inventoryCode(selectedObj[0].inventoryCode);
            data.description(selectedObj[0].description);
        }
    }

    self.changeProcess = function (data, event) {
        if (event.originalEvent) {
            var selectedVal = $(event.srcElement).val() == undefined ? $(event.target).val() : $(event.srcElement).val();
            var selectedObj = $.grep(self.componentProcessDataRows(), function (i, j) {
                return selectedVal.toLowerCase() == i.process().toLowerCase();
            })
            if (selectedObj != null && selectedObj != undefined && selectedObj.length > 0) {
                // var temp_process_row = selectedObj[0];
                // self.componentProcessDataRow(temp_process_row);

                data.process(selectedObj[0].process());
                data.component(selectedObj[0].component());
                data.fileNames(selectedObj[0].fileNames());
                data.adressBlock(selectedObj[0].adressBlock());
                data.Define(selectedObj[0].Define());
                data.Line1(selectedObj[0].Line1());
                data.Line2(selectedObj[0].Line2());
                data.Line3(selectedObj[0].Line3());
                data.Line4(selectedObj[0].Line4());
                data.Line5(selectedObj[0].Line5());
                data.Line6(selectedObj[0].Line6());
                data.Line7(selectedObj[0].Line7());
            }
        }
    }

    self.changeFinishProcess = function (data, event) {
        var selectedVal = $(event.srcElement).val() == undefined ? $(event.target).val() : $(event.srcElement).val();
        var selectedObj = $.grep(self.componentFinishingDataRows(), function (i, j) {
            return selectedVal.toLowerCase() == i.finishProcess().toLowerCase();
        });
        if (selectedObj != null && selectedObj != undefined && selectedObj.length > 0) {
            data.quantity(selectedObj[0].quantity);
            data.finishDesc(selectedObj[0].finishDesc);
            data.additionalInstructions(selectedObj[0].additionalInstructions);
        }
    }

    self.addComponentProcess = function () {
        self.componentFinishingData.push(new componentFinishing());
        $('select[id^=ddlComponentFinish]').selectmenu().selectmenu('refresh', true);
    }

    self.removeComponentProcess = function () {
        self.componentFinishingData.remove(this);
    }

    self.editProcess = function () {
        //var self = this;
        $('#popupEditProcessing').popup().popup('open');
        self.editProcessInfo.push(this);
        $('select[id^=ddlLine]').selectmenu().selectmenu('refresh', true);
    }

    self.cancelProcess = function () {
        self.editProcessInfo.removeAll();
        $('#popupEditProcessing').popup('close');
    }

    self.editFinishProcess = function () {
        $('#popupEditFinishing').popup().popup('open');
        self.editFinishProcessInfo.push(this);
    }

    self.cancelFinishProcess = function () {
        self.editFinishProcessInfo.removeAll();
        $('#popupEditFinishing').popup('close');
    };
    if (self.componentData().length == 0) {
        var temp_comp_data = $.parseJSON(ko.toJSON(self.componentDataRows()[0]));
        var temp_comp_obj = new component();
        temp_comp_obj.component(temp_comp_data.component);
        temp_comp_obj.comments(temp_comp_data.comments);
        temp_comp_obj.description(temp_comp_data.description);
        temp_comp_obj.itemNumber(temp_comp_data.itemNumber);
        temp_comp_obj.inventoryCode(temp_comp_data.inventoryCode);
        self.componentData.push(temp_comp_obj);
    }

    if (self.componentFinishingData().length == 0) {
        self.componentFinishingData.push(self.componentFinishingDataRows()[0]);
    }
};

