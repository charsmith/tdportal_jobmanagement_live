﻿jQuery.support.cors = true;
//******************** Global Variables Start **************************
var gData;
var serviceURL = "";
var jobNumber = "";
var facilityId = "";
var startLineNumber = 0;
var recordsGetCount = 5;
var endLineNumber = 5;
jobNumber = getSessionData("jobNumber");
facilityId = (appPrivileges.customerNumber == CW_CUSTOMER_NUMBER) ? 1 : getSessionData("facilityId");

var urlString = unescape(window.location)
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
var params = queryString.split("&");
var fileType = "";
var fileName = "";
if (params != null) {
    fileType = params[0].substring(params[0].indexOf('=') + 1);
    fileName = params[1].substring(params[1].indexOf('=') + 1);
}
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************

var file_data;
serviceURL = serviceURLDomain + 'api/SampleLine/' + facilityId + '/' + jobNumber + '/source^' + fileType + '/' + fileName + '/' + startLineNumber + '/' + endLineNumber;

$('#_jobFileMappingPreview').live('pagebeforecreate', function (event) {
    displayMessage('_jobFileMappingPreview');
    displayNavLinks();
    $('#btnSaveTop').attr('onclick', "postFileMapData();");
    $('#btnContinueTop').attr('data-icon', 'delete');
    $('#btnContinueTop').attr('onclick', 'cancelData();');
    $('#btnContinueTop').attr('title', 'Cancel');
});

$(document).on('pageshow', '#_jobFileMappingPreview', function (event) {
    makeGData();
    $("#_jobFileMappingPreview").trigger("create");    
    $("#aReturnJob").bind('click', function () {
        $("#aReturnJob").unbind('click');
        window.location.href = "../" + sessionStorage.continueToOrder.substring(0, sessionStorage.continueToOrder.indexOf('.')) + "/" + sessionStorage.continueToOrder;
    });
});
//******************** Page Load Events End **************************


//******************** Public Functions Start ************************

function makeGData() {
    getCORS(serviceURL, null, function (data) {
        gData = data;
        ko.applyBindings(new mapperPreviewViewModel());
        window.setTimeout(function () {
            $("#tblMappingPreview").table("refresh");
            var el = $('a[data-gettype=p]');
            $(el).addClass('ui-disabled');
            el[0].disabled = true;
            el[1].disabled = true;
        }, 100);
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
}

var colData = function (col_name, col_data) {
    var self = this;
    self.colName = ko.observable({});
    self.colName = col_name;
    self.colData = ko.observable({});
    self.colData = col_data;
};

var rowsInfo = function (row_data) {
    var self = this;
    self.rowData = ko.observableArray([]);
    self.rowData(row_data);
};

var mapperPreviewViewModel = function () {
    var self = this;
    self.headerRow = ko.observableArray([]);
    self.dataRows = ko.observableArray([]);
    $.each(gData, function (key, val) {
        var obj = {};
        var col_data = [];
        $.each(val.sampleFieldList, function (key1, val1) {
            if (self.headerRow().length < val.sampleFieldList.length) {
                self.headerRow.push({
                    name: "Col" + key1,
                    index: ("Col" + key1).replace(' ', '_'),
                    width: '75',
                    align: 'left',
                    sortable: false
                });
            }
            col_data.push(new colData("Col" + key1, val1.value));            
        });        
        self.dataRows.push(new rowsInfo(col_data));
    });
    self.fetchRecords = function (data, event) {
        var el = event.target || event.tagetElement;
        var type = $(el).attr('data-getType');
        if (type == "p") {
            startLineNumber = (startLineNumber > 0) ? (startLineNumber - recordsGetCount) : startLineNumber;
            endLineNumber = (endLineNumber > recordsGetCount) ? endLineNumber - recordsGetCount : recordsGetCount;
            if (startLineNumber <= 0) {
                var el = $('a[data-gettype=p]');
                $(el).addClass('ui-disabled');
                el[0].disabled = true;
                el[1].disabled = true;
            }
            else {
                var el = $('a[data-gettype=n]');
                $(el).removeClass('ui-disabled');
                el[0].disabled = false;
                el[1].disabled = false;
            }
        }
        else {
            startLineNumber = endLineNumber;
            endLineNumber = endLineNumber + recordsGetCount;
            if (startLineNumber >= 0) {                
                var el = $('a[data-gettype=p]');
                $(el).removeClass('ui-disabled');
                el[0].disabled = false;
                el[1].disabled = false;

                var el = $('a[data-gettype=n]');
                $(el).removeClass('ui-disabled');
                el[0].disabled = false;
                el[1].disabled = false;
            }
        }
        serviceURL = serviceURLDomain + 'api/SampleLine/' + facilityId + '/' + jobNumber + '/source^' + fileType + '/' + fileName + '/' + startLineNumber + '/' + endLineNumber;

        getCORS(serviceURL, null, function (data) {
            if (data.length == 0) {
                if (type == "n") {
                    var el = $('a[data-gettype=n]');
                    $(el).addClass('ui-disabled');
                    el[0].disabled = true;
                    el[1].disabled = true;
                }
                else {
                    var el = $('a[data-gettype=p]');
                    $(el).addClass('ui-disabled');
                    el[0].disabled = true;
                    el[1].disabled = true;
                }
            }
            gData = data;
            var new_data = [];
            $.each(gData, function (key, val) {
                var obj = {};
                var col_data = [];
                var headers_list = [];
                $.each(val.sampleFieldList, function (key1, val1) {
                    if (headers_list.length < val.sampleFieldList.length) {
                        headers_list.push({
                            name: "Col" + key1,
                            index: ("Col" + key1).replace(' ', '_'),
                            width: '75',
                            align: 'left',
                            sortable: false
                        });
                    }
                    self.headerRow(headers_list);
                    col_data.push(new colData("Col" + key1, val1.value));
                    //obj["Col" + key1] = val1.value;
                });                
                new_data.push(new rowsInfo(col_data));
            });
            self.dataRows(new_data);
            $("#tblMappingPreview").table("refresh");
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };
};

function fnSubmitMapper() {
    document.location.href = '../jobFileMapper/jobFileMapper.html?ft=' + fileType + '&fn=' + fileName;
}
//******************** Public Functions End **************************