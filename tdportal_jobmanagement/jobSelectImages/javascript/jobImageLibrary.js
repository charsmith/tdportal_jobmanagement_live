﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
//******************** Data URL Variables Start **************************
var gServiceEmailList = "../jobSelectImages/JSON/_jobImageLibrary.JSON";
var gServiceEmailListData;
var emailData = [];
var pageObj;
var gOutputData;
var gPageSize = 1;
var gServiceUserData;

var isDragDrop = false;
var isMultiple = false;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
isDragDrop = ('draggable' in document.createElement('span'));
isMultiple = ('draggable' in document.createElement('span'));


//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobImageLibrary').live('pagebeforecreate', function (event) {
    displayMessage('_jobImageLibrary');
    createConfirmMessage('_jobImageLibrary')
    getLOData();
    //load IFRAME for IE to upload files.
    if ($.browser.msie) {
        $('#formUpload').remove();
    }
    loadUploadIFrame();
    window.setTimeout(function () {
        if (pageObj == undefined || pageObj == null) {
            pageObj = new makeEmailRecipients();
            var $dropArea = $("#drop-area");
            $dropArea.on({
                "drop": pageObj.makeDrop,
                "dragenter": pageObj.ignoreDrag,
                "dragover": pageObj.ignoreDrag
            });
            ko.applyBindings(pageObj);
            $("div ul").find('select').selectmenu();
        }
        $('#dvLOImageRecipients').listview('refresh');
    }, 1500);
    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        pageObj.makeDrop(e, true, e.target)
    });

    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });

    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        getBubbleCounts(gOutputData);
    }
    setDemoHintsSliderValue();
    createDemoHints("jobImageLibrary");
    createPlaceHolderforIE();
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER)
        getUsersData();
});

$(document).on('pageshow', '#_jobImageLibrary', function (event) {
    persistNavPanelState();
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        $('#dvSetAsDefault').css('display', 'none');
    }
    if (!dontShowHintsAgain && (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
                           && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" ||
                               sessionStorage.showDemoHints == "on")
                           && (sessionStorage.isImageLibraryDemoHintsDisplayed == undefined || sessionStorage.isImageLibraryDemoHintsDisplayed == null ||
                               sessionStorage.isImageLibraryDemoHintsDisplayed == "false")) {
        sessionStorage.isImageLibraryDemoHintsDisplayed = true;
        $('#imageLibraryHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#imageLibraryHints').popup('open', { positionTo: '#dvImages' });
        }, 500);
    }
});

function getLOData() {
    $.getJSON(gServiceEmailList, function (data) {
        $.each(data, function (key, val) {
            if (key.indexOf(jobCustomerNumber) > -1) {
                emailData = val;
            }
        });
    });
};

function createPagerObject1(total_pages, start, end) {
    var pager_length = Math.ceil(total_pages / gPageSize);
    var pager_object = [];
    if (total_pages > 1) {
        for (var j = 0; j < total_pages; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j) });
        }
    }
    return pager_object;
}

var emailRecipientGroup = function (name, email_list, selected_page_value, search_value) {
    var self = this;
    self.emails = ko.observableArray([]);
    $.each(email_list, function (key, val) {
        if (val.loThumbnailImg != undefined && val.loThumbnailImg != null && val.loThumbnailImg != "") {
            if (jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                if ($.parseJSON(sessionStorage.selectedLoanOffierToEdit).loIndex == "0") {
                    if (val.loThumbnailImg.toString().indexOf('logo') > -1)
                        self.emails.push(new emailRecipient(val));
                }
                else if (val.loThumbnailImg.toString().indexOf('logo') == -1)
                    self.emails.push(new emailRecipient(val));

            }
            else
                self.emails.push(new emailRecipient(val));
        }
    });
    self.caption = ko.observable(name);
    self.pagingData = ko.observableArray([]).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 200 } });
    self.selectedPageValue = ko.observable();
    self.selectedPageValue(selected_page_value);
    self.showMoreOption = ko.observable();
    self.showSearchField = ko.observable();
    self.searchValue = ko.observable();
    self.prevSearchValue = ko.observable('');
    self.searchValue(((search_value != undefined && search_value != null) ? search_value : ""));
    self.selectedFilterOption = ko.observable();
    self.displayUsers = ko.observable();
    self.selectedFilterOption('fileName');
};

var emailRecipient = function (data_info) {
    var self = this;
    self.firstName = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.firstName : "");
    self.lastName = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.lastName : "");
    self.userName = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.userName : "");
    self.fileName = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.fileName : "");
    self.loThumbnailImg = ko.observable();
    var thumb_nail = getThumbnailPath(logoPath);
    thumb_nail += (data_info != undefined && data_info != null && data_info != "") ? data_info.loThumbnailImg : "";
    self.loThumbnailImg(thumb_nail);
    self.email = ko.observable((data_info != undefined && data_info != null && data_info != "" && data_info.email != undefined && data_info.email != null) ? data_info.email : "");
    self.fieldsList = ko.computed(function () {
        var temp_fields = "";
        if (jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
            temp_fields = "Name: " + self.firstName() + ' ' + self.lastName();
        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER)
            temp_fields = "Location: Jupiter Dental";

        return temp_fields;
    });
    self.isDefault = ko.observable(false);
    self.defaultText = ko.observable('');

    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        self.isDefault(((data_info.isDefault == true) ? true : false));
    }
}

function getThumbnailPath(thumb_nail_logo_path) {
    switch (jobCustomerNumber) {
        case AAG_CUSTOMER_NUMBER: thumb_nail_logo_path += "AAG/LO_Headshots/"; break;
        case DCA_CUSTOMER_NUMBER: thumb_nail_logo_path += "DCA/"; break;
        case SK_CUSTOMER_NUMBER:
            var temp_image_info = "";
            if (sessionStorage.selectedLoanOffierToEdit != undefined && sessionStorage.selectedLoanOffierToEdit != null && sessionStorage.selectedLoanOffierToEdit != "")
                temp_image_info = $.parseJSON(sessionStorage.selectedLoanOffierToEdit);
            if (temp_image_info.loIndex != "0") {
                thumb_nail_logo_path += "SportsKing/" + jQuery.parseJSON(sessionStorage.jobSetupOutput).templateName + "/";
            }
            else
                thumb_nail_logo_path += "SportsKing/";
            break;
        case KUBOTA_CUSTOMER_NUMBER: thumb_nail_logo_path += "Kubota/"; break;
        case GWA_CUSTOMER_NUMBER: thumb_nail_logo_path += "GWA/"; break;
    }

    return thumb_nail_logo_path;
}

var imageFileVM = function (location_pk, file_name, file_size, file_status, file_pk) {
    var self = this;
    self.locationPk = ko.observable('');
    self.locationPk(location_pk);
    self.fileName = ko.observable('');
    self.fileName(file_name);
    self.fileSizePretty = ko.observable('');
    self.fileSizePretty(file_size);
    self.fileStatus = ko.observable('');
    self.fileStatus(file_status);
    self.filePk = ko.observable('');
    self.filePk(file_pk);
};

var makeEmailRecipients = function () {
    var self = this;
    new taskAttachments(self);
    self.emailsList = ko.observableArray([]);
    self.selectedUserProfile = ko.observable({});
    self.selectedUserToUpload = ko.observable({});
    self.selectedEmailListItem = ko.observable({});
    self.prevSelectedEmailListItem = {};
    self.selectedEmailType = ko.observable();
    self.emailsQuantity = ko.observable();
    self.sendEmailType = ko.observable();
    self.emailTypeMode = ko.observable();
    self.sendEmailsList = ko.observableArray([]);
    self.displayUsers = ko.observableArray([]);
    self.showDefault = ko.observable(false);
    self.showDefault(((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? true : false));
    self.getPageDisplayName = function (page_name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var page_display_name = '';
        $.each(new String(page_name), function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                page_display_name += ((!page_display_name.endsWith(' ') && !page_display_name.endsWith('-')) ? ' ' : '') + value;
            }
            else {
                page_display_name += value;
            }
        });
        return page_display_name;
    };
    var temp_key_email = "";
    var temp_email_list = [];
    $.each(emailData, function (key, val) {
        temp_key_email = self.getPageDisplayName(key.initCap());
        self.emailsList.push(new emailRecipientGroup(temp_key_email, val, 0, ''));
    });
    var temp_data = {};
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        $.each(gServiceUserData, function (key, val) {
            temp_data = {};
            ko.mapping.fromJS(val, {}, temp_data);
            self.displayUsers.push(temp_data);
        });
    }
    else if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
            if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
                $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                    temp_data = {};
                    var temp_loc_data = {};
                    temp_loc_data["userId"] = val.storeId;
                    //temp_loc_data["userName"] = val.storeId + ' - ' + val.marketManager;
                    temp_loc_data["userName"] = ((val.storeId != "") ? val.storeId + ' - ' : "") + val.city + ', ' + val.state + ' ' + val.zip;
                    ko.mapping.fromJS(temp_loc_data, {}, temp_data);
                    self.displayUsers.push(temp_data);
                });
            }
        }
    }
    self.addUserClick = function (data, event) {
        self.selectedUserProfile(data);
        self.insertIntoLayoutAction();
    };
    self.editUserClick = function (data, event) {
        self.selectedUserProfile(data);
        if ((jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && sessionStorage.selectedLoanOffierToEdit != undefined && sessionStorage.selectedLoanOffierToEdit != null && sessionStorage.selectedLoanOffierToEdit != "")
            if (($.parseJSON(sessionStorage.selectedLoanOffierToEdit)).loIndex != "0")
                $('#popupUploadImage').find('a').filter(':contains("Set as Default Logo")').text('View Image');

        $('#popupUploadImage').popup('open');
    };
    //Displays Wait image while opening preview.
    self.displayOverlay = function () {
        $('#waitPopUp').popup('open', { positionTo: 'window' });
        $('#overlay').css({ top: '40%', left: '50%', margin: '-' + ($('#overlay').height() / 2) + 'px 0 0 -' + ($('#overlay').width() / 2) + 'px' });
    }

    self.openArtworkFile = function () {
        if (self.selectedUserProfile().loThumbnailImg().indexOf(jQuery.parseJSON(sessionStorage.jobSetupOutput).templateName) > -1) {
            //self.displayOverlay();
            //$('#waitPopUp').popup('open', { positionTo: 'window' });
            var content_type = 'application/pdf';
            var pdf_preview_url = getThumbnailPath(logoPath) + self.selectedUserProfile().loThumbnailImg().substring(self.selectedUserProfile().loThumbnailImg().lastIndexOf('/')).replace('png', 'pdf');
            var htmlText = "";
            if (content_type == 'application/pdf') {
                htmlText = "<embed width=850 height=500 type='" + content_type + "' src='" + pdf_preview_url + "' id='selectedFile'></embed>";
            } else {
                htmlText = "<img src='" + pdf_preview_url + "' id='selectedFile'></img>";
            }
            $('#popupPDFContent').html(htmlText);

            window.setTimeout(function getDelayNDisplayFile() {
                if ($('#selectedFile')[0].clientWidth > 0 && $('#selectedFile')[0].clientHeight > 0)
                    var display = $("#popPDF");
                if (display != undefined) {
                    display.scroll(function () {

                        var ctrls = $("#popPDF img");
                        console.log("************************");
                        ctrls.each(function (index) {
                            console.log($(this).position().top);

                            if ($(this).position().top < 0)
                                $(this).css("visibility", "hidden")//.hide();
                            else {
                                $(this).css("visibility", "")//.show();
                            }
                        });
                    });
                }

                window.setTimeout(function getDelay() {
                    $('#waitPopUp').popup('close');
                    $('#popPDF').popup('open', { positionTo: 'window' });
                }, 200);
            }, 5000);
        }
    }

    self.insertIntoLayoutAction = function () {

        var temp_user = ko.toJS(self.selectedUserProfile);
        delete temp_user.fieldsList;
        var gOutputData = {};
        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);

            //    if (gOutputData.selectImagesAction != undefined && gOutputData.selectImagesAction != null && gOutputData.selectImagesAction != "") {
            //        if (gOutputData.selectImagesAction.selectedImagesList != undefined && gOutputData.selectImagesAction.selectedImagesList != null && gOutputData.selectImagesAction.selectedImagesList != "") {
            //            gOutputData.selectImagesAction.selectedImagesList.push(temp_user);
            //        }
            //    }
            //    else {
            //        gOutputData["selectImagesAction"] = {};
            //        gOutputData.selectImagesAction["isEnabled"] = "true";
            //        gOutputData.selectImagesAction["type"] = "images";
            //        gOutputData.selectImagesAction["name"] = "images1";
            //        gOutputData.selectImagesAction["selectedImagesList"] = [];
            //        gOutputData.selectImagesAction.selectedImagesList.push(temp_user);
            //    }
            //    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        }
        //$('#popupUploadImage').popup('close');
        //$('#confirmMsg').html('Do you want to place this image in the layout?');
        //$('#okButConfirm').text('Continue');         
        //$('#okButConfirm').bind('click', function () {
        //    $('#okButConfirm').unbind('click');
        //    $('#popupConfirmDialog').popup('close');
        var temp_image_info = "";
        if (sessionStorage.selectedLoanOffierToEdit != undefined && sessionStorage.selectedLoanOffierToEdit != null && sessionStorage.selectedLoanOffierToEdit != "")
            temp_image_info = $.parseJSON(sessionStorage.selectedLoanOffierToEdit);
        if (jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
            if (temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex] == undefined || temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex] == null || temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex] == "") {
                temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex] = {};
            }
            temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex]["firstName"] = temp_user.firstName;
            temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex]["lastName"] = temp_user.lastName;
            temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex]["emailAddress"] = (temp_user.email) ? temp_user.email : temp_user.userName;
            temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex]["loThumbnailImg"] = ((jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && temp_image_info.loIndex != "0") ? ("/" + jQuery.parseJSON(sessionStorage.jobSetupOutput).templateName + temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/'))) : temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/'));
            temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex]["nmls"] = "";
            temp_image_info.loInfo[temp_image_info.selectedInHome][temp_image_info.selectedLoanOfficerId][temp_image_info.loIndex]["id"] = "";

            sessionStorage.selectedLoanOffierToEdit = JSON.stringify(temp_image_info);
        }
        else {
            var thumbnail_img = ((jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && temp_image_info.loIndex != "0") ? ("/" + jQuery.parseJSON(sessionStorage.jobSetupOutput).templateName + temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/'))) : temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/'));
            //gOutputData.variableImageAction.variableDict[temp_image_info.loIndex]["firstName"] = thumbnail_img.substring(1).substring(0, thumbnail_img.substring(1).lastIndexOf('/'));
            //gOutputData.variableImageAction.variableDict[temp_image_info.loIndex]["lastName"] = "";
            //gOutputData.variableImageAction.variableDict[temp_image_info.loIndex]["emailAddress"] = "";
            //var image_info = (jobCustomerNumber == SK_CUSTOMER_NUMBER && temp_image_info.loIndex != "0") ? ("/" + jQuery.parseJSON(sessionStorage.jobSetupOutput).templateName + temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/'))) : temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/'));
            var image_info = temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/') + 1);
            gOutputData.variableImageAction.variableDict["image" + (parseInt(temp_image_info.loIndex) + 1)] = (jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? image_info.toLowerCase().replace('.png', '') : image_info;
            //gOutputData.variableImageAction.variableDict[temp_image_info.loIndex]["loThumbnailImg"] = (jobCustomerNumber == SK_CUSTOMER_NUMBER && temp_image_info.loIndex != "0") ? ("/" + jQuery.parseJSON(sessionStorage.jobSetupOutput).templateName + temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/'))) : temp_user.loThumbnailImg.substring(temp_user.loThumbnailImg.lastIndexOf('/'));
            //gOutputData.variableImageAction.variableDict[temp_image_info.loIndex]["nmls"] = "";
            //gOutputData.variableImageAction.variableDict[temp_image_info.loIndex]["id"] = "";
            sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        }
        sessionStorage.removeItem('selectedUserProfile');

        window.location.href = "../jobSelectImages/jobSelectImages.html";
        //});
        //$('#popupConfirmDialog').popup('open')
    };
    self.setAsDefault = function () {
        if (($.parseJSON(sessionStorage.selectedLoanOffierToEdit)).loIndex != "0" && (jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)) {
            $('#popupUploadImage').popup('close');
            self.openArtworkFile();
        }
        else {
            $.each(self.emailsList(), function (key, val) {
                $.each(val.emails(), function (key1, val1) {
                    if (self.selectedUserProfile().fileName() == val1.fileName())
                        val1.isDefault(true);
                    else
                        val1.isDefault(false);
                });
            });
            self.selectedUserProfile().isDefault(true);
            $('#popupUploadImage').popup('close');
        }
    }

    self.moreClick = function (el, e) {

        if (e.originalEvent) {
            var ctrl_id = (e.targetElement || e.currentTarget || e.target).id;
            var ctrl = $('#' + ctrl_id);
            var context_data = ko.contextFor(ctrl[0]).$data;

            context_data.selectedPageValue(context_data.selectedPageValue() + 1)

            self.getSelectedLOList(ctrl_id, context_data, context_data.caption(), (context_data.selectedPageValue()));

            $("#" + ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
            $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
            $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');

        }

    };
    self.pageChanged = function (el, e) {

        if (e.originalEvent) {
            var ctrl_id = (e.targetElement || e.target || e.currentTarget).id;
            var ctrl = $('#' + e.target.id);
            var context_data = ko.contextFor(ctrl[0]).$data;
            self.getSelectedLOList(ctrl_id, context_data, context_data.caption(), ctrl.val());
            $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
            $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
        }

    };
    self.selectArtworkClick = function (data, event) {
        var el = event.target || event.currentTarget;
        $('#ddlFileType').val('25').selectmenu('refresh');
        $('#ddlFileType').trigger('change');
        $('#popupSetTaskStatus').popup('open');
    };
    self.cancelSelectArtworkClick = function () {
        //self.filesToAttach([]);
        self.fileToDropBox([]);
        self.selectedFiles = [];
        $('#popupSetTaskStatus').popup('close');
    };
    self.taskStatusClick = function (data, event) {
        $('#txtComments').val('');
        $('#popupSetTaskStatus').popup('open');
        $("#dvUploadedFilesList div ul").each(function (i) {
            $(this).listview();
        });
        $("#dvUploadedFilesList").collapsibleset('refresh');
    };
    self.getSelectedLOList = function (id, context_data, caption, page_index) {
        var emails_list_service_url = serviceURLDomain + "api/GizmoEmailCustomer_find/" + jobCustomerNumber + "/" + sessionStorage.facilityId + "/" + sessionStorage.jobNumber + "/" + context_data.caption().toLowerCase()

        var post_json = {
            "pageIndex": (page_index != undefined && page_index != null && page_index != "") ? page_index : 0,
        };
        var search_term = context_data.selectedFilterOption();
        post_json[search_term] = "";

        post_json[search_term] = (context_data.searchValue() != "" && $('#' + id).find('input[ data-type="search"]').val() != $('#' + id).find('input[ data-type="search"]').attr('placeholder')) ? context_data.searchValue() : "";

        postCORS(emails_list_service_url, JSON.stringify(post_json), function (response_data) {
            context_data.emails.removeAll();
            $.each(response_data.emails, function (key, val) {
                context_data.emails.push(new emailRecipient(val))
            });
            context_data.pagingData(createPagerObject1(response_data.totalPages, 0, 0));
            context_data.prevSearchValue(context_data.searchValue());
            var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
            var is_more_visible = (parseInt(selected_page_value + 1) < parseInt(response_data.totalPages)) ? true : false;
            context_data.showMoreOption(is_more_visible);
            $("#" + context_data.caption()).find('ul').trigger("create").listview().listview('refresh');
            window.setTimeout(function setUI() {
                $("div[id='dv" + context_data.caption() + "']").find('ul').listview('refresh').trigger('create');
                $("div[id='dv" + context_data.caption() + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
                $("div[id='dv" + context_data.caption() + "']").find('ul').find("input[data-type=search]").trigger('create');
                createPlaceHolderforIE();
            }, 5);
        }, function (error_response) {
            showErrorResponseText(error_response, false);
        });
    };
    self.search = function (data, event) {

        /* if (!self.searchValidation()) return false;
         var selected_target = $('#hdnSelectedGroup').val();
         var page_index = 0;
         var context_data = "";
         var ctrl_id = selected_target.replace(/ /g, '_');
         if (selected_target != "") {
             context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
             page_index = context_data.selectedPageValue();
             page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
             page_index = (context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1) ? 0 : parseInt(page_index);
             if (event != undefined)
                 context_data.selectedPageValue(0);
             self.getSelectedLOList(ctrl_id, context_data, context_data.caption(), page_index);
         }*/

    };
    self.searchWithEnterKey = function (data, event) {
        //try {
        //    debugger;
        //    if (event.which == 13) {
        //        //ko.contextFor($('#' + $('#hdnSelectedGroup').val().replace(/ /g, '_'))[0]).$data.searchValue(event.target.value);
        //        //self.search(data, event);
        //        //return false;
        //    }
        //    return true;
        //}
        //catch (e) {
        //}
    };
    self.searchValidation = function () {
        var context_data = "";
        var regxSearch;
        var selected_target = $('#hdnSelectedGroup').val();
        //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
        context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
        var search_value = context_data.searchValue().toLowerCase();
        var selected_option = $("input[name=rdoFilterList]:checked").val();
        switch (selected_option) {
            case "firstName":
            case "lastName":
                regxSearch = /^[a-zA-Z ]*$/;
                break;
            case "fileName":
                regxSearch = /^[ A-Za-z0-9@_.-]*$/;
                break;
        }
        if (search_value != "" && !regxSearch.test(search_value)) {
            $('#alertmsg').html("Please enter valid search text");
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            return false;
        }
        return true;
    };
};

String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
function getUsersData() {
    var user_json = {};
    user_json = { "customerNumber": jobCustomerNumber, "facility": sessionStorage.facilityId };
    postCORS(serviceURLDomain + "api/Membership_getUserList", JSON.stringify(user_json), function (data) {
        gServiceUserData = data;
    },
     function (response_error) {
         showErrorResponseText(response_error, false);
         $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
     });
}

