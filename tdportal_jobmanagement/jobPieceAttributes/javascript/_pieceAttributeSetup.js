﻿var pieceAttrs = function () {
    var self = this;
    var percent = 0;
    var urlString = unescape(window.location)
    var querystring = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
    var params = querystring.split("&");
    var gCaption = "";
    var filesToUpload = [];
    if (params[1] != null && params[1].split("=")[1] != null)
        gCaption = params[1].split("=")[1];

    var gServiceUrl = '../JSON/_jobSetup.JSON';
    var gData;
    var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
    var gTemplateUrlForAll = serviceURLDomain + 'api/Template';
    var gOutputData;
    var pagePrefs;

    //************************ FUNCTIONS START ****************************************//

    self.bindTemplates = function () {
        if (sessionStorage.jobSetupLoadObject == undefined && sessionStorage.jobSetupLoadObject == undefined) {
            self.makeGData();
        }
        else {
            if (sessionStorage.jobSetupLoadObject != undefined) {
                gData = jQuery.parseJSON(sessionStorage.jobSetupLoadObject);
                gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                gData = jQuery.grep(gData, function (obj) {
                    return obj.list.toLowerCase() === "mailstreamaction";
                });
                self.buildLists();
                self.buildOutputLists();
            }
        }
        if (gOutputData != null) {
            getBubbleCounts(gOutputData);
        }
    };

    //creates page objects and loads
    self.makeGData = function () {
        $.getJSON(gServiceUrl, function (data) {
            gData = data;
            sessionStorage.jobSetupLoadObject = JSON.stringify(gData);
            gData = jQuery.grep(gData, function (obj) {
                return obj.list.toLowerCase() === "mailstreamaction";
            });
            self.buildLists();
            if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined) {
                self.makeGOutputData();
            }
            else {
                gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                self.buildOutputLists();
            }
        });
    };

    //Displays persisted piece attributes data into the page.
    self.makeGOutputData = function () {
        $.getJSON(gOutputServiceUrl, function (dataOutput) {
            gOutputData = dataOutput;
            self.buildOutputLists();
        });
    };

    //Loads piece attributes options into drop downs
    self.loadDropDownValues = function (Obj, ctrl) {
        $.each(Obj, function (key, val) {
            //if ((sessionStorage.userRole == "user" || sessionStorage.userRole == "power") && (jobCustomerNumber == CW_CUSTOMER_NUMBER)) {
            if ((sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) {
                if (ctrl.toLowerCase() == "mailclass" && val.item.toLowerCase() != "periodical") {
                    $("#" + ctrl).append('<option value="' + val.value + '">' + val.item + '</option>');
                }
            }
            else {
                $("#" + ctrl).append('<option  value="' + val.value + '">' + val.item + '</option>');
            }

            //eval(getURLDecode(pagePrefs.scriptBlockDict.loadDropDownValues)); //comes from page preferences web service
        });
        //if ((sessionStorage.userRole == "user" || sessionStorage.userRole == "power") && (jobCustomerNumber == CW_CUSTOMER_NUMBER) && (ctrl.toLowerCase() == "mailclass")) {
        if ((sessionStorage.userRole == "user" || sessionStorage.userRole == "power") && (ctrl.toLowerCase() == "mailclass")) {
            $("#" + ctrl).val("1");
        }
        window.setTimeout(function delay() { $("#" + ctrl).selectmenu('refresh'); }, 100);
    };

    //Creates piece attributes.
    self.pieceAttributes = function (Obj, ctrl, type) {
        if (type == "ddl")
            self.loadDropDownValues(Obj, ctrl);

        if (type == "textbox")
            $("#" + ctrl).val(Obj);
    };

    //Builds controls list in a page.
    self.buildLists = function () {
        $('#divIncremental').empty();
        $('#divIncremental').css('display', 'none');
        $.each(gData[0].items, function (key, val) {
            switch (val.name) {
                case "mailClass":
                case "mailType":
                case "entryPoint":
                case "crrtSort":
                case "rateType":
                case "postagePayment":
                case "pstgPymnt":
                case "permit":
                case "sortOrder":
                case "scratchOff":
                case "couponPerfs":
                    self.pieceAttributes(val.values, val.name, "ddl");
                    break;
                case "width":
                case "height":
                    if ((sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) {
                        $('#' + val.name).addClass('ui-disabled');
                        $('#' + val.name).attr('disabled', true);
                        $('#' + val.name).css('opacity', '0.7');
                    }
                    self.pieceAttributes(val.values, val.name, "textbox");
                case "thickness":
                case "weight":
                    self.pieceAttributes(val.values, val.name, "textbox");
                    break;
                case "isIMBTracked":
                    self.pieceAttributes(val.values, val.name, "chk");
                    break;

            }
        });
    };

    //Displays the persisted data in to the created controls..
    self.buildOutputLists = function () {
        var file_data = (gOutputData.uploadList != undefined) ? gOutputData.uploadList : 0;
        if ($("#fileCount")) {
            $("#fileCount").text("0");
            if (file_data.length > 0) {
                $.each(file_data, function (key, val) {
                    if (val.fileName != "") {
                        $("#fileCount").text(file_data.length);
                    }
                });
            }
            else
                $("#fileCount").text("0");
        }
        $.each(gOutputData.mailstreamAction, function (key, val) {
            if (key != undefined) {
                if ($("#" + key)[0] != undefined) {
                    if ($("#" + key)[0].type == "text")
                        $("#" + key).val(val);
                    else if ($("#" + key)[0].type == "select-one" && val != null && val != "") {
                        $("#" + key).val(val.toLowerCase());
                        window.setTimeout(function delay() { $("#" + key).selectmenu('refresh'); }, 100);
                    }
                }
                //else if (key.toLowerCase() == 'optionalattributesdict' && val != null && val != "") {
                //    var inc_enhancements = '<h3 id="incrementalH3" style="color: #369">Incremental Enhancements</h3>';
                //    $.each(val, function (optAttributes_key, optAttributes_val) {
                //        inc_enhancements += "<div class='ui-field-contain' id='sp'" + optAttributes_key + " data-role='fieldcontain'><label style='' for='" + optAttributes_key + "' class='select'><span>*" + self.getDisplayLabel(optAttributes_key) + ":</span></label>";
                //        inc_enhancements += "<select style=''  id='" + optAttributes_key + "' name='" + optAttributes_key + "' data-mini='true' data-theme='e'>";
                //        var options = '';
                //        var ddl_options_data = $.grep(gData[0].items, function (obj) {
                //            return obj.name === optAttributes_key;
                //        });
                //        if (ddl_options_data.length > 0)
                //            $.each(ddl_options_data[0].values, function (opt_key, opt_val) {
                //                inc_enhancements += '<option value="' + opt_val.value + '" ' + ((opt_val.value.toLowerCase() == optAttributes_val.toLowerCase()) ? 'selected' : '') + ' >' + opt_val.item + '</option>';
                //            });
                //        inc_enhancements += "</select></div>";
                //    });
                //    $('#divIncremental').empty();
                //    $('#divIncremental').append(inc_enhancements);
                //    $.each($('#divIncremental').find('select'), function (a, b) {
                //        $(this).selectmenu();
                //    });
                //}
            }
        });
        if (jobCustomerNumber == "1") {
            var counter = 0;
            for (i = 0; i < gOutputData.mailstreamAction.advancedPresortOptions.length; i++) {
                counter++;
                if (counter > 1)
                    self.addAdvPresortRow();
                //$('#ddlBatch' + counter).val(gOutputData.mailstreamAction.advancedPresortOptions[i].batch);
                //window.setTimeout(function delay() { $('#ddlBatch' + counter).selectmenu('refresh'); }, 100);
                $('#txtHeight' + counter).val(gOutputData.mailstreamAction.advancedPresortOptions[i].height);
                $('#txtWidth' + counter).val(gOutputData.mailstreamAction.advancedPresortOptions[i].width);
                $('#txtThickness' + counter).val(gOutputData.mailstreamAction.advancedPresortOptions[i].thickness);
                $('#txtWeight' + counter).val(gOutputData.mailstreamAction.advancedPresortOptions[i].weight);
            }
        }

        var templates_list = jQuery.parseJSON(sessionStorage.templates);
        var post_card_display_format = gOutputData.templateName;
        var selected_template = $.grep(templates_list, function (obj) {
            return obj.value === gOutputData.templateName;
        });
        if (selected_template.length > 0) {
            var template_name = selected_template[0].text;
            if (sessionStorage.jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && sessionStorage.jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
                if (selected_template[0].text.indexOf('x') > -1) {
                    var post_card_display_format_arry = selected_template[0].text.split('x');
                    post_card_display_format = post_card_display_format_arry[0] + ' x ' + post_card_display_format_arry[1];
                }
                template_name = (post_card_display_format + ((gOutputData.templateName.toLowerCase().indexOf('selfmailer') > -1) ? " Self Mailer" : ((post_card_display_format.toLowerCase().indexOf('postcard') > -1) ? "" : " Postcard"))).replace(/_/g, ' ');
            }            
            $('#postCardHeader').text(self.getDisplayLabel($.trim(template_name)));
        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
    };

    //Gets the display name of each piece attribute labels.
    self.getDisplayLabel = function (attr_name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var display_name = '';
        $.each(new String(attr_name), function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                if (attr_name[key - 1] != undefined && attr_name[key - 1].match(PATTERN))
                    display_name += value;
                else
                    display_name += ' ' + value;
            }
            else {
                if (parseInt(value) > -1 && (attr_name[key - 1] != undefined && attr_name[key - 1].toUpperCase().match(PATTERN)))
                    display_name += (key == 0) ? value : '-' + value;
                else
                    display_name += (key == 0 || attr_name[key - 1] == " ") ? value.toUpperCase() : value;
            }
        });
        if (display_name.toLowerCase().indexOf('scratch') > -1) display_name = display_name.replace(' ', '-');
        return display_name.replace('( ', '(');
    };

    //Binds the user selected data from controls into local job ticket json session.
    self.bindData = function () {
        $.each(gOutputData.mailstreamAction, function (key, val) {
            if (key != undefined) {
                if (key.toLowerCase() == 'optionalattributesdict' && val != null) {
                    $.each(val, function (opt_key, opt_val) {
                        gOutputData.mailstreamAction[key][opt_key] = ($("#" + opt_key).length > 0 && $("#" + opt_key).val() != null) ? jQuery.trim($("#" + opt_key).val()) : opt_val;
                    });
                }
                else {
                    if (key == "mailClass" && gOutputData.mailstreamAction[key] != jQuery.trim($("#" + key).val()))
                        gOutputData["needsPostageReportUpdate"] = true; //if quantity, template and/or mail class have changed, we need make this attribue to true

                    gOutputData.mailstreamAction[key] = ($("#" + key).length > 0 && $("#" + key).val() != null) ? jQuery.trim($("#" + key).val()) : val;
                }
            }
        });

        if (jobCustomerNumber == "1") {
            var adv_presort_options_cnt = $('#tbody').find('select').length;
            if (adv_presort_options_cnt > 0) {
                gOutputData.mailstreamAction["advancedPresortOptions"] = [];
                $.each($('#tbody').find('select'), function (a, b) {
                    gOutputData.mailstreamAction.advancedPresortOptions.push({
                        batch: $('#ddlBatch' + (a + 1)).val(),
                        width: jQuery.trim($('#txtWidth' + (a + 1)).val()),
                        height: jQuery.trim($('#txtHeight' + (a + 1)).val()),
                        thickness: jQuery.trim($('#txtThickness' + (a + 1)).val()),
                        weight: jQuery.trim($('#txtWeight' + (a + 1)).val())
                    });
                });
            }
        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
    };

    //validates and persists the selected /entered piece attributes information.
    self.updatePieceAttributes = function () {
        if (!self.validatePieceAttributes())
            return false;
        else {
            self.bindData();
            return true;
        }
    };

    //Controller to accept only numerics.
    self.acceptNumerics = function (eventObj) {
        var keycode;
        if (eventObj.keyCode) //For IE
            keycode = eventObj.keyCode;
        else if (eventObj.Which)
            keycode = eventObj.Which; // For FireFox
        else
            keycode = eventObj.charCode; // Other Browser
        if (keycode < 44 || keycode > 57 || keycode == 45 || keycode == 47) eventObj.returnValue = false;
    };

    //Constructs validation message for piece attributes.
    self.pieceCtrlValidation = function (ctrl, message, type) {
        if (type == 'text') {
            if ($.trim($("#" + ctrl).val()) == "" || $("#" + ctrl).val() == 0)
                return '<li>' + message + '</li>';
        }
        else {
            if ($("#" + ctrl).val() == "select")
                return '<li>' + message + '</li>';
        }
        return "";
    };

    //Validates piece attributes entered by User.
    self.validatePieceAttributes = function () {
        var msg = "<ul>";
        var text_box_list = $('#dvPieceAttributes input:text,select');
        $.each(text_box_list, function (key, ctrl) {
            //if ((sessionStorage.userRole == "user" || sessionStorage.userRole == "power") && appPrivileges.customerNumber == CW_CUSTOMER_NUMBER) {
            if (sessionStorage.userRole == "user" || sessionStorage.userRole == "power") {
                if (ctrl.id == 'width' || ctrl.id == 'height' || ctrl.id == 'mailClass')
                    msg = msg + self.pieceCtrlValidation(ctrl.id, 'Please fill ' + ctrl.name + ' field.', ctrl.type);
            }
            else {
                msg = msg + self.pieceCtrlValidation(ctrl.id, 'Please fill ' + ctrl.name + ' field.', ctrl.type);
            }
            //eval(getURLDecode(pagePrefs.scriptBlockDict.validation)); //comes from page preferences web service
        });
        msg = msg + "</ul>"
        if (msg != "<ul></ul>") {
            $('#alertmsg').html(msg);
            $('#popupDialog').popup('open');
            return false;
        }
        return true;
    };

    //Generates advanced presort options.
    self.addAdvPresortRow = function () {
        var new_batch_div = "";
        var row_cnt = 0;
        var row_cnt = $('#dvAdvOptions').find('div[id^=dvAdvBatchOptionsTable]').length
        if (row_cnt < 2) {
            new_batch_div = '<div id="dvAdvBatchOptionsTable' + (row_cnt + 1) + '" data-role="collapsible" data-content-theme="c" data-collapsed-icon="carat-r" data-expanded-icon="carat-d">' +
                                    '<h3>Batch ' + (row_cnt + 1) + '</h3>' +
                                    '<div data-role="fieldcontain" >' +
                                        '<label for="txtWidth' + (row_cnt + 1) + '">Width:</label>' +
                                        '<textarea cols="40" rows="8" data-mini="true" name="txtWidth' + (row_cnt + 1) + '" id="txtWidth' + (row_cnt + 1) + '" onkeypress="return pieceAttributesKeyValidation(event);"></textarea>' +
                                    '</div>' +
                                    '<div data-role="fieldcontain">' +
                                        '<label for="txtHeight' + (row_cnt + 1) + '">Height:</label>' +
                                        '<textarea cols="40" rows="8" name="txtHeight' + (row_cnt + 1) + '" id="txtHeight' + (row_cnt + 1) + '" data-mini="true" onkeypress="return pieceAttributesKeyValidation(event);"></textarea>' +
                                    '</div>' +
                                    '<div data-role="fieldcontain">' +
                                        '<label for="txtThickness' + (row_cnt + 1) + '">Thickness:</label>' +
                                        '<textarea cols="40" rows="8" name="txtThickness' + (row_cnt + 1) + '" id="txtThickness' + (row_cnt + 1) + '" data-mini="true" onkeypress="return pieceAttributesKeyValidation(event);"></textarea>' +
                                    '</div>' +
                                    '<div data-role="fieldcontain">' +
                                        '<label for="txtWeight' + (row_cnt + 1) + '">Weight:</label>' +
                                        '<textarea cols="40" rows="8" name="txtWeight' + (row_cnt + 1) + '" id="txtWeight' + (row_cnt + 1) + '" data-mini="true" onkeypress="return pieceAttributesKeyValidation(event);"></textarea>' +
                                    '</div>' +
                                    '<div align="right">' +
                                        '<a href="#" data-role="button" data-icon="delete" data-theme="a" data-inline="true" data-mini="true" onclick="removeAdvPresortRow();">Remove Batch</a>' +
                                    '</div>' +
                                '</div>'

            $('#dvAdvOptions').append(new_batch_div);
            $('.ui-page').trigger('create');
        }
        //$('#dvAdvPresortOptRows').collapsibleset('refresh');
    };

    //Removes advanced presort options.
    self.removeAdvPresortRow = function () {
        //var row_cnt = $('#tbody').find('select').length;
        var row_cnt = $('#dvAdvOptions').find('div[id^=dvAdvBatchOptionsTable]').length
        if (row_cnt > 1) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            var adv_rows = gOutputData.mailstreamAction.advancedPresortOptions;
            adv_rows.splice(row_cnt - 1, 1);
            gOutputData.advancedPresortOptions = adv_rows;
            sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
            $('#dvAdvBatchOptionsTable' + row_cnt).remove();
            $('.ui-page').trigger('create');
            //$('#dvAdvPresortOptRows').collapsibleset('refresh');
        }
    };

    //************************ FUNCTIONS END ****************************************//
}

// Numeric only control handler
jQuery.fn.ForceNumericOnly =
function () {
    return this.each(function () {
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY       
            return (
                key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};