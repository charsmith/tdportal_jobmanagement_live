﻿var appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));

$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
var pageObj;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;

$('#_jobPieceAttributes').live('pagebeforecreate', function (event) {
    pageObj = new pieceAttrs();
    displayNavLinks();
    displayMessage('_jobPieceAttributes');
    !(appPrivileges.dispaly_advPresortOptRows != undefined && appPrivileges.dispaly_advPresortOptRows) ? $("#dvAdvPresortOptRows").hide() : "";  //getting info from authorization web service

    if (sessionStorage.pieceAttributesPrefs == undefined || sessionStorage.pieceAttributesPrefs == null || sessionStorage.pieceAttributesPrefs == "")
        if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
            getPagePreferences('pieceAttributes.html', sessionStorage.facilityId, CW_CUSTOMER_NUMBER);
        else
            getPagePreferences('pieceAttributes.html', sessionStorage.facilityId, jobCustomerNumber);
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.pieceAttributesPrefs));
    }
   // loadMobility();
    if (sessionStorage.templates == undefined) {
        getCORS(gTemplateUrlForAll, null, function (data) {
            sessionStorage.templates = JSON.stringify(data);
        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
    }
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
    createDemoHints("jobPieceAttributes");
});

$(document).on('pageshow', '#_jobPieceAttributes', function (event) {

    if (!dontShowHintsAgain && (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isPieceAttributesDemoHintsDisplayed == undefined || sessionStorage.isPieceAttributesDemoHintsDisplayed == null || sessionStorage.isPieceAttributesDemoHintsDisplayed == "false")) {
        sessionStorage.isPieceAttributesDemoHintsDisplayed = true;
        $('#pieceAttributesHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#pieceAttributesHints').popup('open', { positionTo: '#dvPieceAttributes' });
        }, 500);
    }

    var powerUserPrefs = ["spthickness", "thickness", "spweight", "weight", "spmailType", "mailType", "spentryPoint", "entryPoint", "spcrrtSort", "crrtSort", "spsortOrder", "sortOrder", "sprateType", "rateType", "sppstgPymnt", "pstgPymnt", "sppermit", "permit"];
    if (sessionStorage.userRole == "power") {
        if (sessionStorage.pieceAttributesPrefs != undefined || sessionStorage.pieceAttributesPrefs != null || sessionStorage.pieceAttributesPrefs != "") {
            var power_prefs = $.parseJSON(sessionStorage.pieceAttributesPrefs);
            $.each(powerUserPrefs, function (key, val) {
                power_prefs.buttonPrefsDict[val].showControl = 0;
            });
            sessionStorage.pieceAttributesPrefs = JSON.stringify(power_prefs);
            managePagePrefs(power_prefs);
        }
    }
    pagePrefs = jQuery.parseJSON(sessionStorage.pieceAttributesPrefs);

    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;
    if (pagePrefs != null) {
        pageObj.bindTemplates();
    }
    persistNavPanelState();
    if ((jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER) || ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && appPrivileges.roleName != "admin"))
        $('#dvAddressBlockPlacement').css('display', 'none');
    $('#sldrShowHints').slider("refresh");
});
