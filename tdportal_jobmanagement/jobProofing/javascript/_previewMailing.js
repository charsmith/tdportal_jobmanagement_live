jQuery.support.cors = true;
//******************** Global Variables Start **************************
var gOutputData;
var jobNumber = "";
var facilityId = "";
jobNumber = getSessionData("jobNumber");
facilityId = getSessionData("facilityId");
var urlString = unescape(window.location);
if (appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER) {
    urlString = "../jobProofing/jobProofing.html?fileName=TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf&jobNumber=" + sessionStorage.jobNumber;
}
else if (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER) { //Need to update pdf kubato customer
    urlString = "../jobProofing/jobProofing.html?fileName=DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf&jobNumber=" + sessionStorage.jobNumber;
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if (gOutputData.templateName.indexOf('_11x6_') > -1) {
            urlString = "../jobProofing/jobProofing.html?fileName=DCA_42754_DoubleDentRite_DM_Jupiter.pdf&jobNumber=" + sessionStorage.jobNumber;
        }
    }
}
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length);
var params = queryString.split("&");
var encodedFileName = getURLParameter("fileName");
var hard_coded_path_File = "";
if (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER) { //Need to update pdf kubato customer   
    hard_coded_path_File = "images\\pdf\\DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if (gOutputData.templateName.indexOf('_11x6_') > -1) {
            hard_coded_path_File = "images\\pdf\\DCA_42754_DoubleDentRite_DM_Jupiter.pdf";
        }
    }
}
else if (appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER) {    
    hard_coded_path_File = "images\\pdf\\TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
}
function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
}
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_previewMailing').live('pagebeforecreate', function () {
    $('#viewFrame').attr("src", hard_coded_path_File);
    displayNavLinks();
});

$(document).bind('pageshow', '#_previewMailing', function () {
    $('#btnContinue').attr('onclick', 'continueToOrder()');
    window.setTimeout(function () {
        updateFooterInfo();
    }, 1000);
    if (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail != "" && sessionStorage.isOpenedFromEmail == "true") {
        $('#btnContinue').css('display', 'none');
    }
});
function annotate()
{
    var preview_proof = "../jobProofing/jobProofing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
    if (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER) {
        preview_proof = "../jobApproval/jobApproval.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
    }
    
   // getNextPageInOrder();
    window.location.href = preview_proof;
}
//******************** Page Load Events End **************************