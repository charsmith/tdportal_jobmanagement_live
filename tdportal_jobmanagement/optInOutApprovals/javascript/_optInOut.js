﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var selectedStoreId;
var gOutputData;
var gDataStartIndex = 0;
var gInnerData = [];
var storeData = "";

var urlString = unescape(window.location);
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
var params = queryString.split("&");
var encryptedString = (params[0] != undefined && params[0] != "") ? params[0] : "";

var email = "";
var user = "";
var password = "";
var customerNumber = "";
var jobNumber = "";
var jobTypeId = "0";
var facilityId = "";
var optInOutData = "";
var districtOptInOutEncryptedURL = serviceURLDomain + 'api/Encryption_decrypt/';
var districtOptInOutURL = serviceURLDomain + 'api/ManagerApproval_stores/';
var optInPOSTUrl = serviceURLDomain + 'api/ManagerApproval_stores/';

//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_districtOptInOut').live('pagebeforecreate', function (event) {
    displayMessage('_districtOptInOut');
    createConfirmMessage('_districtOptInOut');
    $.getJSON("JSON/_optInOut.JSON", function (data) {
        optInOutData = data;
    });
    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage 
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }

    if (sessionStorage.desktop) // desktop storage 
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    else if (localStorage.mobile) // mobile storage
        return true;

    if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
        alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
        window.open('', '_self', '');
        window.close();
        return false;
    }
    else {
        window.setTimeout(function () {
            getData();
        }, 1500);

    }
});

//******************** Public Functions Start **************************
function getData() {
    makeGData();
}

function makeGData() {
    getCORS(districtOptInOutEncryptedURL + encryptedString, null, function (url_with_data) {
        //angie.white@caseys.com|admin_cgs1|admin_cgs1|155711|83965|1
        var data_split = [];
        if (url_with_data != undefined && url_with_data != "") {
            data_split = url_with_data.split('|');

            email = (data_split[0] != undefined && data_split[0] != "") ? data_split[0] : "";
            user = (data_split[1] != undefined && data_split[1] != "") ? data_split[1] : "";
            password = (data_split[2] != undefined && data_split[2] != "") ? data_split[2] : "";
            customerNumber = (data_split[3] != undefined && data_split[3] != "") ? data_split[3] : "";
            //customerNumber = JETS_CUSTOMER_NUMBER; // TO be removed after Jet's Demo.

            jobNumber = (data_split[4] != undefined && data_split[4] != "") ? data_split[4] : "";
            facilityId = (data_split[5] != undefined && data_split[5] != "") ? data_split[5] : "";
            jobTypeId = (data_split[6] != undefined && data_split[6] != "") ? data_split[6] : "0";
            //jobTypeId = 52;//for Jets Demo
        }
        sessionStorage.authString = makeBasicAuth(user, password);
        var manager_Email = "";
        var store_list = [];
        switch (customerNumber) {
            case JETS_CUSTOMER_NUMBER:
                manager_Email = "joan.eglseder@caseys.com";
                store_list.push(
                {
                    "storeId": "IL-017",
                    "city": "Champaign",
                    "state": "IL",
                    "isChecked": 0,
                    "targetedPretty": "0",
                    "costPretty": "$0.00",
                    "selectedPostcard": ""
                });
                store_list.push({
                    "storeId": "MI-056",
                    "city": "Lansing",
                    "state": "MI",
                    "isChecked": 0,
                    "targetedPretty": "0",
                    "costPretty": "$0.00",
                    "selectedPostcard": ""
                });
                break;

        }
        if (customerNumber == JETS_CUSTOMER_NUMBER) {
            var temp_json = {
                "managerEmail": manager_Email,
                "description": (jobTypeId == 71) ? "December 2015 Opt-in Postcard" : "April 2016 Opt-In Postcard",
                "inHomeDate": (jobTypeId == 71) ? "12/31/2016" : "12/31/2016",
                "optInEndDate": (jobTypeId == 71) ? "12/31/2016" : "12/31/2016",
                "approveByEndDate": "12/20/2016",
                "storeList": store_list

                //    [
                //                        {
                //                            "storeId": "1637",
                //                            "city": "DALLAS CITY",
                //                            "state": "IL",
                //                            "isChecked": 0,
                //                            "targetedPretty": "0",
                //                            "costPretty": "$0.00",
                //                            "selectedPostcard": ""
                //                        }
                //]
            };
            gData = temp_json;
            displayStoresList();
            displayImages(customerNumber);
            setOptInApprovalLogos(customerNumber);

        }
        else {
            getCORS(districtOptInOutURL + customerNumber + '/' + facilityId + '/' + jobNumber + '/' + email + '/opt-in', null, function (data) {
                //$.getJSON("JSON/_districtOptInOut.JSON", function (data) {
                gData = data;
                displayStoresList();
                displayImages(customerNumber);
                setOptInApprovalLogos(customerNumber);
            }, function (error_response) {
                showErrorResponseText(error_response);
            });
        }
    }, function (error_response) {
        showErrorResponseText(error_response);
    });
}
function displayImages(customer_number) {
    if (customer_number == CASEYS_CUSTOMER_NUMBER) {
        $("#headerText").text(optInOutData[jobTypeId].headerText);
        $("#paragraphOptIn").text(optInOutData[jobTypeId].paragraphOptIn);
        $("#orderedListInfo").html(optInOutData[jobTypeId].orderedListInfo);
        $("#paragraphUserMessage").text(optInOutData[jobTypeId].paragraphUserMessage);

        var images_path = optInImagesPath + jobNumber + "_";
        if (jobTypeId == 72) {
            var img_front = images_path + optInOutData[jobTypeId].pizzaVersion1Source;
            var img_back = images_path + optInOutData[jobTypeId].pizzaVersion2Source;
            $('#trDeliveryImgPizzaVersion').css('display', 'none');
            $('#imagePizzaVersion1').attr('src', img_front);
            $('#imgPizzaVersion1Name').html(optInOutData[jobTypeId].pizzaVersion1Name);
            $('#imagePizzaVersion1').attr('alt', optInOutData[jobTypeId].pizzaVersion1AlternateText);
            $('#imagePizzaVersion2').attr('src', img_back);
            $('#imgPizzaVersion2Name').html(optInOutData[jobTypeId].pizzaVersion2Name);
            $('#imagePizzaVersion2').attr('alt', optInOutData[jobTypeId].pizzaVersion2AlternateText);
            $('#aDOO').attr('href', images_path + optInOutData[jobTypeId].dooReferenceLink);
            $('#aPostCard').attr('href', images_path + optInOutData[jobTypeId].postcardReferenceLink);
            $("#paragraphCriticalNote").html(optInOutData[jobTypeId].paragraphNote);
            $('#paragraphDeliveryNote').css('display', 'none');
            $('#trDeliveryOfferLink1').css('display', 'none');
            $('#trDeliveryOfferLink2').css('display', 'none');
            $('#trDeliveryOfferLink3').css('display', 'none');
        }
        else if (jobTypeId == 71) {
            $('#trImgPizzaVersion').css('display', 'none');
            var img_front = images_path + optInOutData[jobTypeId].pizzaVersion1Source;
            var img_back = images_path + optInOutData[jobTypeId].pizzaVersion2Source;
            $('#imageOffer1').attr('src', img_front);
            $('#imageOffer2').attr('src', img_back);
            $('#lnkOffer1').text(optInOutData[jobTypeId].offer1Name);
            $('#lnkOffer1').attr('href', images_path + optInOutData[jobTypeId].offer1Link);
            $('#lnkOffer2').text(optInOutData[jobTypeId].offer2Name);
            $('#lnkOffer2').attr('href', images_path + optInOutData[jobTypeId].offer2Link);
            $('#lnkOffer3').text(optInOutData[jobTypeId].offer3Name);
            $('#lnkOffer3').attr('href', images_path + optInOutData[jobTypeId].offer3Link);
            $("#paragraphDeliveryNote").html(optInOutData[jobTypeId].paragraphNote);
            $('#paragraphCriticalNote').css('display', 'none');
        }
    }
    else if (customer_number == JETS_CUSTOMER_NUMBER) {        
		$("#headerText").text(optInOutData[jobTypeId].headerText);
        $("#paragraphOptIn").text(optInOutData[jobTypeId].paragraphOptIn);
        $("#orderedListInfo").html(optInOutData[jobTypeId].orderedListInfo);
        $("#paragraphUserMessage").text(optInOutData[jobTypeId].paragraphUserMessage);
        $("#paragraphNote").text(optInOutData[jobTypeId].paragraphNote);
		
        var images_path = logoPath + "JetsPizza/";
        var img_front = images_path + "Grand_Opening_6x11_Postcard.png";
        var img_back = images_path + "Celebrate_Spring_6x11_Postcard.png";
        $('#imagePizzaVersion1').attr('src', img_front);
        $('#imagePizzaVersion1').attr('alt', 'Grand Opening 6x11 Postcard');
        $('#imgPizzaVersion1Name').html("Grand Opening 6x11 Postcard*");
        $('#imagePizzaVersion2').attr('src', img_back);
        $('#imagePizzaVersion2').attr('alt', 'Celebrate Spring 6x11 Postcard');
        $('#imgPizzaVersion2Name').html("Celebrate Spring 6x11 Postcard*");
        $('#aDOO').attr('href', images_path + "Grand_Opening_6x11_Postcard.pdf");
        $('#aPostCard').attr('href', images_path + "Celebrate_Spring_6x11_Postcard.pdf");
    }
}
//load the store locations...
function displayStoresList() {
    storeData = "";    
    if ((gData == null || gData == "") || (gData.storeList == undefined) || (gData.storeList.length == 0)) {
        var empty_message = "<br/><div style='font-size: 12px; font-weight: bold; color:red;'>Attention. No stores were found for your user login. " +
                            " Please contact caseysgeneralstore@tribunedirect.com if you have any questions.</font>";
        $('#spText').html(empty_message);
        $('#trInstructions').hide();
        $('#btnSubmitOpt').css('display', 'none');
        $('#trImgPizzaVersion').css("display", "none");
        $('#optInContents').css('display', 'none');
    } else {

        var the_text = "";
        var sign_in_end_date = new Date(gData.optInEndDate);
        var current_date = new Date();
        var sign_in_end_date_diff = dateDifference(sign_in_end_date, current_date);
        if (parseInt(sign_in_end_date_diff) >= 1) {
            the_text = '<div style="font-size: 12px; font-weight: bold; color:red;text-align:center;"><br />The Opt In period has Closed.</div>';
            $('#btnSubmitOpt').hide();
            $('#trInstructions').hide();
            $('#spText').html(the_text);
            $('#trImgPizzaVersion').css("display", "none");
            $('#optInContents').css('display', 'none');
        } else {             
            $.each(gData, function (key, val) {
                switch (key.toLowerCase()) {
                    case "description":
                        if (jobTypeId != 71) {
                            storeData += '<li><b>Campaign:</b> ' + val + '</li>';
                            $('#navHeader').html(val);
                        }
                        break;
                    case "inhomedate":
                        storeData += '<li><b>In-Home ' + ((jobTypeId == 71) ? 'Date' : 'Start') + ':</b> ' + val + '</li>';
                        break;
                    case "optinenddate":
                        storeData += '<li style="color:#C00"><b>Opt-In Period Ends:</b> ' + val + '</li>';
                        $("#hdnOptInDate").val(val);
                        break;                        
                }
            });
            $('#ulPostCardInfo').html(storeData);
            buildStores();
        }
    }
}

//to display the "All Stores"...
function buildStores() {
    var li_chk_routes = "";
    li_chk_routes = '<fieldset id="optInStoresSet" data-role="controlgroup" data-mini="true" data-inset="false" data-theme="c">';

    li_chk_routes += '<input type="checkbox" name="checkbox-100a" id="checkbox-100a" data-theme="a" onclick="checkCheckboxes(this.id);"/>';
    if (customerNumber != JETS_CUSTOMER_NUMBER)
        li_chk_routes += '<label for="checkbox-100a">Opt In for the Entire District</label><div id="dvChk" class="ui-corner-all">';
    else
        li_chk_routes += '<label for="checkbox-100a">Opt In All</label><div id="dvChk" class="ui-corner-all">';
    var checked_cnt = 0;
    $.each(gData.storeList, function (store_key, store_val) {
        var is_checked = "";
        if (store_val.isChecked == 1) {
            is_checked = "checked";
            checked_cnt = parseInt(checked_cnt) + 1;
        }
        li_chk_routes += '<input type="checkbox" id=' + store_val.storeId + ' ' + is_checked + ' onclick="fnSelect(this, \'' + store_val.storeId + '\')" />';
        li_chk_routes += '<label for="' + store_val.storeId + '">' + store_val.storeId + ' - ' + store_val.city.toUpperCase() + ', ' + store_val.state.toUpperCase() + '</label>';
        var ddl_postcard = "";

        if (is_checked == "checked") {
            ddl_postcard += createPostCardSelectionList(customerNumber, store_val.storeId, store_val.selectedPostcard);

        }
        li_chk_routes += ddl_postcard;
    });
    li_chk_routes += '</div></fieldset>';
    $('#dvLocations').append(li_chk_routes);
    $("#dvLocations").trigger("create");
    if (checked_cnt == gData.storeList.length) $('#checkbox-100a').attr('checked', true).checkboxradio('refresh');

    if (customerNumber == CASEYS_CUSTOMER_NUMBER) {
        $('select[id^=ddlSelectPostcard]').parent().parent().css('display', ((jobTypeId == 71) ? 'block' : 'none'));
    }
    else {
        $.each($('#dvChk input:checkbox:not(:checked)'), function (a, b) {
            $('#ddlSelectPostcard' + $(this)[0].id).parent().parent().css('display', ((jobTypeId == 71) ? 'block' : 'none'));
        });
    }
}

function createPostCardSelectionList(customer_number, store_id, selected_post_card) {
    var ddl_postcard = "";
    var selected_postcard = "";
    if (customer_number == CASEYS_CUSTOMER_NUMBER) {
        ddl_postcard += '<select name="ddlSelectPostcard' + store_id + '" id="ddlSelectPostcard' + store_id + '" data-mini="true" data-theme="f" onchange="fnPostcardSelect(this, \'' + store_id + '\')">';
        if (selected_post_card == "") selected_postcard = "selected"; else "";
        if (jobTypeId == 71) {
            ddl_postcard += '<option value="-1" ' + selected_postcard + '>Select Postcard for Store ' + store_id + '</option>';
            if (selected_post_card == "Critical Store Postcard") selected_postcard = "selected"; else selected_postcard = "";
            ddl_postcard += '<option value="Critical Store Postcard"' + selected_postcard + '>Non-Delivery</option>';
            if (selected_post_card == "Seven Day Delivery") selected_postcard = "selected"; else selected_postcard = "";
                ddl_postcard += '<option value="Seven Day Delivery" ' + selected_postcard + '>7 Day Delivery</option>';
            if (selected_post_card == "Thursday-Sunday Delivery") selected_postcard = "selected"; else selected_postcard = "";
            ddl_postcard += '<option value="Thursday-Sunday Delivery" ' + selected_postcard + '>Thurs – Sun Delivery</option>';
            if (selected_post_card == "Two Day Delivery: Friday-Saturday") selected_postcard = "selected"; else selected_postcard = "";
            ddl_postcard += '<option value="Two Day Delivery: Friday-Saturday" ' + selected_postcard + '>Fri – Sat Delivery</option>';
            //if (selected_post_card == "Delivery Only") selected_postcard = "selected"; else selected_postcard = "";
            //ddl_postcard += '<option value="Delivery Only"' + selected_postcard + '>Delivery Only</option>';
            //if (selected_post_card == "Carry-Out and/or Delivery") selected_postcard = "selected"; else selected_postcard = "";
            //ddl_postcard += '<option value="Carry-Out and/or Delivery" ' + selected_postcard + '>Carry-Out and/or Delivery</option>';
        }
        else {
            if (selected_post_card == "Critical Store Postcard") selected_postcard = "selected"; else selected_postcard = "";
            //ddl_postcard += '<option value="-1" ' + selected_postcard + '>Select Postcard for Store ' + store_id + '</option>';
            ddl_postcard += '<option value="Critical Store Postcard"' + selected_postcard + '>Critical Store Postcard</option>';
        }
        ddl_postcard += '</select>';
    }
    else if (customer_number == JETS_CUSTOMER_NUMBER) {
        ddl_postcard += '<select name="ddlSelectPostcard' + store_id + '" id="ddlSelectPostcard' + store_id + '" data-mini="true" data-theme="f" onchange="fnPostcardSelect(this, \'' + store_id + '\')">';
        if (selected_post_card == "") selected_postcard = "selected"; else "";
        ddl_postcard += '<option value="-1" ' + selected_postcard + '>Select Postcard for Store ' + store_id + '</option>';
        if (selected_post_card == "Grand Opening 6x11 Postcard") selected_postcard = "selected"; else selected_postcard = "";
        ddl_postcard += '<option value="Grand Opening 6x11 Postcard"' + selected_postcard + '>Grand Opening 6x11 Postcard</option>';
        if (selected_post_card == "Celebrate Spring 6x11 Postcard") selected_postcard = "selected"; else selected_postcard = "";
        ddl_postcard += '<option value="Celebrate Spring 6x11 Postcard" ' + selected_postcard + '>Celebrate Spring 6x11 Postcard</option>';
        ddl_postcard += '</select>';
    }
    return ddl_postcard;
}

function fnSelect(ctrl, checked_store_id) {
    var selected_chk = jQuery.grep(gData.storeList, function (a, b) {
        return a.storeId === checked_store_id.toString();
    });

    if (ctrl.checked) {
        selected_chk[0].isChecked = 1;
        $('#' + ctrl.id).attr('checked', true).checkboxradio('refresh');
        insertSelectArtworkDropdown($('#' + ctrl.id), ctrl.id);
        if (jobTypeId == 72)
            $('#ddlSelectPostcard' + ctrl.id).val('Critical Store Postcard').trigger('change');
        //else if (jobTypeId == 71)
        //    $('#ddlSelectPostcard' + ctrl.id).val('Carry-Out and/or Delivery').trigger('change');
    }
    else {
        selected_chk[0].isChecked = 0;
        selected_chk[0].selectedPostcard = "";
        $('#' + ctrl.id).attr('checked', false).checkboxradio('refresh');
        $('#ddlSelectPostcard' + ctrl.id).remove();
         
    }

    var unchecked_stores = $('#dvChk input:checkbox:not(:checked)');
    if (unchecked_stores.length > 0)
        $('#checkbox-100a').attr('checked', false).checkboxradio('refresh');
    else
        $('#checkbox-100a').attr('checked', true).checkboxradio('refresh');
     
    $("#dvLocations").trigger("create");
    if (customerNumber == CASEYS_CUSTOMER_NUMBER) {        
        $('#ddlSelectPostcard' + ctrl.id).parent().parent().css('display', ((jobTypeId == 71) ? 'block' : 'none'));
    }
}

function insertSelectArtworkDropdown(checkbox, store_id) {
    var ddl_postcard = "";
    ddl_postcard += createPostCardSelectionList(customerNumber, store_id, "");
    $(ddl_postcard).insertAfter(checkbox.parent());
}

function fnPostcardSelect(ctrl, checked_store_id) {
    var selected_chk = jQuery.grep(gData.storeList, function (a, b) {
        return a.storeId === checked_store_id.toString();
    });
    if ($('#ddlSelectPostcard' + checked_store_id).val() != "-1")
        selected_chk[0].selectedPostcard = $('#ddlSelectPostcard' + checked_store_id).val();
    else
        selected_chk[0].selectedPostcard = "";
}

function checkCheckboxes(id) {
    if ($('#checkbox-100a')[0].checked) {
        $('#dvChk input[type=checkbox]').each(function () {
            $(this).attr('checked', true);
            if ($('#ddlSelectPostcard' + $(this)[0].id).length == 0)
                insertSelectArtworkDropdown($(this), $(this)[0].id);
            if (jobTypeId == 72)
                $('#ddlSelectPostcard' + $(this)[0].id).val('Critical Store Postcard').trigger('change');
            //else if (jobTypeId == 71)
            //    $('#ddlSelectPostcard' + $(this)[0].id).val('Carry-Out and/or Delivery').trigger('change');
            $.each(gData.storeList, function (key, val) {
                val.isChecked = 1;
            });
        });
    }
    else {
        $('#dvChk input[type=checkbox]').each(function () {
            $(this).attr('checked', false);
            $('#ddlSelectPostcard' + $(this)[0].id).parent().parent().remove();
            $.each(gData.storeList, function (key, val) {
                $('#ddlSelectPostcard' + val.storeId).remove();
                val.isChecked = 0;
                val.selectedPostcard = "";
            });
        });
    }

    $("#dvChk input[type=checkbox]").each(function (i) {
        $(this).checkboxradio("refresh");
    });
    $("#dvLocations").trigger("create");
    if (customerNumber == CASEYS_CUSTOMER_NUMBER) {
        $('select[id^=ddlSelectPostcard]').parent().parent().css('display', ((jobTypeId == 71) ? 'block' : 'none'));
    }
}

function fnSubmit() {    
    var checked_stores_count = $('#dvChk input:checkbox:checked');
    if (checked_stores_count.length == 0) {
        $('#confirmMsg').html("Submitting without selecting any of your locations will result in <b>Opting Out</b> all of your locations for this mailing. Do you want to continue with this Opt-Out?");
        $('#okButConfirm').text("Yes, Opt-Out");
        $('#okButConfirm').bind('click', function () {
            $('#okButConfirm').unbind('click');
            window.setTimeout(function () { submitOptIns(); }, 500);
            $('#popupConfirmDialog').popup('close');
        });
        $('#popupConfirmDialog').popup('open');
    } else {
        submitOptIns();
    }
}
function submitOptIns() {
	var msg = "Saving failed.  Contact Administrator!";
    postCORS(optInPOSTUrl + customerNumber + '/' + facilityId + '/' + jobNumber + '/' + email + '/post/opt-in/' + jobTypeId, JSON.stringify(gData), function (response) {
        if (response.indexOf("success") != "-1") {
            msg = "Thank You!</br></br>Your Opt In selections have been submitted successfully." +
                "You may continue to make changes to your selections up to the end of the Opt In period on " + $("#hdnOptInDate").val() + ". " +
                "Any stores left deselected at that time will automatically Opt Out from the mailing." +
                "You may continue to edit and submit your selections or close this screen.";
            $('#alertmsg').html(msg);
            $('#popupDialog').popup('open');
        }
        else {
            $('#alertmsg').html(msg);
            $('#popupDialog').popup('open');
        }
    }, function (response_error) {
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
    });
}
//******************** Public Functions End **************************
