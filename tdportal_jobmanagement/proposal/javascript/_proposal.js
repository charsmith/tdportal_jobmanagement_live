﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************

var proposalJsonURL = 'JSON/_proposalType.JSON';
var proposalTypeData = "";
sessionStorage.companyName = sessionStorage.companyName; //"toysRUs";//"toysRUs";
//******************** Global Variables End **************************                                                                                                                                                                                          

//******************** Page Load Events Start **************************

$('#_dataProposalType').live('pagebeforecreate', function (event) {
    var isMobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        isMobile = true;
    }
    if (!isMobile)
        $('head').append('<link href="../proposal/css/2_column.css" rel="stylesheet" type="text/css" />');

    var bkgd_img = "";
    var customer_logo = "";
    var customer_alt = ""
    bkgd_img = (sessionStorage.companyName.toLowerCase() == "goodyear") ? "goodyear_bkgd.jpg" : "toysRus_bkgd.png";
    customer_logo = (sessionStorage.companyName.toLowerCase() == "goodyear") ? "goodyear_logo.png" : "toysRus_logo.png";
    customer_alt = (sessionStorage.companyName.toLowerCase() == "goodyear") ? "ToysRUs" : "Goodyear";
    $('#dvBkgd').css('background-image', 'url(customerLogos/' + bkgd_img + ')');
    $('#imgLogo').attr('src', 'customerLogos/' + customer_logo);
    $('#imgLogo').attr('alt', customer_alt);
    if (sessionStorage.companyName.toLowerCase() == "goodyear") $('#ulProposals').attr('data-theme', 'b');
});

$(document).on('pageshow', '#_dataProposalType', function (event) {
    $.getJSON(proposalJsonURL, function (data) {
        proposalTypeData = data;
        loadProposalType();
    });
});

//******************** Public Functions Start **************************

function loadProposalType() {
    var control_to_add = '';
    $('#ulProposals').empty();
    $.each(proposalTypeData, function (key, val) {
        if (val.customer.toLowerCase() == sessionStorage.companyName.toLowerCase()) {  //TODO: Once the login works fine, we can make it dynamic..
            $.each(val.items[0], function (key_items, val_items) {
                control_to_add += "<li><a onclick='navigate(\"" + val_items.targetUrl + "\")'>" + val_items.proposalType + "</a></li>";
            });
        }
    });
    $('#ulProposals').append(control_to_add);
    $('#ulProposals').listview('refresh');
}

function navigate(url) {
    if (url == "../pages/jobSelectLocations.html" || url =="../listSelection/jobListSelection.html") {
        sessionStorage.customerNumber = CASEYS_CUSTOMER_NUMBER;
        sessionStorage.jobNumber = "400538";
    }
    if (url == "../pages/jobListManagement.html") {
        sessionStorage.customerNumber = CW_CUSTOMER_NUMBER;
        sessionStorage.jobNumber = "-10924";
    }
    if (url == "../jobApprovalCheckout/jobApprovalCheckout.html") {
        sessionStorage.customerNumber = CW_CUSTOMER_NUMBER;
        sessionStorage.jobNumber = "-10924";
    }
    sessionStorage.facilityId = "1";
    window.location.href = url;
}

//******************** Public Functions End **************************
