﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
//var gServiceAddOrderUrl = 'JSON/adOrder.JSON';
var gServiceUserProfileUrl = "../userProfile/JSON/_userProfile.JSON";
var gStateServiceUrl = '../JSON/_listStates.JSON';
var gFacilityServiceUrl = "../JSON/_facility.JSON";
var gCompanyServiceUrl = "../userProfile/JSON/_company.JSON";
var statesJSON;
var facilityData;
var primaryFacilityData;
var companyData;
var gUserInfo = {};
var user_arr = [];
var pageObj;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_userProfile').live('pagebeforecreate', function (event) {
    loadStates();
   // displayNavLinks();
    displayMessage('_userProfile');
    createConfirmMessage("_userProfile");

    //createOrderConfirmMessage("_userProfile");
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    if (sessionStorage.selectedUser != undefined && sessionStorage.selectedUser != null && sessionStorage.selectedUser != "") {
        $('#btnCreateUser').text('Update User');
    }
    else {
        $('#btnCreateUser').text('Create User');
    }
});

$(document).on('pageshow', '#_userProfile', function (event) {
    window.setTimeout(function loadUserInfo() {
        pageObj = new loadUserProfileInfoVM();
        window.setTimeout(function bindData() {
            ko.applyBindings(pageObj);
            $('#ddlCompany').val(jobCustomerNumber).selectmenu('refresh');
            $('#ddlFacility').val(sessionStorage.facilityId).selectmenu('refresh');
            if (appPrivileges.customerNumber != "1") {
                $('#ddlCompany').attr('disabled', 'disabled');
                $('#ddlFacility').attr('disabled', 'disabled');
            }
            $('#ddlState').selectmenu('refresh');
            $('#ddlCompany').selectmenu('refresh');
            $('#ddlRole').selectmenu('refresh');
        }, 500);
    }, 500);
});

var userProfileVm = function (data) {
    var self = this;
    self.userName = ko.observable(data.userName);
    self.password = ko.observable(data.password);
    self.confirmPassword = ko.observable(data.confirmPassword);
    self.firstName = ko.observable(data.firstName);
    self.lastName = ko.observable(data.lastName);
    self.email = ko.observable(data.email);
    self.active = ko.observable(data.active);
    self.phone = ko.observable(data.phone == undefined || data.phone == null || data.phone == "" ? "" : '(' + data.phone.substr(0, 3) + ') ' + data.phone.substr(3, 3) + '-' + data.phone.substr(6));
    self.address = ko.observable(data.address);
    self.address2 = ko.observable(data.address2);
    self.city = ko.observable(data.city);
    self.zip = ko.observable(data.zip);
    self.managerName = ko.observable(data.managerName);
    self.areaCode = ko.observable(data.areaCode);
    self.fax = ko.observable(data.fax == undefined || data.fax == null || data.fax == "" ? "" : '(' + data.fax.substr(0, 3) + ') ' + data.fax.substr(3, 3) + '-' + data.fax.substr(6));
    self.state = ko.observable(data.state);
    //self.facility = ko.observable(data.facility);
    self.primaryFacility = ko.observable(data.primaryFacility);
    //self.company= ko.observable(data.company);
    self.role = ko.observable(data.role);
    self.customerNumber = ko.observable(data.customerNumber);

}

var statesVm = function (data) {
    var self = this;
    self.states = data;
}

var loadUserProfileInfoVM = function () {
    var self = this;
    //var user_name = "test_user_bbb1";
    //var user_name = "wright_bbb101";
    var user_name = (sessionStorage.selectedUser != undefined && sessionStorage.selectedUser != null && sessionStorage.selectedUser != "") ? sessionStorage.selectedUser : "";
    self.userProfileInfo;
    self.states = [];
    self.states = statesJSON;
    self.primaryFacilityData = [];
    self.primaryFacilityData = primaryFacilityData;
    self.facilityData = [];
    self.facilityData = primaryFacilityData;
    self.companyData = [];
    self.companyData = companyData;
    if (sessionStorage.selectedUser != undefined && sessionStorage.selectedUser != null && sessionStorage.selectedUser != "") {
        getCORS(serviceURLDomain + "api/Membership_getUserName/" + user_name, null, function (data) {
            self.userProfileInfo = new userProfileVm(data);
        }, function (response_error) {
            var temp_data = {};
            temp_data["userName"] = "";
            temp_data["password"] = "";
            temp_data["confirmPassword"] = "";
            temp_data["firstName"] = "";
            temp_data["lastName"] = "";
            temp_data["email"] = "";
            temp_data["active"] = "";
            temp_data["phone"] = "";
            temp_data["address"] = "";
            temp_data["address2"] = "";
            temp_data["city"] = "";
            temp_data["zip"] = "";
            temp_data["managerName"] = "";
            temp_data["areaCode"] = "";
            temp_data["fax"] = "";
            temp_data["state"] = "";

            temp_data["primaryFacility"] = sessionStorage.facilityId;
            temp_data["role"] = "";
            temp_data["customerNumber"] = jobCustomerNumber;
            self.userProfileInfo = new userProfileVm(temp_data);
			showErrorResponseText(response_error, false);
            //$('#alertmsg').text(response_error.responseText);
            //$('#popupDialog').popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
            //if (response_error.status == 0) {
            //    $('#alertmsg').text("Unknown Error: Contact a Site Administrator");
            //    $('#popupDialog').popup('open');
                //$("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
//            }
//            else {
//                $('#alertmsg').text(response_error.statusText);
//                $('#popupDialog').popup('open');
//                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
//            }
        });
    }
    else {
        self.userProfileInfo = new userProfileVm({});
    }
    //Creting new user
    self.createUserClick = function (user_info) {
        if (!userProfileValidation()) return false;
        var post_user_data = $.parseJSON(ko.toJSON(user_info.userProfileInfo));
        post_user_data.phone = (post_user_data.phone != undefined && post_user_data.phone != null && post_user_data.phone != "") ? post_user_data.phone.replace(/-/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/ /g, '') : '';
        post_user_data.fax = (post_user_data.fax != undefined && post_user_data.fax != null && post_user_data.fax != "") ? post_user_data.fax.replace(/-/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/ /g, '') : '';
        post_user_data.zip = (post_user_data.zip != undefined && post_user_data.zip != null && post_user_data.zip != "") ? post_user_data.zip.replace(/-/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/ /g, '') : '';
        post_user_data.primaryFacility = (post_user_data.primaryFacility != undefined && post_user_data.primaryFacility != null && post_user_data.primaryFacility != "") ? post_user_data.primaryFacility : ($("#ddlFacility").val() != "" ? $("#ddlFacility").val() : '');
        post_user_data.customerNumber = (post_user_data.customerNumber != undefined && post_user_data.customerNumber != null && post_user_data.customerNumber != "") ? post_user_data.customerNumber : ($("#ddlCompany").val() != "" ? $("#ddlCompany").val() : '');

        delete post_user_data['confirmPassword'];
        postCORS(serviceURLDomain + "api/Membership_create_update", JSON.stringify(post_user_data), function (response) {
            var msg = 'User ' + ((sessionStorage.selectedUser != undefined && sessionStorage.selectedUser != null && sessionStorage.selectedUser != "") ? ' updated ' : ' created ') + 'successfully.';
            if (response.replace(/"/g, "") != "success") {
                msg = "Data was not saved.";
                $('#alertmsg').text(msg);
                $('#popupDialog').popup('open');
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
            }
            else {
                sessionStorage.removeItem('selectedUser');
                $('#alertmsg').text(msg);
                $('#popupDialog').popup('open');
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');displayGizmoUsers('gizmo')");
            }
        }, function (response_error) {
            showErrorResponseText(response_error, false);
            //$('#alertmsg').text(response_error.responseText);
            //$('#popupDialog').popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
        });

    };
}
var userProfileValidation = function () {
    var msg = "";
    var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var phoneFaxRegx = /^\(?[0-9]{3}(\-|\)) ?[0-9]{3}-[0-9]{4}$/;
    var zipRegx = /^\d{5}$|^\d{5}-\d{4}$/;
    var regularExpression = /(?=(?:.*?[!@#$%*()_+^&}{:;?.]){1})(?!.*\s)[0-9a-zA-Z!@#$%*()_+^&].{7,30}$/;
    if ($("#txtUserName").val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter Username";
        else
            msg = "Please enter Username";
        $("#txtPassword").focus();
    }
    if(sessionStorage.selectedUser == undefined || sessionStorage.selectedUser == null ||sessionStorage.selectedUser == ""){
        if ($("#txtPassword").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Password";
            else
                msg = "Please enter Password";
            $("#txtPassword").focus();
        }
        
        if ($("#txtPassword").val() != "" && !regularExpression.test($("#txtPassword").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Password should contain atleast 8 characters,1 numeric,1 non alpha-numeric character.";
            else
                msg = "Password should contain atleast 8 characters,1 numeric,1 non alpha-numeric character.";
            $("#txtPassword").focus();
        }
        if (msg == "" && ($("#txtPassword").val() != $("#txtConfirmPassword").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Password and Confirm password  must be the same";
            else
                msg = "Password and Confirm password  must be the same";
        }
    }
    if ($("#txtFirstName").val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter First name";
        else
            msg = "Please enter First name";
        $("#txtFirstName").focus();
    }
    if ($("#txtLastName").val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter Last name";
        else
            msg = "Please enter Last name";
        $("#txtLastName").focus();
    }
    if ($("#txtEmail").val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter Email address.";
        else
            msg = "Please enter Email address.";
    }
    
    else if (!regEmail.test($("#txtEmail").val())) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter valid Email address.";
        else
            msg = "Please enter valid Email address.";
    }
    
    if ($("#ddlCompany").val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select Company";
        else
            msg = "Please select Company";
        $("#ddlCompany").focus();
    }
    if ($("#ddlFacility").val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select Facility";
        else
            msg = "Please select Facility";
        $("#ddlFacility").focus();
    }
    if ($("#ddlRole").val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select Role";
        else
            msg = "Please select Role";
        $("#ddlRole").focus();
    }
   
    if ($('#txtPhone').val() != "" && !phoneFaxRegx.test($('#txtPhone').val())) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter valid Phone number.";
        else
            msg = "Please enter valid Phone number.";
    }
    if ($('#txtFax').val() != "" && !phoneFaxRegx.test($('#txtFax').val())) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter valid Fax number.";
        else
            msg = "Please enter valid Fax number.";
    }
    if ($('#txtZip').val() != "" && !zipRegx.test($('#txtZip').val())) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter valid Zip code.";
        else
            msg = "Please enter Zip code.";
    }
    if (msg != "") {
        $('#alertmsg').html(msg);
        $('#alertmsg').css('text-align', 'left');
        $('#popupResetPassword').popup('close');
        $("#popupDialog").popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupResetPassword').popup('open');");
        return false;
    }
    return true;

}
var loadStates = function () {
    $.getJSON(gStateServiceUrl, function (result) {
        statesJSON = result;
    });
    $.getJSON(gFacilityServiceUrl, function (result) {
        facilityData = result.facility;
        primaryFacilityData = result.primaryfacility;
    });

    getCORS(serviceURLDomain + "api/Customer/", null, function (data) {
        companyData = data;
    }, function (error_response) {
        showErrorResponseText(error_response,true);
    });

}
function displayGizmoUsers(type) {
    if (sessionStorage.selectedUser != undefined && sessionStorage.selectedUser != null && sessionStorage.selectedUser != "") {
        sessionStorage.removeItem('selectedUser');
    }
    if (type == "gizmo") {
        window.location.href = "adminUsers.html";
    }
    else {
        window.location.href = "../index.htm";
    }
}

