﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gFacilityServiceUrl = "../JSON/_facility.JSON";
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var gServiceAdminUserUrl = (sessionStorage.customerNumber != JETS_CUSTOMER_NUMBER) ? " ../userProfile/JSON/_adminUser.JSON" : " ../userProfile/JSON/_JetsUsers.JSON";
var gServiceAdminUserData;
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$.getJSON(gFacilityServiceUrl, function (result) {
    primaryFacilityData = result.facility;
});
$('#_adminUser').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_adminUser');
    createConfirmMessage("_adminUser");

    getCORS(serviceURLDomain + "api/Customer/", null, function (data) {
        companyData = data;
        $('#companyTemplate').tmpl(companyData).appendTo('#ddlCustomer');
    }, function (error_response) {
        showErrorResponseText(error_response,true);
    });
    //createOrderConfirmMessage("_userProfile");
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    var post_json = {};
    post_json = {
        "active": "0",
        "customerNumber": jobCustomerNumber,
        "facility": sessionStorage.facilityId
    };
    //$.getJSON(gServiceAdminUserUrl, function (data) {
    postCORS(serviceURLDomain + "api/Membership_getUserList", JSON.stringify(post_json), function (data) {
        gServiceAdminUserData = data;
    },
     function (response_error) {
			showErrorResponseText(response_error,false);
             //$('#alertmsg').text("Unknown Error: Contact a Site Administrator");
             //$('#popupDialog').popup('open');
             $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
     });

});

$(document).on('pageshow', '#_adminUser', function (event) {
    window.setTimeout(function bindUsersInfo() {
        $('#facilityTemplate').tmpl(primaryFacilityData).appendTo('#ddlFacility');
        $('#ddlCustomer').val(jobCustomerNumber).selectmenu('refresh');
        $('#ddlFacility').val(sessionStorage.facilityId).selectmenu('refresh');
        if (appPrivileges.customerNumber != "1") {
            $('#ddlCustomer').attr('disabled', 'disabled');
            $('#ddlFacility').attr('disabled', 'disabled');
        }
        pageObj = new loadUsersInfo();
        ko.applyBindings(pageObj);
        window.setTimeout(function renderUI() {
            $('#ulAdminUserData').listview('refresh');
        }, 50);
    }, 1000);
});

var adminUserProfileVm = function (data) {
    var self = this;
    self.user = data.userName;
    self.company = data.company;
    self.status = data.status;
    self.userName = data.userName;
    self.userEmail = data.email;
}

var loadUsersInfo = function () {
    var self = this;
    self.diplayAdminUserData = ko.observableArray([]);
    //    var post_json = {};
    //    post_json = {
    //        "active": "0",
    //        "customerNumber": CW_CUSTOMER_NUMBER,
    //        "facility": "1"
    //    };
    //    //$.getJSON(gServiceAdminUserUrl, function (data) {
    //    postCORS(serviceURLDomain + "api/Membership_getUserList", JSON.stringify(post_json), function (data) {
    var temp_data = {};
    $.each(gServiceAdminUserData, function (key, val) {
        temp_data = {};
        ko.mapping.fromJS(val, {}, temp_data);
        self.diplayAdminUserData.push(temp_data);
        //$.each(val, function (key1, val1) {
        //self.diplayAdminUserData.push(new adminUserProfileVm(val));

        //});
    });
    //    },
    //     function (response_error) {
    //         if (response_error.status == 0) {
    //             $('#alertmsg').text("Unknown Error: Contact a Site Administrator");
    //             $('#popupDialog').popup('open');
    //             $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    //         }
    //         else {
    //             $('#alertmsg').text(response_error.responseText);
    //             $('#popupDialog').popup('open');
    //             $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    //         }
    //     });



    self.userClick = function (data) {
        sessionStorage.selectedUser = data.userName();
        window.location.href = " ../userProfile/userProfile.html";
    };
    self.searchClick = function (data) {
        if (validateUserData()) {
            var post_json = {};
            post_json = {
                "active": "0",
                "customerNumber": $('#ddlCustomer').val(),
                "facility": $('#ddlFacility').val()
            };
            if ($('#txtUserName').val() != "")
                post_json["userName"] = $('#txtUserName').val();

            if ($('#txtFirstName').val() != "")
                post_json["firstName"] = $('#txtFirstName').val();

            if ($('#txtLastName').val() != "")
                post_json["lastName"] = $('#txtLastName').val();

            if ($('#txtEmail').val() != "")
                post_json["email"] = $('#txtEmail').val();

            if ($('#ddlRole').val() != "-1")
                post_json["role"] = $('#ddlRole').val();

            postCORS(serviceURLDomain + "api/Membership_getUserList", JSON.stringify(post_json), function (data) {
                gServiceAdminUserData = data;
                var temp_data = {};
                self.diplayAdminUserData([]);
                $.each(gServiceAdminUserData, function (key, val) {
                    temp_data = {};
                    ko.mapping.fromJS(val, {}, temp_data);
                    self.diplayAdminUserData.push(temp_data);
                });
                $('#ulAdminUserData').listview('refresh');
            },
            function (response_error) {
					showErrorResponseText(response_error,false);
                    //$('#alertmsg').text("Unknown Error: Contact a Site Administrator");
                    //$('#popupDialog').popup('open');
                    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
            });

        }
    };
    self.cancelClick = function () {
        window.location.href = "../index.htm";
    };
};


var displayAdminUser = function () {
    var admin_arr = [];
    self.diplayAdminUserData;

}

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else {
        window.location.href = " ../userProfile/userProfile.html";
    }
}

function validateUserData() {
    var msg = "";
    var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var regFirstLast = /^[a-zA-Z ]*$/;
    if (appPrivileges.customerNumber == "1") {
        if ($("#ddlCompany").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select Company Name.";
            else
                msg = "Please select Company Name.";
        }
        if ($("#ddlFacility").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select Facility.";
            else
                msg = "Please select Facility.";
        }
    }
    if ($("#txtFirstName").val() != "" && !regFirstLast.test($("#txtFirstName").val())) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter valid First Name";
        else
            msg = "Please enter valid First Name";
    }
    if ($("#txtLastName").val() != "" && !regFirstLast.test($("#txtLastName").val())) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter valid Last Name";
        else
            msg = "Please enter valid Last Name";
    }

    if ($("#txtEmail").val() != "" && !regEmail.test($("#txtEmail").val())) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please enter valid Email address.";
        else
            msg = "Please enter valid Email address.";
    }

    if (msg != "") {
        $('#alertmsg').html(msg);
        $('#alertmsg').css('text-align', 'left');
        $('#popupResetPassword').popup('close');
        $("#popupDialog").popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close')");
        return false;
    }
    return true;
}
