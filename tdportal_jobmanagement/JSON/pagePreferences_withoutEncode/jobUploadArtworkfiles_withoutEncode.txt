{
    "pageName": "uploadArtwork.html",
    "userPagePrefsDict": {},
    "rolePagePrefsDict": {},
    "customerPagePrefsDict": {
        "155370": {
            "divPrefsDict": {
                "dvSelectedLocations": {
                    "name": "dvSelectedLocations",
                    "showControl": 1
                },
                "dvVersionCode": {
                    "name": "dvVersionCode",
                    "showControl": 1
                }
            },
			"scriptBlockDict" : {
				"pageshow" : "loadSelectedStores('Art');",
				"file_dataPush" : "file_data.push({
								"fileName": "",
								"fileSizePretty": "",
								"fileStatus": "",
								"locationId": "",
								"versionCode": ""
							});",
				"makeFileListAddLocation" : "' | Location: ' + val.locationId + ((val.versionCode != undefined && val.versionCode != "") ? ' | Version : ' + val.versionCode : "");",
				"addFileInfo" : "file_list = jQuery.grep(file_info, function (obj) {
								return obj.locationId != undefined && obj.fileName.substring(obj.fileName.indexOf('.') + 1) === file_type && obj.locationId.toLowerCase() === locationId.toLowerCase();
							});
							if (!isNaN(parseInt(selectedKey)) && file_list.length > 0) {
								file_info[selectedKey].fileName = file_name;
								file_info[selectedKey].locationId = locationId;
								file_info[selectedKey].versionCode = version_code;
							}
							else {
								if (file_list.length > 0) {
									$.each(file_list, function (key1, val1) {
										if (val1.fileName == file_name && file_type == val1.fileName.substring(val1.fileName.indexOf('.') + 1)) {
											val1.fileName = file_name;
											is_file_exists = true;
										}

										if (is_file_exists) {
											sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
											buildOutputLists(true);
											return;
										}

									});
								}
								if (file_list.length == 0 && file_info[0].fileName == "") {
									file_info[0].fileName = file_name;
									file_info[0].locationId = locationId;
									file_info[0].versionCode = version_code;
								}
								else if (!is_file_exists) {
										file_info.push({
											"fileName": file_name,
											"fileSizePretty": "",
											"fileStatus": "",
											"uploadFileType": file_type,
											"locationId": locationId,
											"versionCode": version_code
										});
								}
							}",
				"updateFileInfo" : "var file_info = jQuery.grep(artFilesToUpload, function (obj) {
									return obj.fileName === file_name && obj.fileName.substring(obj.fileName.indexOf('.') + 1) === file_type && obj.locationId.toLowerCase() == selected_location.toLowerCase();
								});
								if (file_info.length > 0) {
									file_info[0].fileName = file_name;
									file_info[0].locationId = selected_location;
									file_info[0].versionCode = $('#txtVersionCode').val();
									var tdFileInputsTemp = document.getElementById('spnFileInput');
									var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
									var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
									$('#' + fileInput.parentNode.parentNode.id).find('input[id=' + fileInput.id.substring(0, fileInput.id.length - 2) + ']').remove();
									$(tdFileInputsTemp).find('input[type=file]')[$(tdFileInputsTemp).find('input[type=file]').length - 1].style.display = "";
									uploadingFiles = (uploadingFiles.split('|').length > 0) ? uploadingFiles.substring(0, uploadingFiles.lastIndexOf('|')) : "";
									$('#popupListDefinition').popup('close');
									$('#validationAlertmsg').html('The selected file already exists in the list. Please select another file.');
									$("#validationPopupDialog").popup('open');
								}
								else {
									if (file_type != undefined && file_name != undefined) {
										var file_list_info = {};
										file_list_info["fileName"] = file_name;
										file_list_info["locationId"] = selected_location;
										file_list_info["versionCode"] = $('#txtVersionCode').val();
										artFilesToUpload.push(file_list_info);
									}
									else {
										resetFileUploadPopup();
										$('#popupListDefinition').popup('close');
										$('#hidUploadFile').val('');
									}
								}",
				"updateEditFileInfo" : "var file_info = jQuery.grep(artFilesToUpload, function (obj) {
									return obj.fileName === old_file_name && obj.fileName.substring(obj.fileName.indexOf('.') + 1) === old_file_type && obj.locationId.toLowerCase() == old_file_location.toLowerCase();
								});
								if (file_info.length > 0) {
									file_info[0].fileName = file_name;
									file_info[0].locationId = selected_location;
									file_info.versionCode = $('#txtVersionCode').val();
									$('#popupListDefinition').popup('close');
									resetFileUploadPopup();
								}", 
				"removeArtworkFilePushKey" : "if (location_id != "null") {
								if (val1.fileName.substring(val1.fileName.indexOf('.') + 1).toLowerCase() === file_type.toLowerCase() && val1.fileName.toLowerCase() === file_name.toLowerCase() && val1.locationId.toLowerCase() == location_id.toLowerCase()) {
									remove_files.push(key1);
								}}", 
				"removeArtworkFilePushStructure" : "file_info.push({
									"fileName": "",
									"fileSizePretty": "",
									"fileStatus": "",
									"uploadFileType": "",
									"locationId": "",
									"versionCode": ""
								});",
				"popUpListDefinitionHide" : "if ((v.fileName.substring(v.fileName.indexOf('.') + 1) == file_type) && (v.locationId.toLowerCase() == location_id.toLowerCase()) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name))
									index = count - 1;
								return (v.fileName.substring(v.fileName.indexOf('.') + 1) == file_type) ? ((v.locationId.toLowerCase() == location_id.toLowerCase()) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name) ? false : true) : true;",
				"makeFileListBeforePost" : "'<p>Type: ' + val.fileName.substring(val.fileName.indexOf('.') + 1) + ' | Location: ' + val.locationId + ((val.versionCode != "") ? ' | Version: ' + val.versionCode : "") + '</p>';",
				"removeFileBeforeUpload" : "if ((v.fileName.substring(v.fileName.indexOf('.') + 1) === filetype_to_remove) && (v.fileName.substring(v.fileName.lastIndexOf('\\') + 1) === file_name_to_remove) && (v.locationId.toLowerCase() == location_id.toLowerCase()))
								index = count - 1;
								return (v.fileName.substring(v.fileName.indexOf('.') + 1) === filetype_to_remove) ? ((v.locationId.toLowerCase() == location_id.toLowerCase()) && (v.fileName.substring(v.fileName.lastIndexOf('\\') + 1) === file_name_to_remove) ? false : true) : true;"
			}
        },
        "1": {
            "divPrefsDict": {
                "dvSelectedLocations": {
                    "name": "dvSelectedLocations",
                    "showControl": 0
                },
                "dvVersionCode": {
                    "name": "dvVersionCode",
                    "showControl": 0
                }
            },
			"scriptBlockDict" : {
				"pageshow" : "",
				"file_dataPush" : "file_data.push({
								"fileName": "",
								"fileSizePretty": "",
								"fileStatus": ""
							});",
				"makeFileListAddLocation" : "",
				"addFileInfo" : "file_list = jQuery.grep(file_info, function (obj) {
								return obj.fileName.substring(obj.fileName.indexOf('.') + 1) === file_type;
							});
							if (!isNaN(parseInt(selectedKey)) && file_list.length > 0) {
								file_info[selectedKey].fileName = file_name;
							}
							else {
								if (file_list.length > 0) {
									$.each(file_list, function (key1, val1) {
										if (val1.fileName == file_name && file_type == val1.fileName.substring(val1.fileName.indexOf('.') + 1)) {
											val1.fileName = file_name;
											is_file_exists = true;
										}

										if (is_file_exists) {
											sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
											buildOutputLists(true);
											return;
										}

									});
								}
								if (file_list.length == 0 && file_info[0].fileName == "") {
									file_info[0].fileName = file_name;
								}
								else if (!is_file_exists) {
										file_info.push({
											"fileName": file_name,
											"fileSizePretty": "",
											"fileStatus": "",
											"uploadFileType": file_type
										});
								}
							}",
				"updateFileInfo" : "var file_info = jQuery.grep(artFilesToUpload, function (obj) {
									return obj.fileName === file_name && obj.fileName.substring(obj.fileName.indexOf('.') + 1) === file_type;
								});
								if (file_info.length > 0) {
									file_info[0].fileName = file_name;
									var tdFileInputsTemp = document.getElementById('spnFileInput');
									var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
									var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
									$('#' + fileInput.parentNode.parentNode.id).find('input[id=' + fileInput.id.substring(0, fileInput.id.length - 2) + ']').remove();
									$(tdFileInputsTemp).find('input[type=file]')[$(tdFileInputsTemp).find('input[type=file]').length - 1].style.display = "";
									uploadingFiles = (uploadingFiles.split('|').length > 0) ? uploadingFiles.substring(0, uploadingFiles.lastIndexOf('|')) : "";
									$('#popupListDefinition').popup('close');
									$('#validationAlertmsg').html('The selected file already exists in the list. Please select another file.');
									$("#validationPopupDialog").popup('open');
								}
								else {
									if (file_type != undefined && file_name != undefined) {
										var file_list_info = {};
										file_list_info["fileName"] = file_name;
										artFilesToUpload.push(file_list_info);
									}
									else {
										resetFileUploadPopup();
										$('#popupListDefinition').popup('close');
										$('#hidUploadFile').val('');
									}
								}",
				"updateEditFileInfo" : "var file_info = jQuery.grep(artFilesToUpload, function (obj) {
									return obj.fileName === old_file_name && obj.fileName.substring(obj.fileName.indexOf('.') + 1) === old_file_type;
								});
								if (file_info.length > 0) {
									file_info[0].fileName = file_name;
									$('#popupListDefinition').popup('close');
									resetFileUploadPopup();
								}", 
				"removeArtworkFilePushKey" : "if (val1.fileName.substring(val1.fileName.indexOf('.') + 1).toLowerCase() === file_type.toLowerCase() && val1.fileName.toLowerCase() === file_name.toLowerCase()) {
									remove_files.push(key1);
								}", 
				"removeArtworkFilePushStructure" : "file_info.push({
									"fileName": "",
									"fileSizePretty": "",
									"fileStatus": "",
									"uploadFileType": ""
								});",
				"popUpListDefinitionHide" : "if ((v.fileName.substring(v.fileName.indexOf('.') + 1) == file_type) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name))
									index = count - 1;
								return (v.fileName.substring(v.fileName.indexOf('.') + 1) == file_type) ? ((v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name) ? false : true) : true;",
				"makeFileListBeforePost" : "'<p>Type: ' + val.fileName.substring(val.fileName.indexOf('.') + 1) + '</p>';",
				"removeFileBeforeUpload" : "if ((v.fileName.substring(v.fileName.indexOf('.') + 1) === filetype_to_remove) && (v.fileName === file_name_to_remove))
								index = count - 1;
								return (v.fileName.substring(v.fileName.indexOf('.') + 1) === filetype_to_remove) ? ((v.fileName === file_name_to_remove) ? false : true) : true;"
			}
        },
        "155711": {            
			"scriptBlockDict" : {
				"pageshow" : "",
				"file_dataPush" : "",
				"makeFileListAddLocation" : "",
				"addFileInfo" : "",
				"updateFileInfo" : "",
				"updateEditFileInfo" : "", 
				"removeArtworkFilePushKey" : "", 
				"removeArtworkFilePushStructure" : "",
				"popUpListDefinitionHide" : "",
				"makeFileListBeforePost" : "",
				"removeFileBeforeUpload" : ""
			}
        },
        "999900": {            
			"scriptBlockDict" : {
				"pageshow" : "",
				"file_dataPush" : "",
				"makeFileListAddLocation" : "",
				"addFileInfo" : "",
				"updateFileInfo" : "",
				"updateEditFileInfo" : "", 
				"removeArtworkFilePushKey" : "", 
				"removeArtworkFilePushStructure" : "",
				"popUpListDefinitionHide" : "",
				"makeFileListBeforePost" : "",
				"removeFileBeforeUpload" : ""
			}
        }
    },
    "defaultPagePrefs": {
        "buttonPrefsDict": {}
    }
}