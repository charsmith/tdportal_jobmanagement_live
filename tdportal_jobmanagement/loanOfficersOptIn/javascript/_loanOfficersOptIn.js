﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var selectedStoreId;
var gOutputData;
var gDataStartIndex = 0;
var gInnerData = [];
var storeData = "";

var gServiceUrl = serviceURLDomain + "api/Authorization";
var gEditJobService = serviceURLDomain + "api/JobTicket/";

var urlString = unescape(window.location);
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
var params = queryString.split("&");
var encryptedString = (params[0] != undefined && params[0] != "") ? params[0] : "";

var email = "";
var user = "";
var password = "";
var customerNumber = "";
var jobNumber = "";
var facilityId = "";
var loanOfficersOptInData = "";

var districtOptInOutEncryptedURL = serviceURLDomain + 'api/Encryption_decrypt/';
var districtOptInOutURL = serviceURLDomain + 'api/ManagerApproval_stores/';
var optInPOSTUrl = serviceURLDomain + 'api/ManagerApproval_stores/';

//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_loanOfficersOptIn').live('pagebeforecreate', function (event) {
    displayMessage('_loanOfficersOptIn');
    $.getJSON("JSON/_loanOfficersOptIn.JSON", function (data) {
        loanOfficersOptInData = data;
        if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
            alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
            window.open('', '_self', '');
            window.close();
            return false;
        }
        else {
            //window.setTimeout(function () {
                getData();
            //}, 5000);
        }
    });
    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage 
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }

    if (sessionStorage.desktop) // desktop storage 
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    else if (localStorage.mobile) // mobile storage
        return true;
});

//******************** Public Functions Start **************************
function getData() {
    makeGData();
}

function makeGData() {
    if (sessionStorage.isLoOptin == undefined || sessionStorage.isLoOptin == null && sessionStorage.isLoOptin == "" && sessionStorage.isLoOptin == "false") {
        getCORS(districtOptInOutEncryptedURL + encryptedString, null, function (url_with_data) {
            //angie.white@caseys.com|admin_cgs1|admin_cgs1|155711|83965|1
            var data_split = [];
            if (url_with_data != undefined && url_with_data != "") {
                data_split = url_with_data.split('|');

                email = (data_split[0] != undefined && data_split[0] != "") ? data_split[0] : "";
                user = (data_split[1] != undefined && data_split[1] != "") ? data_split[1] : "";
                
                password = (data_split[2] != undefined && data_split[2] != "") ? data_split[2] : "";
                customerNumber = (data_split[3] != undefined && data_split[3] != "") ? data_split[3] : "";
                customerNumber = AAG_CUSTOMER_NUMBER; // TO be removed after Jet's Demo.

                jobNumber = (data_split[4] != undefined && data_split[4] != "") ? data_split[4] : "";
                facilityId = (data_split[5] != undefined && data_split[5] != "") ? data_split[5] : "";
                sessionStorage.authString = makeBasicAuth(user, password);

                user = "user_aag";
                sessionStorage.authString = makeBasicAuth("user_aag", "welcome123$");
                jobNumber = "-16719";
                //jobNumber = "-1";
                facilityId = "2"                
                loginAuthorization(url_with_data);
               
            }

        }, function (error_response) {
            showErrorResponseText(error_response);
        });
    }
    if (customerNumber == AAG_CUSTOMER_NUMBER) {
        var temp_json = {
            "description": "February 2016 Lunch & Learn",
            "inHomeDate": "2/1/2016",
            "optInEndDate": "2/29/2016",
        };
        //customerNumber = jobCustomerNumber;
        gData = temp_json;
        displayStoresList();
        displayImages(customerNumber);
        setOptInApprovalLogos(customerNumber);
    }
}

//Funtions to authorise the user and get the job info.  --- START ---
function makeBasicAuth(user, password) {
    var tok = user + ':' + password;
    var hash = Base64.encode(tok);
    return "Basic " + hash;
}

function displayImages(customer_number) {
    if (customer_number == AAG_CUSTOMER_NUMBER) {
        $("#headerText").text(loanOfficersOptInData.headerText);
        $("#paragraphOptIn").text(loanOfficersOptInData.paragraphOptIn);
        $("#orderedListInfo").html(loanOfficersOptInData.orderedListInfo);
        $("#paragraphUserMessage").html(loanOfficersOptInData.paragraphUserMessage);
        $("#paragraphNote").text(loanOfficersOptInData.paragraphNote);

        var images_path = logoPath + "AAG/";
        var img_front = images_path + loanOfficersOptInData.pizzaVersion1Source;
        var img_back = images_path + loanOfficersOptInData.pizzaVersion2Source;
        $('#imagePizzaVersion1').attr('src', img_front);
        $('#imgPizzaVersion1Name').html(loanOfficersOptInData.pizzaVersion1Name);
        $('#imagePizzaVersion1').attr('alt', loanOfficersOptInData.pizzaVersion1AlternateText);
        $('#imagePizzaVersion2').attr('src', img_back);
        $('#imgPizzaVersion2Name').html(loanOfficersOptInData.pizzaVersion2Name);
        $('#imagePizzaVersion2').attr('alt', loanOfficersOptInData.pizzaVersion2AlternateText);
        $('#aDOO').attr('href', images_path + loanOfficersOptInData.dooReferenceLink);
        $('#aPostCard').attr('href', images_path + loanOfficersOptInData.postcardReferenceLink);
    }
}

//load the store locations...
function displayStoresList() {
    storeData = "";
    //gData = null;
    if ((gData == null || gData == "")) {
        var empty_message = "<br/><div style='font-size: 12px; font-weight: bold; color:red;'>Attention. No stores were found for your user login. " +
                            " Please contact caseysgeneralstore@tribunedirect.com if you have any questions.</font>";
        $('#spText').html(empty_message);
        $('#trInstructions').hide();
        $('#btnSubmitOpt').css('display', 'none');
        $('#trImgPizzaVersion').css("display", "none");
        $('#optInContents').css('display', 'none');
    } else {

        var the_text = "";
        var sign_in_end_date = new Date(gData.optInEndDate);
        var current_date = new Date();
        var sign_in_end_date_diff = dateDifference(sign_in_end_date, current_date);
        if (parseInt(sign_in_end_date_diff) >= 1) {
            the_text = '<div style="font-size: 12px; font-weight: bold; color:red;text-align:center;"><br />The Opt In period has Closed.</div>';
            $('#btnSubmitOpt').hide();
            $('#trInstructions').hide();
            $('#spText').html(the_text);
            $('#trImgPizzaVersion').css("display", "none");
            $('#optInContents').css('display', 'none');
        } else {
            //the_text = '<div style="font-size: 12px; font-weight: normal">Check the stores below to opt-in to the campaign, select a postcard and submit your selections.</div>';
            //$('#spText').html(the_text);
            $.each(gData, function (key, val) {
                switch (key.toLowerCase()) {
                    case "description":
                        storeData += '<li><b>Campaign:</b> ' + val + '</li>';
                        $('#navHeader').html(val);
                        //storeData += val;
                        break;
                    case "inhomedate":
                        storeData += '<li><b>In-Home Start:</b> ' + val + '</li>';
                        break;
                    case "optinenddate":
                        storeData += '<li style="color:#C00"><b>Opt-In Period Ends:</b> ' + val + '</li>';
                        $("#hdnOptInDate").val(val);
                        break;
                        //case "image":  
                        //    $('#trImgPizzaVersion').css("display", "block");  
                        //    break;  
                }
            });
            $('#ulPostCardInfo').html(storeData);
        }
    }
}

function submitClicked() {
    window.setTimeout(function () {
        if ($('#chkOptInStartSetup').is(':checked')) {
            window.location.href = "../jobSelectLocations/jobSelectLocations.html";
        }
        else if ($('#chkOptInStartSetupTwoDates').is(':checked')) {
            sessionStorage.setUpTwoDate = true;
            window.location.href = "../jobSelectLocations/jobSelectLocations.html";
        }
        else if ($('#chkOptOut').is(':checked')) {
            $('#alertmsg').text("Thank you. You have been opted-out for this campaign. If you change your mind, click the Opt-In button to change your status.");
            $('#popupDialog').popup('open');
            return false;
        }
        else {
            $('#alertmsg').text("Please make an Opt-In or Opt-out selection.");
            $('#popupDialog').popup('open');
        }
    }, 1000);
}

function loginAuthorization(response) {
    getCORS(gServiceUrl, null, function (data) {
        if (data.Message != undefined) {
            $('#alertmsg').text(data.Message);
            $('#popupDialog').popup('open');
            return false;
        }
        else {

            //data.roleName = "power";

            var key_to_delete = [];
            $.each(data, function (key, val) {
                if (key.toLowerCase().indexOf("display_page_") > -1) {
                    //if (key.toLowerCase().indexOf("display_page_submitorder") == -1) {
                    key_to_delete.push(key);
                    //}                   
                }
            });
            $.each(key_to_delete, function (key, val) {
                delete data[val];
            });
            var pages = ["selectLocations", "selectImages", "customText", "listSelection", "proofing", "submitOrder"];
            var index = 0;
            var temp_data = {};
            $.each(data, function (key, val) {
                if (key.toLowerCase().indexOf("page_home") > -1) {
                    $.each(pages, function (idx, page_name) {
                        temp_data["display_page_" + page_name] = "true";
                    });
                    temp_data[key] = val;
                    //return false;
                }
                else
                    temp_data[key] = val;
                index++;
            });
            captureAndNavigate(temp_data, response);
        }

    }, function (response_error) {
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        showErrorResponseText(response_error, false);
    });
}

function getEditJobData() {
    getCORS(gEditJobService + facilityId + "/" + jobNumber + "/" + sessionStorage.username, null, function (data) {
        var g_output_data = data;
        sessionStorage.jobSetupOutput = JSON.stringify(g_output_data);
        sessionStorage.jobSetupOutputCompare = JSON.stringify(g_output_data);
        gOutputData = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput) : [];
        sessionStorage.jobDesc = g_output_data.jobName;

        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
                if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                    artworkFileInfo[0] = (gOutputData.artworkAction.artworkFileList[fileName] != undefined && gOutputData.artworkAction.artworkFileList[fileName] != null && Object.keys(gOutputData.artworkAction.artworkFileList[fileName]).length > 0) ? gOutputData.artworkAction.artworkFileList[fileName] : {};
                    //                    artworkFileInfo = $.grep(gOutputData.artworkAction.artworkFileList, function (obj) {
                    //                        return obj.fileName === fileName;
                    //                    });
                }
                artworkFileInfo = (artworkFileInfo[0] != undefined && artworkFileInfo[0] != null) ? artworkFileInfo[0] : [];
                $('#btnJobProofs').show();
                if (Object.keys(gOutputData.artworkAction.artworkFileList).length == 1) {
                    $('#btnJobProofs').hide();
                    var available_buttons = $('#dvProofButtons').find('a[data-role=button]');
                    $.each(available_buttons, function (key, val) {
                        if (key == 0) {
                            $(available_buttons[0]).addClass('ui-corner-lt');
                            $(available_buttons[0]).addClass('ui-corner-lb');
                        }
                        else if (key == (available_buttons.length - 1)) {
                            $(available_buttons[0]).addClass('ui-corner-rt');
                            $(available_buttons[0]).addClass('ui-corner-rb');
                        }
                    });
                    //$('#artApproal').addClass('.ui-corner-rt');
                }
            }
        }

        displayJobInfo();
    }, function (error_response) {
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        showErrorResponseText(error_response, false);
    });
}

function captureAndNavigate(data, response) {
    var customer_number = data["customerNumber"];
    data.roleName = "user";
    sessionStorage.username = user;
    sessionStorage.userRole = "user";//(data["roleName"].indexOf('_') > -1) ? data["roleName"].split('_')[0] : data["roleName"];
    sessionStorage.companyName = data["companyName"].toLowerCase();
    sessionStorage.customerNumber = data["customerNumber"];
    sessionStorage.jobCustomerNumber = data["customerNumber"];
    sessionStorage.facilityId = facilityId;
    sessionStorage.jobNumber = jobNumber;
    saveSessionData("appPrivileges", JSON.stringify(data));
    appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
    //sessionStorage.nextPageInOrder = "../jobUploadArtwork/jobUploadArtwork.html";

    //sessionStorage.nextPageInOrder = '../jobUploadArtwork/jobUploadArtwork.html';


    sessionStorage.isLoOptin = true;

    if (data.customerNumber == AAG_CUSTOMER_NUMBER) {
        loadLoanOffiersList();
        loadDemoHintsInfo();
    }
    if (jobNumber != "-1")
        window.setTimeout(function () {
            getEditJobData();
        }, 3000);
    //else {
    //    //if (sessionStorage.userRole == "power")
    //    //    window.location.href = "../jobSelectTemplate/jobSelectTemplate.html";
    //    //else
    //        //window.location.href = "../jobSelectLocations/jobSelectLocations.html";
    //}
    //getEditJobData();
}

function getEditJobData() {
    getCORS(gEditJobService + facilityId + "/" + jobNumber + "/" + sessionStorage.username, null, function (data) {
        sessionStorage.jobDesc = data.jobName;
        makeEditJobData(data);
        //if (sessionStorage.userRole == "power")
        //    window.location.href = "../jobSelectTemplate/jobSelectTemplate.html";
        //else
        //window.location.href = "../jobSelectLocations/jobSelectLocations.html";
    }, function (error_response) {
        showErrorResponseText(error_response, false);
    });
}
//Funtion to authorise the user and get the job info.  --- END ---
//******************** Public Functions End **************************
