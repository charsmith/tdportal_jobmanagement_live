﻿var locationFiltersAndPaging = function (self) {
    ko.utils.extend(self, new displayMapAndMarkers(self)); // Extends the view model to integrate the api to display markers etc.
    //Closes the popups - search
    self.closePopups = function () {
        $('#popupSearch').popup().popup('close');
    };

    self.findAllAvblLocByOIRecipientClick = function (data, event) {
        var ctrl = event.target || event.currentTarget || event.targetElement;
        if (ctrl.checked) {
            $('#dvOptInUsers').css('display', 'block');
        }
        else {
            $('#dvOptInUsers').css('display', 'none');
        }
    };

    self.userSelected = function (data, event) {
        var ctrl = event.currentTarget || event.target;
        if (ctrl.checked)
            self.selectedOptInUser.push(data.userName);
        else {
            var temp_idx;
            $.each(self.selectedOptInUser, function (key, val) {
                if (val == data.userName) {
                    temp_idx = key;
                    return false;
                }
            })
            if (temp_idx != undefined && temp_idx != null && temp_idx !== "")
                self.selectedOptInUser.splice(temp_idx, 1);
        }
    };
    self.clearSearchFields = function () {
        $('#txtLocationName').val('');
        $('#txtStoreId').val('');
        $('#txtCity').val('');
        $('#txtAddress').val('');
        $('#ddlState').val('');
        $('#txtState').val('');
        $('#txtZipCode').val('');
        $('#fldStOptInUsers input[type="checkbox"]').checkboxradio().attr('checked', false).checkboxradio('refresh');
        $('#fldSetStatus input[type="checkbox"]').checkboxradio().attr('checked', false).checkboxradio('refresh');
    };

    self.findAllLocationsChanged = function () {
        self.clearSearchFields();
        createPlaceHolderforIE();
    };

    //Displays the locations search popup 
    //Fields would be displayed based on the type selected - existing/new.
    self.searchLocationsPopup = function (data, event) {
        self.loadOptInUsersAndStatus();
        self.searchText('');
        self.selectedOptInUser = [];
        self.clearSearchFields();
        $('#chkFindAllLocations').attr('checked', false).checkboxradio('refresh');
        $('#chkFindAllAvblLocByOIRecipient').attr('checked', false).checkboxradio('refresh').trigger('change');
        createPlaceHolderforIE();

        $('#headerSearch').text('Search Existing Locations');
        if (self.storeId != undefined && self.storeId != null && $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == null)
            $('#txtStoreId').parent().css('display', 'block');
        else
            $('#txtStoreId').parent().css('display', 'none');
        $('#txtState').parent().css('display', 'block');
        $('#dvDdlState').css('display', 'none');
        $('#txtAddress').parent().css('display', 'block');
        $('#dvNewLocationLink').css('display', 'block');
        $('#txtLocationName').val('');
        $('#txtLocationName').parent().css('display', 'none');
        $('#fsRbtypes').css('display', 'none');
        $('#txtZipCode').parent().css('display', 'block');
        $('#txtZipCode').css('display', 'block');
        $('#dvFindAllLocations').css('display', 'block');
        $('#fldStOptInUsers input[type="checkbox"]').checkboxradio().checkboxradio('refresh');
        $('#fldSetStatus').addClass('ui-corner-all');
        $('#fldSetStatus div').trigger('create');
        $('#fldSetStatus').trigger('create');
        $('#dvStatusList').collapsibleset().collapsibleset('refresh');
        $('#fldSetStatus input[type="checkbox"]').checkboxradio().checkboxradio('refresh');
        $('#fldSetStatus input[type="checkbox"]').attr('checked', true).checkboxradio('refresh');
        $('#dvSearchFilters').collapsibleset('refresh');
        $('#dvFilterByGeography').trigger('collapsibleexpand');
        //$('#ddlDisplayResultsBy').val('status').selectmenu('refresh').trigger('change');
        $('#ddlState').selectmenu().selectmenu('refresh');
        $('#popupSearch').popup().popup('open');
    };

    //Performs the Search button operation.
    self.searchClick = function () {
        if ($('#popupSearch').parent().hasClass('ui-popup-active')) {
            $('#btnSearch').trigger('click');
        }
        else if ($('#popupDialog').parent().hasClass('ui-popup-active')) {
            $('#okBut').trigger('click');
        }
        else if ($('#popUpLocationProfile').parent().hasClass('ui-popup-active')) {
        }
        else if ($('#popUpConfirmChange').parent().hasClass('ui-popup-active')) {
        }
        else
            $('#btnContinue').trigger('onclick');
    };

    //Validates the search fields and searches the locations 
    self.locationsSearch = function () {
        var store_id = ($('#txtStoreId').val() != "") ? $('#txtStoreId').val() : "";
        store_id = (store_id != $('#txtStoreId').attr('placeholder')) ? store_id : "";
        var store_address = ($('#txtAddress').val() != "") ? $('#txtAddress').val() : "";
        store_address = (store_address != $('#txtAddress').attr('placeholder')) ? store_address : "";
        var search_state = ($('#txtState').val() != "") ? $('#txtState').val() : "";
        search_state = (search_state != $('#txtState').attr('placeholder')) ? search_state : "";
        var search_zip = ($('#txtZipCode').val() != "") ? $('#txtZipCode').val() : "";
        search_zip = (search_zip != $('#txtZipCode').attr('placeholder')) ? search_zip : "";

        var search_city = ($('#txtCity').val() != "") ? $('#txtCity').val() : "";
        search_city = (search_city != $('#txtCity').attr('placeholder')) ? search_city : "";
        var search_state = "";
        var search_business_name = "";

        search_state = ($('#txtState').val() != "") ? $('#txtState').val() : "";
        search_state = (search_state != $('#txtState').attr('placeholder')) ? search_state : "";
        var find_all = ($('#chkFindAllLocations').is(':checked')) ? "all" : "";

        var optIn_user = (self.selectedOptInUser.length > 0) ? self.selectedOptInUser.join(',') : "";
        var selected_status = "";
        $.each($('#fldSetStatus').find('input[type="checkbox"]:checked'), function () {
            selected_status += (selected_status != "") ? "," + $(this).val() : $(this).val();
        });

        var msg = "";
        if ($('#ddlDisplayResultsBy').val() != "status") {
            if ($('#ddlDisplayResultsBy').val() == "marketManager" && optIn_user == "") {
                msg = "Please select at least one Market Manager";
            } else if ($('#ddlDisplayResultsBy').val() == "marketManager" && self.selectedOptInUser.length > 4) {
                msg = "Please select not more than four Market Managers to search.";
            } else if ($('#ddlDisplayResultsBy').val() == "state" && search_state == "") {
                msg = "Please enter at least one state";
            } else if ($('#ddlDisplayResultsBy').val() == "state" && search_state != "" && search_state.split(',').length > 4) {
                msg = "Please enter not more than four states to search.";
            }
        }
        if (msg != "") {
            return msg;
        } else {
            var search_json = {};
            if ($('#txtState').parent().css('display') == 'block') {
                if (find_all != "")
                    search_json["all"] = find_all;
                else {
                    if (store_id != "")
                        search_json["storeId"] = (store_id.endsWith(',')) ? store_id.substring(0, store_id.length - 1) : store_id;

                    if (store_address != "")
                        search_json["address"] = (store_address.endsWith(',')) ? store_address.substring(0, store_address.length - 1) : store_address;

                    if (search_city != "")
                        search_json["city"] = (search_city.endsWith(',')) ? search_city.substring(0, search_city.length - 1) : search_city; //.replace(/, /g, ',');

                    if (search_state != "") {
                        search_state = (search_state.endsWith(',')) ? search_state.substring(0, search_state.length - 1) : search_state;
                        if (search_state.indexOf(',') > -1) {
                            var selected_states = search_state.split(',');
                            search_state = selected_states.join(',').replace(/ /g, '');
                        }
                        search_json["state"] = search_state; //.replace(/, /g, ',');
                    }
                    if (search_zip != "")
                        search_json["zip"] = (search_zip.endsWith(',')) ? search_zip.substring(0, search_zip.length - 1) : search_zip;

                    if (optIn_user != "")
                        search_json["marketManager"] = optIn_user;

                    if (selected_status != "")
                        search_json["approvalStatusDesc"] = selected_status;
                }
                self.searchTextExisting = JSON.stringify(search_json);
                self.isPagerDisplayed = false;
                $('#spnSearchTerms').html('');

                $.each(self.statusList(), function (key, val) {
                    self[val.statusKey.replace('-', '').initLower() + "PageId"](0);
                });
                if ($('#ddlDisplayResultsBy').val() == "state" || $('#ddlDisplayResultsBy').val() == "marketManager") {
                    var source_list = ($('#ddlDisplayResultsBy').val() == "state") ? ((search_state != "") ? search_state.split(',') : []) : self.selectedOptInUser;
                    $.each(source_list, function (key, val) {
                        if (self["anonymous" + (key + 1) + "PageId"] == undefined)
                            self["anonymous" + (key + 1) + "PageId"] = ko.observable('0');
                        self["anonymous" + (key + 1) + "PageId"](0);

                        var temp_markers = $.grep(self.markers_array, function (obj) {
                            return obj.type.toLowerCase() === "anonymous" + (key + 1);
                        });
                        if (temp_markers == undefined || temp_markers == null || temp_markers == "") {
                            self.markers_array.push({
                                type: "anonymous" + (key + 1),
                                markers: [],
                                latlngs: [],
                                addresses: []
                            });
                        }
                        else {
                            temp_markers[0].markers = [];
                            temp_markers[0].latlngs = [];
                            temp_markers[0].addresses = [];
                        }
                    });
                }
                self.getFilterdLocations(JSON.stringify(search_json));
            }
        }
    };

    //Gets the existing locations based on the given search criteria.
    self.getFilterdLocations = function (search_json) {
        var temp_search_json = {};
        var source_filter = [];
        var tmp_json = $.parseJSON(search_json);
        self.displayLocations.removeAll();
        if ($('#ddlDisplayResultsBy').val() == "status") {
            $.each($('#fldSetStatus').find('input[type="checkbox"]:checked'), function () {
                source_filter.push($(this).val());
            });
            if (source_filter.length == 0) {
                $.each($('#fldSetStatus').find('input[type="checkbox"]'), function () {
                    source_filter.push($(this).val());
                });
            }
        }
        else {
            if ($('#ddlDisplayResultsBy').val() == "state") {
                if (tmp_json.state != undefined && tmp_json.state != null && tmp_json.state != "") {
                    $.each(tmp_json.state.split(','), function (key, val) {
                        source_filter.push(val);
                    });
                }
            }
            else if ($('#ddlDisplayResultsBy').val() == "marketManager") {
                source_filter = (self.selectedOptInUser.length > 0) ? self.selectedOptInUser : "";
            }
        }
        var group_by_values = [];
        var page_id = 0;
        $.each(source_filter, function (key, val) {
            if ($('#ddlDisplayResultsBy').val() == "status") {
                page_id = self[val.replace('-', '').initLower() + "PageId"]();
            }
            else if ($('#ddlDisplayResultsBy').val() != "status") {
                page_id = self["anonymous" + (key + 1) + "PageId"]();
            }
            group_by_values.push({
                "name": val,
                "pageIndex": page_id
            })

        });
        temp_search_json["groupBy"] = {
            "name": $('#ddlDisplayResultsBy').val(),
            "values": group_by_values
        };
        temp_search_json["search"] = {};
        if (search_json != undefined && search_json != null && search_json != "") {
            temp_search_json.search = search_json;
        }
        self.displaySearchTerms(search_json, tmp_json);
        postCORS(gLocationGroupedSearchUrl + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber + "/" + gOutputData.jobTypeId, JSON.stringify(temp_search_json), function (data) {
            self.gDataFiltered = data;
            self.loadOptInStatusGrid();
            $('#popupSearch').popup().popup('close');
            $('#waitPopUp').popup('open', { positionTo: 'window' });
            $('#waitPopUp').css('z-index', 9999);
            window.setTimeout(function () { self.getLocations(); }, 1000);
        }, function (error_response) {
            $('#popupSearch').popup().popup('close');
            showErrorResponseText(error_response, false);
        });
    };

    self.displaySearchTerms = function (search_json, tmp_json) {
        var geographic_search = "";
        var search_term = "";
        $('#spnSearchTerms').html("");
        if (search_json != undefined && search_json != null && search_json != "") {
            if (tmp_json.all != undefined && tmp_json.all != null && tmp_json.all != '')
                search_term = 'All Available';
            if (tmp_json.storeId != undefined && tmp_json.storeId != null && tmp_json.storeId != '')
                search_term = tmp_json.storeId;
            if (tmp_json.address != undefined && tmp_json.address != null && tmp_json.address != '')
                search_term += ($.trim(search_term) != "") ? ", " + tmp_json.address : tmp_json.address;
            if (tmp_json.city != undefined && tmp_json.city != null && tmp_json.city != '')
                search_term += ($.trim(search_term) != "") ? ", " + tmp_json.city : tmp_json.city;
            if (tmp_json.state != undefined && tmp_json.state != null && tmp_json.state != '')
                search_term += ($.trim(search_term) != "") ? ", " + tmp_json.state : tmp_json.state;
            if (tmp_json.zip != undefined && tmp_json.zip != null && tmp_json.zip != '')
                search_term += ($.trim(search_term) != "") ? ", " + tmp_json.zip : tmp_json.zip;

            geographic_search = (search_term != "") ? '<b>Geography-</b>' + search_term : "";

            if (tmp_json.marketManager != undefined && tmp_json.marketManager != null && tmp_json.marketManager != '')
                geographic_search = (geographic_search != "") ? geographic_search + ' ,<b> Market Managers-</b>' + tmp_json.marketManager : '<b> Market Managers-</b>' + tmp_json.marketManager;
            if (tmp_json.approvalStatusDesc != undefined && tmp_json.approvalStatusDesc != null && tmp_json.approvalStatusDesc != '')
                geographic_search = (geographic_search != "") ? geographic_search + ' ,<b> Status-</b>' + tmp_json.approvalStatusDesc : '<b> Status-</b>' + tmp_json.approvalStatusDesc;
        }
        geographic_search = (geographic_search != "") ? geographic_search + ', <b> Results By: </b>' + $('#ddlDisplayResultsBy option:selected').text() : '<b >Results By: </b>' + $('#ddlDisplayResultsBy option:selected').text();

        $('#spnSearchTerms').html(geographic_search);
    };

    self.getLocations = function () {
        self.allLocations = {};
        self.clearMarkers();
        $.each(self.gDataFiltered.groupByList, function (key, val) {
            self.loadLocations(key, val);
        });
        self.swapGridBlocks();
        $('#mapBlock').css('display', 'block');
        $('#dvLocationGrid').trigger('create');
    };

    self.swapGridBlocks = function () {
        self.displayLocations.removeAll();
        $('#dvLocationGrid').trigger('create');
        if ($('#ddlDisplayResultsBy').val() != "status") {
            $.each(self.allLocations, function (key, val) {
                self.displayLocations.push(val);
            });
            if (self.uIViewConfig.map())
                self.displayLocations.push(ko.observable({ "name": ko.observable("map") })());
        }
        else {
            $.each(self.uIViewConfig, function (key, val) {
                if (val() && (key != "map" && key != "grid")) {
                    if (self.allLocations[key.toLowerCase()] != undefined)
                        self.displayLocations.push(self.allLocations[key.toLowerCase()]);
                }
                else if (val() && key == "map")
                    self.displayLocations.push(ko.observable({ "name": ko.observable("map") })());
            });
        }
        if (self.uIViewConfig.map()) {
            self.initMapCanvas();
        }
    };

    self.viewPrefChanged = function (data, event) {
        var ctrl = event.currentTarget || event.targetElement || event.target;
        if (ctrl.value == "grid") { return false; }
        self.swapGridBlocks();
        $('#mapBlock').css('display', 'block');
        $('#dvLocationGrid').trigger('create');
        if (self.uIViewConfig.map())
            self.initMapCanvas();
    };

    self.loadOptInUsersAndStatus = function () {
        //Loads all the marketManagers.
        self.optInUsersList.removeAll();
        self.statusList.removeAll();
        getCORS(gLocationSearchUrl + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber + '/' + gOutputData.jobTypeId, null, function (data) {
            $.each(data, function (key, val) {
                if (key == "marketManagers") {
                    var managers = [];
                    var temp_managers = {};
                    for (var key in val)
                        managers.push(key);
                    managers.sort();

                    for (var i = 0; i < managers.length; i++) {
                        temp_managers[managers[i]] = val[managers[i]];
                    }
                    $.each(temp_managers, function (manager_key, count_val) {
                        if (manager_key != "") {
                            self.optInUsersList.push(new optInUser(manager_key, count_val));
                        }
                    });
                }
                if (key == "statuses") {
                    $.each(Object.keys(val).sort(), function (sorted_key, sorted_val) {
                        self.statusList.push(new statusInfo(sorted_val, val[sorted_val]));
                        self.uIViewConfig[sorted_val.replace('-', '').initLower()] = ko.observable(true);
                        self[sorted_val.replace('-', '').initLower() + "PageId"] = ko.observable('0');
                        self.markers_array.push({
                            type: sorted_val.replace('-', '').initLower(),
                            markers: [],
                            latlngs: [],
                            addresses: []
                        });
                        if (self.gridColumns.indexOf(sorted_val.replace('-', ' ')) == -1)
                            self.gridColumns.push(sorted_val.replace('-', ' '));
                    });
                    self.uIViewConfig["map"] = ko.observable(true);
                }
            });
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };

    self.statusChanged = function (data, event) {
        var ctrl = event.targetElement || event.currentTarget || event.target;
        if (ctrl.value != "status")
            $('#dvLocationsPrefs input[type=checkbox]:not([value=grid]):not([value=map])').attr('disabled', 'disabled').addClass('ui-disabled').checkboxradio('refresh');
        else
            $('#dvLocationsPrefs input[type=checkbox]:not([value=grid]):not([value=map])').removeAttr('disabled', 'disabled').removeClass('ui-disabled').checkboxradio('refresh');
    }
};