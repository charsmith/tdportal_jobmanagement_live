﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var jobNumber = "";
var facilityId = "";
jobNumber = getSessionData("jobNumber");
facilityId = sessionStorage.facilityId;

var serviceJSONURL = "../optInStatus/JSON/_optInStatus.JSON";
var serviceURL = serviceURLDomain + "api/CheckoutCaseys/" + jobCustomerNumber + "/" + facilityId + "/" + sessionStorage.jobNumber;
var searchLocationsURL = "../optInStatus/JSON/_optInRevisions.JSON";
var gLocationGroupedSearchUrl = serviceURLDomain + 'api/Locations_grouped/';
var gLocationSearchUrl = serviceURLDomain + 'api/Locations/';
var gOutputData;
var locationsJSON = [];
var pageObj;
var optInUsersList = ko.observableArray([]);
var gMapBoundsUSA;
var gMapCenter;
var map;
var geocoder;
var service;
var isPageLoad = true;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_optInStatus').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_optInStatus');
    loadingImg("_optInStatus");
    $('#loadingText1').text('Loading Locations...');
    window.onload = loadScript(true);
    if (sessionStorage.ApprovalCheckoutPrefs == undefined || sessionStorage.ApprovalCheckoutPrefs == null || sessionStorage.ApprovalCheckoutPrefs == "")
        getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, jobCustomerNumber);
    else
        managePagePrefs(jQuery.parseJSON(sessionStorage.ApprovalCheckoutPrefs));
    //test for mobility...

    $.getJSON(searchLocationsURL, function (data) {
        locationsJSON = data;
    });
    //loadMobility();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });

    if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
        setDemoHintsSliderValue();
    }
    else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        setDemoHintsSliderValue();
        //window.setTimeout(function loadHints() {
        createDemoHints("jobOptInStatus");
        // }, 100);
    }

    //Loads all the optin users.

});

$(document).on('pageshow', '#_optInStatus', function (event) {
    persistNavPanelState();
    //    if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
    //        $('#divBottom').css('display', 'block');
    //        //$('#tdShowHints').attr('style', 'width:95px;height:55px;');
    //        $('#btnSave').css('display', 'none');
    //        $('#btnContinue').css('display', 'none');
    //    }


    pageObj = new makeOptInStatus();
    $(document).ready(function () {
        window.setTimeout(function () {
            if (pageObj.uIViewConfig.map())
                pageObj.initMapCanvas();
        }, 2000);
        window.setTimeout(function () {
            pageObj.getLocations();
            $('#dvLocationsPrefs input[type="checkbox"]').attr('checked', true).checkboxradio().checkboxradio('refresh');
        }, 1000);
    });
    ko.applyBindings(pageObj);
    pageObj.getFilterdLocations();
    $('#dvAvailable').trigger('create');
    $('#dvOptedIn').trigger('create');
    $('#dvOptedOut').trigger('create');
    $('#dvPending').trigger('create');
    $('#dvApproved').trigger('create');

    $('#btnContinue').css('display', 'none');
    
    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        $('#optInOutStatus p a').text('Turn Hints Off');
        $('#pendingApproved p a').text('Turn Hints Off');
    }
    if (!dontShowHintsAgain && (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isOptInStatusHintsDisplayed == undefined || sessionStorage.isOptInStatusHintsDisplayed == null || sessionStorage.isOptInStatusHintsDisplayed == "false")) {
        sessionStorage.isOptInStatusHintsDisplayed = true;
        $('#statusSummary').popup('close');
        window.setTimeout(function loadHints() {
            $('#statusSummary').popup('open', { positionTo: '#tblCheckOutrGrid' });
        }, 10000);
    }
});

var mapping = { 'ignore': ["__ko_mapping__"] }

// ******************** Location Model Start **************************
// View model of a location
var locationModel1 = function (data, index, icon) {
    var self = this;
    ko.mapping.fromJS(data, {}, self);
    self.computedStoreName = ko.computed(function () {
        deferEvaluation: true
        var full_name = "";
        var store_type = '';
        var store_location = '';
        var store_id = '';
        // if (self.storeType != undefined && self.storeType != null && $.parseJSON(sessionStorage.locationConfig).hidden["storeType"] == undefined || ($.parseJSON(sessionStorage.locationConfig).hidden["storeType"] != undefined && $.parseJSON(sessionStorage.locationConfig).hidden["storeType"] == null))
        store_type = (($.trim(self.storeType()) != "") ? self.storeType() + ' - ' : "");
        //if (self.location != undefined && self.location != null && $.parseJSON(sessionStorage.locationConfig).hidden["location"] == undefined || ($.parseJSON(sessionStorage.locationConfig).hidden["location"] != undefined && $.parseJSON(sessionStorage.locationConfig).hidden["location"] == null))
        store_location = (($.trim(self.location()) != "") ? self.location() : "");
        //if (self.storeId != undefined && self.storeId != null && $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == undefined || ($.parseJSON(sessionStorage.locationConfig).hidden["storeId"] != undefined && $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == null)) {
        store_id = (self.storeId() != "") ? self.storeId() + ' - ' : "";
        //}
        if (self.firstName != undefined && self.firstName != null && self.lastName != undefined && self.lastName != null) {
            store_id = self.firstName() + ' ' + self.lastName() + ' - ';
        }

        full_name = store_id + store_type + store_location
        full_name = ((full_name.trim().endsWith('-')) ? (full_name.trim().substring(0, full_name.trim().length - 1).trim()) : full_name);
        full_name = full_name + ((self.marketManager != undefined && self.marketManager != null && self.marketManager != "" && self.marketManager() != undefined && self.marketManager() != null && self.marketManager() != "") ? ' (' + self.marketManager() + ')' : '');
        return full_name;
    });

    self.storeCompleteAddress = ko.computed(function () {
        var full_address = "";
        if (self.address != undefined && self.address != null) {
            full_address = self.address();
        }
        if (self.address2 != undefined && self.address2 != null && self.address2() != "") {
            full_address = (full_address != "") ? (full_address + ', ' + self.address2()) : self.address2();
        }
        if (self.city != undefined && self.city != null) {
            full_address = (full_address != "") ? ((!full_address.endsWith(',') ? full_address + ', ' : full_address + '') + self.city()) : self.city();
        }
        if (self.state != undefined && self.state != null) {
            full_address = (full_address != "") ? ((!full_address.endsWith(',') ? full_address + ', ' : full_address + '') + self.state()) : self.state();
        }
        if (self.zip != undefined && self.zip != null) {
            full_address = (full_address != "") ? (full_address + ' ' + self.zip()) : self.zip();
        }
        return full_address;
    });
    self.quantity = ko.observable(((data.quantity != undefined && data.quantity != null && data.quantity != '') ? data.quantity : ''));
    //self.isQuantityRequired = ko.observable(((jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != REGIS_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER) ? true : false));
    self.locationIndex = index + '-' + data.storeId;
    self.locationMarker = ko.observable('');
    self.locationMarker(icon);
}

var locationsList = function (data) {
    var self = this;
    self.name = ko.observable(data.name);
    self.locations = ko.observableArray(data.locations);
    self.scrollRequired = ko.observable(data.scrollRequired);
    self.height = ko.observable(data.height);
    self.showPaging = ko.observable(data.showPaging);
    self.enableNext = ko.observable(data.enableNext);
    self.enablePrevious = ko.observable(data.enablePrevious);
    self.currentPageNumber = ko.observable(data.currentPageNumber);
    self.totalPages = ko.observable(data.totalPages);
    self.locationsRange = ko.observable(data.locationsRange);
    self.totalLocations = ko.observable(data.totalLocations);
    self.groupByStatus = ko.observable(data.groupByStatus);
    self.type = ko.observable(data.type);
};

var statusInfo = function (status_key, status_count) {
    var self = this;
    self.statusKey = status_key;
    self.statusCount = status_count;
}

var optInUser = function (user_name, count) {
    var self = this;
    self.count = count;
    self.userName = user_name;
    self.computedUserNameWithCount = ko.computed(function () {
        return user_name + " (" + count + ")";
    });
};

var makeOptInStatus = function () {
    var self = this;
    pageObj = new locationFiltersAndPaging(self);
    self.infowindow = ko.observable();
    self.searchTextExisting = "";

    self.searchText = ko.observable('');
    self.gDataFiltered;

    self.gridColumns = ko.observableArray([]);
    self.gridRows = ko.observableArray([]);

    self.optInUsersList = ko.observableArray([]);
    self.statusList = ko.observableArray([]);

    //*****************  New Implementation Variables START **********************//
    self.allLocations = {};
    self.displayLocations = ko.observableArray([]);
    //********************************* END **************************************//

    self.uIViewConfig = {};
    self.uIViewConfig["grid"] = ko.observable(true);
    self.loc_lst = [];
    self.markers_array = [];
    var markers1 = [];
    new displayMapAndMarkers(self);

    self.makeGData = function () {
        if (jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
            $.getJSON(serviceJSONURL, function (data) {
                rows = [];
                gData = data;
                openLocations = gData.openLocations;
                pendingLocations = gData.pendingApproval;
                approvedLocations = gData.approvedLocations;
                optOutLocations = gData.optOutLocations;
                optInLocations = gData.optInLocations;

                var postage_storeid = [];
                var postage_quantity = [];
                var postage_store_name = [];
                $.each(approvedLocations, function (a, b) {
                    postage_storeid[a] = b.storeId;
                    postage_quantity[a] = (b.qty != undefined) ? b.qty.replace(/,/g, "") : 0;
                    postage_store_name[a] = b.storeName;
                });


                rows.push({
                    "inHomeDate": gData.approvalCheckoutSummary.rows[0].inHomeDate,
                    "openLocations": gData.approvalCheckoutSummary.rows[0].openLocations,
                    "optOutLocations": gData.approvalCheckoutSummary.rows[0].optOutLocations,
                    "optInLocations": gData.approvalCheckoutSummary.rows[0].optInLocations,
                    "pendingLocations": gData.approvalCheckoutSummary.rows[0].pendingLocations,
                    "approvedLocations": gData.approvalCheckoutSummary.rows[0].approvedLocations,
                    //"approvedQty": "",
                    "costPerPiece": gData.approvalCheckoutSummary.rows[0].costPerPiece,
                    "total": gData.approvalCheckoutSummary.rows[0].total
                });
                //if (sessionStorage.userRole == "admin")
                //   rows["billingInvoice"] = "<img id='imgBilling' src='../ApprovalCheckout/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick='fnBillingInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\",\"" + postage_store_name + "\")'\>"

                //$.each(checkoutSummary.rows, function (a, b) {
                //b["postageInvoice"] = "<img id='imgPostage" + a + "' src='../ApprovalCheckout/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)' onclick='fnPostageInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\")'\>";
                //if (sessionStorage.userRole == "admin")
                //b["billingInvoice"] = "<img id='imgBilling" + a + "' src='../ApprovalCheckout/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick='fnBillingInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\",\"" + postage_store_name + "\")'\>";
                //});
                var gridCols = ["In-Home Date", "Open Locations", "Opted Out Locations", "Opted In Locations", "Pending Locations", "Approved Locations", "Cost Per Piece", "Total"];
                //self.buildCheckoutSummaryGrid(rows);
                $("#tblCheckOutrGrid").css("display", "block");
                new checkOutSummaryVM(rows, gridCols);
                //ko.applyBindings(new checkOutSummaryVM(rows, gridCols));
                $("#tblCheckOutrGrid").table("refresh");
                // self.buildLocations(pendingLocations, approvedLocations, optOutLocations, optInLocations);

            });
        } else {
            getCORS(serviceURL, null, function (data) {
                rows = [];
                gData = data;
                pendingLocations = (gData.locationsDict.pending != undefined && gData.locationsDict.pending != null) ? gData.locationsDict.pending : [];
                approvedLocations = (gData.locationsDict.approved != undefined && gData.locationsDict.approved != null) ? gData.locationsDict.approved : [];
                optOutLocations = (gData.locationsDict["opted-out"] != undefined && gData.locationsDict["opted-out"] != null) ? gData.locationsDict["opted-out"] : [];
                optInLocations = (gData.locationsDict["opted-in"] != undefined && gData.locationsDict["opted-in"] != null) ? gData.locationsDict["opted-in"] : [];
                openLocations = [];
                var postage_storeid = [];
                var postage_quantity = [];
                var postage_store_name = [];
                $.each(approvedLocations, function (a, b) {
                    postage_storeid[a] = b.storeId;
                    postage_quantity[a] = (b.qty != undefined) ? b.qty.replace(/,/g, "") : 0;
                    postage_store_name[a] = b.storeName;
                });

                rows.push({
                    "inHomeDate": gData.inHomeDate,
                    "openLocations": 0,
                    "optOutLocations": gData.optedOutTotal,
                    "optInLocations": gData.optedInTotal,
                    "pendingLocations": gData.pendingTotal,
                    "approvedLocations": gData.approvedTotal,
                    //"approvedQty": "",
                    "costPerPiece": gData.costPerPiece,
                    "total": gData.total,
                    "postageInvoice": "<img id='imgPostage' src='../optInStatus/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)' onclick='fnPostageInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\")'\>"
                });
                if (sessionStorage.userRole == "admin")
                    rows["billingInvoice"] = "<img id='imgBilling' src='../optInStatus/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick='fnBillingInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\",\"" + postage_store_name + "\")'\>"

                //$.each(checkoutSummary.rows, function (a, b) {
                //b["postageInvoice"] = "<img id='imgPostage" + a + "' src='../optInStatus/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)' onclick='fnPostageInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\")'\>";
                //if (sessionStorage.userRole == "admin")
                //b["billingInvoice"] = "<img id='imgBilling" + a + "' src='../optInStatus/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick='fnBillingInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\",\"" + postage_store_name + "\")'\>";
                //});
                var gridCols = ["In-Home Date", "Open Locations", "Opted Out Locations", "Opted In Locations", "Pending Locations", "Approved Locations", "Cost Per Piece", "Total"];
                //self.buildCheckoutSummaryGrid(rows);
                $("#tblCheckOutrGrid").css("display", "block");
                new checkOutSummaryVM(rows, gridCols);
                $("#tblCheckOutrGrid").table("refresh");
                //self.buildLocations(pendingLocations, approvedLocations, optOutLocations, optInLocations);
            }, function (error_response) {
                showErrorResponseText(error_response);
                $("#tblCheckOutrGrid").css("display", "none");
            });
        }
    }

    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null)
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if (gOutputData != null) {
            getUploadedFilesCount();
        }
    }
    self.anonymous1PageId = ko.observable('0');
    self.anonymous2PageId = ko.observable('0');

    self.clearSearchFields = function () {
        $('#txtLocationName').val('');
        $('#txtStoreId').val('');
        $('#txtCity').val('');
        $('#txtAddress').val('');
        $('#ddlState').val('');
        $('#txtState').val('');
        $('#txtZipCode').val('');
        $('#fldStOptInUsers input[type="checkbox"]').checkboxradio().attr('checked', false).checkboxradio('refresh');
        $('#fldSetStatus input[type="checkbox"]').checkboxradio().attr('checked', false).checkboxradio('refresh');
    };

    self.makeGOutputData();
    self.loadOptInUsersAndStatus();
    self.setSearchFocus = function (ctrl) {
        var v = $("#search" + ctrl).val();
        $("#search" + ctrl).val('');
        $("#search" + ctrl).val(v);
        $('#search' + ctrl)[0].focus();
    }

    //Initiates the Search/Filter the locations.
    self.filteredLocations = function () {
        var msg = self.locationsSearch();
        if (msg != undefined && msg != null && msg != "") {
            $('#alertmsg').html(msg);
            $('#popupSearch').popup().popup('close');
            $("#okBut").unbind('click');
            $("#okBut").bind('click', function () {
                $('#popupDialog').popup().popup('close');
                window.setTimeout(function openPopSearch() {
                    $('#popupSearch').popup().popup('open');
                }, 100);
            });
            $('#popupDialog').popup().popup('open');
            return false;
        }
        else {
            $('#popupSearch').popup().popup('close');
            $('#dvSearchLocationsMap').css('display', 'block');
            $('#map-canvas').css('display', 'block');
        }
    };

    self.initMapCanvas = function () {
        var latlng = new google.maps.LatLng(43.1462421, -104.9929722);
        geocoder = new google.maps.Geocoder();
        geocoder.geocode(/*{ 'address': 'US' }*/{ 'latLng': latlng }, function (results, status) {
            if (status == "OK") {
                var mapOptions = {
                    //zoom: 12,
                    zoom: 7,
                    //center: latlng,
                    center: results[0].geometry.location,
                    //mapTypeId: google.maps.MapTypeId.TERRAIN
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                if ($.parseJSON(self.uIViewConfig.map()))
                    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                else
                    map = new google.maps.Map(document.getElementById('tempMapCanvas'), mapOptions);
                gMapBoundsUSA = new google.maps.LatLngBounds(new google.maps.LatLng(28.70, -127.50), new google.maps.LatLng(48.85, -55.90));
                gMapCenter = map.getCenter();
                $('#mapBlock').css('display', 'block')
                self.initializeMap();
            }
        });
    };

    //Fires when the map tiles loaded.
    self.tilesLoaded = function () {
        google.maps.event.clearListeners(map, 'tilesloaded');
    };

    //Initializes the map and loads the ordered locations from the session.
    self.initializeMap = function () {
        service = new google.maps.places.PlacesService(map);
        google.maps.event.addListener(map, 'tilesloaded', self.tilesLoaded);
        if (self.uIViewConfig.map())
            self.displayMarkers();
    };

    self.loadLocations = function (idx, locations_data) {
        var search_string = "";
        var index = 0;
        var latLngPts = [];
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === (($('#ddlDisplayResultsBy').val() == "status") ? locations_data.value.replace('-', '').initLower() : "anonymous" + (idx + 1));
        });
        var search_string = "";
        var temp_store = {};
        $.each(locations_data.locations, function (a, b) {
            var marker_info = self.getMarkerColorAndFolder(b.status);
            var icon = logoPath + 'mapMarkers/' + marker_info.folder + '/marker' + marker_info.folder.toUpperCase() + '_' + marker_info.color + (a + 1) + '.png';
            temp_store = new locationModel1(b, a, icon);
            self.loc_lst.push(temp_store);
            latLngPts.push({ 'lat': temp_store.latitude(), 'lng': temp_store.longitude() });
            search_string += (search_string != '') ? '|' + ((temp_store.storeCompleteAddress() != "") ? temp_store.storeCompleteAddress() : '') : (temp_store.storeCompleteAddress() != "") ? temp_store.storeCompleteAddress() : '';
            search_string += (temp_store.computedStoreName() != "") ? '^' + temp_store.computedStoreName() : '^';
            search_string += ((temp_store.phone != undefined && temp_store.phone != null && temp_store.phone() != "") ? '^' + temp_store.phone() : '^');
            search_string += '^' + temp_store.locationIndex; // +'^';
            search_string += '^' + temp_store.status();
            current_markers[0].latlngs.push({ 'lat': temp_store.latitude(), 'lng': temp_store.longitude() });
            current_markers[0].addresses.push(search_string);
            search_string = "";
            temp_store = {};
        });
        var temp_block = new locationsList({
            "name": locations_data.value,
            "locations": self.loc_lst,
            "scrollRequired": (locations_data.locations.length > 6),
            "height": ((locations_data.locations.length > 6) ? ((locations_data.totalCount > 50) ? "490px" : "540px") : "auto"),
            "showPaging": (locations_data.totalCount > 50),
            "enableNext": ((locations_data.totalCount - locations_data.startIndex) >= 50), // TO DO- based on pageId
            "enablePrevious": (locations_data.startIndex > 0),
            "currentPageNumber": ($('#ddlDisplayResultsBy').val() == "status") ? (parseInt(self[locations_data.value.initLower().replace('-', '') + 'PageId']()) + 1) : (parseInt(self["anonymous" + (idx + 1) + "PageId"]()) + 1),
            "totalPages": Math.ceil(locations_data.totalCount / 50),
            "locationsRange": ((locations_data.startIndex + 1) + ' of ' + (locations_data.finalIndex)),
            "totalLocations": locations_data.totalCount,
            "groupByStatus": ($('#ddlDisplayResultsBy').val() == "status"),
            "type": ($('#ddlDisplayResultsBy').val() == "status") ? locations_data.value : ("anonymous" + (idx + 1))
        });
        self.allLocations[locations_data.value.toLowerCase().replace('-', '')] = temp_block;
        self.loc_lst = [];
    };

    //Gets the page wise existing locations 
    self.getPagingData = function (data, event) {
        var ctrl = event.currentTarget;
        var location_type = $(ctrl).attr("data-btntype");
        var page_id = self[location_type.initLower() + "PageId"]();

        if (!$('#' + ctrl.id).hasClass('ui-disabled')) {
            if ($(ctrl).text().indexOf("Previous") > -1) {
                if (parseInt(page_id) > 0) {
                    self[location_type.initLower() + "PageId"](parseInt(page_id) - 1);
                }
            }
            else
                if ($(ctrl).text().indexOf("Next") > -1) {
                    self[location_type.initLower() + "PageId"](parseInt(page_id) + 1);
                }
            self.getFilterdLocations(self.searchTextExisting);
            self.getLocations();
        }
    };

    self.loadOptInStatusGrid = function () {
        rows = [];
        self.gridRows.removeAll();
        self.gridColumns.removeAll();
        var gridCols = ["Locations"];
        var gridCols_Temp = [];
        var rows_json = {};
        var total_count = 0;
        if ($('#ddlDisplayResultsBy').val() == "status") {
            var open_cnt = 0;
            var declined_cnt = 0;
            $.each(self.gDataFiltered.groupByList, function (key, val) {
                rows_json[val.value.replace('-', '').initLower() + 'Locations'] = val.totalCount;
                total_count += val.totalCount;
            });
        }
        else {
            $.each(self.gDataFiltered.groupByList, function (key, val) {
                $.each(val.locations, function (key1, val1) {
                    if (rows_json[val1.status.replace('-', '').initLower() + 'Locations'] == undefined)
                        rows_json[val1.status.replace('-', '').initLower() + 'Locations'] = 0;
                    rows_json[val1.status.replace('-', '').initLower() + 'Locations'] = (rows_json[val1.status.replace('-', '').initLower() + 'Locations']) + 1;
                });
                total_count += val.totalCount;
            });
        }
        var is_found = false;
        $.each(self.statusList(), function (key, val) {
            $.each(rows_json, function (key1, val1) {
                if (key1.toLowerCase() == val.statusKey.replace('-', '').toLowerCase() + 'locations') { is_found = true; return false; }
            });
            if (!is_found) {
                rows_json[val.statusKey.replace('-', '').initLower() + "Locations"] = 0;
            }
            self.gridColumns.push(val.statusKey.replace('-', ' '));
            is_found = false;
        });
        gridCols = gridCols.concat(self.gridColumns().sort())
        rows_json["locations"] = total_count;
        rows.push(rows_json);

        self.gridRows.push(rows_json);
        self.gridColumns(gridCols);
        window.setTimeout(function () {
            $("#tblCheckOutrGrid").css("display", "block");
            $("#tblCheckOutrGrid").css("display", "");
            $("#tblCheckOutrGrid").table("refresh");
        }, 1000);
    };
};

var checkOutSummaryVM = function (rows, gridCols) {
    gridRows = rows;
    gridColumns = gridCols;
}

//This function is to initialize the map and it is a callback parameter to google service url.
//THIS FUNCTION NEVER BE DELETED.
function loadPage() {
}

//Closes the initial popup
function closeInitialPopup(popup_ctrl) {
    $('#' + popup_ctrl).popup('close');
}

//Closes the popups - search
function closePopups() {
    $('#popupSearch').popup().popup('close');
};

//Capitalizes the first letter in the given string.
String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

//Makes the first letter into the lower case in the given string.
String.prototype.initLower = function () {
    return this.replace(/(?:^|\s)[A-Z]/g, function (m) {
        return m.toLowerCase();
    });
};

//Checks the given string ends with a specific (given character).
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

//Checks the given string starts with a specific (given character).
String.prototype.startsWith = function (str) {
    return this.indexOf(str) == 0;
};