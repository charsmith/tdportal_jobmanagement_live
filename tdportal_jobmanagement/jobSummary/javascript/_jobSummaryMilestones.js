﻿var jobSummaryMilestones = function (self) {
    //Sets the UI for MilestoneList popup.
    self.setMilestoneListPopup = function () {
        var popup_milestone_list = "";
        $.each(self.gData, function (key, val) {
            if (val.list.toLowerCase() == "milestonedict") {
                var milestone_obj = val.items;
                $.each(milestone_obj, function (a, b) {
                    //if (appPrivileges.customerNumber == "1") {
                    if (appPrivileges.roleName == "admin") {
                        var is_checked = "";
                        is_checked = (b.isVisible) ? " checked " : "";

                        var is_required = "";
                        is_required = (b.isRequired) ? " *" : "";
                        popup_milestone_list += '<input data-theme="c" type="checkbox" name="chk' + a + '" id="chk' + a + '" class="custom" value="' + a + '" ' + is_checked + ' onclick="pageObj.setSelectedMilestones(this,\'' + a + '\',\'' + b.description + '\',' + b.isRequired + ');" /><label for="chk' + a + '">' + b.description + is_required + '</label>';
                    }
                    //else if ((b.description.toLowerCase() == "art arrival" || b.description.toLowerCase() == "data" || b.description.toLowerCase() == "data arrival" || b.description.toLowerCase().indexOf("list") != -1 || b.description.toLowerCase() == "in-home" || b.description.toLowerCase() == "sale start" || b.description.toLowerCase() == "sale end"))
                    else if (b.isVisible)
                        popup_milestone_list += '<input data-theme="c" type="checkbox" name="chk' + a + '" id="chk' + a + '" class="custom" value="' + a + '" checked onclick="pageObj.setSelectedMilestones(this,\'' + a + '\',\'' + b.description + '\',' + b.isRequired + ');" /><label for="chk' + a + '">' + b.description + '</label>';
                    //else
                    //    popup_milestone_list += '<input data-theme="c" type="checkbox" name="chk' + a + '" id="chk' + a + '" class="custom" value="' + a + '" onclick="pageObj.setSelectedMilestones(this,\'' + a + '\',\'' + b.description + '\',' + b.isRequired + ');"/><label for="chk' + a + '">' + b.description + '</label>';
                });
            }
        });
        $(popup_milestone_list).appendTo('#fsMilestoneList');
        $("div[data-role=fieldcontain]").trigger('create');
    }

    //Sets the selected milestones into the local object.
    self.setSelectedMilestones = function (ctrl, selected_milestone, description, is_required) {
        //if (appPrivileges.customerNumber == "1") {
        if (appPrivileges.roleName == "admin") {
            if (is_required && !ctrl.checked) {
                $(ctrl).attr('checked', true).checkboxradio('refresh');
                return false;
            }
            else {
                var selected_options = $('#hdnSelectedMilestones').val();
                if (ctrl.checked)
                    selected_options += (selected_options != "") ? ',' + selected_milestone : selected_milestone;
                else {
                    var selected_options_list = selected_options.split(',');
                    var selected_options_index = "";
                    $.each(selected_options_list, function (a, b) {
                        if (b == selected_milestone) {
                            selected_options_index = a;
                            return false;
                        }
                    });
                    selected_options_list.splice(selected_options_index, 1);
                    selected_options = selected_options_list.join(',');
                }
                $('#hdnSelectedMilestones').val(selected_options);
                $('#hdnSelectedMilestone').val(selected_milestone);
            }
        }
        //else if ((description.toLowerCase() == "art arrival" || description.toLowerCase() == "data" || description.toLowerCase() == "data arrival" || description.toLowerCase().indexOf("list") != -1 || description.toLowerCase() == "in-home" || description.toLowerCase() == "sale start" || description.toLowerCase() == "sale end")) {
        else if (b.isVisible) {
            $(ctrl).attr('checked', true).checkboxradio('refresh');
            return false;
        }
    }

    //Fires an alert when if user submits without either Prior Alert or Late Email Alert.
    self.fnEmailMandatoryValidation = function () {
        if (($("#ddlPriorEmailAlerts").val() != "No Prior Alert" || $("#ddlLateEmailAlerts").val() != "No Late Alert") && $.trim($("#email").val()) == "") {
            $("#popupAlerts").popup("close");
            $('#alertmsg').html("Email should not be empty!");
            $('#okBut').attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');$('#popupAlerts').popup('open');");
            $("#popupDialog").popup("open");
        }
    }

    //Submits the selected email alerts information.
    self.submitEmailAlerts = function () {
        self.fnEmailMandatoryValidation();
        var selected_option = $('#hdnSelectedMilestone').val();
        var is_alert_required = false;
        if ($('#ddlPriorEmailAlerts').val().toLowerCase() != "no prior alert" || $('#ddlLateEmailAlerts').val().toLowerCase() != "no late alert") {
            $('#chk' + selected_option + 'EmailAlerts').attr('checked', true).checkboxradio('refresh');
            is_alert_required = true;
        }
        else {
            $('#chk' + selected_option + 'EmailAlerts').attr('checked', false).checkboxradio('refresh');
            is_alert_required = false;
        }
        var milestones_obj = {};
        $.each(self.gData, function (key, val) {
            if (val.list.toLowerCase() == "milestonedict") {
                $.each(val.items, function (a, b) {
                    if (a == selected_option) {
                        if (b.optionalAttributesDict == undefined && b.optionalAttributesDict == null) {
                            b["optionalAttributesDict"] = {};
                        }
                        if (b.optionalAttributesDict.emailAlerts == undefined && b.optionalAttributesDict.emailAlerts == null) {
                            b.optionalAttributesDict["emailAlerts"] = {};
                        }

                        if (b.optionalAttributesDict.emailAlerts.priorAlert == undefined)
                            b.optionalAttributesDict.emailAlerts["priorAlert"] = "";

                        if (b.optionalAttributesDict.emailAlerts.lateAlert == undefined)
                            b.optionalAttributesDict.emailAlerts["lateAlert"] = "";

                        if (b.optionalAttributesDict.emailAlerts.sendTo == undefined)
                            b.optionalAttributesDict.emailAlerts["sendTo"] = "";

                        b.optionalAttributesDict.emailAlerts.priorAlert = $('#ddlPriorEmailAlerts').val();
                        b.optionalAttributesDict.emailAlerts.lateAlert = $('#ddlLateEmailAlerts').val();
                        b.optionalAttributesDict.emailAlerts.sendTo = $('#email').val();
                        return false;
                    }
                });
                milestones_obj = val.items;
                self.milestoneAfterUpdate.push(self.gData[val.list]);
            }
        });
        $('#popupAlerts').popup('close');
        var milestone_list = self.buildMilestonesList(milestones_obj);
        var temp_milestones = "";
        $.each(milestone_list, function (a, b) {
            temp_milestones += b;
        });
        $('#milestones').find('li[id= liAddRemoveMilestones]').nextAll().remove();
        $(temp_milestones).insertAfter($('#milestones').find('li[id= liAddRemoveMilestones]'));

        $("li fieldset[data-role=controlgroup]").trigger('create');
        $.each($('milestones input[type=checkbox]'), function (a, b) {
            $(this).checkboxradio('refresh')
        });
        $('#milestones').listview('refresh');
        $('#popupMilestones').popup('close');
        $('#hdnSelectedMilestones').val('');
        $('#hdnSelectedMilestone').val('');
    }

    //Displays the email alerts popup with required options selected.
    self.populateEmailAlers = function (selected_option, event, prior_email, late_email, send_to) {
        event.preventDefault();
        event.stopPropagation();
        $('#hdnSelectedMilestone').val(selected_option);
        $('#ddlPriorEmailAlerts').val((prior_email != "") ? prior_email : "No Prior Alert").selectmenu('refresh');
        $('#ddlLateEmailAlerts').val((late_email != "") ? late_email : "No Late Alert").selectmenu('refresh');
        $('#email').val(send_to);
        $('#popupAlerts').popup('close');
        $('#popupAlerts').popup('open');
    }

    //Displays milestone list popup
    self.openMilestoneListPopUp = function () {
        $('#popupMilestones').popup('open');
    }

    //Closes milestone list popup
    self.closeMilestoneListPopup = function () {
        $('#popupMilestones').popup('close');
        var selected_milestones = $('#hdnSelectedMilestones').val().split(',');
        $.each(selected_milestones, function (a, b) {
            $('#chk' + b).attr('checked', false).checkboxradio('refresh');
        });
        $.each($('#hdnFilteredMilestones').val().split(','), function (a, b) {
            $('#chk' + b).attr('checked', true).checkboxradio('refresh');
        });
        //setMilestoneListPopup();
    }

    //Refreshing milestone list
    self.refreshMilestoneList = function () {
        var milestone_obj = {};
        $.each(self.gData, function (key, val) {
            if (val.list.toLowerCase() == "milestonedict") {
                milestone_obj = val.items;
                $.each(val.items, function (milestone_key, milestone_val) {
                    milestone_val.isVisible = ($('#chk' + milestone_key).attr('checked') == 'checked') ? true : false;
                    if (milestone_key == $('#hdnSelectedMilestone').val()) {
                        if (milestone_val.optionalAttributesDict != undefined && milestone_val.optionalAttributesDict != null) {
                            if (milestone_val.optionalAttributesDict.emailAlerts != undefined && milestone_val.optionalAttributesDict.emailAlerts != null) {
                                if ((milestone_val.optionalAttributesDict.emailAlerts.priorAlert != undefined && milestone_val.optionalAttributesDict.emailAlerts.priorAlert != null) ||
                            (milestone_val.optionalAttributesDict.emailAlerts.lateAlert != undefined && milestone_val.optionalAttributesDict.emailAlerts.lateAlert != null)) {
                                    if ((milestone_val.optionalAttributesDict.emailAlerts.priorAlert.toLowerCase() != "" && milestone_val.optionalAttributesDict.emailAlerts.priorAlert.toLowerCase() != "no prior alert") ||
                                        (milestone_val.optionalAttributesDict.emailAlerts.lateAlert.toLowerCase() != "" && milestone_val.optionalAttributesDict.emailAlerts.lateAlert.toLowerCase() != "no late alert"))
                                        email_alerts_checked = 'checked';
                                    milestone_val.optionalAttributesDict.emailAlerts.priorAlert = "";
                                    milestone_val.optionalAttributesDict.emailAlerts.lateAlert = "";
                                    milestone_val.optionalAttributesDict.emailAlerts.sendTo = "";
                                }
                            }
                        }
                        //return false;
                    }
                });
                //return false;
            }
        });
        var milestone_list = self.buildMilestonesList(milestone_obj);
        var temp_milestones = "";
        $.each(milestone_list, function (a, b) {
            temp_milestones += b;
        });
        $('#milestones').find('li[id= liAddRemoveMilestones]').nextAll().remove();
        $(temp_milestones).insertAfter($('#milestones').find('li[id= liAddRemoveMilestones]'));

        $("li fieldset[data-role=controlgroup]").trigger('create');
        $.each($('milestones input[type=checkbox]'), function (a, b) {
            $(this).checkboxradio('refresh')
        });
        $('#milestones').listview('refresh');
        $('#popupMilestones').popup('close');
        $('#hdnSelectedMilestones').val('');
    }

    //Generates Milestones list.
    self.buildMilestonesList = function (mile_stone_obj) {
        var counter = 0;
        var temp_mile_stones_data = [];
        var filtered_milestones = "";
        var email_alerts_checked = "";
        var prior_email_info = "";
        var late_email_info = "";
        var send_to_email_info = "";
        $.each(mile_stone_obj, function (key2, val2) {
            key2 = key2.toLowerCase();
            email_alerts_checked = "";
            prior_email_info = "";
            late_email_info = "";
            send_to_email_info = "";
            var asterisk = (val2.isRequired == 1) ? "* " : "";

            if (val2.isVisible != undefined && val2.isVisible) {
                if ((appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == REGIS_CUSTOMER_NUMBER || appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER) && val2.description != undefined) //"Data"/ "list arrival" to be displayed as "Data Arrival" - Charlie mail dated 11/5/2013
                    if (val2.description.toLowerCase() == "data" || val2.description.toLowerCase() == "list arrival")
                        val2.description = "Data Arrival";

                if ($("#hdnMilestoneFilter").val() == "rush") {
                    temp_mile_stones_data.push('<li  data-icon="edit" class="ui-bar-c"  id="edit" style="padding-top:1px;padding-bottom:1px;"><h3>' + asterisk + val2.description + '</h3>');
                }
                else if ($("#hdnMilestoneFilter").val() == "express") {
                    temp_mile_stones_data.push('<li data-icon="edit" class="ui-bar-c" id="edit" style="padding-top:1px;padding-bottom:1px;"><h3>' + val2.description + '</h3>');
                }
                else {
                    if (appPrivileges.customerNumber != "1" && appPrivileges.customerNumber != REDPLUM_CUSTOMER_NUMBER)
                        temp_mile_stones_data.push('<li data-icon="edit" id="edit" data-theme="c"><a onclick="pageObj.populateForm(\'' + key2 + '\');" href="#edit" data-rel="popup" data-position-to="window"  data-inline="true"> <h3>' + asterisk + ((val2.description != undefined) ? val2.description : val2) + '</h3>');
                    else {
                        if (val2.optionalAttributesDict != undefined && val2.optionalAttributesDict != null) {
                            if (val2.optionalAttributesDict.emailAlerts != undefined && val2.optionalAttributesDict.emailAlerts != null) {
                                if ((val2.optionalAttributesDict.emailAlerts.priorAlert != undefined && val2.optionalAttributesDict.emailAlerts.priorAlert != null) ||
                            (val2.optionalAttributesDict.emailAlerts.lateAlert != undefined && val2.optionalAttributesDict.emailAlerts.lateAlert != null)) {
                                    if ((val2.optionalAttributesDict.emailAlerts.priorAlert.toLowerCase() != "" && val2.optionalAttributesDict.emailAlerts.priorAlert.toLowerCase() != "no prior alert") ||
                                        (val2.optionalAttributesDict.emailAlerts.lateAlert.toLowerCase() != "" && val2.optionalAttributesDict.emailAlerts.lateAlert.toLowerCase() != "no late alert"))
                                        email_alerts_checked = 'checked';
                                    prior_email_info = val2.optionalAttributesDict.emailAlerts.priorAlert;
                                    late_email_info = val2.optionalAttributesDict.emailAlerts.lateAlert;
                                    send_to_email_info = val2.optionalAttributesDict.emailAlerts.sendTo;
                                }
                            }
                        }
                        //temp_mile_stones_data.push('<li data-icon="custom" id="edit"><a onclick="populateEmailAlers(\'' + key2 + '\',event,\'' + prior_email_info + '\',\'' + late_email_info + '\',\'' + send_to_email_info + '\');" href="#popupAlerts" data-rel="popup" data-position-to="window"  data-inline="true"> <h3>' + ((val2.description != undefined) ? val2.description : val2) + '</h3>');
                        temp_mile_stones_data.push('<li data-icon="edit" id="edit" data-theme="c"><a onclick=nonEditableMilestoneMessage("' + key2 + '"); href="#" data-rel="popup" data-position-to="window"  data-inline="true"> <h3>' + asterisk + ((val2.description != undefined) ? val2.description : val2) + '</h3>');
                    }
                }

                var one_day = 1000 * 60 * 60 * 24;
                var val2_scheduled_date = "";
                var val2_completed_date = "";
                var dt_diff = "";
                var due_date = "";
                var dt_sche_diff = "";
                var curr_date = new Date();
                if (val2.scheduledDate == "1/1/1900 12:00:00 AM")
                    val2.scheduledDate = "";
                if (val2.completedDate == "1/1/1900 12:00:00 AM")
                    val2.completedDate = "";

                if (val2.scheduledDate != "" && val2.scheduledDate != undefined) {
                    due_date = new Date(val2.scheduledDate);
                    val2_scheduled_date = (val2.scheduledDate.indexOf(' ') == -1) ? val2.scheduledDate : val2.scheduledDate.substring(0, val2.scheduledDate.indexOf(' '));
                    dt_diff = Math.floor((due_date.getTime() - curr_date.getTime()) / one_day);
                }

                if (val2.completedDate != "" && val2.completedDate != undefined) {
                    val2_completed_date = (val2.completedDate.indexOf(' ') == -1) ? val2.completedDate : val2.completedDate.substring(0, val2.completedDate.indexOf(' '));
                    var due_comp_date = new Date(val2.completedDate);
                    if (due_date != "")
                        dt_sche_diff = Math.floor((due_comp_date.getTime() - due_date.getTime()) / one_day);
                }

                if ($("#hdnMilestoneFilter").val() == "rush") {
                    if (key2.toLowerCase() == "mail")
                        temp_mile_stones_data.push('<p><input type="text" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="txt' + key2 + '" id="txt' + key2 + '" placeholder="MM/DD/YY" data-mini="true" data-theme="a" value="' + val2_scheduled_date + '" onfocus="nonEditableMilestoneMessage(\'' + key2 + '\');" onmousedown="nonEditableMilestoneMessage(\'' + key2 + '\');"/>');
                    else
                        temp_mile_stones_data.push('<p><input type="text" onblur="pageObj.dateValidationAndPersist(this,2, \'' + key2 + '\');" onkeyup="pageObj.fnSaveMilestones(\'' + key2 + '\');" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="txt' + key2 + '" id="txt' + key2 + '" placeholder="MM/DD/YY" data-mini="true" data-theme="a" value="' + val2_scheduled_date + '" onmousedown="pageObj.fnShowCal(this, \'' + key2 + '\');"/><br \>');
                    //if (counter == 0)
                    if (key2.toLowerCase().indexOf("inhome") > -1)
                        temp_mile_stones_data.push('All orders require a minimum of a 3 day in-home designation window.');

                    temp_mile_stones_data.push('</p></li>');
                }
                else if ($("#hdnMilestoneFilter").val() == "express") {
                    temp_mile_stones_data.push('<p><input type="text" onblur="pageObj.dateValidationAndPersist(this,2, \'' + key2 + '\');" onkeyup="pageObj.fnSaveMilestones(\'' + key2 + '\');" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="txt' + key2 + '" id="txt' + key2 + '" placeholder="MM/DD/YY" data-mini="true" data-theme="a" value="' + val2_completed_date + '" onmousedown="pageObj.fnShowCal(this, \'' + key2 + '\');"/><br\>');
                    //if (counter == 0)
                    if (key2.toLowerCase().indexOf("inhome") > -1)
                        temp_mile_stones_data.push('All orders require a minimum of a 3 day in-home designation window.');
                    temp_mile_stones_data.push('</p></li>');
                }
                else {
                    var milestone_item = "";
                    if (val2_completed_date == "") {
                        if (val2_scheduled_date == "") {
                            milestone_item = '<p><em>Click to enter Scheduled Date<br>Click to enter Completed Date</em></p>';
                            //temp_mile_stones_data.push('<p><em>Click to enter Scheduled Date<br>Click to enter Completed Date</em></p></a></li>');
                        }
                        else if ((dt_diff) >= -1) {
                            milestone_item = '<p><em>Scheduled <span id="spnScheduledDate' + key2 + '">' + val2_scheduled_date + '</span></em><br>Click to enter Completed Date</p>';
                            //temp_mile_stones_data.push('<p><em>Scheduled <span id="spnScheduledDate' + key2 + '">' + val2_scheduled_date + '</span></em><br>Click to enter Completed Date</p></a></li>');
                        }
                        else {
                            milestone_item = '<p><em class="incompleteMilestone">Scheduled <span id="spnScheduledDate' + key2 + '">' + val2_scheduled_date + '</span></em><br>Click to enter Completed Date</p>';
                            //temp_mile_stones_data.push('<p><em class="incompleteMilestone">Scheduled <span id="spnScheduledDate' + key2 + '">' + val2_scheduled_date + '</span></em><br>Click to enter Completed Date</p></a></li>');
                        }
                    }
                    else {
                        if (val2_scheduled_date == "") {
                            milestone_item = '<p><em>Click to enter Scheduled Date</br>Completed <span id="spnCompletedDate' + key2 + '">' + val2_completed_date + '</span></em></p>';
                            //temp_mile_stones_data.push('<p><em>Click to enter Scheduled Date</br>Completed <span id="spnCompletedDate' + key2 + '">' + val2_completed_date + '</span></em></p></a></li>');
                        }
                        else if (dt_sche_diff <= 0) {
                            milestone_item = '<p>Scheduled <span id="spnScheduledDate' + key2 + '">' + val2_scheduled_date + '</span><em class="completedMilestone"></br>Completed <span id="spnCompletedDate' + key2 + '">' + val2_completed_date + '</span></em></p>';
                            //temp_mile_stones_data.push('<p>Scheduled <span id="spnScheduledDate' + key2 + '">' + val2_scheduled_date + '</span><em class="completedMilestone"></br>Completed <span id="spnCompletedDate' + key2 + '">' + val2_completed_date + '</span></em></p></a></li>');
                        }
                        else {
                            milestone_item = '<p>Scheduled <span id="spnScheduledDate' + key2 + '">' + val2_scheduled_date + '</span><em class="incompleteMilestone"></br>Completed <span id="spnCompletedDate' + key2 + '">' + val2_completed_date + '</span></em></p>';
                            //temp_mile_stones_data.push('<p>Scheduled <span id="spnScheduledDate' + key2 + '">' + val2_scheduled_date + '</span><em class="incompleteMilestone"></br>Completed <span id="spnCompletedDate' + key2 + '">' + val2_completed_date + '</span></em></p></a></li>');
                        }
                    }
                    var inhome_msg = "";
                    if (key2.toLowerCase().indexOf("inhome") > -1)
                    //inhome_msg = '<p' + ((appPrivileges.customerNumber == "1") ? ' style="padding-left:15px"' : "") + '>All orders require a minimum of a 3 day in-home </br>designation window.</p>';
                        inhome_msg = '<p>All orders require a minimum of a 3 day in-home designation window.</p>';

                    milestone_item += inhome_msg + "</a>";
                    milestone_item += '</li>';
                    temp_mile_stones_data.push(milestone_item);
                }
                counter++;
                filtered_milestones += (filtered_milestones != "") ? ',' + key2 : key2;
            }
        });
        $('#hdnFilteredMilestones').val(filtered_milestones);
        return temp_mile_stones_data;
    }

    //Validates the user entered date and format and persists the value.
    self.dateValidationAndPersist = function (ctrl, number, val) {
        fnDateFormatMMDDYYYY(ctrl, number);
        self.fnSaveMilestones(val);
    }

    //Displays the datepicker window when the user clicks on it and persists when selected any date.
    self.fnShowCal = function (obj, name) {
        jQuery(obj).datepicker(
        {
            onSelect: function () {
                self.fnSaveMilestones(name);
            }
        });
    }

    //Closes the milestone popup.
    self.closeEditMilestonePopUp = function (id) {
        $(id).keypress(function (e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterKey");
                $('#popupLogin').popup('open');
                //$('#popupLogin').dialog('open');
                $('#popupLogin').find('#btnSubmitMilestones').trigger('click');
                $('#popupLogin').popup('close');
                //$('#popupLogin').dialog('close');

                return false;
            }
        });
    }

    //Saves the milestones information into local object.
    self.fnSaveMilestones = function (ctrl_name) {
        $.each(self.gData, function (key, val) {
            switch (val.list) {
                case "milestoneDict":
                    $.each(val.items, function (keyMileStones, valMileStones) {
                        keyMileStones = keyMileStones.toLowerCase();
                        if (ctrl_name == keyMileStones) {
                            if ($("#hdnMilestoneFilter").val() == "rush") {
                                valMileStones.scheduledDate = $("#txt" + keyMileStones).val();
                                valMileStones.scheduledBy = sessionStorage.username;
                                valMileStones.hasChanged = 1;

                            }
                            else if ($("#hdnMilestoneFilter").val() == "express") {
                                valMileStones.completedDate = $("#txt" + keyMileStones).val();
                                valMileStones.completedBy = sessionStorage.username;
                                valMileStones.hasChanged = 1;
                            }
                        }
                    });
                    self.milestoneAfterUpdate = [];
                    self.milestoneAfterUpdate.push(self.gData[key]);
                    break;
            }

        });
    }

    //Opens the edit milestones popup for view all milestones option.
   self.populateForm = function (pk) {
        $("#hdnSelectedMilestone").val(pk);
        var single_data;
        $.each(self.gData, function (key, val) {
            if (key != undefined) {
                switch (val.list) {
                    case "milestoneDict":
                        $.each(val.items, function (key1, val1) {
                            key1 = key1.toLowerCase();
                            if (key1 == pk) {
                                if (val1.completedDate == "1/1/1900") {
                                    val1.completedDate = "";
                                }
                                $("#scheduledDate").val((val1.scheduledDate.indexOf(' ') == -1) ? val1.scheduledDate : val1.scheduledDate.substring(0, val1.scheduledDate.indexOf(' ')));
                                $("#completedDate").val((val1.completedDate.indexOf(' ') == -1) ? val1.completedDate : val1.completedDate.substring(0, val1.completedDate.indexOf(' ')));
                                return false;
                            }
                        });
                        break;
                }
            }
        });

        jQuery("#scheduledDate").datepicker({
            minDate: new Date(2009, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true
        });

        jQuery("#completedDate").datepicker({
            minDate: new Date(2009, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true
        });
       //$('#popupLogin').popup('open');
        $('#scheduledDate').datepicker('disable');
        $('#completedDate').datepicker('disable');
        $(document).on("popupafteropen", "#popupLogin", function (event, ui) {
            $('#scheduledDate').datepicker('enable');
            $('#completedDate').datepicker('enable');
        });
        $('#popupLogin').popup('open');
        //$("#popupLogin").dialog({
        //    title: null,
        //    resizable: false,
        //    modal: true,
        //    open: function () {
        //        $('#scheduledDate').datepicker('enable');
        //        $('#completedDate').datepicker('enable');
        //    }
        //});
    }

    //Updating value
    self.updateVal = function () {
        if (!fnDateFormatMMDDYYYY('scheduledDate', 1)) //date format validation
            return false;
        if (!fnDateFormatMMDDYYYY('completedDate', 1)) //date format validation
            return false;

        var gdata_milestone = {};
        gdata_milestone = jQuery.grep(self.gData, function (obj) {
            return obj.list.toLowerCase() === "milestonedict";
        });

        var sched_date = $("#scheduledDate").val();
        var cmpl_date = $("#completedDate").val();

        if (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) { //TD
            var msg = "";
            $.each(gdata_milestone[0].items, function (key, value) {
                key = key.toLowerCase();
                if (value.isRequired == 1 && $("#hdnSelectedMilestone").val() == key && $("#scheduledDate").val() == "") {
                    msg = value.description + " date is mandatory!";
                }
            });

            if (msg != "") {
                $("#popupLogin").popup("close");
                //$('#popupLogin').dialog('close');
                $('#alertmsg').html(msg);
                $("#popupDialog").popup('open');
                //$('#popupLogin').dialog('open');
                return false;
            }
        }

        var is_load = false;
        $.each(self.gData, function (key, val) {
            if (key != undefined) {
                switch (val.list) {
                    case "milestoneDict":
                        $.each(val.items, function (key1, val1) {
                            key1 = key1.toLowerCase();
                            if (key1 == $("#hdnSelectedMilestone").val()) {
                                val1.scheduledDate = sched_date;
                                val1.scheduledBy = sessionStorage.username;
                                val1.completedDate = cmpl_date;
                                val1.completedBy = sessionStorage.username;
                                val1.hasChanged = 1;
                            }
                        });
                        self.milestoneAfterUpdate = [];
                        self.milestoneAfterUpdate.push(self.gData[key]);
                        break;
                }
            }
            if (is_load)
                return false;
        });

        var temp_mile_stones_data = [];
        var selector_id = "", list_div = "", caption = "";
        $.each(self.gData, function (key, val) {
            if (key != undefined) {
                switch (val.list) {
                    case "milestoneDict":
                        temp_mile_stones_data = self.buildMilestonesList(val.items);
                        selector_id = 'chicagoSelect';
                        list_div = 'milestones';
                        caption = 'MILESTONES';
                        break;
                }
            }
        });
        if (temp_mile_stones_data.length > 0)
            self.makeList(temp_mile_stones_data, selector_id, list_div, caption);
        self.buildOutputLists();
        //$('#popupLogin').dialog('close');
        $("#popupLogin").popup("close");
    }


    //Save milestone data to job ticket
    self.saveMilestoneDataToJobTicket = function () {
        var gdata_milestone = {};
        gdata_milestone = jQuery.grep(self.gData, function (obj) {
            return obj.list.toLowerCase() === "milestonedict";
        });

        var msg = self.popupValidation(gdata_milestone[0], 'scheduledDate');
        if (msg != "") {
            msg = "<b>Scheduled Milestones</b> </br>" + msg;
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $("#popupDialog").popup("open");
            return false;
        }
        else {
            var g_data = {};
            g_data["milestoneAction"] = jQuery.grep(self.gData, function (obj) {
                return obj.list.toLowerCase() === "milestonedict";
            });

            var g_milestone_dict = {};
            g_milestone_dict["milestoneDict"] = g_data["milestoneAction"][0].items;

            var post_milestone_url = serviceURLDomain + 'api/Milestone_save/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
            postCORS(post_milestone_url, JSON.stringify(g_milestone_dict), function (response) {
                var msg = '';
                if (response != "") {
                    self.milestoneBeforeUpdate = self.milestoneAfterUpdate = g_data.milestoneAction[0];
                    msg = 'Milestones data was saved.';
                }
                else
                    msg = "Milestones data was not saved.";
                $('#alertmsg').text(msg);
                $('#popupDialog').popup('open');
            }, function (response_error) {
                //var msg = 'Error in saving.';
                //var msg = response_error.responseText;
                //$('#alertmsg').text(msg);
                //$('#popupDialog').popup('open');
                showErrorResponseText(error_response, false);
            });
        }
    }

    //Validating milestone
    self.fnMilestonesValidation = function () {
        var msg = "";

        //eval(getURLDecode(pagePrefs.scriptBlockDict.fnMilestonesValidation)); // getting code from page preference web service
        var in_home_date = ($("#txtinhome").val() == undefined || $("#txtinhome").val() == "") ? "" : new Date($("#txtinhome").val());
        var list_arrival_date = ($("#txtlist").val() == undefined || $("#txtlist").val() == "") ? "" : new Date($("#txtlist").val());
        var art_arrival_date = ($("#txtartarrival").val() == undefined || $("#txtartarrival").val() == "") ? "" : new Date($("#txtartarrival").val());
        var sale_start_date = ($("#txtsalestart").val() == undefined || $("#txtsalestart").val() == "") ? "" : new Date($("#txtsalestart").val());
        var sale_end_date = ($("#txtsaleend").val() == undefined || $("#txtsaleend").val() == "") ? "" : new Date($("#txtsaleend").val());

        if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) { //cw
            if ($("#hdnMilestoneFilter").val() != 'standard') {
                var gdata_milestone = {};
                gdata_milestone = jQuery.grep(self.gData, function (obj) {
                    return obj.list.toLowerCase() === "milestonedict";
                });

                $.each(gdata_milestone[0].items, function (key, value) {
                    if (value.isRequired == 1 && $("#txt" + key).val() == "") {
                        if (msg != "")
                            msg += "</br>";
                        msg += value.description + " date is mandatory!";
                    }
                });

                var current_date = new Date();
                var tmp_in_home_date = ($("#txtinhome").val() == undefined) ? "" : new Date($("#txtinhome").val());
                tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4));

                switch (sessionStorage.userRole.toLowerCase()) {
                    case "admin":
                        if (list_arrival_date != "" && (!(list_arrival_date < tmp_in_home_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Data/List arrival date should be at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }

                        if (art_arrival_date != "" && (!(art_arrival_date < tmp_in_home_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Art arrival date should be at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }
                        break;
                    case "power":
                    case "user":
                        var tmp_current_date = new Date();
                        var tmp_in_home_date = ($("#txtinhome").val() == undefined) ? "" : new Date($("#txtinhome").val());

                        var current_date = new Date();
                        var curr_date = (current_date != "") ? new Date(current_date).setHours(0, 0, 0, 0) : "";

                        tmp_current_date.setDate(tmp_current_date.getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10));
                        tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10));

                        if (list_arrival_date != "" && (list_arrival_date <= curr_date || list_arrival_date > tmp_in_home_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Data/List Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days prior to the in-home date.";
                        }

                        if (art_arrival_date != "" && (art_arrival_date <= curr_date || art_arrival_date > tmp_in_home_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Art Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days prior to the in-home date.";
                        }
                        break;
                }

                if (sale_start_date != "" && sale_end_date != "" && sale_start_date > sale_end_date) {
                    if (msg != "")
                        msg += "</br></br>";
                    msg += "Sale end date should be greater than or equal to Sale start date.";
                }
            }
        }
        else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) { //cgs            
            var sign_up_start = new Date($("#txtsignUpStart").val());
            var sign_up_end = new Date($("#txtsignUpEnd").val());
            var approvals_start = new Date($("#txtapprovalsStart").val());
            var approvals_end = new Date($("#txtapprovalsEnd").val());
            var in_home_date = new Date($("#txtinHome").val());
            var current_date = new Date();

            var tmp_in_home_date = new Date($("#inHomeDate").val());
            tmp_in_home_date.setDate(tmp_in_home_date.getDate() - 4);

            switch (sessionStorage.userRole.toLowerCase()) {
                case "power":
                case "user":
                    var sign_start_date_diff = dateDifference(sign_up_start, in_home_date);
                    if (sign_start_date_diff < 42)
                        msg += "The sign up start date can be no less than 6 weeks prior to the In-Home.</br></br>";

                    var approvals_start_date_diff = dateDifference(approvals_start, in_home_date);
                    if (approvals_start_date_diff < 28)
                        msg += "The approval start date can be no less than 4 weeks prior to the In-Home.</br></br>";

                    var sign_approvals_start_date_diff = dateDifference(sign_up_start, approvals_start);
                    if (sign_approvals_start_date_diff < 14)
                        msg += "The sign up start date should be at least 2 weeks from the approvals start date.</br></br>";

                    var sign_end_date_diff = dateDifference(sign_up_start, sign_up_end);
                    if (sign_end_date_diff < 7)
                        msg += "Sign up end date should be at least be 1 week from Sign up start date.</br></br>";

                    var approvals_end_date_diff = dateDifference(approvals_start, approvals_end);
                    if (approvals_end_date_diff < 7)
                        msg += "Approvals end date should be at least be 1 week from Approvals start date.";
            }
        }
        else if (jobCustomerNumber == "1" || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER) { //TD
            if ($("#hdnMilestoneFilter").val() != 'standard') {
                var gdata_milestone = {};
                gdata_milestone = jQuery.grep(self.gData, function (obj) {
                    return obj.list.toLowerCase() === "milestonedict";
                });

                $.each(gdata_milestone[0].items, function (key, value) {
                    if (value.isRequired == 1 && $("#txt" + key).val() == "") {
                        if (msg != "")
                            msg += "</br>";
                        msg += value.description + " date is mandatory!";
                    }
                });

                var current_date = new Date();
                var tmp_in_home_date = ($("#txtinhome").val() == undefined) ? "" : new Date($("#txtinhome").val());
                tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4));

                switch (sessionStorage.userRole.toLowerCase()) {
                    case "admin":
                        if (list_arrival_date != "" && (!(list_arrival_date < tmp_in_home_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Data/List arrival date should be at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }

                        if (art_arrival_date != "" && (!(art_arrival_date < tmp_in_home_date))) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "Art arrival date should be at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                        }
                        break;
                    case "power":
                    case "user":
                        var tmp_current_date = new Date();
                        var tmp_in_home_date = ($("#txtinhome").val() == undefined) ? "" : new Date($("#txtinhome").val());

                        var current_date = new Date();
                        var curr_date = (current_date != "") ? new Date(current_date).setHours(0, 0, 0, 0) : "";

                        tmp_current_date.setDate(tmp_current_date.getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7)); // 7days for TD normal user
                        tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7)); // 7days for TD normal user

                        if (list_arrival_date != "" && (list_arrival_date <= curr_date || list_arrival_date > tmp_in_home_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Data/List Arrival date should be set between the current date and 7 days prior to the in-home date.";
                        }

                        if (art_arrival_date != "" && (art_arrival_date <= curr_date || art_arrival_date > tmp_in_home_date)) {
                            if (msg != "")
                                msg += "</br></br>";
                            msg += "The Art Arrival date should be set between the current date and 7 days prior to the in-home date.";
                        }
                        break;
                }

                if (sale_start_date != "" && sale_end_date != "" && sale_start_date > sale_end_date) {
                    if (msg != "")
                        msg += "</br></br>";
                    msg += "Sale end date should be greater than or equal to Sale start date.";
                }
            }
        }

        if (msg != undefined && msg != "" && msg != null) {
            $('#alertmsg').html(msg);
            $("#popupDialog").popup('open');
            return false;
        }
        else {
            return true;
        }
    }

    //Display validation popup
    self.popupValidation = function (val, type) {
        var msg = "";
        //eval(getURLDecode(pagePrefs.scriptBlockDict.popupValidation));
        var in_home_date = "";
        var list_arrival_date = "";
        var art_arrival_date = "";
        var sale_start_date = "";
        var sale_end_date = "";
        $.each(val.items, function (key1, val1) {
            if (key1.toLowerCase().indexOf('inhome') > -1) {
                in_home_date = (val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "";
            }
            if (key1.toLowerCase().indexOf('list') > -1) {
                list_arrival_date = (val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "";
            }
            if (key1.toLowerCase().indexOf('artarrival') > -1) {
                art_arrival_date = (val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "";
            }
            if (key1.toLowerCase().indexOf('salestart') > -1) {
                sale_start_date = (val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "";
            }
            if (key1.toLowerCase().indexOf('saleend') > -1) {
                sale_end_date = (val1 != undefined && val1[type] != "") ? new Date(val1[type]) : "";
            }
            //list_arrival_date = (val.items.list != undefined && val.items.list[type] != "") ? new Date(val.items.list[type]) : "";
            //art_arrival_date = (val.items.artArrival != undefined && val.items.artArrival[type] != "") ? new Date(val.items.artArrival[type]) : "";
            //sale_start_date = (val.items.salestart != undefined && val.items.salestart[type] != "") ? new Date(val.items.salestart[type]) : "";
            //sale_end_date = (val.items.saleend != undefined && val.items.saleend[type] != "") ? new Date(val.items.saleend[type]) : "";
        });

        var current_date = new Date();
        var tmp_in_home_date = (val.items.inhome != undefined && val.items.inhome[type] != "") ? new Date(val.items.inhome[type]) : "";

        $.each(val.items, function (key, value) {
                if (value.isRequired == 1 && value.isVisible == true && value[type] == "" && !(key == "inhome" && type == "completedDate")) {
                    if (msg != "")
                        msg += "</br>";
                    msg += value.description + " date is mandatory!";
                }
        });

        if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) { //cw
            if (tmp_in_home_date != "")
                tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4));

            switch (sessionStorage.userRole.toLowerCase()) {
                case "admin":
                    if (tmp_in_home_date != "" && list_arrival_date != "" && (!(list_arrival_date < tmp_in_home_date))) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "Data/List arrival date should be at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 :4) + " days less than the In home date.";
                    }

                    if (tmp_in_home_date != "" && art_arrival_date != "" && (!(art_arrival_date < tmp_in_home_date))) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "Art arrival date should be at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                    }
                    break;
                case "power":
                case "user":
                    var tmp_current_date = new Date();
                    var tmp_in_home_date = (val.items.inhome != undefined && val.items.inhome[type] != "") ? new Date(val.items.inhome[type]) : "";

                    var current_date = new Date();
                    var curr_date = (current_date != "") ? new Date(current_date).setHours(0, 0, 0, 0) : "";

                    tmp_current_date.setDate(tmp_current_date.getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10));
                    if (tmp_in_home_date != "")
                        tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10));

                    if (tmp_in_home_date != "" && list_arrival_date != "" && (list_arrival_date <= curr_date || list_arrival_date > tmp_in_home_date)) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "The Data/List Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days prior to the in-home date.";
                    }

                    if (tmp_in_home_date != "" && art_arrival_date != "" && (art_arrival_date <= curr_date || art_arrival_date > tmp_in_home_date)) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "The Art Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 10) + " days prior to the in-home date.";
                    }
                    break;
            }
            if (sale_start_date != "" && sale_end_date != "" && sale_start_date > sale_end_date) {
                if (msg != "")
                    msg += "</br></br>";
                msg += "Sale end date should be greater than or equal to Sale start date.";
            }
        }
        else if (jobCustomerNumber == "1" || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER) { //TD
            $.each(val.items, function (key, value) {
                if (value.isRequired == 1 && value[type] == "" && !(key == "inhome" && type == "completedDate")) {
                    if (msg != "")
                        msg += "</br>";
                    msg += value.description + " date is mandatory!";
                }
            });

            switch (sessionStorage.userRole.toLowerCase()) {
                case "admin":
                    if (tmp_in_home_date != "" && list_arrival_date != "" && (!(list_arrival_date < tmp_in_home_date))) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "Data/List arrival date should be at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 :4) + " days less than the In home date.";
                    }

                    if (tmp_in_home_date != "" && art_arrival_date != "" && (!(art_arrival_date < tmp_in_home_date))) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "Art arrival date should be at least " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 4) + " days less than the In home date.";
                    }
                    break;
                case "power":
                case "user":
                    var tmp_current_date = new Date();
                    var tmp_in_home_date = in_home_date;//(val.items.inhome != undefined && val.items.inhome[type] != "") ? new Date(val.items.inhome[type]) : "";

                    var current_date = new Date();
                    var curr_date = (current_date != "") ? new Date(current_date).setHours(0, 0, 0, 0) : "";

                    tmp_current_date.setDate(tmp_current_date.getDate() + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7)); // 7 days for TD normal user
                    if (tmp_in_home_date != "")
                        tmp_in_home_date.setDate(tmp_in_home_date.getDate() - ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7)); // 7 days for TD normal user

                    if (tmp_in_home_date != "" && list_arrival_date != "" && (list_arrival_date <= curr_date || list_arrival_date > tmp_in_home_date)) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "The Data/List Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7) + " days prior to the in-home date.";
                    }

                    if (tmp_in_home_date != "" && art_arrival_date != "" && (art_arrival_date <= curr_date || art_arrival_date > tmp_in_home_date)) {
                        if (msg != "")
                            msg += "</br></br>";
                        msg += "The Art Arrival date should be set between the current date and " + ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3 : 7) + " days prior to the in-home date.";
                    }
                    break;
            }
            if (sale_start_date != "" && sale_end_date != "" && sale_start_date > sale_end_date) {
                if (msg != "")
                    msg += "</br></br>";
                msg += "Sale end date should be greater than or equal to Sale start date.";
            }
        }
        return msg;
    };
};