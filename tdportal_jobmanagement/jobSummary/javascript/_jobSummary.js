﻿jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var gServiceUrl = (jobCustomerNumber != SUNTIMES_CUSTOMER_NUMBER) ? '../JSON/_reports.JSON' : '../JSON/_stmgReports.JSON';
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var dailyTaskServiceUrl = "../JSON/_dailyTaskList.JSON";
var dailyTaskData = {};
var pagePrefs = "";
var jobDesc = '';
jobDesc = getSessionData("jobDesc");
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_jobSummary', function (event) {
    //makeGData();
    //makeGOutputData();  
    var $dropArea = $("#drop-area");
    $dropArea.on({
        "drop": pageObj.makeDrop,
        "dragenter": pageObj.ignoreDrag,
        "dragover": pageObj.ignoreDrag
    });
    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        pageObj.makeDrop(e, true, e.target)
    });

    if (sessionStorage.singleJobPrefs != undefined && sessionStorage.singleJobPrefs != null && sessionStorage.singleJobPrefs != "null" && sessionStorage.singleJobPrefs != "")
        pageObj.loadData();
    $('#dvcontrolGroupName').find('#pCustomerNumber').remove();

    if (appPrivileges.roleName == "admin") {
        var customer_number = '<p style="padding-top:24px;" id="pCustomerNumber">Customer Number : ' + jobCustomerNumber + '</p>';
        $('#dvcontrolGroupName').append(customer_number);
    }

    $("#_jobSummary").trigger("create");
    $('#thermometer-f').attr('data-percent', getPendingApprovalPercent(sessionStorage.jobDesc));
    if (appPrivileges.roleName != "admin" || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
        $("#divReports").css("display", "none");
        $("#dvDataSpace").css("display", "none");
    }
    if ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && appPrivileges.roleName != "user") {
        $("#divReports").css("display", "block");
        $("#dvDataSpace").css("display", "block");
    }
    if (jobCustomerNumber == SK_CUSTOMER_NUMBER && appPrivileges.roleName != "admin")
        $("#dvData").hide();

    if (appPrivileges.customerNumber != "1" && appPrivileges.customerNumber != REDPLUM_CUSTOMER_NUMBER) {
        $('#dvFlagComments').css('display', 'none');
        $('#divComments').css('display', 'none');
    }
    $('#filterChkViewAllComments').attr('checked', true).checkboxradio('refresh');
    //$("#filterChkViewAllComments").attr("checked", "checked");
    //if (appPrivileges.customerNumber == REGIS_CUSTOMER_NUMBER || appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER || appPrivileges.customerNumber == "99998" || appPrivileges.customerNumber == "99997") {
    if ( jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
        $('#aUploadData').text('List Selection');
        $('#aUploadData').attr('onclick', 'pageObj.fnEditJob("listSelection");');
    }
    setLogo(jobCustomerNumber);
    if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
        loadDemoJobTicket();
    }
    $('.thermometer-noconfig').thermometer({
        speed: 'slow'
    });    
    $('#ulOrderStatus p span:lt(' + getPendingApprovalStatus(getPendingApprovalPercent(sessionStorage.jobDesc)) + ')').css('color', 'green');
	 $('#divOrderStatus').css('display', 'none');
    $('div[data-role=collapsible-set] h3 a').removeAttr('href');
});

$('#_jobSummary').live('pagebeforecreate', function (event) {

    if (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) {
        getDailyTaskData();
    }

    //load IFRAME for IE to upload files.
    loadUploadIFrame();

    pageObj = new jobSummary();
    //getData();
    if (sessionStorage.singleJobPrefs == undefined || sessionStorage.singleJobPrefs == null || sessionStorage.singleJobPrefs == "")
        if (jobCustomerNumber == SUNTIMES_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
            getPagePreferences('singleJob.htm', sessionStorage.facilityId, ((jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER )? "1" :CW_CUSTOMER_NUMBER));
        }
        else {
            getPagePreferences('singleJob.htm', sessionStorage.facilityId, ((jobCustomerNumber == JETS_CUSTOMER_NUMBER) ? CASEYS_CUSTOMER_NUMBER : appPrivileges.customerNumber));
        }
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.singleJobPrefs));
    }

    //if (appPrivileges.customerNumber == "1" && appPrivileges.roleName == "admin" && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
    if ((jobCustomerNumber == "1" || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin" && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
        $('#aAddJobs').css('display', 'none');
        //$('#aEditJobs').addClass('ui-corner-bl ui-corner-br');
        $('#aEditJobs').addClass('ui-corner-all');
    }
    else {
        $('#aAddJobs').css('display', 'none');
        $('#aEditJobs').addClass('ui-corner-tl ui-corner-tr');
        $('#aUploadArtwork').addClass('ui-corner-bl ui-corner-br');

        if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
            $('#aUploadData').addClass('ui-corner-bl ui-corner-br');
        }
        if ((appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) && (appPrivileges.roleName == "user" || appPrivileges.roleName == "power")) {
            $('#aEditJobs').css('display', 'none');
            //$('#btnSignOut').addClass('ui-corner-all');
        }
    }
    if (sessionStorage.isGizmoJob != undefined && sessionStorage.isGizmoJob != null && sessionStorage.isGizmoJob != "" && !JSON.parse(sessionStorage.isGizmoJob)) {
        $('#aEditJobs').css('display', 'none');
        $('#aAddJobs').addClass('ui-corner-bl ui-corner-br');
    }

    $('#aTribTrack').css('display', 'none');
    $('#aShippingOptimization').css('display', 'none');
    //}

    displayMessage('_jobSummary');
    var user_name;

    $("#popPDF").on({
        popupbeforeposition: function () {
            var maxHeight = $(window).height() - 80 + "px";
            $("#popPDF img").css("max-height", maxHeight);
            $("#popPDF embed").css("max-height", maxHeight);
            var maxWidth = $(window).width() - 80 + "px";
            $("#popPDF img").css("max-width", maxWidth);
            $("#popPDF embed").css("max-width", maxWidth);
        }
    });
    user_name = sessionStorage.username;

    var neg_job_number = getSessionData("negJobNumber");
    var header_text = jobDesc + ' | ' + pageObj.jobNumber;
    if (neg_job_number != undefined && neg_job_number != null && neg_job_number != "")
        header_text += ' (' + neg_job_number + ')';
    $('#navHeader').html('<h3>' + header_text + '</h3>');

    $('#aEditJobs').attr('onclick', 'pageObj.fnEditJob("edit");');

    if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER) {
        $('#aUploadData').show();
        $('#aUploadArtwork').show();
        $('#aUploadData').attr('onclick', 'pageObj.fnEditJob("uploadData");');
        $('#aUploadArtwork').attr('onclick', 'pageObj.fnEditJob("uploadArt");');
        $('#dvArtwork').show();
    }
    else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == SUNTIMES_CUSTOMER_NUMBER) {
        $('#aUploadData').show();
        $('#aUploadArtwork').hide();
        $('#aUploadArtwork').text('Opt-In Status');
        $('#aUploadArtwork').attr('onclick', 'pageObj.fnEditJob("optInStatus");');
        $('#dvArtwork').hide();
        $('#dvData').hide();
    }
    else {
        $('#aUploadData').hide();
        $('#aUploadArtwork').hide();
        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || (jobCustomerNumber == SK_CUSTOMER_NUMBER && appPrivileges.roleName == "admin")) {
            $('#dvArtwork').show();
            $('#dvData').show();
        }
        else
            $('#dvArtwork').hide();
    }
    if (jobCustomerNumber == "203")
        $('#aUploadData').hide();


    $('#aAddJobs').attr('href', '../jobSelectTemplate/jobSelectTemplate.html');

    //test for mobility...
    loadMobility();
    loadingImg("_jobSummary");

});

function getPendingApprovalPercent(job_name) {
    var count = 0;
    for (var x = 0, c = ''; c = job_name.charAt(x) ; x++) {
        count += c.charCodeAt(0);
    }
    count = count % 100;
    count = count < 15 ? 100 : count;
    return count;
};

function getPendingApprovalStatus(pending_approval_percent) {
    if (pending_approval_percent <= 15) return 1;
    else if (pending_approval_percent <= 30) return 2;
    else if (pending_approval_percent <= 45) return 3;
    else if (pending_approval_percent <= 60) return 4;
    else if (pending_approval_percent <= 75) return 5;
    else if (pending_approval_percent <= 90) return 6;
    else if (pending_approval_percent <= 100) return 7;

    return '';
}

function getDailyTaskData() {
    $.getJSON(dailyTaskServiceUrl, function (data) {
        if (data[jobCustomerNumber] != undefined && data[jobCustomerNumber] != null)
            dailyTaskData = data[jobCustomerNumber];
    });
}
//******************** Page Load Events End **************************

