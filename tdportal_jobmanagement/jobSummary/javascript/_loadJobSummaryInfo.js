﻿var jobSummary = function () {
    var self = this;
    //******************** Global Variables Start **************************
    self.jobNumber = "";
    var facilityId = "";
    self.jobNumber = getSessionData("jobNumber");
    self.milestoneBeforeUpdate = [];
    self.milestoneAfterUpdate = [];
    self.facilityId = getSessionData("facilityId");
    facilityId = getSessionData("facilityId");
    self.gData = [];
    self.gJobComments = [];
    self.gOutputData;
    self.gDataAll;
    self.gPageSize = 100;
    self.gDataStartIndex = 0;
    self.thumbnail_img = "";
    self.thumbnail_img_back = "";
    self.cust_logo = "";
    self.id = "";
    self.isJobProcessed = 'Yes';
    var gEditJobService = serviceURLDomain + "api/JobTicket/" + self.facilityId + "/" + self.jobNumber + "/" + sessionStorage.username;
    //******************** Global Variables End **************************
    new jobSummaryComments(self);
    new jobSummaryMilestones(self);

    //******************** Public Functions Start **************************
    //Loads job summary page data
    self.loadData = function () {
        pagePrefs = jQuery.parseJSON(sessionStorage.singleJobPrefs);

        if (!fnVerifyScriptBlockDict(pagePrefs))
            return false;

        if (pagePrefs != null) {
            eval(getURLDecode(pagePrefs.scriptBlockDict.pagebeforecreate));

            self.displayImages();

            self.makeGData();
            self.makeGOutputData();
        }
    }

    //Displays the selected template images (background and front images) for the selected job.
    self.displayImages = function () {
        if (appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
            var img_back = sessionStorage.jobNumber + "_Caseys-Pizza-DeliveryOnly-back.png";
            var img_front = sessionStorage.jobNumber + "_Caseys-Pizza-DeliveryOnly-front.png";
            self.thumbnail_img = logoPath + "Caseys/" + img_front;
            self.thumbnail_img_back = logoPath + "Caseys/" + img_back;
            $(document).ready(function () {
                //thumbnail found, show front thumbnail
                var imgThumbnail = new Image();
                $(imgThumbnail)
            .load(function () {
                $(this).hide();
                $('#divThumbnail')
                    .append(this);
                $(this).fadeIn();
            })
            .attr('src', self.thumbnail_img);
                //thumbnail found, show back thumbnail
                var imgThumbnailBack = new Image();
                $(imgThumbnailBack)
            .load(function () {
                $(this).hide();
                $('#divThumbnail')
                    .append(this);
                $(this).fadeIn();
            })
            .attr('src', self.thumbnail_img_back);
            });
        }
        else {
            if (appPrivileges.customerNumber != "1" && appPrivileges.customerNumber != REDPLUM_CUSTOMER_NUMBER) {
                $(document).ready(function () {
                    //thumbnail found, show front thumbnail
                    var imgThumbnail = new Image();
                    $(imgThumbnail)
        .load(function () {
            $(this).hide();
            $('#divThumbnail')
                .append(this);
            $(this).fadeIn();
            $('#divText').hide();
        })
        .attr('src', self.thumbnail_img);
                    //thumbnail found, show back thumbnail
                    var imgThumbnailBack = new Image();
                    $(imgThumbnailBack)
        .load(function () {
            $(this).hide();
            $('#divThumbnail')
                .append(this);
            $(this).fadeIn();
            $('#divText').hide();
        })
            .attr('src', self.thumbnail_img_back);
                    //thumbnail not found, show customer logo
                    var imgCustLogo = new Image();
                    $(imgCustLogo)
        .load(function () {
            $(this).hide();
            $('#divCustImage')
                .append(this);
            $(this).fadeIn();
            $('#divText').hide();
        })
        .attr('src', self.cust_logo);
                    //both not found, show text
                    $('#divText').html('<h3>' + jobDesc + ' | ' + self.jobNumber + '</h3>');
                });
            }
        }
    };

    //validates edit button click - if id is empty stops the action.
    self.fnEditJob = function (id) {
        if (id != "") {
            if (!self.homeButtonClick(id)) return false;
        }
        self.getEditJobData(id);
    }

    //Gets the selected job data from service.
    self.getEditJobData = function (id) {
        getCORS(gEditJobService, null, function (data) {
            self.assignGetData(data, id);
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    }

    //Creates required local session objects for the selected job when edit is clicked.
    self.assignGetData = function (data, id) {
        if (data == null) {
            $('#alertmsg').text("No data exists for the selected Job. Please contact Administrator.");
            $('#popupDialog').popup('open');
            return false;
        }
        else {
            makeEditJobData(data);
            self.navigation(id);
        }
    }

    //Navigates to required page based on the button clicked.
    self.navigation = function (id) {
        if (id == 'edit')
            window.location.href = "../jobSelectTemplate/jobSelectTemplate.html";
        else if (id == 'uploadData')
            window.location.href = "../jobUploadList/jobUploadList.html";
        else if (id == 'listSelection')
            window.location.href = "../listSelection/jobListSelection.html";
        else if (id == 'uploadArt')
            window.location.href = "../jobUploadArtwork/jobUploadArtwork.html";
        else if (id == 'optInStatus')
            window.location.href = "../optInStatus/optInStatus.html";
        else if (id == "")
            self.saveMilestoneDataToJobTicket();
    }

    //Loads the saved milestone information for the selected job into UI.
    self.buildOutputLists = function () {
        $.each(self.gData, function (key, val) {
            if (key != undefined) {
                switch (key) {
                    case "milestoneDict":
                        $.each(val[0].items, function (keyMileStones, valMileStones) {
                            if ($("#hdnMilestoneFilter").val() == "standard") {
                                $("#spnScheduledDate" + keyMileStones).text((valMileStones.scheduledDate.indexOf(' ') == -1) ? valMileStones.scheduledDate : valMileStones.scheduledDate.substring(0, valMileStones.scheduledDate.indexOf(' ')));

                                $("#spnCompletedDate" + keyMileStones).text((valMileStones.completedDate.indexOf(' ') == -1) ? valMileStones.completedDate : valMileStones.completedDate.substring(0, valMileStones.completedDate.indexOf(' ')));
                            }
                            else if ($("#hdnMilestoneFilter").val() == "rush") {
                                $("#txt" + keyMileStones).val((valMileStones.scheduledDate.indexOf(' ') == -1) ? valMileStones.scheduledDate : valMileStones.scheduledDate.substring(0, valMileStones.scheduledDate.indexOf(' ')));
                            }
                            else if ($("#hdnMilestoneFilter").val() == "express") {
                                $("#txt" + keyMileStones).val((valMileStones.completedDate.indexOf(' ') == -1) ? valMileStones.completedDate : valMileStones.completedDate.substring(0, valMileStones.completedDate.indexOf(' ')));
                            }
                        });
                        break;
                }
            }
        });
    }

    //Loads the milestones for the option selected in the milestones dropdown.
    self.updateStartIndex = function (ctrl, selector_id, list_div, caption) {
        $("#hdnMilestoneFilter").val($("#" + ctrl.id).val());
        $.each(self.gData, function (key, val) {
            var temp_mile_stones_data = [];
            var selector_id = "", list_div = "", caption = "";
            switch (val.list) {
                case "milestoneDict":
                    temp_mile_stones_data = self.buildMilestonesList(val.items);
                    selector_id = 'chicagoSelect';
                    list_div = 'milestones';
                    caption = 'MILESTONES';
                    break;
            }
            if (temp_mile_stones_data.length > 0)
                self.makeList(temp_mile_stones_data, selector_id, list_div, caption);

        });
        self.buildOutputLists();
        $('#dvMilestones').trigger('create');
        $('#dailyTaskListsCollapSet').collapsibleset('refresh');
        $('#milestones li').removeClass('ui-body-inherit');
        $("#dvMilestones h3.ui-collapsible-heading").trigger("click")
    }

    //Generates the items (milestones, comments, reports) sections and adds to UI to display.
    self.makeList = function (array_name, selector_id, list_div, caption) {
        var header = "";
        var selector;
        var array = eval(array_name);
        var selector = "";

        var temp_array = array.slice(self.gDataStartIndex, self.gDataStartIndex + self.gPageSize)
        //eval(getURLDecode(pagePrefs.scriptBlockDict.makeList));
        if (appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || (appPrivileges.customerNumber == CW_CUSTOMER_NUMBER && appPrivileges.roleName != "admin")) { // for casey's hide the reports section. For cgs, in regional home hide reports, customers, info and acct.mgmt,production sections.
            //header = '<li data-role="list-divider">' + caption + '';
if (caption.toUpperCase() == 'MILESTONES'){
header = '<li data-role="list-divider" style="height:23px">';
}
        } else {
            //header = '<li data-role="list-divider"><a target="_self" href="regionalHome.htm?reg=' + list_div + '" style="text-decoration:none; color:#fff;">' + caption + '</a>';
            if ((appPrivileges.customerNumber != "1" && jobCustomerNumber != "1" && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && appPrivileges.customerNumber != REDPLUM_CUSTOMER_NUMBER) || list_div != "milestones") {
                // header = '<li data-role="list-divider" style="height:23px"><a target="_self" href="" style="text-decoration:none; color:#fff;">' + caption + '</a>';
                if (caption.toUpperCase() == 'MILESTONES') {
                    header = '<li data-role="list-divider" style="height:23px"><a target="_self" href="" style="text-decoration:none; color:#fff;"></a>';
                }
            }
            else
                header = '<li><a target="_self" href="" style="text-decoration:none; color:#fff;">&nbsp;&nbsp;</a>';
        }

        if (caption.toUpperCase() == 'MILESTONES') {
            var sel_option = $("#hdnMilestoneFilter").val();
            var ddl_top = "-6px";
            if ((appPrivileges.customerNumber == "1" && jobCustomerNumber != "1" && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER))
                ddl_top = "3px";
            header += '<div id="containDiv" style="position: absolute; top: ' + ddl_top + '; right: 10px; margin: 0; padding: 0;height:15px;" data-role="controlgroup" data-type="horizontal" data-mini="true" >';
            header += '<select name="selMilestones" id="selMilestones" data-theme="e" data-overlay-theme="d" data-native-menu="true" data-mini="true" style="height:15px;" onchange="pageObj.updateStartIndex(this,\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\');">';
            header += (sel_option == 'standard') ? '<option value="standard" selected>View All</option>' : '<option value="standard">View All</option>';
            header += (sel_option == 'rush') ? '<option value="rush" selected>Edit Scheduled Date</option>' : '<option value="rush">Edit Scheduled Date</option>';
            header += (sel_option == 'express') ? '<option value="express" selected>Edit Completed Date</option>' : '<option value="express">Edit Completed Date</option>';
            header += '</select></div>';
        }
        if (caption.toUpperCase() == 'COMMENTS' && appPrivileges.customerNumber == "1" && appPrivileges.roleName == "admin") {
            header += '<div id="containDivComments" style="position: absolute; top: -4px; right: 10px; margin: 0; padding: 0;" data-role="controlgroup" data-type="horizontal" data-mini="true" >';
            header += '<div align="right"><a href="#" id="btnFilterComments" data-role="button" data-icon="myapp-filter"  data-iconpos="notext" data-inline="true" data-theme="e" onclick="$(\'#popupFilterComments\').popup(\'open\');">Filter Comments</a></div></div>';
        }
        if (header != "")
            header += '</li>'
        //if (caption.toUpperCase() == 'MILESTONES' && appPrivileges.customerNumber == "1")
        if (caption.toUpperCase() == 'MILESTONES' && (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER || appPrivileges.roleName == "admin"))
            header += '<li data-theme="d" id="liAddRemoveMilestones"><a href="#" onclick="pageObj.openMilestoneListPopUp()" data-rel="popup" data-position-to="window"  data-inline="true" style="font-size:12px;" >Add/Remove Milestones</a></li>';

        temp_array.splice(0, 0, header)

        if ((appPrivileges.customerNumber == "1" && jobCustomerNumber != "1" && appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER) && list_div == "milestones") {
            $('#ulMilestones').remove();
            $('#dvMilestones').remove();
            var temp_list = '<ul id="milestones" data-role="listview" data-theme="f" data-inset="false" style="margin-left:-1em;margin-right:-1em;border-left-style:solid;border-right-style:solid;border-left-width:thin;border-right-width:thin;border-left-color:lightgray;border-right-color:lightgray;">' + temp_array.join(' ') + '</ul>';
            var temp_milestones_set = '<div data-role="collapsible" data-inset="false" id="dvMilestones"><h3 style="margin:0;">MILESTONES</h3>' + temp_list + '</div>';
            $('#dailyTaskListsCollapSet').append(temp_milestones_set);
        }
        else {
            $('#' + list_div).empty();
            $('#' + list_div).append(temp_array.join(' '));
            $('#' + list_div).listview('refresh');
        }
        //
        $("#containDiv select").each(function (i) {
            $(this).selectmenu();
        });
        $("li fieldset[data-role=controlgroup]").trigger('create');
        $.each($('milestones input[type=checkbox]'), function (a, b) {
            $(this).checkboxradio('refresh')
        });

        $('#containDivComments').trigger('create');
    }

    //Get the Data for various sections (milestones, personnel, files, comments etc) from service.
    self.makeGData = function () {
        $.getJSON(gServiceUrl, function (data) {
            self.gData.push({
                "list": "reports",
                "items": data
            });
            //if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
                //self.loadListApprovals();
                //self.loadArtApprovals();
                //$('#divListApproval').css('display', 'block');
                //$('#divArtApproval').css('display', 'block');
            //} 
            if (jobCustomerNumber != "1" && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER) {
                new dailyTaskListInfo(self);
                getCORS(serviceURLDomain + "api/Milestone/" + self.facilityId + "/" + self.jobNumber + "/" + jobCustomerNumber + "/null", null, function (milestone_data) {
                    self.loadMilestoneData(milestone_data);
                }, function (error_response) {
                    showErrorResponseText(error_response, false);
                });
            }
            else {
                new dailyTaskListInfo(self);
            }
            getCORS(serviceURLDomain + "api/Personnel/" + self.facilityId + "/" + self.jobNumber, null, function (personnel_data) {
                self.loadPersonnelData(personnel_data);
            }, function (error_response) {
                showErrorResponseText(error_response, false);
            });
            //Comments load        
            if (appPrivileges.customerNumber == "1") {
                self.getFilteredComments();
            }

            getCORS(serviceURLDomain + "api/Files/" + self.facilityId + "/" + self.jobNumber, null, function (list_data) {
                self.loadListData(list_data);
                self.buildLists();
            }, function (error_response) {
                showErrorResponseText(error_response, false);
            });
        });
    }

    //Generates the output data.
    self.makeGOutputData = function () {
        $.getJSON(gOutputServiceUrl, function (dataOutput) {
            self.gOutputData = dataOutput;
            self.buildOutputLists();
        });
    }

    self.orderStatusInfo = function () {
        var order_status_info = {

        };
    };

    self.loadListApprovals = function () {
        var list_approvals_data = {
            "listApproval": {
                "List Summary.pdf": {
                    "fileName": "List Summary.pdf",
                    "versionDate": "5/20/2016 4:14 AM",
                    "overallStatus": 1,
                    "approverList": [{
                        "email": "jim.wright@tribunedirect.com",
                        "approvalStatus": 1
                    }, {
                        "email": "charlie.smith@tribunedirect.com",
                        "approvalStatus": 1
                    }]
                }
            },
            "name": "listApproval",
            "type": "list",
            "isEnabled": true
        };
        var items = [];
        $.each(list_approvals_data.listApproval, function (key, val) {
            items.push(val);
        });
        self.gData.push({
            "list": "listApprovalDict",
            "items": items
        });
    };

    self.loadArtApprovals = function () {
        var art_approval_data = {
            "artApproval": {
                "Drink Fish Repeat_9x6.pdf": {
                    "fileName": "Drink Fish Repeat_9x6.pdf",
                    "versionDate": "5/20/2016 4:14 AM",
                    "overallStatus": 4,
                    "approverList": [{
                        "email": "jim.wright@tribunedirect.com",
                        "approvalStatus": 4
                    }, {
                        "email": "charlie.smith@tribunedirect.com",
                        "approvalStatus": 4
                    }]
                },
                "Spike the Pike_6x4.pdf": {
                    "fileName": "Spike the Pike_6x4.pdf",
                    "overallStatus": 4,
                    "versionDate": "5/20/2016 4:55 AM",
                    "approverList": [{
                        "email": "jim.wright@tribunedirect.com",
                        "approvalStatus": 4
                    }]
                }
            },
            "name": "artApproval",
            "type": "artwork",
            "isEnabled": true
        };
        if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            art_approval_data = {
                "artApproval": {
                    "SportsKing 6x9 Postcard.pdf": {
                        "fileName": "SportsKing 6x9 Postcard.pdf",
                        "versionDate": "5/27/2016 4:14 PM",
                        "overallStatus": 4,
                        "approverList": [{
                            "email": "jim.wright@tribunedirect.com",
                            "approvalStatus": 4
                        }, {
                            "email": "charlie.smith@tribunedirect.com",
                            "approvalStatus": 4
                        }]
                    },
                    "SportsKing Basketball Mockup 6x9PC.pdf": {
                        "fileName": "SportsKing Basketball Mockup 6x9PC.pdf",
                        "overallStatus": 4,
                        "versionDate": "5/27/2016 4:55 PM",
                        "approverList": [{
                            "email": "jim.wright@tribunedirect.com",
                            "approvalStatus": 4
                        }]
                    }
                },
                "name": "artApproval",
                "type": "artwork",
                "isEnabled": true
            };
        }
        var items = [];
        $.each(art_approval_data.artApproval, function (key, val) {
            items.push(val);
        });
        self.gData.push({
            "list": "artApprovalDict",
            "items": items
        });
    };

    //Geneartes object for personal section data.
    self.loadPersonnelData = function (personnel_data) {
        if (personnel_data != "" && personnel_data.items.length > 0) {
            self.gData.push({
                "list": "personalDict",
                "items": personnel_data.items
            });
        }
        else {
            $('#dvPersonnel').hide();
        }
    }

    //Generates object File list section data
    self.loadListData = function (list_data) {
        if (list_data != "" && list_data != undefined && list_data.length > 0) {
            var data_files = [];
            var art_files = [];
            $.each(list_data, function (key, val) {
                $.each(val.items, function (key1, val1) {
                    if (val.list.toLowerCase() == 'source')
                        data_files.push(val1);
                    else if (val.list.toLowerCase() == 'artwork')
                        art_files.push(val1);
                });

            });
            self.gData.push({
                "list": "dataDict",
                "items": data_files
            });
            self.gData.push({
                "list": "artDict",
                "items": art_files
            });
        }
        else {
            self.gData.push({
                "list": "dataDict",
                "items": []
            });
            self.gData.push({
                "list": "artDict",
                "items": []
            });
        }
    };

    //Generates object for milestone section data
    self.loadMilestoneData = function (milestone_data) {
        if (milestone_data != null && milestone_data != "" && milestone_data.milestoneDict != undefined) {
            if (self.gData != undefined) {
                self.gData.push({
                    "list": "milestoneDict",
                    "items": sortMilestones(milestone_data.milestoneDict)
                });

                var milestone_info = $.extend(true, {}, milestone_data.milestoneDict);

                self.milestoneBeforeUpdate.push({
                    "list": "milestoneDict",
                    "items": sortMilestones(milestone_info)
                });

                self.milestoneAfterUpdate = $.extend(true, [], self.milestoneBeforeUpdate);
                self.setMilestoneListPopup();
            }
        }
        //self.buildLists();
        window.setTimeout(function setCol2() {
            $('#dvDailyTasksList').trigger('create');
        }, 500);
    }

    //Ganerates list items for report, data dic,art,personal, milestone and comments
    self.buildLists = function () {
        $.each(self.gData, function (key, val) {
            var temp_mile_stones_data = [];
            var selector_id = "", list_div = "", caption = "", report_name = "";
            var newly_added_report_list = ["processingSummary", "demographics"];//"billingInvoice","postageAdvanceInvoice",
            switch (val.list) {
                case "reports":
                    //eval(getURLDecode(pagePrefs.scriptBlockDict.buildListsReports));
                    if (self.isJobProcessed === "Yes") {
                        if (appPrivileges.customerNumber != CASEYS_CUSTOMER_NUMBER) { // for casey's hide the reports section.
                        $.each(val.items, function (keyRpt, valRpt) {
                            if (valRpt.userRole.toLowerCase().indexOf(appPrivileges.roleName.toLowerCase()) > -1) {
                                // if (valRpt.reportName != "jobInformationRecap") {
                                if (valRpt.reportName == "jobInformationRecap") {
                                    if (jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER)
                                        temp_mile_stones_data.push('<li id="' + valRpt.reportName + '" data-theme="c"><a onclick=pageObj.openPDF(\'' + valRpt.reportName + '\');  data-rel="popup" data-position-to="window"  data-inline="true">' + valRpt.header + '</a></li>');
                                } else {
                                    if (((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (valRpt.reportName != "IS State Store Counts" && valRpt.reportName != "IS Zip Code Counts" && valRpt.reportName != "processingSummary" && valRpt.reportName != "demographics")) || ((jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && valRpt.reportName != "processingSummary" && valRpt.reportName != "demographics") || (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && valRpt.reportName == "Billing Invoice"))) {
                                        report_name = valRpt.reportName.replace(/ /g, '_');
                                        temp_mile_stones_data.push('<li id="' + report_name + '" data-theme="c"><a ' + (($.inArray(report_name, newly_added_report_list) == -1) ? 'onclick=pageObj.populateOutputForm(\'' + report_name + '\');' : '') + ' href="#popupReportsParam" data-rel="popup" data-position-to="window"  data-inline="true">' + valRpt.header + '</a></li>');
                                    }
                                }
                                // }
                            }
                        });
                        }
                        selector_id = 'reportSelect';
                        list_div = 'reports';
                        caption = 'REPORTS';
                    }
                    break;
                case "dataDict":
                    //eval(getURLDecode(pagePrefs.scriptBlockDict.buildListsDataDict));
                    if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) { // for casey's hide the reports section.
                        var source_type = '<li data-role="list-divider" style="background-color:Gray;">Source</li>';
                        var seed_type = '<li data-role="list-divider" style="background-color:Gray;">Seed</li>';
                        var dnm_type = '<li data-role="list-divider" style="background-color:Gray;">Do Not Mail</li>';
                        var source_display = seed_display = dnm_display = false;
                        $.each(val.items, function (key2, val2) {
                            var file_mapping_url = '../jobFileMapper/jobFileMapper.html?ft=' + $.trim(val2.dir.toLowerCase()) + '&fn=' + val2.name;
                            var li = '<li data-icon="false" id="li' + key2 + '" +  data-theme="c"><a target="_self" ' + (((appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin") ? 'onclick="pageObj.navigateMapper(\'' + file_mapping_url + '\');"' : '') + '>' + val2.name + '<span class="ui-li-count">' + val2.size + '</span></a></li>';
                            if ($.trim(val2.dir.toLowerCase()) == "source") {
                                source_type += li;
                                source_display = true;
                            }
                            else if ($.trim(val2.dir.toLowerCase()) == "seed") {// && appPrivileges.roleName == "admin"
                                seed_type += li;
                                seed_display = true;
                            }
                            else if ($.trim(val2.dir.toLowerCase()) == "do not mail") { //&& appPrivileges.roleName == "admin"
                                dnm_type += li;
                                dnm_display = true;
                            }
                        });

                        var file_list = "";
                        if (source_display)
                            file_list = source_type;
                        if (seed_display)
                            file_list = (file_list != "") ? (file_list + seed_type) : seed_type;
                        if (dnm_display)
                            file_list = (file_list != "") ? (file_list + dnm_type) : dnm_type;

                        temp_mile_stones_data.push(file_list);

                        selector_id = 'customerSelect';
                        list_div = 'data';
                        caption = 'DATA';
                    }
                    break;
                case "artDict":
                    var artwork_files = '';
                    $.each(val.items, function (key2, val2) {
                        if (val2.name.toLowerCase().indexOf('.db') == -1) {
                            //var file_mapping_url = 'fileMappingMapper.html?ft=' + $.trim(val2.dir.toLowerCase()) + '&fn=' + val2.name;
                            var li = '<li data-icon="false" id="li' + key2 + '" +  data-theme="c"><a target="_self" onclick="pageObj.displayArtwork(\'' + val2.name + '\')">' + val2.name + '<span class="ui-li-count">' + val2.size + '</span></a></li>';
                            artwork_files += li;
                        }
                    });
                    temp_mile_stones_data.push(artwork_files);

                    selector_id = 'customerSelect';
                    list_div = 'art';
                    caption = 'ARTWORK';
                    break;
                case "personalDict":
                    //eval(getURLDecode(pagePrefs.scriptBlockDict.buildListsPersonalDict));
                    //if (appPrivileges.customerNumber != CASEYS_CUSTOMER_NUMBER) { // for casey's hide the reports section.
                    $.each(val.items, function (key2, val2) {
                        temp_mile_stones_data.push('<li data-icon="false" data-theme="c"><a href="#">' + ((val2.name != undefined) ? val2.name : "") + '<p class="ui-li-aside">' + ((val2.modified != undefined) ? val2.modified : "") + '</p></a></li>');
                    });
                    selector_id = 'personnelSelect';
                    list_div = 'personnel';
                    caption = 'CONTRIBUTORS';
                    //}
                    break;
                case "milestoneDict":
                    if (appPrivileges.customerNumber != "1" && appPrivileges.customerNumber != REDPLUM_CUSTOMER_NUMBER)
                        $('#dvDailyTasksList').hide();
                    //eval(getURLDecode(pagePrefs.scriptBlockDict.buildListsMilestoneDict));
                    //if (appPrivileges.customerNumber != CASEYS_CUSTOMER_NUMBER) { // for casey's hide the milestones section.
                    temp_mile_stones_data = self.buildMilestonesList(val.items);
                    if (temp_mile_stones_data.length == 0)
                        temp_mile_stones_data.push("");
                    selector_id = 'chicagoSelect';
                    list_div = 'milestones';
                    caption = 'MILESTONES';
                    //}
                    break;
                case "commentsDict":
                    //temp_mile_stones_data.push('<li data-role="list-divider"><a href="#" style="text-decoration:none; color:#fff;">COMMENTS</a></li>');
                    temp_mile_stones_data.push('<li data-theme="c" data-icon="myapp-comment"><a href="#popupComments" data-rel="popup" data-position-to="window" data-inline="true" style="font-size:12px;" >Add Comments</p></a></li>');
                    val.items = val.items.sort(function (a, b) {
                        return new Date(b.created) - new Date(a.created);
                    });
                    $.each(val.items, function (keycomment, valcomment) {
                        var img_icons;
                        img_icons = (valcomment.wantsIS == "1") ? '<img src="' + logoPath + 'mileStoneIcons/iconList.png" width="16" height="16" title=" IS "/>' : "";
                        img_icons += (valcomment.wantsJobInstructions == "1") ? '<img src="' + logoPath + 'mileStoneIcons/iconWhitepaper.png" width="16" height="16" title="Job Instructions" />' : "";
                        img_icons += (valcomment.wantsProductionReport == "1") ? '<img src="' + logoPath + 'mileStoneIcons/iconProduction.png" width="16" height="16" title="Production Schedule" />' : "";
                        //temp_mile_stones_data.push('<li><h3><span style="font-size:12px; font-weight:normal;">' + valcomment.emailId + ' - ' + valcomment.date + '</span></h3><p><span style="font-size:14px">' + valcomment.comment + '</span></p></li>');
                        temp_mile_stones_data.push('<li><h3><span style="font-size:12px; font-weight:normal;">' + valcomment.userName + ' - ' + valcomment.created + '</span></h3><p><span class="wrapword" style="font-size:14px;" >' + valcomment.comment + '</span></p><span><p>' + img_icons + '</p></span></li>');
                    });
                    list_div = 'comments';
                    caption = 'COMMENTS';
                    break;
                case "dailyTaskList":
                    //if (jobCustomerNumber == "1")
                    //    $('#ulMilestones').hide();
                    //else
                    //    $('#ulMilestones').show();
                    ko.applyBindings(val.items);
                    if (jobCustomerNumber == "1" || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER) {
                        $('#dvDailyTasksList').trigger('create');
                        //$('#dvDailyTaskList h3.ui-collapsible-heading').first().trigger("click")
                    }
                    break;
                case "listApprovalDict":
                    var art_files = "";
                    $.each(val.items, function (key, val) {
                        art_files = "";
                        if (val.fileName != "") {
                            var approval_icon = "";
                            if (val.overallStatus == undefined && val.overallStatus == null);
                            switch (val.overallStatus) {
                                case 6:
                                    approval_icon = "icon_approved_with_changes.png";
                                    break;
                                case 4:
                                    approval_icon = "icon_pending.png";
                                    break;
                                case 1:
                                    approval_icon = "icon_approved.png";
                                    break;
                                case 0:
                                    approval_icon = "icon_rejected.png";
                                    break;
                            }
                            var approval_status = getApprovalStatus(parseInt(val.overallStatus));
                            if (val.overallStatus == "0")
                                rejected_count = 1;

                            var approval_status = getApprovalStatus(parseInt(val.overallStatus));

                            art_files += '<li data-icon="false" data-theme="c"><a class="word_wrap" style="cursor: default"><img src="../images/' + approval_icon + '" alt="' + val.approvalStatus + '" title="' + approval_status + '" class="ui-li-icon ui-corner-none"><h3 title="' + val.fileName + '" style="cursor:default;margin-top:0em;">' + val.fileName + '</h3><p title="' + self.getApproversList(val).replace('<br />', '&#013;') + '" style="cursor:default" class="word_wrap">';
                            art_files += 'Generated: ' + val.versionDate + ' | ';
                            art_files += self.getApproversList(val);
                            art_files += '</p></a></li>';
                            temp_mile_stones_data.push(art_files);
                        }
                    });
                    list_div = 'listapproval';
                    caption = 'LIST APPROVAL';
                    //temp_mile_stones_data = art_files;
                    break;
                case "artApprovalDict":
                    var art_files = "";
                    $.each(val.items, function (key, val) {
                        art_files = "";
                        if (val.fileName != "") {
                            var approval_icon = "";
                            if (val.overallStatus == undefined && val.overallStatus == null);
                            switch (val.overallStatus) {
                                case 6:
                                    approval_icon = "icon_approved_with_changes.png";
                                    break;
                                case 4:
                                    approval_icon = "icon_pending.png";
                                    break;
                                case 1:
                                    approval_icon = "icon_approved.png";
                                    break;
                                case 0:
                                    approval_icon = "icon_rejected.png";
                                    break;
                            }
                            var approval_status = getApprovalStatus(parseInt(val.overallStatus));
                            if (val.overallStatus == "0")
                                rejected_count = 1;

                            var approval_status = getApprovalStatus(parseInt(val.overallStatus));

                            art_files += '<li data-icon="false" data-theme="c"><a class="word_wrap" style="cursor: default"><img src="../images/' + approval_icon + '" alt="' + val.approvalStatus + '" title="' + approval_status + '" class="ui-li-icon ui-corner-none"><h3 title="' + val.fileName + '" style="cursor:default;margin-top:0em;">' + val.fileName + '</h3><p title="' + self.getApproversList(val).replace('<br />', '&#013;') + '" style="cursor:default" class="word_wrap">';
                            art_files += self.getApproversList(val);
                            art_files += '</p></a></li>';
                            temp_mile_stones_data.push(art_files);
                        }
                    });
                    list_div = 'artapproval';
                    caption = 'ART APPROVAL';
                    break;
            }
            if (temp_mile_stones_data.length > 0)
                self.makeList(temp_mile_stones_data, selector_id, list_div, caption);
        });
    }

    //Generates the Approvers and Viewers list
    self.getApproversList = function (source_obj) {
        var approvers_list = "";
        var viewers_list = "";
        //var temp_approvers_list = $.extend(true, [], approvers_list);
        if (source_obj.approverList != undefined && source_obj.approverList != null && source_obj.approverList.length > 0) {
            $.each(source_obj.approverList, function (key, val) {
                approvers_list += (approvers_list != "") ? ', ' + val.email + ' is ' + getApprovalStatus(parseInt(val.approvalStatus)) : 'Approvers: ' + val.email + ' is ' + getApprovalStatus(parseInt(val.approvalStatus));
            });
        }
        return approvers_list;
    };

    //Navigating file mapper
    self.navigateMapper = function (url) {
        sessionStorage.continueToOrder = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        window.location.href = url;
    }

    //Displaying selected artwork
    self.displayArtwork = function (file_name) {
        $('#divError').text('');
        $('#waitPopUp').popup('open', { positionTo: 'window' });
        $('#loadingText1').text("Loading Artwork.....");
        //var preview_artwork_url = serviceURLDomainInternal + "api/FileUpload_get/" + facilityId + "/" + jobNumber + "/" + file_name;
        var file_type = file_name.substring(file_name.lastIndexOf('.') + 1).toLowerCase();
        //file_type = "jpg";
        var content_type = null;
        switch (file_type) {
            case "pdf":
                content_type = 'application/pdf'
                break;
            case "jpg":
            case "jpeg":
            case "png":
            case "gif":
            case "tif":
            case "tiff":
                content_type = 'image/' + file_type
                break;
            case "svg":
                content_type = 'image/svg+xml'
                break;
        }
        var preview_artwork_url = serviceURLDomain + "api/FileUpload_get/" + self.facilityId + "/" + self.jobNumber + "/" + escape(file_name) + "/" + escape(content_type.replace(/\//g, '|'));
        var htmlText = null;
        if (content_type == 'application/pdf') {
            htmlText = "<embed width=850 height=500 type='" + content_type + "' src='" + preview_artwork_url + "' id='selectedFile'></embed>";
        } else {
            htmlText = "<img src='" + preview_artwork_url + "' id='selectedFile'></img>";
        }
        $('#popupPDFContent').html(htmlText);

        window.setTimeout(function getDelayNDisplayFile() {
            if ($('#selectedFile')[0].clientWidth > 0 && $('#selectedFile')[0].clientHeight > 0)
                var display = $("#popPDF");
            if (display != undefined) {
                display.scroll(function () {
                    var ctrls = $("#popPDF img");
                    console.log("************************");
                    ctrls.each(function (index) {
                        console.log($(this).position().top);

                        if ($(this).position().top < 0)
                            $(this).css("visibility", "hidden")//.hide();
                        else {
                            $(this).css("visibility", "")//.show();
                        }
                    });
                });
            }
            $('#waitPopUp').popup('close');
            window.setTimeout(function getDelay() {
                $('#popPDF').popup('open', { positionTo: 'window' });
            }, 200);

        }, 5000);
    };

    //Populating output form
    self.populateOutputForm = function (report_name) {
        $("#hdnReportSelected").val(report_name);
        $('#divIsDPRejectDetailsReport').hide();
        $('#divIsDPMailStreamSummaryReport').hide();
        $('#dvBillingInvInfo').hide();
        $('#dvIssueDate').hide();

        $("#selectOutputType").val('pdf').selectmenu('refresh');

        if (jobCustomerNumber == SUNTIMES_CUSTOMER_NUMBER) {
            $('#dvIssueDate').show();
            $('#divIsDPRejectDetailsReport').hide();
            $('#divIsDPMailStreamSummaryReport').hide();
        }
        if (report_name.toLowerCase() == "billing_invoice" || report_name.toLowerCase() == "rptpostagerequestall") {
            $('#dvBillingInvInfo').show();
            $("#txtCutomerIdInvoiceInfo").val(appPrivileges.customerNumber);
            //$("#txtJobNumberInvoiceSummary").val(jobNumber);
            //$("#txtFacilityIdInvoiceSummary").val(facilityId);
            $("#txtFacilityIdInvoiceInfo").val(sessionStorage.facilityId);
            $('#txtJobNumberInvoiceInfo').val(sessionStorage.jobNumber)

            if (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER) {
                $('#dvBillingInvInfo label[for="txtCutomerIdInvoiceInfo"]').hide();
                $('#txtCutomerIdInvoiceInfo').parent().css('display', 'none');

                $('#dvBillingInvInfo label[for="txtFacilityIdInvoiceInfo"]').hide();
                $('#txtFacilityIdInvoiceInfo').parent().css('display', 'none');

                $('#dvBillingInvInfo label[for="txtJobNumberInvoiceInfo"]').hide();
                $('#txtJobNumberInvoiceInfo').parent().css('display', 'none');
                $('#lblPositiveNumbers').css('display', 'none');
            }
        }
        $('#popupReportsParam').popup('open');
        $('#loadingText1').text("Loading Report.....");
    }

    //Open pdf file
    self.openPDF = function (report_name) {
        var htmlText = "<iframe width=850 HEIGHT=500 src='../pages/dataWorksReports.aspx?frmPage=singleJob&type=jobInformationRecap'></iframe>";
        $('#popupPDFContent').html(htmlText);
        $('#popPDF').popup('open');
        $('#loadingText1').text("Loading Report.....");
    }

    //Cancel report update
    self.updateReportCancel = function () {
        $("#popupReportsParam").popup("close");
    }

    self.validateReportsInfo = function (report_name) {
        var msg;
        switch (report_name.replace(/_/g, '')) {
            case "Materialsat2.5%":
            case "MaterialsNotReceived":
            case "MaterialswithWeights":
            case "LateDelivery":
                if ($('#txtIssueDate').val() == "") {
                    msg = "Please enter Issue Date";
                }
                break;
        }
        if (msg != undefined && msg != null && msg != "") {
            $("#popupReportsParam").popup('open');
            $("#popupReportsParam").popup('close');
            $('#alertmsg').html(msg);
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupReportsParam').popup('open');");
            return false;
        }
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
        return true;
    }

    //Update report value
    self.updateReportVal = function () {
        var output_type = $("#selectOutputType").val();
        var report_name = $("#hdnReportSelected").val();
        var ssrs_folder = "Gizmo";
        var input_report_params = [];
        if (!self.validateReportsInfo(report_name)) return false;
        $("#popupReportsParam").popup("close");
        switch (report_name) {
            case "DP_Address_Occurances":
                ssrs_folder = "IS Data Processing";
                break;
            case "billingInvoice":
            case "rptPostageRequestAll":
            case "IS_Reject_Details":
            case "IS_Mailstream_Summary":
            case "IS_Standardization_Summary":
            case "IS_State_Store_Counts":
            case "IS_Zip_Code_Counts":
                ssrs_folder = "Gizmo";
                break;
        }
        createJSONForReport(report_name, output_type, input_report_params, ssrs_folder, self.jobNumber);
    }

    //Alert message
    self.homeButtonClick = function (id) {
        var diff = [];
        diff = DiffObjects(self.milestoneBeforeUpdate, self.milestoneAfterUpdate);

        if (diff.length > 0) {
            var message = 'Leaving this screen without saving will cause you to lose all unsaved changes. Click "Leave This Screen" to leave this screen without saving or click "Continue With My Order".';
            $('#homeBtnMsg').text(message);
            $('#btnContinue').text("Cancel");
            $('#btnLeaveThisScreen').attr("onclick", "pageObj.leavePageWithoutSaveToDifferentPage('" + id + "');");
            $('#popupHomeClickDialog').popup('open');
            return false;
        }
        if (id == 'signout') {
            clearAllSession();
            if (appPrivileges.customerNumber == REGIS_CUSTOMER_NUMBER || appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER || appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) {
                var url1 = '../login.htm?q=' + appPrivileges.customerNumber;
                //window.location.href = "../login_new.htm";
                window.location.href = url1;
            }
            else
                window.location.href = '../login.htm';
            return false;
        }
        return true;
    }

    //Redirecting to index page
    self.redirectToIndex = function () {
        if (!pageObj.homeButtonClick(''))
            return false;
        redirectToHome();
    }

    //Leaving page without saving
    self.leavePageWithoutSaveToDifferentPage = function (id) {
        if (id == 'signout') {
            clearAllSession();
            if (appPrivileges.customerNumber == REGIS_CUSTOMER_NUMBER || appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) {
                var url2 = '../login.htm?q=' + appPrivileges.customerNumber;
                window.location.href = url2;
            }
            else
                window.location.href = '../login.htm';
        }
        else if (id == 'logoClick') {
            window.location.href = "../index.htm";
        }
        else if (id == 'new') {
            window.location.href = "../jobSelectTemplate/jobSelectTemplate.html";
            fnManageSessionsForNewJobs();
        }
        else if (id != '')
            pageObj.getEditJobData(id);
        else {
            redirectToHome();
        }
    }
}
//******************** Public Functions End **************************