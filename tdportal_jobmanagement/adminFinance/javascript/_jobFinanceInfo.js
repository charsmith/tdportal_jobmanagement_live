﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gAppFinanceServiceUrl = (sessionStorage.isAggregated) ? "../adminFinance/JSON/_jobFinanceAggregateAR.JSON" : "../adminFinance/JSON/_jobFinanceInfo.JSON";
var gServiceJobData;
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobFinanceInfo').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_jobFinanceInfo');
    createConfirmMessage("_jobFinanceInfo");
    getJobInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
});

$(document).on('pageshow', '#_jobFinanceInfo', function (event) {
    window.setTimeout(function () {
        pageObj = new loadFinanceInfo();
        ko.applyBindings(pageObj);
        $('div').trigger('create');
        $('select[data-role=slider]').slider('refresh');
        $('input[data-fieldType="dateRec"]').datepicker();
    }, 500);
});

var getJobInfo = function () {
    $.getJSON(gAppFinanceServiceUrl, function (data) {
        gServiceJobData = data;
    });
};

var paymentInfoVm = function (data) {
    var self = this;
    if (data.postageReceivedPretty != undefined && data.postageReceivedPretty != null)
        self.postageReceivedPretty = ko.observable(data.postageReceivedPretty);
    if (data.billingReceivedPretty != undefined && data.billingReceivedPretty != null)
        self.billingReceivedPretty = ko.observable(data.billingReceivedPretty);
    if (data.billingCheck != undefined && data.billingCheck != null)
        self.billingCheck = ko.observable(data.billingCheck);
    if (data.postageCheck != undefined && data.postageCheck != null)
        self.postageCheck = ko.observable(data.postageCheck);
    if (data.postageDate != undefined && data.postageDate != null)
        self.postageDate = ko.observable(data.postageDate);
    if (data.billingDate != undefined && data.billingDate != null)
        self.billingDate = ko.observable(data.billingDate);
};

var nonAggregateJobsInfoVm = function (data) {
    var self = this;
    self.isCombinedInv = ko.observable('off');
    self.combinedInvInfoList = ko.observableArray([]);
    self.paymentInfoList = ko.observableArray([]);
    $.each(data.paymentInfoList, function (pmt_key, pmt_val) {
        self.paymentInfoList.push(new paymentInfoVm(pmt_val));
    });
};

var aggregatedJobsInfoVm = function (data) {
    var self = this;
    self.jobNumber = ko.observable(data.jobNumber);
    self.jobName = ko.observable(data.jobName);
    self.isCombinedInv = ko.observable('off');
    self.combinedInvInfoList = ko.observableArray([]);
    self.paymentInfoList = ko.observableArray([]);
    $.each(data.paymentInfoList, function (pmt_key, pmt_val) {
        self.paymentInfoList.push(new paymentInfoVm(pmt_val));
    });
};

var loadFinanceInfo = function () {
    var self = this;

    self.jobs = ko.observableArray([]).extend({ notify: 'always' });
    self.isAggregatedJob = ko.observable({});
    self.isAggregatedJob((sessionStorage.isAggregated != undefined && sessionStorage.isAggregated != null && sessionStorage.isAggregated == "true") ? true : false);
    self.combinedInvOptions = [{ 'invOptionText': 'On', 'invOptionValue': "on" }, { 'invOptionText': 'Off', 'invOptionValue': "off" }];
    self.jobInfo = ko.observable({});
    self.aggregatedPaymentInfoList = ko.observableArray([]);
    self.isPayInAggregate = ko.observable('false');
    $.each(gServiceJobData, function (key1, val1) {
        key1 = key1.toLowerCase();
        switch (key1) {
            case "jobinfo":
                self.jobInfo(val1);
                break;
            case "aggregatedjobs":
                $.each(val1, function (agg_jobs_key, agg_jobs_val) {
                    self.jobs.push(new aggregatedJobsInfoVm(agg_jobs_val));
                });
                break;
            case "aggregateinfo":
                self.aggregatedPaymentInfoList.push(new nonAggregateJobsInfoVm(val1));
                break;
            case "paymentinfolist":
                self.jobs.push(new nonAggregateJobsInfoVm(gServiceJobData));
                break;
            default:
                break;
        }
    });

    self.addPostageOrInvoice = function (data, event) {
        var ele = event.target || event.currentTarget;
        var root_context = ko.contextFor(ele).$root;
        var type = $(ele).attr('data-addType');
        var pmt_info_lst = data.paymentInfoList;


        if (type == 'p') {
            var has_found = false;
            var target_obj;
            var target_index;
            var target_obj = ko.utils.arrayFirst(pmt_info_lst(), function (obj, index) {
                target_index = index;
                return (!obj.hasOwnProperty('postageReceivedPretty') && !obj.hasOwnProperty('postageCheck') && !obj.hasOwnProperty('postageDate')); // <-- is this the desired seat?
            });
            if (target_obj) {
                has_found = true;
                target_obj["postageReceivedPretty"] = ko.observable("");
                target_obj["postageCheck"] = ko.observable("");
                target_obj["postageDate"] = ko.observable("");
                data.paymentInfoList.splice(target_index, 1);
                data.paymentInfoList.splice(target_index, 0, target_obj);
            }
            if (!has_found) {
                var temp_post_inv = { 'postageReceivedPretty': '', 'postageCheck': '','postageDate' : '' }
                data.paymentInfoList.push(new paymentInfoVm(temp_post_inv));
            }
        }
        else {
            var has_found = false;
            var target_obj;
            var target_index;
            var target_obj = ko.utils.arrayFirst(pmt_info_lst(), function (obj, index) {
                target_index = index;
                return (!obj.hasOwnProperty('billingReceivedPretty') && !obj.hasOwnProperty('billingCheck') && !obj.hasOwnProperty('billingDate')); // <-- is this the desired seat?
            });
            if (target_obj) {
                has_found = true;
                target_obj["billingReceivedPretty"] = ko.observable("");
                target_obj["billingCheck"] = ko.observable("");
                target_obj["billingDate"] = ko.observable("");
                data.paymentInfoList.splice(target_index, 1);
                data.paymentInfoList.splice(target_index, 0, target_obj);
            }
            if (!has_found) {
                var temp_post_inv = { 'billingReceivedPretty': '', 'billingCheck': '', 'billingDate': '' }
                data.paymentInfoList.push(new paymentInfoVm(temp_post_inv));
            }
        }
        if (root_context.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
        $('input[data-fieldType="dateRec"]').datepicker();
    };
    self.combineUncombineInv = function (data, event) {
        var ele = event.target || event.currentTarget;
        var root_context = ko.contextFor(ele).$root;
        if (ele.value == "off") {

        }
        else {
            if (data.combinedInvInfoList().length == 0) {
                var temp_info = {};
                temp_info["receivedPretty"] = ko.observable("");
                temp_info["receivedCheck"] = ko.observable("");
                temp_info["receivedDate"] = ko.observable("");
                data.combinedInvInfoList.push(temp_info);
            }
        }
        if (root_context.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
        $('input[data-fieldType="dateRec"]').datepicker();
        // $('div').trigger('create');
    };
    self.addPayments = function (data, event) {
        var temp_info = {};
        var ele = event.target || event.currentTarget;
        var root_context = ko.contextFor(ele).$root;
        temp_info["receivedPretty"] = ko.observable("");
        temp_info["receivedCheck"] = ko.observable("");
        temp_info["receivedDate"] = ko.observable("");
        data.combinedInvInfoList.push(temp_info);
        if (root_context.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
    };
    self.makePayInAggregate = function (data, event) {
        var ele = event.target || event.currentTarget;
        if ($(ele).is(':checked'))
            data.isPayInAggregate('true');
        else
            data.isPayInAggregate('false');
        if (data.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
        $('input[data-fieldType="dateRec"]').datepicker();
    };

    self.saveAggregateInvoiceData = function () {
        if (!self.validateInvoice()) return false;
    };
    self.validateInvoice = function () {
        var msg = "";
        var invoice_data = $('input[type="text"][data-fieldType=ic],[data-fieldType=ia],[data-fieldType=a],[data-fieldType=c],[data-fieldType=dateRec]');
        var regNumbers = /^[0-9]*$/;
        var regCurrency = /^\$?[0-9]+(\.[0-9][0-9])?$/;// /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/; //
        var job_desc_regex = /^[ A-Za-z0-9,._-]*$/;
        var type = "";
        var count_a = 0;
        var count_va = 0;
        var count_iav=0
        var count_vc=0
        var count_icv=0
        var count_ia = 0;
        var count_c = 0;
        var count_ic = 0;
        var count_dt = 0;
        $.each(invoice_data, function (key, val) {
            type = $(val).attr('data-fieldType');
            if (type == "a" && count_a < 1) {

                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Postage Amount";
                    else
                        msg = "Please enter Postage Amount";
                    if (msg != "" && count_a < 1) {
                        count_a++;
                    }
                }
            }
            if (type == "a" && count_va < 1) {
                if ($(val).val() != "" && !regCurrency.test($(val).val())) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter valid Postage Amount";
                    else
                        msg = "Please enter valid Postage Amount";
                    if (msg != "" && count_va < 1) {
                        count_va++;
                    }
                }
            }
            if (type == "c" && count_c < 1) {
                if ($(val).val() == "")  {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Postage Check Number";
                    else
                        msg = "Please enter Postage Check Number";
                    if (msg != "" && count_c < 1) {
                        count_c++;
                    }
                }
            }
            if (type == "c" && count_vc < 1) {
                if ($(val).val() != "" && !regNumbers.test($(val).val())) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter valid Postage Check Number";
                    else
                        msg = "Please enter valid Postage Check Number";
                    if (msg != "" && count_vc < 1) {
                        count_vc++;
                    }
                }
            }
            if (type == "ia" && count_ia < 1) {
                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Invoice Amount";
                    else
                        msg = "Please enter Invoice Amount";
                    if (msg != "" && count_ia < 1) {
                        count_ia++;
                    }
                }
            }
            if (type == "ia" && count_iav < 1) {
                if ($(val).val() != "" && !regCurrency.test($(val).val())) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter valid Invoice Amount";
                    else
                        msg = "Please enter valid Invoice Amount";
                    if (msg != "" && count_iav < 1) {
                        count_iav++;
                    }
                }
            }
            if (type == "ic" && count_ic < 1) {
                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Invoice Check Number";
                    else
                        msg = "Please enter Invoice Check Number";
                    if (msg != "" && count_ic < 1) {
                        count_ic++;
                    }
                }
            }
            if (type == "ic" && count_icv < 1) {
                if ($(val).val() != "" && !regNumbers.test($(val).val())) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter valid Invoice Check Number";
                    else
                        msg = "Please enter valid Postag Invoice Number";
                    if (msg != "" && count_icv < 1) {
                        count_icv++;
                    }
                }
            }
            if (type == "dateRec" && count_dt < 1) {
                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Date";
                    else
                        msg = "Please enter Date";
                    if (msg != "" && count_dt < 1) {
                        count_dt++;
                    }
                }
            }
        });
        if ($('#txtNotes').val() != "" && !job_desc_regex.test($('#txtNotes').val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += '<br />Please enter valid Notes.The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';
            else
                msg = 'Please enter valid Notes.The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close')");
            return false;
        }
        return true;

    };
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if ((type == "joblookup")) {
        sessionStorage.removeItem('isAggregated');
        window.location.href = " ../adminFinance/financeJobLookUp.html";
    }
};

