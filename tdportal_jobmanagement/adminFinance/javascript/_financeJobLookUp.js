﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gJobsListServiceUrl = "JSON/_jobsList.JSON";
var gFacilityServiceUrl = "../JSON/_facility.JSON";
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var gServiceAdminFinanceData;
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$.getJSON(gFacilityServiceUrl, function (result) {
    primaryFacilityData = result.facility;
});
$('#_financeJobLookUp').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_financeJobLookUp');
    createConfirmMessage("_financeJobLookUp");

    getCORS(serviceURLDomain + "api/Customer/", null, function (data) {
        companyData = data;
        $('#companyTemplate').tmpl(companyData).appendTo('#ddlCustomer');
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
    //createOrderConfirmMessage("_userProfile");
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    var post_json = {};
    post_json = {
        "active": "0",
        "customerNumber": jobCustomerNumber,
        "facility": sessionStorage.facilityId
    };
    $.getJSON(gJobsListServiceUrl, function (data) {
        gServiceAdminFinanceData = data;
    });

});

$(document).on('pageshow', '#_financeJobLookUp', function (event) {
    window.setTimeout(function bindUsersInfo() {
        $('#facilityTemplate').tmpl(primaryFacilityData).appendTo('#ddlFacility');
        $('#ddlCustomer').val(jobCustomerNumber).selectmenu('refresh');
        $('#ddlFacility').val(sessionStorage.facilityId).selectmenu('refresh');
        if (appPrivileges.customerNumber != "1") {
            $('#ddlCustomer').attr('disabled', 'disabled');
            $('#ddlFacility').attr('disabled', 'disabled');
        }
        pageObj = new loadJobsInfo();
        ko.applyBindings(pageObj);
        window.setTimeout(function () {
            $('#ulAdminUserData').listview('refresh');
            $('#ulMultipleJobsInvoice').listview('refresh');
        }, 50);
    }, 1000);
    if ($('#chkRemoveAggregateJobs').is(':checked')) {
        $('#btnAggregateJobs').removeClass('ui-disabled');
        $('#btnAggregateJobs')[0].disabled = false;
    }
    else {
        $('#btnAggregateJobs').addClass('ui-disabled');
        $('#btnAggregateJobs')[0].disabled = true;
    }

});

var jobInfo = function (job_desc, job_attrs, job_number, mail_date, in_home, job_customer, isAggregate) {
    var self = this;
    self.jobDesc = ko.observable({});
    self.jobDesc(job_desc);
    self.jobAttrs = ko.observable({});
    self.jobAttrs(job_attrs);
    self.jobNumber = ko.observable(job_number);
    self.mailDate = ko.observable(mail_date);
    self.inHomeDate = ko.observable(in_home);
    self.jobCustomerNumber = ko.observable(job_customer);
    self.isAggregate = ko.observable(JSON.parse(isAggregate) ? 'k' : 'c');
};

var loadJobsInfo = function () {
    var self = this;
    self.displayJobsInfoList = ko.observableArray([]);
    self.displayMultipleJobInvoice = ko.observableArray([]);
    //    var post_json = {};
    //    post_json = {
    //        "active": "0",
    //        "customerNumber": CW_CUSTOMER_NUMBER,
    //        "facility": "1"
    //    };
    //    //$.getJSON(gServiceAdminUserUrl, function (data) {
    //    postCORS(serviceURLDomain + "api/Membership_getUserList", JSON.stringify(post_json), function (data) {
    var temp_data = {};
    $.each(gServiceAdminFinanceData, function (key, val) {
        //temp_data = {};
        //ko.mapping.fromJS(val, {}, temp_data);
        //self.diplayAdminUserData.push(temp_data);
        var job_attrs = "";
        var counter = 0;
        var attr_val = "";
        var is_aggregate = val.isAggregate;
        $.each(val, function (key1, val1) {
            if (key1.toLowerCase() != "job description" && key1.toLowerCase() != "customer number" && key1.toLowerCase() != "isaggregate") {
                if (job_attrs == "" && counter == 0)
                    job_attrs = "<p>";

                if (key1.toLowerCase() == "job number")
                    attr_val = "TD# :" + val1;
                else
                    attr_val = key1 + ":" + val1;

                job_attrs += (job_attrs != "" && counter > 0) ? (" | " + attr_val) : (attr_val);
                counter = ((JSON.parse(is_aggregate) && counter >= 1) || (counter >= 2)) ? 0 : counter + 1;
                //counter = (counter >= 2) ? 0 : counter + 1;
                job_attrs += (job_attrs != "" && counter == 0) ? "<br />" : "";
            }
        });
        job_attrs += "</p>";
        self.displayJobsInfoList.push(new jobInfo(val["Job Description"], job_attrs, val["Job Number"], val["Mail"], val["In Home"], val["Customer Number"], val["isAggregate"]));
        job_attrs = "";
        counter
    });
    //    },
    //     function (response_error) {
    //         if (response_error.status == 0) {
    //             $('#alertmsg').text("Unknown Error: Contact a Site Administrator");
    //             $('#popupDialog').popup('open');
    //             $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    //         }
    //         else {
    //             $('#alertmsg').text(response_error.responseText);
    //             $('#popupDialog').popup('open');
    //             $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    //         }
    //     });



    self.jobClick = function (data) {
        if ($('#chkRemoveAggregateJobs').is(':checked')) {
            sessionStorage.isAggregated = true;
        }
        window.location.href = " ../adminFinance/jobFinanceInfo.html";
    };
    self.searchClick = function (data) {
        if (self.validateJobData()) {
            var post_json = {};
            post_json = {
                "customerNumber": $('#ddlCustomer').val(),
                "facility": $('#ddlFacility').val(),
                "filters": {},
                "dateSearch": {},
                "searchTerm": {}
            };
            if ($('#txtJobNumber').val() != "")
                post_json.searchTerm["jobNumber"] = $('#txtJobNumber').val();

            if ($('#txtJobDesc').val() != "")
                post_json.searchTerm["jobDescription"] = $('#txtJobDesc').val();

            if ($('#fromDate').val() != "" && $('#toDate').val()) {
                post_json.dateSearch["fromDate"] = $('#fromDate').val();
                post_json.dateSearch["toDate"] = $('#toDate').val();
            }
            var chk_filter_selected = $('input[type=checkbox]:checked');
            if (chk_filter_selected.length > 0) {
                $.each(chk_filter_selected, function (key, val) {
                    if (val.value == "openPostageInv")
                        post_json.filters["openPostageInvoice"] = true;
                    else if (val.value == "openAccRec")
                        post_json.filters["openAccountsReceivable"] = true;
                    else if (val.value == "notInvoiced")
                        post_json.filters["havNotBeenInvoiced"] = true;
                });
            }
            //postCORS(serviceURLDomain + "api/Membership_getUserList", JSON.stringify(post_json), function (data) {
            //    gServiceAdminUserData = data;
            //    var temp_data = {};
            //    self.diplayAdminUserData([]);
            //    $.each(gServiceAdminUserData, function (key, val) {
            //        temp_data = {};
            //        ko.mapping.fromJS(val, {}, temp_data);
            //        self.diplayAdminUserData.push(temp_data);
            //    });
            //    $('#ulAdminUserData').listview('refresh');
            //},
            //function (response_error) {
            //    showErrorResponseText(response_error, false);
            //    //$('#alertmsg').text("Unknown Error: Contact a Site Administrator");
            //    //$('#popupDialog').popup('open');
            //    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
            //});

        }
        if ($('#chkRemoveAggregateJobs').is(':checked')) {
            $('#btnAggregateJobs').removeClass('ui-disabled');
            $('#btnAggregateJobs')[0].disabled = false;
        }
        else {
            $('#btnAggregateJobs').addClass('ui-disabled');
            $('#btnAggregateJobs')[0].disabled = true;
        }
    };
    self.cancelClick = function () {
        window.location.href = "../index.htm";
    };
    self.clearFilter = function () {
        $('#ddlFilterMilestones').val('-1').selectmenu('refresh');
        $('#ddlFilterRegions').val('-1').selectmenu('refresh');
        $('#ddlFilterDepartments').val('-1').selectmenu('refresh');
        $('#ddlFilterUsers').val('-1').selectmenu('refresh');
        $('#ulSelectedUsers').empty();
        $('#dtpStart').val('');
        $('#dtpEnd').val('');
        $('#ddlFilterRegions').val(sessionStorage.facilityId).selectmenu('refresh').trigger('change');
        if (($('#dtpStart') || $('#dtpEnd')).hasClass('ui-input-text')) {
            $('#dtpStart')[0].disabled = true;
            $('#dtpEnd')[0].disabled = true;
            $('#dtpStart').css('opacity', '0.7');
            $('#dtpEnd').css('opacity', '0.7');
        }
        $("input[id$='dtpStart']").datepicker("option", "maxDate", null);
        $("input[id$='dtpEnd']").datepicker("option", "minDate", null);
        if (flag)
            $('#btnFiltersSubmit').trigger('click');
    };
    self.showHomePage = function (type) {
        if (type == "home") {
            window.location.href = "../index.htm";
        }
        else {
            window.location.href = " ../adminFinance/financeJobLookUp.html";
        }
    };
    self.validateJobData = function () {
        var msg = "";
        var job_desc_regex = /^[ A-Za-z0-9,._-]*$/;
        var job_desc_value = $('#txtJobDesc').val().trim();
        var job_num_regex = /^[0-9-]*$/;
        var job_num = $('#txtJobNumber').val()
        if (appPrivileges.customerNumber == "1") {
            if ($("#ddlCompany").val() == "") {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please select Company Name.";
                else
                    msg = "Please select Company Name.";
            }
            if ($("#ddlFacility").val() == "") {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please select Facility.";
                else
                    msg = "Please select Facility.";
            }
        }

        if (job_desc_value != "" && !job_desc_regex.test(job_desc_value)) {
            if (msg != undefined && msg != "" && msg != null)
                msg += '<br />Please enter a valid job description. The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';
            else
                msg = 'Please enter a valid job description. The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';
        }

        if (job_num != "" && !job_num_regex.test(job_num)) {
            if (msg != undefined && msg != "" && msg != null)
                msg += '<br />Please enter a valid job number.';
            else
                msg = 'Please enter a valid job number.';
        }

        var chk_filters = $('input[type=checkbox]:checked');

        var from_date = $('#fromDate').val();
        var to_date = $('#toDate').val();
        if (from_date != "" && to_date == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += '<br />Please enter To date.';
            else
                msg = 'Please enter To date.';
        }
        if (to_date != "" && from_date == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += '<br />Please enter From date.';
            else
                msg = 'Please enter From date.';
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $('#popupResetPassword').popup('close');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close')");
            return false;
        }
        return true;
    }

    self.validateSelectedJobs = function () {
        var ctrls = $('#ulMultipleJobsInvoice input[type=checkbox]');
        var cust_no = "";
        var is_multiple = false;
        $.each(ctrls, function (key, val) {
            if ($(val).attr('value') != undefined && $(val).attr('value') != null && $(val).attr('value') != "" && cust_no != "" && cust_no != $(val).attr('value')) {
                is_multiple = true;
                return false;
            }
            cust_no = $(val).attr('value');
        });
        return is_multiple;
    };

    self.aggregateJobsClick = function () {
        if (self.validateSelectedJobs()) {
            $('#alertmsg').html('All the selected jobs should belong to the same customer in order to create aggregation');
            $('#alertmsg').css('text-align', 'left');
            // $('#popAggregateJobs').popup('close');
            $('#popupDialog').popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popAggregateJobs').popup('open');");
            //$("#okBut").attr("onclick", "$(\'#popupDialog\').popup(\'close\');$(\'#popAggregateJobs\').popup(\'open\')");
            return false;
        }
        else {
            window.setTimeout(function () {
                $("#btnAggregateJobs").attr("onclick", "$('#popAggregateJobs').popup('open')");
            }, 500);
            $('#popAggregateJobs').popup('open')
        }
    };

    self.aggregateOptionValueChanged = function () {
        var chk_checked = $('#ulMultipleJobsInvoice input[type="checkbox"]:checked').length;
        var chk_unchecked = $('#ulMultipleJobsInvoice input[type="checkbox"]').length;
        if (chk_checked < chk_unchecked) {
            if ($('#chkSelectAll').is(':checked')) {
                $('#chkSelectAll').attr('checked', false).checkboxradio('refresh');
            }
        }
        else {
            $('#chkSelectAll').attr('checked', true).checkboxradio('refresh');
        }
    };
    self.selectAllAggregateJobs = function () {
        if ($('#chkSelectAll').is(':checked')) {
            $('#ulMultipleJobsInvoice input[type="checkbox"]').each(function () {
                this.checked = true;
            });
        }
        else {
            $('#ulMultipleJobsInvoice input[type="checkbox"]').each(function () {
                this.checked = false;
            });
        }
    };
    self.onchangeRemoveAggregate = function () {
        if ($('#chkRemoveAggregateJobs').not(':checked')) {
            $('#btnAggregateJobs').addClass('ui-disabled');
            $('#btnAggregateJobs')[0].disabled = true;
        }

    };
};


