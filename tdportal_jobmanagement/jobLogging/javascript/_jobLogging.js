﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
//var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
//var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/153009/403745';
var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
var gLogDetailsUrl = 'JSON/_logDetails.JSON';
var pageObj;
//******************** Global Variables End **************************


//******************** Page Load Events Start **************************
$('#_jobLogging').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    pageObj = new logDetails();

    displayNavLinks();
    displayMessage('_jobLogging');

    $('#jobListInfo').css('display', 'none');
    pageObj.getLoggingData();

    //test for mobility...
    //loadMobility();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
});

$('#_jobLogging').live('pageshow', function (event) {
    //window.setTimeout(function makeDelayLoad() {
        pageObj.loadLoggingInfo();
        persistNavPanelState();
    //}, 700);
});

//******************** Page Load Events End **************************
//******************** Public Functions Start **************************
var logDetails = function () {
    var self = this;
    self.gLogginData = [];
    self.gLogDetailsData = [];
    self.gOutputData = [];
    self.logging = ko.observableArray(self.gLogginData);
    self.selectedItems = ko.observable("1");
    self.listJobDetails = ko.observableArray([]);
    //This function will display details info for selected logging
    self.setJobDetailsInfo = function (event_id, current) {
        self.listJobDetails([]);
        getCORS(gLogListUrl + '/' + event_id + '/0/' + current, null, function (data) {
            $.each(data, function (a, b) {
                for (i = 0; i < b.length; i++) {
                    self.listJobDetails.push(b[i]);
                }
            });
            $('#jobListInfo').css('display', 'block');
            $("div ul").each(function (I) {
                $(this).listview();
            });
            $("#jobListInfo").collapsibleset('refresh');

            $('#jobListInfo').trigger('create');
            $('.ui-page').trigger('create').listview().listview("refresh");

        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
        if (sessionStorage.jobSetupOutput != undefined) {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            getBubbleCounts(self.gOutputData);
        }
    }

    //This function will execute when logging dropdown changed
    self.loggingChanged = function (event, data) {
        var search_data;
        self.setJobDetailsInfo(self.selectedItems()[0], 0);
    }

    //This function will provide logging info
    self.getLoggingData = function () {
        getCORS(gLogListUrl + '/0/1/0', null, function (data) {
            $.each(data, function (a, b) {
                self.gLogginData.push(b[0]);
            });
        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
    }

    //This function will load default logging information
    self.loadLoggingInfo = function () {
        var logging = [];
        self.setJobDetailsInfo(0, 1);
        ko.applyBindings(pageObj);
        $('#jobListInfo').css('display', 'block');
        $("div ul").each(function (I) {
            $(this).listview();
        });
        $("#jobListInfo").collapsibleset('refresh');

        $('#jobListInfo').trigger('create');
        $('.ui-page').trigger('create').listview().listview("refresh");

    }

}
//******************** Public Functions End **************************