﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gCompanyInfoServiceUrl = "../jobCompanyInfo/JSON/_companyInfo.JSON";
var gServiceCompanyData;
var gOutputData;
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_variableCompanyInfo').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_variableCompanyInfo');
    createConfirmMessage("_variableCompanyInfo");
    getCompanyInfo();
    displayNavLinks();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
});

$(document).on('pageshow', '#_variableCompanyInfo', function (event) {
    window.setTimeout(function () {
        pageObj = new loadCompanyInfo();
        ko.applyBindings(pageObj);
        $('input[type=checkbox]').trigger('create');
        $('div').trigger('create');
        $('#navbar').trigger('create');
        $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
        $('#chkReplicate').checkboxradio('refresh');
        $('#ddlLocations').selectmenu('refresh');
        pageObj.getSelectedInfo();
    }, 500);

    persistNavPanelState();
});

var getCompanyInfo = function () {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if (gOutputData.companyInfoAction != undefined && gOutputData.companyInfoAction != null && gOutputData.companyInfoAction != "") {
            gServiceCompanyData = gOutputData.companyInfoAction;
        }
    }
    else if (sessionStorage.templateSetupConfigJSON != undefined && sessionStorage.templateSetupConfigJSON != null && sessionStorage.templateSetupConfigJSON != "") {
        gServiceCompanyData = $.parseJSON(sessionStorage.templateSetupConfigJSON);
    } else {
        $.getJSON(gCompanyInfoServiceUrl, function (data) {
            gServiceCompanyData = data;
        });
    }
};

var locationVm = function (store_id, store_name, store_pk) {
    var self = this;
    self.locationId = store_pk;
    if (store_id != undefined && store_id != null && store_id != "")
        self.locationName = store_id + ' - ' + store_name;
    else
        self.locationName = store_name;
};

var fieldInfo = function (label, value, is_required, show_chkbox) {
    var self = this;
    self.label = ko.observable('');
    self.label(label);
    self.value = ko.observable('');
    self.value(value);
    self.isRequired = ko.observable('');
    self.isRequired(is_required);
};

var loadCompanyInfo = function () {
    var self = this;
    self.locationsList = ko.observableArray([]);
    self.varCompanyInfo = ko.observable({});
    self.prevVarCompanyInfo = ko.observable({});
    self.selectedInHomeStoreCompanyInfo = ko.observable({});
    self.selectedInhomeInfo = ko.observable({});
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.selectedLocation = ko.observable({});
    self.isFirstTime = ko.observable(true);
    self.selectedStore = ko.observable({});
    self.isReplicated = ko.observable();
    self.previousSelectedStore = ko.observable({});
    var temp_arr = [];
    var temp_arr1 = ko.observableArray([]);
    var temp_key = "";
    var index = 0;
    var temp_company_info = {};
    var show_chk = false;

    var company_info = {};

    $.each(gServiceCompanyData, function (key, val) {
        if (key.toLowerCase() != "isreplicated") {
            company_info[key] = {};
            $.each(val, function (key1, val1) {
                company_info[key][key1] = ko.observable({});
                $.each(val1, function (key2, val2) {

                    if (temp_key == "")
                        temp_key = key2;
                    if (temp_key != key2) {
                        temp_company_info[temp_key] = temp_arr;
                        temp_key = key2;
                        temp_arr = [];
                    }
                    if (temp_key == "companyInfo")
                        show_chk = true;
                    else
                        show_chk = false;
                    if (key2 != "name" && key2 != "isEnabled") {
                        $.each(val2, function (inner_key1, inner_val1) {
                            temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
                        });
                        if (index == (Object.keys(val1).length - 1)) {
                            temp_company_info[temp_key] = temp_arr;
                            temp_key = key2;
                            temp_arr = [];
                        };
                    }
                    index++;
                });
                if (Object.keys(temp_company_info).length > 0) {
                    company_info[key][key1](temp_company_info);
                    temp_company_info = {};
                }
            });
        }
        else {
            if (val == true)
                self.isReplicated(true);
            else
                self.isReplicated(false);
        }
    });

    if (Object.keys(company_info).length > 0) {
        self.varCompanyInfo(company_info);
        var temp = ko.toJS(company_info);
        self.prevVarCompanyInfo(temp);
        company_info = {};
    }

    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
            $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                if (key == 0) {
                    self.selectedLocation(val.pk);
                    self.selectedStore(val.pk);
                    self.previousSelectedStore(self.selectedStore());
                }
                self.locationsList.push(new locationVm(val.storeId, val.storeName, val.pk));
            });
        }
    }

    if (gOutputData != undefined && gOutputData != null) {
        var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
        var cnt = -1;
        $.each(gOutputData, function (key, val) {
            if (key.toLowerCase().indexOf('inhome') > -1) {
                var temp_list = {};
                temp_list[key] = val;
                self.inHomeDatesList.push({
                    "showCheckIcon": false,
                    'dateValue': val
                });
                cnt++;
                if (cnt == 0)
                    nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                else
                    nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
            }
        });
        nav_bar_items += '</ul></div>';
        self.currentInHome(self.inHomeDatesList()[0]);
    }
    self.confirmContinue = function (current_page, page_name, i_count, click_type) {
        var ctrls = $('#dvNavBar div ul li a.ui-icon-edit');
        if (ctrls.length > 0 && click_type=="onlycontinue") {
            var tmp_list = "";
            $.each(ctrls, function (key, val) {
                tmp_list += (tmp_list != "") ? (key < (ctrls.length - 1)) ? ', ' : ' and ' + $(this).attr('data-inHome') : $(this).attr('data-inHome');
            });

            $('#confirmMsg').html('You have not completed setup for ' + tmp_list + '. Do you want to stay on this page to continue setup or go to the next page?');
            $('#okButConfirm').text('Go To Next Page');
            $('#cancelButConfirm').text('Continue Setup');

            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#popupConfirmDialog').popup('close');
            });
            $('#okButConfirm').bind('click', function () {
                $('#okButConfirm').unbind('click');
                $('#popupConfirmDialog').popup('close');
                goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
                self.updateCompanyInfo(current_page, page_name, i_count, click_type);
            });
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        else {
            //goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
            return self.updateCompanyInfo(current_page, page_name, i_count, click_type);
        }
    };
    self.updateCompanyInfo = function (current_page, page_name, i_count, click_type) {
        self.validateForDataExistency();
        var is_valid = false;
        // Call validation function to validate the data in the fields.  If it returns true proceed further else stop further execution.
        var temp_json = ko.mapping.toJS(self.varCompanyInfo);
        gOutputData.companyInfoAction = temp_json;
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        if (click_type == "onlycontinue")
            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
        return true;
    };
    self.navBarItemClick = function (data, event) {
        self.validateForDataExistency();
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var ele = event.target || event.targetElement;
        $('#dvNavBar div ul li a').removeClass('ui-btn-active');
        $(ele).addClass('ui-btn-active');
        if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 1)) {
            self.selectedInHomeStoreCompanyInfo({});
        }
        self.currentInHome(ko.dataFor(ele));
        self.getSelectedInfo();
    }

    self.locationChanged = function (data, event) {
        self.validateForDataExistency();
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        var selected_store = $(ele).val();
        self.selectedStore(self.selectedLocation());
        self.getSelectedInfo();

        var curr_info = ko.toJS(self.varCompanyInfo());
        var prev_info = ko.toJS(self.prevVarCompanyInfo());
        var diff_between_json_objects = [];
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            $.each(prev_info, function (key2, val2) {
                if (key1 == key2) {
                    $.each(val1, function (key3, val3) {
                        var curr_temp1 = val3;
                        var prev_temp2 = val2[key3];
                        diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                        if (diff_between_json_objects.length != 0) {
                            is_diff_found = true;
                            return false;
                        }
                    });
                }
                if (is_diff_found) {
                    return false;
                }
            });
            if (is_diff_found) {
                return false;
            }
        });

        if (is_diff_found) {
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
        }
        else {
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
        }
    };

    self.getSelectedInfo = function () {
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var selected_inhome = self.currentInHome().dateValue;
        var is_found = false;
        self.selectedInHomeStoreCompanyInfo({});
        $.each(self.varCompanyInfo(), function (key, val) {
            if (key == selected_inhome) {
                self.selectedInhomeInfo(val);
                $.each(val, function (key1, val1) {
                    if (key1 == self.selectedStore()) {
                        self.selectedInHomeStoreCompanyInfo(val1());
                        is_found = true;
                        return false;
                    }
                });
                if (is_found)
                    return false;
            }
        });
        $('div').trigger('create');
        var temp = ko.toJS(self.varCompanyInfo());
        self.updateInHomeNavBarUI(temp);
    };

    self.updateInHomeNavBarUI = function (temp) {
        $.each(temp, function (key, val) {
            var is_valid = self.verifyData(val);
            if (is_valid) {
                $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
            }
            else {
                $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
            }
        });
    };

    self.replicateChanged = function (data, event) {
        var ele = event.target || event.targetElement;
        $('#confirmMsg').html('Replicating Setup Entries will overwrite any previous information entered.');
        $('#okButConfirm').text('Continue');

        var curr_info = ko.toJS(self.varCompanyInfo());
        var prev_info = ko.toJS(self.prevVarCompanyInfo());
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            $.each(prev_info, function (key2, val2) {
                if (key1 == key2) {
                    $.each(val1, function (key3, val3) {
                        var curr_temp1 = val3;
                        var prev_temp2 = val2[key3];
                        diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                        if (diff_between_json_objects.length != 0) {
                            is_diff_found = true;
                            return false;
                        }
                    });
                }
                if (is_diff_found) {
                    return false;
                }
            });
            if (is_diff_found) {
                return false;
            }
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.replicateData();
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        if ($(ele).is(':checked') && is_diff_found) {
            $('#popupConfirmDialog').popup('open');
        }
    };
    self.replicateData = function () {
        var selected_inhome = self.currentInHome().dateValue;
        $.each(self.selectedInhomeInfo(), function (key1, val1) {
            $.each(self.varCompanyInfo(), function (key2, val2) {
                if (selected_inhome != key2) {
                    val2[key1](val1());
                }
            });
        });
        self.varCompanyInfo().isReplicated(true);
        var temp = ko.toJS(self.varCompanyInfo());
        self.prevVarCompanyInfo(temp);
        $.each(temp, function (key, val) {
            if (key != "isReplicated") {
                var is_valid = self.verifyData(val);
                if (is_valid) {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
                }
                else {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
                }
            }
        });
    };

    self.validateForDataExistency = function () {
        var temp_info = ko.toJS(self.selectedInhomeInfo);
        var is_valid = self.verifyData(temp_info);
        var selected_inhome = self.currentInHome().dateValue;
        if (is_valid) {
            $('#dvNavBar div ul li a[data-inHome="' + selected_inhome + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
        }
        else {
            $('#dvNavBar div ul li a[data-inHome="' + selected_inhome + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
        }
    };

    self.verifyData = function (temp_info) {
        var is_data_found = false;
        $.each(temp_info, function (key1, val1) {
            $.each(val1, function (key2, val2) {
                if (key2.toLowerCase() == "companyinfo" || key2.toLowerCase() == "custominfo") {
                    $.each(val2, function (key3, val3) {
                        if (val3.value == "") {
                            is_data_found = false;
                            return false;
                        }
                        else {
                            is_data_found = true;
                        }
                    });
                    if (!is_data_found) {
                        return false;
                    }
                }

            });
            if (!is_data_found) {
                return false;
            }
        });
        return is_data_found;
    };

    self.showOrderedLocations = function () {
        $('#popUpLocations').popup('open');
    };

    self.manageLocationsPopup = function (data, event) {
        var ele = event.target || event.targetElement;
        if ($(ele).attr('data-btnType') == "submit") {
            self.previousSelectedStore(self.selectedStore());
        }
        else {
            self.selectedStore(self.previousSelectedStore());
            self.selectedLocation(self.previousSelectedStore())
        }
        $('#ddlLocations').selectmenu('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        var sel_location = $('#ddlLocations option:selected').text();
        header_text = header_text + ' (' + sel_location + ')';
        $('#navHeader').text(header_text);
        $('#popUpLocations').popup('close');
    };
    self.previewMailing = function () {
        getNextPageInOrder();
        var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    }
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if ((type == "template")) {
        window.location.href = " ../adminTemplateSetup/templateSetup.html";
    }
    else if ((type == "vcc")) {
        window.location.href = " ../adminTemplateSetup/variableCompanyInfo.html";
    }
};

