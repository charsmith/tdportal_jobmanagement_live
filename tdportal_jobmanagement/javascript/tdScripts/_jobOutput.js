﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gServiceUrl = '../JSON/_jobOutput.JSON';
var gData;
var gOutputData;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobOutput').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobOutput');
        if (sessionStorage.advancedOptionsCount == undefined) {
        sessionStorage.advancedOptionsCount = 7;
    }
    confirmMessage();
    createControls();
    $("div[data-role='header']").eq(0).find('a').eq(2).addClass('ui-btn-right');
    //test for mobility...
    loadMobility();
});

$(document).on('pageshow', '#_jobOutput', function (event) {
    if (sessionStorage.jsonOutPutLoadObject == undefined && sessionStorage.jsonOutPutLoadObject == undefined) {
        makeGData();
        makeGOutputData();
    }
    else {
        gData = jQuery.parseJSON(sessionStorage.jsonOutPutLoadObject);
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        populateDropDowns();
        populateDropDownsWithData();
    }
});
//******************** Page Load Events End **************************

//******************** Public Functions Start **************************
function makeGData() {
    $.getJSON(gServiceUrl, function (data) {
        gData = data;
        populateDropDowns();
        populateDropDownsWithData();
    });
}

function createControls() {
    $('#spnOutputFileMap').append("<select data-mini='true' id='fieldName' data-theme='b'></select>");
    $('#spnOverrideFileMapSignOff').append("<select data-mini='true' id='fieldName' data-theme='b'></select>");
    $('#spnOverrideFileMapSample').append("<select data-mini='true' id='fieldName' data-theme='b'></select>");
    $('#spnOutPutView').append("<select data-mini='true' id='fieldName' data-theme='b'></select>");
    $('#spnOutputDirection').append("<select data-mini='true' id='fieldName' data-theme='b'></select>");
    $('#dvAdditionalFields').append("<select data-mini='true' id='selectFactor0' data-theme='b'></select>");

    $('#spanPrintType').append("<select data-mini='true' id='ddlPrintType' data-theme='b' onchange='setDefaultValue(this);'></select>");
    addSelections();
}

function selectOption(ctrl) {
    var count = ctrl.id.charAt(ctrl.id.length - 1);
    var selected_text = $('#' + ctrl.id + " option:selected").val();
    if (selected_text.toLowerCase() == "custom") {
        $("#lblCustom" + count).show();
        $("#txtCustom" + count).show();
    }
    else {
        $("#lblCustom" + count).hide();
        $("#txtCustom" + count).hide();
    }

}

function setDefaultValue(ctrl) {
    var default_val1 = new Array("IMB", "OEL", "Custom", "First Name - Last Name", "Address Line 1", "Address Line 2", "City, State Zip / Zip + 4");
    var default_val2 = new Array("IMB", "Sort Line", "OEL", "First Name - Last Name", "Address Line 1", "Address Line 2", "City, State Zip / Zip + 4");

    var count = ctrl.id.charAt(ctrl.id.length - 1);
    var selected_text = $('#' + ctrl.id).val();

    for (i = 0; i <= sessionStorage.advancedOptionsCount; i++) {
        if (selected_text == 'Laser') {
            if (i <= default_val1.length) {
                $('#fieldName' + (i + 1)).val(default_val1[i]);
                $('#fieldName' + (i + 1)).selectmenu('refresh');
            }
        }
        else {
            if (i <= default_val2.length) {
                $('#fieldName' + (i + 1)).val(default_val2[i]);
                $('#fieldName' + (i + 1)).selectmenu('refresh');
            }
        }
    }
    $('#addressList').listview('refresh');
    $('.ui-page').trigger('create');
}

function addSelections() {
    var count;
    var li = "";
    for (i = 0; i <= sessionStorage.advancedOptionsCount; i++) {
        li = '<li data-role="fieldcontain" id="liAdderLine' + (i + 1) + '"><label for="spanAdderLine' + (i + 1) + '" class="select label">Addr Line ' + (i + 1) + ':</label><span id="spanAdderLine' + (i + 1) + '"><select data-mini="true" id="fieldName' + (i + 1) + '" data-theme="b" onchange="selectOption(this);"></select></span><br/><label for="custom" class="select label" id="lblCustom' + (i + 1) + '" style="display:none;">Custom:</label><input style="display:none;" type="text" name="custom" id="txtCustom' + (i + 1) + '" value="" /></li>';
        $('#addressList').append(li);
    }
}


function addAdderLine() {
    var count = $('#addressList').find('[id^=fieldName]').length;
    if (sessionStorage.advancedOptionsCount != undefined) {
        sessionStorage.advancedOptionsCount++;
    }
    li = '<li data-role="fieldcontain" id="liAdderLine' + (count + 1) + '"><label for="spanAdderLine' + (count + 1) + '" class="select label">Addr Line ' + (count + 1) + ':</label><span id="spanAdderLine' + (count + 1) + '"><select data-mini="true" id="fieldName' + (count + 1) + '" data-theme="b" onchange="selectOption(this);"></select></span><br/><label for="custom" class="select label" id="lblCustom' + (count + 1) + '" style="display:none;">Custom:</label><input style="display:none;" type="text" name="custom" id="txtCustom' + (count + 1) + '" value="" /></li>';
    $('#addressList').append(li);

    var addr_line_options = jQuery.grep(gData, function (a, b) {
        return a.list == "outputAddrLines";
    });
    fillControls(addr_line_options[0].items, 'spanAdderLine' + (count + 1));

    $('#addressList').listview('refresh');
    $('.ui-page').trigger('create');
}

function removeAdderLine() {
    if (sessionStorage.advancedOptionsCount != undefined) {
        sessionStorage.advancedOptionsCount--;
    }
    var count = $('#addressList').find('[id^=fieldName]').length;
    $('#addressList').find('[id=liAdderLine' + count +']').remove();
    $('#addressList').listview("refresh");
    $('.ui-page').trigger('create');
}

function makeGOutputData() {
    gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
    if (gOutputData != null) {
        var file_data = (gOutputData.uploadList != undefined) ? gOutputData.uploadList : 0;
        if ($("#fileCount")) {
            $("#fileCount").text("0");
            $.each(file_data, function (key, val) {
                if (val.fileName != "") {
                    $("#fileCount").text(file_data.length);
                }
            });
        }
    }
}

function makeVisibleDropDown(status, lbl_ctrl, ddl_ctrl) {
    if (status.checked == true) {
        $('#' + lbl_ctrl).css('visibility', 'visible');
        $('#' + ddl_ctrl).css('visibility', 'visible');
    }
    else {
        $('#' + lbl_ctrl).css('visibility', 'hidden');
        $('#' + ddl_ctrl).css('visibility', 'hidden');
    }

}

function addFields() {
    var current_index = $('#dvAdditionalFields').find('select[id^=selectFactor]').length;
    gData = jQuery.parseJSON(sessionStorage.jsonOutPutLoadObject);
    var json_data = [];
    json_data = jQuery.grep(gData, function (obj) {
        return obj.list === "sofFieldsList";
    });
    var control_to_add = '<div class=ui-select><div class="ui-btn ui-btn-up-b ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-right" data-theme="a" data-iconpos="right" data-icon="arrow-d" data-mini="true" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperEls="span"><span class="ui-btn-inner ui-btn-corner-all">';
    control_to_add += '<span class=ui-btn-text ><span>Select</span></span><span class="ui-icon ui-icon-arrow-d ui-icon-shadow">&nbsp;</span></span>';
    control_to_add += '<select name="fieldName" id="selectFactor' + (parseInt(current_index) + 1) + '" data-mini="true" onchange="assignColumnData(this)">';
    $.each(json_data[0].items, function (i, option) {
        if (option.value != 'select') {
            control_to_add += '<option value="' + option.value + '">' + option.name + '</option>';
        }
    });
    control_to_add += '</select></span></div></div>';
    $('#dvAdditionalFields').append(control_to_add);
}

function assignColumnData(ctrl) {
    var current_control = $('#' + ctrl.id);
    var selected_text = current_control.get(0).options[$('#' + ctrl.id).get(0).selectedIndex].value;
    var is_selected = false;
    var ddl_factor = $('#dvAdditionalFields').find('select[id^=selectFactor]');
    ddl_factor.each(function (index) {
        if ($(this).val() == selected_text && $(this).attr('id') != ctrl.id) {
            $('#alertmsg').text('This Field is already selected.');
            $('#popupDialog').popup('open');
            is_selected = true;
            return true;
        }
    });
    //to set the selected value in the drop down.
    if (!is_selected) {
        current_control.closest('.ui-select').find('.ui-btn-text').find('span').html(current_control.get(0).options[$('#' + ctrl.id).get(0).selectedIndex].text);
    }
}

function clearFields() {
    var additional_fields = $('#dvAdditionalFields').find('select[id^=selectFactor]');
    if (additional_fields.length > 1) {
        additional_fields.splice(1, additional_fields.length - 1);

        var control_to_add = '<div class=ui-select><div class="ui-btn ui-btn-up-b ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-right" data-theme="a" data-iconpos="right" data-icon="arrow-d" data-mini="true" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperEls="span"><span class="ui-btn-inner ui-btn-corner-all">';
        control_to_add += '<span class=ui-btn-text ><span>Select</span></span><span class="ui-icon ui-icon-arrow-d ui-icon-shadow">&nbsp;</span></span>';
        control_to_add += "<select data-mini='true' id='selectFactor0' data-theme='b' data-mini='true' onchange='assignColumnData(this)'>" + additional_fields[0].innerHTML + "</select>";
        control_to_add += '</span></div></div>';

        $('#dvAdditionalFields').html(control_to_add);
    }
}

function populateFieldNameDropDown(is_extended_file_map) {
    var serviceUrl = is_extended_file_map ? '../JSON/_listSource.JSON' : '../JSON/_listSourceBasicSet.JSON';
    $('#dvAdditionalFields').find('select').get(0).options.length = 0;
    var ddl_control = $('#dvAdditionalFields').find('select[id^=selectFactor]');
    $.getJSON(serviceUrl, function (data) {
        if (typeof (data) != 'undefined') {
            var json_data = [];
            json_data = jQuery.grep(gData, function (obj) {
                return obj.list === "sofFieldsList";
            });
            json_data[0].items = data[0].FieldName;
            sessionStorage.jsonOutPutLoadObject = JSON.stringify(gData);
            ddl_control.each(function (index) {
                $(this).get(0).options.length = 0;
                $.each(data[0].FieldName, function (i, option) {
                    ddl_control.each(function (index) {
                        $(this).append($('<option/>').attr("value", option.value).text(option.name));
                    });

                });
            });
            $('#dvAdditionalFields').find('select').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html('state');
            $('#dvAdditionalFields').find('select').eq(0).val('state');
            ddl_control.each(function (index) {
                $(this).closest('.ui-select').find('.ui-btn-text').find('span').html('Select');
            });
        }
    });
}

function extendedMapOnOff(ctrl_slider_extended_map) {
    var is_file_map_on = $('#' + ctrl_slider_extended_map).val();
    if (is_file_map_on == 'off') {
        populateFieldNameDropDown(false);
    }
    else {
        populateFieldNameDropDown(true);
    }
}

function populateDropDowns() {
    $.each(gData, function (key, val) {
        switch (val.list) {
            case "outputFileMap":
                fillControls(val.items, 'spnOutputFileMap');
                fillControls(val.items, 'spnOverrideFileMapSignOff');
                fillControls(val.items, 'spnOverrideFileMapSample');
                break;
            case "outputView":
                fillControls(val.items, 'spnOutPutView');
                break;
            case "outputDirection":
                fillControls(val.items, 'spnOutputDirection');
                break;
            case "sofFieldsList":
                fillControls(val.items, 'dvAdditionalFields');
                //setting the default selected value
                $('#dvAdditionalFields').find('select#selectFactor0').eq(0).val("select");
                $('#dvAdditionalFields').find('select#selectFactor0').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html('Select');
                break;
            case "outputAddrLines":
                for (i = 0; i <= sessionStorage.advancedOptionsCount; i++) {
                    fillControls(val.items, 'spanAdderLine' + (i + 1));
                }
                $('#addressList').listview("refresh");
                $('.ui-page').trigger('create');
                break;
            case "outputPrintType":
                fillControls(val.items, 'spanPrintType');
                break;
        }
    });
    sessionStorage.jsonOutPutLoadObject = JSON.stringify(gData);
}

function populateDropDownsWithData() {
    if (gOutputData != null) {
        getUploadedFilesCount();
        $.each(gOutputData.outputAction, function (key, val) {
            switch (key) {
                case "prdFileMap":
                    fillControlsWithData(val, 'spnOutputFileMap', 'ddl');
                    fillControlsWithData(val, 'spnOverrideFileMapSignOff', 'chk');
                    fillControlsWithData(val, 'spnOverrideFileMapSample', 'chk');
                    break;
                case "view":
                    fillControlsWithData(val, 'spnOutPutView', 'ddl');
                    break;
                case "sofWantsSeeds":
                    if (val) {
                        $('#checkbox-1a').click();
                        $('#checkbox-1a').click();
                    }
                    break;
                case "direction":
                    fillControlsWithData(val, 'spnOutputDirection', 'ddl');
                    break;
                case "sofFileMapChkBox":
                    if (val) {
                        $('#chkOverrideFileMapSignOff').click();
                        $('#chkOverrideFileMapSignOff').click();
                    }
                    break;
                case "sofFileMap":
                    if (val != null && val != undefined && val != "" && val.toLowerCase() != "select") {
                        $('#chkOverrideFileMapSignOff').click();
                        $('#chkOverrideFileMapSignOff').click();
                        fillControlsWithData(val, 'spnOverrideFileMapSignOff', 'ddl');
                    }
                    else {
                        $('#chkOverrideFileMapSample').attr("checked", false).checkboxradio("refresh");
                    }
                    break;
                case "sofFactor":
                    $("#slider").val(val);
                    break;
                case "sofFieldsList":
                    var field_count = 1;
                    if (val.length > 0) {
                        $.each(val[0], function (fieldKey, fieldValue) {
                            if (field_count > 1)
                                addFields();
                            field_count++;
                            var ddlControl = $('#dvAdditionalFields').find('select[id^=selectFactor]').last();
                            if (ddlControl.length > 0) {
                                $.each(ddlControl[0], function (ddlKey, ddlValue) {
                                    if (fieldValue == $("#" + ddlControl[0].id + ' option')[ddlKey].value) {
                                        $('#' + ddlControl[0].id).eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html(ddlValue.text);
                                        $("#" + ddlControl[0].id + " option").eq(ddlKey).attr('selected', true);
                                    }
                                });
                            }
                        });
                    }
                    break;
                case "sasFileMapChkBox":
                    if (val) {
                        $('#chkOverrideFileMapSample').click();
                        $('#chkOverrideFileMapSample').click();
                    }
                    break;
                case "sasFileMap":
                    if (val != null && val != undefined && val != "" && val.toLowerCase() != "select") {
                        $('#chkOverrideFileMapSample').click();
                        $('#chkOverrideFileMapSample').click();
                        fillControlsWithData(val, 'spnOverrideFileMapSample', 'ddl');
                    }
                    else {
                        $('#chkOverrideFileMapSample').attr("checked", false).checkboxradio("refresh");
                    }
                    break;
                case "sasQuantity":
                    $("#name").val(val);
                    break;
                case "advancedOptions":
                    $.each(val[0], function (key, val) {
                        if (key.toLowerCase() === "printtype") {
                            $('#ddlPrintType').val(val);
                            $('#ddlPrintType').selectmenu('refresh');
                        }
                        else
                            $('#' + key).val(val);
                        $('#' + key).selectmenu('refresh');
                    });
                    break;
                case "printType":
                    fillControlsWithData(val, 'spanPrintType', 'ddl');
                case "adderLine1":
                    fillControlsWithData(val, 'spanAdderLine1', 'ddl');
                case "adderLine2":
                    fillControlsWithData(val, 'spanAdderLine2', 'ddl');
                case "adderLine3":
                    fillControlsWithData(val, 'spanAdderLine3', 'ddl');
                case "custom":
                    fillControlsWithData(val, 'spanAdderLine3', 'txt');
                case "adderLine4":
                    fillControlsWithData(val, 'spanAdderLine4', 'ddl');
                case "adderLine5":
                    fillControlsWithData(val, 'spanAdderLine5', 'ddl');
                case "adderLine6":
                    fillControlsWithData(val, 'spanAdderLine6', 'ddl');
                case "adderLine7":
                    fillControlsWithData(val, 'spanAdderLine7', 'ddl');
            }
        });
    }
    sessionStorage.jsonOutPutLoadObject = JSON.stringify(gData);
}

function updateSetupOutput() {
    var ret_value = false;
    if (gOutputData != null) {
        $.each(gOutputData, function (key, val) {
            if (key == "outputAction") {
                $.each(val, function (key_output, val_output) {
                    switch (key_output) {
                        case "prdFileMap":
                            val[key_output] = $("#spnOutputFileMap option:selected").val();
                            break;
                        case "view":
                            val[key_output] = $("#spnOutPutView option:selected").val();
                            break;
                        case "direction":
                            val[key_output] = $("#spnOutputDirection option:selected").val();
                            break;
                        case "sofWantsSeeds":
                            val[key_output] = $('#checkbox-1a').parents(0).find('label').eq(0).hasClass('ui-checkbox-on');
                            break;
                        case "sofFileMapChkBox":
                            val[key_output] = $("#chkOverrideFileMapSignOff").is(':checked');
                            break;
                        case "sofFileMap":
                            val[key_output] = $("#spnOverrideFileMapSignOff option:selected").val();
                            break;
                        case "sasFileMapChkBox":
                            val[key_output] = $("#chkOverrideFileMapSample").is(':checked');
                            break;
                        case "sasFileMap":
                            val[key_output] = $("#spnOverrideFileMapSample option:selected").val();
                            break;
                        case "sasQuantity":
                            val[key_output] = $("#name").val();
                            break;
                        case "sofFactor":
                            val[key_output] = $("#slider").val();
                            break;
                        case "sofFieldsList":
                            var sof_fields_list = {};
                            var ddlFactor = $('#dvAdditionalFields').find('select[id^=selectFactor]');
                            var field_name = "";
                            var selected_fields = {};
                            val_output = [];
                            ddlFactor.each(function (index) {
                                field_name = "field" + (index + 1);
                                selected_fields[field_name] = $(this).val();
                            });
                            val_output.push(selected_fields);
                            val[key_output] = val_output;
                            break;
                        case "advancedOptions":
                            var adv_options = {};
                            var ddlFieldsList = $('#addressList').find('[id^=fieldName]');
                            var field_name = "";
                            var selected_fields = {};
                            val_output = [];
                            selected_fields["printType"] = $('#ddlPrintType').val();
                            ddlFieldsList.each(function (index) {
                                field_name = "fieldName" + (index + 1);
                                selected_fields[field_name] = $(this).val();
                            });
                            val_output.push(selected_fields);
                            val[key_output] = val_output;
                            break;
                        case "printType":
                            val[key_output] = $("#spanPrintType option:selected").val();
                            break;
                        case "adderLine1":
                            val[key_output] = $("#spanAdderLine1 option:selected").val();
                            break;
                        case "adderLine2":
                            val[key_output] = $("#spanAdderLine2 option:selected").val();
                            break;
                        case "adderLine3":
                            val[key_output] = $("#spanAdderLine3 option:selected").val();
                            break;
                        case "custom":
                            val[key_output] = $("#custom").val();
                            break;
                        case "adderLine4":
                            val[key_output] = $("#spanAdderLine4 option:selected").val();
                            break;
                        case "adderLine5":
                            val[key_output] = $("#spanAdderLine5 option:selected").val();
                            break;
                        case "adderLine6":
                            val[key_output] = $("#spanAdderLine6 option:selected").val();
                            break;
                        case "adderLine7":
                            val[key_output] = $("#spanAdderLine7 option:selected").val();
                            break;
                    }
                });
                ret_value = true;
            }
        });

        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);

        var job_setup_json = jQuery.parseJSON(sessionStorage.jobSetupOutput);

        if (job_setup_json != null) {
            job_setup_json.outputAction = gOutputData.outputAction;
            sessionStorage.jobSetupOutput = JSON.stringify(job_setup_json);
        }
    }
    return ret_value;
}

function fillControls(items, ctrl) {
    $('#' + ctrl + ' option').remove();
    $.each(items, function (i, option) {
        $('#' + ctrl).find('select').eq(0).append($('<option/>').attr("value", option.value).text(option.name));
        if ((ctrl == 'spnOutPutView' || ctrl == 'spnOutputDirection') && option.name.indexOf('Default') > -1)
            $('#' + ctrl).find('select').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html(option.name);
        else if (option.value == "select")
            $('#' + ctrl).find('select').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html('Select');
    });
}

function fillCheckBoxWithData(item, id) {
    $('#' + id).parent().eq(0).find('label').eq(0).removeClass('ui-checkbox-off').addClass('ui-checkbox-on');
}

function fillControlsWithData(item, ctrl, type) {
    if (type == 'ddl') {
        $.each($("#" + ctrl + " option"), function (i, option) {
            if (item == $("#" + ctrl + ' option')[i].value) {
                $('#' + ctrl).find('select').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html($("#" + ctrl + ' option')[i].text);
                $("#" + ctrl + " option").eq(i).attr('selected', true);
            }
        });
    }

}

function callJobSubmit() {
    updateSetupOutput();
    document.location.href = 'jobSubmitJob.html';
}
function closePopup(popup_ctrl) {
    $("#" + popup_ctrl).popup("close");
    //makeGData();
}
//******************** Public Functions End **************************