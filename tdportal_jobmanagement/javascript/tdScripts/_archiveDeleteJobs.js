﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var gFacilityServiceUrl = "JSON/_facility.JSON";
var primaryFacilityData;
var companyData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$.getJSON(gFacilityServiceUrl, function (result) {
    primaryFacilityData = result.facility;
});
$('#_archiveDeleteJobs').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_archiveDeleteJobs');
    createConfirmMessage("_archiveDeleteJobs");
    getCORS(serviceURLDomain + "api/Customer/", null, function (data) {
        companyData = data;
        $('#companyTemplate').tmpl(companyData).appendTo('#ddlCustomer');
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    var post_json = {};

});

$(document).on('pageshow', '#_archiveDeleteJobs', function (event) {
    window.setTimeout(function bindUsersInfo() {
        $('#facilityTemplate').tmpl(primaryFacilityData).appendTo('#ddlFacility');
        $('#ddlFacility').val(sessionStorage.facilityId).selectmenu('refresh');
        $('#ddlCustomer').val(sessionStorage.customerNumber).selectmenu('refresh');
        if (appPrivileges.customerNumber != "1") {
            $('#ddlCustomer').attr('disabled', 'disabled');
            $('#ddlFacility').attr('disabled', 'disabled');
        }
    }, 1000);
});

function deleteJob() {
    if (!validateData()) return false;
    post_json = {
        "jobNumber": $('#txtJobNumber').val(),//'-15254',
        "customerNumber": $('#ddlCustomer').val(),
        "facilityId": $('#ddlFacility').val()
    };
    var gdelete_service_url = serviceURLDomain + "api/Job_delete";
    postCORS(gdelete_service_url, JSON.stringify(post_json), function (data) {
        $('#popupConfirmDialog').popup('close');
        var msg = "The following resources have been deleted for Job Number<b> " + data.jobNumber + '</b>';
        msg += '<ul>';
        $.each(data, function (key, val) {
            if (key.toLowerCase() != "jobnumber") {
                msg += '<li>' + val + '</li>';
            }
        });
        msg += '</ul>';
        $('#alertmsg').html(msg);
        $('#txtJobNumber').val('');
        if (appPrivileges.customerNumber == "1") {
            var cust_val = (sessionStorage.selectedCustomerNumber == undefined || sessionStorage.selectedCustomerNumber == null || sessionStorage.selectedCustomerNumber == "") ? sessionStorage.customerNumber : sessionStorage.selectedCustomerNumber;
            var facility_val = (sessionStorage.selectedFacilityId == undefined || sessionStorage.selectedFacilityId == null || sessionStorage.selectedFacilityId == "") ? sessionStorage.facilityId : sessionStorage.selectedFacilityId;
            $('#ddlCustomer').val(cust_val).selectmenu('refresh');
            $('#ddlFacility').val(facility_val).selectmenu('refresh');
        }
        window.setTimeout(function displayJobDeleteInfo() {
            $('#popupDialog').popup('open');
        }, 1000);
    },
   function (response_error) {
       showErrorResponseText(response_error, false);
       $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
   });
}

function popupDeleteWarning() {
    if (!validateData()) return false;
    var msg = "";
    //$('#confirmMsg').html("Are you sure you want to Delete this job? <br />This will delete all job instructions, associated databases and all site entries.");
    $('#confirmMsg').html("Deleting a job CANNOT be undone. Click 'Delete' to continue or 'Cancel' if you do not want to delete the job.'");
    $('#okButConfirm').text('Delete');
    $('#okButConfirm').attr('onclick', 'deleteJob();');
    $('#popupConfirmDialog').popup('open');
}

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "index.htm";
        if (sessionStorage.selectedCustomerNumber != undefined && sessionStorage.selectedCustomerNumber != null)
            sessionStorage.removeItem("selectedCustomerNumber");
        if (sessionStorage.selectedFacilityId != undefined && sessionStorage.selectedFacilityId != null)
            sessionStorage.removeItem("selectedFacilityId");
    }
    else {
        window.location.href = " ../userProfile/userProfile.html";
    }
}
function getCutomer(temp) {
    if (temp == "cust") {
        sessionStorage.selectedCustomerNumber = $('#ddlCustomer').val();
    }
    else if (temp == "facility") {
        sessionStorage.selectedFacilityId = $('#ddlFacility').val();
    }
}

function validateData() {
    var msg = "";
    // var regxCustFacility = /^[0-9]+$/;
    var regxJobNumber = /^[0-9 \-]+$/;  // /\-\d*$/;
    if ($("#txtJobNumber").val() == "") {
        msg += (msg != undefined && msg != "" && msg != null) ? "<br />Please enter Job Number" : "Please enter Job Number";
    }
    else if ($("#txtJobNumber").val() != "" && !regxJobNumber.test($("#txtJobNumber").val())) {
        msg += (msg != undefined && msg != "" && msg != null) ? "<br />Please enter valid Job Number" : "Please enter valid Job Number";
    }
    if (appPrivileges.customerNumber == "1") {
        if ($("#ddlCompany").val() == "") {
            msg += (msg != undefined && msg != "" && msg != null) ? "<br />Please select Company Name." : "Please select Company Name.";
        }
        if ($("#ddlFacility").val() == "") {
            msg += (msg != undefined && msg != "" && msg != null) ? "<br />Please select Facility." : "Please select Facility."
        }
    }
    if (msg != "") {
        $('#alertmsg').html(msg);
        $('#alertmsg').css('text-align', 'left');
        $("#popupDialog").popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close')");
        return false;
    }
    return true;
}