﻿jQuery.support.cors = true;
//******************** Global Variables Start **************************
//var gServiceUrl = serviceURLDomain + "api/Authorization";
var gServiceUrl = serviceURLDomain + "api/Authorization";
//var forgotPasswordUrl = serviceURLDomain + "api/forgottenPassword/";
var forgotPasswordUrl = serviceURLDomain + "api/Password/1/";
var resetPasswordUrl = serviceURLDomain + "api/Password/";
//var gServiceUrl = myProtocol + "campingworld.tribunedirect.com/api/Authorization";
var browserVersion = "";
var newLoginUrl = "JSON/_login.JSON";
var newLoginUrlData = {};
var urlString = unescape(window.location)
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
var cust_id = getParameterByName('q');

//******************** Page Load Events Start **************************

var cust_name = document.URL.substring(document.URL.indexOf('//') + 2, document.URL.indexOf('.'));

// var environments = ["tddev", "tddemo", "tdlive", "safeway", "lowes", "tdstage", "aag", "sa", "orlandohealth", "tribunepublishing", "10.180.32.96", "localhost", "sg-srv-td2"];
// $.each(environments, function (key, val) {
    // if (val.toLowerCase().indexOf(cust_name.toLowerCase()) > -1 || (document.URL.substring(document.URL.indexOf('//') + 2).split('.').length > 4)) {
        // cust_name = "";
    // }
// });
// if (cust_name == "")
    // cust_id = 1;


$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_login').live('pagebeforecreate', function (event) {
    var login_url = document.URL;
    displayNewLoginPage(login_url);
    displayMessage('_login');

    if (cust_name.toLowerCase().indexOf('mediaworks') > -1 || cust_id == DATAWORKS_CUSTOMER_NUMBER) {
        $('#okBut').attr('data-theme', 'i');
        $('#btnSend').attr('data-theme', 'i');
        $('#btnForgotPassCancel').attr('data-theme', 'i');
    }

    $('#popupDialog').attr("data-rel", "dialog");
    $('#popupDialog').attr("data-dismissible", "false");
    //test for mobility...
    //loadMobility();
    if (cust_name.toLowerCase().indexOf('kubota') > -1 || cust_id == KUBOTA_CUSTOMER_NUMBER)
        setCustomGizmoLogo(KUBOTA_CUSTOMER_NUMBER);
    else
        setGizmoLogo();

    browserVersion = fnGetBrowserVersion
    // if (cust_id == BBB_CUSTOMER_NUMBER) {
    $('#passwordForgotReset').css('display', 'none');
    // }
});

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_login', function (event) {
    if (sessionStorage.isEmailLogin == undefined && sessionStorage.isEmailLogin == null) {
        fnCheckBrowserVersion(); //checking the browser compatibility

        $(window).load(function () { // On load
            $('#col2').css('height', $(document).height() - 80);
        });
        $(window).resize(function () { // On resize
            $('#col2').css('height', $(window).height() - 80);
        });
    }
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function fnGetBrowserVersion() {
    var N = navigator.appName, ua = navigator.userAgent, tem;
    //var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    var M = ua.match(/(chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
    M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
    return M[1];
}
//******************** Public Functions Start **************************
$(document).ready(function () {
    $('#login').click(function () {
        if (fnCheckBrowserVersion() == false) return false; //checking the browser compatibility

        if ($('[name=uid_r]').val() == "" || $('[name=pwd_r]').val() == "") {
            $('#alertmsg').text('Please enter Username and password');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');");
        }
        else if (($('[name=uid_r]').val().toLowerCase().indexOf("user") != -1 && $('[name=uid_r]').val().toLowerCase().indexOf("power") != -1 && $('[name=uid_r]').val().toLowerCase().indexOf("admin") != -1)) {
            $('[name=uid_r]').select();
            $('#alertmsg').text('This username and password combination is unknown, please try again.');
            $("#popupDialog").popup('open');
        }            
        else {            
                sessionStorage.authString = makeBasicAuth($('[name=uid_r]').val(), $('[name=pwd_r]').val());
            getUserAuthenticate();
        }
    });

    $('#cancel').click(function () {
        return false;
    });
});

function displayNewLoginPage(loginUrl) {
    $.getJSON(newLoginUrl, function (data) {
        newLoginUrlData = data;
        var default_data = newLoginUrlData["default"];
        var default_customer_number = "1";
        var custom_data = [];
        $.each(newLoginUrlData, function (key, val) {
            var is_cust_name_exists = (cust_name.toLowerCase().indexOf(val.customerName.replace(/ /g, '').replace(/'/g, '').toLowerCase()) > -1);
            if (is_cust_name_exists || (!is_cust_name_exists && cust_id == key)) {
                default_customer_number = key;
                custom_data = val;
                return false;
            }
        });
        if (Object.keys(default_data).length == Object.keys(custom_data).length) {
            default_data = custom_data;
        }
        else if (Object.keys(custom_data).length > 0) {
            $.each(custom_data, function (key, val) {
                default_data[key] = custom_data[key];
            });
        }
        var hdrImage_logo = default_data.hdrImageLogo;
        $('#hdrImage').append(hdrImage_logo);
        $('#_login').addClass(default_data.bkgdImage);
        $('#dvHeader').attr('data-theme', default_data.dvHeaderTheme);
        $('#dvHeader').addClass(default_data.dvHeaderClass);
        if ($('#dvHeader').hasClass('ui-bar-inherit')) {
            $('#dvHeader').removeClass('ui-bar-inherit')
        }
        $('#dvHeader').css('display', default_data.dvHeaderDisplay);
        $('#dvHeader').attr('style', default_data.dvHeaderColor);
        $('#hdrSpacer').attr('src', default_data.hdrSpacer);
        $('#dvUserPwd').addClass(default_data.dvUserPwd);
        $('#dvUserPwd').attr('data-theme', default_data.dvUserPwdTheme);
        $('#dvUserPwd').attr('style', default_data.dvUserPwdBgColor);
        $('#Lusername').attr('data-theme', default_data.txtLUserNameTheme);
        $('#Lpassword').attr('data-theme', default_data.txtLUserPwdTheme);
        $('#Lusername').attr('style', default_data.txtLUserNameThemeStyle);
        $('#Lpassword').attr('style', default_data.txtLUserPwdThemeStyle);
        $('#img1').attr('src', default_data.img1);
        $('#img2').attr('src', default_data.img2);
        $('#img3').attr('src', default_data.img3);
        $('#img1').attr('style', default_data.img1Width);
        $('#img2').attr('style', default_data.img2Width);
        $('#img3').attr('style', default_data.img3Width);
        $('#gizmoImage').attr('src', default_data.gizmoImage);
        if (default_data.img1Href != undefined && default_data.img1Href != null && default_data.img1Href != "") {
            var img_ele = "";
            img_ele = '<a href="images/FAQs.pdf" target="_blank">'
            img_ele += $('#img1')[0].outerHTML;
            img_ele += '</a>'
            $('#img1').replaceWith(img_ele);
        }
        if (default_data.img2Href != undefined && default_data.img2Href != null && default_data.img2Href != "") {
            var img_ele = "";
            img_ele = '<a href="' + default_data.img2Href + '" target="_blank">'
            img_ele += $('#img2')[0].outerHTML;
            img_ele += '</a>'
            $('#img2').replaceWith(img_ele);
        }
        if (default_data.img3Href != undefined && default_data.img3Href != null && default_data.img3Href != "") {
            var img_ele = "";
            img_ele = '<a href="' + default_data.img3Href + '" >'
            img_ele += $('#img3')[0].outerHTML;
            img_ele += '</a>'
            $('#img3').replaceWith(img_ele);
        }

        $('#login').attr('data-theme', default_data.login);
        var btn_class = 'ui-btn-up-' + default_data.login;
        $('#login').removeClass('ui-btn-up-c');
        $('#login').addClass(btn_class);
        $('#login').attr('style', default_data.loginBtnColor);
        $('#aForgotPassword').attr('style', default_data.aForgotPassword);
        $('#dvHeading').attr('style', default_data.dvHeading);
        $('#dvloginText').attr('style', default_data.dvloginText);
        $('#pageHeader').html(default_data.pageHeader);
        $('#pLoginText').html(default_data.pLoginText);
        if (default_customer_number == KUBOTA_CUSTOMER_NUMBER) {
            $("#dvLoginPlaceHolder").css('background-color', '#f15922');
        }
        updateFooterInfo(default_customer_number);        
    });
}

function fnCheckBrowserVersion() {
    var ret_val = true;
    if (($.browser.msie && browserVersion < 9) || ($.browser.safari && browserVersion < 5)) {
        //$('#alertmsg').text('This site is only supported in the following browsers: Internet Explorer 9, Internet Explorer 10, Chrome, Firefox and Safari 5.0.');
        $('#HLogin').hide();
        var frame_height = (window.screen.availHeight > 860) ? window.screen.availHeight - 154 : '100%';
        $('#dvBrowserInfo').css('display', 'block');
        $('#dvBrowserInfo').css('height', frame_height);
        $('#dvBrowserInfo').css('width', '100%');
        $('#dvBrowserInfo').css('position', 'absolute');
        $('#dvBrowserInfo').css('top', '100px');
        $('#dvBrowserInfo').css('text-align', 'center');        
        return false;
    }
}

function fnClickLogin(eventObj) {
    var keycode;
    if (eventObj.keyCode) //For IE
        keycode = eventObj.keyCode;
    else if (eventObj.Which)
        keycode = eventObj.Which; // For FireFox
    else
        keycode = eventObj.charCode; // Other Browser

    if (keycode === 13)
        $("#login").trigger("click");
}

function fnForgotPassword(type) {
    if (type == "forgot") {
        $("#hdrPasswordType").html("Forgot My Password");
        $('#spnPasswordText').html('Enter your username below and click "Send Password" to have a new password sent to your email address.');
        $('#btnSend').text('Send Password');
    }
    else {
        $("#hdrPasswordType").html("Reset Password");
        $('#spnPasswordText').html('To reset your password, enter your username below and click "Reset Password".');
        $('#btnSend').text('Reset Password');
    }
    $('#txtUserName').val('');
    $('#popupForgotPassword').popup('open');
}

function fnCancel() {
    $('#divError').css('display', 'none');
    $('#popupForgotPassword').popup('close');
}

function fnSendPassword() {
    var type = ($('#hdrPasswordType').text().toLowerCase().indexOf('forgot') > -1) ? 'forgot' : 'reset';
    if (!fnValidateUserName()) return false;
    if (type == "forgot") {
        getCORS(forgotPasswordUrl + jQuery.trim($("#txtUserName").val()) + '/asdf', null, function (data) {
            $('#popupForgotPassword').popup('close');
            $('#alertmsg').text('A new password has been sent to the email for this account.');
            $('#popupDialog').popup('open');
        }, function (error_response) {
            if (error_response.responseText.toLowerCase() == "error: profile. forgot my password..") {
                $('#alertmsg').text('We are unable to change the password for this account. Please contact a system administrator.');
                $('#popupForgotPassword').popup('close');
                $('#popupDialog').popup('open');
                $("#okBut").attr("onclick", "$('#popupForgotPassword').popup('close');");
            }
            else {
                showErrorResponseText(error_response, false);
            }
        });
    }
    else {
        $('#popupForgotPassword').popup('close');
    }
}

function fnValidateUserName() {
    if (jQuery.trim($("#txtUserName").val()) == "") {
        $('#alertmsg').text('Username should not be empty.');
        $('#popupForgotPassword').popup('close');
        $('#popupDialog').popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close'); $('#popupForgotPassword').popup('open');");
        return false;
    }
    return true;
}

//******************** Public Functions End **************************