jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gServiceUrl = "api/NexusIndex_v2/";
//if (appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER) // for casey's
//    gServiceUrl = serviceURLDomain + "api/JobStatus/" + appPrivileges.customerNumber + "/1/caseysOnline";

var gServicePreference = serviceURLDomain + "api/Preferences/155711/1/index.htm";

var customerConfigUrl = "JSON/_customerConfiguration.JSON";
var customconfigData = {};
var advanceSearchUrl = "JSON/_advSearch.JSON";
var index_json_url = "JSON/_indexTd.JSON";
var index_jobs_error_url = "JSON/_indexJobProcessError.JSON";
var gUserServiceUrl = 'JSON/_personnelUsers.JSON';
var dailyTaskServiceUrl = "JSON/_myTaskSummary.JSON";
var optInUrl = "https://nexus.tribunedirect.com/api/DistrictEmail/";
var changePasswordUrl = serviceURLDomain + "api/Password/";
var dailyTaskUrlData = {};
var advanceSearchData = {};
var reportsServiceUrl = (jobCustomerNumber == SUNTIMES_CUSTOMER_NUMBER) ? "JSON/_stngReportNames.JSON" : "JSON/_reportsName.JSON";
var reportsServiceData = {};
var gData;
var gPageSize = 20;
var gDataStartIndex = 0;
var gDataList = [];
var gPrefData;
var pagePrefs = "";
var facilityId = "";
var vm;
var selectedUsers = [];
var tagJobInfo = {};
var demoEmailsList = [];
var processErrorData;
var usersJson = [];
//facilityId = "1";
facilityId = (sessionStorage.facilityId != undefined && sessionStorage.facilityId != null && sessionStorage.facilityId != "") ? sessionStorage.facilityId : "1";
//else 

if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "" && appPrivileges.customerNumber == ALLIED_CUSTOMER_NUMBER) {
    facilityId = '3';
    sessionStorage.facilityId = "3";
}
var job_mile_stones = ["Scheduled", "Sign-Up Start", "Sign-Up End", "Approval Start", "Approval End", "Sale Start", "Sale End", "Data", "Art Arrival", "Data Complete", "Whitepaper / SO Due", "Printed Materials", "Production", "Postage", "Shipped", "Mail Drop", "In-Home", "Invoice", "Job Complete"];

var facilityJSON = [{ "facilityName": "Chicago", "facilityValue": "1" }, { "facilityName": "Los Angeles", "facilityValue": "2" }];

//******************** Global Variables Start **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_indexGeneral', function () {

    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
        // window.setTimeout(function loadHints() {
        createDemoHints("index");
        if (jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER)
            $('#dvWelcomeMsg').css('display', 'block');
        //}, 500);
    }
    //if (jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName != "admin") || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) {
    //    window.setTimeout(function () {
    //        var source = ((jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName != "admin") || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER) ? "demoListJobs" : "dvJetsDemoJobs";
    //        //$('#' + source + ' div[data-role=collapsible]').trigger('click')
    //        $('#' + source + ' div[data-role=collapsible]').collapsible('expand');
    //    }, (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 3500 : 3500);
    //}
    //if ((jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER /*|| jobCustomerNumber == DCA_CUSTOMER_NUMBER */ || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName != "admin")) && (sessionStorage.isDemoIntroDisplayed == undefined || sessionStorage.isDemoIntroDisplayed == null || sessionStorage.isDemoIntroDisplayed == "" || sessionStorage.isDemoIntroDisplayed == "false")) {
    if ((jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER /*|| jobCustomerNumber == DCA_CUSTOMER_NUMBER*/ || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && !is_valid_domain && appPrivileges.roleName != "admin")) && (sessionStorage.isDemoIntroDisplayed == undefined || sessionStorage.isDemoIntroDisplayed == null || sessionStorage.isDemoIntroDisplayed == "" || sessionStorage.isDemoIntroDisplayed == "false")) {
        sessionStorage.isDemoIntroDisplayed = true;
        window.setTimeout(function () {
            $('#popupGizmoIntro').popup('open');
            window.setTimeout(function () {
                $('#popupGizmoIntro').popup('close');
            }, 10000);
        }, 500);
    }

    //if (appPrivileges.roleName == "admin" && appPrivileges.customerNumber == "1") {
    if ((appPrivileges.roleName == "admin" && (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER)) && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
        //$('#dvPreferences').empty();
        $('#btnFilter').remove();
        showFilters();
        showFilterTasks();
        window.setTimeout(function () {
            $('#dvPreferences').collapsibleset('refresh');
        }, 50);
    }
    //if (appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || appPrivileges.customerNumber == "99998" || appPrivileges.customerNumber == "99997" || appPrivileges.customerNumber == "99996" || appPrivileges.customerNumber == "1") {
    //if (appPrivileges.customerNumber == "99998" || appPrivileges.customerNumber == "99997" || appPrivileges.customerNumber == "99996" || appPrivileges.customerNumber == "1") {
    //    showDemos();
    //    window.setTimeout(function applyThemes() {
    //        $('#dvPreferences').collapsibleset('refresh');
    //    }, 50);
    //}
    window.setTimeout(function () {
        if (sessionStorage.indexPrefs != undefined && sessionStorage.indexPrefs != null && sessionStorage.indexPrefs != "null" && sessionStorage.indexPrefs != "")
            loadData();
        $("#_indexGeneral").trigger("create");
    }, ((appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) ? 1000 : 0));
    //if (appPrivileges.customerNumber== CW_CUSTOMER_NUMBER && (getSessionData("userRole") == "user" || getSessionData("userRole") == "power"))
    //$("#dvJobs").css("display", "block");
    //if (appPrivileges.customerNumber == CW_CUSTOMER_NUMBER) {

    //$('#viewJobs').html(viewJobs()); //TO BE Discussed

    $('.ui-page').trigger('create');
    //if(getSessionData("userRole") == "admin")
    //    $("#btnJobHistory").css("display", "block");
    $("#aSort").css("display", "block");
    $("#btnJobHistory").css("display", "none");
    //}

    //if (appPrivileges.customerNumber == "1") {
    if ((appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
        //$('#lnkNewJob').css('display', 'none');
        //$("#btnJobHistory").css("display", "none");
        //$("#dvDailyTaskListData").css("display", "block");
        //$('#btnSignOut').addClass('ui-corner-all');
        loadRegionsDDL();
        $('#ddlRegions').val(sessionStorage.facilityId).selectmenu('refresh');
    }

    if (appPrivileges.customerNumber != "1" && appPrivileges.customerNumber != REDPLUM_CUSTOMER_NUMBER) {
        $('#dvRegionsDDL').css('display', 'none');
        //$("#dvDailyTaskListData").css("display", "none");
        //$('#dvReports').addClass('ui-corner-all');
    }
    //if (jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER)
    //if (jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName != "admin"))
    if (jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && !is_valid_domain && appPrivileges.roleName != "admin"))
        displayReportsList();
    if (appPrivileges.roleName == "admin") {
        $('#dvAdministration').css('display', 'block');
        $('#btnFilter').css('display', 'block');
        //        window.setTimeout(function loadDropdowns() {
        //            loadTagRegionsDDL(facilityJSON, 'filter');
        //            loadDepartmentsDDL(departmentsJson, 'filter')
        //            loadTagUsersDDL(usersJson, 'filter');
        //            // $('#fldSetFilterOptions').controlgroup('refresh');
        //        }, 1000);
        ////        window.setTimeout(function applyThemes() {
        ////            $('#fldSetFilterOptions').controlgroup('refresh');
        ////        }, 2000);
        if (jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
            $('#dvJetsDemoJobs').css('display', 'none');
            $('#dvBobsList').css('display', 'none');
            $('#demoListJobs').css('display', 'none');
            //$('#dvJetsDemoJobs').css('display', 'none');
            $('#dvWelcomeMsg').css('display', 'none');
            $('#dvAboutDemo').css('display', 'none');
            $('#btnHint').css('display', 'none');
        }
        else {
            loadAboutThisDemoFaq();
            if (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER) {
                loadDemoReportsJson();
                $('#dvPreferences').css('display', 'none');
            }
            else {
                displayReportsList();
            }
            //$('#dvWelcomeMsg').css('display', 'block');
            $('#btnHint').css('display', 'block');
            $('#btnHint').css('display', '');
            if (jobCustomerNumber == BBB_CUSTOMER_NUMBER) {
                $('#dvJetsDemoJobs').css('display', 'none');
                $('#dvBobsList').css('display', 'block');
                $('#dvAboutDemo').css('display', 'block');
            }
            else if (jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) {
                $('#demoListJobs').css('display', 'none');
                $('#dvJetsDemoJobs').css('display', 'block');
                $('#dvBobsList').css('display', 'none');
                if (jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER)
                    $('#dvAboutDemo').css('display', 'block');
                else $('#dvAboutDemo').css('display', 'none');
                $('#dvAdministration').css('display', 'block');
            }
            else {
                $('#dvJetsDemoJobs').css('display', 'none');
                if (jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
                    $('#demoListJobs').css('display', 'none');
                }
                $('#dvBobsList').css('display', 'none');
                $('#dvAboutDemo').css('display', 'none');
            }
        }
        $('#btnDeleteJob').css('display', 'block');
        $('#btnDeleteJob').css('display', '');
        $('#okButConfirm').text('Delete Job');
    }
    else {
        $('#dvAdministration').css('display', 'none');
        if (jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
            $('#dvBobsList').css('display', 'none');
            $('#demoListJobs').css('display', 'none');
            $('#dvJetsDemoJobs').css('display', 'none');
            $('#dvWelcomeMsg').css('display', 'none');
            $('#dvAboutDemo').css('display', 'none');
        }
        else {
            loadAboutThisDemoFaq();
            $('#dvPreferences').css('display', 'none');
            //$('#dvWelcomeMsg').css('display', 'block');
            if (jobCustomerNumber == BBB_CUSTOMER_NUMBER) {
                $('#dvJetsDemoJobs').css('display', 'none');
                $('#dvBobsList').css('display', 'block');
                $('#dvAboutDemo').css('display', 'block');
            }
            else if (jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) {
                $('#dvJetsDemoJobs').css('display', 'block');
                $('#demoListJobs').css('display', 'none');
                $('#dvBobsList').css('display', 'none');
                if (jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER)
                    $('#dvAboutDemo').css('display', 'block');
                else $('#dvAboutDemo').css('display', 'none');
                if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
                    $('#ulPickDemo').parent().parent().remove();
                    $('#dvAboutDemo div[data-role=collapsible]').addClass('ui-last-child');
                    $('#dvAboutDemo').collapsibleset('refresh');
                }
                $('#dvAdministration').css('display', 'none');
            }
            else {
                $('#dvJetsDemoJobs').css('display', 'none');
                if (jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
                    $('#demoListJobs').css('display', 'none');
                }
                $('#dvBobsList').css('display', 'none');
                $('#dvAboutDemo').css('display', 'none');
            }
        }
    }

    if (appPrivileges.roleName != "admin")
        $('#aSort').addClass('ui-corner-all');
    /*$.each($('#col1 div[data-role=collapsible-set]'), function (key, val) {
    $(this).collapsibleset().trigger('create'); ;

    });*/


    $('#btnSort').addClass('ui-btn-corner-all');
    if (jobCustomerNumber == SUNTIMES_CUSTOMER_NUMBER) {
        //$('#lnkMaterialsAdmin').addClass('ui-corner-bl ui-corner-br');
        $('#lnkNewJob').hide();
        $('#lnkMaterialsAdmin').show();
        $('#lnkMaterialsAdmin').addClass('ui-first-child ui-last-child');
        $('#lnkMaterialsAdmin').addClass('ui-corner-all');
    }
    else {
        $('#lnkMaterialsAdmin').hide();
        if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER) {
            $('#lnkNewJob').text('Start A New Job');
        }
        else if ((jobCustomerNumber == AAG_CUSTOMER_NUMBER && (is_valid_domain || appPrivileges.roleName == "admin")) || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
            //else if ((jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.userRole == "admin") || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
            $('#lnkNewJob').text('Start Upload Process');
        }
            //else if ((jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName == "power")) {
        else if ((jobCustomerNumber == AAG_CUSTOMER_NUMBER && !is_valid_domain && appPrivileges.roleName == "power")) {
            $('#lnkEditJob').text('Start Job');
        }

        $('#lnkNewJob').addClass('ui-corner-all');
        $('#lnkNewJob').css('border-bottom-right-radius', '0.7em');
        $('#lnkNewJob').css('border-bottom-left-radius', '0.7em');
    }
    $('#lnAdmin').hide();
    if (getSessionData("userRole") == "admin" && (jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER)) {
        //To show customer config button for admin only
        $("#aCustomerConfig").css("display", "block");
        //$("#dvReports").css("display", "block");
        $('#dvDemoEmailsList').css('display', 'none');
    }
    else {
        if (jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER)
            $('#dvDemoEmailsList').css('display', 'block');
        $("#aCustomerConfig").css("display", "none");
        //$("#dvReports").css("display", "none");
        //$('#dvHearder').removeClass('ui-bar');
        //if ($('#btnSignOut').hasClass('ui-corner-all'))
        //  $('#btnSignOut').removeClass('ui-corner-all').addClass('ui-corner-right');
        //$('#btnSignOut').addClass('ui-corner-left');
        $('#btnSignOut').addClass('ui-corner-all');
        $('#btnSignOut').addClass('ui-first-child');
        //$('#dvPreferences').css('display', 'none');
        $('#dvAdministration').css('display', 'none');
        //if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
        //    $('#dvAdministration').css('display', 'block');
        //}
    }

    if (appPrivileges.customerNumber == REGIS_CUSTOMER_NUMBER || appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == SUNTIMES_CUSTOMER_NUMBER || appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER || appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || appPrivileges.customerNumber == WALGREENS_CUSTOMER_NUMBER || appPrivileges.customerNumber == ALLIED_CUSTOMER_NUMBER || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER || appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {//appPrivileges.customerNumber == "186"
        //$('#btnSignOut').attr('href', 'login_new.htm');
        var url1 = 'login.htm?q=' + appPrivileges.customerNumber;
        $('#btnSignOut').attr('href', url1);
    }
    else
        $('#btnSignOut').attr('href', 'login.htm');

    //if (appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER) {
    //    $('#lnkNewJob').css('display', 'none');
    //    $('#btnSignOut').addClass('ui-corner-all');
    //}
    if ($('#ddlFilterMilestones').val() == "-1") {
        if (($('#dtpStart') || $('#dtpEnd')).hasClass('ui-input-text')) {
            $('#dtpStart')[0].disabled = true;
            //$('#dtpStart').addClass('ui-disabled');
            $('#dtpStart').css('opacity', '0.7');
            //$('#dtpEnd').addClass('ui-disabled');
            $('#dtpEnd')[0].disabled = true;
            $('#dtpEnd').css('opacity', '0.7');
        }
    }
    if ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && (appPrivileges.roleName == "power" || appPrivileges.roleName == "user")) {
        $('#lnkNewJob').css('display', 'none');
    }
    if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER && getSessionData("userRole") != "power")
        $('#dvAdministration').css('display', 'block');
    /* Caseys Op-in and Appoval email links for Demo

    if (appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER && appPrivileges.roleName == "admin") {
    if ($('#lnkNewJob').hasClass('ui-corner-top')) {
    $('#lnkNewJob').removeClass('ui-corner-all')
    }
    $('#lnkSendOptInEmail').css('display', 'block');
    $('#lnkSendApprovalEmail').css('display', 'block');
    $('#lnkSendApprovalEmail').addClass('ui-corner-bl ui-corner-br')
    }
    else {
    $('#lnkSendOptInEmail').css('display', 'none');
    $('#lnkSendApprovalEmail').css('display', 'none');
    } */
});

$('#_indexGeneral').live('pagebeforecreate', function () {
    //    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
    //        loadDemoHintsInfo();
    //    }
    if (appPrivileges.roleName != "user") {
        $('<li><a href="" onclick="deleteGizmoUsers();">Archive or Delete Jobs</a></li><li><a href="" onclick="confirmFlushInMemoryDB();">Flush In-Memory DB</a></li>').appendTo('#ul2');
        $('<li style="display:none"><a href="" onclick="manageFin();">Finance</a></li>').appendTo('#ul2');
        if (appPrivileges.roleName == "admin") {
            $('<li style="display:none"><a href="" onclick="navEmailLists();">Email Lists</a></li>').appendTo('#ul2');
            $('<li><a href="" id="printStreamConnection" onclick="displayFacilityList();">Printstream Connections</a></li>').appendTo('#ul2');
            if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                $('<li><a href="" id="lmaSetup" onclick="loadLMASetup();">LMA Setup</a></li>').appendTo('#ul2');
                if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER)
                    $('<li><a href="" id="loadStoreData" onclick="loadStoreData();">Load Store Data</a></li>').appendTo('#ul2');
            }
        }
        //$('<li><a href="" onclick="navTemplateSetup();">Template Setup</a></li>').appendTo('#ul2');
    }
    else {        
        $('<li><a href="" id="lmaSetup" onclick="loadLMASetup();">LMA Setup</a></li>').appendTo('#ul2');
        $("li").first().css("display", "none");
    }

    if ((appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
        getDailyTaskData();
    }
    if (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") {
        $("#hdnSortOpenJobs").val(sessionStorage.indexPageSortOrder);
        if ($("#hdnSortOpenJobs").val() == 1)
            $('#chkInHome').attr('checked', true);
        else if ($("#hdnSortOpenJobs").val() == 2)
            $('#chkTemplate').attr('checked', true);
        else if ($("#hdnSortOpenJobs").val() == 3) {
            $('#chkInHome').attr('checked', true);
            $('#chkTemplate').attr('checked', true);
        }
    }
    getProcessErrorJobs();
    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER)
        loadDemoEmailsList();
    //loadPersonalGroups(sessionStorage.facilityId);
    //loadUsersToJson();
    displayMessage('_indexGeneral');
    createConfirmMessage("_indexGeneral");
    getData();
    var region = jobCustomerNumber; // appPrivileges.customerNumber;
    if (region != null && region != "1") {
        setLogo(region);
    }
    else if (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER)
        setLogo(sessionStorage.facilityId);
    //test for mobility...
    //loadMobility();
    if (appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER)
        setCustomGizmoLogo(appPrivileges.customerNumber);
    else
        setGizmoLogo();

    loadingImg("_indexGeneral");
    //if (appPrivileges.customerNumber == "1")

    getNextPageInOrder();
});
function getProcessErrorJobs() {
    $.getJSON(index_jobs_error_url, function (data) {
        processErrorData = data;
    });
}
function getData() {
    if (sessionStorage.indexPrefs == undefined || sessionStorage.indexPrefs == null || sessionStorage.indexPrefs == "null" || sessionStorage.indexPrefs == "") {
        if (appPrivileges.customerNumber == REGIS_CUSTOMER_NUMBER || appPrivileges.customerNumber == "203" || appPrivileges.customerNumber == SUNTIMES_CUSTOMER_NUMBER || appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || appPrivileges.customerNumber == BBB_CUSTOMER_NUMBER || appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER || appPrivileges.customerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || appPrivileges.customerNumber == SANDIEGO_CUSTOMER_NUMBER || appPrivileges.customerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER || appPrivileges.customerNumber == SAFEWAY_CUSTOMER_NUMBER || appPrivileges.customerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
            getPagePreferences('index.htm', "1", (appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) ? "1" : CW_CUSTOMER_NUMBER);
        else
            getPagePreferences('index.htm', "1", (appPrivileges.customerNumber == JETS_CUSTOMER_NUMBER) ? CASEYS_CUSTOMER_NUMBER : appPrivileges.customerNumber);
    }
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.indexPrefs));
    }
}

function loadData() {
    pagePrefs = jQuery.parseJSON(sessionStorage.indexPrefs);
    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;

    if (pagePrefs != null) {
        //gServiceUrl = eval(getURLDecode(pagePrefs.scriptBlockDict.gServiceUrl));
        //gServiceUrl = serviceURLDomain + "api/CWIndexV2/" + $("#hdnSortOpenJobs").val();
        var user_role = getSessionData("userRole");
        if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && user_role == "user") || (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && user_role != "admin")) {
            //eval(getURLDecode(pagePrefs.scriptBlockDict.pagebeforecreate));
            var theInstructions = getURLDecode(pagePrefs.scriptBlockDict.pagebeforecreate);
            var F = new Function(theInstructions);
            if (theInstructions != "")
                return (F());
        }
    }
    makeGData();

}

//******************** Page Load Events End ****************************

//******************** Public Functions Start **************************
var DailyTaskModel = function (process_name, process_number, process_attrs, process_theme, facility_id, process_status, icon_alt_text, is_editable) {
    var self = this;
    self.processName = process_name;
    self.processNumber = process_number;
    self.processAttrs = process_attrs;
    self.processTheme = process_theme;
    self.facilityId = facility_id;
    self.processStatusIcon = process_status;
    self.isEditable = (is_editable == 0) ? false : true;
};

var DailyTaskListModel = function (title, process_list, incomplete_cnt) {
    var self = this;
    self.groupTitle = title;
    self.incompleteCount = ko.observable(incomplete_cnt);
    self.processListData = ko.observableArray([]);
    self.processListData(process_list);
    //self.taskFacilityId(facility_id);
};

var JobModel = function (job_name, job_indentifier, job_attrs, filter_text, job_theme, job_number, job_customer_number, facility_id, milestone_icon, icon_alt_text, is_editable) {
    var self = this;
    self.jobName = job_name;
    self.jobIdentifier = job_indentifier;
    self.jobAttrs = job_attrs;
    self.filterText = filter_text;
    self.jobTheme = job_theme;
    self.jobNumber = job_number;
    self.jobCustomerNumber = job_customer_number;
    self.facilityId = facility_id;
    self.milestoneIcon = milestone_icon;
    self.milestoneIconAltText = icon_alt_text;
    self.isEditable = (is_editable == 0) ? false : true;
    //if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && job_number == "414943") {
    //    self.pendingApprovalPercent = "100";
    //    self.pendingApprovalStatusPercent = '100'
    //    self.pendingApprovalStatus = getPendingApprovalStatus("100");
    //}
    //else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && job_number == "414987") {
    //    self.pendingApprovalPercent = '3';
    //    self.pendingApprovalStatusPercent = '45'
    //    self.pendingApprovalStatus = getPendingApprovalStatus('45');
    //}
    //else {
    self.pendingApprovalPercent = getPendingApprovalPercent(job_name, job_number);
    self.pendingApprovalStatusPercent = self.pendingApprovalPercent;
    self.pendingApprovalStatus = getPendingApprovalStatus(self.pendingApprovalPercent, job_number);
    //}
};


var jobGroupModel = function (title, show_region, job_items, paging_info, selected_page, search_value, facility_id, show_tagging) {
    var self = this;
    self.groupTitle = title;
    self.regionTitle = getTribuneFacilityName(facility_id);
    self.showRegion = show_region;
    self.listViewData = ko.observableArray([]); //.extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 0} });
    self.pagingData = ko.observableArray(paging_info).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 200 } });
    self.selectedPageValue = ko.observable();
    var selected_value = (selected_value != undefined && selected_value != null && selected_value != "") ? selected_page : "0";
    self.selectedPageValue(selected_value);
    self.listViewData(job_items);
    self.searchValue = ko.observable();
    self.prevSearchValue = ko.observable('');
    self.searchValue(((search_value != undefined && search_value != null) ? search_value : ""));
    self.searchedRows = ko.observableArray([]);

    self.showMoreOption = ko.observable();
    var is_more_visible = (paging_info.length > 0 && parseInt(selected_value) < paging_info.length) ? true : false;
    self.showMoreOption(is_more_visible);
    self.showTagging = show_tagging;
    self.showSearchField = ko.observable();

    //    self.thvalue = ko.computed(function () {
    //        return self.searchValue();
    //    }, self).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 600 }, throttle: 1 });
    //    self.thvalue.subscribe(function (val) {
    //        //if (val !== '') {
    //        var selected_target = $('#hdnSelectedGroup').val();
    //        var foundKeys = [];
    //        var page_index = 0;
    //        var context_data = "";
    //        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
    //        if (selected_target != "") {
    //            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
    //                return gData.indexLists[key] === self.groupTitle;
    //            });
    //            var root_context = ko.contextFor($('#' + ctrl_id)[0]).$root;
    //            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
    //            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
    //            var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
    //            page_index = context_data.selectedPageValue();
    //            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
    //            getSelectedData(ctrl_id, self, self.groupTitle, gServiceUrl_data, foundKeys[0], page_index, sort_order, '');
    //        }
    //        //}
    //    });
    //{ throttle: 500 }
    self.dataIcon = 'user'; //((title.indexOf('Open') > -1) ? 'myapp-tag' : ((title.indexOf('Tagged') > -1) ? 'myapp-tag' : ''));
};

var demoEmailListModel = function (data) {
    var self = this;
    self.name = ko.observable(data.name);
    self.type = ko.observable(data.type);
    self.startTime = ko.observable((data.startTime != undefined && data.startTime != null) ? data.startTime : "");
    self.endTime = ko.observable((data.endTime != undefined && data.endTime != null) ? data.endTime : "");
    self.id = ko.observable(data.id);
    self.showStartEndTime = ko.computed(function () {
        return (self.startTime() != "" && self.endTime() != "");
    });
};

var viewModel = function () {
    var self = this;
    taggingInfo(self);
    self.loadPersonalGroups(sessionStorage.facilityId);
    self.person = ko.observable();
    self.editDefaultJob = function () {
        var temp_default_json = (sessionStorage.defaultJobDetails != undefined && sessionStorage.defaultJobDetails != null && sessionStorage.defaultJobDetails != "") ? $.parseJSON(sessionStorage.defaultJobDetails) : {};
        if (Object.keys(temp_default_json).length > 0) {
            sessionStorage.jobCustomerNumber = jobCustomerNumber;
            sessionStorage.jobNumber = temp_default_json.defaultJobNumber;
            sessionStorage.isGizmoJob = true;
            sessionStorage.jobDesc = temp_default_json.defaultJobName;
            if (temp_default_json.defaultJobNumber < 0)
                sessionStorage.negJobNumber = temp_default_json.defaultJobNumber;
            //Gets the selected job data from service.
            var g_edit_job_service = serviceURLDomain + "api/JobTicket/" + sessionStorage.facilityId + "/" + temp_default_json.defaultJobNumber + "/" + sessionStorage.username;
            getCORS(g_edit_job_service, null, function (data) {
                if (data == null) {
                    $('#alertmsg').text("No data exists for the selected Job. Please contact Administrator.");
                    $('#popupDialog').popup('open');
                    return false;
                }
                else {
                    makeEditJobData(data);
                    window.location.href = "../jobSelectTemplate/jobSelectTemplate.html";
                }
            }, function (error_response) {
                showErrorResponseText(error_response);
            });

            //Creates required local session objects for the selected job when edit is clicked.
            self.assignGetData = function () {

            };
            //storeJobInfo(navlink_counter, current_context.jobNumber, current_context.jobNumber, current_context.jobName, current_context.facilityId, current_context.jobCustomerNumber, current_context.isEditable);
        }
    };
    self.jobClick = function (current_context, index, title) {
        var navlink_counter = (index + 1) + '_' + title.replace(/ /g, '') + '_' + current_context.jobNumber;
        if (current_context.jobCustomerNumber == BBB_CUSTOMER_NUMBER || current_context.jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || current_context.jobCustomerNumber == AAG_CUSTOMER_NUMBER || current_context.jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || current_context.jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || current_context.jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || current_context.jobCustomerNumber == JETS_CUSTOMER_NUMBER || current_context.jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || current_context.jobCustomerNumber == CW_CUSTOMER_NUMBER || current_context.jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || current_context.jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || current_context.jobCustomerNumber == LOWES_CUSTOMER_NUMBER || current_context.jobCustomerNumber == DCA_CUSTOMER_NUMBER || current_context.jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || current_context.jobCustomerNumber == SK_CUSTOMER_NUMBER || current_context.jobCustomerNumber == GWA_CUSTOMER_NUMBER)
            sessionStorage.showDemoHints = "on";
        storeJobInfo(navlink_counter, current_context.jobNumber, current_context.jobNumber, current_context.jobName, current_context.facilityId, current_context.jobCustomerNumber, current_context.isEditable);
    };

    self.pageChanged = function (el, e, parent) {
        if (e.originalEvent) {
            var ctrl_id = (e.targetElement || e.target || e.currentTarget).id;
            var ctrl = $('#' + e.target.id);
            var context_data = ko.contextFor(ctrl[0]).$parent;

            var foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === context_data.groupTitle;
            });
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            //var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, foundKeys[0], ctrl.val(), sort_order);

            //window.setTimeout(function setUI() {
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
            //}, 40);
        }
    };
    self.moreClick = function (el, e) {
        if (e.originalEvent) {
            var ctrl_id = (e.targetElement || e.currentTarget || e.target).id;
            var ctrl = $('#' + ctrl_id);
            var context_data = ko.contextFor(ctrl[0]).$parent;

            var foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === context_data.groupTitle;
            });
            context_data.selectedPageValue(context_data.selectedPageValue() + 1);
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            // var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;

            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, foundKeys[0], (context_data.selectedPageValue()), sort_order);
            //window.setTimeout(function setUI() {
            $("#" + +ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
            //}, 40);
        }
    };

    self.selectedJobName = ko.observable();
    self.getSelectedRegions = function (job_info) {
        self.selectedJobName(job_info.jobName + ' | ' + job_info.jobNumber);
        $('#dvOptions').empty();
        $('#ulWhomsBeenTagged').empty();
        $('#ulSelectedRegionsOrUsers').empty();
        $('#ddlTaggingType').val("-1").selectmenu('refresh');
        //self.selectedJobName = item.JobName() + ' | ' + ((item.InHome().indexOf('Number') > 0) ? item.InHome() : (item.JobFields().split('|')[0] != '') ? item.JobFields().split('|')[0] : item.InHome());
        //$("#selectedJob").text(self.selectedValue);
        //$("#fsRegions").trigger('create');
        self.loadPersonalGroups(sessionStorage.facilityId);
        self.selectedJob = ko.observable(job_info);
        var tag_get_info = serviceURLDomain + "api/Tagging_get/" + sessionStorage.facilityId + "/" + job_info.jobCustomerNumber + "/" + job_info.jobNumber;
        getCORS(tag_get_info, null, function (response_data) {
            tagJobInfo = response_data;
            self.displayJobWhosBeenTagged();

        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });

        $('#popupJobStatus').popup('open');
    };
    self.selectedJob = ko.observable({});
    //self.tagJobClick = function () {
    //    var tagging_type = "";
    //    var selected_users = "";
    //    //        if () {
    //    //            tagging_type = 'my';
    //    //        }
    //    //        else
    //    selected_users = sessionStorage.username;
    //    if ($('#ddlTaggingType').val() == "2") {
    //        tagging_type = 'region';
    //    }
    //    else if ($('#ddlTaggingType').val() == "1" || $('#ddlTaggingType').val() == "4") {
    //        tagging_type = 'assigned';
    //        if ($('#ddlTaggingType').val() == "4")
    //            selected_users = $('#ulSelectedRegionsOrUsers li a[value!=""][title!=Remove]').attr('value');
    //    }
    //    var temp = tagJobInfo;
    //    var other_facility_id = "-1";
    //    if (tagging_type == 'region')
    //    //other_facility_id = $('#ddlTagRegions').val();
    //        if ($('#ulSelectedRegionsOrUsers li a[value!=""][title!=Remove]').length > 0)
    //            other_facility_id = $('#ulSelectedRegionsOrUsers li a[value!=""][title!=Remove]').attr('value');
    //    /*if (other_facility_id == appPrivileges.facility_id) {
    //    $('#popupJobStatus').popup('close');
    //    return false;
    //    }*/

    //    if (Object.keys(tagJobInfo).length > 0) {
    //        //var tagg_job_url = serviceURLDomain + 'api/Tagging/' + sessionStorage.facilityId + '/' + self.selectedJob().jobCustomerNumber + '/' + self.selectedJob().jobNumber + '/' + selected_users + '/' + tagging_type + '/' + other_facility_id;
    //        var tagg_job_url = serviceURLDomain + 'api/Tagging/' + sessionStorage.facilityId + '/' + self.selectedJob().jobCustomerNumber + '/' + self.selectedJob().jobNumber;
    //        $('#popupJobStatus').popup('close');
    //        //getCORS(tagg_job_url, null, function (response_data) {
    //        postCORS(tagg_job_url, JSON.stringify(tagJobInfo), function (response_data) {
    //            var tagged_info = response_data;
    //            if (response_data == "success") {
    //                $('#alertmsg').text("Job has been tagged/assigned successfully.");
    //                $('#popupDialog a[id=okBut]').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
    //                $('#popupDialog').popup('open');
    //            }

    //        }, function (error_response) {
    //            showErrorResponseText(error_response, true);
    //        });
    //    }
    //};

    self.getData = function (id) {
        var context_data = ko.dataFor($('#' + id)[0]);
        var foundKeys = Object.keys(gData.indexLists).filter(function (key) {
            return gData.indexLists[key] === context_data.groupTitle;
        });
        $('#hdnSelectedGroup').val(context_data.groupTitle);
        //var page_index = $('#ddl' + context_data.groupTitle.replace(/ /g, '_')).val();
        var page_index = context_data.selectedPageValue();
        page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
        var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
        //var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
        var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
        getSelectedData(id, context_data, context_data.groupTitle, gServiceUrl_data, foundKeys[0], page_index, sort_order);
    };

    self.fnSortJobs = function () {
        var sort_order = null;
        //    if ($("#chkInHome").is(':checked') && $("#chkTemplate").is(':checked'))
        //        sort_open_jobs = 3; //both checked
        //    else if ($("#chkTemplate").is(':checked'))
        //        sort_open_jobs = 2; //only template checked
        //    else if ($("#chkInHome").is(':checked'))
        //        sort_open_jobs = 1; //only inhome date checked
        if ($("#chkTemplate").is(':checked'))
            sort_order = 'template'; //only template checked
        else if ($("#chkInHome").is(':checked'))
            sort_order = 'inHomeDate'; //only inhome date checked
        else if ($("#chkJobNumber").is(':checked'))
            sort_order = 'jobNumber'; //only inhome date checked
        else if ($("#chkJobName").is(':checked'))
            sort_order = 'jobName'; //only inhome date checked
        sessionStorage.indexPageSortOrder = sort_order;

        $("#hdnSortOpenJobs").val(sort_order);
        //$('#List2').hasClass('ui-collapsible-expanded')
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            var gServiceUrl_data = "";
            var index_list_name = (foundKeys.length > 0) ? foundKeys[0] : "";
            //gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, index_list_name, page_index, sort_order);
        }


        $('#popupSort').popup('close');
        //$("#col2 div[id^=List]").empty();
        //$("#col3 div[id^=List]").empty();
        //makeGData($("input[type=radio]:checked").val());
    };
    self.getSelectedFacilityJobs = function () {
        clearFilter(false);
        sessionStorage.facilityId = $('#ddlRegions').val();
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            var gServiceUrl_data = "";
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            var index_list_name = (foundKeys.length > 0) ? foundKeys[0] : "";
            //gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, index_list_name, page_index, sort_order);
        }
    };

    self.filterJobs = function (data, el, event) {
        if ((event.srcElement || event.originalTarget) != undefined && (event.srcElement || event.originalTarget).textContent == "Filter") {
            if (!(validateFilters()))
                return false;
        }
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            var gServiceUrl_data = "";
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            var index_list_name = (foundKeys.length > 0) ? foundKeys[0] : "";
            //gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, index_list_name, page_index, sort_order);
        }
        // $('#popupFilters').popup('close');
    };
    self.search = function (data, event) {
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            //var root_context = ko.contextFor($('#' + ctrl_id)[0]).$root;
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            //var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            if (context_data.prevSearchValue() != "")
                page_index = (context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1) ? 0 : parseInt(page_index);
            if (event != undefined) {
                window.setTimeout(function () {
                    getSelectedData(ctrl_id, context_data, selected_target, gServiceUrl_data, foundKeys[0], page_index, sort_order);
                }, 1000);
                //context_data.selectedPageValue(0);
            }
            else {
                context_data.selectedPageValue(0);
            }
            window.setTimeout(function () {
                $('#dv' + selected_target.replace(/ /g, '_')).find('input[data-type=search]').focus();
            }, 1200);
            //(ctrl, context_data, title, service_url, index_list_name, page_index, sort_order, search_term)
        }
    };
    self.searchWithEnterKey = function (data, event) {
        if (event.which == 13) {
            ko.contextFor($('#dv' + $('#hdnSelectedGroup').val().replace(/ /g, '_'))[0]).$parent.searchValue(event.target.value);
            self.search(data, event);
            return false;
        }
        return true;
    };

    self.refreshAdminEmailListConfirm = function () {
        $('#confirmMsg').html("This will update the current email configuration actions for this customer. Do you want to continue?");
        $('#okButConfirm').attr('onclick', 'ko.contextFor(this).$root.refreshAdminEmailList();');
        $('#okButConfirm').text('Ok');
        $('#popupConfirmDialog').popup('open');
        $('#popupConfirmDialog').popup('open');
    };

    self.refreshAdminEmailList = function () {
        getCORS(serviceURLDomain + "api/GizmoEmail_refresh/" + facilityId + "/" + jobCustomerNumber, null, function (data) {
            if (data == "success") {
                $('#alertmsg').text("Admin email actions config has been refreshed successfully.");
                $('#popupConfirmDialog').popup('close');
                window.setTimeout(function () {
                    $('#popupDialog').popup('open');
                }, 500);
            }
        }, function (response_error) {
            $('#alertmsg').text(response_error);
            $('#popupDialog').popup('open');
        });
    };

    self.getUserConfirmationForEmailAction = function (data) {
        var is_send_mail = true;
        if (data.startTime() == "" || data.endTime() == "") {
            is_send_mail = false;
        }
        if (is_send_mail) {
            var st_dt_parts = data.startTime().split('/');
            var st_date = new Date(st_dt_parts[2], st_dt_parts[0] - 1, st_dt_parts[1]).setHours(0, 0, 0, 0);

            var end_dt_parts = data.endTime().split('/');
            var end_date = new Date(end_dt_parts[2], end_dt_parts[0] - 1, end_dt_parts[1]).setHours(0, 0, 0, 0);

            var today = new Date().setHours(0, 0, 0, 0);
            if (today >= st_date && today <= end_date) {
                $('#confirmMsg').html("Are you sure you want to fire the " + data.name() + " email action using the options set up in the email config file?");
                $('#okButConfirm').attr('onclick', 'ko.contextFor(this).$root.sendEmailAction(\'' + data.type() + '\',\'' + data.id() + '\',event);');
                $('#okButConfirm').text('Ok');
                $('#popupConfirmDialog').popup('open');
                $('#popupConfirmDialog').popup('open');
            }
            else {
                is_send_mail = false;
            }
        }

        if (!is_send_mail) {
            $('#alertmsg').html("This email cannot be sent. It is outside of the configured start and end time range.");
            $('#popupDialog').popup('open');
        }
    };
    self.sendEmailAction = function (type, id, event) {
        getCORS(serviceURLDomain + "api/GizmoEmail_basic/" + facilityId + "/" + jobCustomerNumber + "/" + type + "/" + id, null, function (response) {
            if (response == "success") {
                $('#popupConfirmDialog').popup('close');
                var msg = "Your email action was successfully sent.";
                $('#alertmsg').html(msg);
                //$('#popupDialog').popup('open');
                window.setTimeout(function () {
                    $('#popupDialog').popup('open');
                }, 1000);
            }
        }, function (response_error) {
            $('#popupConfirmDialog').popup('close');
            showErrorResponseText(response_error, false);
            //$('#alertmsg').text(response_error.responseText || response_error.statusText);
            //window.setTimeout(function dispMsg() {
            //    $('#popupDialog').popup('open');
            //}, 500);
        });
    };
    self.getUserConfirmationForDelete = function () {
        $('#confirmMsg').html("Are you sure you want to Delete this job? <br />This will delete all job instructions, associated databases and all site entries.");
        $('#okButConfirm').attr('onclick', 'ko.contextFor(this).$root.deleteJob(ko.contextFor(this).$data,event);');
        $('#popupJobStatus').popup('close');
        $('#popupConfirmDialog').popup('open');
        $('#popupConfirmDialog').popup('open');
    };
    //self.deleteJob = function (data, event) {
    //    var delete_job_json = { "customerNumber": data.selectedJob().jobCustomerNumber, "facilityId": data.selectedJob().facilityId, "jobNumber": data.selectedJob().jobNumber };
    //    ///
    //    var gdelete_service_url = serviceURLDomain + "api/Job_delete";
    //    postCORS(gdelete_service_url, JSON.stringify(delete_job_json), function (response_data) {
    //        $('#popupConfirmDialog').popup('close');
    //        var msg = "The following resources have been deleted for Job Number<b>" + response_data.jobNumber + '</b>';
    //        msg += '<ul>';
    //        $.each(response_data, function (key, val) {
    //            if (key.toLowerCase() != "jobnumber") {
    //                msg += '<li>' + val + '</li>';
    //            }
    //        });
    //        msg += '</ul>';
    //        $('#alertmsg').html(msg);
    //        window.setTimeout(function () {
    //            $('#popupDialog').popup('open');
    //        }, 1000);
    //        var selected_target = $('#hdnSelectedGroup').val();
    //        var foundKeys = [];
    //        var page_index = 0;
    //        var context_data = "";
    //        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
    //        if (selected_target != "") {
    //            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
    //                return gData.indexLists[key] === selected_target;
    //            });
    //            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
    //            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
    //            //var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
    //            var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
    //            page_index = context_data.selectedPageValue();
    //            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
    //            page_index = (context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1) ? 0 : parseInt(page_index);
    //            getSelectedData(ctrl_id, context_data, selected_target, gServiceUrl_data, foundKeys[0], page_index, sort_order);
    //        }
    //        //{"jobNumber":-14706,"database":"No database dropped for negative job numbers.","scheduler":"Deleted scheduler and other administrative entries.","dirProject":"Deleted project directories for this job."}
    //    }, function (error_response) {
    //        $('#popupConfirmDialog').popup('close');
    //        showErrorResponseText(error_response, true);

    //    });

    //};
};

function openPopups(ctrl) {
    $('#' + ctrl).find('select').val('-1').selectmenu('refresh');
    $('#popupFilters').popup('open');
}

function getSelectedData(ctrl, context_data, title, service_url, index_list_name, page_index, sort_order) {
    $('#' + ctrl).data('collapsed', false);
    var post_json = {};
    //post_json["pageIndex"] = (context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1 || context_data.searchValue() == "") ? 0 : parseInt(page_index); //(context_data.searchValue() == '') ? parseInt(page_index) : 0;
    post_json["pageIndex"] = page_index;
    post_json["indexListName"] = index_list_name;
    post_json["sortBy"] = sort_order;
    if (context_data.searchValue() != "")
        post_json["searchTerm"] = context_data.searchValue();

    //if (appPrivileges.roleName == "user")
    //    post_json["assignedList"] = [{ "userId": sessionStorage.username}];

    var filter_users = $('#ulSelectedUsers li a[value!=""][title!=Remove]').attr('value');
    if (filter_users != undefined && filter_users != null && filter_users != "")
        post_json["assignedList"] = [{ "userId": filter_users }];

    if (index_list_name == "myTaggedJobs") {
        post_json["assignedList"] = [{ "userId": sessionStorage.username }];
        //post_json["indexListName"] = 'openJobs';
    }
    if ($('#ddlFilterMilestones').length > 0 && $('#ddlFilterMilestones').val() != -1) {
        var ms_search = {};
        ms_search["milestone"] = $('#ddlFilterMilestones').val();
        ms_search["fromDate"] = $('#dtpStart').val();
        ms_search["toDate"] = $('#dtpEnd').val();
        post_json["milestoneSearch"] = ms_search;
    }
    //    "milestoneSearch":
    //{ "milestone": "inhome", "fromDate": "12/29/2014", "toDate": "01/01/2015" }

    var facility_id = "";
    facility_id = sessionStorage.facilityId;
    if ((appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) && $('#ddlRegions').val() != "select")
        facility_id = $('#ddlRegions').val();
    post_json["facilityList"] = [{ "facilityId": facility_id }];
    //post_json["searchTerm"] = sort_order; //TO BE REMOVED
    var jobs_list = [];
    postCORS(service_url, JSON.stringify(post_json), function (response_data) {
        //jobs_list.push(new jobGroupModel(key, false, val));
        if (appPrivileges.roleName == "admin" && index_list_name == "jobsWithProcessingErrors") {// && response_data.jobs.length>0
            response_data.indexLists["jobsWithProcessingErrors"] = "Jobs With Processing Errors";
            response_data["jobs"] = processErrorData.jobs;
            response_data["target"] = processErrorData.target;
            response_data["totalCount"] = processErrorData.totalCount;
            response_data["startIndex"] = processErrorData.startIndex;
            response_data["finalIndex"] = processErrorData.finalIndex;
        }
        //if (appPrivileges.customerNumber == "1") {
        if (appPrivileges.roleName == "admin") {
            //My Tagged Jobs, Assigned Jobs 
            //response_data.indexLists["myTaggedJobs"] = "My Tagged Jobs";
            //response_data.indexLists["assignedJobs"] = "Assigned Jobs";
        }
        var job_name = "", in_home = "";
        if (response_data.jobs != undefined && response_data.jobs != null && response_data.jobs != "") {
            $.each(response_data.jobs, function (key, val) {
                job_name = (val["Job Name"] != undefined && val["Job Name"] != null && val["Job Name"] != "") ? val["Job Name"] : '';
                in_home = (val["In-Home"] != undefined && val["In-Home"] != null && val["In-Home"] != "") ? 'In-Home: <b>' + val["In-Home"] + '</b>' : '';
                var in_home_theme = '';
                if (val["In-Home"] != undefined && val["In-Home"] != null && val["In-Home"] != "") {
                    var due_date = new Date(val["In-Home"]);
                    var curr_date = new Date();
                    var dt_diff = daysBetween(curr_date, due_date);
                    if (response_data.target.toLowerCase().indexOf('open') == -1)
                        in_home_theme = (dt_diff < 0) ? 'red' : '';
                }
                in_home = (in_home == "") ? ((val["Job Number"] != undefined && val["Job Number"] != null && val["Job Number"] != "") ? '<font color=' + in_home_theme + '>Job Number:<b>' + val["Job Number"] + '</b></font>' : '') : '<font color=' + in_home_theme + '>' + in_home + '</font>';
                in_home = (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == OH_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER || appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER) ? "" : in_home;
                var index = 0;
                var previous_split_index = 0;
                var temp_attr = "";
                var filter_text = "";
                var break_index = ((appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == OH_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER || appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER) ? 2 : 3);
                var elements_toSkip = ["isGizmo", "Facility ID", "Customer Number", "Customer Name", "Job Name", "Created By User"];
                var attr_list = "";
                var temp_job_attr_key = "";
                $.each(val, function (job_attr_key, job_attr_val) {
                    if (job_attr_val != "" || job_attr_val == 0) {
                        if (((appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == OH_CUSTOMER_NUMBER || appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER) && (job_attr_key != "isGizmo")) || (appPrivileges.customerNumber != DCA_CUSTOMER_NUMBER && appPrivileges.customerNumber != KUBOTA_CUSTOMER_NUMBER && appPrivileges.customerNumber != CW_CUSTOMER_NUMBER && appPrivileges.customerNumber != OH_CUSTOMER_NUMBER && appPrivileges.customerNumber != SK_CUSTOMER_NUMBER && appPrivileges.customerNumber != GWA_CUSTOMER_NUMBER && appPrivileges.customerNumber != CASEYS_CUSTOMER_NUMBER) || ((appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER) && (jQuery.inArray(job_attr_key, elements_toSkip) == -1))) {
                            filter_text += (filter_text != "") ? ' | ' + job_attr_val : job_attr_val;
                            //if (job_attr_key == "In-Home") date_name = job_attr_key;
                            //if (job_attr_key != "Job Name" && job_attr_key != "In-Home" && job_attr_key != "Customer Number" && job_attr_key != "Customer Name" && job_attr_key != "Facility ID") {
                            temp_attr = ((val["In-Home"] == undefined || val["In-Home"] == null) && job_attr_key == "Job Number") ? "" : job_attr_key;
                            if (temp_attr != "") {
                                temp_job_attr_key = ((appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER) && (job_attr_key == "Entry Point")) ? "Entry" : job_attr_key;
                                if ((index - previous_split_index) == break_index) {
                                    attr_list += "<br/>";
                                }
                                if (temp_job_attr_key == "Size" && job_attr_val == "x") {
                                    var size_val = '';
                                    attr_list += (attr_list != "") ? ((((index - previous_split_index) < break_index) ? " | " : "") + temp_job_attr_key + ": " + size_val) : "<p style='margin-top:-2px;'>" + (temp_job_attr_key + ": " + size_val);
                                }
                                else
                                    attr_list += (attr_list != "") ? ((((index - previous_split_index) < break_index) ? " | " : "") + temp_job_attr_key + ": " + ((job_attr_val != null) ? job_attr_val : "")) : "<p style='margin-top:-2px;'>" + (temp_job_attr_key + ": " + ((job_attr_val != null) ? job_attr_val : ""));
                            }
                            if ((index - previous_split_index) == break_index)
                                previous_split_index = index;
                            index++;
                        }
                    }
                });
                if (attr_list != "")
                    attr_list += '</p>';
                var mile_stone_icon = getMilestoneIcon(val);
                var mile_stone_alt_text = mile_stone_icon.substring(mile_stone_icon.lastIndexOf('/') + 1, mile_stone_icon.lastIndexOf('.')).replace('icon', '').replace('warning', '').replace(/_/g, ' ');
                var job_number = (val["Job Number"] != undefined && val["Job Number"] != null && val["Job Number"] != "") ? parseInt(val["Job Number"]) : '';
                //var facility_id = (val["Facility ID"] != undefined && val["Facility ID"] != null) ? eval(val["Facility ID"]) : '1';
                //sessionStorage.facilityId = facility_id;
                var job_customer_number = (val["Customer Number"] != undefined && val["Customer Number"] != null && val["Customer Number"] != "") ? parseInt(val["Customer Number"]) : appPrivileges.customerNumber;
                var data_theme = '';
                if (job_number > -1)
                    if (val["isGizmo"] != undefined && val["isGizmo"] != null && val["isGizmo"] == 0) {
                        data_theme = 'j';
                    }
                    else
                        data_theme = 'c';
                else
                    if (val["isGizmo"] != undefined && val["isGizmo"] != null && val["isGizmo"] == 0) {
                        data_theme = 'j';
                    }
                    else
                        data_theme = 'g';

                var job_item = new JobModel(job_name, in_home, attr_list, filter_text, data_theme, job_number, job_customer_number, sessionStorage.facilityId, mile_stone_icon, mile_stone_alt_text, val["isGizmo"]);

                //var job_item = new JobModel(job_name, in_home, attr_list, filter_text, ((job_number > -1) ? '' : 'e'), job_number, job_customer_number, facility_id, mile_stone_icon, mile_stone_alt_text, val["isGizmo"]);
                jobs_list.push(job_item);
            });
        }
        //var is_show_region = (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) ? true : false;
        //var paging_info = createPagerObject(val);
        //context_data.listViewData(jobs_list);
        //col1_data.push(new jobGroupModel(context_data.groupTitle, is_show_region, jobs_list, '', '1', '', sessionStorage.facilityId));
        //$('#' + ctrl).trigger('expand');
        context_data.prevSearchValue(context_data.searchValue());
        context_data.regionTitle = getTribuneFacilityName(sessionStorage.facilityId);
        context_data.pagingData.removeAll();
        context_data.pagingData(createPagerObject1(response_data.totalCount));
        var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
        var is_more_visible = (context_data.pagingData().length > 0 && parseInt(selected_page_value + 1) < context_data.pagingData().length) ? true : false;
        context_data.showMoreOption(is_more_visible);
        context_data.listViewData.removeAll();
        context_data.listViewData(jobs_list);
        //        if (jobs_list.length == 0)
        //            context_data.searchValue('');
        //$("#col2").trigger("create");
        //$("#" + +ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
        var ctrl_id_target = ctrl;
        if (ctrl.indexOf('btn') > -1 || ctrl.indexOf('ddl') > -1)
            ctrl_id_target = 'dv' + ctrl.replace('ddl', '').replace('btn', '');
        // || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName != "admin" && !is_valid_domain)
        if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName != "admin" && !is_valid_domain)) {
            //$("#demoListJobs").find('ul').trigger("create").listview().listview('refresh');
            //$('#demoListJobs').collapsibleset('refresh');
            $("#demoListJobs").find('ul').listview().listview('refresh');
            $("div[id='demoListJobs']").find('ul').find('select').selectmenu().selectmenu('refresh');
            $("div[id='demoListJobs']").find('ul').find("input[data-type=search]").parent().trigger('create');
            $('#demoListJobs').collapsibleset('refresh');
            $('.thermometer-noconfig').thermometer({
                speed: 'slow'
            });
            //$('#demoListJobs').trigger('create');
        }
        else if (jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
            //$("#dvJetsDemoJobs").find('ul').trigger("create").listview().listview('refresh');
            $("#dvJetsDemoJobs").find('ul').listview().listview('refresh');
            $("div[id='dvJetsDemoJobs']").find('ul').find('select').selectmenu().selectmenu('refresh');
            $("div[id='dvJetsDemoJobs']").find('ul').find("input[data-type=search]").parent().trigger('create');
            $('#dvJetsDemoJobs').collapsibleset('refresh');
            $('.thermometer-noconfig').thermometer({
                speed: 'slow'
            });
            //$('#dvJetsDemoJobs').trigger('create');
        }
        else {
            $("#" + ctrl_id_target).find('ul').listview().listview('refresh');
            if (ctrl_id_target.indexOf('dv') != 0)
                ctrl_id_target = "dv" + ctrl_id_target;

            $("div[id='" + ctrl_id_target + "']").find('ul').listview().listview('refresh');
            $("div[id='" + ctrl_id_target + "']").find('ul').find('select').selectmenu().selectmenu('refresh');
            $("div[id='" + ctrl_id_target + "']").find('ul').find("input[data-type=search]").parent().trigger('create');
            $("div[id='" + ctrl_id_target + "']").collapsibleset().collapsibleset('refresh');
            //$("#" + ctrl_id_target).find('ul').trigger("create").listview().listview('refresh');
            //window.setTimeout(function setUI() {
            //    $("div[id='dv" + ctrl_id_target + "']").find('ul').listview('refresh').trigger('create');
            //    $("div[id='dv" + ctrl_id_target + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            //    $("div[id='dv" + ctrl_id_target + "']").find('ul').find("input[data-type=search]").trigger('create');
            //}, 5);
            $('.thermometer-noconfig').thermometer({
                speed: 'slow'
            });

        }
        if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER && (sessionStorage.isCoopDisplayed == undefined || sessionStorage.isCoopDisplayed == null || sessionStorage.isCoopDisplayed == "" || sessionStorage.isCoopDisplayed == false)) {
            sessionStorage.isCoopDisplayed = true;
            loadPopup();
        }

    }, function (error_response) {
        showErrorResponseText(error_response, true);
        //var is_show_region = (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) ? true : false;
        //var paging_info = createPagerObject(val);
        //context_data.listViewData(jobs_list);
        //col1_data.push(new jobGroupModel(context_data.groupTitle, is_show_region, jobs_list, '', '1', '', sessionStorage.facilityId));
        //$('#' + ctrl).trigger('expand');
        context_data.prevSearchValue(context_data.searchValue());
        context_data.regionTitle = getTribuneFacilityName(sessionStorage.facilityId);
        context_data.pagingData(createPagerObject1(0));
        var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
        var is_more_visible = (context_data.pagingData().length > 0 && parseInt(selected_page_value + 1) < context_data.pagingData().length) ? true : false;
        context_data.showMoreOption(is_more_visible);
        context_data.listViewData.removeAll();
        context_data.listViewData(jobs_list);
        //        if (jobs_list.length == 0)
        //            context_data.searchValue('');
        //$("#col2").trigger("create");
        //$("#" + +ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
        var ctrl_id_target = ctrl;
        if (ctrl.indexOf('btn') > -1 || ctrl.indexOf('ddl') > -1)
            ctrl_id_target = 'dv' + ctrl.replace('ddl', '').replace('btn', '');

        $("#" + ctrl_id_target).find('ul').trigger("create").listview().listview('refresh');
        window.setTimeout(function () {
            $("div[id='dv" + ctrl_id_target + "']").find('ul').listview('refresh').trigger('create');
            $("div[id='dv" + ctrl_id_target + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            $("div[id='dv" + ctrl_id_target + "']").find('ul').find("input[data-type=search]").trigger('create');
            if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER) {
				sessionStorage.isCoopDisplayed = true;
                loadPopup();
            }
        }, 5);
    });
}


function makeGData() {
    //if (appPrivileges.customerNumber == CW_CUSTOMER_NUMBER || appPrivileges.customerNumber == REGIS_CUSTOMER_NUMBER || appPrivileges.customerNumber == "203") {

    //gServiceUrl = serviceURLDomain + "api/NexusIndex/" + appPrivileges.customerNumber;
    var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
    //var page_url = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
    var page_url = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
    //    if ($('#ddlRegions').val() != undefined && $('#ddlRegions').val() != null)
    //        gServiceUrl = gServiceUrl + "/" + $('#ddlRegions').val();

    //        if (user_jobs_only == 'myjobs')
    //            gServiceUrl = gServiceUrl + '/' + sessionStorage.username; //userid

    //        gServiceUrl = gServiceUrl + '/' + $("#hdnSortOpenJobs").val();
    //}

    $.each($('input[id^=search]'), function () {
        $(this).val('');
    });
    gDataStartIndex = 0;
    gDataList = [];
    var post_json = {};
    post_json["pageIndex"] = 0;
    post_json["indexListName"] = "";
    post_json["sortBy"] = sort_order;
    // post_json["searchTerm"] = ""; // TO BE Removed
    //if (appPrivileges.roleName == "user")
    //    post_json["assignedList"] = [{ "userId": sessionStorage.username}];

    var facility_id = "";
    facility_id = "1";
    if ($('#ddlRegions').length > 0 && $('#ddlRegions').val() != "select")
        facility_id = $('#ddlRegions').val();
    post_json["facilityList"] = [{ "facilityId": facility_id }];
    if (vm != undefined && vm != null && vm.person() != undefined) {
        //$("#List2").find('div[data-role=collapsible]').remove();
        //var temp_vm = new makeViewModel();
        //vm.person(new makeViewModel());
        //$("#col2").trigger("create");
        //$("#col3").trigger("create");
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";

        if (selected_target != "") {
            var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            var gServiceUrl_data = "";
            sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            var index_list_name = (foundKeys.length > 0) ? foundKeys[0] : "";
            //gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, index_list_name, page_index, sort_order);
        }
    } else {
        postCORS(page_url, JSON.stringify(post_json), function (data) {
            gData = data;
            // && appPrivileges.roleName == "admin"
            if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && (is_valid_domain || appPrivileges.roleName == "admin")) {
                if (data.defaultJobName != undefined && data.defaultJobName != null && data.defaultJobNumber != undefined && data.defaultJobNumber != null) {
                    var temp_default_job_info = {};
                    temp_default_job_info["defaultJobName"] = data.defaultJobName;
                    temp_default_job_info["defaultJobNumber"] = data.defaultJobNumber;
                    sessionStorage.defaultJobDetails = JSON.stringify(temp_default_job_info);

                    if (data.defaultJobNumber != "-1") {
                        $('#lnkNewJob').css('display', 'none');
                        $('#lnkEditJob').css('display', 'block');
                    }
                }
                // && appPrivileges.roleName == "admin"
                if (data.defaultJobNumber == "-1" && (is_valid_domain || appPrivileges.roleName == "admin"))
                    data.defaultJobNumber = "-16875";
            }

            //            if (appPrivileges.roleName == "admin") {
            //                gData.indexLists["jobsWithProcessingErrors"] = "Jobs With Processing Errors";
            //            }
            //if (appPrivileges.customerNumber == "1") {
            if (appPrivileges.roleName == "admin") {
                //My Tagged Jobs, Assigned Jobs 
                //gData.indexLists["myTaggedJobs"] = "My Tagged Jobs";
                //gData.indexLists["assignedJobs"] = "Assigned Jobs";
            }
            //buildLists();
            //if (viewModel.person() == undefined) {
            if (vm == undefined) {
                //vm = new makeViewModel();
                vm = new viewModel();
                //ko.cleanNode($('#dvPageDoc')[0]);
                vm.person(new makeViewModel());
                ko.applyBindings(vm);
                $("#col2 div[data-role=collapsible-set]").collapsibleset('refresh');
                $("#col3 div[data-role=collapsible-set]").collapsibleset('refresh');
                // $("#col2").trigger("create");
                // $("#col3").trigger("create");
                // window.setTimeout(function delay() {
                if (appPrivileges.customerNumber != "1" && appPrivileges.customerNumber != REDPLUM_CUSTOMER_NUMBER) {
                    //$('div.ui-collapsible-content', '#dvReports').addClass('ui-corner-all');
                    //$('div.ui-collapsible-content', "#dvReports").trigger('expand');
                }
                //            $.each($('#dvDailyTaskList div[data-role="collapsible"]'), function (a, b) {
                //                if (a == 0)
                //                    $(b).removeClass('ui-corner-bottom');
                //                else if (a == ($('#dvDailyTaskList  div[data-role="collapsible"]').length - 1))
                //                    $(b).removeClass('ui-corner-top');
                //                else {
                //                    $(b).removeClass('ui-corner-top ui-corner-bottom');
                //                }
                //            });
                var source = (jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) ? "div[id=col2]" : (jobCustomerNumber == BBB_CUSTOMER_NUMBER || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && !is_valid_domain) || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? "div[id=demoListJobs]" : "div[id = dvJetsDemoJobs]";
                //var source = (jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) ? "div[id=col2]" : (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? "div[id=demoListJobs]" : "div[id = dvJetsDemoJobs]";
                $.each($(source + ' div[data-role=collapsible]'), function () {
                    $(this).on("collapsibleexpand", function () {
                        $(this).data("previous-state", "expanded");
                        //window.setTimeout(function searchClearBindEvent() {
                        $('#' + this.id + 'a.ui-input-clear').bind('onclick', function () { });
                        //}
                        //, 1000);
                        $('.ui-content').on('click', '.ui-input-clear', function () {
                            vm.search();
                        });
                        vm.getData(this.id);
                    });

                    $(this).on("collapsiblecollapse", function () {
                        $(this).data("previous-state", "collapsed");
                        //$('#hdnSelectedGroup').val('');
                    });
                });
                //}, 500);
                if (jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && !is_valid_domain && appPrivileges.roleName != "admin") || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                    //if (jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                    //window.setTimeout(function () {
                    source = ((jobCustomerNumber == AAG_CUSTOMER_NUMBER && !is_valid_domain && appPrivileges.roleName != "admin") || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? "demoListJobs" : "dvJetsDemoJobs";
                    //source = (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? "demoListJobs" : "dvJetsDemoJobs";
                    //$('#' + source + ' div[data-role=collapsible]').trigger('click')
                    window.setTimeout(function () { $('#' + source + ' div[data-role=collapsible]').collapsible('expand'); }, 100);

                    //}, (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 1000 : 1000);
                }

            }

            //window.setTimeout(function loadDropdowns() {
            vm.loadTagRegionsDDL(facilityJSON, 'filter');
            vm.loadDepartmentsDDL(departmentsJson, 'filter');
            vm.loadTagUsersDDL(usersJson, 'filter');
            $('#fldSetFilterOptions').controlgroup('refresh');
            //}, 1000);
            window.setTimeout(function () {
                $('#fldSetFilterOptions').controlgroup('refresh');
                $('#ulEmailList').listview('refresh');
                //$('#dvDailyTaskList').collapsibleset('refresh');
            }, 2000);

        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    }
    //displayReportsList();

    //$('#dvOpen_job').
}

function getPendingApprovalPercent(job_name, job_number) {
    if (sessionStorage.caseysThermometersStatus != undefined && sessionStorage.caseysThermometersStatus != null && sessionStorage.caseysThermometersStatus != "") {
        var job_schedule = $.parseJSON(sessionStorage.caseysThermometersStatus);
        var curr_date = new Date();
        curr_date = new Date(curr_date.getMonth() + 1 + "/" + curr_date.getDate() + "/" + curr_date.getFullYear());
        var percent = "";
        var prev_end_date = "";
        var index = 0;
        if (job_schedule[job_number] != undefined && job_schedule[job_number] != null && job_schedule[job_number] != "") {
            $.each(job_schedule[job_number], function (key, val) {
                if (key.toLowerCase() == "optin" && curr_date < (new Date(val.startDate))) {
                    percent = 0;
                }
                else if (curr_date <= (new Date(val.endDate)) && index == 0) {
                    percent = val.percent;
                    return false;
                }
                else if (prev_end_date != "" && curr_date > prev_end_date && curr_date <= (new Date(val.endDate))) {
                    percent = val.percent;
                    return false;
                }
                prev_end_date = new Date(val.endDate);
                index++;
            });
        }
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var job_month = job_name.substring(0, job_name.indexOf(' '));
        var job_year = job_name.substring(job_name.indexOf(' ') + 1, ((job_name.indexOf(' ') + 1) + 4));
        job_month = (months.indexOf(job_month) > -1) ? months.indexOf(job_month) : -1;
        var curr_month = (new Date()).getMonth();
        var curr_year = (new Date()).getFullYear();
        if (percent == 0 && job_month > -1 && (job_month < curr_month && job_year >= curr_year))
            percent == "0";
        else if (percent == "")
            percent = "100";
        return percent;
    } else {
        var count = 0;
        for (var x = 0, c = ''; c = job_name.charAt(x) ; x++) {
            count += c.charCodeAt(0);
        }
        count = count % 100;
        count = count < 15 ? 100 : count;
        return count;
    }
};

function getPendingApprovalStatus(pending_approval_percent,job_number) {
    /*if (pending_approval_percent <= 15)
        return 'Open';
    else if (pending_approval_percent <= 30)
        return 'In Processing';
    else if (pending_approval_percent <= 45)
        return 'In Approval';
    else if (pending_approval_percent <= 60)
        return 'In Production';
    else if (pending_approval_percent <= 75)
        return 'In Shipping';
    else if (pending_approval_percent <= 90)
        return 'Mailed';
    else if (pending_approval_percent <= 100)
        return 'In Home';
    else
        return '';
        */
    if (sessionStorage.caseysThermometersStatus != undefined && sessionStorage.caseysThermometersStatus != null && sessionStorage.caseysThermometersStatus != "") {
        var job_schedule = $.parseJSON(sessionStorage.caseysThermometersStatus);
        var curr_date = new Date();
        var status = "Open";
        var prev_end_date = "";
        var index = 0;
        if (job_schedule[job_number] != undefined && job_schedule[job_number] != null && job_schedule[job_number] != "") {
            $.each(job_schedule[job_number], function (key, val) {
                $.each(val, function(key1, val1){
                    if(key1.toLowerCase() =='status' && val['percent'] == pending_approval_percent){
                        status = val1;
                    }
                });
            });
        }
        return status;
    } else {
        return status;
    }
}

function makeViewModel() {
    var self = this;
    var job_name = "";
    var jobs_list = [];
    var attr_list = "";
    var grouped_jobs_col1 = [];
    var grouped_jobs_col2 = [];
    var is_show_region = false, show_tagging = false;
    var paging_info = [];
    self.col1GroupWisePagedJobs = []; // ko.observableArray([]);
    self.col2GroupWisePagedJobs = []; // ko.observableArray([]);
    self.myTaskSummary = []; // ko.observableArray([]);
    self.demoEmailsList = [];
    $.each(gData.indexLists, function (key, val) {
        if (jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER) {
            if (jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
                jobs_list = [];
                is_show_region = (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) ? true : false;
                paging_info = createPagerObject1(jobs_list.length);
                //var show_tagging = (appPrivileges.roleName == "admin" && appPrivileges.customerNumber == "1") ? true : false;
                show_tagging = false;
                grouped_jobs_col1.push(new jobGroupModel(val, is_show_region, jobs_list, paging_info, '0', '', sessionStorage.facilityId, show_tagging, true));
            }
                //else if (((((jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin") || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && val.toLowerCase() == "open jobs") || (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && appPrivileges.roleName != "user" && (val.toLowerCase() == "open jobs")) || (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && appPrivileges.roleName != "admin" && val.toLowerCase() == "open jobs")) {
            else if (((((jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin") || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && !is_valid_domain) || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && val.toLowerCase() == "open jobs") || (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && appPrivileges.roleName != "user" && (val.toLowerCase() == "open jobs")) || (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && appPrivileges.roleName != "admin" && val.toLowerCase() == "open jobs")) {
                jobs_list = [];
                is_show_region = (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) ? true : false;
                paging_info = createPagerObject1(jobs_list.length);
                //var show_tagging = (appPrivileges.roleName == "admin" && jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != MOTIV8_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER) ? true : false;
                show_tagging = false;
                grouped_jobs_col2.push(new jobGroupModel(val, is_show_region, jobs_list, paging_info, '0', '', sessionStorage.facilityId, show_tagging, true));
            }
        }
    });
    if ((jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin") {
        $.each(demoEmailsList, function (key, val) {
            self.demoEmailsList.push(new demoEmailListModel(val));
        });
    }
    if (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) {
        var my_tasks_summary = [];
        var tasks_cnt = "";
        var temp_task_list = [];
        is_show_region = (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER) ? true : false;
        var total_tasks_cnt = 0;
        $.each(dailyTaskUrlData, function (key, val) {
            paging_info = createPagerObject1(val.length);
            $.each(val, function (key1, val1) {
                tasks_cnt = '<b>Tasks: ' + val1["Tasks"] + '</b>';
                job_name = (val1["Job Name"] != undefined && val1["Job Name"] != null && val1["Job Name"] != "") ? val1["Job Name"] : '';
                var index = 0;
                var previous_split_index = 0;
                var temp_attr = "";
                var filter_text = "";
                attr_list = "";
                $.each(val1, function (job_attr_key, job_attr_val) {
                    if (job_attr_key != "Tasks") {
                        filter_text += (filter_text != "") ? ' | ' + job_attr_val : job_attr_val;
                        temp_attr = (job_attr_key == "Job Name") ? "" : job_attr_key;
                        //if (job_attr_key == "Job Name") temp_attr = ""; else temp_attr = job_attr_key;
                        if (job_attr_val != "" || job_attr_val == 0) {
                            if (temp_attr != "") {
                                if ((index - previous_split_index) == 3) {
                                    attr_list += "<br/>";
                                }
                                attr_list += (attr_list != "") ? ((((index - previous_split_index) < 3) ? " | " : "") + job_attr_key + ": " + ((job_attr_val != null) ? job_attr_val : "")) : "<p>" + (job_attr_key + ": " + ((job_attr_val != null) ? job_attr_val : ""));
                                if ((index - previous_split_index) == 3)
                                    previous_split_index = index;
                                index++;
                            }
                        }
                    }
                });
                if (attr_list != "")
                    attr_list += '</p>';
                //temp_task_list.push(new DailyTaskModel(job_name, "", attr_list, 'c', '', val1.Status, '', ((appPrivileges.roleName == "admin") ? 1 : 0)));
                temp_task_list.push(new JobModel(job_name, tasks_cnt, attr_list, filter_text, 'c', val1["Job Number"], appPrivileges.customerNumber, sessionStorage.facilityId, '', '', 0));
                total_tasks_cnt = parseInt(total_tasks_cnt) + parseInt(val1["Tasks"]);
            });
            my_tasks_summary.push(new jobGroupModel("My Task Summary", is_show_region, temp_task_list, paging_info, 0, '', sessionStorage.facilityId, show_tagging));
        });
        self.totalTasksCount = ko.observable();
        self.totalTasksCount(total_tasks_cnt);
        self.myTaskSummary = my_tasks_summary;
    }
    self.col1GroupWisePagedJobs = grouped_jobs_col1;
    self.col2GroupWisePagedJobs = grouped_jobs_col2;
    self.searchedJobs = ko.observableArray([]);
}

function manageFocusAndCursor(ctrl) {
    //var strLength = $(ctrl).val().length;
    //ctrl[0].setSelectionRange(strLength, strLength);
    //ctrl.focus(); //sets focus to element
    var val = ctrl.value; //store the value of the element
    ctrl.value = ''; //clear the value of the element
    ctrl.value = val; //set that value back.  
}

function createPagerObject1(total_jobs) {
    var pager_length = Math.ceil(total_jobs / gPageSize);
    var pager_object = [];
    if (pager_length > 1) {
        for (var j = 0; j < pager_length; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j) });
        }
    }
    return pager_object;
}


function createPagerObject(val) {
    var pager_length = Math.ceil(val.length / gPageSize);
    var pager_object = [];
    if (pager_length > 1) {
        for (var j = 0; j < pager_length; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j + 1) });
        }
    }
    return pager_object;
}


function getMilestoneIcon(job_attrs) {
    var one_day = 1000 * 60 * 60 * 24;
    var dt_diff = "";
    var due_date = "";
    var icon_type = '';
    var curr_date = new Date();
    var job_mile_stones = job_attrs["Status"];
    var mile_stone_key = '';
    //var attr_val = "";
    //var val_scheduled_date = "";
    $.each(job_attrs, function (key, val) {
        //attr_val = "";
        if (job_mile_stones != undefined && job_mile_stones != null && job_mile_stones != '' && job_mile_stones.indexOf(key.toLowerCase().replace(/ /g, '')) > -1) {
            mile_stone_key = key.toLowerCase().replace(/ /g, '_');
            //if (val == "1/1/1900 12:00:00 AM")
            //    attr_val = "";
            if (val != undefined && val != null && val != "") { //  
                due_date = new Date(val);
                //val_scheduled_date = (val.indexOf(' ') == -1) ? val : val.substring(0, val.indexOf(' '));
                dt_diff = Math.floor((due_date.getTime() - curr_date.getTime()) / one_day);
                if (dt_diff < 0) {
                    icon_type = '_warning';
                    return false;
                }
            }
        }
    });
    var mile_stone_icon = "";


    if (mile_stone_key.indexOf('in-home') > -1)
        mile_stone_icon = 'icon_in_home';
    else {
        //if (job_mile_stones.indexOf('start') > -1 || job_mile_stones.indexOf('end') > -1)
        //    mile_stone_icon = 'icon' + job_mile_stones.substring(0, (job_mile_stones.indexOf('start') || job_mile_stones.indexOf('end'))).replace(/ /g, '').replace(/-/g, '').replace(/_/g, '');
        //else
        mile_stone_icon = 'icon' + job_mile_stones;
    }

    if (mile_stone_icon.endsWith('_'))
        mile_stone_icon = mile_stone_icon.substring(0, mile_stone_icon.length - 1);
    if (icon_type != '')
        mile_stone_icon = mile_stone_icon.toLowerCase() + 'warning.png';
    else if (mile_stone_icon.length > 8)
        mile_stone_icon = mile_stone_icon.toLowerCase() + '.png';

    return (mile_stone_icon.length > 8) ? mile_stone_icon : "";
}
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

//function fnSortJobs() {
//    var sort_open_jobs = 0;
////    if ($("#chkInHome").is(':checked') && $("#chkTemplate").is(':checked'))
////        sort_open_jobs = 3; //both checked
////    else if ($("#chkTemplate").is(':checked'))
////        sort_open_jobs = 2; //only template checked
////    else if ($("#chkInHome").is(':checked'))
////        sort_open_jobs = 1; //only inhome date checked
//    if ($("#chkTemplate").is(':checked'))
//        sort_open_jobs = 'template'; //only template checked
//    else if ($("#chkInHome").is(':checked'))
//        sort_open_jobs = 'inHomeDate'; //only inhome date checked
//    else if ($("#chkJobNumber").is(':checked'))
//        sort_open_jobs = 'jobNumber'; //only inhome date checked
//    else if ($("#chkJobName").is(':checked'))
//        sort_open_jobs = 'jobName'; //only inhome date checked
//    sessionStorage.indexPageSortOrder = sort_open_jobs;

//    $("#hdnSortOpenJobs").val(sort_open_jobs);
//    $('#popupSort').popup('close');
//    //$("#col2 div[id^=List]").empty();
//    //$("#col3 div[id^=List]").empty();

//    
//    makeGData($("input[type=radio]:checked").val());
//}

//function getSelectedFacilityJobs() {
//    sessionStorage.facilityId = $('#ddlRegions').val();
//    makeGData("alljobs");
//}

function loadCustomerConfigData() {
    //if (Object.keys(customconfigData).length == 0) {
    var cust_config_data_url = serviceURLDomain + "api/NexusIndex_config/" + appPrivileges.customerNumber;
    getCORS(cust_config_data_url, null, function (data) {
        customconfigData = data;
        bindConfigData();
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
    //    }
    //    else {
    //        bindConfigData()
    //    }

}

String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

function bindConfigData() {
    var config_data = [];

    $.each(customconfigData, function (key1, val1) {
        var div_count = (val1 != undefined && val1 != null && val1 != "" && val1.length > 0) ? Math.round(val1.length / 2) : 0;
        var group_data = [];
        var col_id = 1;
        $.each(val1, function (key2, val2) {
            if (key2 + 1 <= div_count)
                group_data.push(val2);
            else {
                if (key2 + 1 == div_count + 1) {
                    var temp = {};
                    temp[key1 + col_id] = group_data;
                    config_data.push(temp);
                    col_id++;
                    group_data = [];
                }
                group_data.push(val2);
            }

            //$('#chk' + val2.value.initCap()).attr('checked', JSON.parse((val2.isChecked != undefined && val2.isChecked != null && val2.isChecked != "") ? val2.isChecked : "false")); //.checkboxradio('refresh');
        });
        if (group_data.length > 0) {
            var temp = {};
            temp[key1 + col_id] = group_data;
            config_data.push(temp);
            group_data = [];
        }
    });
    $('div[id^=dvConfigCol]').empty();
    $.each(config_data, function (key, val) {
        var index = 0;
        var config_fields = "";
        $.each(val, function (key1, val1) {
            $.each(val1, function (key2, val2) {
                if (index == 0)
                    config_fields += '<fieldset data-role="controlgroup"><legend>' + key1.substring(0, key1.length - 1) + ':</legend>';
                config_fields += '<input type="checkbox" name="chk' + val2.value + '" id="chk' + val2.value + '" ' + ((val2.isChecked != undefined && val2.isChecked != null && val2.isChecked != "" && val2.isChecked == "true") ? "checked" : '') + '>';
                config_fields += '<label for="chk' + val2.value + '"><span style="font-size:14px; font-weight:normal;">' + val2.name + '</span></label>';
                index++;
            });
        });
        config_fields += '</fieldset>';
        $('#dvConfigCol' + (key + 1)).append(config_fields);
    });
    $('#dvConfigGrid').trigger('create');
    $('#popupCustomerConfig').popup('open');
}

function saveCustomerConfigData() {
    var saveConfigInfo = [];
    $.each(customconfigData, function (key1, val1) {
        $.each(val1, function (key2, val2) {
            if ($('#chk' + val2.value).attr('checked')) {
                var temp_val2 = $.extend(true, {}, val2);
                temp_val2.isChecked = "true";
                saveConfigInfo.push(temp_val2);
            }
        });
    });
    //if (Object.keys(saveConfigInfo).length > 0) {
    postCORS(serviceURLDomain + "api/NexusIndex_postConfig/" + appPrivileges.customerNumber, JSON.stringify(saveConfigInfo), function () {
        $('#popupCustomerConfig').popup('close');
        $('#alertmsg').text("Configuration saved successfully.");
        $('#popupDialog a[id=okBut]').attr('onclick', '$(\'#popupDialog\').popup(\'open\');$(\'#popupDialog\').popup(\'close\');');
        $('#popupDialog').popup('open');
        //fnSortJobs();

        makeGData();

    }, function (error_response) {
        $('#popupCustomerConfig').popup('close');
        showErrorResponseText(error_response, false);
        // if (!(jQuery.browser.msie)) {
        //     $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
        // }
        // else {
        //     $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
        // }
        // $('#popupDialog').popup('open');
    });
    //}
    // $('#chk' + val1').attr('checked', false).checkbox('refresh');
}

// fuctionality for the Advanced search
function loadAdvanceSearchData() {
    $('#dvSearchFields').empty();
    $.getJSON(advanceSearchUrl, function (data) {
        advanceSearchData = data;
        fillDeafaultSearchDropdown();
    });
}

function fillDeafaultSearchDropdown() {
    var options_text = "";
    var filter_ddl = "";
    $.each(advanceSearchData, function (key, val) {
        if (key == 0)
            options_text += '<option value="select">Select</option>';
        options_text += '<option value="' + val.item + '" data-options=\'' + JSON.stringify(val.values).replace(/'/g, "\\'") + '\'>' + val.text + '</option>';
    });
    if (options_text != "") {
        var filter_ddl_count = $('#popupAdvSearch select[id^=ddlGroupSelect]').length;
        filter_ddl = '<fieldset data-role="controlgroup" data-type="vertical"><div id="dvSearchFieldsRow' + (filter_ddl_count + 1) + '" width="100%" style="position:relative"><div class="ui-block-a" style="width: 160px" ><fieldset data-role="controlgroup" style="padding:10px"><select id="ddlGroupSelect' + (filter_ddl_count + 1) + '" name="ddlGroupSelect' + (filter_ddl_count + 1) + '" data-theme="b" style="width:80px" data-mini="true" onchange="loadSearchDropdownValues(this);">' + options_text + '</select></fieldset></div></div></fieldset>';
        $('#dvSearchFields').append(filter_ddl);
        $('#popupAdvSearch').popup('open');
        $('#popupAdvSearch div[id=dvSearchFieldsRow' + (filter_ddl_count + 1) + '] div[class=ui-block-a ]').find('fieldset').controlgroup().trigger('create');
    }
}
function loadSearchDropdownValues(ctrl) {
    var load_text = "";
    var filtered_val = "";
    var options = $(ctrl).find(':selected').data('options');
    var ctrl_suffix = ctrl.id.substring(ctrl.id.indexOf('ddlGroupSelect') + 14);
    $.each(options, function (key1, val1) {
        if (key1 == 0)
            load_text += '<option value="select">Select</option>';
        load_text += "<option value='" + val1.value + "'>" + val1.name + "</option>";
    });
    if (load_text != "") {
        if ($('#ddlGroupItems' + ctrl_suffix).length == 0) {
            filtered_val = '<div class="ui-block-b" style="width: 250px" class="ui-bar"><fieldset data-role="controlgroup" style="padding:10px"><select data-theme="b" name="ddlGroupItems' + ctrl_suffix + '" id="ddlGroupItems' + ctrl_suffix + '" onchange="loadDropdownValueDetails(\'dvSearchFieldsRow' + ctrl_suffix + '\',this);" data-mini="true" style="width:100%" >' + load_text + '</select></fieldset></div>';
            $('#dvSearchFieldsRow' + ctrl_suffix).append(filtered_val);
            $('#popupAdvSearch div[id=dvSearchFieldsRow' + (ctrl_suffix) + '] div[class=ui-block-b]').find('fieldset').controlgroup().trigger('create');
        }
        else {

            $("#ddlGroupItems" + ctrl_suffix + " option").remove();
            $("#ddlGroupItems" + ctrl_suffix).append($(load_text));
            $("#ddlGroupItems" + ctrl_suffix).val('select').selectmenu('refresh');
        }
    }

    //$('#dvLoadSearchFieldValues').css('display', 'block');
    //    $.each(advanceSearchData, function (key, val) {
    //        if ($(ctrl).val() == val.item) {
    //           var selected_element = val.values;
    //           $.each(selected_element, function (key1, val1) {
    //               load_text += "<option value='" + val1.value + "'>" + val1.name + "</option>";
    //           });
    //       }
    //   });
    //   if (load_text != "") {
    //       filtered_val = "<select  data-theme='b' >" + load_text + "</select>";
    //       $('#dvLoadSearchFieldValues').append(filtered_val);

    //   }
}

function loadDropdownValueDetails(ctrl_to_append, ctrl) {
    var options = '';
    var txtData = '';
    var ctrl_suffix = ctrl.id.substring(ctrl.id.indexOf('ddlGroupItems') + 13);
    if ($('#ddlMethod' + ctrl_suffix).length == 0) {
        options += '<div class="ui-block-c" style="width: 150px" ><fieldset data-role="controlgroup" style="padding:10px"><select data-theme="b" name="ddlMethod' + ctrl_suffix + '" id="ddlMethod' + ctrl_suffix + '" data-mini="true" data-mini="true" style="width:100%">';
        options += '<option value="equal">Equal</option>';
        options += '<option value="notequal">Not Equal</option></select></fieldset></div>';
        $("#" + ctrl_to_append).append(options);
        txtData += '<div class="ui-block-d" style="width: 150px" ><fieldset data-role="controlgroup" style="padding:10px"><input type="text" name="txtData' + ctrl_suffix + '" id="txtData' + ctrl_suffix + '" value="" maxlength="10" data-mini="true" data-mini="true"/></fieldset></div><div class="ui-block-e"><fieldset data-role="controlgroup" style="padding:10px"><a href="#" onclick="fillDeafaultSearchDropdown();" data-role="button" data-theme="a" data-inline="true" data-mini="true" id="" data-mini="true">Add</a></fieldset></div>';
        $("#" + ctrl_to_append).append(txtData);
        $('#' + ctrl_to_append).css('display', 'block');
        $('#popupAdvSearch div[id=' + ctrl_to_append + '] div[class=ui-block-c]').find('fieldset').controlgroup().trigger('create');
        $('#popupAdvSearch div[id=' + ctrl_to_append + '] div[class=ui-block-d]').find('fieldset').controlgroup().trigger('create');
        $('#popupAdvSearch div[id=' + ctrl_to_append + '] div[class=ui-block-e]').find('fieldset').controlgroup().trigger('create');
    }
    else {
        $("#ddlMethod" + ctrl_suffix).val('equal').selectmenu('refresh');
        $("#txtData" + ctrl_suffix).val('');

    }
}

function saveAdvanceSearchData() {
    if (!validateAdavanceSearchFields())
        return false;
    var selected_rows = $('div[id^=dvSearchFieldsRow]');
    var selected_search_info = {};
    var row_data = {};
    $.each(selected_rows, function (key, val) {
        var field1 = $(val).find('select[id^=ddlGroupSelect' + (key + 1) + ']').val();
        var field_data1 = $(val).find('select[id^=ddlGroupItems' + (key + 1) + ']').val();
        var field_data2 = $(val).find('select[id^=ddlMethod' + (key + 1) + ']').val();
        var field_data3 = $(val).find('input[id^=txtData' + (key + 1) + ']').val();

        row_data[field1] = {
            "field1": field_data1,
            "field2": field_data2,
            "field3": field_data3
        };
        selected_search_info['row' + (key + 1)] = row_data;
        row_data = {};
    });
}

function validateAdavanceSearchFields() {
    var ddl_not_selected = $('#popupAdvSearch select option[value=select]:selected');
    var text_field_not_selected = $('#popupAdvSearch input[type=text][value=""]');
    var msg = "";
    if (ddl_not_selected.length > 0 || text_field_not_selected.length > 0)
        msg = 'Please select required values and click submit';
    if (msg == "")
        return true;
    else {
        $('#alertmsg').html(msg);
        $('#popupAdvSearch').popup('close');
        $('#popupDialog a[id=okBut]').attr('onclick', '$("#popupDialog").popup("close");$("#popupAdvSearch").popup("open");');
        $('#popupDialog').popup('open');
        return false;
    }
}

function loadUsersToJson() {
    $.getJSON(gUserServiceUrl, function () {
        usersJson = dataUsers;
        //fillUsers('ulUserName', 'usersTemplate', usersJson, true);
    });
}

//function loadPersonalGroups(facility_id) {
//    var personal_groups_url = serviceURLDomain + "api/Tagging_personnel/" + facility_id;
//    usersJson = [];
//    departmentsJson = [];
//    var temp_users = [];
//    getCORS(personal_groups_url, null, function (response_data) {
//        $.each(response_data.groups, function (key, val) {

//            departmentsJson.push(key);
//            temp_users = [];
//            $.each(val.people, function (people_key, people_val) {
//                temp_users.push(people_val);
//            });
//            usersJson.push(
//            {
//                "department": key,
//                "users": temp_users
//            }
//            );

//        });
//    }, function (error_response) {
//        showErrorResponseText(error_response, true);
//    });
//}

function loadRegionsDDL() {
    $('#dvRegionsDDL').empty();
    var regions = '<select id="ddlRegions" data-theme="e" data-mini="true" data-iconpos="left" onchange = "ko.contextFor(this).$root.getSelectedFacilityJobs();">';
    regions += '<option value="1">Chicago</option>';
    regions += '<option value="2">Los Angeles</option>';
    //    regions += '<option value="3">Ft. Lauderdale</option>';
    //    regions += '<option value="4">Hartford</option>';
    //    regions += '<option value="5">Orlando</option>';
    //    regions += '<option value="6">Baltimore</option>';
    //    regions += '<option value="7">Allentown</option>';
    regions += '</select>';
    $('#dvRegionsDDL').append(regions).trigger('create');
}

//function loadTagRegionsDDL(facilityJSON, type) {
//    var regions = '';
//    regions += '<option value="-1">-- Select A Region --</option>';
//    $.each(facilityJSON, function (key, val) {
//        var selected = '';
//        if (val.facilityValue == sessionStorage.facilityId) //{
//            selected = 'selected';
//        if (type == "tag") {
//            if (Object.keys(tagJobInfo).length > 0 && tagJobInfo.regions != undefined && tagJobInfo.regions != null && tagJobInfo.regions.length > 0)
//                if (JSON.stringify(tagJobInfo.regions).indexOf(val.facilityValue) == -1 && JSON.stringify(tagJobInfo.regions).indexOf(val.facilityName) == -1)
//                    regions += '<option value="' + val.facilityValue + '" ' + selected + ' >' + val.facilityName + '</option>';
//        }
//        else {
//            regions += '<option value="' + val.facilityValue + '" ' + selected + ' >' + val.facilityName + '</option>';
//        }
//        //}
//    });
//    if (type == 'tag' || type == 'tagusers') {
//        if ($('#ddlTagRegions').length > 0) {
//            $('#ddlTagRegions').empty().append(regions);
//            $('#ddlTagRegions').selectmenu('refresh');
//        }
//        else {
//            regions = '<select id="ddlTagRegions" data-theme="f" data-mini="true" onchange="tagRegionsOrUsersChange(this,\'regions\',\'tag\')" data-iconpos="left">' + regions + '</select>';
//            $('#dvOptions').append(regions);
//        }
//    }
//    else if (type == 'filter') {
//        if ($('#ddlFilterRegions').length > 0) {
//            $('#ddlFilterRegions').empty().append(regions);
//            $('#ddlFilterRegions').selectmenu('refresh');
//        }
//        else {
//            regions = '<select id="ddlFilterRegions" data-theme="e" data-mini="true" onchange="tagRegionsOrUsersChange(this,\'regions\',\'filter\')" data-iconpos="left">' + regions + '</select>';
//            $('#dvFilterOptions').append(regions);
//            $('#ddlFilterRegions').selectmenu();
//        }
//    }
//}

//function getPageDisplayName(name) {
//    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
//    var display_name = '';
//    $.each(new String(name), function (key, value) {
//        if (key > 0 && value.match(PATTERN)) {
//            display_name += ' ' + value;
//        }
//        else {
//            display_name += (key == 0) ? value.initCap() : value;
//        }
//    });
//    return display_name;
//}

//function loadDepartmentsDDL(departments, type) {
//    var depts = '';
//    depts += '<option value="-1">-- Select A Department --</option>';
//    $.each(departmentsJson, function (key, val) {
//        depts += '<option value="' + val + '">' + getPageDisplayName(val) + '</option>';
//    });
//    if (type == 'tag') {
//        if ($('#ddlTagDepartments').length > 0) {
//            $('#ddlTagDepartments').empty().append(depts);
//            $('#ddlTagDepartments').selectmenu('refresh');
//        }
//        else {
//            depts = '<select id="ddlTagDepartments" data-theme="f" data-mini="true" onchange="loadUsers(\'tag\')" data-iconpos="left">' + depts + '</select>';
//            $('#dvOptions').append(depts);
//        }
//    }
//    else if (type == 'filter') {
//        if ($('#ddlFilterDepartments').length > 0) {
//            $('#ddlFilterDepartments').empty().append(depts);
//            $('#ddlFilterDepartments').selectmenu('refresh');
//        }
//        else {
//            depts = '<select id="ddlFilterDepartments" data-theme="e" data-mini="true" onchange="loadUsers(\'filter\')" data-iconpos="left">' + depts + '</select>';
//            $('#dvFilterOptions').append(depts);
//            $('#ddlFilterDepartments').selectmenu();
//        }
//    }
//}

//function loadUsers(type) {
//    loadTagUsersDDL(usersJson, type);
//    if (type == "tag")
//        $('#fldsetOptions').controlgroup('refresh');
//    if (type == "filter")
//        $('#ulSelectedUsers').empty();
//    $('#fldsetFilterOptions').controlgroup('refresh');
//}

//function loadTagUsersDDL(users_json, type) {
//    var users = '';
//    users += '<option value="-1">-- Select A User -- </option>';
//    var temp_users = [];
//    if (($('#ddlTagDepartments').length > 0 && $('#ddlTagDepartments').val() != "-1") || ($('#ddlFilterDepartments').length > 0 && $('#ddlFilterDepartments').val() != "-1")) {
//        temp_users = $.grep(users_json, function (obj) {
//            return (obj.department === ((type === "tag") ? $('#ddlTagDepartments').val() : $('#ddlFilterDepartments').val()))
//        });
//    }
//    if (temp_users.length > 0) {
//        $.each(temp_users[0].users, function (key, val) {
//            if (type == "tag") {
//                if (Object.keys(tagJobInfo).length > 0 && tagJobInfo.users != undefined && tagJobInfo.users != null)// && tagJobInfo.users.length > 0
//                    if (JSON.stringify(tagJobInfo.users).indexOf(val.userId) == -1 && JSON.stringify(tagJobInfo.users).indexOf(val.name) == -1)
//                        users += '<option value="' + val.userId + '">' + val.name + '</option>';
//            }
//            else
//                users += '<option value="' + val.userId + '">' + val.name + '</option>';
//        });
//    }
//    if (type == "tag") {
//        if ($('#ddlTaggingType').val() == 4) {
//            if ($('#ddlTagUsers').length > 0) {
//                $('#ddlTagUsers')[0].disabled = false;
//                $('#ddlTagUsers').empty().append(users);
//                $('#ddlTagUsers').selectmenu('refresh');
//            }
//            else {
//                users = '<select id="ddlTagUsers" data-theme="f" data-mini="true" onchange="tagRegionsOrUsersChange(this,\'users\',\'' + type + '\')" data-iconpos="left">' + users + '</select>';
//                $('#dvOptions').append(users);
//            }
//        }
//    }
//    else if (type == 'filter') {
//        if ($('#ddlFilterUsers').length > 0) {
//            $('#ddlFilterUsers')[0].disabled = false;
//            $('#ddlFilterUsers').empty().append(users);
//            $('#ddlFilterUsers').selectmenu('refresh');
//        }
//        else {
//            users = '<select id="ddlFilterUsers" data-theme="e" data-mini="true" onchange="tagRegionsOrUsersChange(this,\'users\',\'' + type + '\')" data-iconpos="left">' + users + '</select>';
//            $('#dvFilterOptions').append(users);
//            $('#ddlFilterUsers').selectmenu();
//        }
//    }
//}

//function displayJobWhosBeenTagged() {
//    $('#ulWhomsBeenTagged').empty();
//    var tagged_info_list = '';
//    $.each(tagJobInfo, function (key, val) {
//        if (val.length > 0) {
//            var list_header_text = ((key == "regions") ? 'Region(s)' : 'User(s)');
//            tagged_info_list += '<li data-role="list-divider" style="font-size: 12px;"  data-theme="g">' + list_header_text + '</li>';
//            $.each(val, function (item_key, item_val) {
//                var can_be_removed = false;
//                can_be_removed = (key == "regions" && sessionStorage.facilityId == item_val.regionId) ? false : true;
//                tagged_info_list += '<li ' + ((can_be_removed) ? "data-icon='delete'" : "data-icon='false'") + '><a href="#" value="' + ((key == "regions") ? item_val.regionId : item_val.userId) + '">';
//                tagged_info_list += '<h3 style="font-size: 14px;">' + ((key == "regions") ? item_val.regionName : item_val.userName) + '</h3></a>';
//                tagged_info_list += (can_be_removed) ? '<a href="#" onclick="removeSelectedUserOrRegion(this,\'' + ((key == "regions") ? 'regions' : 'Users') + '\',\'' + ((key == "regions") ? '' : item_val.departmentId) + '\')">Remove</a>' : '';
//                tagged_info_list += '</li>';
//            });
//        }
//    });
//    if (tagged_info_list != '')
//        $('#ulWhomsBeenTagged').append(tagged_info_list).listview('refresh');
//}

//function tagRegionsOrUsersChange(ctrl, tag_type, type) {
//    $('#ulSelectedUsers').empty();
//    var selected_option = $(ctrl).find('option:selected');
//    if (type == "tag") {
//        if (($('#ddlTaggingType').val() == "2" && tag_type == "regions") || ($('#ddlTaggingType').val() == "4" && tag_type == "users")) {
//            var selected_items = "";
//            var tagging_type_text = "";
//            tagging_type_text = (tag_type === "regions") ? 'Regions' : 'Users';
//            var temp_regions_users = [];
//            $.each(tagJobInfo, function (key, val) {
//                if (key == ((tag_type == "regions") ? "regions" : "users")) {
//                    temp_regions_users = val;
//                    return false;
//                }
//            });
//            if (tag_type === "regions") {
//                temp_regions_users.push({
//                    "regionId": $(selected_option).val(),
//                    "regionName": $(selected_option).text()
//                });
//            }
//            else {
//                temp_regions_users.push({
//                    "userId": $(selected_option).val(),
//                    "userName": $(selected_option).text(),
//                    "departmentId": $('#ddlTagDepartments').val()
//                });
//            }

//            if (tag_type == "regions") {
//                if (Object.keys(tagJobInfo).length > 0) {
//                    var temp = tagJobInfo;
//                    tagJobInfo = {};
//                    tagJobInfo["regions"] = temp_regions_users;
//                    $.each(temp, function (key, val) {
//                        if (key != tag_type)
//                            tagJobInfo[key] = val;
//                    });

//                }
//            }
//            if (tag_type == "users")
//                tagJobInfo["users"] = temp_regions_users;

//            displayJobWhosBeenTagged();
//            //            if ($('#ulSelectedRegionsOrUsers li').length == 0)
//            //                selected_items = '<li data-role="list-divider" style="font-size: 12px;">Tagged ' + tagging_type_text + '</li>';
//            //            selected_items += '<li data-icon="delete"><a href="#" data-rel="popup" value="' + $(selected_option).val() + '">';
//            //            selected_items += '<h3 style="font-size: 14px;">' + $(selected_option).text() + '</h3></a>';
//            //            selected_items += '<a href="#" onclick="removeSelectedUserOrRegion(this,\'' + tagging_type_text + '\')">Remove</a></li>';
//            //            $('#ulSelectedRegionsOrUsers').append(selected_items).listview('refresh');
//            //           $('#dvSelectedRegionsOrUsers').css('display', 'block');
//            $(ctrl).find('option:selected').remove();
//        }
//        else {
//            if ($('#ddlTagRegions').val() != "-1") {
//                loadPersonalGroups($('#ddlTagRegions').val());
//                window.setTimeout(function loadDepts() {
//                    loadDepartmentsDDL(departmentsJson, 'tag');
//                    loadTagUsersDDL(usersJson, 'tag');
//                }, 1000);
//            }
//        }
//    }
//    else {
//        if (tag_type == "regions") {
//            if ($('#ddlFilterRegions').val() != "-1") {
//                loadPersonalGroups($('#ddlFilterRegions').val());
//                window.setTimeout(function loadDepts() {
//                    loadDepartmentsDDL(departmentsJson, 'filter');
//                    loadTagUsersDDL(usersJson, 'filter');
//                }, 1000);
//            }
//        }
//        else {
//            var selected_items = "";
//            var tagging_type_text = "Filter";
//            if ($('#ulSelectedUsers li').length == 0)
//                selected_items = '<li data-role="list-divider" style="font-size: 12px;" data-theme="g">Filter Users</li>';
//            selected_items += '<li data-icon="delete"><a href="#" data-rel="popup" value="' + $(selected_option).val() + '">';
//            selected_items += '<h3 style="font-size: 14px;">' + $(selected_option).text() + '</h3></a>';
//            selected_items += '<a href="#" onclick="removeSelectedUserOrRegion(this,\'' + tagging_type_text + '\',\'' + $('#ddlFilterDepartments').val() + '\')">Remove</a></li>';
//            $('#ulSelectedUsers').append(selected_items).listview('refresh');
//            $('#dvSelectedFilterUsers').css('display', 'block');
//            $(ctrl).find('option:selected').remove();
//            $(ctrl)[0].disabled = true;
//        }
//    }
//}

//function removeSelectedUserOrRegion(ctrl, type, department_id) {
//    var source = (type == "Users" || type == "Filter") ? $.extend(true, [], usersJson) : $.extend(true, [], facilityJSON);
//    var selected_items = (type == "Filter") ? $('#ulSelectedUsers li').not('a[value=""][title=Remove]') : $('#ulWhomsBeenTagged li').not('a[value=""][title="Remove"]');
//    var temp_items = [];
//    var temp_regions_users = [];
//    $.each(tagJobInfo, function (key, val) {
//        if (key == ((department_id == "") ? "regions" : "users")) {
//            temp_regions_users = val;
//            return false;
//        }
//    });

//    $.each(selected_items, function (key, val) {
//        if ($(ctrl.previousSibling).attr('value') != $(val).find('a').attr('value')) {
//            if (department_id == "") {
//                temp_items.push({
//                    'facilityName': $(val).find('a').text(),
//                    'facilityValue': $(val).find('a').attr('value')
//                });
//            }
//            else {
//                temp_items.push({
//                    'name': $(val).find('a').text(),
//                    'userId': $(val).find('a').attr('value')
//                });
//            }
//        }
//        else {
//            var key_index;
//            var is_found = false;
//            var selected_item = $(val).find('a').attr('value');
//            $.each(temp_regions_users, function (key, val) {
//                if (is_found) {
//                    return false;
//                }
//                key_index = key;

//                if (((department_id == "") ? val.regionId : val.userId) == selected_item) {
//                    temp_regions_users.splice(key, 1);
//                    is_found = true;
//                    return false;
//                }

//            });
//            //if (type != "Filter")
//            //    if (temp_regions_users.length == 0)
//            //        delete tagJobInfo[((type == "Users") ? 'users' : 'regions')];
//        }
//    });

//    var options = $.grep(source, function (obj) {
//        if (type == "Users" || type == "Filter") {
//            var temp_users = [];
//            $.each(obj.users, function (key_user, val_user) {
//                if (JSON.stringify(temp_items).indexOf(val_user.name) == -1) {
//                    temp_users.push(obj.users[key_user]);
//                }
//            });
//            obj.users = temp_users;
//            return obj;
//        }
//        else
//            return JSON.stringify(temp_items).indexOf(obj.facilityValue) == -1;
//    });

//    if (type == "Users")
//        loadTagUsersDDL(options, 'tag')
//    else if (type == "Filter")
//        loadTagUsersDDL(options, 'filter')
//    else if ($('#ddlTaggingType').val() != "-1" && $('#ddlTaggingType').val() != "1" && $('#ddlTaggingType').val() != "4" && type == "regions")
//        loadTagRegionsDDL(options, 'tag');
//    $(ctrl).parent().remove();
//    if (type == "Filter") {
//        if ($('#ulSelectedUsers li[data-role!="list-divider"]').length == 0) {
//            $('#ulSelectedUsers').empty();
//            $('#dvSelectedFilterUsers').css('display', 'none');
//        }
//    } else {
//        displayJobWhosBeenTagged();

//        //        if ($('#ulWhomsBeenTagged li[data-role!="list-divider"]').length == 0) {
//        //            $('#ulWhomsBeenTagged').empty();
//        //            $('#ulWhomsBeenTagged').css('display', 'none');
//        //        }
//    }
//}

//function fillUsers(ctrl, template_to_bind, json_to_bind, is_page_load) {
//    $('#' + ctrl).empty();
//    if (ctrl.toLowerCase().indexOf('selected') > -1)
//        $("#" + template_to_bind).tmpl(json_to_bind).attr('data-icon', 'delete').appendTo("#" + ctrl);
//    else {
//        $("#" + template_to_bind).tmpl(json_to_bind).appendTo("#" + ctrl);
//    }
//    if (selectedUsers.length > 0) {
//        $.each(selectedUsers, function (a, b) {
//            if (ctrl.toLowerCase().indexOf('selected') == -1)
//                $('#' + ctrl).find('li[id=li' + b.userName + ']').remove();
//        });
//        if (ctrl.toLowerCase().indexOf('selected') > -1)
//            $('#' + ctrl).prepend('<li data-role="list-divider" >Selected Users</li>');
//    }
//    window.setTimeout(function getDealy() { $('#' + ctrl).listview('refresh'); }, 100);
//}

//function addUsers(user_value, user_name, ctrl) {
//    $('#li' + user_value).find('a').removeAttr('onclick');
//    window.setTimeout(function getDelay() { addSelectedUsers(user_value, user_name, ctrl); }, 100);
//}

//function addSelectedUsers(user_value, user_name, ctrl) {
//    selectedUsers.push({ "fullName": user_name, "userName": user_value });
//    selectedUsers = selectedUsers.sort(function (a, b) {
//        return a.fullName > b.fullName ? 1 : -1;
//    });
//    $('#' + ctrl).find('li[id=li' + user_value + ']').remove();
//    fillUsers('ulSelectedUsers', 'selectedUsersTemplate', selectedUsers, false);
//    if ($('#ulSelectedUsers li:not([id^=li])').length == 0)
//        $('#ulSelectedUsers').prepend('<li data-role="list-divider" >Selected Users</li>');
//    $('#ulSelectedUsers').listview('refresh');
//    if (selectedUsers.length > 0)
//        $('#ulSelectedUsers').show();

//}

//function removeUsers(user_value) {
//    window.setTimeout(function getDelay() { removeSelectedUsers(user_value); }, 100);
//}

//function removeSelectedUsers(user_value) {
//    var count = 0;
//    $.each(selectedUsers, function (a, b) {
//        if (b.userName.toLowerCase() == user_value.toLowerCase())
//            return false;
//        else
//            count++
//    });
//    selectedUsers.splice(count, 1);
//    if (selectedUsers.length == 0)
//        $('#ulSelectedUsers').hide();
//    fillUsers('ulSelectedUsers', 'selectedUsersTemplate', selectedUsers, false);
//    if ($('#ulSelectedUsers li:not([id^=li])').length == 0)
//        $('#ulSelectedUsers').prepend('<li data-role="list-divider" >Selected Users</li>');
//    $('#ulSelectedUsers').listview('refresh');
//    fillUsers('ulUserName', 'usersTemplate', usersJson, false);
//}

//function manageTaggingOptions(ctrl_id) {
//    var selected_type = $(ctrl_id).val();
//    if (selected_type != "") {
//        switch (selected_type) {
//            case "1":
//                $('#dvOptions').empty();
//                $('#ddlTagRegions').css('display', 'none');
//                $('#ddlTagDepartments').css('display', 'none');
//                $('#ddlTagUsers').css('display', 'none');
//                $('#dvOptions').css('display', 'block');
//                var facility_name = getTribuneFacilityName(sessionStorage.facilityId);
//                if (Object.keys(tagJobInfo).length > 0 && tagJobInfo.regions != undefined && tagJobInfo.regions != null && tagJobInfo.regions.length > 0) {
//                    if (JSON.stringify(tagJobInfo.regions).indexOf(sessionStorage.facilityId) == -1 && JSON.stringify(tagJobInfo.regions).indexOf(facility_name) == -1)
//                        tagJobInfo.regions.push({ "regionId": sessionStorage.facilityId, "regionName": facility_name });
//                }
//                else
//                    tagJobInfo["regions"] = [{ "regionId": sessionStorage.facilityId, "regionName": facility_name }];

//                if (Object.keys(tagJobInfo).length > 0 && tagJobInfo.users != undefined && tagJobInfo.users != null && tagJobInfo.users.length > 0) {
//                    if (JSON.stringify(tagJobInfo.users).indexOf(sessionStorage.username) == -1 && JSON.stringify(tagJobInfo.users).indexOf(sessionStorage.username) == -1)
//                        tagJobInfo.users.push({ "userId": sessionStorage.username, "userName": sessionStorage.username });
//                }
//                else
//                    tagJobInfo["users"] = [{ "userId": sessionStorage.username, "userName": sessionStorage.username }];
//                displayJobWhosBeenTagged();
//                break;
//            case "2":
//                $('#dvOptions').empty();
//                $('#ulSelectedRegionsOrUsers').empty().listview('refresh');
//                loadTagRegionsDDL(facilityJSON, 'tag');
//                $('#ddlTagRegions').css('display', 'block');
//                $('#ddlTagDepartments').css('display', 'none');
//                $('#ddlTagUsers').css('display', 'none');
//                $('#dvOptions').trigger('create');
//                break;
//            case "3":
//                break;
//            case "4":
//                $('#dvOptions').empty();
//                $('#ulSelectedRegionsOrUsers').empty().listview('refresh');
//                loadTagRegionsDDL(facilityJSON, 'tagusers');
//                loadDepartmentsDDL(departmentsJson, 'tag');
//                loadTagUsersDDL(usersJson, 'tag');
//                $('#ddlTagRegions').css('display', 'block');
//                $('#ddlTagDepartments').css('display', 'block');
//                $('#ddlTagUsers').css('display', 'block');
//                $('#dvOptions').trigger('create');
//                break;
//        }
//        $('#fldsetOptions').controlgroup('refresh');
//    }
//}

function displayReportsList() {
    if (appPrivileges.customerNumber != KUBOTA_CUSTOMER_NUMBER && appPrivileges.customerNumber != CASEYS_CUSTOMER_NUMBER) {
    var accounting_reports = "";
    //if (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER) {
    accounting_reports = '<div id="dvReports" data-role="collapsible"  data-theme="f" data-content-theme="c"  data-collapsed-icon="carat-r" data-expanded-icon="carat-d"><h3>Reports</h3><ul id="ulAccountingReports" data-role="listview">';
    //}
    $.getJSON(reportsServiceUrl, function (data) {
        reportsServiceData = data;
        $.each(reportsServiceData, function (key, val) {
            var show_report = true;
            $.each(val, function (key1, val1) {
                if (val1.userRole.toLowerCase().indexOf(appPrivileges.roleName.toLowerCase()) > -1) {
                    show_report = true;
                    if ((val1.value == "Production_Release_Report" || val1.value == "Lettershop_Production_Report") && (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM_CUSTOMER_NUMBER))
                        show_report = true;
                    else if (((val1.value == "Production_Release_Report" || val1.value == "Lettershop_Production_Report") && appPrivileges.customerNumber != "1" && appPrivileges.customerNumber != REDPLUM_CUSTOMER_NUMBER) ||
                             ((val1.value == "Open_Accounts_Receivable" || val1.value == "Job_Invoice_Summary" || val1.value == "Daily_Milestones") && (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER || appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER)) ||
                             ((val1.value == "Open_Jobs" || val1.value == "Geolocation_Summary") && (appPrivileges.customerNumber == CW_CUSTOMER_NUMBER)) ||
                             (val1.value == "Postage_Summary" && appPrivileges.customerNumber == CASEYS_CUSTOMER_NUMBER))
                        show_report = false;

                    if (show_report)
                        accounting_reports += '<li data-theme="c"><a onclick="populateAccountingReportsOutputForm(\'' + val1.value + '\',\'\')" href="#popupReportsParam" data-rel="popup" data-position-to="window" data-inline="true">' + val1.name + '</a></li>';
                }
            });
        });
        //if (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER) {
        accounting_reports += '</ul></div>';
        $("#colList1").find('#dvReports').remove();
        $("#colList1").append(accounting_reports);
        //    if ($('#ulAccountingReports').length == 0) {
        //       
        //        $("#List").trigger('create');
        $.each($('#ulAccountingReports'), function () {
            $(this).listview();
        });
        $('#colList1').collapsibleset('refresh');
        //    }
        // } else {
        //$('#ulPickDemo').empty();
        // $('#ulPickDemo').html(accounting_reports).listview('refresh');
        //}
    });
  }
}

function getDailyTaskData() {
    $.getJSON(dailyTaskServiceUrl, function (data) {
        dailyTaskUrlData = data;
    });
}

function fnChangePassword() {
    var error_msg = "";
    var is_validate = true;
    if ($("#cnfpwd").val() != $('#newpwd').val()) {
        error_msg = 'Password and Confirm password should be same.';
        is_validate = validateChangePassword(error_msg);
        return is_validate;
    }
    if ($("#cnfpwd").val() == '' || $('#newpwd').val() == '') {
        error_msg = 'Please enter password and confirm password.';
        is_validate = validateChangePassword(error_msg);
        return is_validate;
    }
    var change_pass_url = changePasswordUrl + appPrivileges.facility_id + '/' + appPrivileges.customerNumber + '/' + sessionStorage.username + '/' + jQuery.trim($("#cnfpwd").val());
    getCORS(change_pass_url, null, function () {
        $('#popupChangePassword').popup('close');
        $('#alertmsg').text('Password has been change successfully.');
        $('#popupDialog').popup('open');
        $('#popupChangePassword').popup('close');
    }, function (error_response) {
        if (error_response.responseText.toLowerCase() == "error: profile. forgot my password..") {
            $('#alertmsg').text('We are unable to change the password for this account. Please contact a system administrator.');
            $('#popupChangePassword').popup('close');
            $('#popupDialog').popup('open');
        }
        else {
            showErrorResponseText(error_response, true);
        }
    });

}
function validateChangePassword(error_msg) {
    if (error_msg != '') {
        $('#popupChangePassword').popup().popup('close');
        $('#alertmsg').html(error_msg);
        $("#popupDialog").popup().popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupChangePassword').popup().popup('open');");
        return false;
    }
}

function fnCancel() {
    $('#divError').css('display', 'none');
    $('#popupChangePassword').popup('close');
}

function deleteChkElement() {
    $.each(dailyTaskUrlData, function (key, val) {
        $.each(val.values, function (key1, val1) {
            //$('#chk' + val1.jobNumber), this).remove();
            if ($('#chk' + val1.jobNumber).is(':checked')) {
                //$('#chk' + val1.jobNumber).find('option:selected').remove();
                $('#chk' + val1.jobNumber).removeAttr('checked').checkboxradio('refresh');
            }
        });

    });
}

function showFilters() {
    var filter_show = '<div id="btnFilter" data-role="collapsible" data-theme="e" data-content-theme="c" data-collapsed="true" data-collapsed-icon="myapp-filter" data-expanded-icon="carat-d">';
    filter_show += '<h3>Filter Jobs</h3><div data-role="controlgroup" data-mini="true" align="left">';
    filter_show += '<label for="ddlFilterMilestones">Date Type:</label><select name="ddlFilterMilestones" id="ddlFilterMilestones" data-mini="true" data-theme="e" onchange="enableStartEndDates();">';
    filter_show += '<option value="-1">Select</option><option value="is">Data Processing/IS</option><option value="postage">Postage</option><option value="mailDrop">Mail Drop</option>';
    filter_show += '<option value="verified">Ship Verification</option><option value="inhome">In-Home</option><option value="invoice">Invoice</option></select>';
    filter_show += '</div><div data-mini="true" align="left"><label for="dtpStart">From:</label><input type="text" onfocus="showDatePicker();" placeholder="mm/dd/yyyy" data-clear-btn="false" name="dtpStart" id="dtpStart" data-mini="true">';
    filter_show += '<label for="dtpEnd">To:</label><input type="text" onfocus="showDatePicker();" placeholder="mm/dd/yyyy" data-clear-btn="false" name="dtpEnd" id="dtpEnd" data-mini="true"></div>';
    filter_show += '<div data-role="collapsible-set" id="dvDropAllUsers"><div data-role="collapsible" data-collapsed="false" data-content-theme="c" data-theme="e" data-mini="true" data-collapsed-icon="carat-r" data-expanded-icon="carat-d">';
    filter_show += '<h3>Personnel</h3><div id="dvSelectedFilterUsers" style="display:none"><ul data-role="listview" id="ulSelectedUsers" data-inset="true" data-divider-theme="e" data-theme="c" data-mini="true"></ul></div>';
    filter_show += '<fieldset data-role="controlgroup" data-mini="true" id="fldSetFilterOptions"><div id="dvFilterOptions"></div></fieldset></div>';
    filter_show += '<div align="center"><a href="#" data-role="button" data-theme="f" data-inline="true" data-mini="true" name="btnFiltersCancel" id="btnFiltersCancel" onclick="clearFilter(true);">Clear</a>';
    filter_show += '<a href="#" data-role="button" data-theme="f" data-inline="true" data-mini="true" name="btnFiltersSubmit" id="btnFiltersSubmit" onclick="ko.contextFor(this).$root.filterJobs(ko.contextFor(this).$data,ko.contextFor(this).$parent,event);">Filter</a></div></div></div>';
    $('#dvPreferences').append(filter_show);
    $('#btnFilter').trigger('create');
}

function showDemos() {
    $('#btnDemo').remove();
    var demo_show = '<div id="btnDemo" data-role="collapsible" data-theme="e" data-content-theme="c" data-collapsed="true" data-collapsed-icon="myapp-demo" data-expanded-icon="carat-d">';
    demo_show += ' <h3>Gizmo Demos</h3><ul data-role="listview" >';
    demo_show += ' <li id="liCaseys"><a href="#" onclick="loadDemoCustData(this);" data-custno="99998" data-program="oip" style="font-size:14px;">Opt-In Program</a></li>';
    demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno="99996" data-program="smp" style="font-size:14px;">Shared Mail Program</a></li>';
    //demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno=BRIGHTHOUSE_CUSTOMER_NUMBER data-program="up" style="font-size:14px;">Upload Program</a></li>';
    demo_show += '<li><a href="#" data-program="up" style="font-size:14px;">Upload Program</a></li>';
    demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno="99997" data-program="lmm" style="font-size:14px;">Location Mgmt Module</a></li>';
    demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno="99997" data-program="lsm" style="font-size:14px;">List Selection Module</a></li>';
    demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno="99997" data-program="pam" style="font-size:14px;">Proofing & Approval Module</a></li>';
    demo_show += '</ul></div>';
    $('#dvPreferences').append(demo_show);
    $('#btnDemo').trigger('create');
}

function showFilterTasks() {
    var filter_tasks = '';
    filter_tasks += '<div id="dvFilterTasks" data-role="collapsible" data-theme="e" data-content-theme="c" data-collapsed="true" data-collapsed-icon="myapp-filter" data-expanded-icon="carat-d">';
    filter_tasks += '<h3>Filter Tasks</h3>';
    filter_tasks += '<fieldset data-role="controlgroup" class="ui-controlgroup">';
    filter_tasks += '<input type="radio" name="rdoFilterTasks" id="rdoAllTasks" data-mini="true" class="custom" data-icon="check" title="All Tasks" value="allTasks" checked style="left:0.2em;top:40%" />';
    filter_tasks += '<label for="rdoAllTasks" data-mini="true" title="All Tasks">All Tasks</label>';
    filter_tasks += '<input type="radio" name="rdoFilterTasks" id="rdoMyTasks" data-mini="true" class="custom" data-icon="check" title="My Tasks" value="myTasks" style="left:0.2em;top:40%" />';
    filter_tasks += '<label for="rdoMyTasks" data-mini="true" title="My Tasks">My Tasks</label>';
    filter_tasks += '</fieldset>';
    filter_tasks += '<div align="right"><a data-role="button" data-theme="f" data-inline="true" data-mini="true" name="btnFilterTasks" id="btnFilterTasks">Filter Tasks</a></div></div>';
    $('#dvPreferences').append(filter_tasks);
    $('#dvFilterTasks').trigger('create');
}

function loadDemoCustData(ctrl) {
    var cust_id = $(ctrl).attr('data-custno');
    var demo_program = $(ctrl).attr('data-program');
    if (cust_id != undefined && cust_id != null && cust_id != "") {
        sessionStorage.isDemoSelected = 1;
        if (sessionStorage.demoProgram != demo_program)
            sessionStorage.isDemoInsDisplayed = false;
        sessionStorage.demoProgram = demo_program;
        if (cust_id == "99998" && (appPrivileges.roleName == "power" || appPrivileges.roleName == "user"))
            $('#lnkNewJob').css('display', 'none');
        else
            $('#lnkNewJob').css('display', 'block');
        demoNavlinks(cust_id);
        setLogo(cust_id);
        $('#dvRegionsDDL').css('display', 'none');
        $('#dvPreferences').find('#btnFilter').css('display', 'none');
        $("#dvDailyTaskListData").css("display", "none");
        $('#dvReports').addClass('ui-corner-all');
    }
}

function validateFilters() {
    var msg = '';
    if ($('#ddlFilterMilestones').val() == -1 && $('#dvDropAllUsers li').length == 0) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select at least one option from  Milestones or Personnel(Region,Department and User)";
        else
            msg = "Please select at least one option from  Milestones or Personnel(Region,Department and User)";
    }
    if ($('#ddlFilterMilestones').val() != -1 && $('#dtpStart').val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select From Date";
        else
            msg = "Please select From Date";

    }
    if ($('#ddlFilterMilestones').val() != -1 && $('#dtpEnd').val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select To Date";
        else
            msg = "Please select To Date";
    }
    if (msg != "") {
        $('#alertmsg').html(msg);
        $('#alertmsg').css('text-align', 'left');
        $("#popupDialog").popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupComments').popup('open');");
        return false;
    }
    return true;
}

function clearFilter(flag) {
    $('#ddlFilterMilestones').val('-1').selectmenu('refresh');
    $('#ddlFilterRegions').val('-1').selectmenu('refresh');
    $('#ddlFilterDepartments').val('-1').selectmenu('refresh');
    $('#ddlFilterUsers').val('-1').selectmenu('refresh');
    $('#ulSelectedUsers').empty();
    $('#dtpStart').val('');
    $('#dtpEnd').val('');
    $('#ddlFilterRegions').val(sessionStorage.facilityId).selectmenu('refresh').trigger('change');
    if (($('#dtpStart') || $('#dtpEnd')).hasClass('ui-input-text')) {
        $('#dtpStart')[0].disabled = true;
        $('#dtpEnd')[0].disabled = true;
        $('#dtpStart').css('opacity', '0.7');
        $('#dtpEnd').css('opacity', '0.7');
    }
    $("input[id$='dtpStart']").datepicker("option", "maxDate", null);
    $("input[id$='dtpEnd']").datepicker("option", "minDate", null);
    if (flag)
        $('#btnFiltersSubmit').trigger('click');
}

function enableStartEndDates() {
    if ($('#ddlFilterMilestones').val() != "-1") {
        //$('#dtpStart').removeClass('ui-disabled');
        $('#dtpStart')[0].disabled = false;
        $('#dtpStart').css('opacity', '1');
        //$('#dtpEnd').removeClass('ui-disabled');
        $('#dtpEnd')[0].disabled = false;
        $('#dtpEnd').css('opacity', '1');
    }
    else if ($('#ddlFilterMilestones').val() == "-1") {
        $('#dtpStart').val('');
        $('#dtpEnd').val('');
        if (($('#dtpStart') || $('#dtpEnd')).hasClass('ui-input-text')) {
            $('#dtpStart')[0].disabled = true;
            $('#dtpEnd')[0].disabled = true;
            $('#dtpStart').css('opacity', '0.7');
            $('#dtpEnd').css('opacity', '0.7');
        }
    }
}

function loadDemoEmailsList() {
    getCORS(serviceURLDomain + "api/GizmoEmail_actions/" + facilityId + "/" + jobCustomerNumber, null, function (data) {
        demoEmailsList = data;
    }, function (response_error) {
        //$('#alertmsg').text(response_error);
        //$('#popupDialog').popup('open');
        showErrorResponseText(response_error, false);
    });
}

function fnSendOptInEmail(type) {
    getCORS(optInUrl + jobCustomerNumber + '/' + facilityId + '/408313/' + type, null, function (response) {
        //if (response.indexOf("success") != "-1") {
        $('#alertmsg').html(response);
        $('#popupDialog').popup('open');
        //}
    }, function (response_error) {
        //$('#alertmsg').text(response_error);
        //$('#popupDialog').popup('open');
        showErrorResponseText(response_error, false);
    });
}

function aboutDemoChange(selected_demo_item) {
    //var selected_demo_item = $('#ddlAboutDemo').val();
    if (selected_demo_item != "" && selected_demo_item > 0) {
        $('div[id^=demoText]').css('display', 'none');
        $('#demoText' + selected_demo_item).css('display', 'block');
        $('#popupAboutDemo').popup('open');
        $('#popupAboutDemo').popup('open');
    }
}

function pickDemo(type) {
    switch (type) {
        case "1":
            $('#headerText').text('Automated Job Processing');
            $('#pText').css('display', 'none');
            $('#ulInnderText').css('display', 'none');
            break;
        case "2":
            $('#headerText').text('Small Job Processing');
            $('#pText').css('display', 'block');
            $('#ulInnderText').css('display', 'block');
            $('#pTextArea').css('display', 'none');
            $('#pTextArea1').css('display', 'none');
            break;
        case "3":
            $('#headerText').text('Franchise opt-in email');
            $('#pText').css('display', 'none');
            $('#ulInnderText').css('display', 'block');
            $('#pTextArea').css('display', 'block');
            $('#pTextArea1').css('display', 'none');
            break;
        case "4":
            $('#headerText').text('Mail Tracker');
            $('#pText').css('display', 'none');
            $('#ulInnderText').css('display', 'none');
            $('#pTextArea').css('display', 'block');
            $('#pTextArea1').css('display', 'none');
            break;
        case "5":
            $('#headerText').text('List Selection');
            $('#pText').css('display', 'none');
            $('#ulInnderText').css('display', 'block');
            $('#pTextArea').css('display', 'block');
            $('#pTextArea1').css('display', 'none');
            break;
        default:
            break;
    }
}

function showGizmoUsers() {
    window.location.href = "userProfile/adminUsers.html";
}
function deleteGizmoUsers() {
    window.location.href = "archiveDeleteJobs.html";
}

function loadDemoReportsJson() {
    var demo_reports_json = {};
    $.getJSON("../JSON/_demoReports.JSON", function (data) {
        $.each(data, function (key, val) {
            if (key.indexOf(sessionStorage.customerNumber) > -1) {
                $.each(val, function (key1, val1) {
                    demo_reports_json[key1] = val1;
                });
            }
        });
        if (Object.keys(demo_reports_json).length > 0)
            sessionStorage.demoReportsJson = JSON.stringify(demo_reports_json);
        createDemoReportsList();
    });
}

function loadAboutThisDemoFaq() {
    var demo_faq_json = {};
    $.getJSON("../JSON/_demoAboutThisDemoFaq.JSON", function (data) {
        $.each(data, function (key, val) {
            if (key.indexOf(sessionStorage.customerNumber) > -1) {
                $.each(val, function (key1, val1) {
                    demo_faq_json[key1] = val1;
                });
            }
        });
        if (Object.keys(demo_faq_json).length > 0)
            sessionStorage.demoFaqJson = JSON.stringify(demo_faq_json);
        createDemoFaqList();
    });
}

function createDemoFaqList() {
    if (sessionStorage.demoFaqJson != undefined && sessionStorage.demoFaqJson != null && sessionStorage.demoFaqJson != "") {
        var demo_faqs = {};
        demo_faqs = $.parseJSON(sessionStorage.demoFaqJson);
        $('#dvFaqs').empty();
        var temp_list = "";
        $.each(demo_faqs, function (key, val) {
            temp_list += val;
        });
        if (temp_list != "") {
            //$('#dvFaqs').html(temp_list).trigger('create');
            $('#dvFaqs').html(temp_list).trigger('create').collapsibleset('refresh');
        }
    }

}

function createDemoReportsList() {
    if (sessionStorage.demoReportsJson != undefined && sessionStorage.demoReportsJson != null && sessionStorage.demoReportsJson != "") {
        var demo_reports = {};
        demo_reports = $.parseJSON(sessionStorage.demoReportsJson);
        $('#ulPickDemo').empty();
        var temp_list = "";
        $.each(demo_reports, function (key, val) {
            temp_list += val;
        });
        if (temp_list != "") {
            $('#ulPickDemo').html(temp_list).listview('refresh');
        }
    }

}

function loadStoreData() {
    window.location.href = "../loadStoreData/loadStoreData.html";
}

function loadLMASetup() {
    window.location.href = "../lmaSetup/lmaSetup.html";
}

function displayFacilityList() {
    var demo_faq_json = {};
    $('#dvFacilityNames').empty();
    $.getJSON("../JSON/_facilityList.JSON", function (data) {
        $.each(data, function (key, val) {
            $.each(val, function (key1, val1) {
                demo_faq_json[key1] = val1;
            });
        });
        var facilty_list = '<ul data-role="listview" id="ulFacilityList" data-inset="true">';
        $.each(demo_faq_json, function (key, val) {
            $.each(val, function (key1, val1) {
                if (key1 == "facilityName")
                    facilty_list += '<li class="ui-grid-d ui-responsive"><label class="ui-block-a" style="width:65%;valign:middle;">' + val1 + '</label>';// <label data-bind="attr:{'for':'chk'+ $data.userName.replace(' ','')}, text:$data.computedUserNameWithCount"></label>
                if (key1 == "facilityValue") {
                    facilty_list += '<select class="ui-block-b" data-theme="e" style="width:30%;valign:middle;" data-role="flipswitch" data-mini="true">' + (val1 == sessionStorage.facilityId ? '<option value="off">Off</option><option value="on" selected="">On</option>' : '<option value="off" selected="">Off</option><option value="on">On</option>') + '</select></li>';
                }
            });
        });
        facilty_list += '</ul>';
        $('#dvFacilityNames').append(facilty_list);
        $('#ulFacilityList').trigger('create');
        $('#dvFacilityNames').trigger('create');
        $("#popupFaciltyList").popup('open');
    });
}

function confirmFlushInMemoryDB() {
    //$('#okButConfirm').text('Delete Job');
    $('#confirmMsg').html("Do you really want to delete all of the in-memory job tickets? This will cause them to be recreated from scratch on the next customer request. In general, this will NOT cause data loss, but may take a few minutes to perform. Note that in-memory job tickets are flushed overnight automatically, so it's sometimes a good idea to wait.");
    $('#okButConfirm').text('Yes');
    $('#cancelButConfirm').text('No');
    $('#cancelButConfirm').bind('click', function () {
        $('#cancelButConfirm').unbind('click');
        $('#okButConfirm').unbind('click');
        $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'open\');$(\'#popupConfirmDialog\').popup(\'close\');');
        $('#popupConfirmDialog').popup('close');
    });
    $('#okButConfirm').bind('click', function () {
        $('#okButConfirm').unbind('click');
        var flush_in_mem_db_url = serviceURLDomain + "api/InMemory_flush";
        var post_json = {};
        post_json["userId"] = sessionStorage.username;
        $('#popupConfirmDialog').popup('close');
        postCORS(flush_in_mem_db_url, JSON.stringify(post_json), function (response_data) {
            if (response_data.toLowerCase() == "success") {
                $('#alertmsg').html('In-Memory DB flushed successfully.');
                window.setTimeout(function () {
                    $('#popupDialog').popup('open');
                }, 500);
            }
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    });
    $('#popupConfirmDialog').popup('open');
}

function manageFin() {
    window.location.href = "adminFinance/financeJobLookUp.html";
}

function navEmailLists() {
    window.location.href = "adminEmailLists/emailLists.html";
}
function navTemplateSetup() {
    window.location.href = "adminTemplateSetup/templateSetup.html";
}

function fnDisplayBbbReports() {
    var report_url = "http://s-reportserver/ReportServer/Pages/ReportViewer.aspx?%2fMail+Tracker%2fTracking_Email_Dev&rs:Command=Render&rs:ClearSession=true&customerNumber=" + jobCustomerNumber;  //&effortName=seneca_dev_20150415035959
    window.open(report_url);
    //var customer_number = $('#txtCustNumber').val();
    //var email_effort = $('#ddlEmailEffort').val();
    //window.open("http://s-reportserver/Reports/Pages/Report.aspx?ItemPath=%2fMail+Tracker%2fTracking_Email_Dev" + "/" +customer_number + "/" + email_effort);
}
//******************** Public Functions End *************************