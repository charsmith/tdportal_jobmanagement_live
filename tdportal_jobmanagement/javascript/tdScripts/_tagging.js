jQuery.support.cors = true;

//******************** Global Variables Start **************************
var tagJobInfo = {};
var departmentsJson = [];
//******************** Public Functions Start **************************
//var facilityJSON = [
//{ "facilityName": "Chicago", "facilityValue": "1" },
//{ "facilityName": "Los Angeles", "facilityValue": "2" }
//];
var page_ref = (window.location.href.indexOf('index') > -1) ? 'vm' : 'pageObj';
//taggingInfo.prototype.loadPersonalGroups = function (facility_id) {
//    var personal_groups_url = serviceURLDomain + "api/Tagging_personnel/" + facility_id;
//    usersJson = [];
//    departmentsJson = [];
//    var temp_users = [];
//    getCORS(personal_groups_url, null, function (response_data) {
//        $.each(response_data.groups, function (key, val) {
//            departmentsJson.push(key);
//            temp_users = [];
//            $.each(val.people, function (people_key, people_val) {
//                temp_users.push(people_val);
//            });
//            usersJson.push({
//                "department": key,
//                "users": temp_users
//            });
//        });
//    }, function (error_response) {
//        showErrorResponseText(error_response, true);
//    });
//};
//taggingInfo.prototype.loadUsers = function (type) {
//    self.loadTagUsersDDL(usersJson, type);
//    if (type == "tag")
//        $('#fldsetOptions').controlgroup('refresh');
//    if (type == "filter")
//        $('#ulSelectedUsers').empty();
//    $('#fldsetFilterOptions').controlgroup('refresh');
//};
//taggingInfo.prototype.loadTagUsersDDL = function (users_json, type) {
//    var users = '';
//    users += '<option value="-1">-- Select A User -- </option>';
//    var temp_users = [];
//    if (($('#ddlTagDepartments').length > 0 && $('#ddlTagDepartments').val() != "-1") || ($('#ddlFilterDepartments').length > 0 && $('#ddlFilterDepartments').val() != "-1")) {
//        temp_users = $.grep(users_json, function (obj) {
//            return (obj.department === ((type === "tag") ? $('#ddlTagDepartments').val() : $('#ddlFilterDepartments').val()));
//        });
//    }
//    if (temp_users.length > 0) {
//        $.each(temp_users[0].users, function (key, val) {
//            if ((type == "tag") && (Object.keys(tagJobInfo).length > 0 && tagJobInfo.users != undefined && tagJobInfo.users != null)) {
//                if (JSON.stringify(tagJobInfo.users).indexOf(val.userId) == -1 && JSON.stringify(tagJobInfo.users).indexOf(val.name) == -1)
//                    users += '<option value="' + val.userId + '">' + val.name + '</option>';
//            }
//            else
//                users += '<option value="' + val.userId + '">' + val.name + '</option>';
//        });
//    }
//    if (type == "tag" && $('#ddlTaggingType').val() == 4 && $('#ddlTagUsers').length > 0) {
//        $('#ddlTagUsers')[0].disabled = false;
//        $('#ddlTagUsers').empty().append(users);
//        $('#ddlTagUsers').selectmenu('refresh');
//    }
//    else if (type == "tag" && $('#ddlTaggingType').val() == 4) {
//        users = '<select id="ddlTagUsers" data-theme="f" data-mini="true" onchange="' + page_ref + '.tagRegionsOrUsersChange(this,\'users\',\'' + type + '\')" data-iconpos="left">' + users + '</select>';
//        $('#dvOptions').append(users);
//    }
//    if (type == 'filter' && $('#ddlFilterUsers').length > 0) {
//        $('#ddlFilterUsers')[0].disabled = false;
//        $('#ddlFilterUsers').empty().append(users);
//        $('#ddlFilterUsers').selectmenu('refresh');
//    }
//    else if (type == 'filter') {
//        users = '<select id="ddlFilterUsers" data-theme="e" data-mini="true" onchange="' + page_ref + '.tagRegionsOrUsersChange(this,\'users\',\'' + type + '\')" data-iconpos="left">' + users + '</select>';
//        $('#dvFilterOptions').append(users);
//        $('#ddlFilterUsers').selectmenu();
//    }
//};
//taggingInfo.prototype.displayJobWhosBeenTagged = function () {
//    $('#ulWhomsBeenTagged').empty();
//    var tagged_info_list = '';
//    $.each(tagJobInfo, function (key, val) {
//        if (val.length > 0) {
//            var list_header_text = ((key == "regions") ? 'Region(s)' : 'User(s)');
//            tagged_info_list += '<li data-role="list-divider" style="font-size: 12px;"  data-theme="g">' + list_header_text + '</li>';
//            $.each(val, function (item_key, item_val) {
//                var can_be_removed = false;
//                can_be_removed = (key == "regions" && sessionStorage.facilityId == item_val.regionId) ? false : true;
//                tagged_info_list += '<li data-theme="c"' + ((can_be_removed) ? "data-icon='delete'" : "data-icon='false'") + '><a href="#" value="' + ((key == "regions") ? item_val.regionId : item_val.userId) + '">';
//                tagged_info_list += '<h3 style="font-size: 14px;">' + ((key == "regions") ? item_val.regionName : item_val.userName) + '</h3></a>';
//                tagged_info_list += (can_be_removed) ? '<a href="#" onclick="' + page_ref + '.removeSelectedUserOrRegion(this,\'' + ((key == "regions") ? 'regions' : 'Users') + '\',\'' + ((key == "regions") ? '' : item_val.departmentId) + '\')">Remove</a>' : '';
//                tagged_info_list += '</li>';
//            });
//        }
//    });
//    if (tagged_info_list != '')
//        $('#ulWhomsBeenTagged').append(tagged_info_list).listview('refresh');
//};
//taggingInfo.prototype.tagRegionsOrUsersChange = function (ctrl, tag_type, type) {
//    $('#ulSelectedUsers').empty();
//    var selected_option = $(ctrl).find('option:selected');
//    var selected_items = "";
//    var tagging_type_text = "";
//    if (type == "tag") {
//        selected_items = "";
//        tagging_type_text = "";
//        if (($('#ddlTaggingType').val() == "2" && tag_type == "regions") || ($('#ddlTaggingType').val() == "4" && tag_type == "users")) {
//            tagging_type_text = (tag_type === "regions") ? 'Regions' : 'Users';
//            var temp_regions_users = [];
//            $.each(tagJobInfo, function (key, val) {
//                if (key == ((tag_type == "regions") ? "regions" : "users")) {
//                    temp_regions_users = val;
//                    return false;
//                }
//            });
//            if (tag_type === "regions") {
//                temp_regions_users.push({
//                    "regionId": $(selected_option).val(),
//                    "regionName": $(selected_option).text()
//                });
//            }
//            else {
//                temp_regions_users.push({
//                    "userId": $(selected_option).val(),
//                    "userName": $(selected_option).text(),
//                    "departmentId": $('#ddlTagDepartments').val()
//                });
//            }

//            if ((tag_type == "regions") && (Object.keys(tagJobInfo).length > 0)) {
//                var temp = tagJobInfo;
//                tagJobInfo = {};
//                tagJobInfo["regions"] = temp_regions_users;
//                $.each(temp, function (key, val) {
//                    if (key != tag_type)
//                        tagJobInfo[key] = val;
//                });
//            }
//            if (tag_type == "users")
//                tagJobInfo["users"] = temp_regions_users;

//            self.displayJobWhosBeenTagged();
//            $(ctrl).find('option:selected').remove();
//        }
//        else {
//            if ($('#ddlTagRegions').val() != "-1") {
//                self.loadPersonalGroups($('#ddlTagRegions').val());
//                window.setTimeout(function () {
//                    self.loadDepartmentsDDL(departmentsJson, 'tag');
//                    self.loadTagUsersDDL(usersJson, 'tag');
//                }, 1000);
//            }
//        }
//    }
//    else {
//        if (tag_type == "regions") {
//            if ($('#ddlFilterRegions').val() != "-1") {
//                self.loadPersonalGroups($('#ddlFilterRegions').val());
//                window.setTimeout(function () {
//                    self.loadDepartmentsDDL(departmentsJson, 'filter');
//                    self.loadTagUsersDDL(usersJson, 'filter');
//                }, 1000);
//            }
//        }
//        else {
//            selected_items = "";
//            tagging_type_text = "Filter";
//            if ($('#ulSelectedUsers li').length == 0)
//                selected_items = '<li data-role="list-divider" style="font-size: 12px;" data-theme="g">Filter Users</li>';
//            selected_items += '<li data-icon="delete"><a href="#" data-rel="popup" value="' + $(selected_option).val() + '">';
//            selected_items += '<h3 style="font-size: 14px;">' + $(selected_option).text() + '</h3></a>';
//            selected_items += '<a href="#" onclick="' + page_ref + '.removeSelectedUserOrRegion(this,\'' + tagging_type_text + '\',\'' + $('#ddlFilterDepartments').val() + '\')">Remove</a></li>';
//            $('#ulSelectedUsers').append(selected_items).listview('refresh');
//            $('#dvSelectedFilterUsers').css('display', 'block');
//            $(ctrl).find('option:selected').remove();
//            $(ctrl)[0].disabled = true;
//        }
//    }
//};
//taggingInfo.prototype.removeSelectedUserOrRegion = function (ctrl, type, department_id) {
//    var source = (type == "Users" || type == "Filter") ? $.extend(true, [], usersJson) : $.extend(true, [], facilityJSON);
//    var selected_items = (type == "Filter") ? $('#ulSelectedUsers li').not('a[value=""][title=Remove]') : $('#ulWhomsBeenTagged li').not('a[value=""][title="Remove"]');
//    var temp_items = [];
//    var temp_regions_users = [];
//    $.each(tagJobInfo, function (key, val) {
//        if (key == ((department_id == "") ? "regions" : "users")) {
//            temp_regions_users = val;
//            return false;
//        }
//    });

//    $.each(selected_items, function (key, val) {
//        if ($(ctrl.previousSibling).attr('value') != $(val).find('a').attr('value')) {
//            if (department_id == "") {
//                temp_items.push({
//                    'facilityName': $(val).find('a').text(),
//                    'facilityValue': $(val).find('a').attr('value')
//                });
//            }
//            else {
//                temp_items.push({
//                    'name': $(val).find('a').text(),
//                    'userId': $(val).find('a').attr('value')
//                });
//            }
//        }
//        else {
//            //var key_index;
//            var is_found = false;
//            var selected_item = $(val).find('a').attr('value');
//            $.each(temp_regions_users, function (key, val) {
//                if (is_found) {
//                    return false;
//                }
//                //key_index = key;

//                if (((department_id == "") ? val.regionId : val.userId) == selected_item) {
//                    temp_regions_users.splice(key, 1);
//                    is_found = true;
//                    return false;
//                }

//            });
//        }
//    });

//    var options = $.grep(source, function (obj) {
//        if (type == "Users" || type == "Filter") {
//            var temp_users = [];
//            $.each(obj.users, function (key_user, val_user) {
//                if (JSON.stringify(temp_items).indexOf(val_user.name) == -1) {
//                    temp_users.push(obj.users[key_user]);
//                }
//            });
//            obj.users = temp_users;
//            return obj;
//        }
//        else
//            return JSON.stringify(temp_items).indexOf(obj.facilityValue) == -1;
//    });

//    if (type == "Users")
//        self.loadTagUsersDDL(options, 'tag');
//    else if (type == "Filter")
//        self.loadTagUsersDDL(options, 'filter');
//    else if ($('#ddlTaggingType').val() != "-1" && $('#ddlTaggingType').val() != "1" && $('#ddlTaggingType').val() != "4" && type == "regions")
//        self.loadTagRegionsDDL(options, 'tag');
//    $(ctrl).parent().remove();
//    if (type == "Filter") {
//        if ($('#ulSelectedUsers li[data-role!="list-divider"]').length == 0) {
//            $('#ulSelectedUsers').empty();
//            $('#dvSelectedFilterUsers').css('display', 'none');
//        }
//    } else {
//        self.displayJobWhosBeenTagged();
//    }
//};


//taggingInfo.prototype.addUsers = function (user_value, user_name, ctrl) {
//    $('#li' + user_value).find('a').removeAttr('onclick');
//    window.setTimeout(function () { self.addSelectedUsers(user_value, user_name, ctrl); }, 100);
//};
//taggingInfo.prototype.fillUsers = function (ctrl, template_to_bind, json_to_bind) {
//    $('#' + ctrl).empty();
//    if (ctrl.toLowerCase().indexOf('selected') > -1)
//        $("#" + template_to_bind).tmpl(json_to_bind).attr('data-icon', 'delete').appendTo("#" + ctrl);
//    else {
//        $("#" + template_to_bind).tmpl(json_to_bind).appendTo("#" + ctrl);
//    }
//    if (selectedUsers.length > 0) {
//        $.each(selectedUsers, function (a, b) {
//            if (ctrl.toLowerCase().indexOf('selected') == -1)
//                $('#' + ctrl).find('li[id=li' + b.userName + ']').remove();
//        });
//        if (ctrl.toLowerCase().indexOf('selected') > -1)
//            $('#' + ctrl).prepend('<li data-role="list-divider" >Selected Users</li>');
//    }
//    window.setTimeout(function () { $('#' + ctrl).listview('refresh'); }, 100);
//};
//taggingInfo.prototype.addSelectedUsers = function (user_value, user_name, ctrl) {
//    selectedUsers.push({ "fullName": user_name, "userName": user_value });
//    selectedUsers = selectedUsers.sort(function (a, b) {
//        return a.fullName > b.fullName ? 1 : -1;
//    });
//    $('#' + ctrl).find('li[id=li' + user_value + ']').remove();
//    self.fillUsers('ulSelectedUsers', 'selectedUsersTemplate', selectedUsers);
//    if ($('#ulSelectedUsers li:not([id^=li])').length == 0)
//        $('#ulSelectedUsers').prepend('<li data-role="list-divider" >Selected Users</li>');
//    $('#ulSelectedUsers').listview('refresh');
//    if (selectedUsers.length > 0)
//        $('#ulSelectedUsers').show();

//};
//taggingInfo.prototype.removeUsers = function (user_value) {
//    window.setTimeout(function () { self.removeSelectedUsers(user_value); }, 100);
//};
//taggingInfo.prototype.removeSelectedUsers = function (user_value) {
//    var count = 0;
//    $.each(selectedUsers, function (a, b) {
//        if (b.userName.toLowerCase() == user_value.toLowerCase())
//            return false;
//        else
//            count++;
//    });
//    selectedUsers.splice(count, 1);
//    if (selectedUsers.length == 0)
//        $('#ulSelectedUsers').hide();
//    self.fillUsers('ulSelectedUsers', 'selectedUsersTemplate', selectedUsers);
//    if ($('#ulSelectedUsers li:not([id^=li])').length == 0)
//        $('#ulSelectedUsers').prepend('<li data-role="list-divider" >Selected Users</li>');
//    $('#ulSelectedUsers').listview('refresh');
//    self.fillUsers('ulUserName', 'usersTemplate', usersJson);
//};
//taggingInfo.prototype.manageTaggingOptions = function (ctrl_id) {
//    var selected_type = $(ctrl_id).val();
//    if (selected_type != "") {
//        switch (selected_type) {
//            case "1":
//                $('#dvOptions').empty();
//                $('#ddlTagRegions').css('display', 'none');
//                $('#ddlTagDepartments').css('display', 'none');
//                $('#ddlTagUsers').css('display', 'none');
//                $('#dvOptions').css('display', 'block');
//                var facility_name = getTribuneFacilityName(sessionStorage.facilityId);
//                if (Object.keys(tagJobInfo).length > 0 && tagJobInfo.regions != undefined && tagJobInfo.regions != null && tagJobInfo.regions.length > 0) {
//                    if (JSON.stringify(tagJobInfo.regions).indexOf(sessionStorage.facilityId) == -1 && JSON.stringify(tagJobInfo.regions).indexOf(facility_name) == -1)
//                        tagJobInfo.regions.push({ "regionId": sessionStorage.facilityId, "regionName": facility_name });
//                }
//                else
//                    tagJobInfo["regions"] = [{ "regionId": sessionStorage.facilityId, "regionName": facility_name }];

//                if (Object.keys(tagJobInfo).length > 0 && tagJobInfo.users != undefined && tagJobInfo.users != null && tagJobInfo.users.length > 0 && (JSON.stringify(tagJobInfo.users).indexOf(sessionStorage.username) == -1)) {
//                    tagJobInfo.users.push({ "userId": sessionStorage.username, "userName": sessionStorage.username });
//                }
//                else
//                    tagJobInfo["users"] = [{ "userId": sessionStorage.username, "userName": sessionStorage.username }];
//                self.displayJobWhosBeenTagged();
//                break;
//            case "2":
//                $('#dvOptions').empty();
//                $('#ulSelectedRegionsOrUsers').empty().listview('refresh');
//                self.loadTagRegionsDDL(facilityJSON, 'tag');
//                $('#ddlTagRegions').css('display', 'block');
//                $('#ddlTagDepartments').css('display', 'none');
//                $('#ddlTagUsers').css('display', 'none');
//                $('#dvOptions').trigger('create');
//                break;
//            case "3":
//                break;
//            case "4":
//                $('#dvOptions').empty();
//                $('#ulSelectedRegionsOrUsers').empty().listview('refresh');
//                self.loadTagRegionsDDL(facilityJSON, 'tagusers');
//                self.loadDepartmentsDDL(departmentsJson, 'tag');
//                self.loadTagUsersDDL(usersJson, 'tag');
//                $('#ddlTagRegions').css('display', 'block');
//                $('#ddlTagDepartments').css('display', 'block');
//                $('#ddlTagUsers').css('display', 'block');
//                $('#dvOptions').trigger('create');
//                break;
//            default:
//                break;
//        }
//        $('#fldsetOptions').controlgroup('refresh');
//    }
//};
//taggingInfo.prototype.getPageDisplayName = function (name) {
//    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
//    var display_name = '';
//    $.each(name, function (key, value) {
//        if (key > 0 && value.match(PATTERN)) {
//            display_name += ' ' + value;
//        }
//        else {
//            display_name += (key == 0) ? value.initCap() : value;
//        }
//    });
//    return display_name;
//};

var loadTagRegionsDDL = function (self) {
    self.loadTagRegionsDDL = function (facilityJSON, type) {
        var regions = '';
        regions += '<option value="-1">-- Select A Region --</option>';
        $.each(facilityJSON, function (key, val) {
            var selected = '';
            if (val.facilityValue == sessionStorage.facilityId) //{
                selected = 'selected';
            if ((type == "tag") && Object.keys(tagJobInfo).length > 0 && (tagJobInfo.regions != undefined && tagJobInfo.regions != null && tagJobInfo.regions.length > 0)) {
                if (JSON.stringify(tagJobInfo.regions).indexOf(val.facilityValue) == -1 && JSON.stringify(tagJobInfo.regions).indexOf(val.facilityName) == -1)
                    regions += '<option value="' + val.facilityValue + '" ' + selected + ' >' + val.facilityName + '</option>';
            }
            else {
                regions += '<option value="' + val.facilityValue + '" ' + selected + ' >' + val.facilityName + '</option>';
            }
        });
        if (type == 'tag' || type == 'tagusers') {
            if ($('#ddlTagRegions').length > 0) {
                $('#ddlTagRegions').empty().append(regions);
                $('#ddlTagRegions').selectmenu('refresh');
            }
            else {
                regions = '<select id="ddlTagRegions" data-theme="f" data-mini="true" onchange="' + page_ref + '.tagRegionsOrUsersChange(this,\'regions\',\'tag\')" data-iconpos="left">' + regions + '</select>';
                $('#dvOptions').append(regions);
            }
        }
        else if (type == 'filter') {
            if ($('#ddlFilterRegions').length > 0) {
                $('#ddlFilterRegions').empty().append(regions);
                $('#ddlFilterRegions').selectmenu('refresh');
            }
            else {
                regions = '<select id="ddlFilterRegions" data-theme="e" data-mini="true" onchange="' + page_ref + '.tagRegionsOrUsersChange(this,\'regions\',\'filter\')" data-iconpos="left">' + regions + '</select>';
                $('#dvFilterOptions').append(regions);
                $('#ddlFilterRegions').selectmenu();
            }
        }
    };
};
var loadDepartmentsDDL = function (self) {
    self.loadDepartmentsDDL = function (departments, type) {
        var depts = '';
        depts += '<option value="-1">-- Select A Department --</option>';
        $.each(departmentsJson, function (key, val) {
            depts += '<option value="' + val + '">' + self.getPageDisplayName(val) + '</option>';
        });
        if (type == 'tag') {
            if ($('#ddlTagDepartments').length > 0) {
                $('#ddlTagDepartments').empty().append(depts);
                $('#ddlTagDepartments').selectmenu('refresh');
            }
            else {
                depts = '<select id="ddlTagDepartments" data-theme="f" data-mini="true" onchange="' + page_ref + '.loadUsers(\'tag\')" data-iconpos="left">' + depts + '</select>';
                $('#dvOptions').append(depts);
            }
        }
        else if (type == 'filter') {
            if ($('#ddlFilterDepartments').length > 0) {
                $('#ddlFilterDepartments').empty().append(depts);
                $('#ddlFilterDepartments').selectmenu('refresh');
            }
            else {
                depts = '<select id="ddlFilterDepartments" data-theme="e" data-mini="true" onchange="' + page_ref + '.loadUsers(\'filter\')" data-iconpos="left">' + depts + '</select>';
                $('#dvFilterOptions').append(depts);
                $('#ddlFilterDepartments').selectmenu();
            }
        }
    };
};
var loadPersonalGroups = function (self) {
    self.loadPersonalGroups = function (facility_id) {
        var personal_groups_url = serviceURLDomain + "api/Tagging_personnel/" + facility_id;
        usersJson = [];
        departmentsJson = [];
        var temp_users = [];
        getCORS(personal_groups_url, null, function (response_data) {
            $.each(response_data.groups, function (key, val) {
                departmentsJson.push(key);
                temp_users = [];
                $.each(val.people, function (people_key, people_val) {
                    temp_users.push(people_val);
                });
                usersJson.push({
                    "department": key,
                    "users": temp_users
                });
            });
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };
};
var loadUsers = function (self) {
    self.loadUsers = function (type) {
        self.loadTagUsersDDL(usersJson, type);
        if (type == "tag")
            $('#fldsetOptions').controlgroup('refresh');
        if (type == "filter")
            $('#ulSelectedUsers').empty();
        $('#fldsetFilterOptions').controlgroup('refresh');
    };
};
var loadTagUsersDDL = function (self) {
    self.loadTagUsersDDL = function (users_json, type) {
        var users = '';
        users += '<option value="-1">-- Select A User -- </option>';
        var temp_users = [];
        if (($('#ddlTagDepartments').length > 0 && $('#ddlTagDepartments').val() != "-1") || ($('#ddlFilterDepartments').length > 0 && $('#ddlFilterDepartments').val() != "-1")) {
            temp_users = $.grep(users_json, function (obj) {
                return (obj.department === ((type === "tag") ? $('#ddlTagDepartments').val() : $('#ddlFilterDepartments').val()));
            });
        }
        if (temp_users.length > 0) {
            $.each(temp_users[0].users, function (key, val) {
                if ((type == "tag") && (Object.keys(tagJobInfo).length > 0 && tagJobInfo.users != undefined && tagJobInfo.users != null)) {
                    if (JSON.stringify(tagJobInfo.users).indexOf(val.userId) == -1 && JSON.stringify(tagJobInfo.users).indexOf(val.name) == -1)
                        users += '<option value="' + val.userId + '">' + val.name + '</option>';
                }
                else
                    users += '<option value="' + val.userId + '">' + val.name + '</option>';
            });
        }
        if (type == "tag" && $('#ddlTaggingType').val() == 4 && $('#ddlTagUsers').length > 0) {
            $('#ddlTagUsers')[0].disabled = false;
            $('#ddlTagUsers').empty().append(users);
            $('#ddlTagUsers').selectmenu('refresh');
        }
        else if (type == "tag" && $('#ddlTaggingType').val() == 4) {
            users = '<select id="ddlTagUsers" data-theme="f" data-mini="true" onchange="' + page_ref + '.tagRegionsOrUsersChange(this,\'users\',\'' + type + '\')" data-iconpos="left">' + users + '</select>';
            $('#dvOptions').append(users);
        }
        if (type == 'filter' && $('#ddlFilterUsers').length > 0) {
            $('#ddlFilterUsers')[0].disabled = false;
            $('#ddlFilterUsers').empty().append(users);
            $('#ddlFilterUsers').selectmenu('refresh');
        }
        else if (type == 'filter') {
            if ($('#ddlFilterUsers').length > 0) {
                $('#ddlFilterUsers')[0].disabled = false;
                $('#ddlFilterUsers').empty().append(users);
                $('#ddlFilterUsers').selectmenu('refresh');
            }
            else {
                users = '<select id="ddlFilterUsers" data-theme="e" data-mini="true" onchange="' + page_ref + '.tagRegionsOrUsersChange(this,\'users\',\'' + type + '\')" data-iconpos="left">' + users + '</select>';
                $('#dvFilterOptions').append(users);
                $('#ddlFilterUsers').selectmenu();
            }
        }
    };
};
var displayJobWhosBeenTagged = function (self) {
    self.displayJobWhosBeenTagged = function () {
        $('#ulWhomsBeenTagged').empty();
        var tagged_info_list = '';
        $.each(tagJobInfo, function (key, val) {
            if (val.length > 0) {
                var list_header_text = ((key == "regions") ? 'Region(s)' : 'User(s)');
                tagged_info_list += '<li data-role="list-divider" style="font-size: 12px;"  data-theme="g">' + list_header_text + '</li>';
                $.each(val, function (item_key, item_val) {
                    var can_be_removed = false;
                    can_be_removed = (key == "regions" && sessionStorage.facilityId == item_val.regionId) ? false : true;
                    tagged_info_list += '<li data-theme="c"' + ((can_be_removed) ? "data-icon='delete'" : "data-icon='false'") + '><a href="#" value="' + ((key == "regions") ? item_val.regionId : item_val.userId) + '">';
                    tagged_info_list += '<h3 style="font-size: 14px;">' + ((key == "regions") ? item_val.regionName : item_val.userName) + '</h3></a>';
                    tagged_info_list += (can_be_removed) ? '<a href="#" onclick="' + page_ref + '.removeSelectedUserOrRegion(this,\'' + ((key == "regions") ? 'regions' : 'Users') + '\',\'' + ((key == "regions") ? '' : item_val.departmentId) + '\')">Remove</a>' : '';
                    tagged_info_list += '</li>';
                });
            }
        });
        if (tagged_info_list != '')
            $('#ulWhomsBeenTagged').append(tagged_info_list).listview('refresh');
    };
};
var tagRegionsOrUsersChange = function (self) {
    self.tagRegionsOrUsersChange = function (ctrl, tag_type, type) {
        $('#ulSelectedUsers').empty();
        var selected_option = $(ctrl).find('option:selected');
        var selected_items = "";
        var tagging_type_text = "";
        if (type == "tag") {
            selected_items = "";
            tagging_type_text = "";
            if (($('#ddlTaggingType').val() == "2" && tag_type == "regions") || ($('#ddlTaggingType').val() == "4" && tag_type == "users")) {
                tagging_type_text = (tag_type === "regions") ? 'Regions' : 'Users';
                var temp_regions_users = [];
                $.each(tagJobInfo, function (key, val) {
                    if (key == ((tag_type == "regions") ? "regions" : "users")) {
                        temp_regions_users = val;
                        return false;
                    }
                });
                if (tag_type === "regions") {
                    temp_regions_users.push({
                        "regionId": $(selected_option).val(),
                        "regionName": $(selected_option).text()
                    });
                }
                else {
                    temp_regions_users.push({
                        "userId": $(selected_option).val(),
                        "userName": $(selected_option).text(),
                        "departmentId": $('#ddlTagDepartments').val()
                    });
                }

                if ((tag_type == "regions") && (Object.keys(tagJobInfo).length > 0)) {
                    var temp = tagJobInfo;
                    tagJobInfo = {};
                    tagJobInfo["regions"] = temp_regions_users;
                    $.each(temp, function (key, val) {
                        if (key != tag_type)
                            tagJobInfo[key] = val;
                    });
                }
                if (tag_type == "users")
                    tagJobInfo["users"] = temp_regions_users;

                self.displayJobWhosBeenTagged();
                $(ctrl).find('option:selected').remove();
            }
            else {
                if ($('#ddlTagRegions').val() != "-1") {
                    self.loadPersonalGroups($('#ddlTagRegions').val());
                    window.setTimeout(function () {
                        self.loadDepartmentsDDL(departmentsJson, 'tag');
                        self.loadTagUsersDDL(usersJson, 'tag');
                    }, 1000);
                }
            }
        }
        else {
            if (tag_type == "regions") {
                if ($('#ddlFilterRegions').val() != "-1") {
                    self.loadPersonalGroups($('#ddlFilterRegions').val());
                    window.setTimeout(function () {
                        self.loadDepartmentsDDL(departmentsJson, 'filter');
                        self.loadTagUsersDDL(usersJson, 'filter');
                    }, 1000);
                }
            }
            else {
                selected_items = "";
                tagging_type_text = "Filter";
                if ($('#ulSelectedUsers li').length == 0)
                    selected_items = '<li data-role="list-divider" style="font-size: 12px;" data-theme="g">Filter Users</li>';
                selected_items += '<li data-icon="delete"><a href="#" data-rel="popup" value="' + $(selected_option).val() + '">';
                selected_items += '<h3 style="font-size: 14px;">' + $(selected_option).text() + '</h3></a>';
                selected_items += '<a href="#" onclick="' + page_ref + '.removeSelectedUserOrRegion(this,\'' + tagging_type_text + '\',\'' + $('#ddlFilterDepartments').val() + '\')">Remove</a></li>';
                $('#ulSelectedUsers').append(selected_items).listview('refresh');
                $('#dvSelectedFilterUsers').css('display', 'block');
                $(ctrl).find('option:selected').remove();
                $(ctrl)[0].disabled = true;
            }
        }
    };
};
var removeSelectedUserOrRegion = function (self) {
    self.removeSelectedUserOrRegion = function (ctrl, type, department_id) {
        var source = (type == "Users" || type == "Filter") ? $.extend(true, [], usersJson) : $.extend(true, [], facilityJSON);
        var selected_items = (type == "Filter") ? $('#ulSelectedUsers li').not('a[value=""][title=Remove]') : $('#ulWhomsBeenTagged li').not('a[value=""][title="Remove"]');
        var temp_items = [];
        var temp_regions_users = [];
        $.each(tagJobInfo, function (key, val) {
            if (key == ((department_id == "") ? "regions" : "users")) {
                temp_regions_users = val;
                return false;
            }
        });

        $.each(selected_items, function (key, val) {
            if ($(ctrl.previousSibling).attr('value') != $(val).find('a').attr('value')) {
                if (department_id == "") {
                    temp_items.push({
                        'facilityName': $(val).find('a').text(),
                        'facilityValue': $(val).find('a').attr('value')
                    });
                }
                else {
                    temp_items.push({
                        'name': $(val).find('a').text(),
                        'userId': $(val).find('a').attr('value')
                    });
                }
            }
            else {
                //var key_index;
                var is_found = false;
                var selected_item = $(val).find('a').attr('value');
                $.each(temp_regions_users, function (key, val) {
                    if (is_found) {
                        return false;
                    }
                    //key_index = key;

                    if (((department_id == "") ? val.regionId : val.userId) == selected_item) {
                        temp_regions_users.splice(key, 1);
                        is_found = true;
                        return false;
                    }

                });
            }
        });

        var options = $.grep(source, function (obj) {
            if (type == "Users" || type == "Filter") {
                var temp_users = [];
                $.each(obj.users, function (key_user, val_user) {
                    if (JSON.stringify(temp_items).indexOf(val_user.name) == -1) {
                        temp_users.push(obj.users[key_user]);
                    }
                });
                obj.users = temp_users;
                return obj;
            }
            else
                return JSON.stringify(temp_items).indexOf(obj.facilityValue) == -1;
        });

        if (type == "Users")
            self.loadTagUsersDDL(options, 'tag');
        else if (type == "Filter")
            self.loadTagUsersDDL(options, 'filter');
        else if ($('#ddlTaggingType').val() != "-1" && $('#ddlTaggingType').val() != "1" && $('#ddlTaggingType').val() != "4" && type == "regions")
            self.loadTagRegionsDDL(options, 'tag');
        $(ctrl).parent().remove();
        if (type == "Filter") {
            if ($('#ulSelectedUsers li[data-role!="list-divider"]').length == 0) {
                $('#ulSelectedUsers').empty();
                $('#dvSelectedFilterUsers').css('display', 'none');
            }
        } else {
            self.displayJobWhosBeenTagged();
        }
    };
};
var addUsers = function (self) {
    self.addUsers = function (user_value, user_name, ctrl) {
        $('#li' + user_value).find('a').removeAttr('onclick');
        window.setTimeout(function () { self.addSelectedUsers(user_value, user_name, ctrl); }, 100);
    };
};
var fillUsers = function (self) {
    self.fillUsers = function (ctrl, template_to_bind, json_to_bind) {
        $('#' + ctrl).empty();
        if (ctrl.toLowerCase().indexOf('selected') > -1)
            $("#" + template_to_bind).tmpl(json_to_bind).attr('data-icon', 'delete').appendTo("#" + ctrl);
        else {
            $("#" + template_to_bind).tmpl(json_to_bind).appendTo("#" + ctrl);
        }
        if (selectedUsers.length > 0) {
            $.each(selectedUsers, function (a, b) {
                if (ctrl.toLowerCase().indexOf('selected') == -1)
                    $('#' + ctrl).find('li[id=li' + b.userName + ']').remove();
            });
            if (ctrl.toLowerCase().indexOf('selected') > -1)
                $('#' + ctrl).prepend('<li data-role="list-divider" >Selected Users</li>');
        }
        window.setTimeout(function () { $('#' + ctrl).listview('refresh'); }, 100);
    };
};
var addSelectedUsers = function (self) {
    self.addSelectedUsers = function (user_value, user_name, ctrl) {
        selectedUsers.push({ "fullName": user_name, "userName": user_value });
        selectedUsers = selectedUsers.sort(function (a, b) {
            return a.fullName > b.fullName ? 1 : -1;
        });
        $('#' + ctrl).find('li[id=li' + user_value + ']').remove();
        self.fillUsers('ulSelectedUsers', 'selectedUsersTemplate', selectedUsers);
        if ($('#ulSelectedUsers li:not([id^=li])').length == 0)
            $('#ulSelectedUsers').prepend('<li data-role="list-divider" >Selected Users</li>');
        $('#ulSelectedUsers').listview('refresh');
        if (selectedUsers.length > 0)
            $('#ulSelectedUsers').show();

    };
};
var removeUsers = function (self) {
    self.removeUsers = function (user_value) {
        window.setTimeout(function () { self.removeSelectedUsers(user_value); }, 100);
    };
};
var removeSelectedUsers = function (self) {
    self.removeSelectedUsers = function (user_value) {
        var count = 0;
        $.each(selectedUsers, function (a, b) {
            if (b.userName.toLowerCase() == user_value.toLowerCase())
                return false;
            else
                count++;
        });
        selectedUsers.splice(count, 1);
        if (selectedUsers.length == 0)
            $('#ulSelectedUsers').hide();
        self.fillUsers('ulSelectedUsers', 'selectedUsersTemplate', selectedUsers);
        if ($('#ulSelectedUsers li:not([id^=li])').length == 0)
            $('#ulSelectedUsers').prepend('<li data-role="list-divider" >Selected Users</li>');
        $('#ulSelectedUsers').listview('refresh');
        self.fillUsers('ulUserName', 'usersTemplate', usersJson);
    };
};
var manageTaggingOptions = function (self) {
    self.manageTaggingOptions = function (ctrl_id) {
        var selected_type = $(ctrl_id).val();
        if (selected_type != "") {
            switch (selected_type) {
                case "1":
                    $('#dvOptions').empty();
                    $('#ddlTagRegions').css('display', 'none');
                    $('#ddlTagDepartments').css('display', 'none');
                    $('#ddlTagUsers').css('display', 'none');
                    $('#dvOptions').css('display', 'block');
                    var facility_name = getTribuneFacilityName(sessionStorage.facilityId);
                    if (Object.keys(tagJobInfo).length > 0 && tagJobInfo.regions != undefined && tagJobInfo.regions != null && tagJobInfo.regions.length > 0) {
                        if (JSON.stringify(tagJobInfo.regions).indexOf(sessionStorage.facilityId) == -1 && JSON.stringify(tagJobInfo.regions).indexOf(facility_name) == -1)
                            tagJobInfo.regions.push({ "regionId": sessionStorage.facilityId, "regionName": facility_name });
                    }
                    else
                        tagJobInfo["regions"] = [{ "regionId": sessionStorage.facilityId, "regionName": facility_name }];

                    if (Object.keys(tagJobInfo).length > 0 && tagJobInfo.users != undefined && tagJobInfo.users != null && tagJobInfo.users.length > 0 && (JSON.stringify(tagJobInfo.users).indexOf(sessionStorage.username) == -1)) {
                        tagJobInfo.users.push({ "userId": sessionStorage.username, "userName": sessionStorage.username });
                    }
                    else
                        tagJobInfo["users"] = [{ "userId": sessionStorage.username, "userName": sessionStorage.username }];
                    self.displayJobWhosBeenTagged();
                    break;
                case "2":
                    $('#dvOptions').empty();
                    $('#ulSelectedRegionsOrUsers').empty().listview('refresh');
                    self.loadTagRegionsDDL(facilityJSON, 'tag');
                    $('#ddlTagRegions').css('display', 'block');
                    $('#ddlTagDepartments').css('display', 'none');
                    $('#ddlTagUsers').css('display', 'none');
                    $('#dvOptions').trigger('create');
                    break;
                case "3":
                    break;
                case "4":
                    $('#dvOptions').empty();
                    $('#ulSelectedRegionsOrUsers').empty().listview('refresh');
                    self.loadTagRegionsDDL(facilityJSON, 'tagusers');
                    self.loadDepartmentsDDL(departmentsJson, 'tag');
                    self.loadTagUsersDDL(usersJson, 'tag');
                    $('#ddlTagRegions').css('display', 'block');
                    $('#ddlTagDepartments').css('display', 'block');
                    $('#ddlTagUsers').css('display', 'block');
                    $('#dvOptions').trigger('create');
                    break;
                default:
                    break;
            }
            $('#fldsetOptions').controlgroup('refresh');
        }
    };
};
var getPageDisplayName = function (self) {
    self.getPageDisplayName = function (name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var display_name = new Array(name);
        $.each(display_name, function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                display_name += ' ' + value;
            }
            else {
                display_name += (key == 0) ? value.initCap() : value;
            }
        });
        return display_name;
    };
};
var taggingInfo = function (self) {
    self.tagJobClick = function () {
        //var tagging_type = "";
        //if ($('#ddlTaggingType').val() == "2") {
        //    tagging_type = 'region';
        //}
        //else if ($('#ddlTaggingType').val() == "1" || $('#ddlTaggingType').val() == "4") {
        //    tagging_type = 'assigned';
        //}

        if (Object.keys(tagJobInfo).length > 0) {
            var job_number = "";
            job_number = (window.location.href.indexOf('index') > -1) ? self.selectedJob().jobNumber : sessionStorage.jobNumber;
            var job_cust_number = (window.location.href.indexOf('index') > -1) ? self.selectedJob().jobCustomerNumber : jobCustomerNumber;
            var tagg_job_url = serviceURLDomain + 'api/Tagging/' + sessionStorage.facilityId + '/' + job_cust_number + '/' + job_number;
            $('#popupJobStatus').popup('close');
            postCORS(tagg_job_url, JSON.stringify(tagJobInfo), function (response_data) {
                if (response_data == "success") {
                    $('#alertmsg').text("Job has been assigned successfully.");
                    $('#popupDialog a[id=okBut]').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                    $('#popupDialog').popup('open');
                }

            }, function (error_response) {
                showErrorResponseText(error_response, true);
            });
        }
    };
    loadTagRegionsDDL(self);
    loadDepartmentsDDL(self);
    loadPersonalGroups(self);
    loadUsers(self);
    loadTagUsersDDL(self);
    displayJobWhosBeenTagged(self);
    tagRegionsOrUsersChange(self);
    removeSelectedUserOrRegion(self);
    addUsers(self);
    fillUsers(self);
    addSelectedUsers(self);
    removeUsers(self);
    removeSelectedUsers(self);
    manageTaggingOptions(self);
    getPageDisplayName(self);
};

//******************** Public Functions End *************************