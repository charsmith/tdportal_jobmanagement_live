﻿$('#_jobSpecialInstructions').live('pagebeforecreate', function (event) {
    displayNavLinks();

    if (jQuery.browser.msie) {
        createIFrame();
    }
    //test for mobility...
    loadMobility();
});

$(document).on('pageshow', '#_jobSpecialInstructions', function (event) {
    if (!jQuery.browser.msie) {
        getData();
    }
});

$.mobile.popup.prototype.options.history = false;

var gServiceUrl = '../JSON/_jobSpecialInstructions.JSON';
var gData;
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var gOutputData;

function getData() {
    if (jQuery.browser.msie) {
        setTimeout(function loadData() { callService("GET", gServiceUrl, ""); }, 2000);
    }
    else {
        $.getJSON(gServiceUrl, function (data) {
            gData = data;
            buildLists();
            makeGOutputData();
        });
    }
}

function makeGOutputData() {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        buildOutputLists();
    }
    else {
        $.getJSON(gOutputServiceUrl, function (dataOutput) {
            gOutputData = dataOutput;
            buildOutputLists();
        });
    }
}

function buildLists() {
    $.each(gData, function (key, val) {
        switch (val.list) {
            //case "mergeFilter":
            //    createTable('Merge');
            //    break;
            case "batchFilter":
                createTable('Batch');
                break;
            case "versionFilter":
                createTable('Version');
                break;
        }
    });
}

function buildOutputLists() {
    if (gOutputData != null) {
        getUploadedFilesCount();
    }
    $.each(gOutputData.instructionsAction[0], function (keyInstructionsAction, valInstructionsAction) {
        switch (keyInstructionsAction) {
            //case "mergeInstructions":
            //    var counter = 0;
            //    for (var i = 0; i < valInstructionsAction.length; i++) {
            //        counter++;
            //        if (counter > 1)
            //            createTable('Merge');
            //        $('#txtMergeName' + counter).val(valInstructionsAction[i].name);
            //        $('#selMergeFilter' + counter).val(valInstructionsAction[i].filter);
            //        $('#selMergeFilter' + counter).selectmenu('refresh');
            //        $('#taMergeDescription' + counter).val(valInstructionsAction[i].description);

            //    }
            //    break;
            case "batchInstructions":
                var counter = 0;
                for (var i = 0; i < valInstructionsAction.length; i++) {
                    counter++;
                    if (counter > 1)
                        createTable('Batch');
                    $('#txtBatchName' + counter).val(valInstructionsAction[i].name);
                    $('#selBatchFilter' + counter).val(valInstructionsAction[i].filter);
                    $('#selBatchFilter' + counter).selectmenu('refresh');
                    $('#taBatchDescription' + counter).val(valInstructionsAction[i].description);
                }
                break;
            case "versionInstructions":
                var counter = 0;
                for (var i = 0; i < valInstructionsAction.length; i++) {
                    counter++;
                    if (counter > 1)
                        createTable('Version');
                    $('#txtVersionName' + counter).val(valInstructionsAction[i].name);
                    $('#selVersionFilter' + counter).val(valInstructionsAction[i].filter);
                    $('#selVersionFilter' + counter).selectmenu('refresh');
                    $('#taVersionDescription' + counter).val(valInstructionsAction[i].description);
                }
                break;
        }

    });
}

function createTable(type) {
    var count = $("#ul" + type).find('input[id^=txt' + type + ']').length;
    count += 1;

    var data = '<li data-role="fieldcontain" id="li' + type + count + '"><a><div data-role="fieldcontain">';
    data += '<label for="txt' + type + 'Name' + count + '">Name:</label><input type="text" name="txt' + type + 'Name' + count + '" id="txt' + type + 'Name' + count + '" value="" data-mini="true" /></div>';
    data += '<div data-role="fieldcontain"><label for="sel' + type + 'Filter' + count + '" class="select">Filter:</label>';
    data += '<select name="sel' + type + 'Filter' + count + '" id="sel' + type + 'Filter' + count + '" data-mini="true"></select></div>';
    data += '<div data-role="fieldcontain"><label for="ta' + type + 'Description' + count + '">Description:</label>';
    data += '<textarea cols="40" rows="8" name="ta' + type + 'Description' + count + '" id="ta' + type + 'Description' + count + '" data-mini="true"></textarea></div>';
    data += '</a><a href="#" onclick="fnRemove(\'' + type + '\',\'' + count + '\');">Remove</a></li>';

    $("#ul" + type).append(data);
    $(".ui-page").trigger('create');
    $("#ul" + type).listview('refresh');

    $.each(gData, function (key, val) {
        switch (val.list) {
            case type.toLowerCase() + "Filter":
                fillValues(val, 'sel' + type + 'Filter' + count);
                break;
        }
    });
}

function fnRemove(type, count) {
    $("#li" + type + count).remove();
}

function fillValues(Obj, ctrl) {
    $.each(Obj.items, function (key, val) {
        $("#" + ctrl).append('<option  value="' + val.value + '">' + val.name + '</option>');
    });
    $("#" + ctrl).selectmenu('refresh');
}

function saveSpecialInstructions() {
    $.each(gOutputData.instructionsAction[0], function (keyInstructionsAction, valInstructionsAction) {
        switch (keyInstructionsAction) {
            //case "mergeInstructions":
            //    gOutputData.instructionsAction[0]["mergeInstructions"] = [];
            //    $.each($("#ulMerge").find('input[id^=txtMergeName]'), function (a, b) {
            //        gOutputData.instructionsAction[0]["mergeInstructions"].push({
            //            name: $('#txtMergeName' + (a + 1)).val(),
            //            filter: $('#selMergeFilter' + (a + 1)).val(),
            //            description: $('#taMergeDescription' + (a + 1)).val()
            //        });
            //    });
            //    break;
            case "batchInstructions":
                gOutputData.instructionsAction[0]["batchInstructions"] = [];
                $.each($("#ulBatch").find('input[id^=txtBatchName]'), function (a, b) {
                    gOutputData.instructionsAction[0]["batchInstructions"].push({
                        name: $('#txtBatchName' + (a + 1)).val(),
                        filter: $('#selBatchFilter' + (a + 1)).val(),
                        description: $('#taBatchDescription' + (a + 1)).val()
                    });
                });
                break;
            case "versionInstructions":
                gOutputData.instructionsAction[0]["versionInstructions"] = [];
                $.each($("#ulVersion").find('input[id^=txtVersionName]'), function (a, b) {
                    gOutputData.instructionsAction[0]["versionInstructions"].push({
                        name: $('#txtVersionName' + (a + 1)).val(),
                        filter: $('#selVersionFilter' + (a + 1)).val(),
                        description: $('#taVersionDescription' + (a + 1)).val()
                    });
                });
                break;
        }
    });
    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
    return true;
}