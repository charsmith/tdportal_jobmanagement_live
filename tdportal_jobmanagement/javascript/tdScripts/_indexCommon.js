﻿function updateStartIndex(selector_id, list_div, caption, column) {
    var multiplier = parseInt($('#' + selector_id).prop("selectedIndex"));
    gDataStartIndex = multiplier * ((caption == "CUSTOMER") ? 10 : gPageSize);
    var page_array = [];

    if (window.location.href.toString().indexOf('regional') > -1) {
        var temp_caption = "";
        if (caption.toLowerCase() == "account management")
            temp_caption = "Pre Is";
        else if (caption.toLowerCase() == "information services")
            temp_caption = "IS";
        else if (caption.toLowerCase() == "los angeles")
            temp_caption = "la";
        else
            temp_caption = caption;
        if ($('#search' + (($('#search' + temp_caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? temp_caption.toLowerCase().replace(/ /g, "_") : list_div)).val() != "" && $('#search' + (($('#search' + temp_caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? temp_caption.toLowerCase().replace(/ /g, "_") : list_div)).val() != undefined) {
            searchHistory(selector_id, list_div, caption, column);
        }
        else {
            $.each(gDataList, function (facility_key, facitlity_val) {
                if (facitlity_val.list != null && (facitlity_val.list.toLowerCase() == ((temp_caption.toLowerCase() == "los angeles") ? "la" : temp_caption.replace(/_/g, ' ')).toLowerCase())) {
                    page_array = facitlity_val.items;
                }
            });
            makeList(page_array, selector_id, list_div, caption, parseInt(column), true);
        }
    }
    else {
        if ($('#search' + (($('#search' + caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? caption.toLowerCase().replace(/ /g, "_") : list_div)).val() != "" && $('#search' + (($('#search' + caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? caption.toLowerCase().replace(/ /g, "_") : list_div)).val() != undefined) {
            searchHistory(selector_id, list_div, caption, column);
        }
        else {
            $.each(gDataList, function (facility_key, facitlity_val) {
                if (facitlity_val.list != null && (facitlity_val.list.toLowerCase() == ((caption.toLowerCase() == "los angeles") ? "la" : caption.replace(/_/g, ' ')).toLowerCase())) {
                    page_array = facitlity_val.items;
                }
            });
            makeList(page_array, selector_id, list_div, caption, parseInt(column), true);
        }
    }
    if (window.location.href.toLowerCase().indexOf('index_cw') == -1) {
        $("div ul").each(function (i) {
            $(this).listview();
        });
        // if (!jQuery.browser.msie) {
        $("div select").each(function (i) {
            $(this).selectmenu();
        });
        //}
        //$("#containDiv" + caption.toLowerCase().replace(/ /g, "_")).css({ "position": "absolute", "top": "0", "right": "0", "margin": "0", "padding": "0", "width": "120px" });
        $("#containDiv" + caption.toLowerCase().replace(/ /g, "_")).attr({ "data-role": "controlgroup", "data-type": "horizontal", "data-mini": "true" });
    }
}

function createFacility(facility_name, job_list, add_to_column, ddl_change) {
    var search_value = "";
    if ($(job_list).length > 0) {
        var header = $(job_list).splice(0, 1);
        var page_ddl = $(job_list).splice(1, 1);
        //var job_list = [];
        if ($(job_list).find('#search' + facility_name).length > 0)
            job_list = $(job_list).splice(2, $(job_list).length);
        else
            job_list = $(job_list);
        var str = "";
        var job_list = $.each(job_list, function (idx2, val2) {
            str = (val2.outerHTML != undefined) ? val2.outerHTML : '';
        });
        str = '<li  data-role="fieldcontain" data-theme="a" data-inset="false" style="padding:0px;" ><div class="page-select-breakpoint">' + ((page_ddl.length > 0) ? page_ddl[0].outerHTML : '') + '</div></li>' + str;
        facility = '<div data-role="collapsible" data-theme="b" data-content-theme="c" data-collapsed="false">' + ((header.length > 0) ? header[0].outerHTML : '');
        facility += '<ul id="' + facility_name + '" data-role="listview" data-inset="false" data-split-icon="myapp-tag" data-split-theme="d" >' + str + '</ul></div>';
        //facility += '<ul id="' + facility_name + '" data-role="listview" data-inset="false" data-split-icon="myapp-tag" data-split-theme="d" ></ul></div>';
    }

    if ($.find('#' + facility_name).length > 0) {
        search_value = $('#search' + facility_name).val();
        if (isIE10Browser()) { //getting error in IE10 and hence handled
            $('#search' + facility_name).parent().nextAll().remove();
            $(job_list).insertAfter($('#search' + facility_name).parent());
        }
        else {
            $('#search' + facility_name).parent().parent().nextAll().remove();
            $(job_list).insertAfter($('#search' + facility_name).parent().parent());
        }
        //$('#' + facility_name).empty();

        //$('#' + facility_name).append(job_list);
        $('#' + facility_name).each(function (i) {
            $(this).listview();
        });
        $('#' + facility_name).listview('refresh');
        //$('.ui-page').trigger('create');
        $('#search' + facility_name).val(search_value);
    }
    else {
        $("#col" + add_to_column).find('#List' + add_to_column).append(facility);
        $('.ui-page').trigger('create');
        //$("#col" + add_to_column).find('#List' + add_to_column).collapsibleset('refresh');
    }
}

function searchHistory(selector_id, list_div, caption, column) {
    var filterd_list = [];
    var temp_caption = "";
    if (caption.toLowerCase() == "account management")
        temp_caption = "pre Is";
    else if (caption.toLowerCase() == "information services")
        temp_caption = "Is";
    else if (caption.toLowerCase() == "los angeles")
        temp_caption = "la";
    else
        temp_caption = caption;

    var search_value = ($('#search' + temp_caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? $('#search' + temp_caption.toLowerCase().replace(/ /g, "_")).val().replace(/'/g, "`") : $('#search' + list_div).val().replace(/'/g, "`");
    var search_data = jQuery.grep(gDataList, function (a, b) {
        return a.list.toLowerCase().replace('.', '').replace(/ /g, '') === selector_id.replace('.', '').replace(/ /g, '').substring(0, selector_id.replace('.', '').replace(/ /g, '').indexOf('Select')).replace(/_/g, '').toLowerCase();
    });

    if (search_value != "" && search_value != undefined) {
        var searched_li = jQuery.grep(search_data[0].items, function (a, b) {
            //var filter_text = ($(a).attr('data-filtertext') != undefined && $(a).attr('data-filtertext') != "") ? a.substring(a.indexOf('data-filtertext'), a.indexOf('><a') - 1) : "";
            var filter_text = ($(a).attr('data-filtertext') != undefined && $(a).attr('data-filtertext') != "") ? $(a).attr('data-filtertext') : "";
            if (filter_text != "") {
                filter_text = (filter_text.indexOf('=') > -1) ? filter_text.substring(filter_text.indexOf('=') + 2) : filter_text;
                if (window.location.href.toString().indexOf('regional') > -1 && (selector_id == "productionSelect" || selector_id == "preIsSelect" || selector_id == "productionSelect") && (sessionStorage.isEventTriggered != undefined && sessionStorage.isEventTriggered == "yes")) {
                    //$.trim($(a).text().substring(0, $(a).text().indexOf($(a).find('a').attr('id').substring($(a).find('a').attr('id').lastIndexOf('_') + 1))))
                    //if ($.trim($(a).text().substring(0, $(a).text().indexOf($(a).find('a').attr('id').substring($(a).find('a').attr('id').lastIndexOf('_') + 1)))) == search_value) {
                    if ($.trim($(a).text().substring($(a).text().indexOf('|') + 2, $(a).text().length)).replace(/'/g, "`") == search_value) {
                        filterd_list.push(a);
                    }
                }
                else {
                    if (window.location.href.toString().indexOf('regional') > -1) {
                        if (filter_text.toLowerCase().indexOf($('#search' + (($('#search' + temp_caption.replace(/ /g, "")).val() != undefined) ? temp_caption.replace(/ /g, "") : list_div)).val().replace(/'/g, "`").toLowerCase()) > -1) {
                            filterd_list.push(a);
                        }
                    }
                    else {
                        if (filter_text.toLowerCase().indexOf($('#search' + (($('#search' + caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? caption.toLowerCase().replace(/ /g, "_") : list_div)).val().replace(/'/g, "`").toLowerCase()) > -1) {
                            filterd_list.push(a);
                        }
                    }
                }
                return (filter_text.toLowerCase().indexOf($('#search' + (($('#search' + caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? caption.toLowerCase().replace(/ /g, "_") : list_div)).val().replace(/'/g, "`").toLowerCase()) > -1);
            }
        });
        if (selector_id == "productionSelect") {
            filterd_list.sort(function (a, b) {
                return ($(a).text() > $(b).text()) ? 1 : -1;
            });
        }

        makeList(filterd_list, selector_id.replace(/ /g, '').replace('.', ''), list_div, caption, column, false);

    }
    else {
        gDataStartIndex = 0;
        $('#' + selector_id.replace(/ /g, '').replace('.', '')).val(gDataStartIndex);
        $('#' + selector_id.replace(/ /g, '').replace('.', '')).selectmenu('refresh');
        var searched_li = jQuery.grep(search_data[0].items, function (a, b) {
            var filter_text = ($(a).attr('data-filtertext') != undefined && $(a).attr('data-filtertext') != "") ? $(a).attr('data-filtertext') : "";
            if (filter_text != "") {
                filterd_list.push(a);
            }
        });
        makeList(filterd_list, selector_id.replace(/ /g, '').replace('.', ''), list_div, caption, column, true);
        ($('#search' + caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? $('#search' + caption.toLowerCase().replace(/ /g, "_")).blur() : $('#search' + list_div).blur();
    }

    if (sessionStorage.desktop)
        ($('#search' + caption.toLowerCase().replace(/ /g, "_")).val() != undefined) ? $('#search' + caption.toLowerCase().replace(/ /g, "_")).focus() : $('#search' + list_div).focus();
}
