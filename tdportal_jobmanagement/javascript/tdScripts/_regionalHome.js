jQuery.support.cors = true;

//******************** Global Variables Start **************************
//var urlString = unescape(window.location)
//var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
//var params = queryString.split("&");
var gData = [];
var facilityId = getSessionData("facilityId");
var custServiceURL = serviceURLDomain + "api/Customer/" + facilityId;
var productionServiceURL = serviceURLDomain + "api/ProductionStatus/" + facilityId;
var personnelServiceURL = serviceURLDomain + "api/Personnel/" + facilityId;
var gPageSize = 4;
var gDataStartIndex = 0;
var gDataList = [];
var gUserServiceUrl = '../JSON/_personnelUsers.JSON';
var usersJson = [];
var selectedUsers = [];
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_regionalHome', function (event) {
    //buildLists();
    $("#_regionalHome").trigger("create");
    if (sessionStorage.regionalHomePrefs != undefined && sessionStorage.regionalHomePrefs != null && sessionStorage.regionalHomePrefs != "null" && sessionStorage.regionalHomePrefs != "") {
        loadData();
    }
    if (appPrivileges.roleName != "admin") {
        $('#lnkNewJob').css('display', 'none');
        $('#aProfileSettings').css('display', 'none');
        $('#btnSignOut').addClass('ui-corner-all');
    }
});

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_regionalHome').live('pagebeforecreate', function (event) {
    $('#lnkNewJob').attr('href', 'jobSelectTemplate.html');
    getData();
    if (appPrivileges.customerNumber == "1") {
        setLogo(facilityId);
    }
    else
        setLogo(appPrivileges.customerNumber);

    //makeGData();
    //test for mobility...
    loadMobility();
    loadingImg('_regionalHome');
    //if (getSessionData("userRole") == "user" || getSessionData("userRole") == "power") {
    //    $('#viewJobs').html(viewJobs());
    //    $('.ui-page').trigger('create');
    //}
    loadUsersToJson();
});

function makeGData() {
    //if (user_jobs_only == 'myjobs')
    //    productionServiceURL = productionServiceURL + '/' + sessionStorage.username; //userid

    getCORS(productionServiceURL, null, function (prod_data) {
        gData = prod_data;
        //gData.push({
        //    "list": "production",
        //    "items": prod_data
        //});

    }, function (error_response) {
        showErrorResponseText(error_response);
    });

    //if (user_jobs_only == 'myjobs')
    //    custServiceURL = custServiceURL + '/' + sessionStorage.username; //userid
    getCORS(custServiceURL, null, function (cust_data) {

        gData.push({
            "list": cust_data.list,
            "items": cust_data.items
        });
    }, function (error_response) {
        showErrorResponseText(error_response);
    });

    // Seneca
    // 		I updated the rptName you had LettershopProductionReport and Production_Release_Report 
    // the report names was recently changed to Lettershop Production Report and Production Release Report
    // I preferred to update the underscores to Spaces rather than try to insert spaces
    // Bill
    gData.push({
        "list": "reports",
        "items": [
                    { "name": "Lettershop Production", "rptName": "Lettershop_Production_Report", "rptpk": 1 },
                    { "name": "Production Release", "rptName": "Production_Release_Report", "rptpk": 2 }
        ]
    });
    //gData.push({
    //    "list": "data",
    //    "items": []
    //});
    //gData.push({
    //    "list": "accountManagement",
    //    "items": []
    //});
}

//******************** Page Load Events End **************************

//******************** Public Functions Start **************************

function loadUsersToJson() {
    $.getJSON(gUserServiceUrl, function (dataUsers) {
        usersJson = dataUsers;
        fillUsers('ulUserName', 'usersTemplate', usersJson, true);
    });
}

function fillUsers(ctrl, template_to_bind, json_to_bind, is_page_load) {
    $('#' + ctrl).empty();
    if (ctrl.toLowerCase().indexOf('selected') > -1)
        $("#" + template_to_bind).tmpl(json_to_bind).attr('data-icon', 'delete').appendTo("#" + ctrl);
    else {
        $("#" + template_to_bind).tmpl(json_to_bind).appendTo("#" + ctrl);
    }
    if (selectedUsers.length > 0) {
        $.each(selectedUsers, function (a, b) {
            if (ctrl.toLowerCase().indexOf('selected') == -1)
                $('#' + ctrl).find('li[id=li' + b.userName + ']').remove();
        });
        if (ctrl.toLowerCase().indexOf('selected') > -1)
            $('#' + ctrl).prepend('<li data-role="list-divider" >Selected Users</li>');
    }
    window.setTimeout(function getDealy() { $('#' + ctrl).listview('refresh'); }, 100);
}

function addUsers(user_value, user_name, ctrl) {
    $('#li' + user_value).find('a').removeAttr('onclick');
    window.setTimeout(function getDelay() { addSelectedUsers(user_value, user_name, ctrl); }, 100);
}

function addSelectedUsers(user_value, user_name, ctrl) {
    selectedUsers.push({ "fullName": user_name, "userName": user_value });
    selectedUsers = selectedUsers.sort(function (a, b) {
        return a.fullName > b.fullName ? 1 : -1;
    });
    $('#' + ctrl).find('li[id=li' + user_value + ']').remove();
    fillUsers('ulSelectedUsers', 'selectedUsersTemplate', selectedUsers, false);
    if ($('#ulSelectedUsers li:not([id^=li])').length == 0)
        $('#ulSelectedUsers').prepend('<li data-role="list-divider" >Selected Users</li>');
    $('#ulSelectedUsers').listview('refresh');
    if (selectedUsers.length > 0)
        $('#ulSelectedUsers').show();

}

function removeUsers(user_value) {
    window.setTimeout(function getDelay() { removeSelectedUsers(user_value); }, 100);
}

function removeSelectedUsers(user_value) {
    var count = 0;
    $.each(selectedUsers, function (a, b) {
        if (b.userName.toLowerCase() == user_value.toLowerCase())
            return false;
        else
            count++
    });
    selectedUsers.splice(count, 1);
    if (selectedUsers.length == 0)
        $('#ulSelectedUsers').hide();
    fillUsers('ulSelectedUsers', 'selectedUsersTemplate', selectedUsers, false);
    if ($('#ulSelectedUsers li:not([id^=li])').length == 0)
        $('#ulSelectedUsers').prepend('<li data-role="list-divider" >Selected Users</li>');
    $('#ulSelectedUsers').listview('refresh');
    fillUsers('ulUserName', 'usersTemplate', usersJson, false);
}

function makeList(array_name, selector_id, list_div, caption, ddl_change) {
    var tarray = [];
    var header;
    var selector;
    var array = eval(array_name);
    var selector = "";
    var search_ctrl = $('#search' + list_div);
    if (array.length > ((caption == "CUSTOMER") ? 10 : gPageSize)) {
        var page_size = Math.round(array.length / ((caption == "CUSTOMER") ? 10 : gPageSize));
        if (page_size * ((caption == "CUSTOMER") ? 10 : gPageSize) < array.length) {
            page_size++;
        }

        if (gDataStartIndex > array.length) gDataStartIndex = 0;
        var selected_index = $('#' + selector_id).prop("selectedIndex");
        var more_list_id = (selected_index == undefined) ? 0 : selected_index;
        if ((more_list_id + 1) > page_size) {
            more_list_id = selected_index = 0;
        }
        if (search_ctrl.length == 0) {
            var selector = '<div id="containDiv' + list_div + '" style="top: 0; right: 0; margin: 0; padding: 0; width:120px; position: absolute;" data-role="controlgroup" data-type="horizontal" data-mini="true" ><select name="' + selector_id + '" id="' + selector_id + '" data-theme="b" data-overlay-theme="d" data-native-menu="true" data-mini="true" onchange="updateStartIndex(\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\')">';

            for (var i = 0; i < page_size; i++) {
                var is_selected = (i == selected_index) ? "selected" : "";
                selector += '<option ' + is_selected + ' value="' + i + '">Page ' + (1 + i) + '</option>';
            }
            selector += '</select></div>';
        }
        else {
            var page_list = $('#' + selector_id);
            page_list[0].options.length = [];
            var page_options = "";
            for (var i = 0; i < page_size; i++) {
                var is_selected = (i == selected_index) ? "selected" : "";
                page_options += '<option ' + is_selected + ' value="' + i + '">Page ' + (1 + i) + '</option>';
            }
            $('#' + selector_id).html(page_options);
            $('#containDiv' + list_div).show();
        }
    }
    else {
        if (search_ctrl.length == 0) {
            var selector = '<div id="containDiv' + list_div + '" style="position: absolute; top: 0; right: 0; margin: 0; padding: 0; width:120px;" data-role="fieldgroup" data-type="horizontal" data-mini="true"></div>';
        }
        else {
            $('#' + selector_id).val(0);
            $('#' + selector_id).selectmenu('refresh');
            $('#containDiv' + list_div).hide();
        }
    }
    var temp_array = (array.length > gDataStartIndex) ? array.slice(gDataStartIndex, gDataStartIndex + ((caption == "CUSTOMER") ? 10 : gPageSize)) : array;

    if (search_ctrl.length == 0) {
        if (caption == "CUSTOMER" || caption == "INFORMATION SERVICES" || caption == "PRODUCTION" || caption == "ACCOUNT MANAGEMENT") {
            var search_field = '<li data-theme="c"><input data-mini="true" type="search" name="search' + list_div + '" id="search' + list_div + '" value="" onchange="searchHistory(\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\');" onkeyup="$(\'#search' + list_div + '\').trigger(\'onchange\');"/></li>';
            temp_array.splice(0, 0, search_field)
        }

        header = '<li data-role="list-divider"><a target="_self" style="text-decoration:none; color:#fff;">' + caption + '</a>' + selector + '</li>'
        temp_array.splice(0, 0, header);
    }

    if ((more_list_id + 1) < page_size) {
        var more_list = '<li data-theme="c" ><a style="text-decoration:none;" href="#" onclick="getMoreData(\'' + selector_id + '\',\'' + (more_list_id + 1) + '\');">More...</a></li>';
        temp_array.splice(temp_array.length, 0, more_list);
    }
    if (search_ctrl.length > 0) {
        var search_value = $('#search' + list_div).val();
        if (isIE10Browser()) { //getting error in IE10 and hence handled
            $('#search' + list_div).parent().nextAll().remove();
            $(temp_array.join(' ')).insertAfter($('#search' + list_div).parent());
        }
        else {
            $('#search' + list_div).parent().parent().nextAll().remove();
            $(temp_array.join(' ')).insertAfter($('#search' + list_div).parent().parent());
        }
    }
    else {
        $('#' + list_div).empty();
        $('#' + list_div).append(temp_array.join(' '));
    }
    $('#' + list_div).listview('refresh');
    $('.ui-page').trigger('create');
    $('#search' + list_div).val(search_value);
}

function getData() { // This function is called at page body load when the application is opened in IE.  
    if (sessionStorage.regionalHomePrefs == undefined || sessionStorage.regionalHomePrefs == null || sessionStorage.regionalHomePrefs == "")
        if (appPrivileges.customerNumber == REGIS_CUSTOMER_NUMBER)
            getPagePreferences('regionalHome.htm', sessionStorage.facilityId, CW_CUSTOMER_NUMBER);
        else
            getPagePreferences('regionalHome.htm', sessionStorage.facilityId, appPrivileges.customerNumber);
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.regionalHomePrefs));
    }
}

function loadData() {
    pagePrefs = jQuery.parseJSON(sessionStorage.regionalHomePrefs);

    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;

    if (pagePrefs != null) {
        //eval(getURLDecode(pagePrefs.scriptBlockDict.pagebeforecreateimgThumbnail));
        makeGData();
        buildLists();
    }
}

function buildLists() {
    $.each(gData, function (key, val) {
        var job_counter = 0;
        var navlink_counter = '';
        var temp_facility_data = [];
        if (val.list != null) {
            switch (val.list.toLowerCase()) {
                case "customer":
                    $.each(val.items, function (key2, val2) {
                        var filter_text = val2.name.replace(/'/g, "`") + ' ' + val2.jobs;
                        temp_facility_data.push('<li id="' + val2.name.replace(/'/g, "`") + '" data-filtertext="' + filter_text + '"><a href="#" title="' + val2.name.replace(/'/g, "`") + '" onclick=getProductionInfo(\'' + val2.name.replace(/ /g, "_").replace(/'/g, "`") + '\',\'' + val2.custNo + '\');>' + val2.name.replace(/'/g, "`") + '<span class="ui-li-count">' + val2.jobs + '</span></a></li>');
                    });
                    gDataList.push({
                        "list": val.list,
                        "items": temp_facility_data
                    });
                    makeList(temp_facility_data, 'customerSelect', 'customer', 'CUSTOMER', false);
                    break;
                case "reports":
                    $.each(val.items, function (key2, val2) { temp_facility_data.push('<li><a onclick=populateOutputForm(\'' + val2.rptName + '\'); href="#popupReportsParam" data-rel="popup" data-position-to="window"  data-inline="true">' + val2.name + '</a></li>'); });
                    makeList(temp_facility_data, 'reportSelect', 'reports', 'REPORTS', false);
                    break;
                case "is":
                    $.each(val.items, function (key2, val2) {
                        job_counter = job_counter + 1;
                        navlink_counter = job_counter + "_is_" + val2.jobNumber;
                        var new_due_date = "";
                        if (val2.jobDueDate != undefined) {
                            var due_date = new Date(val2.jobDueDate);
                            new_due_date = due_date.getMonth() + "/" + due_date.getDate() + "/" + due_date.getFullYear();
                        }
                        var filter_text = val2.customer.replace(/'/g, "`") + ' ' + val2.jobNumber + ' ' + val2.inHomeDate + ' ' + val2.productionArea + ' ' + val2.quantity + ' ' + new_due_date + ' ' + val2.jobName;
                        //var filter_text = val2.customer + ' ' + val2.jobNumber;

                        var li_item = '<li id="' + val2.customer + '"  data-filtertext="' + filter_text + '"><a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + val2.jobNumber + '\',\'' + "" + '\',\'' + val2.customer.replace(/'/g, "`") + '\',\'' + facilityId + '\',\'' + sessionStorage.jobCustomerNumber + '\')"  title="' + val2.customer.replace(/'/g, "`") + '" ><h3>' + val2.jobName + '</h3>';


                        li_item += (new_due_date != undefined && new_due_date != "") ? '<p class=ui-li-aside>' + new_due_date + '</p>' : '';
                        li_item += '<p><b>' + val2.jobNumber + ' </b> | ' + val2.customer + '</p></a></li>';
                        temp_facility_data.push(li_item);



                        //temp_facility_data.push('<li id="' + val2.customer + '" data-filtertext="' + filter_text + '"><a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + val2.jobNumber + '\',\'' + "" + '\',\'' + val2.customer + '\',\'' + facilityId + '\')"  >' + val2.customer + ' ' + val2.jobNumber + '</a></li>');
                    });
                    gDataList.push({
                        "list": val.list,
                        "items": temp_facility_data
                    });
                    makeList(temp_facility_data, 'isSelect', 'is', 'INFORMATION SERVICES', false);
                    //}
                    break;
                case "production":
                    //var prod_items = jQuery.grep(val.items, function (obj) {
                    //    return obj.items.length > 0 && obj.list != null;
                    //});
                    var count = 0;
                    //if (prod_items.length > 0) {
                    if (val.items.length > 0) {
                        //$.each(prod_items, function (prod_key, prod_val) {
                        //    $.each(prod_items[prod_key].items, function (key2, val2) {
                        //$.each(val.items, function (prod_key, prod_val) {
                        $.each(val.items, function (key2, val2) {
                            job_counter = job_counter + 1;
                            navlink_counter = job_counter + "_prod_" + val2.jobNumber;
                            var new_due_date = "";
                            if (val2.jobDueDate != undefined) {
                                var due_date = new Date(val2.jobDueDate);
                                new_due_date = due_date.getMonth() + "/" + due_date.getDate() + "/" + due_date.getFullYear();
                            }
                            var filter_text = val2.customer.replace(/'/g, "`") + ' ' + val2.jobNumber + ' ' + val2.inHomeDate + ' ' + val2.productionArea + ' ' + val2.quantity + ' ' + new_due_date + ' ' + val2.jobName;
                            var li_item = '<li data-filtertext="' + filter_text + '"><a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + val2.jobNumber + '\',\'' + "" + '\',\'' + val2.customer.replace(/'/g, "`") + '\',\'' + facilityId + '\',\'' + sessionStorage.jobCustomerNumber + '\')"  title="' + val2.customer.replace(/'/g, "`") + '" ><h3>' + val2.jobName + '</h3>';


                            li_item += (new_due_date != undefined && new_due_date != "") ? '<p class=ui-li-aside>' + new_due_date + '</p>' : '';
                            li_item += '<p><b>' + val2.jobNumber + ' </b> | ' + val2.customer + '</p></a></li>';
                            temp_facility_data.push(li_item);

                            //temp_facility_data.push('<li data-filtertext="' + filter_text + '"><a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + val2.jobNumber + '\',\'' + "" + '\',\'' + val2.customer.replace(/'/g, "`") + '\',\'' + facilityId + '\')"  title="' + val2.customer.replace(/'/g, "`") + '" >' + val2.customer.replace(/'/g, "`") + ' ' + val2.jobNumber + '</a></li>');
                        });
                        //});
                    }
                    gDataList.push({
                        "list": val.list,
                        "items": temp_facility_data
                    });
                    makeList(temp_facility_data, 'productionSelect', 'production', 'PRODUCTION', false);
                    break;
                case "pre is":
                    $.each(val.items, function (key2, val2) {
                        job_counter = job_counter + 1;
                        navlink_counter = job_counter + "_preIs_" + val2.jobNumber;
                        var new_due_date = "";
                        if (val2.jobDueDate != undefined) {
                            var due_date = new Date(val2.jobDueDate);
                            new_due_date = due_date.getMonth() + "/" + due_date.getDate() + "/" + due_date.getFullYear();
                        }
                        var filter_text = val2.customer.replace(/'/g, "`") + ' ' + val2.jobNumber + ' ' + val2.inHomeDate + ' ' + val2.productionArea + ' ' + val2.quantity + ' ' + new_due_date + ' ' + val2.jobName;

                        var li_item = '<li data-filtertext="' + filter_text + '"><a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + val2.jobNumber + '\',\'' + "" + '\',\'' + val2.customer.replace(/'/g, "`") + '\',\'' + facilityId + '\',\'' + sessionStorage.jobCustomerNumber + '\')"  title="' + val2.customer.replace(/'/g, "`") + '" ><h3>' + val2.jobName + '</h3>';


                        li_item += (new_due_date != undefined && new_due_date != "") ? '<p class=ui-li-aside>' + new_due_date + '</p>' : '';
                        li_item += '<p><b>' + val2.jobNumber + ' </b> | ' + val2.customer + '</p></a></li>';
                        temp_facility_data.push(li_item);

                        //temp_facility_data.push('<li data-filtertext="' + filter_text + '"><a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + val2.jobNumber + '\',\'' + "" + '\',\'' + val2.customer.replace(/'/g, "`") + '\',\'' + facilityId + '\')"  title="' + val2.customer.replace(/'/g, "`") + '" >' + val2.customer.replace(/'/g, "`") + ' ' + val2.jobNumber + '</a></li>');
                    });
                    gDataList.push({
                        "list": val.list,
                        "items": temp_facility_data
                    });
                    makeList(temp_facility_data, 'preIsSelect', 'preIs', 'ACCOUNT MANAGEMENT', false);
                    break;
            }
        }
    });
    $("div ul").each(function (i) { $(this).listview(); });

    $("div select").each(function (i) {
        $(this).trigger('create');
    });
}

function getProductionInfo(cust_name, cust_no) {
    gDataStartIndex = 0;
    sessionStorage.isEventTriggered = "yes"
    sessionStorage.jobCustomerNumber = cust_no;
    $('#productionSelect').val(0);
    $('#productionSelect').selectmenu('refresh');

    $('#isSelect').val(0);
    $('#isSelect').selectmenu('refresh');

    $('#preIsSelect').val(0);
    $('#preIsSelect').selectmenu('refresh');

    $('.ui-page').trigger('create');

    $('#searchproduction').val(cust_name.replace(/_/g, ' '));
    $('#searchproduction').trigger('onkeyup');

    $('#searchis').val(cust_name.replace(/_/g, ' '));
    $('#searchis').trigger('onkeyup');

    $('#searchpreIs').val(cust_name.replace(/_/g, ' '));
    $('#searchpreIs').trigger('onkeyup');
    if (sessionStorage.isEventTriggered != undefined)
        sessionStorage.removeItem("isEventTriggered");
}

function populateOutputForm(report_name) {
    $("#hdnReportSelected").val(report_name);
}
function updateReportCancel() {
    $("#popupReportsParam").popup("close");
}
function updateReportVal() {
    var output_type = $("#selectOutputType").val();
    var report_name = $("#hdnReportSelected").val();
    $("#popupReportsParam").popup("close");
    var input_report_params = [];
    switch (report_name) {
        case "Lettershop_Production_Report":
        case "Production_Release_Report":
            report_name = report_name.split("_").join(" ")
            var ssrsFolder = "ProductionScheduling";
            input_report_params.push({
                "name": "facilityId",
                "value": facilityId
            });
            break;
    };
    createJSONForReport(report_name, output_type, input_report_params, ssrsFolder, "");
}
//******************** Public Functions End **************************
