﻿jQuery.support.cors = true;

//var myProtocol = "http://";
//var myProtocol = "https://";
//var myProtocol = window.location.protocol + "//";

//******************** Global Variables Start **************************
var urlString = unescape(window.location);
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length);
var demo_job_ticket = {};
var homePage = "";

var returnValue = false;
var date = new Date();
/* create an array of days which need to be disabled */
var disabledDays = ["0"];
var pageHref = window.location.href;

var caseysStaicJobNumber = "";
var skApprovalStaticJobNumber = window.location.href.toLocaleLowerCase().indexOf('tddev') > -1 ? '-17769' : "-17790";

var valid_domain_names = ["aag", "tdlive"];
var current_domain = window.location.href;
current_domain = current_domain.substring(current_domain.indexOf("//") + 2);
current_domain = current_domain.substring(0, current_domain.indexOf("/"));
current_domain = (current_domain.indexOf(":") > -1) ? current_domain.substring(0, current_domain.indexOf(":")) : current_domain;
current_domain = (current_domain.split('.').length <= 3) ? current_domain.substring(0, current_domain.indexOf('.')) : current_domain;
var is_valid_domain = valid_domain_names.indexOf(current_domain) > -1;

var appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var jobCustomerNumber = (sessionStorage.selectedCustomerInfo != undefined && sessionStorage.selectedCustomerInfo != null && sessionStorage.selectedCustomerInfo != "") ?
    ((sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber :
    ((appPrivileges != undefined && appPrivileges != null && appPrivileges != "") ? appPrivileges.customerNumber : '')) :
    ((sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber :
    ((appPrivileges != undefined && appPrivileges != null && appPrivileges != "") ? appPrivileges.customerNumber : ''));

var jobCustomerName = (sessionStorage.selectedCustomerInfo != undefined && sessionStorage.selectedCustomerInfo != null && sessionStorage.selectedCustomerInfo != "") ?
                        $.parseJSON(sessionStorage.selectedCustomerInfo).customerName : ((appPrivileges != undefined && appPrivileges != null) ? appPrivileges.companyName : "");


/*to get application name*/

var myDomain = "";
var serviceURLDomain = "";
var reportsURL = "";
var mapsUrl = "";

var myProtocol = "https://";
if (window.location.host.toString().indexOf('10.180.32.96') > -1)
    myProtocol = "http://";

//var myProtocol = "http://";
if (window.location.host.toString().indexOf('sg-srv-td2') > -1 || window.location.host.toString().indexOf('s-tobor1') > -1 || window.location.host.toString().indexOf('localhost') > -1) {// 
    // you need this to user s-list2008 and access the services directly
    myProtocol = "http://";
    serviceURLDomain = myProtocol + "nexusDev.tribunedirect.com" + "/";
    reportsURL = myProtocol + "reportingStage.tribunedirect.com" + "/";
    mapsUrl = myProtocol + "mapStage.tribunedirect.com" + "/";
}
else {
    // for production the service urls must be like this for url rewriting
    serviceURLDomain = myProtocol + window.location.host + "/";
    reportsURL = myProtocol + window.location.host + "/";
    mapsUrl = myProtocol + window.location.host + "/";
}

var optInImagesPath = "http://images.tribunedirect.com/exposedImages/gizmo/";

var logoPath = myProtocol + window.location.hostname + ':' + window.location.port + '/' + 'KaleidoScopeReSources/';

//var logoPath = 'http://tddev.tribunedirect.com/LogoRepository/';
//var logoPath = 'https://tddev.tribunedirect.com/KaleidoScopeReSources/';  

var rootPath = myProtocol + window.location.host + "/";

//var serviceURLDomain = myProtocol + myDomain;
//var serviceURLDomain = myProtocol + "localhost:59073/";
//var serviceURLDomainInternal = myProtocol + "nexusstageinternal.tribunedirect.com/";
//var serviceURLDomainInternal = myProtocol + "localhost:59073/";
var serviceURLDomainIPInternal = serviceURLDomain;
//var serviceURLDomainIPInternal = myProtocol + "10.180.32.98/";

if (window.location.host.toString().indexOf('10.180.32.96') > -1)
    serviceURLDomainIPInternal = myProtocol + "10.180.32.96/";


var path = window.location.pathname;
var errorMessage = ' <b>One or more web services may temporarily be offline.</b><br/>Please try refreshing your browser window. <br />If this does not help, please contact a system administrator for assistance.<br />';

if (path.indexOf("/") == 0) {
    path = path.substring(1);
}
path = path.split("/", 1);
if (rootPath.toString().indexOf('localhost') > -1) {
    if (path != "") {
        rootPath = rootPath + path + "/";
    }
}
else {
    if (path != "" && path != "pages" && path.toString().indexOf('.htm') > -1) {
        rootPath = rootPath + "/";
    }
}

if (window.location.href.indexOf('login.htm') == -1)// && window.location.href.indexOf('login_new.htm') == -1
    validateLogin();

if (appPrivileges != null && appPrivileges != undefined && appPrivileges.customerNumber != null && appPrivileges.customerNumber != undefined && appPrivileges.customerNumber == "1") {
    window.setTimeout(function () {
        var dv_footer = '';
        dv_footer = '<a data-rel="popup" data-icon="myapp-change" id="btnRFC" onclick="getRFCPopupInfo();">Request For Change</a>';
        // $('#footer').append(dv_footer);
        // $('#footer').addClass('ui-bar');
        // $('#footer').find('p').remove();
        //$('#footer').trigger('create');
        $('#footer').css('height', '58px');
        $('#footer a').css('border', '0px');
        $('#footer a').css('box-shadow', 'none');
        if (window.location.href.toString().toLowerCase().indexOf('jobsummary') > -1)
            $('#footer a').css('margin-top', '0em');
        $('#footer a').css('padding', '0px 12px 0px 12px;');
    }, 500);

}
else {
    //$(document).ready(function () {
    window.setTimeout(function () {
        $('#footer').css('height', '58px');
        $('#footer a').css('border', '0px');
        $('#footer a').css('box-shadow', 'none');

        updateFooterInfo(jobCustomerNumber);

        if (window.location.href.toString().toLowerCase().indexOf('jobsummary') > -1)
            $('#footer a').css('margin-top', '0em');
        $('#footer a').css('padding', '0px 12px 0px 12px');
    }, 500);
    //});
}
//******************** Global Variables End **************************

//******************** Public Functions Start **************************
function updateFooterInfo(customer_number) {
    var footer_src_file = "";
    var footer_src_alt_attr = "";
    var footer_img_width = "";
    var footer_href = "";
    switch (customer_number) {
        case DCA_CUSTOMER_NUMBER:
            footer_src_file = '../images/mdg_logo_footer.gif';
            footer_src_alt_attr = "Dental Care Alliance";
            footer_img_width = "30px";
            footer_href = 'http://www.mdgadvertising.com/';
            break;
        case KUBOTA_CUSTOMER_NUMBER:
            footer_src_file = '../images/gww_footer_logo.png';
            footer_src_alt_attr = "Kubota";
            footer_img_width = "186px";
            footer_href = 'http://www.gwa-inc.com/';
            break;
    }
    if (footer_src_file != "") {
        $('#footer a.ui-btn-right img').attr('src', footer_src_file);
        $('#footer a.ui-btn-right img').attr('alt', footer_src_alt_attr);
        $('#footer a.ui-btn-right img').css('width', footer_img_width);
        $('#footer a.ui-btn-right img').css('height', "30px");
        $('#footer a[data-iconpos="left"]').removeAttr('href');
        $('#footer a.ui-btn-right').attr('href', footer_href);
        $('#footer a.ui-btn-right').css('padding', '0px 12px 0px 12px');
    }
    else {
        $('#customerFooterLogo').attr('src', '../images/CA_footer_logo.png');
        $('#customerFooterLogo').attr('alt', "Conversion Alliance");
    }
}

$(document).ready(function () {
    $("div[data-role=popup]").on("popupbeforeposition", function (event, ui) {
        if (window.location.href.toLowerCase().indexOf('jobapproval') == -1) {
            var ctrl = event.currentTarget || event.targetElement || event.target;
            var popuphiddenCtrl = '<button style="position:fixed; top:10px; left:10px; opacity:0"></button>';

            if ($(ctrl).find('div[data-role=content]').length > 0)
                $(ctrl).find('div[data-role=content]').prepend(popuphiddenCtrl);
            else
                $(ctrl).find('div').eq(0).prepend(popuphiddenCtrl);
        }
    });
});

function loadScript(is_callback) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    if (window.location.href.toLowerCase().indexOf('lma') > 0)
        script.src = myProtocol + 'maps.googleapis.com/maps/api/js?key=AIzaSyCksu8ebvrDG-yOmLhLmpn6TpSZYh0ov94&v=3.exp&libraries=places,geometry,drawing&callback=pageObj.makeGoogleMap';
    else
        script.src = myProtocol + 'maps.googleapis.com/maps/api/js?key=AIzaSyCksu8ebvrDG-yOmLhLmpn6TpSZYh0ov94&v=3.exp&libraries=places,geometry,drawing' + ((is_callback) ? '&' + 'callback=loadPage' : '&' + 'callback=makeGoogleMap');
    script.setAttribute('async', true);
    $(document).ready(function () {
        document.body.appendChild(script);
    });

}


function initialize() {
    var mapOptions = {
        zoom: 6,
        center: new google.maps.LatLng(43.1462421, -104.9929722),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
}


function loadLocationConfig() {
    if ((sessionStorage.locationConfig == undefined || sessionStorage.locationConfig == null || sessionStorage.locationConfig == "") || (appPrivileges.customerNumber != jobCustomerNumber)) {
        var location_config_url = serviceURLDomain + 'api/Locations_config/' + jobCustomerNumber;
        getCORS(location_config_url, null, function (data) {
            sessionStorage.locationConfig = JSON.stringify(data);
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    }
};

function setCustomGizmoLogo(customer_number) {
    var gizmo_img_path = "";
    if (customer_number != undefined && customer_number != "" && (customer_number == KUBOTA_CUSTOMER_NUMBER || customer_number == GWA_CUSTOMER_NUMBER))
        gizmo_img_path = logoPath + "kubota_equipment.png";

    $(document).ready(function () {
        //thumbnail found, show thumbnail
        var gizmo_img_thumbnail = new Image();
        $(gizmo_img_thumbnail)
        .load(function () {
            $(this).hide();
            $('#dvCustomGizmoLogo').empty();
            $('#dvCustomGizmoLogo').append(this);
            $(this).fadeIn(50);
            $(this).css('opacity', '100');
            gizmo_img_thumbnail.alt = "Gizmo";
            gizmo_img_thumbnail.title = "Gizmo";
            //gizmo_img_thumbnail.maxWidth = "275";
            $(gizmo_img_thumbnail).css('max-width', '275px');
            $(gizmo_img_thumbnail).css('width', '96%');
            $(gizmo_img_thumbnail).attr('alt', 'Gizmo');
            $('#dvGizmoLogo').css('display', 'none');
            $('#dvCustomGizmoLogo').css('display', 'block');
            //$(gizmo_img_thumbnail).attr('height', '100%');
        })
        .attr('src', gizmo_img_path);
    });
}

function setGizmoLogo() {
    var gizmo_img_path = "";
    gizmo_img_path = logoPath + "gizmo_logo.png";
    $(document).ready(function () {
        //thumbnail found, show thumbnail
        var gizmo_img_thumbnail = new Image();
        $(gizmo_img_thumbnail)
        .load(function () {
            $(this).hide();
            $('#dvGizmoLogo').empty();
            $('#dvGizmoLogo').append(this);
            $(this).fadeIn(50);
            $(this).css('opacity', '100');
            gizmo_img_thumbnail.alt = "Gizmo";
            gizmo_img_thumbnail.title = "Gizmo";
            //gizmo_img_thumbnail.maxWidth = "275";
            $(gizmo_img_thumbnail).css('max-width', '275px');
            $(gizmo_img_thumbnail).css('width', '96%');
            $(gizmo_img_thumbnail).attr('alt', 'Gizmo');
            //$(gizmo_img_thumbnail).attr('height', '100%');
        })
        .attr('src', gizmo_img_path);
    });
}

function setOptInApprovalLogos(customerNumber) {
    var image_path = '';
    switch (customerNumber) {
        case CASEYS_CUSTOMER_NUMBER:
            image_path = logoPath + "Caseys/" + "caseysgeneralstore_logo2.png";
            break;
        case JETS_CUSTOMER_NUMBER: //Jets
            image_path = logoPath + "jets_logo.png";
            break;
        case AAG_CUSTOMER_NUMBER:
            image_path = logoPath + "aag_logo.png";
            break;
    }

    $(document).ready(function () {
        //thumbnail found, show thumbnail
        var imgThumbnail = new Image();
        $(imgThumbnail)
        .load(function () {
            $(this).hide();
            $('#divCustLogo')
                .empty();
            $('#divCustLogo').append(this);
            $(this).fadeIn(50);
            $(this).css('opacity', '100');
        })
        .attr('src', image_path);
    });
}

//function setLogo(region) {
function setLogo(customerNumber) {
    var image_path = '';
    switch (customerNumber) {
        case "1":
        case SUNTIMES_CUSTOMER_NUMBER:
            image_path = logoPath + "td_chicago_logo.png";
            break;
        case WALGREENS_CUSTOMER_NUMBER: //walgreens
            image_path = logoPath + "wags_landing_logo.png";
            break;
        case "2":
            image_path = logoPath + "td_la_logo.png"
            break;
        case "3":
            image_path = logoPath + "td_ftlauderdale_logo.png";
            break;
        case "4":
            image_path = logoPath + "td_orlando_logo.png";
            break;
        case "5":
            image_path = logoPath + "td_hartford_logo.png";
            break;
        case "6":
            image_path = logoPath + "td_baltimore_logo.png"
            break;
        case "7":
            image_path = logoPath + "td_allentown_logo.png";
            break;
            //        case "1":                                                                 
            //            image_path = logoPath + "td_chicago_logo.png";                                                                 
            //            break;                                                 
        case REGIS_CUSTOMER_NUMBER:
            image_path = logoPath + "Regis/regis_logo.png";
            break;
        case CW_CUSTOMER_NUMBER:
            image_path = logoPath + "campingworld_logo.png";
            break;
        case BRIGHTHOUSE_CUSTOMER_NUMBER: //Bright House
            image_path = logoPath + "brighthouse_logo.png";
            break;
        case DATAWORKS_CUSTOMER_NUMBER:
            image_path = logoPath + "dataworks_logo.png";
            break;
        case CASEYS_CUSTOMER_NUMBER:
            image_path = logoPath + "Caseys/" + "caseysgeneralstore_logo.png";
            break;
        case BBB_CUSTOMER_NUMBER:
            image_path = logoPath + "bobs_bait_and_beer.gif";
            break;
        case SPORTSAUTHORITY_CUSTOMER_NUMBER://Sports Authority  
            image_path = logoPath + "sportsAuthority_logo.png";
            break;
        case AAG_CUSTOMER_NUMBER://AAG
            image_path = logoPath + "aag_logo.png";
            break;
        case TRIBUNEPUBLISHING_CUSTOMER_NUMBER://Tribune Publishing
            image_path = logoPath + "tribunePublishing_logo.png";
            break;
        case JETS_CUSTOMER_NUMBER: //Jets
            image_path = logoPath + "jets_logo.png";
            break;
        case MOTIV8_CUSTOMER_NUMBER: //Jets
            image_path = logoPath + "motiv8_logo.png";
            break;
        case ACQUIREDHEALTH_CUSTOMER_NUMBER: //Acquire Health
            image_path = logoPath + "ah_logo.png";
            break;
        case ALLIED_CUSTOMER_NUMBER: //Allied
            image_path = logoPath + "allied_logo.png";
            break;
        case REDPLUM_CUSTOMER_NUMBER://Red Plum Inside Shopper
            image_path = logoPath + "rp_logo.png";
            break;
        case SAFEWAY_CUSTOMER_NUMBER://Safeway
            image_path = logoPath + "safeway_logo.png";
            break;
        case LOWES_CUSTOMER_NUMBER://Lowes
            image_path = logoPath + "lowes_logo.png";
            break;
        case DCA_CUSTOMER_NUMBER://Dental Care Alliance
            image_path = logoPath + "dentalCareAlliance_logo.png";
            break;
        case KUBOTA_CUSTOMER_NUMBER://Kubota
            image_path = logoPath + "kubota_logo.png";
            break;
        case OH_CUSTOMER_NUMBER://Orlando Health
            image_path = logoPath + "orlandoHealth_logo.png";
            break;
        case SANDIEGO_CUSTOMER_NUMBER://San Diego Union-Tribune
            image_path = logoPath + "sanDiegoUT_logo.png";
            break;
        case SK_CUSTOMER_NUMBER://SportsKing
            image_path = logoPath + "sportsKing_logo.png";
            break;
        case GWA_CUSTOMER_NUMBER://GWA
            image_path = logoPath + "gwa_logo.png";
            break;
    }

    $(document).ready(function () {
        //thumbnail found, show thumbnail
        var imgThumbnail = new Image();
        $(imgThumbnail)
        .load(function () {
            $(this).hide();
            $('#divCustLogo')
                .empty();
            if (customerNumber == REGIS_CUSTOMER_NUMBER) {
                imgThumbnail.src = image_path;
                imgThumbnail.alt = "Regis";
                imgThumbnail.style = "width: 96%; max-width:175px;";
            }
            else if (customerNumber == BBB_CUSTOMER_NUMBER || customerNumber == MOTIV8_CUSTOMER_NUMBER || customerNumber == DCA_CUSTOMER_NUMBER || customerNumber == SK_CUSTOMER_NUMBER) {
                imgThumbnail.src = image_path;
                imgThumbnail.title = (customerNumber == DCA_CUSTOMER_NUMBER) ? "Dental Care Alliance" : ((customerNumber == SK_CUSTOMER_NUMBER) ? "SportsKing" : "Bob's Bait and Beer");
                imgThumbnail.style = "width: 96%; max-width:200px;";
                $(imgThumbnail).css("width", "96%");
                $(imgThumbnail).css("max-width", "300px");
            }
            else if (customerNumber == KUBOTA_CUSTOMER_NUMBER) {
                imgThumbnail.src = image_path;
                imgThumbnail.title = "Kubota";
                imgThumbnail.style = "width: 96%; max-width:200px;";
                $(imgThumbnail).css("width", "96%");
                $(imgThumbnail).css("max-width", "300px");
            }
            else if (customerNumber == GWA_CUSTOMER_NUMBER) {
                imgThumbnail.src = image_path;
                imgThumbnail.title = "GWA";
                //imgThumbnail.style = "width: 96%; max-width:200px;";
                //$(imgThumbnail).css("width", "96%");
                $(imgThumbnail).css("max-width", "300px");
            }
            else if (customerNumber == "1") {
                imgThumbnail.src = image_path;
                $(imgThumbnail).css('max-width', '275px');
                $(imgThumbnail).css('width', '100%');
            }
            //            else if (customerNumber == AAG_CUSTOMER_NUMBER) {
            //                imgThumbnail.src = image_path;
            //                imgThumbnail.title = "Bob's Bait and Beer";
            //                imgThumbnail.style = "width: 96%; max-width:200px;";
            //            }
            $('#divCustLogo').append(this);
            $(this).fadeIn(50);
            $(this).css('opacity', '100');
        })
        .attr('src', image_path);
        var href = '';
        if (window.location.href.toString().indexOf('index') > -1) {
            href = window.location.href;
        }
        else {
            href = "../index.htm"

            if (window.location.href.toString().toLowerCase().indexOf('jobsummary') > -1) {
                $(imgThumbnail).attr('onclick', 'if(pageObj.homeButtonClick("logoClick")) window.location.href="' + href + '"');
            }
            else
                $(imgThumbnail).attr('onclick', 'window.location.href="' + href + '"');

            $(imgThumbnail).css('cursor', 'pointer');
        }
    });
}
//Clicking the enter key to navigate to the next page while editing
$(document).keypress(function (e) {
    if (e.which == 13) {// enter pressed
        if (window.location.href.toString().toLowerCase().indexOf('joblistselection') > -1) {
            //if (!($('#btnRefresh').hasClass('ui-disabled'))) {
                isEnterPress = true;
                $('#btnRefresh').trigger('click');
            //}
        }
        else if (window.location.href.toString().toLowerCase().indexOf('jobselectlocations') > -1 || window.location.href.toString().toLowerCase().indexOf('optinstatus') > -1) {
            pageObj.searchClick();
        }
        else if ($(e.target).attr('id') != 'txtJobComments')
            if (window.location.href.toString().toLowerCase().indexOf('jobproofing') == -1 && (window.location.href.toString().toLowerCase().indexOf('jobapproval') == -1) && (window.location.href.toString().toLowerCase().indexOf('jobcustomtext') == -1) && (window.location.href.toString().toLowerCase().indexOf('joboffersetup') == -1))
                $('#btnContinue').trigger('onclick');
    }
});

function isDataChanged(page_name) {
    var diff_between_json_objects = [];
    diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(gData)), jQuery.parseJSON(getSessionData("gDataCompare")));
    if (diff_between_json_objects.length != 0) {
        $('#pageRedirect').val(page_name);
        $('#popupSignout').popup('open');
    }
    else
        document.location.href = page_name;
}

function getTribuneFacilityId(facilityName) {
    switch (facilityName.toLowerCase()) {
        case "chicago":
        case "northlake":
            return 1;
            break;
        case "los angeles":
            return 2;
            break;
        case "ft lauderdale":
            return 3;
            break;
        case "hartford":
            return 4;
            break;
        case "orlando":
            return 5;
            break;
        case "baltimore":
            return 6;
            break;
        case "allentown":
            return 7;
            break;
    }
}

function getTribuneFacilityName(facilityId) {
    switch (facilityId) {
        case "1":
            return "Chicago";
            break;
        case "2":
            return "Los Angeles";
            break;
        case "3":
            return "Ft. Lauderdale";
            break;
        case "4":
            return "Hartford";
            break;
        case "5":
            return "Orlando";
            break;
        case "6":
            return "Baltimore";
            break;
        case "7":
            return "Allentown";
            break;
    }
}

function callService(method, url, post_json_obj) {
    method_type = method;
    my_json = post_json_obj;
    req_url = url;
    var win = document.getElementById("inlineframe").contentWindow;
    win.postMessage(method_type + "|" + sessionStorage.authString + "|" + url + "|" + my_json, "*");
}

function createIFrame() {
    var iframe = '<iframe name="inlineframe" id="inlineframe" src="' + serviceURLDomain + 'service.html" frameborder="0" scrolling="auto" width="500" height="180" marginwidth="5" marginheight="5" ></iframe>'
    $("body").append(iframe);
    //$(document).ready(function () {
    getData();
    //});
}

function allowSundays(date) {
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    var d_day = date.getDay();
    return [true, ''];
}

/* utility functions */
function nationalDays(date) {
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    var d_day = date.getDay();
    //console.log('Checking (raw): ' + m + '-' + d + '-' + y);
    for (i = 0; i < disabledDays.length; i++) {
        if (disabledDays[i] == d_day) {
            return [false];
        }
    }
    return [true, ''];
}
function noWeekendsOrHolidays(date) {
    var noWeekend = jQuery.datepicker.noWeekends(date);
    return noWeekend[0] ? nationalDays(date) : noWeekend;
}

function displayBlockDays(date, days) {
    var cur_date = new Date();
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    var d_day = date.getDay();

    var tmp_cur_date = cur_date;
    tmp_cur_date.setDate(cur_date.getDate() + days - 1);

    if (date < tmp_cur_date) {
        return [false];
    }
    else
        for (i = 0; i < disabledDays.length; i++) {
            if (disabledDays[i] == d_day) {
                return [false];
            }
        }
    return [true, ''];
}

function displayBlockDaysWithSundays(date, days) {
    var cur_date = new Date();
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    var d_day = date.getDay();

    var tmp_cur_date = cur_date;
    tmp_cur_date.setDate(cur_date.getDate() + days - 1);

    if (date < tmp_cur_date) {
        return [false];
    }
    return [true, ''];
}

function makeBasicAuth(user, password) {
    var tok = user + ':' + password;
    var hash = Base64.encode(tok);
    return "Basic " + hash;
}

function clearAllSession() {
    //var cookies = document.cookie.split(";");
    //for (var i = 0; i < cookies.length; i++) {
    //    $.removeCookie(cookies(i));
    //}
    sessionStorage.clear();
}

function redirectToIndexPage() {
    $('#btnLeaveThisScreen').attr('onclick', 'leavePageWithoutSave();');
    if (!homeClickValidation())
        return false;
    redirectToHome();
}

function redirectToSingleJobPage() {
    var on_click = "removeJobSessions('single');"
    //on_click += "window.location.href='" + ((sessionStorage.jobNumber != -1) ? "../pages/singleJob.htm" : "../index.htm") + "'";
    on_click += "window.location.href='" + ((sessionStorage.jobNumber != -1) ? "../jobSummary/jobSummary.htm" : "../index.htm") + "'";
    $('#btnLeaveThisScreen').attr('onclick', on_click);
    if (!homeClickValidation())
        return false;
    removeJobSessions('single');
    window.location.href = ((sessionStorage.jobNumber != -1) ? "../jobSummary/jobSummary.htm" : "../index.htm");
}

function callJobSubmit() {
    document.location.href = 'jobSetupSubmitJob.html';
}

function updateStorePaymentInfoInSession() {
    var selected_locations_list = [];
    var store_payment_info_list = [];
    var updated_store_payment_info_list = [];

    var output_json = jQuery.parseJSON(sessionStorage.jobSetupOutput);
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != "" && sessionStorage.jobSetupOutput != null) {
        if (output_json.selectedLocationsAction != undefined && output_json.selectedLocationsAction !== null) {
            if (output_json.selectedLocationsAction.selectedLocationsList != undefined && output_json.selectedLocationsAction.selectedLocationsList != null) {
                selected_locations_list = output_json.selectedLocationsAction.selectedLocationsList;
            }
        }

        if (output_json.checkOutAction != undefined && output_json.checkOutAction != null) {
            if (output_json.checkOutAction.storePaymentInfoList != undefined && output_json.checkOutAction.storePaymentInfoList != null) {
                store_payment_info_list = output_json.checkOutAction.storePaymentInfoList;
            }
        }
    }
    if (selected_locations_list != undefined && selected_locations_list != null && selected_locations_list.length > 0) {
        if (store_payment_info_list != undefined && store_payment_info_list != null && store_payment_info_list.length > 0) {
            $.each(selected_locations_list, function (a, b) {
                var temp_store_payment_info = {};
                var store_payment_info = $.grep(store_payment_info_list, function (obj) {
                    return (obj.storeId === b.storeId);
                });

                if (store_payment_info.length > 0) {
                    store_payment_info = store_payment_info[0];
                    $.each(store_payment_info, function (key, val) {
                        temp_store_payment_info[key] = store_payment_info[key];
                    });
                }
                else {
                    $.each(store_payment_info_list[0], function (key, val) {
                        temp_store_payment_info[key] = (b[key] != undefined) ? b[key] : "0";
                    });
                }
                updated_store_payment_info_list.push(temp_store_payment_info);
            });

            if (output_json.checkOutAction != undefined && output_json.checkOutAction != null) {
                if (output_json.checkOutAction.storePaymentInfoList != undefined && output_json.checkOutAction.storePaymentInfoList != null) {
                    output_json.checkOutAction.storePaymentInfoList = updated_store_payment_info_list;
                }
            }
            sessionStorage.jobSetupOutput = JSON.stringify(output_json);
        }
    }
}

function postJobSetUpData(ticket_action) {
    $('#btnSaveTop').addClass("ui-disabled");
    $('#btnSave').addClass("ui-disabled");

    var job_setup_json = jQuery.parseJSON(sessionStorage.jobSetupOutput);
    var jobNumber = getSessionData("jobNumber");

    if ((jobCustomerNumber === CW_CUSTOMER_NUMBER || jobCustomerNumber === BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber === BBB_CUSTOMER_NUMBER || jobCustomerNumber === AAG_CUSTOMER_NUMBER || jobCustomerNumber === TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber === SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber === ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber === SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber === WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber === DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber === SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber === LOWES_CUSTOMER_NUMBER) && jobNumber != "-1") //Biz rules for needsBillingSave flag
        job_setup_json = fnNeedsBillingSave(job_setup_json);

    updateStorePaymentInfoInSession();
    var order_locations = job_setup_json.selectedLocationsAction.selectedLocationsList;
    if (sessionStorage.updateLocationsList != undefined && sessionStorage.updateLocationsList != null && sessionStorage.updateLocationsList !== "") {
        job_setup_json.selectedLocationsAction.selectedLocationsList = $.parseJSON(sessionStorage.updateLocationsList);
    }
    else {
        job_setup_json.selectedLocationsAction.selectedLocationsList = [];
    }

    var post_job_url = serviceURLDomain + 'api/JobTicket/' + sessionStorage.facilityId + '/' + sessionStorage.jobNumber + '/' + sessionStorage.username + '/' + ticket_action + '/ticket';
    postCORS(post_job_url, JSON.stringify(job_setup_json), function (response) {
        $('#btnSaveTop').removeClass("ui-disabled");
        $('#btnSave').removeClass("ui-disabled");
        var msg = (sessionStorage.jobNumber == "-1") ? 'Your job was saved.' : 'Your job was updated.';
        if (response != "") {
            if (!isNaN(parseInt(response.replace(/"/g, "")))) {
                jobNumber = parseInt(response.replace(/"/g, ""));
                sessionStorage.jobNumber = jobNumber;
                job_setup_json.jobNumber = jobNumber;
                if (typeof pageObj != 'undefined' && pageObj.gOutputData != undefined && pageObj.gOutputData != null && pageObj.gOutputData != "")
                    pageObj.gOutputData.jobNumber = jobNumber;
                else if (typeof gOutputData != 'undefined' && gOutputData != null && gOutputData != "")
                    gOutputData.jobNumber = jobNumber;

                sessionStorage.jobDesc = (job_setup_json.jobName != undefined) ? job_setup_json.jobName : "";
                $('#liJobSummary').css('display', "block");
                displayJobInfo();
                job_setup_json["needsPostageReportUpdate"] = false; //if quantity, template and/or mail class have changed, we need make this attribue to true
                if ((jobCustomerNumber === CW_CUSTOMER_NUMBER || jobCustomerNumber === BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber === BBB_CUSTOMER_NUMBER || jobCustomerNumber === AAG_CUSTOMER_NUMBER || jobCustomerNumber === TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber === SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber === ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber === SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber === WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber === DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber === SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber === LOWES_CUSTOMER_NUMBER) && job_setup_json.needsBillingSave != undefined)
                    job_setup_json.needsBillingSave = false;

                job_setup_json.selectedLocationsAction.selectedLocationsList = order_locations;
                //getCORS(serviceURLDomain + "api/Locations_dropDown/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + jobNumber + "/0/2/" + response.jobTypeId, null, function (data) {
                //    if (data.locationsSelected != undefined && data.locationsSelected != null && data.locationsSelected != "")
                //        job_setup_json.selectedLocationsAction.selectedLocationsList = data.locationsSelected;
                //}, function (error_response) {
                //    showErrorResponseText(error_response, true);
                //    createEditJobTicketSession(g_output_data)
                //});
                sessionStorage.jobSetupOutput = JSON.stringify(job_setup_json);
                sessionStorage.jobSetupOutputCompare = JSON.stringify(job_setup_json);
                sessionStorage.removeItem("updateLocationsList");
                sessionStorage.removeItem('tempSelectedStores');
            }
            else {
                msg = response;
            }
        }
        else
            msg = "Your job was not saved.";

        if (window.location.href.toString().toLowerCase().indexOf('jobproofing') == -1 && window.location.href.toString().toLowerCase().indexOf('joblistmanagement') == -1 && window.location.href.toString().toLowerCase().indexOf('jobuploadartwork') == -1 && window.location.href.toString().toLowerCase().indexOf('jobuploadlist') == -1) {
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
            $('#okBut').unbind('click');
        }
        else if ((window.location.href.toString().toLowerCase().indexOf('jobuploadartwork') > -1 || window.location.href.toString().toLowerCase().indexOf('jobuploadlist') > -1) && sessionStorage.saveClick) {
            removeSessionData("saveClick");
            $('#alertmsg').text(msg);
            $('#okBut').unbind('click');
            $('#popupDialog').popup('open');
        }
    }, function (response_error) {
        //var msg = 'Job creation failed.';
        if (response_error.status == 409) {
            msg = "Your job <b>" + job_setup_json.jobName + "</b> could not be saved as there is already a job in the system with that name. Please rename your job and try again.";
            $('#alertmsg').html(msg);
            $('#okBut').unbind('click');
        }
        else
            if (window.location.href.toString().toLowerCase().indexOf('joblistmanagement') == -1 && window.location.href.toString().toLowerCase().indexOf('jobuploadlist') == -1)
                //        var error_msg = (response_error.responseText != '' && response_error.responseText != undefined) ? response_error.responseText.replace(/\{/g, '').replace(/\}/g, '') : 'Unknown Error: Contact a Site Administrator'
                //$('#alertmsg').text(error_msg);
                //$('#popupDialog').popup('open');
                showErrorResponseText(response_error, false);
    });

}

function fnNeedsBillingSave(job_setup_json_new) {
    var job_setup_json_old = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);

    var temp_new = [];
    temp_new = fnGetNeedsBillingSaveJSON(temp_new, job_setup_json_new);

    var temp_old = [];
    if (job_setup_json_old != undefined && job_setup_json_old != null)
        temp_old = fnGetNeedsBillingSaveJSON(temp_old, job_setup_json_old);

    if (temp_old.length > 0 && temp_new.length > 0) {
        var diff = [];
        diff = DiffObjects(temp_new, temp_old);
        if (diff.length > 0)
            job_setup_json_new["needsBillingSave"] = true;
        else
            job_setup_json_new["needsBillingSave"] = false;
    }
    return job_setup_json_new;
}

function fnGetNeedsBillingSaveJSON(obj, job_ticket_json) {
    obj.push({ "templateName": job_ticket_json.templateName });
    obj.push({ "mailstreamAction": job_ticket_json.mailstreamAction });
    obj.push({ "selectedLocationsAction": job_ticket_json.selectedLocationsAction });
    return obj;
}

function fnNewAnalysis() {
    clearSessionData();
    saveSessionData("isNewAnalysis", "true")
    removeSessionData("LoadAdvertisers");
    if (pageHref.indexOf("pages") > -1) {
        window.location = "jobAnalysisSetup.html";
    } else {
        window.location = "pages/jobAnalysisSetup.html";
    }
}

function fnNewJob(adv) {
    var demo_hints_json = (sessionStorage.demoHintsJson != undefined && sessionStorage.demoHintsJson != null && sessionStorage.demoHintsJson != "") ? sessionStorage.demoHintsJson : "";
    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
        sessionStorage.isDemoSelected = 1;
        sessionStorage.showDemoHints = "on";
    }
    if (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0) {
        //if (!(sessionStorage.username.toLowerCase().indexOf('demo') > -1)) {
        sessionStorage.removeItem('jobCustomerNumber');
    }
    else {
        demoNavlinks((sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : jobCustomerNumber);
    }
    if (pageHref.indexOf("pages") > -1) {
        if (pageHref.toLowerCase().indexOf('singlejob') > -1) {
            if (homeButtonClick('new'))
                window.location = "../jobSelectTemplate/jobSelectTemplate.html";
            //window.location = "../pages/jobSelectTemplate.html";
            else
                return false;
        }
        else
            if (appPrivileges.customerNumber == SUNTIMES_CUSTOMER_NUMBER)
                window.location = "jobAdOrder/jobAdOrder.html";
            else
                window.location = "../jobSelectTemplate/jobSelectTemplate.html";
    } else {
        if (appPrivileges.customerNumber == SUNTIMES_CUSTOMER_NUMBER)
            window.location = "jobAdOrder/jobAdOrder.html";
        else
            window.location = "jobSelectTemplate/jobSelectTemplate.html";
    }
    fnManageSessionsForNewJobs();
    if (sessionStorage.demoHintsJson == undefined || sessionStorage.demoHintsJson == null || sessionStorage.demoHintsJson == "") sessionStorage.demoHintsJson = demo_hints_json;
}

function fnMaterialsAdmin() {
    //    var custNavLinksInfo = (sessionStorage.custNavLinks != undefined && sessionStorage.custNavLinks != null && sessionStorage.custNavLinks != "") ? jQuery.parseJSON(sessionStorage.custNavLinks) : appPrivileges;
    //    custNavLinksInfo = (custNavLinksInfo == undefined || custNavLinksInfo == null || Object.keys(custNavLinksInfo).length == 0) ? appPrivileges : custNavLinksInfo;
    //    var temp_links = {};
    //    $.each(custNavLinksInfo, function (key, val) {
    //        if (key == "display_page_materialsReceiving" || key == "display_page_adOrder") {
    //            temp_links[key] = val;
    //        }
    //    });

    //    custNavLinksInfo = temp_links;
    //    sessionStorage.custNavLinks = JSON.stringify(custNavLinksInfo);
    // window.location = "jobMaterialsReceiving/jobMaterialsReceiving.html";
    window.location = "jobAdOrder/jobAdOrder.html";
}

function fnManageSessionsForNewJobs() {
    var facility_id = sessionStorage.facilityId;

    clearSessionData();   //call this fucntion to clear the existing "job" related data in the job setup process.
    saveSessionData("jobNumber", "-1"); //jobnumber session for "Add New Job" functionality. jobNumber will be "-1" for add new.
    //    if (sessionStorage.customerNumber != "1")
    //        saveSessionData("facilityId", "1");
    //    else
    saveSessionData("facilityId", facility_id);
}

//When user clicks on "Load Circulation" and "Load Advertisers" buttons..
function fnNavListMgmt(button_name) {
    if (button_name == "loadCirc") {
        if (sessionStorage.LoadAdvertisers != null) {
            sessionStorage.removeItem("filesToUpload");
        }
        saveSessionData("LoadCirculation", button_name);
        sessionStorage.removeItem("LoadAdvertisers");
    }
    else {
        if (sessionStorage.LoadCirculation != null) {
            sessionStorage.removeItem("filesToUpload");
        }
        saveSessionData("LoadAdvertisers", button_name);
        sessionStorage.removeItem("LoadCirculation");
    }
    if (pageHref.indexOf("pages") > -1) {
        window.location = "jobListManagement.html";
    } else {
        window.location = "pages/jobListManagement.html";
    }
    clearSessionData();
}
function requestChangePopup() {
    var request_change = '';
    request_change = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupRFC" data-theme="c" class="ui-corner-all" data-history="false" data-tolerance="30, 15, 30, 15"><div data-role="content"><h3>Request For Change</h3>';
    request_change += '<label for="text-basic">Change Owner:</label><input type="text" name="text-basic" id="text-basic" value=""><label for="text-basic">Initiator:</label><input  type="text" name="text-basic" id="txtInitiator" value="">';
    request_change += '<div style="padding-top:12px;"><label for="text-basic">Title:</label><input type="text" name="text-basic" id="text2" value="" data-mini="true"></div>';
    request_change += '<div style="padding-top:12px;"><label for="textarea">Change Description:<br /><span style="font-size:12px;">(Reason for change, costs, benefits, consequences if not implemented, references to issues)</span></label>';
    request_change += '<textarea cols="40" rows="8" name="textarea" id="textarea" data-mini="true"></textarea></div><div data-role="fieldcontain" ><label for="fieldName" class="select">Category:</label>';
    request_change += '<select name="fieldName" id="fieldName" data-mini="true" data-theme="a"><option value="">Select...</option><option value="9">Accounting</option><option value="8">Approvals</option><option value="7">Checkout</option>';
    request_change += '<option value="3">Data</option><option value="4">Job Ordering</option><option value="5">Job Processing</option><option value="6">List Selection (Mapping)</option><option value="10">Reporting</option>';
    request_change += '<option value="1">Scheduling</option><option value="11">Templates</option><option value="2">Uploads</option></select></div>';
    request_change += '<div ><fieldset data-role="controlgroup"  data-mini="true"><legend>Urgency:</legend><input type="radio" name="radio-choice-1" id="radio-choice-1" value="choice-1" data-theme="a"><label for="radio-choice-1">Very High (Emergency Change)</label>';
    request_change += '<input type="radio" name="radio-choice-1" id="radio-choice-2" value="choice-2" data-theme="a"><label for="radio-choice-2">High</label><input type="radio" name="radio-choice-1" id="radio-choice-3" value="choice-3" data-theme="a">';
    request_change += '<label for="radio-choice-3">Normal</label><input type="radio" name="radio-choice-1" id="radio-choice-4" value="choice-4" data-theme="a"><label for="radio-choice-4">Low</label></fieldset></div>';
    request_change += '<div style="padding-top:12px;"><label>Attach Files:</label><br /><a href="#" data-role="button" data-inline="true" data-mini="true" data-theme="a">Choose File</a> No Files Selected</div>';
    request_change += '<div><p align="right"><a href="#" data-role="button" data-inline="true" data-theme="a" data-mini="true" onclick="$(\'#popupRFC\').popup(\'close\');">Cancel</a><a href="#" data-role="button" data-inline="true" data-theme="a" data-mini="true" onclick="$(\'#popupRFC\').popup(\'close\');">Submit Request</a></p></div></div></div>';
    $("#footer").prepend(request_change);
}

function getRFCPopupInfo() {
    $('#txtInitiator').val(sessionStorage.username);
    $('#txtInitiator').addClass('ui-disabled');
    $("#txtInitiator").attr('disabled', true);
    $('#txtInitiator').css('opacity', '0.7');
    $("#popupRFC").popup("open");
}

function displayMessage(page_name) {
    showHomeButtonConfirmation(page_name); //for home button validation
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupDialog" data-overlay-theme="d" data-theme="c" style="text-align:left;max-width:500px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1 id="alertHeader">Alert</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="alertmsg"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="okBut" onclick="$(\'#popupDialog\').popup(\'close\');">Ok</a>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

function confirmMessage() {
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupConfirmDialog" data-overlay-theme="d" data-theme="c" style="text-align:left;max-width:400px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<div id="colId"></div>';
    msg_box += '<div id="valList"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelBut" data-mini="true">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" onclick="fnConfirmRemoveRow()" id="okBut" data-mini="true">Ok</a>';
    msg_box += '</div></div>';
    $("#_jobFileMapper").append(msg_box);
}

function createConfirmMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupConfirmDialog" data-overlay-theme="d" data-theme="c" style="text-align:left;max-width:400px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<div id="colId"></div>';
    msg_box += '<div id="valList"></div>';
    msg_box += '<div align="right">';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelButConfirm" data-mini="true" onclick="$(\'#popupConfirmDialog\').popup(\'open\');$(\'#popupConfirmDialog\').popup(\'close\');">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="okButConfirm" data-mini="true">Ok</a>';
    msg_box += '</div>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}


/* 
To store the sessionStorage data, pass the key and value
for e.g saveSessionData("jobId","12234")
*/
function saveSessionData(key, value) {
    sessionStorage[key] = value;
}
/* 
To retreive the sessionStorage data for the given key. 
Pass the key and will return its value from sessionStorage object
for e.g getSessionData("jobId") returns "12234"
*/
function getSessionData(key) {
    return sessionStorage[key];
}
/* 
To clear the session values that are related to "Job" process. This includes Job Number, Jod Description and all the Job setup data. 
This method is called when user clicks on "Add New Job" button to start a new job.
*/
function clearSessionData() {
    sessionStorage.removeItem("jobNumber");
    sessionStorage.removeItem("negJobNumber");
    sessionStorage.removeItem("jobDesc");
    sessionStorage.removeItem("jobSetupOutput");
    sessionStorage.removeItem("isNewAnalysis");
    sessionStorage.removeItem("jobSetupMileStoneOutput");
    sessionStorage.removeItem("templates");
    sessionStorage.removeItem("storeListSelection");
    sessionStorage.removeItem("savedStores");
}
function removeSessionData(sessionName) {
    sessionStorage.removeItem(sessionName);
}
/* 
To store the job info (jobnumber,jod desc) when clicked on a particular jobitem.
Then redirect the page based on the user company to show the singlejob page.
this funtion is called in _index.js,_indexcamping.js,_regionhome.js
*/

function storeJobInfo(counter, job_number, neg_job_number, job_desc, facility_id, job_customer_number, is_gizmo_job) {
    sessionStorage.removeItem("jobSetupOutput");
    sessionStorage.removeItem("jobSetupMileStoneOutput");
    sessionStorage.removeItem("jobNumber");
    sessionStorage.removeItem("negJobNumber");
    saveSessionData("facilityId", facility_id);
    var customer_number = appPrivileges.customerNumber;
    sessionStorage.jobCustomerNumber = job_customer_number;
    sessionStorage.jobNumber = job_number;

    sessionStorage.isGizmoJob = is_gizmo_job;

    if (neg_job_number != "" && (job_number != neg_job_number)) sessionStorage.negJobNumber = neg_job_number;
    sessionStorage.jobDesc = job_desc;
    $('#navLink' + counter).attr('target', '_self');

    //if (job_customer_number == "99997")
    //    getDemoEditJobData();
//else
    if (pageHref.indexOf("pages") > -1) {
        $('#navLink' + counter).attr('href', 'jobSummary/jobSummary.htm');
    } else {
        window.location.href = 'jobSummary/jobSummary.htm';
    }
}
function acceptNumerics(eventObj) {
    var keycode;
    if (eventObj.keyCode) //For IE
        keycode = eventObj.keyCode;
    else if (eventObj.Which)
        keycode = eventObj.Which; // For FireFox
    else
        keycode = eventObj.charCode; // Other Browser
    if (event.keyCode < 44 || event.keyCode > 57 || event.keyCode == 45 || event.keyCode == 47) event.returnValue = false;
}

function ValidateCompanyPhoneFax(args) {
    args = args.replace(/\(/g, '');
    args = args.replace(/\)/g, '');
    args = args.replace(/\-/g, '');
    args = args.replace(/\s+/g, '')

    var val = args;
    var regEx = /^[0-9]+$/;
    if (!regEx.test(val)) {
        return false;
    }
    else {
        if (val > 0) {
            if (val.length != 10)
                return false;
            else
                return true;
        }
        else
            return false;
    }
}

function textBoxOnBlur(elementRef) {
    //5754578787, it should be 575-457-8787
    var elementValue = elementRef.value;
    if (elementValue != '') {
        // Remove all "(", ")", "-", and spaces...
        elementValue = elementValue.replace(/\(/g, '');
        elementValue = elementValue.replace(/\)/g, '');
        elementValue = elementValue.replace(/\-/g, '');
        elementValue = elementValue.replace(/\s+/g, '')

        if (elementValue.length == 10) {
            elementRef.value = (elementValue.substr(0, 3) + '-' + elementValue.substr(3, 3) + '-' + elementValue.substr(6, 4));
            return;
        }
    }
}
function dateInUTCFormat(date) {
    var currDate = new Date(date);
    currDate.setMinutes(currDate.getMinutes() - currDate.getTimezoneOffset());
    return currDate;
}

function daysBetween(currentDate, jobDate) {
    var millisecondsPerDay = 24 * 60 * 60 * 1000;
    return (dateInUTCFormat(jobDate) - dateInUTCFormat(currentDate)) / millisecondsPerDay;
}

function getFileTypeCode(file_type) {
    if (file_type != null && file_type != "") {
        switch (file_type.toLowerCase()) {
            case "source":
                return "1";
                break;
            case "donotmail":
                return "2";
                break;
            case "seed":
                return "6";
                break;
            case "artwork":
                return "25";
                break;

        }
    }
}
function getFileType(file_type_code) {
    switch (file_type_code) {
        case "1":
        case 1:
            return "source";
            break;
        case "2":
        case 2:
            return "donotmail";
            break;
        case "6":
        case 6:
            return "seed";
            break;
        case "25":
        case 25:
            return "artwork";
            break;
    }
}

function getDisplayFileType(file_type) {
    switch (file_type) {
        case "source":
            return "Source";
            break;
        case "donotmail":
            return "Do Not Mail";
            break;
        case "seed":
            return "Seed";
            break;
        case "artwork":
            return "Artwork";
            break;
    }
}

function getURLEncode(str) {
    return encodeURIComponent(str).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
}

function getURLDecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

function getFile() {
    var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'); // document.getElementById('spnFileInput');
    var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
    var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
    $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1].click();
    //    if ($.browser.msie)
    //        window.frames[0].document.getElementById('mailFiles[]').click();
    //    else
    //        document.getElementById("mailFiles[]").click();
}
/*to restrict user to single session */
function validateLogin() {
    var job_proofing = "jobproofing.html";
    var job_approval = "jobapproval.html";
    var lo_optin = "loanofficersoptin.html"
    var post_to_production = "posttoproduction.html";
    if ((window.location.href.toLowerCase().indexOf(job_proofing) > -1) || (window.location.href.toLowerCase().indexOf(job_approval) > -1)) {
        //sessionStorage.userRole = "admin";
        //sessionStorage.companyName = "Camping World";
        //sessionStorage.customerNumber = CW_CUSTOMER_NUMBER;
        // no need to redirect to login page,as these pages will be available to district users. do nothing...
        return true;
    }
    else if (window.location.href.toLowerCase().indexOf(post_to_production) > -1) {
        return false;
    }
    if (sessionStorage.userRole == null && sessionStorage.companyName == null) {
        var dist_opt = "optinout.html";
        var dist_app = "optinapproval.html";
        if ((window.location.href.toLowerCase().indexOf(dist_opt) > -1) || (window.location.href.toLowerCase().indexOf(dist_app) > -1) || (window.location.href.toLowerCase().indexOf(lo_optin) > -1)) {
            // no need to redirect to login page,as these pages will be available to district users. do nothing...
			return true;
        }
        else {
            if (queryString != undefined && queryString != null && queryString != "" && queryString.toLowerCase().indexOf('s1=') > -1) {
                sessionStorage.isEmailLogin = true;
                getEmailUserAuthorization();
            }
            else {
                if (window.location.href.indexOf('login.htm') == -1 && window.location.href.indexOf('pages') > -1) //(window.location.href.indexOf('login_new.htm') == -1 || 
                    window.location.href = rootPath + 'login.htm';
                else if (window.location.href.indexOf('login.htm') == -1)
                    window.location.href = rootPath + 'login.htm';
            }
        }
    }
}

function decodeBase64(input) {
    // private property
    var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    // public method for decoding
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    while (i < input.length) {

        enc1 = _keyStr.indexOf(input.charAt(i++));
        enc2 = _keyStr.indexOf(input.charAt(i++));
        enc3 = _keyStr.indexOf(input.charAt(i++));
        enc4 = _keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

    }
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while (i < output.length) {

        c = output.charCodeAt(i);

        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if ((c > 191) && (c < 224)) {
            c2 = output.charCodeAt(i + 1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = output.charCodeAt(i + 1);
            c3 = output.charCodeAt(i + 2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }

    return string;
    //output = Base64._utf8_decode(output);

    //return output;
}

function CheckKeyCode(e) {
    if (window.event) { var charCode = window.event.keyCode; }
    else if (e) { var charCode = e.which; }
    else { return true; }

    if (charCode == 32) { return true; }
    //if (charCode == 46) { return true; } //decimal not allowed
    if (charCode > 31 && (charCode < 48 || charCode > 57)) { return false; }
    return true;
    //str = jQuery.trim(str);
}

function NumericValidation(event) {
    var keychar = event.keyCode;
    if (keychar > 47 && (keychar < 58)) { return true; }
    return false;
}

function pieceAttributesKeyValidation(e) {
    var browserName = navigator.appName;
    var keychar;

    if (browserName == "Microsoft Internet Explorer") {
        keychar = e.keyCode;
        if (keychar > 45 && (keychar < 58)) { return true; }
        return false;
    }
    else {
        if (window.event) { keychar = window.event.keyCode; }
        else if (e) { keychar = e.which; }
        else { return true; }

        if (keychar == 32) { return true; }
        if (keychar == 46) { if ($(e.target).val().indexOf('.') > -1) return false; else return true; }
        if (keychar > 31 && (keychar < 48 || keychar > 57)) { return false; }
        return true;
    }
}

function browserName(e) {
    var browserName = navigator.appName;
    if (browserName == "Netscape") {
        return CheckKeyCode(e);
    }
    else {
        if (browserName == "Microsoft Internet Explorer") {
            return NumericValidation(e);
        }
        else {
            return CheckKeyCode(e);
        }
    }
}

function setDigits(control) {
    var n = $('#' + control).val().replace(/,/g, "").replace(/ /g, "");
    $('#' + control).val(n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}

function setDigitsForGrid(control_val) {
    var precision_part = (control_val.toString().indexOf('.') != -1) ? control_val.substring(0, control_val.indexOf('.')) : control_val;
    var decimal_part = (control_val.toString().indexOf('.') != -1) ? control_val.substring(control_val.indexOf('.') + 1) : "";
    var precision_with_comma = precision_part.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/ /g, "");
    return (decimal_part != "") ? (precision_with_comma + '.' + decimal_part) : precision_with_comma;
}
//******************** Public Functions End **************************

function getUploadedFilesCount() {
    //if ((appPrivileges.customerNumber == CW_CUSTOMER_NUMBER) && (appPrivileges.roleName != "admin")) $("#uploadedListCount").hide();

    var locations_list = (gOutputData != undefined && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined) ? gOutputData.selectedLocationsAction.selectedLocationsList : [];

    $("#locationsCount").text(locations_list.length);

    var selected_link = getSessionData('advertiser_session');
    if (selected_link == "clickedAdv")
        file_data = (gOutputData != undefined && gOutputData.advertiserFileList != undefined) ? gOutputData.advertiserFileList : [];
    else if (selected_link == "clickedProj")
        file_data = (gOutputData != undefined && gOutputData.projectionFileList != undefined) ? gOutputData.projectionFileList : [];
    else if (selected_link == "clickedStore")
        file_data = (gOutputData != undefined && gOutputData.storeFileList != undefined) ? gOutputData.storeFileList : [];
    else
        file_data = (gOutputData != undefined && gOutputData.standardizeAction.standardizeFileList != undefined) ? gOutputData.standardizeAction.standardizeFileList : [];

    var art_file_data = (gOutputData != undefined && gOutputData.artworkAction != null && gOutputData.artworkAction.artworkFileList != undefined) ? gOutputData.artworkAction.artworkFileList : [];

    var list_file_cnt = 0;
    $.each(file_data, function (key, val) {
        if (val.fileName != "") {
            var file_source_type = getFileType(val.sourceType);
            if (appPrivileges.roleName != "admin" && file_source_type.toLowerCase() == "source" && val.fileName != "") {
                list_file_cnt++;
            }
            else if (appPrivileges.roleName == "admin" && val.fileName != "") {
                list_file_cnt++;
            }
        }
    });
    $("#uploadedListCount").text(list_file_cnt);

    var art_file_cnt = 0;
    $.each(art_file_data, function (key, val) {
        if (val.fileName != "") {
            art_file_cnt++;
        }
    });
    $("#uploadedArtCount").text(art_file_cnt);
}
function getBubbleCounts(gOutputData) {
    //if ((appPrivileges.customerNumber == CW_CUSTOMER_NUMBER) && (appPrivileges.roleName != "admin")) $("#uploadedListCount").hide();

    var locations_list = (gOutputData != undefined && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined) ? gOutputData.selectedLocationsAction.selectedLocationsList : [];

    $("#locationsCount").text(locations_list.length);

    var selected_link = getSessionData('advertiser_session');

    var file_data = (gOutputData != undefined && gOutputData.standardizeAction.standardizeFileList != undefined) ? gOutputData.standardizeAction.standardizeFileList : [];

    var art_file_data = (gOutputData != undefined && gOutputData.artworkAction != null && gOutputData.artworkAction.artworkFileList != undefined) ? gOutputData.artworkAction.artworkFileList : [];

    var list_file_cnt = 0;
    $.each(file_data, function (key, val) {
        if (val.fileName != "") {
            var file_source_type = getFileType(val.sourceType);
            //if (appPrivileges.roleName != "admin" && file_source_type.toLowerCase() == "source" && val.fileName != "") {
            if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && val.sourceType != 1)
                list_file_cnt++;
            else if (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER)
                list_file_cnt++;
            //}
            // else if (appPrivileges.roleName == "admin" && val.fileName != "") {
            //     list_file_cnt++;
            // }
        }
    });
    $("#uploadedListCount").text(list_file_cnt);

    var art_file_cnt = 0;
    $.each(art_file_data, function (key, val) {
        if (val.fileName != "") {
            art_file_cnt++;
        }
    });
    $("#uploadedArtCount").text(art_file_cnt);
}
function dateDifference(d1, d2) {
    var t2 = d2.getTime();
    var t1 = d1.getTime();
    return parseInt((t2 - t1) / (24 * 3600 * 1000));
}

function closeWindow() {
    window.open('', '_self', '');
    window.close();
}

function loadMobility() {
    // RCF Button popup calling for TD customer in all the pages
    if (appPrivileges != null && appPrivileges != undefined && appPrivileges.customerNumber != null && appPrivileges.customerNumber != undefined && appPrivileges.customerNumber == "1")
        requestChangePopup();

    var cols = $('div[class^=col]:not([class=colleft]):not([class=colmid]):not([class=colright]):not([class=colmask]):not([class="colmask leftmenu"]):not([class="colmask blogstyle"])');
    //test for mobility...
    var path = window.location.href.toString().substring(window.location.href.toString().indexOf(window.location.host));
    var path_components = path.split('/');
    var href_css = "";
    if (path_components.length > 3) {
        href_css = "";
        href_css = (cols.length >= 3) ? "../css/screen.css" : "../css/screen2col.css";
    }
    else if (path_components.length == 3) {
        if (cols.length <= 2 && (window.location.href.toString().indexOf('login.htm') > -1)) //|| window.location.href.toString().indexOf('login_new.htm') > -1//window.location.href.toString().indexOf('Stores') > -1 || 
            href_css = "css/screen.css"; //if it is login page
        else if (cols.length > 2) {
            href_css = (path_components.length <= 3 && (window.location.href.toString().indexOf('login') == -1 && window.location.href.toString().indexOf('index') == -1 && window.location.href.toString().indexOf('reset') == -1)) ? "../css/screen.css" : "css/screen.css"; //if it is login page
        }
        else
            href_css = "../css/screen2col.css";
    }
    else {
        href_css = "";
        href_css = (cols.length <= 2 && window.location.href.toString().indexOf('.htm') != -1 && (window.location.href.toString().indexOf('login.htm') == -1)) ? "css/screen2col.css" : "css/screen.css"; //&& window.location.href.toString().indexOf('login_new.htm') == -1)
    }
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage 
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="' + href_css + '" media="screen" />');
        sessionStorage.desktop = true;
    }

    if (sessionStorage.desktop) // desktop storage 
        $('head').append('<link rel="stylesheet" type="text/css" href="' + href_css + '" media="screen" />');
    else if (localStorage.mobile) // mobile storage
        return true;
}

function getPagePreferences(page_name, facility_id, customer_id) {
    var g_page_pref_url = "";
    g_page_pref_url = serviceURLDomain + "api/Preferences/" + customer_id + "/" + facility_id + "/" + page_name
    getCORS(g_page_pref_url, null, function (data) {
        if (data != undefined && data != null) {
            applyPreferences(page_name, data);
            if (isSafari && !isMobile) {
                $('div[data-role=page]').trigger('pageshow');
            }
        }
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
}
function applyPreferences(page_name, data) {
    sessionStorage[page_name.substring(0, page_name.lastIndexOf('.')) + "Prefs"] = JSON.stringify(data);
    managePagePrefs(data);
}
function managePagePrefs(page_pref) {
    if (page_pref != undefined && page_pref != null) {
        if (page_pref.buttonPrefsDict != undefined) {
            $.each(page_pref.buttonPrefsDict, function (a, b) {
                if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) && (b.name == "milestones" || b.name == "reports"))
                    b.showControl = 1;
                if ($('#' + b.name)[0] != undefined && $('#' + b.name)[0].type != undefined && $('#' + b.name)[0].type.indexOf("select") > -1)
                    (parseInt(b.showControl) == 1) ? $("#" + b.name).parent().show() : $("#" + b.name).parent().hide();
                else
                    (parseInt(b.showControl) == 0) ? $('#' + b.name).hide() : $('#' + b.name).show();

                if ($('#sp' + b.name).length > 0)
                    $('#sp' + b.name).css('display', (parseInt(b.showControl) == 1) ? 'block' : 'none');
            });
        }
        if (page_pref.imagePrefsDict != undefined) {
            $.each(page_pref.imagePrefsDict, function (a, b) {
                if ($('#' + b.name)[0] != undefined && $('#' + b.name)[0].type != undefined && $('#' + b.name)[0].type.indexOf("select") > -1)
                    (parseInt(b.showControl) == 1) ? $("#" + b.name).parent().show() : $("#" + b.name).parent().hide();
                else
                    (parseInt(b.showControl) == 0) ? $('#' + b.name).hide() : $('#' + b.name).show();

                if ($('#sp' + b.name).length > 0)
                    $('#sp' + b.name).css('display', (parseInt(b.showControl) == 1) ? 'block' : 'none');
            });
        }
        if (page_pref.divPrefsDict != undefined) {
            $.each(page_pref.divPrefsDict, function (a, b) {
                if ($('#' + b.name)[0] != undefined && $('#' + b.name)[0].type != undefined && $('#' + b.name)[0].type.indexOf("select") > -1)
                    (parseInt(b.showControl) == 1) ? $("#" + b.name).parent().show() : $("#" + b.name).parent().hide();
                else
                    (parseInt(b.showControl) == 0) ? $('#' + b.name).hide() : $('#' + b.name).show();

                if ($('#sp' + b.name).length > 0)
                    $('#sp' + b.name).css('display', (parseInt(b.showControl) == 1) ? 'block' : 'none');
            });
        }
    }
}

function getNextPageInOrder() {
    if (sessionStorage.nextPageInOrder == undefined || sessionStorage.nextPageInOrder == null || sessionStorage.nextPageInOrder == "") {
        var link_ids = $('#ulJobLinks li');
        if ((jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER
            || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == OH_CUSTOMER_NUMBER) && (link_ids.length > 0)) {
            var last_navigation_index = (link_ids[link_ids.length - 1].id.toLowerCase() == "licustomernumber") ? ((link_ids[link_ids.length - 2].id.toLowerCase() == "lilogging") ? link_ids.length - 3 : link_ids.length - 2) : link_ids.length - 1;
            sessionStorage.nextPageInOrder = $('#' + link_ids[last_navigation_index].id + ' a').attr('href');
        }
        else {
            var liIds = link_ids.map(function (i, n) {
                if ($(n)[0].attributes["id"] != undefined && $(n)[0].attributes["id"].value != "") {
                    if ($(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "listmanagement" ||
                        $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "listselection" ||
                    $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "listmapping" ||
                    $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "uploadartwork" ||
                        $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "milestones" ||
                        $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "pieceattributes" ||
                        $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "submitorder" ||
                        (($(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "companyinfo" ||
                        $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "baseselection" ||
                        $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "logoselection" ||
                        $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "selectimages" ||
                        $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "offersetup") ||
                        (window.location.href.toLowerCase().indexOf($(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase()) > -1))) {
                        if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
                            if (link_ids[i + 1] != undefined && link_ids[i + 1] != "") return link_ids[i + 1];
                        }
                        else if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
                            if ($(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "uploadartwork") {
                                if (link_ids[i] != undefined && link_ids[i] != "") return link_ids[i];
                            }
                            else if ($(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "listmapping") {
                                if (link_ids[i + 1] != undefined && link_ids[i + 1] != "") return link_ids[i + 1];
                            }
                        }
                        else if ($(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "companyinfo" ||
                              $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "baseselection" ||
                              $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "logoselection" ||
                              $(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "offersetup") {
                            if (link_ids[i] != undefined && link_ids[i] != "") return link_ids[i];
                        }
                        else if (jobCustomerNumber == "1") {
                            if ($(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "uploadartwork") {
                                if (link_ids[i] != undefined && link_ids[i] != "") return link_ids[i];
                                //                            else {
                                //                                if (link_ids[i + 2] != undefined && link_ids[i + 2] != "") return link_ids[i + 2];
                                //                                else if (link_ids[i] != undefined && link_ids[i] != "") return link_ids[i];
                                //                            }
                            }
                        } else if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
                            if ((i + 1) < link_ids.length) {
                                //alert('inside if');
                                if (link_ids[i + 1] != undefined && link_ids[i + 1] != "") {
                                    //alert('inside if1');
                                    return link_ids[i + 1];
                                }
                                else {
                                    //alert('inside else1');
                                    return link_ids[((i - 1) + 1)];
                                }
                            }
                            else {
                                //alert('inside else');
                                return link_ids[((i - 1) + 1)];
                            }
                        }
                    }
                    else if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
                        if ($(n)[0].attributes["id"].value.substring($(n)[0].attributes["id"].value.indexOf('li') + 2).toLowerCase() == "proofing") {
                            if (link_ids[i] != undefined && link_ids[i] != "") return link_ids[i + 1];
                            //                            else {
                            //                                if (link_ids[i + 2] != undefined && link_ids[i + 2] != "") return link_ids[i + 2];
                            //                                else if (link_ids[i] != undefined && link_ids[i] != "") return link_ids[i];
                            //                            }
                        }
                    }
                }
            });
            if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER) {
                if (liIds.length > 1 && liIds[liIds.length - 1] != undefined && (sessionStorage.currentPage == undefined || sessionStorage.currentPage == null || sessionStorage.currentPage.toLowerCase().indexOf('listselection') == -1))
                    sessionStorage.nextPageInOrder = $('#' + liIds[liIds.length - 1].id + ' a').attr('href');
                else if (liIds.length > 0)
                    sessionStorage.nextPageInOrder = $('#' + liIds[0].id + ' a').attr('href');
            }
            else if (liIds.length > 0)
                sessionStorage.nextPageInOrder = $('#' + liIds[0].id + ' a').attr('href');
        }
    }
}

function continueToOrder() {
    sessionStorage.removeItem('visitedProofs');
    var next_page_in_order = sessionStorage.nextPageInOrder;
    if (window.location.href.toLowerCase().indexOf('previewmailing') == -1) {
        sessionStorage.removeItem('nextPageInOrder');
    }
    sessionStorage.removeItem('currentPage');
    // || appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER
    if (appPrivileges.customerNumber == ALLIED_CUSTOMER_NUMBER || appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER) {
        sessionStorage.authString = makeBasicAuth(sessionStorage.username, "welcome123$");
        //jobCustomerNumber = ALLIED_CUSTOMER_NUMBER;
        jobCustomerNumber = appPrivileges.customerNumber;
        //sessionStorage.jobCustomerNumber = ALLIED_CUSTOMER_NUMBER;
        sessionStorage.jobCustomerNumber = appPrivileges.customerNumber;
    }
    goToNextPage(window.location.href, next_page_in_order, 0, "onlycontinue");

}

function getMoreData(selector_id, page_id) {
    $('#' + selector_id).val(page_id);
    $('#' + selector_id).selectmenu('refresh');
    $('#' + selector_id).trigger('onchange');
}

function DiffObjects(o1, o2) {
    // choose a map() impl.
    // you may use $.map from jQuery if you wish
    var map = Array.prototype.map ?
        function (a) { return Array.prototype.map.apply(a, Array.prototype.slice.call(arguments, 1)); } :
        function (a, f) {
            var ret = new Array(a.length), value;
            for (var i = 0, length = a.length; i < length; i++)
                ret[i] = f(a[i], i);
            return ret.concat();
        };

    // shorthand for push impl.
    var push = Array.prototype.push;

    // check for null/undefined values
    if ((o1 == null) || (o2 == null)) {
        if (o1 != o2)
            return [["", "null", o1 != null, o2 != null]];

        return []; // both null
    }
    // compare types
    if ((o1.constructor != o2.constructor) ||
        (typeof o1 != typeof o2)) {
        return [["", "type", Object.prototype.toString.call(o1), Object.prototype.toString.call(o2)]]; // different type

    }

    // compare arrays
    if (Object.prototype.toString.call(o1) == "[object Array]") {
        if (o1.length != o2.length) {
            return [["", "length", o1.length, o2.length]]; // different length
        }
        var diff = [];
        for (var i = 0; i < o1.length; i++) {
            // per element nested diff
            var innerDiff = DiffObjects(o1[i], o2[i]);
            if (innerDiff) { // o1[i] != o2[i]
                // merge diff array into parent's while including parent object name ([i])
                push.apply(diff, map(innerDiff, function (o, j) { o[0] = "[" + i + "]" + o[0]; return o; }));
            }
        }
        // if any differences were found, return them
        if (diff.length)
            return diff;
        // return nothing if arrays equal
        return [];
    }

    // compare object trees
    if (Object.prototype.toString.call(o1) == "[object Object]") {
        var diff = [];
        // check all props in o1
        for (var prop in o1) {
            // the double check in o1 is because in V8 objects remember keys set to undefined 
            if ((typeof o2[prop] == "undefined") && (typeof o1[prop] != "undefined")) {
                // prop exists in o1 but not in o2
                diff.push(["[" + prop + "]", "undefined", o1[prop], undefined]); // prop exists in o1 but not in o2

            }
            else {
                // per element nested diff
                var innerDiff = DiffObjects(o1[prop], o2[prop]);
                if (innerDiff) { // o1[prop] != o2[prop]
                    // merge diff array into parent's while including parent object name ([prop])
                    push.apply(diff, map(innerDiff, function (o, j) { o[0] = "[" + prop + "]" + o[0]; return o; }));
                }

            }
        }
        for (var prop in o2) {
            // the double check in o2 is because in V8 objects remember keys set to undefined 
            if ((typeof o1[prop] == "undefined") && (typeof o2[prop] != "undefined")) {
                // prop exists in o2 but not in o1
                diff.push(["[" + prop + "]", "undefined", undefined, o2[prop]]); // prop exists in o2 but not in o1

            }
        }
        // if any differences were found, return them
        if (diff.length)
            return diff;
        // return nothing if objects equal
        return [];
    }
    // if same type and not null or objects or arrays
    // perform primitive value comparison
    if (o1 != o2) {
        if (typeof o1 == "string" && typeof o2 == "string") {
            if (o1.toString().toLowerCase() != o2.toString().toLowerCase()) return [["", "value", o1, o2]];
        }
        else
            return [["", "value", o1, o2]];
    }

    // return nothing if values are equal
    return [];
}

function homeClickValidation() {
    var diff = [];

    if (window.location.href.toString().toLowerCase().indexOf('approval') > -1) { //checking differently for approvalCheckout page
        var job_setup_output = {};
        var job_setup_output_compare = {};

        $.each(jQuery.parseJSON(sessionStorage.jobSetupOutput), function (key, val) {
            if (key != "checkoutAction" && key != "checkOutAction")
                job_setup_output[key] = val;
        });

        if (sessionStorage.jobSetupOutputCompare != undefined)
            $.each(jQuery.parseJSON(sessionStorage.jobSetupOutputCompare), function (key, val) {
                if (key != "checkoutAction" && key != "checkOutAction")
                    job_setup_output_compare[key] = val;
            });

        diff = DiffObjects(job_setup_output, job_setup_output_compare);
    }
    else
        diff = DiffObjects(jQuery.parseJSON(sessionStorage.jobSetupOutput), jQuery.parseJSON(sessionStorage.jobSetupOutputCompare));

    if (diff.length > 0) {
        var message = 'Leaving this screen without saving this order will cause you to lose all unsaved changes. Click "Leave This Screen" to leave this screen without saving or click "Continue With My Order" to continue and save.';
        $('#homeBtnMsg').text(message);
        $('#popupHomeClickDialog').popup('open');
        return false;
    }
    return true;
}

function showHomeButtonConfirmation(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupHomeClickDialog" data-overlay-theme="d" data-history="false" data-theme="c" style="max-width:500px;" class="ui-corner-all">' +
					'<div data-role="header" data-theme="d" class="ui-corner-top">' +
					'	<h1>Warning!</h1>' +
					'</div>' +
					'<div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">' +
					'<div id="homeBtnMsg"></div></br>' +
					'	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnLeaveThisScreen" data-theme="a" onclick="leavePageWithoutSave();">Leave This Screen</a>' +
					'	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnContinueWithOrder" data-theme="a" onclick="$(\'#popupHomeClickDialog\').popup(\'close\');">Continue With My Order</a>' +
					'</div>' +
				'</div>';
    $("#" + page_name).append(msg_box);
}

function leavePageWithoutSave() {
    //removeSessionData("jobSetupOutput");
    //removeSessionData("jobSetupOutputCompare");
    redirectToHome();
}

function removeJobSessions(action) {
    Object.keys(sessionStorage)
  .filter(function (k) {
      if (/prefs/.test(k.toLowerCase()))
          sessionStorage.removeItem(k);
  });
    sessionStorage.removeItem("updateLocationsList");
    sessionStorage.removeItem('tempSelectedStores');
    removeSessionData("jobSetupOutput");
    removeSessionData("jobSetupOutputCompare");
    removeSessionData("navGroupCount");
    removeSessionData("navigationCount");
    removeSessionData("artworkFilesInfo");
    removeSessionData("checkForSavedStoreData");
    removeSessionData("savedStores");
    if (sessionStorage.selectedCustomerInfo != undefined && sessionStorage.selectedCustomerInfo != null)
        removeSessionData("selectedCustomerInfo");
    if (sessionStorage.custNavLinks != undefined && sessionStorage.custNavLinks != null)
        removeSessionData("custNavLinks");
    //if (!(sessionStorage.username.toLowerCase().indexOf('demo') > -1)) {
    if (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0) {
        if (action != 'single' && sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null)
            removeSessionData("jobCustomerNumber");
    }
}

function mapVerticalResize() {
    var map_holder_width = (window.location.href.toLowerCase().indexOf('lma') > -1) ? (($(window).width() > 414) ? '98% !important' : '90% !important') : (($(window).width() > 414) ? '98% !important' : '90% !important');
    var map_holder_mobile_width = (window.location.href.toLowerCase().indexOf('lma') > -1) ? (($(window).width() > 414) ? '98% !important' : '90% !important') : (($(window).width() > 414) ? '98% !important' : '90% !important');
    var map_width = (window.location.href.toLowerCase().indexOf('lma') > -1) ? '100% !important' : (($(window).width() > 414) ? '100% !important':'100% !important');
    var map_height = (window.location.href.toLowerCase().indexOf('lma') > -1) ? '95% !important' : (($(window).width() > 414) ? '88% !important' : '85% !important');
    if (window.location.href.toLowerCase().indexOf('lma') == -1) {
    //    var map_holder_css_text = ('width:' + map_holder_width) + ';' + ((window.location.href.toLowerCase().indexOf('lma') == -1 && $(window).width() > 414) ? 'height:86% !important;' : 'height:58% !important;');
    //    var map_css_text = ('width:' + map_width) + ';' + ('height:' + map_height) + ';'
        $('#mapHolder').parent().css('padding-left', '0em');
    //    $('#mapHolder')[0].style.cssText = map_holder_css_text;
    //    $('#map')[0].style.cssText = map_css_text;
    //   // $('#divBottom')[0].style.cssText = (($(window).width() > 414) ? 'margin-top:52% !important;float:right' : 'margin-top:100% !important;float:right');
    }
    //else {
        if (!isMobile) {
            if ($(window).width() > 414) {
                $('#mapHolder').css('width', map_holder_width);
                $('#map').css('width', map_width);
                $('#map').css('height', map_height);
            }
            else {
                $('#mapHolder').css('width', map_holder_width);
                $('#map').css('width', map_width);
                $('#map').css('height', map_height);
            }
        }
        else if (isMobile) {
            if ($(window).width() > 414) {
                $('#mapHolder').css('width', map_holder_mobile_width);
                $('#map').css('width', map_width);
                $('#map').css('height', map_height);
            } else {
                $('#mapHolder').css('width', map_holder_mobile_width);
                $('#map').css('width', map_width);
                $('#map').css('height', map_height);
            }
        }
    //}
}

function redirectToHome() {
    removeJobSessions('home');
    removeSessionData("jobNumber");
    removeSessionData("negJobNumber");
    removeSessionData("isGizmoJob");
    //    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER) {
    //        window.location.href = "../index_bbb.htm";
    //    }
    //    else {
    window.location.href = "../index.htm";
    //}
}

//to show a static image (loading...) while opening the reports.
function loadingImg(page_name) {
    //var over = '<div id="overlay"><img id="loading" alt="Loading...." src="../images/kaleidoscope_wait.gif"></img><span id="loadingText">Loading....</span></div>';
    var image_path = "../images/gizmo_load_anim.gif";
    if (page_name == "_indexGeneral")
        image_path = "images/gizmo_load_anim.gif";
    //var over = '<div id="waitPopUp" data-role="popup" class="ui-content"  style="width:900;height:500;"  data-rel="dialog" data-dismissible="false" data-history="false"><center><span style="width:500px;height:200px;"><img id="loading1" alt="Loading...." src="' + image_path + '" style="width: 100px;height: 100px;" /><p><span id="loadingText1" style="font-weight:bold;color:#4a80af;font-family:Tahoma,verdana, Arial;font-size:large;">Loading Report....</span></p></span></center></div>';
    var over = '<div id="waitPopUp" data-role="popup" class="ui-content"  style="width:900;height:500;"  data-rel="dialog" data-dismissible="false" data-history="false"><center><span style="width:500px;height:200px;"><img id="loading1" alt="Loading...." src="' + image_path + '" /><p><span id="loadingText1" style="font-weight:bold;color:#4a80af;font-family:Tahoma,verdana, Arial;font-size:large;">Loading Report....</span></p></span></center></div>';
    $('#' + page_name).append(over);
}

function showErrorResponseText(response, type) {
    var response_error = "";
    var error_text = 'Unknown error: Contact a Site Administrator';
    if (response != null && response.statusText != undefined && response.statusText != null && response.statusText != "abort") {
        switch (response.status) {
            case 0:
            case 400:
            case 403:
            case 404:
            case 417:
            case 500:
            case 502:
                response_error = error_text;
                break;
            default:
                {
                    if (!(jQuery.browser.msie)) {
                        response_error = (response.responseText != undefined && response.responseText != null && response.responseText != "") ? response.responseText.replace(/\{/g, '').replace(/\}/g, '') : error_text;

                    }
                    else {
                        response_error = (response.errorMessage != undefined && response.errorMessage != "") ? response.errorMessage : ((response.responseText != undefined && response.responseText != null && response.responseText != "") ? response.responseText.replace(/\{/g, '').replace(/\}/g, '') : error_text);
                    }
                }
        }
        if (response_error != "") {
            if (type == true) {
                $('#divError').html(response_error);
                $('#divError').css('display', 'block');
            }
            else {
                window.setTimeout(function popupDelay() {
                    $('#alertmsg').text(response_error);
                    $('#popupDialog').popup('open', { positionTo: 'window' });
                }, 1000);
            }
        }
        else {
            $('#divError').css('display', 'none');
        }
    }
    else if (response != undefined && response.responseText != undefined && response.responseText != null && response.responseText != "") {
        error_text = response.responseText;
        $('#alertmsg').text(error_text);
        if (type == true) {
            $('#divError').html(error_text);
            $('#divError').css('display', 'block');
        }
        else {
            window.setTimeout(function popupDelay() {
                $('#alertmsg').text(error_text);
                $('#popupDialog').popup('open', { positionTo: 'window' });
            }, 1000);
        }
    }
    else {
        if (response != null && response.statusText != undefined && response.statusText != null && response.statusText == "abort" && (window.location.href.toLowerCase().indexOf('uploadlist') > -1 || window.location.href.toLowerCase().indexOf('uploadartwork') > -1)) {
            error_text = "The file upload has been canceled."
        }
        $('#alertmsg').text(error_text);
        window.setTimeout(function popupDelay() {
            $('#popupDialog').popup('open', { positionTo: 'window' });
        }, 1000);
    }
}

function loadPageData(pref_obj) {
    if (!(jQuery.browser.msie) || (jQuery.browser.msie && (pref_obj != undefined && pref_obj != null && pref_obj != "null" && pref_obj != "")))
        loadData();
}
function addIEListener() {
    if (window.addEventListener) {
        addEventListener("message", listener, false);
    } else {
        attachEvent("onmessage", listener);
    }
}
function checkPagePrefsData(page_name, pref_data) {
    if (pref_data == undefined || pref_data == null || pref_data == "")
        getPagePreferences(page_name, sessionStorage.facilityId, jobCustomerNumber);
    else
        managePagePrefs(jQuery.parseJSON(pref_data));
}

function fnConfirmSignout(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupSignout" data-overlay-theme="d" data-history="false" data-theme="c" data-dismissible="false" style="max-width:500px;" class="ui-corner-all">' +
                    '<div data-role="header" data-theme="a" class="ui-corner-top">' +
                    '	<h1>Warning!</h1>' +
                    '</div>' +
                    '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
                    '<div id="pageRedirect"></div>' +
                    '<div id="signoutBtnMsg">You have not Submitted your Approvals! If you would like to Submit your Approvals click \"Cancel\", then \"Submit Approvals\". If you do not want to Submit your Approvals click \"Continue\"</div></br>' +
                    '	<a href="#" data-role="button" data-inline="true" id="btnCancel" data-theme="a" onclick="$(\'#popupSignout\').popup(\'close\');">Cancel</a>' +
                    '	<a href="#" data-role="button" data-inline="true" id="btnContinue" data-transition="flow" data-theme="a" onclick="fnSignoutContinue();">Continue</a>' +
                    '</div>' +
                '</div>';
    $("#" + page_name).append(msg_box);
}

function navigatePages(page_name) {
    if (page_name === "../login.htm") {
        clearAllSession();
    }
    document.location.href = page_name;
}

function fnSignoutContinue() {
    if ($('#pageRedirect').val() == "../login.htm") {
        clearAllSession();
        document.location.href = $('#pageRedirect').val();
    }
    else
        document.location.href = $('#pageRedirect').val();
}

function loadUploadIFrame() {
    if ($.browser.msie) {
        var iframe_form = '<form enctype="multipart/form-data" method="post" id="formUpload" name="formUpload">';
        iframe_form += '<div id="spnFileInput" style="height: 0px; width: 0px; overflow: hidden;">';
        iframe_form += '<input type="file" id="mailFiles[]" name="mailFiles[]" />';
        iframe_form += '</div><div id="upload"></div></form>';
        var i = 0;
        var target = $(window.frames[0].frames.frameElement).contents()[0];
        target.open();
        target.write('<!doctype html><html><head></head><body></body></html>');
        target.close();
        $(window.frames[0].frames.frameElement).contents().find('body').html(iframe_form);
        $(window.frames[0].frames.frameElement).css('display', 'none');
    }
    else {
        $('#ieUploadframe').css('visibility', 'hidden');
    }
}

function fnCreateMWNavLinks(page_name) {
    var mw_navlinks = '<ul id="ulNavLinks">';
    mw_navlinks += '<li><a id="aUpload" data-icon="myapp-upload">Upload</a></li>';
    mw_navlinks += '<li id="liApproval"><a id="aApproval" data-icon="check">Approval</a></li>';
    mw_navlinks += '<li id="liStatus"><a  id="aStatus" data-icon="myapp-status">Status</a></li>';
    mw_navlinks += '<li><a id="aSignOut"   data-theme="i" data-icon="delete">Sign-Out</a></li>';
    mw_navlinks += '</ul>';

    $("#dvMediaworksLinks").append(mw_navlinks);

    if (page_name == "upload") {
        $("#aUpload").attr("data-theme", "i");
        $("#aUpload").attr("class", "ui-btn-active");
        $("#aApproval").attr("data-theme", "i");
        $("#aApproval").attr("onclick", "navigatePages('../fileApproval/mediaworksFileApproval.html')"); //we need to enable this line once the approval page functionality completes
        //$("#aApproval").attr("onclick", "popUpApprovalUnAvailable();");

        $("#aStatus").attr("data-theme", "i");
        $("#aStatus").attr("onclick", "navigatePages('../processStatus/mediaworksProcessStatus.html')");

        $("#aSignOut").attr("onclick", "navigatePages('../login.htm?q=" + appPrivileges.customerNumber + "')");
    }
    else if (page_name == "approval") {
        $("#aUpload").attr("data-theme", "i");
        $("#aApproval").attr("class", "ui-btn-active");
        $("#aUpload").attr("onclick", "isDataChanged('../fileUpload/mediaworksFileUpload.html')");

        $("#aApproval").attr("data-theme", "i");

        $("#aStatus").attr("data-theme", "i");
        $("#aStatus").attr("onclick", "isDataChanged('../processStatus/mediaworksProcessStatus.html')");

        $("#aSignOut").attr("onclick", "isDataChanged('../login.htm?q=" + appPrivileges.customerNumber + "')");
    }
    else if (page_name == "status") {
        $("#aUpload").attr("data-theme", "i");
        $("#aStatus").attr("class", "ui-btn-active");
        if (sessionStorage.publisherId == undefined || sessionStorage.publisherId == "") //for admin without clicking on "publishers" one can't navigate to Upload or Approval links.. if already selected one publisher he can navigate.
            $("#aUpload").attr("onclick", "popUpUploadApprovalRestricted();");
        else
            $("#aUpload").attr("onclick", "navigatePages('../fileUpload/mediaworksFileUpload.html')");

        $("#aApproval").attr("data-theme", "i");
        if (sessionStorage.publisherId == undefined || sessionStorage.publisherId == "") { //for admin without clicking on "publishers" one can't navigate to Upload or Approval links.. if already selected one publisher he can navigate.
            $("#aApproval").attr("onclick", "popUpUploadApprovalRestricted();");
        }
        else {
            $("#aApproval").attr("onclick", "navigatePages('../fileApproval/mediaworksFileApproval.html')"); //we need to enable this line once the approval page functionality completes
            //$("#aApproval").attr("onclick", "popUpApprovalUnAvailable();");
        }

        $("#aStatus").attr("data-theme", "i");

        $("#aSignOut").attr("onclick", "navigatePages('../login.htm?q=" + appPrivileges.customerNumber + "')");
    }

    if (sessionStorage.userRole != "admin") {
        $("#liStatus").remove();
        $("#liApproval").remove();
    }
}

function popUpApprovalUnAvailable() {
    var msg = "The Approval step has not yet been activated.";
    $('#alertmsg').text(msg);
    $('#popupDialog').popup('open');
}

function popUpUploadApprovalRestricted() {
    var msg = "Please select any Publisher from the below list.";
    $('#alertmsg').text(msg);
    $('#popupDialog').popup('open');
}

function fnVerifyScriptBlockDict(page_prefs) {
    if ((page_prefs != null && page_prefs != undefined) && (page_prefs.scriptBlockDict == undefined || page_prefs.scriptBlockDict == null || JSON.stringify(page_prefs.scriptBlockDict).length == 2)) {
        if ($.trim($('#divError').html()) == "") {
            $('#divError').html(errorMessage);
            $('#divError').css('display', 'block');
        }
        return false;
    }
    return true;
}

function fnViewJobs(user_jobs_only) {
    //$('#col2').empty();
    //$('#col3').empty();
    makeGData(user_jobs_only);
}

function viewJobs() {
    var view_jobs = '<div style="margin: 24px 30px 0px 30px;" id="dvJobs">' +
                      ' <fieldset data-role="controlgroup">' +
                      '  <legend></legend>' +
                       '  <input type="radio" name="radio-choice" id="rdoAllJobs" value="alljobs" onclick="fnViewJobs(\'alljobs\');" checked="checked" data-theme="b" data-mini="true" />' +
    //'  <label for="rdoAllJobs" onclick="fnViewJobs(\'alljobs\');">View All Jobs</label>' +
                      '  <label for="rdoAllJobs">All Jobs</label>' +

                      '  <input type="radio" name="radio-choice" id="rdoMyJobs" value="myjobs" onclick="fnViewJobs(\'myjobs\');" data-theme="b" data-mini="true" />' +
                      '  <label for="rdoMyJobs">My Jobs</label>' +
    //'  <label for="rdoMyJobs" onclick="fnViewJobs(\'myjobs\');">View My Jobs</label>' +
                      '  </fieldset>' +
                    ' </div>';
    return view_jobs;
}

function isIE10Browser() {
    var isIE10 = false;
    if (Function('/*@cc_on return /^10/.test(@_jscript_version) @*/')() || Function('/*@cc_on return /^11/.test(@_jscript_version) @*/')()) {
        //if (Function('/*@_jscript_version @*/' > 9)) {
        isIE10 = true;
    }
    return isIE10;
}

function sortMilestones(milestone_dict) {
    var counter = 0;
    var sorted_milestones = {};
    var mile_stone_info = [];
    $.each(milestone_dict, function (a, b) {
        var item_name = b.description.toLowerCase().replace(/ /g, '').replace(/-/g, '');
        //mile_stone_info[counter] = item_name;
        mile_stone_info[counter] = b;
        counter++;
    });
    mile_stone_info.sort(function (a, b) {
        //        if (((a == "salestart" || a == "saleend") && (b == "salestart" || b == "saleend")) ||
        //        (a == "signupstart" || a == "signupend") && (b == "signupstart" || b == "signupend") ||
        //        (a == "approvalstart" || a == "approvalend") && (b == "approvalstart" || b == "approvalend"))
        //            return a < b ? 1 : -1;
        //        else
        //if (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER)
        //return a > b ? 1 : -1;
        return a.ranking > b.ranking ? 1 : -1;
    });
    $.each(mile_stone_info, function (a, b) {
        $.each(milestone_dict, function (c, d) {
            //if (d.description.toLowerCase().replace(/ /g, '').replace(/-/g, '') == b)
            if (d.description.toLowerCase().replace(/ /g, '').replace(/-/g, '') == b.description.toLowerCase().replace(/ /g, '').replace(/-/g, ''))
                sorted_milestones[c] = d;
        });
    });
    return sorted_milestones;
}

function showDatePicker() {
    if ($('#ui-datepicker-div').html() != '')
        $('#ui-datepicker-div').css('z-index', '2000');
}

function nonEditableMilestoneMessage(milestone_type) {
    if (isMilestoneEditable(milestone_type)) {
        showDatePicker();
        if (window.location.href.toLowerCase().indexOf('milestone') > -1)
            pageObj.populateForm(milestone_type);
        else
            pageObj.populateForm(milestone_type);
    }
    else {
        $('#popupDialog').popup('open');
        $('#alertmsg').html("This milestone date cannot be manually set, it is system generated based on the In-Home Milestone and Mail Class.");
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    }
}

function isMilestoneEditable(milestone_type) {
    switch (milestone_type) {
        case "mail":
            return false;
            break;
        default:
            return true;
            break;
    }
}

/* 
Function returns approval type by passing approval type code and 
returns approval type code by passing approval type.
Ex: returns 4 if 'print' is passed and returns 'print' if 4 is passed 
Below are the possible approval codes and approval types
0 = order inclusion
1 = counts
2 = art
3 = proof
4 = print
5 = lettershop
*/
function getApprovalType(approval_type) {
    switch (approval_type) {
        case "order inclusion":
        case 0:
            if (typeof (approval_type) == "number")
                return "order inclusion";
            else
                return 0;
            break;
        case "counts":
        case 1:
            if (typeof (approval_type) == "number")
                return "counts";
            else
                return 1;
            break;
        case "art":
        case 2:
            if (typeof (approval_type) == "number")
                return "art";
            else
                return 2;
            break;
        case "proof":
        case 3:
            if (typeof (approval_type) == "number")
                return "proof";
            else
                return 3;
            break;
        case "print":
        case 4:
            if (typeof (approval_type) == "number")
                return "print";
            else
                return 4;
            break;
        case "lettershop":
        case 5:
            if (typeof (approval_type) == "number")
                return "lettershop";
            else
                return 5;
            break;
        default:
            return "";
    }
}

/* 
Function returns approval type by passing approval type code and 
returns approval type code by passing approval type.
Ex: returns 4 if 'print' is passed and returns 'print' if 4 is passed 
Below are the possible approval codes and approval types
0 = rejected/declined
1 = aproved
4 = open/pending
6 = pending corrections (approved with changes)
*/
function getApprovalStatus(approval_status) {
    switch (approval_status) {
        case "rejected":
        case "declined":
        case 0:
            if (typeof (approval_status) == "number")
                return "declined";
            else
                return 0;
            break;
        case "aproved":
        case 1:
            if (typeof (approval_status) == "number")
                return "approved";
            else
                return 1;
            break;
        case "open":
        case "pending":
        case 4:
            if (typeof (approval_status) == "number")
                return "open";
            else
                return 4;
            break;
        case "pending corrections":
        case "approved with changes":
        case 6:
            if (typeof (approval_status) == "number")
                return "pending corrections";
            else
                return 6;
            break;
        default:
            return "";
    }
}

function calculateGridWidth() {
    // Get width of parent container
    //var width = jQuery(targetContainer).attr('clientWidth');
    var width = jQuery('.col2').find('div[data-role=content]').width();
    if (width == null || width < 1) {
        // For IE, revert to offsetWidth if necessary
        //width = jQuery(targetContainer).attr('offsetWidth');
        width = jQuery('.col2').find('div[data-role=content]').innerWidth();

    }
    width = width - 2; // Fudge factor to prevent horizontal scrollbars
    return width;
}

function loadCustNavLinks(customer_number) {
    var nav_links_url = serviceURLDomain + "api/Authorization/" + customer_number;
    getCORS(nav_links_url, null, function (data) {
        // The below code has to be removed once these information is being returned from service after 
        if (data.customerNumber == DATAWORKS_CUSTOMER_NUMBER) {
            var temp_array = {};
            temp_array["companyName"] = data.companyName;
            temp_array["customerNumber"] = data.customerNumber;
            temp_array["facility_id"] = data.facility_id;
            temp_array["image_logo"] = data.image_logo;
            temp_array["label_scheduledDate"] = data.label_scheduledDate;
            temp_array["pageHome"] = data.pageHome;
            temp_array["roleName"] = data.roleName;
            temp_array["display_page_selectTemplate"] = true;
            temp_array["display_page_uploadList"] = true;
            temp_array["display_page_uploadArtwork"] = true;
            temp_array["display_page_submitOrder"] = true;
            sessionStorage.custNavLinks = JSON.stringify(temp_array);
        }
        else {
            sessionStorage.custNavLinks = JSON.stringify(data);
        }
        if (window.location.href.toString().toLowerCase().indexOf('template') > -1) {
            sessionStorage.jobCustomerNumber = pageObj.gOutputData.customerNumber;
            jobCustomerNumber = pageObj.gOutputData.customerNumber;
            sessionStorage.removeItem('templates');
            pageObj = null;
            window.location.reload();
        }
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
}

function demoNavlinks(demo_cust) {
    var nav_links_url = serviceURLDomain + "api/Authorization/" + demo_cust;
    getCORS(nav_links_url, null, function (data) {
        //data.customerNumber = appPrivileges.customerNumber;
        // data.companyName = appPrivileges.companyName;
        fnManageSessionsForNewJobs();
        sessionStorage.custNavLinks = JSON.stringify(data);
        sessionStorage.jobCustomerNumber = (data.customerNumber != undefined && data.customerNumber != null && data.customerNumber != "") ? data.customerNumber : appPrivileges.customerNumber;
        jobCustomerNumber = (data.customerNumber != undefined && data.customerNumber != null && data.customerNumber != "") ? data.customerNumber : appPrivileges.customerNumber;
        makeGData();
        //window.location.href = "jobSelectTemplate/jobSelectTemplate.html";

    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
}

function setDateFormatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var current_date = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var formated_date = current_date + ' ' + hours + ':' + minutes + ' ' + ampm;
    return formated_date;
}


function closeNavPanel() {
    $('#navLinksPanel').panel('close');
    sessionStorage.isNavPanelOpen = false;
}

function persistNavPanelState() {
    if ($("#navLinksPanel").hasClass("ui-panel-open") == false) {
        if ((sessionStorage.isNavPanelOpen == undefined || sessionStorage.isNavPanelOpen == null || sessionStorage.isNavPanelOpen == "") || ((sessionStorage.isNavPanelOpen != undefined && sessionStorage.isNavPanelOpen != null && sessionStorage.isNavPanelOpen != "") && JSON.parse(sessionStorage.isNavPanelOpen))) {
            $("#navLinksPanel").panel("open");
            sessionStorage.isNavPanelOpen = true;
        }
    }
    else if ((sessionStorage.isNavPanelOpen != undefined && sessionStorage.isNavPanelOpen != null && sessionStorage.isNavPanelOpen != "") && JSON.parse(sessionStorage.isNavPanelOpen)) {
        $("#navLinksPanel").panel("open");
        sessionStorage.isNavPanelOpen = true;
    }
    $('div.ui-slider-inneroffset a').attr('href', '#sldrShowHints');
}

function createPlaceHolderforIE() {
    if ($.browser.msie) {
        $('input[placeholder]').each(function () {
            var input = $(this);
            if ($(input).val() == "") $(input).val(input.attr('placeholder'));
            if ($(input).val() == '' || $(input).val().toLowerCase() == $(input).attr('placeholder').toLowerCase())
                $(input).css('color', 'grey');
            else
                $(input).css('color', 'black');

            $(input).focus(function () {
                if ($(input).val().toLowerCase() == $(input).attr('placeholder').toLowerCase()) {
                    input.val('').css('color', 'black');
                }
                else
                    $(input).css('color', 'black');
            });
            $(input).blur(function () {
                if ($(input).val() == '' || $(input).val().toLowerCase() == $(input).attr('placeholder').toLowerCase()) {
                    $(input).val($(input).attr('placeholder')).css('color', 'grey');
                }
                else
                    $(input).css('color', 'black');
            });
        });
    };
};

// Numeric only control handler
jQuery.fn.ForceNumericOnly =
function () {
    return this.each(function () {
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY            
            return (key == 8 || key == 9 || key == 46 || (key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
        });
    });
};
function ddlKeyboardSelection(id) {
    var typing_Timer;                //timer identifier
    var done_TypingInterval = 200;  //time in ms, 5 second for example
    var totalcahr = '';
    //on keyup, start the countdown
    if ($.browser.msie) {
        $('#' + id).keyup(function (e) {
            totalcahr += e.key;
            clearTimeout(typing_Timer);
            if ($('#' + id).val) {
                typing_Timer = setTimeout(function () {

                    var v = $("#" + id).val();
                    var temp = totalcahr;
                    totalcahr = '';
                    //$("#select-choice-12 option[text=" + temp + "]").attr("selected", "selected");
                    var FirstCharacterUpperCase = temp.substring(0, 1).toUpperCase();
                    var MinusFirstCharacterSubstring = temp.substring(1, temp.length);
                    var MergeValue = FirstCharacterUpperCase + MinusFirstCharacterSubstring;

                    var elem = $("#" + id + " option");
                    var opt, i = 0;
                    while (opt = elem[i++]) {
                        if (opt.text.indexOf(MergeValue) == 0) {
                            opt.selected = true;
                            $("#" + id + " option:selected").change();
                            return;
                        }
                    }


                }, done_TypingInterval);
            }
        });
    }
}
var getFieldDisplayName = function (page_name) {
    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
    var page_display_name = '';
    $.each(new String(page_name), function (key, value) {
        if (key > 0 && value.match(PATTERN)) {
            page_display_name += ' ' + value;
        }
        else {
            page_display_name += value;
        }
    });
    return page_display_name.initCap();
};

//Capitalizes the first letter in the given string.
String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

//Gets the selected job data from service.
var getDemoEditJobData = function () {
    var facilityId = sessionStorage.facilityId;
    jobNumber = sessionStorage.jobNumber;
    var gEditJobService = serviceURLDomain + "api/JobTicket/" + facilityId + "/" + jobNumber + "/" + sessionStorage.username;
    getCORS(gEditJobService, null, function (data) {
        assignGetData(data);
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
}

//Creates required local session objects for the selected job when edit is clicked.
var assignGetData = function (data) {
    loadDemoJobTicket();
    if (data == null) {
        $('#alertmsg').text("No data exists for the selected Job. Please contact Administrator.");
        $('#popupDialog').popup('open');
        return false;
    }
    else {
        makeEditJobData(data);
        var selected_demo_program = "";
        if (sessionStorage.demoProgram != undefined && sessionStorage.demoProgram != null && sessionStorage.demoProgram != "" && sessionStorage.demoProgram == "lmm")
            window.location.href = "jobSelectLocations/jobSelectLocations.html";
        else if (sessionStorage.demoProgram != undefined && sessionStorage.demoProgram != null && sessionStorage.demoProgram != "" && sessionStorage.demoProgram == "pam")
            window.location.href = "jobApproval/jobApproval.html";
        else
            window.location.href = "listSelection/jobListSelection.html";
    }
}

//Creates the required sessions objects for selected job
var makeEditJobData = function (data) {
    var g_output_data = data;

    if (g_output_data["milestoneAction"] == undefined) g_output_data["milestoneAction"] = {};

    if (g_output_data.mailstreamAction.advancedPresortOptions == undefined) g_output_data.mailstreamAction["advancedPresortOptions"] = [];
    if (g_output_data.outputAction.sofFileMapChkBox == undefined) g_output_data.outputAction["sofFileMapChkBox"] = "";
    if (g_output_data.outputAction.sofFieldsList == undefined) g_output_data.outputAction["sofFieldsList"] = {};
    if (g_output_data.outputAction.sasFileMapChkBox == undefined) g_output_data.outputAction["sasFileMapChkBox"] = "";
    if (g_output_data.outputAction.advancedOptions == undefined) g_output_data.outputAction["advancedOptions"] = [];
    if (g_output_data.selectedLocationsAction == undefined) g_output_data["selectedLocationsAction"] = {};

    if (g_output_data.selectedLocationsAction.selectedLocationsList == undefined)
        g_output_data.selectedLocationsAction["selectedLocationsList"] = [];
    if (g_output_data.selectedLocationsAction.name == undefined)
        g_output_data.selectedLocationsAction["name"] = "selectedLocations1";
    if (g_output_data.selectedLocationsAction.type == undefined)
        g_output_data.selectedLocationsAction["type"] = "selectedLocations";

    if (g_output_data.milestoneAction.milestoneDict == undefined) g_output_data.milestoneAction["milestoneDict"] = {}; // else g_output_data.milestoneAction.milestoneDict = gData.milestoneDict;
    //if (g_output_data.facilityId == undefined) g_output_data["facilityId"] = (appPrivileges.customerNumber != "1") ? 1 : data.facilityId;
    if (g_output_data.facilityId == undefined) g_output_data["facilityId"] = (g_output_data.customerNumber != "1") ? 1 : sessionStorage.facilityId;
    if (g_output_data.needsPostageReportUpdate == undefined && g_output_data.customerNumber == CW_CUSTOMER_NUMBER || g_output_data.customerNumber == BBB_CUSTOMER_NUMBER || g_output_data.customerNumber == AAG_CUSTOMER_NUMBER || g_output_data.customerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || g_output_data.customerNumber == SANDIEGO_CUSTOMER_NUMBER || g_output_data.customerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) g_output_data["needsPostageReportUpdate"] = false;
    // START The below code has to be removed once we get this data from service. - 08232015 - chalapathi
    if (g_output_data.customerNumber == CASEYS_CUSTOMER_NUMBER) {
        g_output_data["modulesAction"] = {};
        //if (template_url.indexOf('_5_5_11_') > -1) {
        if (g_output_data.jobTypeId == 71) {
            g_output_data.modulesAction["modulesDict"] = {
                "display_page_selectTemplate": "true",
                "display_page_selectLocations": "true",
                "display_page_milestones": "true",
                //"display_page_pieceAttributes": "true",
                //"display_page_email": "true",
                "display_page_listSelection": "true",
                "display_page_uploadList": "true",
                "display_page_optInStatus": "true"
            };
        }
        else if (g_output_data.jobTypeId == 72) {
            g_output_data.modulesAction["modulesDict"] = {
                "display_page_selectTemplate": "true",
                "display_page_selectLocations": "true",
                "display_page_milestones": "true",
                //"display_page_pieceAttributes": "true",
                "display_page_listSelection": "true",
                "display_page_uploadList": "true",
                //"display_page_uploadArtwork": "true",
                //"display_page_approval": "true",
                //"display_page_submitOrder": "true"
                "display_page_optInStatus": "true"
            };
        }
        else if (g_output_data.jobTypeId == 73) {
            g_output_data.modulesAction["modulesDict"] = {
                "display_page_selectTemplate": "true",
                "display_page_selectLocations": "true",
                "display_page_milestones": "true",
                "display_page_pieceAttributes": "true",
                "display_page_uploadArtwork": "true",
                "display_page_listSelection": "true",
                "display_page_uploadList": "true",
                "display_page_approval": "true",
                "display_page_submitOrder": "true"
                //Order Summary
            };
        }
    }
    //TO BE REMOVED when we get the below from service.- 10072015 - chalapathi
    if (g_output_data.customerNumber == ALLIED_CUSTOMER_NUMBER) {
        var template_selected = g_output_data.templateName.substring(1);
        template_selected = template_selected.substring(0, template_selected.indexOf('_'));
        var temp = getDemoJobTicketComponents(template_selected);
        if (Object.keys(temp).length > 0) {
            $.each(temp, function (key, val) {
                if (g_output_data[key] == undefined) {
                    g_output_data[key] = val;
                }
            });
        }
        g_output_data.selectedLocationsAction = demo_job_ticket.selectedLocationsAction;
    }
    if (g_output_data.customerNumber == AAG_CUSTOMER_NUMBER || g_output_data.customerNumber == DCA_CUSTOMER_NUMBER) {// || g_output_data.customerNumber == SK_CUSTOMER_NUMBER  || g_output_data.customerNumber == KUBOTA_CUSTOMER_NUMBER || g_output_data.customerNumber == GWA_CUSTOMER_NUMBER
        if (g_output_data["companyInfoAction"] == undefined) {
            g_output_data["companyInfoAction"] = {};
            g_output_data.companyInfoAction["isReplicated"] = true;
        }
        if (g_output_data.customerNumber == AAG_CUSTOMER_NUMBER || g_output_data.customerNumber == DCA_CUSTOMER_NUMBER) {// || g_output_data.customerNumber == KUBOTA_CUSTOMER_NUMBER || g_output_data.customerNumber == GWA_CUSTOMER_NUMBER
            if (g_output_data["selectImagesAction"] == undefined) {
                g_output_data["selectImagesAction"] = {};
                g_output_data.companyInfoAction["isReplicated"] = true;
            }
        }
        if (g_output_data.customerNumber == DCA_CUSTOMER_NUMBER ) {// || g_output_data.customerNumber == SK_CUSTOMER_NUMBER
            if (g_output_data["offerSetupAction"] == undefined) {
                g_output_data["offerSetupAction"] = {};
                g_output_data.offerSetupAction["isReplicated"] = true;
            }
        }
        if ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && g_output_data.templateName.indexOf('_11x6_') > -1) {
            g_output_data.mailstreamAction.width = 11;
        }
    }
    // END
    if (g_output_data.customerNumber != "1")
        saveSessionData("facilityId", sessionStorage.facilityId);
    else
        saveSessionData("facilityId", facility_id);
    getCORS(serviceURLDomain + "api/Locations_dropDown/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + g_output_data.jobNumber + "/0/2/" + g_output_data.jobTypeId, null, function (data) {
        if (data.locationsSelected != undefined && data.locationsSelected != null && data.locationsSelected != "")
            g_output_data.selectedLocationsAction.selectedLocationsList = data.locationsSelected;
        createEditJobTicketSession(g_output_data);
    }, function (error_response) {
        showErrorResponseText(error_response, true);
        createEditJobTicketSession(g_output_data)
    });
};

function createEditJobTicketSession(g_output_data) {
    sessionStorage.jobSetupOutput = JSON.stringify(g_output_data);
    sessionStorage.jobSetupOutputCompare = JSON.stringify(g_output_data); //this session is to use in approval checkout page to get 
    sessionStorage.jobCustomerNumber = (g_output_data.customerNumber != null && g_output_data.customerNumber != undefined && g_output_data.customerNumber != "") ? g_output_data.customerNumber : appPrivileges.customerNumber;
    sessionStorage.jobCustomerName = (g_output_data.customerName != null && g_output_data.customerName != undefined && g_output_data.customerName != "") ? g_output_data.customerName : appPrivileges.companyName;

    var selected_customer_info = { "customerNumber": g_output_data.customerNumber, "customerName": g_output_data.customerName };
    sessionStorage.selectedCustomerInfo = JSON.stringify(selected_customer_info);

    if (g_output_data.modulesAction != undefined && g_output_data.modulesAction != null && g_output_data.modulesAction != "" && g_output_data.modulesAction.modulesDict != null && g_output_data.modulesAction.modulesDict != null && g_output_data.modulesAction.modulesDict != "") {
        sessionStorage.custNavLinks = JSON.stringify(g_output_data.modulesAction.modulesDict);
    } else {
        if (sessionStorage.isLoOptin == undefined || sessionStorage.isLoOptin == null || sessionStorage.isLoOptin == "" || sessionStorage.isLoOptin == "false") {
            loadCustNavLinks(g_output_data.customerNumber);
        }
    }
    loadLocationConfig();
}

var demoInsDoneClick = function (ctrl) {
    $('#' + ctrl).popup('close');
    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\')');
};

function getEmailUserAuthorization() {
    //$(document).ready(function () {
    var param = queryString.substring(queryString.indexOf('=') + 1);
    var email_auth_url = serviceURLDomain + "api/Encryption_decQs"
    var email_url_post_info = { "qs": param };
    postCORS(email_auth_url, JSON.stringify(email_url_post_info), function (response_data) {
        sessionStorage.authString = 'Basic ' + response_data.s;
        sessionStorage.username = decodeBase64(response_data.s).split(':')[0];
        homePage = response_data.p;
        if (homePage.toLowerCase().indexOf('index') > -1) {
            homePage = "index.htm";
            getUserAuthenticate();
        }
        else {
            sessionStorage.approveProductionInfo = JSON.stringify(response_data);

            if (response_data.p.indexOf('index') == -1) {
                var page_name = response_data.p;
                homePage = page_name.substring(0, page_name.indexOf('.')) + '/' + page_name.substring(0, page_name.indexOf('.')) + '.html';
            }
            else
                homePage = response_data.p;
            getUserAuthenticate();
        }

        //        var tagged_info = response_data;
        //        if (response_data == "success") {
        //            $('#alertmsg').text("Job has been tagged/assigned successfully.");
        //            $('#popupDialog a[id=okBut]').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
        //            $('#popupDialog').popup('open');
        //        }

    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
    //});
}

function getUserAuthenticate() {
    getCORS(serviceURLDomain + "api/Authorization", null, function (data) {
        if (data.Message != undefined) {
            $('#alertmsg').text(data.Message);
            $('#popupDialog').popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');");
            return false;
        }
        else {
            //if ($('[name=uid_r]').val().toLowerCase() == "admin_sk" || $('[name=uid_r]').val().toLowerCase() == "user_sk" || $('[name=uid_r]').val().toLowerCase() == "power_sk") {
            //    $.getJSON("../JSON/_newCustomerLogin.JSON", function (data) {
            //        data.roleName = ($('[name=uid_r]').val().toLowerCase().indexOf('admin') > -1) ? "admin" : (($('[name=uid_r]').val().toLowerCase().indexOf('power') > -1) ? "power" : ($('[name=uid_r]').val().toLowerCase().indexOf('user') > -1) ? "user" : "");
            //        //if (data.customerNumber == OH_CUSTOMER_NUMBER && data.roleName == "user") {
            //        //    delete data["display_page_proofing"];
            //        //}
            //        captureAndNavigate(data);
            //    });
            //}
            //else
            if (Object.keys(data).length != 0 && data.companyName != undefined && data.companyName != null && data.companyName != "" && data.companyName.toLowerCase() == "unknown") {
                $('#alertmsg').text("Unknown error: Contact a Site Administrator");
                $('#popupDialog').popup('open');
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                return false;
            }
            else {
                //if (data.customerNumber == CASEYS_CUSTOMER_NUMBER) {
                //    $.each(data, function (key, val) {
                //        if (key.indexOf('display_page_') > -1 && key.indexOf('_selectTemplate') == -1)
                //            delete data[key];
                //    });
                //}

                //if ($('[name=uid_r]').val().toLowerCase() == "admin_rp" || $('[name=uid_r]').val().toLowerCase() == "user_rp" || $('[name=uid_r]').val().toLowerCase() == "power_rp") {
                //    sessionStorage.authString = makeBasicAuth($('[name=uid_r]').val(), $('[name=pwd_r]').val());
                //    data.customerNumber = REDPLUM_CUSTOMER_NUMBER;
                //    data.companyName = "Red Plum Inside Shopper";
                //    data.facility_id = "1";
                //}
                if (data.customerNumber == CASEYS_CUSTOMER_NUMBER && (window.location.href.toLowerCase().indexOf('caseysgeneralstore') || window.location.href.toLowerCase().indexOf('tdlive'))) {
                    $.getJSON("../JSON/_caseysThermometersStatus.JSON", function (data) {
                        if (Object.keys(data).length > 0)
                            sessionStorage.caseysThermometersStatus = JSON.stringify(data);
                    });
                }
                captureAndNavigate(data);
            }
        }
    }, function (response_error) {
        showErrorResponseText(response_error, false);
    });
}

function captureAndNavigate(data) {
    var user_role = (data["roleName"].indexOf('_') > -1) ? data["roleName"].split('_')[0] : data["roleName"];
    var customer_number = data["customerNumber"];
    sessionStorage.userRole = user_role;
    sessionStorage.username = (sessionStorage.isEmailLogin != undefined && sessionStorage.isEmailLogin != null && sessionStorage.isEmailLogin != "") ? sessionStorage.username : $('[name=uid_r]').val();
    sessionStorage.companyName = data["companyName"].toLowerCase();
    sessionStorage.customerNumber = data["customerNumber"];
    sessionStorage.facilityId = (data.facility_id != undefined && data.facility_id != null && data.facility_id != "") ? data.facility_id : "1";

    if (sessionStorage.username.toLowerCase().indexOf('sl1') > -1)
        data.companyName = "Suddenlink Corporate";
    else if (sessionStorage.username.toLowerCase().indexOf('reg1') > -1)
        data.companyName = "Regis Corporation";

    saveSessionData("appPrivileges", JSON.stringify(data));
    var appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));

    if (data.companyName.toLowerCase() == 'mediaworks' && sessionStorage.username.toLowerCase() == 'admin_dw1') { //discussed with client, 'admin_dw1' user is not required now.  But, if we login with this user, we are getting errors as return JSON object from authorization web service not contains proper data.
        document.location.href = 'login.htm';
        return false;
    }

    if (data.customerNumber == BBB_CUSTOMER_NUMBER || data.customerNumber == JETS_CUSTOMER_NUMBER || data.customerNumber == CASEYS_CUSTOMER_NUMBER || data.customerNumber == CW_CUSTOMER_NUMBER || data.customerNumber == AAG_CUSTOMER_NUMBER || data.customerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || data.customerNumber == SANDIEGO_CUSTOMER_NUMBER || data.customerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || data.customerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || data.customerNumber == WALGREENS_CUSTOMER_NUMBER || data.customerNumber == MOTIV8_CUSTOMER_NUMBER || data.customerNumber == ALLIED_CUSTOMER_NUMBER || data.customerNumber == REDPLUM_CUSTOMER_NUMBER || data.customerNumber == SAFEWAY_CUSTOMER_NUMBER || data.customerNumber == LOWES_CUSTOMER_NUMBER || data.customerNumber == DCA_CUSTOMER_NUMBER || data.customerNumber == KUBOTA_CUSTOMER_NUMBER || data.customerNumber == GWA_CUSTOMER_NUMBER || data.customerNumber == OH_CUSTOMER_NUMBER || data.customerNumber == SK_CUSTOMER_NUMBER) {
        loadDemoHintsInfo();
        if (data.customerNumber == AAG_CUSTOMER_NUMBER && data.roleName == "power") {
            loadLoanOffiersList();
        }
    }
    window.setTimeout(function getPage() {
        if (data.companyName.toLowerCase().indexOf('mediaworks') > -1 && sessionStorage.userRole == 'admin') { //mail dated 13th Sep'13.. Need to redirect to status page if he is admin and mediaworks.
            document.location.href = 'processStatus/mediaworksProcessStatus.html';
            return false;
        }
        //    else if (data.companyName.toLowerCase().indexOf('mediaworks') && sessionStorage.userRole != 'admin') {
        //        document.location.href = 'fileUpload/mediaworksFileUpload.html';
        //        return false;
        //    }

        if (data.companyName.toLowerCase().indexOf('mediaworks') > -1) {
            saveSessionData("jobNumber", data.jobNumber.replace(".00", "")); // noticed that jobNumber coming as ".00" also like "1234.00".
            saveSessionData("publisherId", data.publisherId);
            document.location.href = appPrivileges.page_home;
        }
        else
            //        if (data["customerNumber"] == BBB_CUSTOMER_NUMBER)
            //            document.location.href = "index_bbb.htm"; // appPrivileges.page_home;
            //        else
            document.location.href = (sessionStorage.isEmailLogin != undefined && sessionStorage.isEmailLogin != null && sessionStorage.isEmailLogin != "") ? homePage : "index.htm";  //appPrivileges.page_home;
    }, (queryString != undefined && queryString != null && queryString != "" && queryString.toLowerCase().indexOf('s1=') > -1) ? 0 : 1300);
}

function toggleDemoHints(page_name) {
    if ($('#sldrShowHints').val() == "on") {
        sessionStorage.showDemoHints = "on";
        localStorage.doNotShowHintsAgain = "false";
        sessionStorage.isTemplateDemoHintsDisplayed = "false"; //Template Page

        //Locations
        sessionStorage.isLocationsDemoHintsDisplayed = "false";
        sessionStorage.isLocationOrderedFirstTime = "false";
        sessionStorage.isNewLocationSearchFirstTime = "false";
        sessionStorage.isMapDisplayed = "false";
        
        //List Upload
        sessionStorage.isListUploadDemoHintsDisplayed = "false";
        sessionStorage.isListFileUploadedFirstTime = "false";
        sessionStorage.isListUploadingHintsDisplayed = "false";

        //Artwork Upload
        sessionStorage.isArtUploadDemoHintsDisplayed = "false";
        sessionStorage.isArtFileSelectedFirstTime = "false";
        sessionStorage.isArtFileUploadedFirstTime = "false";

        sessionStorage.isMileStonesDemoHintsDisplayed = "false";

        //Submit Order
        sessionStorage.isSubmitOrderDemoHintsDisplayed = "false";
        sessionStorage.isListSelectionDemoHintsDisplayed = "false";
        sessionStorage.isPostalBoundarySelectionsHintsDisplayed = "false";
        sessionStorage.isSelectionMethodHintsDisplayed = "false";
        sessionStorage.isOptInStatusHintsDisplayed = "false";

        //PieceAttributes
        sessionStorage.isPieceAttributesDemoHintsDisplayed = "false";

        //Select Images
        sessionStorage.isSelectImagesDemoHintsDisplayed = "false";
        //Image Library
        sessionStorage.isImageLibraryDemoHintsDisplayed = "false";
        //Custom Text
        sessionStorage.isCustomTextDemoHintsDisplayed = "false";
        //Offer Setup
        sessionStorage.isOfferSetupDemoHintsDisplayed = "false";
        //List Selection
        sessionStorage.isStoreSelectedFirstTime = "false";
        sessionStorage.isCarrierRouteSelectedFirstTime = "false";
        sessionStorage.isPostalBoundarySelectedFirstTime = "false";
        sessionStorage.isDemographicFilterSelectedFirstTime = "false";

        //Approval
        sessionStorage.isApprovalDemoHintsDisplayed = "false";

        //LMA Setup
        sessionStorage.isLMADemoHintsDisplayed = "false";
        sessionStorage.isViewByCountyFirstTime = "false";
    }
    else {
        sessionStorage.showDemoHints = "off";
    }
    var dont_show_hints_again = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
    if (!dont_show_hints_again && sessionStorage.showDemoHints == "on") {
        if (page_name.toLowerCase().indexOf("selecttemplate") > -1) {
            $('#dvWelcomeDemoPopup').popup('close');
            $('#navHints').popup('open', { positionTo: '#navHint' });
        }
        else if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf("schedulemilestones") > -1) {
            $('#mileStoneHints input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#mileStoneHints').popup('open', { positionTo: '#ulMileStones' });
        }
        else if ((jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf("selectlocations") > -1) {
            $('#searchLocations input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#searchLocations').popup('open', { positionTo: '#dvExistingLocationsContainer' });
        }
        else if (page_name.toLowerCase().indexOf("uploadlist") > -1) {
            if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
                $('#uploadingListHints input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
                $('#uploadingListHints').popup('open', { positionTo: '#navHeader' });
            }
            else {
                $('#uploadListDemoHints input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
                $('#uploadListDemoHints').popup('open', { positionTo: '#navHeader' });
            }
        }
        else if (page_name.toLowerCase().indexOf("uploadartwork") > -1) {
            $('#uploadArtDemoHints input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#uploadArtDemoHints').popup('open', { positionTo: '#navHeader' });
        }
        else if (page_name.toLowerCase().indexOf("submitorder") > -1) {
            $('#approveEmailDemoHints input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#approveEmailDemoHints').popup('open', { positionTo: '#txtEmails' });
        }
        else if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf("listselection") > -1) {
            $('#storeSelection input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#storeSelection').popup('open', { positionTo: '#dvSelectedLocations' });
        }
        else if (page_name.toLowerCase().indexOf("optinstatus") > -1) {
            $('#statusSummary input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#statusSummary').popup('open', { positionTo: '#tblCheckOutrGrid' });
        }
        else if ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf("approval") > -1) {
            $('#markupAnnotations input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#markupAnnotations').popup('open', { positionTo: '#dvProofFileName left' });
        }
        else if (page_name.toLowerCase().indexOf("selectimages") > -1) {
            $('#selectImagesHints input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#selectImagesHints').popup('open', { positionTo: '#dvImages' });
        }       
        else if (page_name.toLowerCase().indexOf("customtext") > -1) {
            $('#customTextHints input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#customTextHints').popup('open', { positionTo: '#dvCmpInfo' });
        }
        else if (page_name.toLowerCase().indexOf("offersetup") > -1) {
            $('#offerSetupHints input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#offerSetupHints').popup('open', { positionTo: '#dvOfferSetup' });
        }
        else if (page_name.toLowerCase().indexOf("lmasetup") > -1) {
            $('#dvWelcomeDemoPopup input[type=checkbox]').attr('checked', dont_show_hints_again).checkboxradio('refresh');
            $('#dvWelcomeDemoPopup').popup('open', { positionTo: '#map' });
        }
    }
}

function setDemoHintsSliderValue() {
    var dont_show_again = (localStorage.doNotShowHintsAgain != undefined && localStorage.doNotShowHintsAgain != null && localStorage.doNotShowHintsAgain != "" && localStorage.doNotShowHintsAgain == "true") ? true : false;
    if (dont_show_again) {
        sessionStorage.showDemoHints = "off";
    }
    if ((sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on")) {
        $('#sldrShowHints').val('on');
    }
    else if (sessionStorage.showDemoHints == "off") {
        $('#sldrShowHints').val('off');
    }
}

function turnDemoHintsOffClick(ctrl) {
    if ($('#sldrShowHints').length == 0)
        sessionStorage.showDemoHints = "off";
    else
        $('#sldrShowHints').val('off').slider("refresh").trigger('change');
    $('#' + ctrl).popup('close');

}

function doNotShowMeThisAgainClick(event, curr_popup) {
    var ctrl = event.target || event.targetElement || event.currentTarget;
    if ($(ctrl).is(':checked')) {
        localStorage.setItem('doNotShowHintsAgain', "true");
        sessionStorage.showDemoHints = "off";
        setDemoHintsSliderValue();
        $('#sldrShowHints').slider("refresh");
        if (dontShowHintsAgain != undefined || dontShowHintsAgain != null) {
            dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
        }
    }
    $('#' + curr_popup).popup('close');
}

function loadDemoJobTicket() {
    //demo_job_ticket
    $.getJSON("../JSON/_alliedDemoJobTicket1.JSON", function (data) {
        demo_job_ticket = data;
    });
}

function loadLoanOffiersList() {
    $.getJSON("../JSON/_loanOfficers.JSON", function (data) {
        if (data.length > 0) {
            sessionStorage.loanOffiersList = JSON.stringify(data);
        }
    });
}


function getDemoJobTicketComponents(template_selected) {
    if (demo_job_ticket[template_selected] != undefined) {
        return demo_job_ticket[template_selected];
    }
    else return {};
}

function loadDemoHintsInfo() {
    var demo_hints_json = {};
    $.getJSON("../JSON/_demoHintsInfo.JSON", function (data) {
        var temp_key = "";
        $.each(data, function (key, val) {
            if (Object.keys(val).length > 0) {
                temp_key = key;
                var temp_arr = {};
                $.each(val, function (key1, val1) {
                    if (key1.indexOf(sessionStorage.customerNumber) > -1) {
                        $.each(val1, function (key2, val2) {
                            if (key2 == "dvWelcomeMsg") {
                                if (sessionStorage.customerNumber == AAG_CUSTOMER_NUMBER && !is_valid_domain && sessionStorage.userRole == "power") {
                                    temp_arr[key2] = {
                                        "hintHeading": "<h2>How does Gizmo work?</h2>",
                                        "text1": "<p><b>Use this site to upload your Qualified and Unqualified lists for AAG</b></p>",
                                        "text2": "<p>For a new list you have, just click the \"Start Job\" link on this page. You'll be prompted to upload your file(s).</p>",
                                        "text3": "<p>That's it!</p>",
                                        "text4": "<p>Once your lists have been uploaded, the Gizmo system, will automatically run postal hygiene, address standardization, and postal presort logic on your uploaded list(s). Once these are complete, Gizmo will send you a final report for your review.</p>",
                                        "text5": "<p><b>Use this site to setup and manage your Lunch and Learn Opt-In program</b></p>",
                                        "text6": "<p>Click \"Start Job\" to get going. Then select the Lunch and Learn template from the template options. Helpful blue hint boxes will guide you through the process until your comfortable on your own.</p>",
                                        "text7": "<p>Need to check on an existing job? You'll find a list of your Open Jobs on the right. Clicking the job will take you to a job summary, from there you can click \"Edit Job\" to manage or make changes to a job in progress.</p>",
                                        "text8": "<p>Have problems or questions? Please contact your Tribune Direct representative.</p>"
                                    };
                                }
                                else
                                    temp_arr[key2] = val2;
                            }
                            else {
                                if (key2 == "introducingGizmo" && (is_valid_domain) && sessionStorage.customerNumber == AAG_CUSTOMER_NUMBER) { }
                                    //if (key2 == "introducingGizmo" &&  sessionStorage.userRole == "admin" &&sessionStorage.customerNumber == AAG_CUSTOMER_NUMBER) { }
                                else
                                    temp_arr[key2] = val2;
                            }
                        });
                    }
                });
                if (Object.keys(temp_arr).length > 0) {
                    demo_hints_json[temp_key] = temp_arr;
                    temp_arr = {};
                    temp_key = "";
                }
            }
        });
        if (Object.keys(demo_hints_json).length > 0)
            sessionStorage.demoHintsJson = JSON.stringify(demo_hints_json);
    });
}

function createDemoHints(page_name) {
    if (sessionStorage.demoHintsJson != undefined && sessionStorage.demoHintsJson != null && sessionStorage.demoHintsJson != "") {
        var page_demo_hints = {};
        page_demo_hints = $.parseJSON(sessionStorage.demoHintsJson);
        page_demo_hints = (page_demo_hints[page_name] != undefined && page_demo_hints[page_name] != null && page_demo_hints[page_name] != "") ? page_demo_hints[page_name] : {};
        $.each(page_demo_hints, function (key, val) {
            var temp_msgs = "";
            $.each(val, function (key1, val1) {
                temp_msgs += val1;
            });
            if (temp_msgs != "") {
                $('#' + key).html(temp_msgs);
                temp_msgs = "";
            }
        });
    }
}

function previewMailing() {
    $('#divError').text('');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    $('#loadingText1').text("Loading Preview.....");
    var file_name = "Beer By The Pier 11x5_5.pdf";
    var file_type = file_name.substring(file_name.lastIndexOf('.') + 1).toLowerCase();
    var content_type = null;
    switch (file_type) {
        case "pdf":
            content_type = 'application/pdf'
            break;
        case "jpg":
        case "jpeg":
        case "png":
        case "gif":
        case "tif":
        case "tiff":
            content_type = 'image/' + file_type
            break;
        case "svg":
            content_type = 'image/svg+xml'
            break;
    }
    //api/Gmc/{facility_id}/{customer_number}/{job_number}/{user_id}
    var preview_artwork_url = serviceURLDomain + "api/Gmc/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber + "/" + sessionStorage.username;
    var htmlText = null;
    if (content_type == 'application/pdf') {
        htmlText = "<embed class='ui-responsive'  style='width:850px;height:500px;'  type='" + content_type + "' src='" + preview_artwork_url + "' id='selectedFile'></embed>";
    } else {
        htmlText = "<img src='" + preview_artwork_url + "' id='selectedFile'></img>";
    }
    $('#popupPDFContent').html(htmlText);

    window.setTimeout(function getDelayNDisplayFile() {
        if ($('#selectedFile')[0].clientWidth > 0 && $('#selectedFile')[0].clientHeight > 0)
            var display = $("#popPDF");
        if (display != undefined) {
            display.scroll(function () {
                var ctrls = $("#popPDF img");
                console.log("************************");
                ctrls.each(function (index) {
                    console.log($(this).position().top);

                    if ($(this).position().top < 0)
                        $(this).css("visibility", "hidden")
                    else {
                        $(this).css("visibility", "")
                    }
                });
            });
        }
        $('#waitPopUp').popup('close');
        window.setTimeout(function getDelay() {
            $('#popPDF').popup('open', { positionTo: 'window' });
        }, 200);

    }, 5000);
}

//******** Global Constants *********//
var DCA_CUSTOMER_NUMBER = "100382"; // Dental Care Alliance
var SK_CUSTOMER_NUMBER = "293"; //SportsKing
var MOTIV8_CUSTOMER_NUMBER = "222"; //Motiv8
var WALGREENS_CUSTOMER_NUMBER = "123900"; //Walgreens
var REDPLUM_CUSTOMER_NUMBER = "153009"; //Redplum
var AAG_CUSTOMER_NUMBER = "6000420"; //American Advisor Group
var CW_CUSTOMER_NUMBER = "155370"; //Camping World
var REGIS_CUSTOMER_NUMBER = "154021"; //Regis Corp
var BRIGHTHOUSE_CUSTOMER_NUMBER = "99999"; //Bright House
var DATAWORKS_CUSTOMER_NUMBER = "999900"; //Data Works
var CASEYS_CUSTOMER_NUMBER = "155711"; //Casey's General Stores
var SUNTIMES_CUSTOMER_NUMBER = "4000021"; //Sun Times New Group
var BBB_CUSTOMER_NUMBER = "180"; //Bob’s Bait and Beer
var SPORTSAUTHORITY_CUSTOMER_NUMBER = "219"; //SPORTS AUTHORITY
var TRIBUNEPUBLISHING_CUSTOMER_NUMBER = "6000616"; //Tribune Publishing
var JETS_CUSTOMER_NUMBER = "189"; //Jets
var ACQUIREDHEALTH_CUSTOMER_NUMBER = "247"; //Acquire Health
var ALLIED_CUSTOMER_NUMBER = "100334"; //Allied
var SAFEWAY_CUSTOMER_NUMBER = "261"; //Safeway
var LOWES_CUSTOMER_NUMBER = "262"; //Lowes
var KUBOTA_CUSTOMER_NUMBER = "279"; //Kubota
var GWA_CUSTOMER_NUMBER = "284"; //GWA
var OH_CUSTOMER_NUMBER = "287"; //Orlando Health
var SANDIEGO_CUSTOMER_NUMBER = "6000608"; //San Diego Union-Tribune
//***** End *******//