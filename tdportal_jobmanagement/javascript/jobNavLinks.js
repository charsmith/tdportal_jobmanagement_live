﻿/*
FileName: jobNavLinks.js
path: /javascripts/jobNavLinks.js

The purpose of this page is to display the "Job" Navigation buttons in the job process pages.
Based on the user company and role, the buttons will be shown/hide.
This page is included in ALL the job*.html pages.
*/
var userRole = '';

var isNewAnalysis = '';
var isCurrent = false;
var displayPages = [];

userRole = getSessionData("userRole");
isNewAnalysis = getSessionData("isNewAnalysis");

getCustNavLinks();

function getCustNavLinks() {
    displayPages = [];
    var custNavLinksInfo = (sessionStorage.custNavLinks != undefined && sessionStorage.custNavLinks != null && sessionStorage.custNavLinks != "") ? jQuery.parseJSON(sessionStorage.custNavLinks) : appPrivileges;
    custNavLinksInfo = (custNavLinksInfo == undefined || custNavLinksInfo == null || Object.keys(custNavLinksInfo).length == 0) ? appPrivileges : custNavLinksInfo;


    if (jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER && custNavLinksInfo.dispaly_advPresortOptRows != undefined && custNavLinksInfo.dispaly_advPresortOptRows != null) custNavLinksInfo.dispaly_advPresortOptRows = false;
    if (jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER && appPrivileges.dispaly_advPresortOptRows != undefined && appPrivileges.dispaly_advPresortOptRows != null) appPrivileges.dispaly_advPresortOptRows = false;
    if (jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER) saveSessionData("appPrivileges", JSON.stringify(appPrivileges));
    if (custNavLinksInfo != undefined && custNavLinksInfo != null) {
        //var temp_cust_number = "";
        //temp_cust_number = (sessionStorage.custNavLinks != undefined && sessionStorage.custNavLinks != null && sessionStorage.custNavLinks != "") ? custNavLinksInfo.customerNumber : jobCustomerNumber;
        //if (temp_cust_number != CASEYS_CUSTOMER_NUMBER && appPrivileges.roleName == "user") delete custNavLinksInfo["display_page_approvalCheckout"]
        //if (temp_cust_number != CASEYS_CUSTOMER_NUMBER) {

        if (jobCustomerNumber == BBB_CUSTOMER_NUMBER) {
            delete custNavLinksInfo["display_page_selectLocations"];
        }
        if ((!is_valid_domain && jobCustomerNumber == AAG_CUSTOMER_NUMBER) || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
            //if (jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
            delete custNavLinksInfo["display_page_selectLocations"];
            delete custNavLinksInfo["display_page_uploadArtwork"];
            if (sessionStorage.isLoOptin != undefined && sessionStorage.isLoOptin != null && sessionStorage.isLoOptin != "" && sessionStorage.isLoOptin == "true") {
                delete custNavLinksInfo["display_page_selectTemplate"];
                delete custNavLinksInfo["display_page_uploadList"];
            }
        }
        if (jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER) {
            delete custNavLinksInfo["display_page_selectLocations"];
            delete custNavLinksInfo["display_page_milestones"];
            delete custNavLinksInfo["display_page_pieceAttributes"];
            delete custNavLinksInfo["display_page_instructions"];
            delete custNavLinksInfo["display_page_listSelection"];
            delete custNavLinksInfo["display_page_uploadArtwork"];
            delete custNavLinksInfo["display_page_finance"];
            delete custNavLinksInfo["display_page_approval"];
            delete custNavLinksInfo["display_page_logging"];
            delete custNavLinksInfo["display_page_submitOrder"];
        }
        if (jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER) {
            $.each(custNavLinksInfo, function (key, val) {
                if (key.toLowerCase().indexOf('display_page') > -1 && (key.toLowerCase() != 'display_page_selecttemplate' && key.toLowerCase() != 'display_page_submitorder')) {
                    delete custNavLinksInfo[key];
                    //$(this).removeItem;
                }
            });
        }
        if (jobCustomerNumber == CW_CUSTOMER_NUMBER) {
            delete custNavLinksInfo["display_page_instructions"];
            delete custNavLinksInfo["display_page_logging"];
        }
        if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
           // delete custNavLinksInfo["display_page_optInStatus"];
        }
        if (sessionStorage.jobNumber != undefined && sessionStorage.jobNumber != null && sessionStorage.jobNumber != "" && parseInt(sessionStorage.jobNumber) <= -1) {
            //var approval_page = {};
            //approval_page["page"] = "approval";
            //displayPages.splice(displayPages.length, 0, approval_page);
            if (jobCustomerNumber != CASEYS_CUSTOMER_NUMBER)
                delete custNavLinksInfo["display_page_approval"];
        }
        //}
        // if (jobCustomerNumber != "99997") {
        $.each(custNavLinksInfo, function (key, value) {
            if (key.indexOf('display_page_loadCurrentBuy') == -1 && key.indexOf('display_page_') != -1) {
                if ((jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) && (key.toLowerCase().indexOf('instructions') > -1 || key.toLowerCase().indexOf('logging') > -1)) { }
                else {
                    displayPages.push({
                        "page": key.substring(key.lastIndexOf('_') + 1)
                    });
                }
            }
        });
        //}

        //Need to be removed later
        if (jobCustomerNumber == SUNTIMES_CUSTOMER_NUMBER) {
            if (displayPages.length > 2) {
                displayPages.splice(0, 1);
                displayPages.splice(2, 6);
                //displayPages.splice(1, 6);
            }
            //        var job_materialsreceiving_page = {};
            //        job_materialsreceiving_page["page"] = "materialsReceiving";
            //        displayPages.splice(1, 0, job_materialsreceiving_page);

            var b = displayPages[1];
            displayPages[1] = displayPages[0];
            displayPages[0] = b;

            //        var job_reports_page = {};
            //        job_reports_page["page"] = "Reports";
            //        displayPages.splice(2, 0, job_reports_page);

            sessionStorage.navigationCount = 2;
        }
    }
    if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
        var proof_index = "";
        $.each(displayPages, function (key, val) {
            if (val.page.toLowerCase() == "companyinfo") {
                val.page = "customText"
            }
            if (val.page.toLowerCase() == "submitorder") {
                proof_index = key;
                return false;
            }
        });
        var job_proof_page = {};
        job_proof_page["page"] = "proofing";
        displayPages.splice((proof_index), 0, job_proof_page);
    }

    //if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
    //    var listSelection_index = "";
    //    $.each(displayPages, function (key, val) {
    //        if (val.page.toLowerCase() == "listselection") {
    //            listSelection_index = key;
    //            return false;
    //        }
    //    });
    //    //    var job_pattributes_page = {};
    //    //    job_pattributes_page["page"] = "pieceAttributes";
    //    //    displayPages.splice((milestone_index + 1), 0, job_pattributes_page);
    //    var job_email_page = {};
    //    job_email_page["page"] = "email";
    //    displayPages.splice((listSelection_index), 0, job_email_page);

    //    //    var job_submit_order_page = {};
    //    //    job_submit_order_page["page"] = "submitOrder";
    //    //    displayPages.splice(displayPages.length, 0, job_submit_order_page);
    //}
    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
        var gOut_put_data;
        var template_name = "";
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOut_put_data = $.parseJSON(sessionStorage.jobSetupOutput);
            template_name = gOut_put_data.templateName;
        }
        var listSelection_index = "";
        $.each(displayPages, function (key, val) {
            if (val.page.toLowerCase() == "listselection") {
                listSelection_index = key;
                return false;
            }
        });
        var upload_list_page = {};
        if ((jobCustomerNumber != SK_CUSTOMER_NUMBER) || (jobCustomerNumber == SK_CUSTOMER_NUMBER && userRole == "admin")) {
            upload_list_page["page"] = "uploadList";
            displayPages.splice((listSelection_index + 1), 0, upload_list_page);
        }
       
        if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            if (template_name != "" && template_name.indexOf('_11x6_') == -1 && jobCustomerNumber == SK_CUSTOMER_NUMBER) {
                var offer_setup_page = {};
                offer_setup_page["page"] = "offerSetup";
                displayPages.splice((listSelection_index), 0, offer_setup_page);


                var custom_text_page = {};
                custom_text_page["page"] = "customText";
                displayPages.splice((listSelection_index), 0, custom_text_page);
            }
            if (jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
                var select_images_page = {};
                select_images_page["page"] = "selectImages";
                displayPages.splice((listSelection_index), 0, select_images_page);
            }

        }
        else if (appPrivileges.roleName == "user") {
            var piece_attributes_list = "";
            $.each(displayPages, function (key, val) {
                if (val.page.toLowerCase() == "pieceattributes") {
                    piece_attributes_list = key;
                    return false;
                }
            });
            displayPages.splice(piece_attributes_list, 1);
        }

        $.each(displayPages, function (key, val) {
            if (val.page.toLowerCase() == "proofing") {
                listSelection_index = key;
                return false;
            }
        });
        var approval_page = {};
        approval_page["page"] = "approval";
        displayPages.splice((listSelection_index), 1, approval_page);
    }
    if (jobCustomerNumber == OH_CUSTOMER_NUMBER) {
        var proofing_index = "";
        $.each(displayPages, function (key, val) {
            if (val.page.toLowerCase() == "proofing") {
                proofing_index = key;
                return false;
            }
        });
        if (proofing_index != "") {
            var approval_page = {};
            approval_page["page"] = "approval";
            displayPages.splice((proofing_index), 1, approval_page);
        }
    }

    if (appPrivileges != undefined && appPrivileges != null && appPrivileges.roleName == "admin" && sessionStorage.jobNumber != "" && sessionStorage.jobNumber != "-1" && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER) {
        var submit_order_index = "";
        $.each(displayPages, function (key, val) {
            if (val.page.toLowerCase() == "submitorder") {
                submit_order_index = key;
                return false;
            }
        });
        //if (submit_order_index != "") {
        //    var job_logging_page = {};
        //    job_logging_page["page"] = "logging";
        //    displayPages.splice(submit_order_index + 1, 0, job_logging_page);
        //}
    }

    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName == "power" && !is_valid_domain) {
        //sessionStorage.jobNumber="-1";
        if (sessionStorage.jobNumber == "-1") {
            pages = ["selectTemplate", "selectLocations", "milestones", "pieceAttributes", "optInStatus"];
        }
        else {
            pages = ["selectTemplate", "selectLocations", "milestones", "pieceAttributes", "selectImages", "customText", "listSelection", "proofing", "optInStatus"];
        }
        displayPages = [];
        var page_obj = {};
        $.each(pages, function (key, val) {
            page_obj = {};
            page_obj["page"] = val;
            displayPages.push(page_obj);
        });
    }

    if (jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER && (userRole == "user" || userRole == "power")) {
        var index_upload_list = "";
        $.each(displayPages, function (key, val) {
            if (val.page.toLowerCase() == "pieceattributes") {
                index_upload_list = key;
                return false;
            }
        });
        displayPages.splice(index_upload_list, 1);
    }
    if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
        //var index_submit_order = "";
        //$.each(displayPages, function (key, val) {
        //    if (val.page.toLowerCase() == "submitorder") {
        //        index_submit_order = key;
        //        return false;
        //    }
        //});
        //var job_logoSelection_page = {};
        //job_logoSelection_page["page"] = "logoSelection";
        //displayPages.splice((index_submit_order), 0, job_logoSelection_page);

        //var job_offer_setup_page = {};
        //job_offer_setup_page["page"] = "offerSetup";
        //displayPages.splice((index_submit_order + 1), 0, job_offer_setup_page);

        //$.each(displayPages, function (key, val) {
        //    if (val.page.toLowerCase() == "baeselection") {
        //        val.page = "baseSelection";
        //        return false;
        //    }
        //});
    }
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName == "user" && !is_valid_domain && sessionStorage.isLoOptin != undefined && sessionStorage.isLoOptin != null && sessionStorage.isLoOptin != "" && sessionStorage.isLoOptin == "true") {
        var index_select_location = "";
        $.each(displayPages, function (key, val) {
            if (val.page.toLowerCase() == "selectlocations") {
                index_select_location = key;
                return false;
            }
        });
        var opt_in_selection_page = {};
        opt_in_selection_page["page"] = "Opt-In Selection";
        displayPages.splice(index_select_location, 0, opt_in_selection_page);
    }

    //if (displayPages.length > 0 && (sessionStorage.isOpenedFromEmail == undefined && sessionStorage.isOpenedFromEmail == null) && (sessionStorage.jobNumber != undefined && sessionStorage.jobNumber != "-1" && (jobCustomerNumber != BBB_CUSTOMER_NUMBER && appPrivileges.roleName != "user"))) {
    if (displayPages.length > 0 && (sessionStorage.isOpenedFromEmail == undefined && sessionStorage.isOpenedFromEmail == null) && (sessionStorage.jobNumber != undefined && sessionStorage.jobNumber != "-1") && (((jobCustomerNumber == BBB_CUSTOMER_NUMBER) && appPrivileges.roleName != "user") || (jobCustomerNumber != BBB_CUSTOMER_NUMBER && (jobCustomerNumber != AAG_CUSTOMER_NUMBER || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName == "power" && !is_valid_domain)) && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER))) {
        //if (displayPages.length > 0 && (sessionStorage.isOpenedFromEmail == undefined && sessionStorage.isOpenedFromEmail == null) && (sessionStorage.jobNumber != undefined && sessionStorage.jobNumber != "-1") && (((jobCustomerNumber == BBB_CUSTOMER_NUMBER) && appPrivileges.roleName != "user") || (jobCustomerNumber != BBB_CUSTOMER_NUMBER && (jobCustomerNumber != AAG_CUSTOMER_NUMBER) && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER))) {
        var job_summary_page = {};
        job_summary_page["page"] = "Job Summary";
        displayPages.splice(0, 0, job_summary_page);
    }
}


function getPageDisplayName(page_name) {
    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
    var page_display_name = '';
    $.each(new String(page_name), function (key, value) {
        if (key > 0 && value.match(PATTERN)) {
            page_display_name += ' ' + value;
        }
        else {
            page_display_name += value;
        }
    });

    if ((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || ((sessionStorage.custNavLinks == undefined || sessionStorage.custNavLinks == null || sessionStorage.custNavLinks == "") && jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER)) && page_name.toLowerCase().indexOf('management') > -1) page_display_name = "Upload List";
    //if ((jobCustomerNumber == CW_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf('approval') > -1) page_display_name = "Checkout & Approval";
    return page_display_name;
}

function buildPageLinks(page_name, page_short_name, on_click_function_name, link_page_name, page_display_name) {
    var nav_links;
    var on_click = "";
    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
        page_display_name = (page_display_name == "Job  Summary") ? "Summary" : page_display_name;
        page_display_name = (page_display_name == "Select Template") ? "Templates" : page_display_name;
        page_display_name = (page_display_name == "Select Locations") ? "Locations" : page_display_name;
        page_display_name = (page_display_name == "Upload Artwork") ? "Art Upload" : page_display_name;
        page_display_name = (page_display_name == "List Selection") ? "List" : page_display_name;
        page_display_name = (page_display_name == "Upload List") ? "List Upload" : page_display_name;
        page_display_name = (page_display_name == "Approval") ? "Proofing & Approval" : page_display_name;
    }
    if (jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER) {
        page_display_name = (page_display_name == "Select Template") ? "Templates" : page_display_name;
    }
    var spn_id = (page_name.toLowerCase() == "listmanagement" || page_name.toLowerCase() == "uploadlist") ? "uploadedListCount" : ((page_name.toLowerCase() == "selectlocations") ? 'locationsCount' : ((page_name.toLowerCase() == "uploadartwork") ? "uploadedArtCount" : ''));
    //if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && page_name.toLowerCase().indexOf('approvalcheckout') > -1)
    if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf('optinstatus') > -1)
        page_link = '../optInStatus/' + link_page_name + '.html';
        //    else if (page_name.toLowerCase().indexOf('instructions') > -1)
        //        page_link = '../' + page_name + '/' + page_name + '.html';
    else if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && page_name.toLowerCase().indexOf('opt-in selection') > -1) {
        page_link = '../loanOfficersOptIn/' + link_page_name + '.html';
    }
    else {
        if (page_name == "logging")
            page_link = '../job' + link_page_name + '/job' + link_page_name + '.html';
        else if (page_name.toLowerCase() == "submitorder")
            page_link = '../jobSubmitOrder/job' + link_page_name + '.html';
        else if (page_name.toLowerCase() == 'instructions' || page_name.toLowerCase() == "selecttemplate" || page_name.toLowerCase() == "materialsreceiving" || page_name.toLowerCase() == "schedulemilestones" || page_name.toLowerCase() == "milestones" || page_name.toLowerCase() == "pieceattributes" || page_name.toLowerCase() == "selectlocations" || page_name.toLowerCase() == "adorder" || page_name.toLowerCase() == "uploadlist" || page_name.toLowerCase() == "jobsummary" || page_name.toLowerCase() == "uploadartwork" || page_name.toLowerCase() == "reports" || page_name.toLowerCase() == 'email' || page_name.toLowerCase() == 'customtext' || page_name.toLowerCase() == 'baseselection' || page_name.toLowerCase() == 'logoselection' || page_name.toLowerCase() == 'offersetup' || page_name.toLowerCase() == 'selectimages' || ((jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && page_name.toLowerCase() == 'proofing'))
            if (page_name.toLowerCase().indexOf("schedule") == -1 && page_name.toLowerCase() == "milestones")
                page_link = '../jobSchedule' + page_name + '/jobSchedule' + link_page_name + '.html';
            else
                page_link = '../job' + page_name + '/job' + link_page_name + '.html';
        else
            page_link = (page_name.toLowerCase().indexOf('approvalcheckout') > -1) ? '../jobApprovalCheckout/job' + link_page_name + '.html' : ((page_name.toLowerCase().indexOf('finance') > -1) ? '../jobFinance/job' + link_page_name + '.html' : ((page_name.toLowerCase().indexOf('approval') > -1) ? '../jobApproval/jobApproval.html' : ((page_name.toLowerCase().indexOf('summary') > -1) ? '../jobSummary/jobSummary.html' : '../pages/job' + link_page_name + '.html')));
    }
    //    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && page_name.toLowerCase().indexOf('listmanagement') > -1) {
    //        page_link = '../listSelection/jobListSelection.html';
    //    }
    if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf('listselection') > -1) {
        page_link = '../listSelection/jobListSelection.html';
        //on_click = "sessionStorage.removeItem('nextPageInOrder');getNextPageInOrder();"
    }
    if (jobCustomerNumber == "1" && page_name.toLowerCase().indexOf('proofing') > -1) {
        page_link = '../proofing/jobProofing.html';
    }
    if (jobCustomerNumber == "1" && page_name.toLowerCase().indexOf('mergepurge') > -1) {
        page_link = '../mergepurge/jobMergePurge.html';
    }
    if (page_name.toLowerCase().indexOf(page_short_name) > -1) {
        if (page_name.toLowerCase() == "uploadartwork" || page_name.toLowerCase() == "listmanagement" || page_name.toLowerCase() == "uploadlist") {// || page_name.toLowerCase() == "selectlocations"
            nav_links = '<li id="li' + link_page_name + '" data-theme="d" name="' + page_short_name + '"><a pageLink ="' + page_link + '" target="_self" onclick="navigateToPage(\'' + page_link + '\');">' + page_display_name + '<span class="ui-li-count" style="right:45px;float:right" id="' + spn_id + '">0</span></a></li>';
        }
        else {
            if (page_name.toLowerCase() == 'schedulemilestones') {
                nav_links = '<li id="li' + link_page_name + '" data-theme="d" name="' + page_short_name + '"><a pageLink ="' + page_link + '" target="_self" onclick="navigateToPage(\'' + page_link + '\');">Milestones </a></li>';
            }
            else
                nav_links = '<li id="li' + link_page_name + '" data-theme="d" name="' + page_short_name + '"><a pageLink ="' + page_link + '" target="_self" onclick="' + (((jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) && page_name.toLowerCase() == "proofing") ? "proofingClick()" : "navigateToPage(\'" + page_link + "\')") + '">' + page_display_name + '</a></li>';
        }
        isCurrent = true;
    }
    else {
        if (isCurrent && (page_name.toLowerCase().indexOf(page_short_name) == -1)) {
            on_click_event = "";
            if ((page_short_name.toLowerCase().indexOf('template') > -1 || page_short_name.toLowerCase().indexOf("materialsreceiving") > -1 || page_short_name.toLowerCase().indexOf("milestones") > -1 || page_short_name.toLowerCase().indexOf("pieceattributes") > -1 || page_short_name.toLowerCase().indexOf("customtext") > -1 || page_short_name.toLowerCase().indexOf("locations") > -1 || page_short_name.toLowerCase().indexOf("uploadlist") > -1 || page_short_name.toLowerCase().indexOf("artwork") > -1 || page_short_name.toLowerCase().indexOf("baseselection") > -1 || page_short_name.toLowerCase().indexOf("logoselection") > -1 || page_short_name.toLowerCase().indexOf("offersetup") > -1 || page_short_name.toLowerCase().indexOf("selectimages") > -1 || ((jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) && page_short_name.toLowerCase().indexOf("proofing") > -1)) && (on_click_function_name != "")) {//
                on_click_event = 'return pageObj.' + on_click_function_name + '(\'' + page_link + '\',\'' + '' + '\');';
            }
            else if (page_short_name.toLowerCase().indexOf('listselection') > -1) {
                on_click_event = 'return ' + on_click_function_name + '(\'' + page_link + '\',\'' + 'onlycontinue' + '\');';
            }
            else if (on_click_function_name != "") {
                on_click_event = 'return ' + on_click_function_name + '();';
            }
            else {
                on_click_event = "";
            }
            //on_click_event = ((page_name.toLowerCase() == "selecttemplate" || page_name.toLowerCase() == "schedulemilestones" || page_name.toLowerCase() == "pieceattributes" || page_name.toLowerCase() == "selectlocations") && (on_click_function_name != "")) ? 'return pageObj.' + on_click_function_name + '(\'' + page_link + '\',\'' + '' + '\');' : ((on_click_function_name != "") ? 'return ' + on_click_function_name + '();' : "");
        }
        else {
            if ((page_short_name.toLowerCase().indexOf('template') > -1 || page_short_name.toLowerCase().indexOf("materialsreceiving") > -1 || page_short_name.toLowerCase().indexOf("milestones") > -1 || page_short_name.toLowerCase().indexOf("pieceattributes") > -1 || page_short_name.toLowerCase().indexOf("locations") > -1 || page_short_name.toLowerCase().indexOf("uploadlist") > -1 || page_short_name.toLowerCase().indexOf("artwork") > -1 || page_short_name.toLowerCase().indexOf('email') > -1 || page_short_name.toLowerCase().indexOf("customtext") > -1 || page_short_name.toLowerCase().indexOf("baseselection") > -1 || page_short_name.toLowerCase().indexOf("logoselection") > -1 || page_short_name.toLowerCase().indexOf("offersetup") > -1 || page_short_name.toLowerCase().indexOf("selectimages") > -1 || ((jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) && page_short_name.toLowerCase().indexOf("proofing") > -1)) && (on_click_function_name != "")) {
                on_click_event = 'return pageObj.' + on_click_function_name + '(\'' + page_link + '\',\'' + '' + '\');';
            }
            else if (page_short_name.toLowerCase().indexOf('listselection') > -1) {
                on_click_event = 'return ' + on_click_function_name + '(\'' + page_link + '\',\'' + 'onlycontinue' + '\');';
            }
            else if (on_click_function_name != "") {
                on_click_event = 'return ' + on_click_function_name + ((on_click_function_name.toLowerCase().indexOf('(') > -1 && on_click_function_name.toLowerCase().indexOf(')') > -1) ? '' : '();');
            }
            else {
                on_click_event = "";
            }
            //on_click_event = ((page_name.toLowerCase() == "selecttemplate" || page_name.toLowerCase() == "schedulemilestones" || page_name.toLowerCase() == "pieceattributes" || page_name.toLowerCase() == "selectlocations") && (on_click_function_name != "")) ? 'return pageObj.' + on_click_function_name + '(\'' + page_link + '\',\'' + '' + '\');' : ((on_click_function_name != "") ? 'return pageObj.' + on_click_function_name + ((on_click_function_name.toLowerCase().indexOf('(') > -1 && on_click_function_name.toLowerCase().indexOf(')') > -1) ? '' : '();') : "");
        }

        if (page_name.toLowerCase() == "job summary") {
            //on_click_event = "var is_valid =" + on_click_event.substring(on_click_event.indexOf(' ') + 1) + " if(is_valid) redirectToSingleJobPage(); else return false;";
            on_click_event = "redirectToSingleJobPage()";
        }

        if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER)
            on_click_event = "sessionStorage.currentPage='" + page_link + "'; sessionStorage.removeItem('nextPageInOrder');getNextPageInOrder();" + on_click_event;

        if (page_name.toLowerCase() == "job summary") {
            var data_icon = 'data-icon="carat-u"';
            //page_link = ((sessionStorage.jobNumber != -1) ? "../pages/singleJob.htm" : ((jobCustomerNumber == 1) ? "../index.htm" : "../index_cw.htm"));
            page_link = ((sessionStorage.jobNumber != -1) ? "../jobSummary/jobSummary.html" : "../index.htm");
            var is_displayed = (sessionStorage.jobNumber != "-1") ? " style='display:block'" : " style='display:none'";
            nav_links = '<li id="liJobSummary" ' + data_icon + is_displayed + ' onclick="' + on_click_event + '"><a href="#" target="_self">' + page_display_name + '</a></li>';
        }
        else if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
            //            if (page_display_name == "Stores")
            //                nav_links = '<li id="li' + link_page_name + '" onclick="' + on_click_event + '"><a href="' + page_link + '" target="_self">Select Locations</a></li>';
            //            else 

            //if (page_name.toLowerCase() == "selectlocations")
            //    nav_links = '<li id="li' + link_page_name + '" onclick="' + on_click_event + '"><a href="' + page_link + '" target="_self">Select Locations<span class="ui-li-count" style="right:45px;float:right" id="' + spn_id + '">0</span></a></li>';
            //else
            if (page_name.toLowerCase() == "uploadlist" || page_name.toLowerCase() == "uploadartwork" || page_name.toLowerCase() == "listmanagement") {
                //nav_links = '<li id="li' + link_page_name + '" name="' + page_short_name + '"><a href="' + page_link + '" target="_self">' + page_display_name + '<span class="ui-li-count" style="right:45px;float:right" id="' + spn_id + '">0</span></a></li>';
                nav_links = '<li id="li' + link_page_name + '"><a href="' + page_link + '" target="_self">' + page_display_name + '<span class="ui-li-count" style="right:45px;float:right" id="' + spn_id + '">0</span></a></li>';
            }
            else {
                if (page_name.toLowerCase() == 'schedulemilestones') {
                    nav_links = '<li id="li' + link_page_name + '" onclick="' + on_click_event + '"><a href="' + page_link + '" target="_self">Milestones</a></li>';
                }
                else {
                    nav_links = '<li id="li' + link_page_name + '" onclick="' + on_click_event + '"><a href="' + page_link + '" target="_self" onclick="' + (((jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) && page_name.toLowerCase() == "proofing") ? "proofingClick()" : '') + '">' + page_display_name + '</a></li>';
                }
            }
        }
        else {
            if (page_name.toLowerCase() == "uploadartwork" || page_name.toLowerCase() == "listmanagement" || page_name.toLowerCase() == "uploadlist") { //|| page_name.toLowerCase() == "selectlocations"
                nav_links = '<li id="li' + link_page_name + '" onclick="' + on_click_event + '"><a href="' + page_link + '" target="_self">' + page_display_name + '<span class="ui-li-count" style="right:45px;float:right" id="' + spn_id + '">0</span></a></li>';
            }
            else {
                if (page_name.toLowerCase() == 'schedulemilestones') {
                    nav_links = '<li id="li' + link_page_name + '" onclick="' + on_click_event + '"><a href="' + page_link + '" target="_self">Milestones </a></li>';
                }
                else
                    nav_links = '<li id="li' + link_page_name + '" onclick="' + on_click_event + '"><a href="' + page_link + '" target="_self">' + page_display_name + '</a></li>';
            }
        }
    }
    return nav_links;
}

function navigateToPage(page_link) {
    window.location.href = page_link;
};

function proofingClick() {
    getNextPageInOrder();
    window.location.href = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
    return false;
}

function displayNavLinks() {
    var data_theme = 'a';
    var path_name = window.location.pathname;
    var page_name = path_name.substring(path_name.lastIndexOf('/') + 1);
    var btnSaveContinue = '<div class="ui-grid-d ui-responsive"  style="float:right;">';
    var btnExecute = "";
    var load_advt = getSessionData("LoadAdvertisers");
    var headerTextButtons = '';
    //for showing Save and Continue buttons at the top of each header navigation pages..   
    if (page_name.indexOf("SelectTemplate") > -1) {
        headerTextButtons = '<div class="ui-btn-right" data-role="controlgroup" data-type="horizontal" class="ui-btn-right"> ' +
                '<a id="btnSaveTop" name="btnSaveTop" data-role="button" data-icon="myapp-save" data-iconpos="notext" data-inline="true" title="Save" data-mini="true">Save</a>' +
                '<a id="btnContinueTop" title="Continue" data-role="button" data-icon="carat-r" data-theme="a" data-iconpos="notext" data-inline="true" data-mini="true">Continue</a> ' +
                '</div>';
        if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            if (page_name.indexOf('UploadArtwork') > -1) {
                btnSaveContinue += '<div class="ui-block-a" style="width:auto;"><table style="border-style:none;padding-top:5px;"><tr><td>';
            }
            else {
                btnSaveContinue += '<div class="ui-block-a" style="width:auto;"><table style="border-style:none;"><tr><td>';
            }
            btnSaveContinue += '<label for="sldrShowHints">Hints:&nbsp;</label></td><td><select data-role="slider" data-theme="b" id="sldrShowHints" onchange="toggleDemoHints(\'' + page_name + '\');"><option value="off">Off</option><option value="on" selected="">On</option></select></td></tr></table></div>';
        }
    }
    else if (page_name.indexOf("SelectLocations") > -1) {
        headerTextButtons = '<div class="ui-btn-right" data-role="controlgroup" data-type="horizontal" class="ui-btn-right"> ' +
                '<a id="btnSaveTop" name="btnSaveTop" data-role="button" data-icon="myapp-save" data-iconpos="notext" data-inline="true" title="Save" data-mini="true">Save</a>' +
                '<a id="btnContinueTop" title="Continue" data-role="button" data-icon="carat-r" data-theme="a" data-iconpos="notext" data-inline="true" data-mini="true">Continue</a> ' +
               // '<a href="#demoInstructions1" data-rel="popup" id="btnDemoInstructions" data-position-to="window" data-transition="pop" data-role="button" data-icon="myapp-help" data-iconpos="notext" data-inline="true" data-mini="true">Help Documentation</a>' +
                '</div>';
        if (jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            if (page_name.indexOf('UploadArtwork') > -1) {
                btnSaveContinue += '<div class="ui-block-a" style="width:auto;"><table style="border-style:none;padding-top:5px;"><tr><td>';
            }
            else {
                btnSaveContinue += '<div class="ui-block-a" style="width:auto;"><table style="border-style:none;"><tr><td>';
            }
            btnSaveContinue += '<label for="sldrShowHints">Hints:&nbsp;</label></td><td><select data-role="slider" data-theme="b" id="sldrShowHints" onchange="toggleDemoHints(\'' + page_name + '\');"><option value="off">Off</option><option value="on" selected="">On</option></select></td></tr></table></div>';

            btnSaveContinue += '<div style="width:auto;" class="ui-block-b"><a id="btnSave" name="btnSave" data-role="button" data-inline="true"  data-theme="a">Save</a></div>';
        }
        else if ((jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && page_name.indexOf("SubmitOrder") > -1) {
            if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && sessionStorage.userRole != "user") || jobCustomerNumber != CASEYS_CUSTOMER_NUMBER)
                headerTextButtons = '<div class="ui-btn-right" data-role="controlgroup" data-type="horizontal" class="ui-btn-right"> ' +
                        '<a id="btnSaveTop" name="btnSaveTop" data-role="button" data-icon="myapp-save" data-iconpos="notext" data-inline="true" title="Save" data-mini="true">Save</a>'
            '</div>';
        }
    } else {
        headerTextButtons = '<div class="ui-btn-right" data-role="controlgroup" data-type="horizontal" class="ui-btn-right">';
        //headerTextButtons += (((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "user") || (appPrivileges.customerNumber == "1" && page_name.indexOf('Email') > -1) || (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER)) ? '' : '<a id="btnContinueTop" title="Continue" data-role="button" data-icon="carat-r" data-theme="a" data-iconpos="notext" data-inline="true" data-mini="true">Continue</a> ';
        if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && sessionStorage.userRole != "user") || jobCustomerNumber != CASEYS_CUSTOMER_NUMBER)
            headerTextButtons += (((page_name.indexOf("SubmitOrder") > -1) && (jobCustomerNumber == AAG_CUSTOMER_NUMBER)) ? '<a id="btnPreviewMailing" name="btnSaveTop" data-role="button" data-icon="eye" data-iconpos="notext" data-inline="true" title="Preview Mailing" data-mini="true" data-bind="click:$root.previewMailing">Preview Mailing</a>' : '') + '<a id="btnSaveTop" name="btnSaveTop" data-role="button" data-icon="myapp-save" data-iconpos="notext" data-inline="true" title="Save" data-mini="true">Save</a>';

        headerTextButtons += ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) || (appPrivileges != undefined && appPrivileges.customerNumber == "1" && page_name.indexOf('Email') > -1) || (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER)) ? '' : '<a id="btnContinueTop" title="Continue" data-role="button" data-icon="carat-r" data-theme="a" data-iconpos="notext" data-inline="true" data-mini="true">Continue</a></div> ';

        if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            if (page_name.indexOf('UploadArtwork') > -1) {
                btnSaveContinue += '<div class="ui-block-a" style="width:auto;"><table style="border-style:none;padding-top:5px;"><tr><td>';
            }
            else {
                btnSaveContinue += '<div class="ui-block-a" style="width:auto;"><table style="border-style:none;"><tr><td>';
            }
            btnSaveContinue += '<label for="sldrShowHints">Hints:&nbsp;</label></td><td><select data-role="slider" data-theme="b" id="sldrShowHints" onchange="toggleDemoHints(\'' + page_name + '\');"><option value="off">Off</option><option value="on" selected="">On</option></select></td></tr></table></div>';
        }
        if (page_name.indexOf('optInStatus') > -1) {
            btnSaveContinue += '<div style="width:auto;" class="ui-block-c"><a href="#" data-role="button" id="btnSubmit" data-inline="true" data-mini="true" data-theme="a" onclick="return postJobSetUpData(\'submit\')">Close Ordering &amp; Submit Approved Orders</a></div>';
            btnSaveContinue += '<div style="width:auto;" class="ui-block-b"><a id="btnSave" name="btnSave" data-role="button" data-inline="true"  data-theme="a">Save</a></div>';
        }
        else if (page_name.indexOf('OptInStatus') == -1) {

            btnSaveContinue += ((jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && (page_name.indexOf('CustomText') > -1 || page_name.indexOf('BaseSelection') > -1 || page_name.indexOf('LogoSelection') > -1 || page_name.indexOf('SelectImages') > -1 || ((page_name.indexOf('SubmitOrder') > -1) && (jobCustomerNumber != AAG_CUSTOMER_NUMBER)) || page_name.indexOf('OfferSetup') > -1)) ? '<div style="width:auto;" class="ui-block-d" ><a id="btnPreviewMailing" name="btnPreviewMailing" data-role="button" data-inline="true" title="Preview Mailing"  data-theme="a" data-bind="click:$root.previewMailing;">Preview Mailing</a></div>' : '';

            //btnSaveContinue += ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER) ? '<td align="right">' : '') + '<a id="btnSave" name="btnSave" data-role="button" data-inline="true"  data-theme="a">Save</a>' + ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER) ? '</td>' : '');
            btnSaveContinue += '<div style="width:auto;" class="ui-block-b"><a id="btnSave" name="btnSave" data-role="button" data-inline="true"  data-theme="a">Save</a></div>';
        }
    }
    $("#dvHeaderButtons").html(headerTextButtons);

    if (userRole == "user" && page_name.indexOf("SubmitOrder") > -1) {
        $('#btnContinueTop').css('visibility', 'hidden');
    }
    btnExecute = '<a id="btnExecute" name="btnExecute" data-role="button" data-theme="i" data-inline="true">Submit Approved Order for Execution</a>';
    if ((page_name.indexOf("SubmitOrder") == -1 && page_name.indexOf('optInStatus') == -1 && page_name.indexOf('ListSelection') == -1)) {
        //btnSaveContinue += ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || (((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin") || ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "user" && page_name.indexOf("UploadList") == -1))) ? '<td align="right">' : '');
        //btnSaveContinue += ((page_name.indexOf('Email') == -1 && (jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER)) || (page_name.indexOf('Email') > -1 && jobCustomerNumber == JETS_CUSTOMER_NUMBER) || ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin") || ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "user" && page_name.indexOf("UploadList") == -1)) ? '<a id="btnContinue" name="btnContinue" data-role="button" data-icon="carat-r" data-iconpos="right" data-theme="a" data-inline="true">Continue</a>' : '';
        if (page_name.indexOf('SelectTemplate') > -1)
            btnSaveContinue += ((page_name.indexOf('Email') == -1 && (jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER)) || (page_name.indexOf('Email') > -1 && jobCustomerNumber == JETS_CUSTOMER_NUMBER) || ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && page_name.indexOf("UploadList") == -1)) ? '<div style="width:auto;" class="ui-block-e" ><a id="btnSave" name="btnSave" data-role="button" data-inline="true"  data-theme="a">Save</a><a id="btnContinue" name="btnContinue" data-role="button" data-icon="carat-r" data-iconpos="right" data-theme="a" data-inline="true">Continue</a></div>' : '';
        else
            btnSaveContinue += ((page_name.indexOf('Email') == -1 && (jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER)) || (page_name.indexOf('Email') > -1 && jobCustomerNumber == JETS_CUSTOMER_NUMBER) || ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && page_name.indexOf("UploadList") == -1)) ? '<div style="width:auto;" class="ui-block-e" ><a id="btnContinue" name="btnContinue" data-role="button" data-icon="carat-r" data-iconpos="right" data-theme="a" data-inline="true">Continue</a></div>' : '';
        //btnSaveContinue += ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || (((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "admin") || ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "user" && page_name.indexOf("UploadList") == -1))) ? '</td></tr></table>' : (((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "user") ? '</td></tr></table>' : ''));
    }
    else if (page_name.indexOf("SubmitOrder") > -1 && (jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER) && userRole == "admin") {
        btnSaveContinue += '<div style="width:auto;" class="ui-block-e"><a id="btnContinue" name="btnContinue" data-role="button" data-icon="carat-r" data-iconpos="right" data-theme="a" data-inline="true">Continue</a></div>';
    }
    else if (page_name.indexOf("ListSelection") > -1) {
        btnSaveContinue += '<div style="width:auto;" class="ui-block-e"><a id="btnContinue" name="btnContinue" data-role="button" data-icon="carat-r" data-iconpos="right" data-theme="a" data-inline="true">Continue</a></div>';
    }
    btnSaveContinue += '</div>';
    $("#divBottom").html(btnSaveContinue);
    if (page_name.indexOf("SubmitJob") > -1) {
        $('#btnSave').css('visibility', 'hidden');
        $('#btnContinue').css('visibility', 'hidden');
    }
    else if (((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && page_name.indexOf("Approval") > -1)) {// || page_name.indexOf("Finance") > -1
        $("#divBottom").html(btnExecute + btnSaveContinue);
        (sessionStorage.jobNumber == '-1') ? $('#btnSave').text("Place Order") : $('#btnSave').text("Update Order")
        //$('#btnSave').text("Submit");
        $('#btnContinue').css('display', 'none');
        $('#btnExecute').text("Submit Approved Order for Execution");
    }
    else if (page_name.indexOf("SpecialInstructions") > -1) {
        if (jobCustomerNumber == "1" && (userRole == "user" || userRole == "power")) {
            $('#btnContinue').css('visibility', 'hidden');
        }
    }
    else if (page_name.indexOf("AdOrder") > -1) {
        $('#btnContinue').css('display', 'none');
        $('#btnSave').css('visibility', 'hidden');
    }
    //else if (page_name.indexOf("Template") > -1) {//|| page_name.indexOf("Locations") > -1
    //    $('#btnSave').css('visibility', 'hidden');
    //}
    var load_advt = getSessionData("LoadAdvertisers");

    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
        if (sessionStorage.userRole != "admin") //for CGS and normal user should not get Save button in any of the pages
            $('#btnSave').css('display', 'none');
        //$('#btnSave').css('visibility', 'hidden');
    }
    createNavLinks();
}

function createNavLinks() {
    var path_name = window.location.pathname;
    var page_name = path_name.substring(path_name.lastIndexOf('/') + 1);
    var display_navlinks = '';
    display_navlinks += '<li data-theme="a" data-icon="carat-l" id="liCloseMenu"><a onclick="closeNavPanel();" >Close Menu</a></li>';
    if (sessionStorage.isLoOptin == undefined || sessionStorage.isLoOptin == null || sessionStorage.isLoOptin == "false" || sessionStorage.isLoOptin == "") {
        display_navlinks += '<li data-icon="home" id="liHome" ><a href="#" onclick="redirectToIndexPage();">Home</a></li>';
    }
    $.each(displayPages, function (key, value) {
        var link_page_name = value.page.substring(0, 1).toUpperCase() + value.page.substring(1);
        var page_display_name = getPageDisplayName(link_page_name);
        if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) && value.page.toLowerCase().indexOf('optinstatus') > -1) link_page_name = "optInStatus";
        if ((jobCustomerNumber == AAG_CUSTOMER_NUMBER) && value.page.toLowerCase().indexOf('opt-in selection') > -1) link_page_name = "loanOfficersOptIn";
        switch (page_name.toLowerCase()) {
            case "jobselectlocations.html":
                display_navlinks += buildPageLinks(value.page, 'locations', 'checkLocationsSelected', link_page_name, page_display_name);
                break;
            case "jobstores.html":
                display_navlinks += buildPageLinks(value.page, 'stores', 'checkLocationsSelected', link_page_name, page_display_name);
                break;
            case "jobselecttemplate.html":
                display_navlinks += buildPageLinks(value.page, 'template', 'updateSelectedTemplate', link_page_name, page_display_name);
                break;
            case "jobschedulemilestones.html":
                display_navlinks += buildPageLinks(value.page, 'milestones', 'fnMilestonesValidation', link_page_name, page_display_name);
                break;
            case "jobpieceattributes.html":
                display_navlinks += buildPageLinks(value.page, 'pieceattributes', 'updatePieceAttributes', link_page_name, page_display_name);
                break;
            case "jobmergepurge.html":
                display_navlinks += buildPageLinks(value.page, 'mergepurge', '', link_page_name, page_display_name);
                break;
            case "joblistmanagement.html":
                display_navlinks += buildPageLinks(value.page, 'listmanagement', 'validateUploadingListFiles', link_page_name, page_display_name);
                break;
            case "jobuploadlist.html":
                display_navlinks += buildPageLinks(value.page, 'uploadlist', 'validateUploadingListFiles', link_page_name, page_display_name);
                break;
            case "jobuploadartwork.html":
                display_navlinks += buildPageLinks(value.page, 'artwork', 'validateUploadingArtFiles', link_page_name, page_display_name);
                break;
            case "jobapprovalcheckout.html":
                display_navlinks += buildPageLinks(value.page, 'approvalcheckout', "bindCheckoutData('validate')", link_page_name, page_display_name);
                break;
            case "jobsubmitorder.html":
                display_navlinks += buildPageLinks(value.page, 'submitorder', "", link_page_name, page_display_name);
                break;
            case "jobfinance.html":
                display_navlinks += buildPageLinks(value.page, 'finance', "", link_page_name, page_display_name);
                break;
            case "jobapproval.html":
                display_navlinks += buildPageLinks(value.page, 'approval', "", link_page_name, page_display_name);
                break;
            case "optinstatus.html":
                display_navlinks += buildPageLinks(value.page, 'optinstatus', '', link_page_name, page_display_name);
                break;
            case "joboutput.html":
                display_navlinks += buildPageLinks(value.page, 'output', 'updateSetupoutput', link_page_name, page_display_name);
                break;
            case "jobspecialinstructions.html":
                display_navlinks += buildPageLinks(value.page, 'specialinstructions', '', link_page_name, page_display_name);
                break;
            case "jobsubmitjob.html":
                display_navlinks += buildPageLinks(value.page, 'submitjob', '', link_page_name, page_display_name);
                break;
            case "joblogging.html":
                display_navlinks += buildPageLinks(value.page, 'logging', '', link_page_name, page_display_name);
                break;
            case "joblistselection.html":
                display_navlinks += buildPageLinks(value.page, 'listselection', 'validateForChangesAndSave', link_page_name, page_display_name);
                break;
            case "joblistmapping.html":
                display_navlinks += buildPageLinks(value.page, 'listselection', 'validateForChangesAndSave', link_page_name, page_display_name);
                break;
            case "jobinstructions.html":
                display_navlinks += buildPageLinks(value.page, 'instructions', '', link_page_name, page_display_name);
                break;
            case "jobadorder.html":
                display_navlinks += buildPageLinks(value.page, 'adorder', '', link_page_name, page_display_name);
                break;
            case "jobreports.html":
                display_navlinks += buildPageLinks(value.page, 'reports', '', link_page_name, page_display_name);
                break;
            case "jobmaterialsreceiving.html":
                display_navlinks += buildPageLinks(value.page, 'materialsreceiving', '', link_page_name, page_display_name);
                break;
            case "jobemail.html":
                display_navlinks += buildPageLinks(value.page, 'email', '', link_page_name, page_display_name);
                break;
            case "jobcustomtext.html":
                display_navlinks += buildPageLinks(value.page, 'customtext', 'confirmContinue', link_page_name, page_display_name);
                break;
            case "jobbaseselection.html":
                display_navlinks += buildPageLinks(value.page, 'baseselection', 'confirmContinue', link_page_name, page_display_name);
                break;
            case "joblogoselection.html":
                display_navlinks += buildPageLinks(value.page, 'logoselection', 'confirmContinue', link_page_name, page_display_name);
                break;
            case "joboffersetup.html":
                display_navlinks += buildPageLinks(value.page, 'offersetup', 'confirmContinue', link_page_name, page_display_name);
                break;
            case "jobselectimages.html":
                display_navlinks += buildPageLinks(value.page, 'selectimages', '', link_page_name, page_display_name);
                break;
            case "jobproofing.html":
                display_navlinks += buildPageLinks(value.page, 'proofing', '', link_page_name, page_display_name);
                break;
        }
    });

    if (sessionStorage.userRole == "admin")
        display_navlinks += '<li  id="liCustomerNumber" ><br /><b>Customer Number: ' + jobCustomerNumber + '</b></li>';
    $("#ulJobLinks").empty();
    $("#ulJobLinks").append(display_navlinks);
    window.setTimeout(function getNavLinks() {
        $("#ulJobLinks").listview('refresh');
    }, 30);
    getLiElements();
    window.setTimeout(function setDelay() {
        if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER)
            $('#liApprovalCheckout').find('a').removeAttr('href');
        if ($('#liCustomerNumber').hasClass('ui-btn-up-c'))
            $('#liCustomerNumber').removeClass('ui-btn-up-c')
        if (jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER)
            getNextPageInOrder();
    }, 2000);
    displayJobInfo();
    $('div[data-role=footer]').attr('data-tap-toggle', "false");
}


function createNavLinksForProofing() {
    var proof_nav_links = '<li data-theme="a" data-icon="delete"><a href="#" data-rel="close" style="font-size: 14px" onclick="$(\'#navLinksPanel\').panel(\'close\');"><h3></h3></a></li>';

    $.each(displayPages, function (key, value) {
        var link_page_name = value.page.substring(0, 1).toUpperCase() + value.page.substring(1);
        var page_display_name = getPageDisplayName(link_page_name);

        if (value.page.toLowerCase() == "job summary")
            proof_nav_links += '<li><a href="#" onclick="navLinksClick(\'../jobSummary/jobSummary.html\')">' + page_display_name + '</a></li>'
        else if (value.page.toLowerCase().indexOf('milestone') > -1) {
            proof_nav_links += '<li><a href="#" onclick="navLinksClick(\'../pages/jobScheduleMileStones.html\')">Milestones </a></li>';
        }
        else if (value.page.toLowerCase().indexOf('instructions') > -1)
            proof_nav_links += '<li><a href="#" onclick="navLinksClick(\'../jobInstructions/jobInstructions.html\')">Job Instructions</a></li>';
        else if (value.page.toLowerCase().indexOf('mapping') > -1)
            proof_nav_links += '<li><a href="#" onclick="navLinksClick(\'../listSelection/jobListSelection.html\')">' + page_display_name + '</a></li>';
        else if (value.page.toLowerCase().indexOf('approval') > -1 || value.page.toLowerCase().indexOf('finance') > -1)
            proof_nav_links += '<li><a href="#" onclick="navLinksClick(\'../job' + value.page + '/job' + value.page + '.html\')">' + page_display_name + '</a></li>';
        else if (value.page.toLowerCase().indexOf('locations') > -1 || value.page.toLowerCase().indexOf('stores') > -1)
            proof_nav_links += '<li><a href="#" onclick="navLinksClick(\'../pages/job' + value.page + '.html\')">Select Locations</a></li>';
            //        else if (value.page.toLowerCase().indexOf('stores') > -1)
            //            proof_nav_links += '<li><a href="#" onclick="navLinksClick(\'../job' + value.page + '/job' + value.page + '.html\')">Select Locations</a></li>';
        else if (value.page.toLowerCase().indexOf('logging') > -1)
            proof_nav_links += '<li><a href="#" onclick="navLinksClick(\'../job' + value.page + '/job' + value.page + '.html\')">Logging</a></li>';
        else
            proof_nav_links += '<li><a  href="#" onclick="navLinksClick(\'../pages/job' + value.page + '.html\')">' + page_display_name + '</a></li>'

    });
    return proof_nav_links;
}


function hideLinksforUsers() {
    if (userRole == "user" || userRole == "power") {
        $("#liOutPut").remove();
        $("#liSubmitJob").remove();

        $("#dvAccountsReceivable").remove();
    }
}
function hideMileStonesControls() {
    // these will be used in Milestones page
    $("#liArrivalDate").remove();
    $("#liArtArrivalDate").remove();
    $("#liSaleStartDate").remove();
    $("#liSaleEndDate").remove();
}

//to display the Job Number and Name(add/edit mode) on the header section of each page.      
function displayJobInfo() {
    var jobNumber = '';
    var jobDesc = '';
    jobNumber = getSessionData("jobNumber");
    jobDesc = getSessionData("jobDesc");
    var neg_job_number = getSessionData("negJobNumber");

    if (jobNumber != undefined && jobDesc != undefined) {
        var header_text = jobDesc + ' | ' + jobNumber;
        if (neg_job_number != undefined && neg_job_number != null && neg_job_number != "")
            header_text += ' (' + neg_job_number + ')';
        $('#navHeader').html(header_text);
    }
    else {
        if (isNewAnalysis == "true")
            $('#navHeader').html("New Analysis");
        else {
            $('#navHeader').html("New Job");
        }
    }
}

function getLiElements() {
    var link_ids = $('#ulJobLinks li:not([id^=liClose]):not([id^=liHome]):not([id^=liCustomerNumber])');
    //var link_ids = $('#ulJobLinks li:not([id*=Summary])');
    var liIds = link_ids.map(function (i, n) {
        if ($(n)[0].attributes["name"] != undefined && $(n)[0].attributes["name"].value != "") {
            if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) { // for cgs
                if ($(n)[0].attributes["name"].value == "listmanagement") {
                    $("#btnSaveTop").show();
                }
            }
            else {
                if ($(n)[0].attributes["name"].value == "listmanagement" || $(n)[0].attributes["name"].value == "artwork" || $(n)[0].attributes["name"].value == "uploadlist") {
                    if ((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && $(n)[0].attributes["name"].value == "uploadlist" || $(n)[0].attributes["name"].value == "artwork") { // for cgs
                        $("#btnSave").show();
                        $("#btnSaveTop").show();
                    }
                    else {
                       // $("#btnSave").hide(); //hide save button for file upload pages
                        //$("#btnSaveTop").hide(); //hide save button for file upload pa
                    }
                }
            }
            var pages_length = displayPages.length;

            switch (jobCustomerNumber) {
                case "180": //Demo BBB customer
                case "6000420"://AAG
                case "6000616"://Tribune Publishing 
                case "6000608"://San Diego Union-Tribune
                case "247"://Acquire Health
                case MOTIV8_CUSTOMER_NUMBER:
                case "219":
                case WALGREENS_CUSTOMER_NUMBER: //Walgreens
                case DATAWORKS_CUSTOMER_NUMBER: //Media works tribune
                case REDPLUM_CUSTOMER_NUMBER://Red plum
                case SAFEWAY_CUSTOMER_NUMBER://Safeway
                case LOWES_CUSTOMER_NUMBER://Lowes
                case OH_CUSTOMER_NUMBER:
                    if (i <= 0 && sessionStorage.navGroupCount == undefined)
                        sessionStorage.navigationCount = 1;
                    else if ((i > 0 && i <= pages_length) && sessionStorage.navGroupCount > 1)
                        sessionStorage.navigationCount = pages_length;
                    break;
                case CASEYS_CUSTOMER_NUMBER: //casey's
                case JETS_CUSTOMER_NUMBER:
                    //                    if (i <= 0 && sessionStorage.navGroupCount == undefined)
                    //                        sessionStorage.navigationCount = 2;
                    //                    else if ((i > 0 && i <= 6) && sessionStorage.navGroupCount <= 6)
                    //                        sessionStorage.navigationCount = 6;
                    //                    break;
                case CW_CUSTOMER_NUMBER: //camping world
                case ALLIED_CUSTOMER_NUMBER: //Allied
                case DCA_CUSTOMER_NUMBER:
                case SK_CUSTOMER_NUMBER://SportsKing
                case KUBOTA_CUSTOMER_NUMBER://Kubota
                case GWA_CUSTOMER_NUMBER://GWA
                case BRIGHTHOUSE_CUSTOMER_NUMBER:    //Bright House
                case REGIS_CUSTOMER_NUMBER:
                case "203":
                case SUNTIMES_CUSTOMER_NUMBER:
                    if (i <= 0 && sessionStorage.navGroupCount == undefined)
                        sessionStorage.navigationCount = 2; else if ((i > 0 && i <= pages_length) && sessionStorage.navGroupCount > 0)
                            sessionStorage.navigationCount = pages_length;

                    //                    if (jobCustomerNumber == "203") {
                    //                        if (i <= 0 && sessionStorage.navGroupCount == undefined)
                    //                            sessionStorage.navigationCount = 1;
                    //                        else if ((i > 0 && i <= 5) && sessionStorage.navGroupCount == 5)
                    //                            sessionStorage.navigationCount = pages_length;
                    //                    }
                    //                  
                    //                    else {
                    //                        if (i <= 0 && sessionStorage.navGroupCount == undefined)
                    //                            sessionStorage.navigationCount = 2;
                    //                        else if ((i > 0 && i <= 8) && sessionStorage.navGroupCount == 8)
                    //                            sessionStorage.navigationCount = pages_length;
                    //                    }
                    break;
                    //                case SUNTIMES_CUSTOMER_NUMBER:        
                    //                    if (i <= 0 && sessionStorage.navGroupCount == undefined)        
                    //                        sessionStorage.navigationCount = 3;        
                    //                    else if ((i > 0 && i <= pages_length) && sessionStorage.navGroupCount > 1)        
                    //                        sessionStorage.navigationCount = pages_length;        
                    //                    break;        
                case "1": //Conversion Alliance
                    if (i <= 0 && sessionStorage.navGroupCount == undefined)
                        sessionStorage.navigationCount = 2;
                    else if ((i > 0 && i <= 6) && sessionStorage.navGroupCount == 6)
                        sessionStorage.navigationCount = 6;
                    else if (i >= 6 && sessionStorage.navGroupCount > 6)
                        sessionStorage.navigationCount = pages_length;
                    break;
            }

            if ((i + 1) < link_ids.length) {
                $("#btnSaveTop").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i + 1].id).find('a')[0].attributes["href"].value + "\", " + i + ",'save');");
                if (link_ids[i].id.toLowerCase().indexOf('company') > -1) {
                    $("#btnContinue").attr("onclick", "pageObj.confirmContinue(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i + 1].id).find('a')[0].attributes["href"].value + "\", " + i + ",'onlycontinue');");
                    $("#btnContinueTop").attr("onclick", "pageObj.confirmContinue(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i + 1].id).find('a')[0].attributes["href"].value + "\", " + i + ",'onlycontinue');");
                }
                else {
                    $("#btnContinue").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i + 1].id).find('a')[0].attributes["href"].value + "\", " + i + ",'onlycontinue');");
                    $("#btnContinueTop").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i + 1].id).find('a')[0].attributes["href"].value + "\", " + i + ",'onlycontinue');");
                }
                $("#btnSave").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i + 1].id).find('a')[0].attributes["href"].value + "\", " + i + ",'save');");
                $("#btnExecute").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", " + i + ",'submit');");
                //$("#btnExecute").attr("onclick", "goToNextPage(\"" + $('#' + $('#ulJobLinks li')[i].id).find('a')[0].attributes["href"].value + "\", \"" + $('#' + $('#ulJobLinks li')[i + 1].id).find('a')[0].attributes["href"].value + "\", " + i + ",'execute');");
            }
            else if (parseInt(i + 1) == parseInt(link_ids.length)) {
                if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && parseInt(link_ids.length == 1)) {
                    $("#btnContinue").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", " + i + ",'onlycontinue');");
                    $("#btnContinueTop").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", " + i + ",'onlycontinue');");
                }
                $("#btnSaveTop").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", " + i + ",'save');");
                //$("#btnSave").attr("onclick", "goToNextPage(\"" + $('#' + $('#ulJobLinks li')[i].id).find('a')[0].attributes["href"].value + "\", \"" + $('#' + $('#ulJobLinks li')[i].id).find('a')[0].attributes["href"].value + "\", " + i + ",'save');");

                //$("#btnSave").attr("onclick", "goToNextPage(\"" + $('#' + $('#ulJobLinks li')[i].id).find('a')[0].attributes["href"].value + "\", \"" + $('#' + $('#ulJobLinks li')[i].id).find('a')[0].attributes["href"].value + "\", " + i + ",'" + (($(n)[0].attributes["name"].value == 'approvalcheckout') ? 'submit' : 'save') + "');");
                $("#btnSave").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", " + i + ",'save');");
                //$("#btnExecute").attr("onclick", "goToNextPage(\"" + $('#' + $('#ulJobLinks li')[i].id).find('a')[0].attributes["href"].value + "\", \"" + $('#' + $('#ulJobLinks li')[i].id).find('a')[0].attributes["href"].value + "\", " + i + ",'execute');");
                $("#btnExecute").attr("onclick", "goToNextPage(\"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", \"" + $('#' + link_ids[i].id).find('a')[0].attributes["pageLink"].value + "\", " + i + ",'submit');");
            }
        }
        if (sessionStorage.navigationCount == undefined || sessionStorage.navigationCount == "" || sessionStorage.navigationCount == null)
            sessionStorage.navigationCount = 0;
        if ((i + 1) > sessionStorage.navigationCount && (sessionStorage.jobNumber == undefined || sessionStorage.jobNumber == "" || sessionStorage.jobNumber == null || sessionStorage.jobNumber == "-1")) {
            $(n).addClass('ui-disabled');
            $(n).attr('data-icon', "false");
        }
        return $(n).attr('id');
    });
}

function goToNextPage(current_page, page_name, i_count, click_type) {
    if (current_page.toLowerCase().indexOf('jobproofing') == -1) {
        sessionStorage.currentPage = page_name;
        sessionStorage.removeItem('nextPageInOrder');
        getNextPageInOrder();
    }
    //    if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == REGIS_CUSTOMER_NUMBER || jobCustomerNumber == "203") {
    //        if ((click_type == "save") && sessionStorage.userRole.toLowerCase() != "admin") {
    //            var job_ticket_info = $.extend(true, {}, jQuery.parseJSON(sessionStorage.jobSetupOutput));
    //            var job_checkout_info = {};
    //            job_checkout_info["selectedLocationsList"] = job_ticket_info.selectedLocationsAction.selectedLocationsList;
    //            job_checkout_info["isFirstClassMail"] = (job_ticket_info.mailstreamAction.mailClass != undefined && job_ticket_info.mailstreamAction.mailClass == "1") ? 1 : 0;
    //            job_checkout_info["jobTemplate"] = (job_ticket_info.templateName != undefined) ? job_ticket_info.templateName : ((job_ticket_info.template != undefined) ? job_ticket_info.template : '');
    //            job_checkout_info["mailstreamAction"] = job_ticket_info.mailstreamAction;
    //            postCORS(serviceURLDomain + "api/CheckOut/" + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + jQuery.parseJSON(sessionStorage.jobSetupOutput).jobNumber + '/asdf', JSON.stringify(job_checkout_info), function (response) {
    //                if (response.checkOutAction != undefined && response.checkOutAction.checkOutApproval != undefined && response.checkOutAction.checkOutApproval.isArtworkApproved != undefined && response.checkOutAction.checkOutApproval.isCountsApproved != undefined
    //            && response.checkOutAction.checkOutApproval.isArtworkApproved.toLowerCase() == "true" && response.checkOutAction.checkOutApproval.isCountsApproved.toLowerCase() == "true") {
    //                    $('#alertmsg').html("This order has been Approved for production and is no longer editable. If you have questions or require additional changes, please contact an Account Manager at <a href='mailto:CA-CampingTeam@conversionalliance.com'>CA-CampingTeam@conversionalliance.com</a>.");
    //                    sessionStorage.jobSetupOutput = sessionStorage.jobSetupOutputCompare;
    //                    if (window.location.href.indexOf('jobApprovalCheckout') == -1) {
    //                        var href_page = window.location.href;
    //                        $('#okBut').attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close'); window.location.href='" + href_page + "'");
    //                    }
    //                    $("#popupDialog").popup("open");
    //                    return false;
    //                }
    //                else {
    //                    saveJobOnNavigation(current_page, page_name, i_count, click_type);
    //                }
    //            }, function (error_response) {
    //                showErrorResponseText(error_response);
    //            });
    //        }
    //        else {
    //            saveJobOnNavigation(current_page, page_name, i_count, click_type);
    //        }
    //    }
    //    else {
    saveJobOnNavigation(current_page, page_name, i_count, click_type);
    //    }
}

function saveJobOnNavigation(current_page, page_name, i_count, click_type) {
    var return_value = true;
    switch (current_page.substring(current_page.lastIndexOf('/') + 1)) {
        case "jobSelectLocations.html":
        case "jobStores.html":
            return_value = pageObj.checkLocationsSelected(page_name, 'saveContinue', current_page, page_name, i_count, click_type);
            break;
        case "jobSelectTemplate.html":
            return_value = pageObj.updateSelectedTemplate();
            break;
        case "jobScheduleMilestones.html":
            return_value = pageObj.fnMilestonesValidation();
            break;
        case "jobPieceAttributes.html":
            return_value = pageObj.updatePieceAttributes();
            break;
        case "jobUploadArtwork.html":
            return_value = pageObj.validateUploadingArtFiles();
            if (return_value)
                sessionStorage.saveClick = true; // This is only upload list page when user clicks on save button (right top corner / bottom) to save merge/purge info.
            break;
        case "jobUploadList.html":
        case "jobListManagement.html":
            return_value = pageObj.validateUploadingListFiles();
            if (return_value)
                sessionStorage.saveClick = true; // This is only upload list page when user clicks on save button (right top corner / bottom) to save merge/purge info.
            break;
        case "jobApprovalCheckout.html":
        case "jobApproval.html":
            return_value = bindCheckoutData(click_type);
            //Validations to be defined
            break;
        case "jobOutPut.html":
            return_value = updateSetupOutput();
            //Validations to be defined
            break;
        case "jobSpecialInstructions.html":
            return_value = saveSpecialInstructions();
            break;
        case "jobSubmitJob.html":
            //Validations to be defined
            break;
        case "jobInstructions.html":
        case "jobAdOrder.html":
            //return_value = bindData();
            return_value = true;
            break;
        case "jobListSelection.html":
            return_value = validateForChangesAndSave(page_name, click_type);
            break;
        case "jobCustomText.html":
        case "jobBaseSelection.html":
        case "jobLogoSelection.html":
        case "jobOfferSetup.html":
            return_value = pageObj.confirmContinue(current_page, page_name, i_count, click_type);
            break;
    }
    navigateTo(return_value, current_page, page_name, i_count, click_type);
}


function navigateTo(return_value, current_page, page_name, i_count, click_type) {
    if (return_value) {
        switch (jobCustomerNumber) {
            case BBB_CUSTOMER_NUMBER: //Demo BBB customer
            case AAG_CUSTOMER_NUMBER://AAG
            case TRIBUNEPUBLISHING_CUSTOMER_NUMBER://Tribune Publishing 
            case SANDIEGO_CUSTOMER_NUMBER://San Diego Union-Tribune
            case ACQUIREDHEALTH_CUSTOMER_NUMBER://Acquire Health
            case SPORTSAUTHORITY_CUSTOMER_NUMBER://SA
            case WALGREENS_CUSTOMER_NUMBER: //Walgreens
            case MOTIV8_CUSTOMER_NUMBER:
            case DATAWORKS_CUSTOMER_NUMBER: //Media works tribune
            case REDPLUM_CUSTOMER_NUMBER: //Red Plum Inside Shopper
            case SAFEWAY_CUSTOMER_NUMBER://Safeway
            case LOWES_CUSTOMER_NUMBER://Lowes
                if (sessionStorage.navGroupCount == undefined && current_page.toLowerCase().indexOf('template') > -1)
                    sessionStorage.navGroupCount = 4;
                break;
            case OH_CUSTOMER_NUMBER:
                if (sessionStorage.navGroupCount == undefined && current_page.toLowerCase().indexOf('template') > -1)
                    sessionStorage.navGroupCount = 6;
                break;
                break;
            case CASEYS_CUSTOMER_NUMBER: //casey's
            case JETS_CUSTOMER_NUMBER:
                if (sessionStorage.navGroupCount == undefined && current_page.toLowerCase().indexOf('locations') > -1)
                    sessionStorage.navGroupCount = 5;
                break;
            case CW_CUSTOMER_NUMBER: //camping world
            case BRIGHTHOUSE_CUSTOMER_NUMBER: //Bright House
            case ALLIED_CUSTOMER_NUMBER: //Allied
            case DCA_CUSTOMER_NUMBER://DCA
            case SK_CUSTOMER_NUMBER://SportsKing
            case KUBOTA_CUSTOMER_NUMBER: //Kubota
            case GWA_CUSTOMER_NUMBER://GWA
            case REGIS_CUSTOMER_NUMBER:
            case "203":
            case SUNTIMES_CUSTOMER_NUMBER:
                if (jobCustomerNumber == "203") {
                    if (sessionStorage.navGroupCount == undefined && current_page.toLowerCase().indexOf('template') > -1)
                        sessionStorage.navGroupCount = 5;
                }
                else if (appPrivileges.customerNumber == SUNTIMES_CUSTOMER_NUMBER) {
                    if (sessionStorage.navGroupCount == undefined && current_page.toLowerCase().indexOf('materialsreceiving') > -1)
                        sessionStorage.navGroupCount = 5;
                }
                else {
                    if (sessionStorage.navGroupCount == undefined && current_page.toLowerCase().indexOf('locations') > -1)
                        sessionStorage.navGroupCount = 8;
                }
                break;
            case "1": //Conversion Alliance
                if (sessionStorage.navGroupCount == undefined && current_page.toLowerCase().indexOf('locations') > -1)
                    sessionStorage.navGroupCount = 6;
                else if (sessionStorage.navGroupCount == 6 && current_page.toLowerCase().indexOf('artwork') > -1)
                    sessionStorage.navGroupCount = 11;
                break;
        }

        if (click_type === "onlycontinue") {
            //if condition line TO BE removed once the approval checkout page is opened to REGIS - REGIS_CUSTOMER_NUMBER customer.
            if (!((jobCustomerNumber == REGIS_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf('jobapprovalcheckout') > -1))
                window.location.href = page_name;
            if ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && page_name.toLowerCase().indexOf('jobsubmitorder') > -1)
                window.location.href = page_name;
        }
        else {
            if (current_page.substring(current_page.lastIndexOf('/') + 1) == 'jobScheduleMilestones.html') {
                postJobSetUpData('save');
                window.setTimeout(function a() { }, 5000);
            }
            else {
                if (current_page.substring(current_page.lastIndexOf('/') + 1) != 'jobApprovalCheckout.html')
                    postJobSetUpData('save');
            }
        }
    }
};