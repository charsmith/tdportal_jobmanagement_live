/**
* This is for Cross-site Origin Resource Sharing (CORS) requests.
*
* Additionally the script will fail-over to a proxy if you have one set up.
*
* @param string   url      the url to retrieve
* @param mixed    data     data to send along with the get request [optional]
* @param function callback function to call on successful result [optional]
* @param string   type     the type of data to be returned [optional]
*/

var isMobile = false;
var mobile = ['iphone', 'ipad', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
for (var i in mobile)
    if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        isMobile = true;
    }

var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
function getCORS(url, data, callback, callbackError) {
    //alert(isSafari);
    //if (jQuery.browser.msie) {
    //    var myReq = new XMLHttpRequest();
    //    myReq.open('GET', url, false);
    //    //myReq.open('GET', url);
    //    myReq.setRequestHeader("Authorization", sessionStorage.authString);
    //    myReq.setRequestHeader("Content-type", "application/json");
    //    myReq.withCredentials = true;
    //    myReq.onload = function () {
    //        if (myReq.responseText.toLowerCase().indexOf('error:') == -1 && myReq.status == 200) {
    //            var JSON = $.parseJSON(myReq.responseText);
    //            if (JSON == null || typeof (JSON) == 'undefined') {
    //                JSON = (data != null) ? $.parseJSON(data.firstChild.textContent) : [];
    //            }
    //            callback(JSON, 'success', this);
    //        }
    //        else {
    //            callbackError(myReq);
    //        }
    //    };
    //    myReq.onerror = callbackError;
    //    myReq.send();
    //    setTimeout(function a() { }, 10000);
    //}
    //else {
    $.ajax({
        type: 'GET',
        url: url,
        processData: true,
        data: {},
        dataType: "json",
        //async: (!isMobile && isSafari) ? true : false,
        async: false,
        beforeSend: function (req) {
            req.setRequestHeader("Authorization", sessionStorage.authString);
            req.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
        },
        success: function (data) {
            if (data != undefined && data != null && (typeof (data) == "object" || typeof (data) == "number" || (typeof (data) != "number" && data.toLowerCase().indexOf('error:') == -1)))
                callback(data, 'success', this);
            else {
                if (typeof (data) == "string") {
                    var temp = data;
                    data = {};
                    data["responseText"] = temp;
                }
                callbackError(data);
            }
        },
        error: callbackError
    });
    //}
}
/**
* This method is for Cross-site Origin Resource Sharing (CORS) POSTs
*
* @param string   url      the url to post to
* @param mixed    data     additional data to send [optional]
* @param function callback a function to call on success [optional]
* @param string   type     the type of data to be returned [optional]
*/
function postCORS(url, data, callback, callbackError) {
    //if (jQuery.browser.msie) {
    //    var myReq = new XMLHttpRequest();
    //    myReq.open("POST", url);
    //    myReq.setRequestHeader("Authorization", sessionStorage.authString);
    //    myReq.setRequestHeader("Content-type", "application/json");
    //    myReq.send(data);
    //    setTimeout(function b() { }, 120000);
    //    myReq.onload = function () {
    //        if (myReq.responseText.toLowerCase().indexOf('error:') == -1 && myReq.status == 200) {
    //            var JSON = $.parseJSON(myReq.responseText);
    //            if (JSON == null || typeof (JSON) == 'undefined') {
    //				JSON = (data != null) ? $.parseJSON(data.firstChild.textContent) : [];
    //            }
    //            callback(JSON, 'success', this);
    //        }
    //        else {
    //            callbackError(myReq);
    //        }

    //    };
    //    myReq.onerror = callbackError;
    //} else {
    $.ajax({
        url: url,
        data: data,
        processData: true,
        dataType: 'json',
        async: false,
        //async: (!isMobile && isSafari) ? true : false,
        contentType: 'application/json; charset=utf-8',
        timeout: 120000,
        type: 'POST',
        beforeSend: function (req) {
            req.setRequestHeader("Authorization", sessionStorage.authString);
        },
        success: function (data) {
            if (data != undefined && data != null && (typeof (data) == "object" || typeof (data) == "number" || (typeof (data) != "number" && data.toLowerCase().indexOf('error:') == -1)))
                callback(data, 'success', this);
            else {
                if (typeof (data) == "string") {
                    var temp = data;
                    data = {};
                    data["responseText"] = temp;
                }
                callbackError(data, 'failed', this);
            }
        },
        error: function (data) {
            callbackError(data, 'failed', this);
        }
    });
    //}
}
