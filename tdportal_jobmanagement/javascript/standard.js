﻿var ie = (document.all && document.getElementById) ? 1 : 0;
var ns = (document.layers) ? 1 : 0;
var ns6 = (document.getElementById && !document.all) ? 1 : 0;

function showHideLayers(the_id,the_action){
    if (ie == 1 || ns6 == 1){
        document.getElementById(the_id).style.visibility=the_action;
    }else{
       if (the_action="hidden"){
            the_action="hide";
       }else{
        the_action="show";
       }
       document.layers[the_id].visibility=the_action;
    }
}

function SimpleSwap(el,which){
    var the_dir = el.getAttribute("src");
    the_dir = the_dir.substring(0, the_dir.lastIndexOf("/") + 1);
    if (which == null){
        var the_src = el.getAttribute("origsrc");
    }else{
        var the_src = el.getAttribute("src");
        the_src = the_src.substring(the_src.lastIndexOf("/") + 1,the_src.length);
        the_src = which + the_src.substring(the_src.lastIndexOf(".") + the_src.length);
    }
    el.src=the_dir + the_src;
}

function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2); i += 3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
