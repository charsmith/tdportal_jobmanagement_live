﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var gDataTarget;
var fileUploadURL = "";
var fileUploadTargetURL = serviceURLDomain + 'api/FileUploadMw_publisher/' + getSessionData("publisherId");
var dropDownJsonURL = 'JSON/_dropDowns.JSON';
var dropDownData;
var pubDialogType = '';
var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_mediaworksFileUpload').live('pagebeforecreate', function (event) {
    displayMessage('_mediaworksFileUpload');
    $('#okBut').attr('data-theme', 'i');
    confirmMessageCurrentBuy();
    fnConfirmSignout('_mediaworksFileUpload');
    fnCreateMWNavLinks('upload');

    $('#popupDialog').attr("data-rel", "dialog");
    $('#popupDialog').attr("data-dismissible", "false");
    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }

    if (sessionStorage.desktop) // desktop storage
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    else if (localStorage.mobile) // mobile storage
        return true;

    if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
        alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
        window.open('', '_self', '');
        window.close();
        return false;
    }
    if (appPrivileges.roleName != "admin")
        $("#divFileMap").hide();

    //load IFRAME for IE to upload files.
    loadUploadIFrame()
    if (sessionStorage["uploadEMail"] != undefined && sessionStorage["uploadEMail"] != null && sessionStorage["uploadEMail"] != "")
        $("#txtEmail").val(sessionStorage["uploadEMail"]);
    loadUploadTargets();
});

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_mediaworksFileUpload', function (event) {
    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        selectFile($(e.target))
    });
    $('#divHeader').html("File Upload for " + appPrivileges.publisherName + " Publications <BR/");
    //    $('#divSubHeader').html("Target: " + month[new Date().getMonth()] + ", " + new Date().getFullYear());
    makeGData();
    $.getJSON(dropDownJsonURL, function (data) {
        dropDownData = data;
        loadDropDowns(dropDownData);
    });

});


//******************** Model Creation Start **************************

// Define a "PublisherSet" class that tracks its own name and children, and has a method to add a new child
var Publication = function (publication_name, children, are_files_uploaded) {
    this.categoryName = publication_name;
    this.dataTheme = (are_files_uploaded) ? 'i' : '';
    this.dataCollapsedIcon = (are_files_uploaded) ? 'check' : '';
    this.id = publication_name.replace(/ /g, '');
    this.children = ko.observableArray(children);
}

function createViewModel() {
    var upload_model = [];
    var are_files_missing = true;
    $.each(gData, function (key, val) {
        var missing_file_items = jQuery.grep(val, function (obj) {
            return obj.origFileName === "";
        });
        $.each(val, function (a, b) {
            if (b.origFileName != "") {
                b["dataTheme"] = 'm';
                b["dataIcon"] = 'check';
            }
            else {
                b["dataTheme"] = 'c';
                b["dataIcon"] = 'myapp-upload';
            }
        });
        var publication = new Publication(key, val, ((missing_file_items.length > 0) ? false : true));
        upload_model.push(publication);
    });
    return upload_model;
}

//******************** Model Creation End **************************


//******************** Public Functions Start **************************
//Load dropdowns
function loadDropDowns(dropDownData) {
    $("#dropDownTemplate").tmpl(dropDownData.distributionLevels).appendTo("#ddlDistribution1");
    $("#ddlDistribution1").find('option[value=5]').remove();
    $('#ddlDistribution1').val('-1').selectmenu('refresh');
    $("#dropDownTemplate").tmpl(dropDownData.distributionLevels).appendTo("#ddlDistribution2");
    $('#ddlDistribution2').val('-1').selectmenu('refresh');
    $("#dropDownTemplate").tmpl(dropDownData.publicationTypes).appendTo("#ddlPublicationType");
    $('#ddlPublicationType').val('-1').selectmenu('refresh');
    $('#lblPublicationType').text('Not Selected');
}

function makeGData() {
    if (sessionStorage.jobNumber != undefined && sessionStorage.jobNumber != "")
        fileUploadURL = serviceURLDomain + 'api/FileUploadMw/' + getSessionData("publisherId") + "/" + sessionStorage.jobNumber;
    else
        fileUploadURL = serviceURLDomain + 'api/FileUploadMw/' + getSessionData("publisherId");

    getCORS(fileUploadURL, null, function (data) {
        gData = data;
        //ko.applyBindings(createViewModel(), $('#divList')[0]);
        $('#divList').empty();
        ko.cleanNode($('#divList')[0]);
        var model = createViewModel();
        ko.applyBindings(model, $('#divList')[0]);
        fillPublicationDDL();
        //displayList();
        $.each($('#divList'), function (obj) {
            $(this).collapsibleset();
        });
        $("#divList ul").each(function (i) {
            $(this).listview();
        });
    }, function (error_response) {
        showErrorResponseText(error_response,true);
    });

}

function fnOpenNewEditPubPop(type) {
    var clear_fields = true;
    if (pubDialogType != '')
        clear_fields = false;

    if (type.toLowerCase() == 'add') {
        $('#popupNewEditPub').find('h3').text('Add A New Publication');
        $('#dvTxtPublicationName').show();
        $('#dvDdlPublicationName').hide();
        $('#tdDeletePub').css('display', 'none');
        $('#dvButtonPad').attr('align', 'right');
        $("#dvLblPublicationType").hide();
        $("#dvDDLPublicationType").show();
    }
    else {
        $('#popupNewEditPub').find('h3').text('Edit Publication');
        $('#dvTxtPublicationName').hide();
        $('#dvDdlPublicationName').show();
        $('#tdDeletePub').css('display', 'block');
        $('#dvButtonPad').attr('align', 'center');
        $("#dvLblPublicationType").show();
        $("#dvDDLPublicationType").hide();
    }
    pubDialogType = type;

    $('#popupDialog').popup('close');
    if (clear_fields) {
        clearFields();
    }

    $('#popupNewEditPub').popup('open');
}
function fillPublicationDDL() {
    $("#ddlPublicationName").empty();
    $("#ddlPublicationName").append('<option value="-1">Select Publication</option>');
    $.each(gData, function (key, val) {
        if (key.toLowerCase() != "publisher files")
            $("#ddlPublicationName").append('<option  value="' + key.replace(/ /g, "") + '">' + key + '</option>');
    });
    $("#ddlPublicationName").val('-1');
    $("#ddlPublicationName").selectmenu('refresh');
}

//load the store locations...
function displayList() {
    $('#divList').empty();
    var counter = 0;
    var counter_general = 0;
    $.each(gData, function (key, val) {
        var control_to_add = '';
        var publisher_items = '';
        $.each(val, function (product_key, product_val) {
            counter_general += 1;
            publisher_items += "<li ";
            if (product_val.origFileName != "") {
                publisher_items += " data-icon='check' data-theme='g'><a onclick='fnpopUp(\"" + product_val.fileType + "\", \"" + key + "\");' data-rel='popup'>";
                counter += 1;
            }
            else
                publisher_items += " data-icon='myapp-upload'><a onclick='fnpopUp(\"" + product_val.fileType + "\", \"" + key + "\");' data-rel='popup'>";

            publisher_items += "<h3>" + product_val.fileType + "</h3>";
            if (product_val.origFileName != "")
                publisher_items += "<p>" + product_val.origFileName + "</p>";
            if (product_val.fileType.toLowerCase() === "current buy file")
                publisher_items += "<p>" + product_val.note + "</p></a><a href='#' onclick='fnRemoveCurrentBuy(\"" + key + "\");' data-rel='Delete' data-icon='delete' data-theme='b'>Remove Current Buy</a></li>";
            else
                publisher_items += "<p>" + product_val.note + "</p></a></li>";
        });

        if (counter === counter_general && counter != 0)
            control_to_add += "<div data-role='collapsible' data-collapsed='true' id='div" + key.replace(/ /g, "") + "' data-theme='g' data-collapsed-icon='check' data-expanded-icon='arrow-d'><h2>" + key + '</h2>';
        else
            control_to_add += "<div data-role='collapsible' data-collapsed='true' id='div" + key.replace(/ /g, "") + "'><h2>" + key + '</h2>';

        control_to_add += "<ul data-role='listview' data-split-icon='myapp-upload' data-split-theme='c'>";

        control_to_add += publisher_items;

        control_to_add += "</ul></div>";


        $('#divList').append(control_to_add);

        counter = 0
        counter_general = 0
    });
    $.each($('#divList'), function (obj) {
        $(this).collapsibleset();
    });
    $("#divList ul").each(function (i) {
        $(this).listview();
    });
}

function fnRemoveCurrentBuy(current_buy_publication) {
    var returnVal = $('#confirmMsg').html("Warning! This action is undoable!<br>If you delete the Current Buy from this publication, the Current Buy file type and all associated uploaded files will be deleted.");
    $('#confirmType').val('currentBuy');
    $('#removeVal').val(current_buy_publication);
    $('#popupConfirmDialog').popup('open');
    return false;
}

function fnConfirm() {

    if ($('#confirmType').val() == "currentBuy") {
        var gdata_publication = gData[$('#removeVal').val()];
        var index = 0;
        var gdata_currentbuy = jQuery.grep(gdata_publication, function (obj, key) {
            gdata_publication[key].hasChanged = -2;
        });
        $('#popupConfirmDialog').popup('close');

        fnSubmitJSON();
    }
    else if ($('#confirmType').val() == "deletePublication") {
        var gdata_publication = {};
        $.each(gData, function (key, val) {
            if (key.toLowerCase() == $('#removeVal').val().toLowerCase()) {
                $.each(val, function (key1, val1) {
                    val1.hasChanged = -1;
                });
            }
        });
        $('#popupConfirmDialog').popup('close');
        fnSubmitJSON();
    }
    else if ($('#confirmType').val() == "duplicatePublicationType") {
        $('#popupConfirmDialog').popup('close');
        fnAddPublication(true);
    }
}

function fnDeletePublication() {
    if ($("#ddlPublicationName").val() === "-1") {
        $('#popupNewEditPub').popup('close');
        $('#alertmsg').text("Please select 'Publication' to delete!");
        setTimeout(function getDelay1() { $('#popupDialog').popup('open'); }, 200);
        $("#okBut").attr("onclick", "$('#btnEditPub').trigger('click')");
        return false;
    }
    else {
        var returnVal = $('#confirmMsg').html("Warning! This action is undoable!<br>If you delete a publication, the publication and all uploaded files will be deleted.");
        $('#confirmType').val('deletePublication');
        $('#removeVal').val($("#ddlPublicationName option:selected").text());
        $('#popupNewEditPub').popup('close');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
        setTimeout(function getDelay() { $('#popupConfirmDialog').popup('open'); }, 500);
        return false;
    }
}

function confirmMessageCurrentBuy() {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupConfirmDialog" data-overlay-theme="a" data-theme="c" style="max-width:400px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<div id="confirmType"></div>';
    msg_box += '<div id="removeVal"></div>';
    msg_box += '<a href="#" data-role="button" data-mini="true" data-inline="true" data-theme="i" id="btnCancel" onclick="$(\'#popupConfirmDialog\').popup(\'close\');">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-mini="true" data-inline="true" data-theme="i" onclick="fnConfirm();" id="btnContinue">Continue</a>';
    msg_box += '</div></div>';
    $("#_mediaworksFileUpload").append(msg_box);
}

function fnpopUp(text, product_key) {
    $('#txtFileName').val('');
    if (!checkEmailAndFile()) return false;
    $("#popupFileUpload").popup("open");
    $("#headerMsg").html(text);
    $('#hidUploadFile').val(text + '|' + product_key);

    var gdata_file_type = jQuery.grep(gData[product_key], function (obj, key) {
        index = key;
        return (obj.fileType.toLowerCase() === text.toLowerCase());
    });
    $('#ddlCircDistribution1').text(getDistributionLevelName(gdata_file_type[0].distributionLevelId1));
    $('#ddlCircDistribution2').text(getDistributionLevelName(gdata_file_type[0].distributionLevelId2));
}

function getDistributionLevelName(level_id) {
    switch (level_id) {
        case 1:
            return "Zip";
            break;
        case 2:
            return "Zip/Zone";
            break;
        case 3:
            return "Sub-Zip";
            break;
        case 4:
            return "County";
            break;
        case 5:
            return "None";
            break;
        default:
            return "Not selected";
            break;
    }
}

function getPublicationType(pub_id) {
    switch (pub_id) {
        case 1:
        case "Home Delivery":
            return (typeof (pub_id) == "number") ? "Home Delivery" : 1;
            break;
        case 2:
        case "Single Copy":
            return (typeof (pub_id) == "number") ? "Single Copy" : 2;
            break;
        case 3:
        case "Total Market Coverage (TMC)":
            return (typeof (pub_id) == "number") ? "Total Market Coverage (TMC)" : 3;
            break;
        case 4:
        case "Hispanic Targeted":
            return (typeof (pub_id) == "number") ? "Hispanic Targeted" : 4;
            break;
        case 5:
        case "Sunday Select":
            return (typeof (pub_id) == "number") ? "Sunday Select" : 5;
            break;
        case 6:
        case "Select Market Coverage (SMC)":
            return (typeof (pub_id) == "number") ? "Select Market Coverage (SMC)" : 6;
            break;
        case -1:
        case "Not Selected":
            return (typeof (pub_id) == "number") ? "Not Selected" : -1;
            break;
    }
}

function fnCancel() {
    resetFileUploadForm();
    $('#popupFileUpload').popup('close');
}

function fnUpdatePublicationInfo(file_name, product_key, text) {
    var gdata_publication = gData[product_key];
    var index = 0;
    var gdata_currentbuy = jQuery.grep(gdata_publication, function (obj, key) {
        index = key;
        if (obj.fileType.toLowerCase() === text.toLowerCase()) {
            obj.origFileName = file_name;
            if (obj["jobNumber"] == undefined)
                obj["jobNumber"] = sessionStorage.jobNumber;
            else
                obj.jobNumber = sessionStorage.jobNumber;
            var date = new Date();
            obj.note = 'Successfully Loaded ' + date.getMonth() + "/" + date.getDay() + "/" + date.getFullYear();
        }
    });
    makeGData();
}

function validationForNewPub() {
    var message = "<ul>";
    if (pubDialogType == "add") {
        if ($.trim($('#txtPublication').val()) == "")
            message += "<li>Publication Name is mandatory.</li>";
        if ($("#ddlPublicationType").val() == "-1")
            message += "<li>Publication type is mandatory.</li>";
    }
    if ($("#ddlDistribution1").val() == "-1")
        message += "<li>Distribution Level 1 is mandatory.</li>";
    if ($("#ddlDistribution2").val() == "-1")
        message += "<li>Distribution Level 2 is mandatory.</li>";

    message += "</ul>";

    if (message != "<ul></ul>") {
        $('#popupNewEditPub').popup('close');
        $('#alertmsg').html(message);
        $('#popupDialog').popup('open');
        $("#okBut").attr("onclick", "fnOpenNewEditPubPop('" + pubDialogType + "');");
        return false;
    }
    return true;
}

function validationForPubType(type) {
    var ret_val = true;
    $.each(gData, function (key, val) {
        $.each(val[0], function (key1, val1) {
            if ((type == "add" && key1 == "productTypeId" && val1 == $("#ddlPublicationType").val()) ||
                (type == "edit" && key1 == "productTypeId" && val1 == getPublicationType($("#lblPublicationType").text()) && key != $("#ddlPublicationName").val())) {
                $('#confirmMsg').html("Only one publication per publication type may be added. Creating this publication will overwrite the name and settings for the previously created publication <b>'" + key + "'</b> .Do you want to continue?");
                $('#confirmType').val('duplicatePublicationType');
                $('#removeVal').val(type);

                if (type == "add") {
                    $('#popupNewEditPub').popup('close');
                }
                else {
                    $('#popupNewEditPub').popup('close');
                }
                $('#popupConfirmDialog').popup('open');
                ret_val = false;
            }
        });
    });
    return ret_val;
}

function fnAddPublication(is_continue) {

    if (pubDialogType == "add") {
        if (!validationForNewPub()) return false;

        var new_publication_name = $.trim($('#txtPublication').val().replace(/'/g, "`"));
        var pub_exists = false;

        $.each(gData, function (key, val) {
            if (key.toLowerCase() === new_publication_name.toLowerCase()) {
                pub_exists = true;
                return false
            }
        });

        if (pub_exists) {
            $('#popupNewEditPub').popup('close');
            $('#alertmsg').text("Duplicate publications are not allowed.");
            $('#popupDialog').popup('open');
            $("#okBut").attr("onclick", "$('#btnNewPub').trigger('click')");
            return false;
        }

        if (gData[new_publication_name] == undefined) {
            if (new_publication_name != "") {
                if (!is_continue)
                    if (!validationForPubType(pubDialogType)) return false;

                gData[new_publication_name] = [];
                gData[new_publication_name].push({
                    "publicationId": 0,
                    "productTypeId": $("#ddlPublicationType").val(),
                    "origFileName": "",
                    "fileMapName": "",
                    "fileTypeId": "1",
                    "fileType": "Circulation File",
                    "distributionLevelId1": $("#ddlDistribution1").val(),
                    "distributionLevelId2": $("#ddlDistribution2").val(),
                    "note": "Click to Upload File",
                    "email": "",
                    "hasChanged": 1,
                    "jobNumber": sessionStorage.jobNumber
                });
                gData[new_publication_name].push({
                    "publicationId": 0,
                    "productTypeId": $("#ddlPublicationType").val(),
                    "origFileName": "",
                    "fileMapName": "",
                    "fileTypeId": "2",
                    "fileType": "Projection File",
                    "distributionLevelId1": $("#ddlDistribution1").val(),
                    "distributionLevelId2": $("#ddlDistribution2").val(),
                    "note": "Click to Upload File",
                    "email": "",
                    "hasChanged": 1,
                    "jobNumber": sessionStorage.jobNumber
                });
                gData[new_publication_name].push({
                    "publicationId": 0,
                    "productTypeId": $("#ddlPublicationType").val(),
                    "origFileName": "",
                    "fileMapName": "",
                    "fileTypeId": "4",
                    "fileType": "Current Buy File",
                    "distributionLevelId1": $("#ddlDistribution1").val(),
                    "distributionLevelId2": $("#ddlDistribution2").val(),
                    "note": "Click to Upload File",
                    "email": "",
                    "hasChanged": 1,
                    "jobNumber": sessionStorage.jobNumber
                });
                $('#popupNewEditPub').popup('close');
            }
        }
    }
    else {
        if (gData[$("#ddlPublicationName option:selected").text()] != undefined) {

            if (!validationForNewPub()) return false;
            var gdata_projfile = jQuery.grep(gData[$("#ddlPublicationName option:selected").text()], function (obj, key) {
                gData[$("#ddlPublicationName option:selected").text()][key].distributionLevelId1 = $("#ddlDistribution1").val();
                gData[$("#ddlPublicationName option:selected").text()][key].distributionLevelId2 = $("#ddlDistribution2").val();
                gData[$("#ddlPublicationName option:selected").text()][key].productTypeId = getPublicationType($("#lblPublicationType").text());
                gData[$("#ddlPublicationName option:selected").text()][key].hasChanged = 1;
            });
            $('#popupNewEditPub').popup('close');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
        }
        else {
            $('#popupNewEditPub').popup('close');
            $('#alertmsg').text("Please select 'publication' to Edit!");
            $("#okBut").attr("onclick", "$('#btnEditPub').trigger('click')");
            $('#popupDialog').popup('open');
            return false;
        }
    }
    fnSubmitJSON();
    clearFields();
}

function clearFields() {
    $('#ddlDistribution1').val('-1').selectmenu('refresh');
    $('#ddlDistribution2').val('-1').selectmenu('refresh');
    $('#txtFileName').val('');

    if (pubDialogType == "add") {
        $('#txtPublication').val('');
        $('#ddlPublicationType').val('-1').selectmenu('refresh');
        $('#ddlDistribution2').val('5').selectmenu('refresh');
    }
    else {
        $('#ddlPublicationName').val('-1').selectmenu('refresh');
        $('#lblPublicationType').text(getPublicationType(-1));
    }
}

function fnDDLPubEditChange() {
    if (gData[$("#ddlPublicationName option:selected").text()] != undefined) {
        var gdata_projfile = jQuery.grep(gData[$("#ddlPublicationName option:selected").text()], function (obj, key) {
            return (obj.fileType.toLowerCase() === "projection file");
        });

        if (gdata_projfile != null && gdata_projfile != undefined && gdata_projfile.length > 0) {
            $("#ddlDistribution1").val((gdata_projfile[0].distributionLevelId1 == 0) ? "-1" : gdata_projfile[0].distributionLevelId1).selectmenu('refresh');
            $("#ddlDistribution2").val((gdata_projfile[0].distributionLevelId2 == 0) ? "-1" : gdata_projfile[0].distributionLevelId2).selectmenu('refresh');
            $("#lblPublicationType").text(getPublicationType(gdata_projfile[0].productTypeId));
        }
        else
            $("#lblPublicationType").text(getPublicationType(-1));

    }
    else {
        $("#ddlDistribution1").val("-1").selectmenu('refresh');
        $("#ddlDistribution2").val("-1").selectmenu('refresh');
        $("#lblPublicationType").text(getPublicationType(-1));
    }
}

function fnSubmitJSON() {
    var mw_json = gData;
    var post_url = serviceURLDomain + "api/FileUploadMw/" + getSessionData("publisherId") + "/bob";

    postCORS(post_url, JSON.stringify(mw_json), function (response) {
        makeGData();
        var msg = 'Data was saved.';
        if (response.replace(/"/g, "") != "success") {
            msg = "Data was not saved.";
        }
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    }, function (response_error) {
        //$('#alertmsg').text(response_error.responseText);
        //$('#popupDialog').popup('open');
        showErrorResponseText(response_error, false);
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    });
    pubDialogType = '';
}

var isComplete = false;
var xhr_req;
var is_aborted = false;
var percent_uploaded = 0;
function formSubmit(e) {
    var input_file = $(($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload')).find('input[type="file"]');
    number_of_files_uploading = input_file.length;
    isComplete = false;
    var start = '';
    var file_name = $(input_file).val();
    file_name = file_name.substring(file_name.lastIndexOf('\\') + 1);
    var publication_info = $('#hidUploadFile').val().split('|');
    e.preventDefault();
    var timer_id = 0;
    var time_stamp = Math.floor(+new Date().getTime() / 1000);
    var increment = 1;
    var timer = (function () {
        var timerElement;
        var timeoutRef;
        var percent_uploaded = 0;
        return {
            start: function (id) {
                if (id) {
                    timerElement = document.getElementById(id);
                }
                timer.run();
            },
            run: function () {
                if (timerElement) {
                    $('#dvProgressBar').popup('open', { positionTo: 'window' });
                    $('#fileName').text(number_of_files_uploading);
                    //Sets the style of progress bar and displays the percent uploaded
                    $('#uploadProgressBar').css('width', percent_uploaded + '%');
                    $('#percentUploaded').empty();
                    $('#percentUploaded').get(0).innerHTML = percent_uploaded + '%';
                }

                if (!isComplete) {
                    timeoutRef = setTimeout(timer.run, 500); //This run function will be called for every 2 secs.

                    //Web service call for progress bar data
                    var pbar_data_url = serviceURLDomain + "api/FileUpload/progress/" + sessionStorage.jobNumber + "^" + sessionStorage.username + "^" + time_stamp + "^1";

                    getCORS(pbar_data_url, null, function (data) {
                        if (typeof (data) != "object" && parseInt(data) != NaN) {
                            if (percent_uploaded < 100)
                                percent_uploaded = JSON.stringify(data); //assigns the values returned from web service.
                        }
                        else {
                            showErrorResponseText(data,true);
                        }
                    }, function (error_response) {
                        showErrorResponseText(error_response,true);
                    });
                }
                else {
                    window.clearTimeout(timeoutRef);
                    $('#dvProgressBar').popup('close');
                    if (!is_aborted) {
                        var text = publication_info[0];
                        var product_key = publication_info[1];
                        fnUpdatePublicationInfo(file_name, product_key, text);
                        resetFileUploadForm();
                        var msg = "Your files have been successfully uploaded.";
                        //$('#dialogbox').prepend('<a id="btnClose" href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right" onclick="removeCloseButton();">Close</a>');
                        //$('#okBut').hide();
                        $('#alertmsg').text(msg);
                        $('#okBut').attr('onclick', "closeMessagePopup();");
                        $('#popupDialog').popup('open');
                        $('#popupDialog a').each(function (i) {
                            $(this).button();
                        });
                    }
                    is_aborted = false;
                    return true;
                }
            }
        }
    } ());

    //Submitting the form to upload
    var form_obj = ($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload');
    $(form_obj).attr('action', serviceURLDomain + 'api/FileUpload/' + sessionStorage.jobNumber + '^' + sessionStorage.username + '^' + time_stamp + '^1/' + file_name + '^1');
    var form = $(form_obj).ajaxSubmit(
                                        {
                                            type: 'post',
                                            beforeSend: function (xhr) {
                                                start = new Date().getTime();
                                                form_xhr = xhr;
                                                $('#popupFileUpload').popup('close');
                                                $('#dvProgressBar').popup('open', { positionTo: 'window' });
                                                timer.start('uploadProgressBar'); //Starts the timer just before the upload starts
                                                xhr.setRequestHeader("Authorization", sessionStorage.authString);
                                            },
                                            complete: function (xhr) {
                                                isComplete = true;
                                                if ($.browser.msie) {
                                                    var status_url = serviceURLDomain + "api/FileUpload/error/" + sessionStorage.jobNumber + "^" + sessionStorage.username + "^" + time_stamp + "^1";
                                                    getCORS(status_url, null, function (data) {
                                                        var status = data;
                                                        if (typeof (status) != "object" && status != undefined && status != null && status != "\"\"") {
                                                            is_aborted = true;
                                                            resetFileUploadForm();
                                                            $('#dvProgressBar').popup('close');
                                                            $('#alertmsg').text(status);
                                                            $('#okBut').attr('onclick', "closeMessagePopup();");
                                                            $('#popupDialog').popup('open');
                                                        } else if (typeof (status) != "object" && status == "\"\"" && !is_aborted) {
                                                            uploadingFiles = "";
                                                        }
                                                        else {
                                                            is_aborted = true;
                                                            showErrorResponseText(status,true);
                                                        }
                                                    }, function (error_response) {
                                                        is_aborted = true;
                                                        $('#dvProgressBar').popup('close');
                                                        //$('#alertmsg').text(error_response.responseText);
                                                        showErrorResponseText(error_response, false);
                                                        $('#okBut').attr('onclick', "closeMessagePopup();");
                                                        //$('#popupDialog').popup('open');
                                                    });
                                                }
                                                else {
                                                    if (xhr.statusText.toLowerCase() == "ok" || xhr.statusText.toLowerCase() == "n/a") {
                                                        uploadingFiles = "";
                                                    }
                                                }
                                                $('#dvProgressBar').popup('close');
                                            },
                                            error: function (error_response) {
                                                if (!($.browser.msie)) {
                                                    isComplete = true;
                                                    is_aborted = true;
                                                    resetFileUploadForm();
                                                    $('#dvProgressBar').popup('close');
                                                    // $('#alertmsg').text(error_response.responseText);
                                                    showErrorResponseText(error_response, false);
                                                    $('#okBut').attr('onclick', "closeMessagePopup();");
                                                    //$('#popupDialog').popup('open');
                                                }
                                            }
                                        });
}

function resetFileUploadForm() {
    var form_obj = ($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload');
    var fileInput = $(form_obj).find('input[type=file]')[0];
    if (fileInput.value != "") {
        // Create a new file input
        var newFileInput = fileInput.cloneNode(true);
        newFileInput.value = null;
        $(form_obj).find('input[type=file]').remove();
        var spn = $(form_obj).find('#spnFileInput');
        $(spn).append(newFileInput);
        $($(form_obj).find('input[type=file]')).bind('change', function (e) {
            selectFile($(e.target));
        });
    }
    $('#txtFileMap').val('');
    $('#txtFileName').val('');
    //$('#txtEmail').val('');
}

function closeMessagePopup() {
    $('#popupDialog').popup('open');
    $('#popupDialog').popup('close');
}

//Cancels the file upload.
function cancelUpload() {
    is_aborted = true;
    form_xhr.abort();
}

//Browse to specific folder
function selectFile(ctrl) {
    var file_name = $(ctrl).val();
    var selected_file_name = $(ctrl).val().substring($(ctrl).val().lastIndexOf('\\') + 1, $(ctrl).val().length);
    var myRegexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;
    if (!myRegexp.test(selected_file_name.substring(0, selected_file_name.lastIndexOf('.')))) {
        $('#popupFileUpload').popup('close');
        $('#txtFileName').val('');
        $('#popupListDefinition').popup('close');
        $('#alertmsg').text('Illegal character in file name. Uploading cannot continue.');
        $('#popupDialog').popup('open');
        return false;
    }
    else {
        var file_name_with_ext = file_name.substring(file_name.lastIndexOf('\\') + 1);
        var extension = file_name_with_ext.substring(file_name_with_ext.lastIndexOf('.') + 1);
        var file_name_without_ext = file_name_with_ext.substring(0, file_name_with_ext.lastIndexOf('.'));
        var allowedExtension = ["CFS", "CSV", "DAT", "DATA", "GZ", "TXT", "ZIP"];
        var isValidFileFormat = false;
        var uploading_file_name = "";
        if (typeof (extension) != 'undefined') {
            for (var i = 0; i < allowedExtension.length; i++) {
                if (extension != undefined && extension.toLowerCase() == allowedExtension[i].toLowerCase()) {
                    isValidFileFormat = true;
                    break;
                }
            }
            if (!isValidFileFormat) {
                resetFileUploadForm();
                $('#txtFileMap').val('');
                $('#txtFileName').val('');
                $('#popupFileUpload').popup('close');
                $('#alertmsg').text('Invalid File Type...Only "CFS", "CSV", "DAT", "DATA", "GZ", "TXT" and "ZIP" types are accepted.');
                $('#popupDialog').popup('open');
                return true;
            }
        }

        $('#txtFileName').val(file_name);
        //populate the file map field
        $('#txtFileMap').val(file_name_without_ext + "_map.xml");
    }
}


function uploadFile(e) {
    if (!validateUploadParameters()) return false;
    sessionStorage["uploadEMail"] = $("#txtEmail").val();
    var publication_info = $('#hidUploadFile').val().split('|');
    var gdata_publication = gData[publication_info[1]];
    var index = 0;
    var file_name = $('#txtFileName').val().substring($('#txtFileName').val().lastIndexOf('\\') + 1, $('#txtFileName').val().length);
    var gdata_upload_to = jQuery.grep(gdata_publication, function (obj, key) {
        index = key;
        if (obj.fileType.toLowerCase() === publication_info[0].toLowerCase()) {
            obj.origFileName = file_name;
            obj.fileMapName = $('#txtFileMap').val();
            obj.distributionLevelId1 = obj.distributionLevelId1;
            obj.distributionLevelId2 = obj.distributionLevelId2;
            obj.email = $('#txtEmail').val();
            obj.hasChanged = 1;
            if (obj["jobNumber"] == undefined)
                obj["jobNumber"] = sessionStorage.jobNumber;
            else
                obj.jobNumber = sessionStorage.jobNumber;
            var date = new Date();
            obj.note = 'Successfully Loaded ' + (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
            return obj;
        }
    });
    var post_url = serviceURLDomain + "api/FileUploadMw/" + getSessionData("publisherId") + "/" + file_name + "/bob";

    postCORS(post_url, JSON.stringify(gdata_upload_to[0]), function (response) {
        if (response.replace(/"/g, "") == "success") {
            formSubmit(e)
        }
        else {
            showErrorResponseText(response_error, false);
            //$('#alertmsg').text(response_error.responseText);
            //$('#popupDialog').popup('open');
            return false;
        }
    }, function (response_error) {
        showErrorResponseText(response_error, false);
        //$('#alertmsg').text(response_error.responseText);
        //$('#popupDialog').popup('open');
    });
    //displayList();
    makeGData();
}


function removeCloseButton() {
    $('#btnClose').remove();
    $('#okBut').show();
    $('#popupDialog').popup('close');
}
function validateUploadParameters() {
    var message = "";
    var form_obj = ($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload');
    if ($(form_obj).find('input[type=file]').val() == "" || $(form_obj).find('input[type=file]').val() == null) {
        message = "Please select a file to upload";
    }


    $('#popupFileUpload').popup('close');
    if (message != "") {
        $('#alertmsg').html(message);
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close'); $('#popupFileUpload').popup('open');");
        $('#popupDialog').popup('open');
        return false;
    }
    return true;
}

function checkEmailAndFile() {
    var email = document.getElementById('txtEmail');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var message = "";
    var msg = '';
    if (email.value == '') {
        msg = 'Please provide an email address';
    }
    else if (!filter.test(email.value)) {
        msg = 'Please provide a valid email address';
    }
    if (msg != '') {
        $('#popupFileUpload').popup('close');
        $('#alertmsg').html(msg);
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
        $('#popupDialog').popup('open');
        return false;
    }
    else {
        return true;
    }
}
function loadUploadTargets() {
    getCORS(fileUploadTargetURL, null, function (data) {
        gDataTarget = data;
        setTargetMonth();
    }, function (error_response) {
        showErrorResponseText(error_response,true);
    });
}

function fnOpenEditUploadTargetsPopup() {
    fillTargetDDL();
    $('#popupEditUploadTargets').popup('open');
}
function setTargetMonth() {
    var cur_date = new Date();
    var cur_day = cur_date.getDate();
    var cur_month = cur_date.getMonth();
    var cur_year = cur_date.getFullYear();
    var temp_date = '';
    var is_target_found = false;
    $.each(gDataTarget, function (key, val) {
        if (key == 55) {
            if (getTargetMonth(val, cur_month, cur_year, cur_day)) {
                //$('#divSubHeader').html("Target: " + month[cur_month] + ", " + cur_year);
                is_target_found = true;
            }
            else if (getTargetMonth(val, cur_month + 1, cur_year, cur_day)) {
                //$('#divSubHeader').html("Target: " + month[cur_month + 1] + ", " + cur_year);
                is_target_found = true;
            }
            if (is_target_found)
                return false;
        }
    });
    if (!is_target_found) {
        $.each(gDataTarget, function (key, val) {
            if (key == 50) {
                if (getTargetMonth(val, cur_month, cur_year, cur_day))
                //$('#divSubHeader').html("Target: " + month[cur_month] + ", " + cur_year);
                    return false;
            }
        });
    }
    //$('#divSubHeader').html("Target: " + month[new Date().getMonth()] + ", " + new Date().getFullYear());
}
function getTargetMonth(val_list, cur_month, year, date) {
    var is_found = false;
    var date_check = new Date(year, cur_month, date);
    var temp_date_begin = "";
    var temp_date_end = "";
    $.each(val_list, function (a, b) {
        temp_date_begin = new Date(b.ActiveBegin);
        temp_date_end = new Date(b.ActiveEnd);
        if (date_check >= temp_date_begin && date_check <= temp_date_end) {
            //if (temp_date.getMonth() == month && temp_date.getFullYear() == year) {
            is_found = true;
            $('#divSubHeader').html("Target: " + b.description);
            sessionStorage.selectedTarget = $('#divSubHeader').html();
            sessionStorage.jobNumber = b.JobNumber;
            return false;
        }
        else
            is_found = false;
    });
    return is_found;
}

function fillTargetDDL() {
    $("#ddlAdvertiserTarget").empty();
    $("#ddlAdvertiserTarget").append('<option value="-1">Select Advertiser Target</option>');

    $("#ddlMonthlyTarget").empty();
    $("#ddlMonthlyTarget").append('<option value="-1">Select Monthly Target</option>');
    $.each(gDataTarget, function (key, val) {
        $.each(val, function (key_attrs, val_attrs) {
            if (key == 50)
                $("#ddlAdvertiserTarget").append('<option value=' + val_attrs.JobNumber + '>' + val_attrs.description + '</option>');
            else if (key == 55)
                $("#ddlMonthlyTarget").append('<option value=' + val_attrs.JobNumber + '>' + val_attrs.description + '</option>');
        });
    });
    $('#ddlAdvertiserTarget').val('-1').selectmenu('refresh');
    $('#ddlMonthlyTarget').val('-1').selectmenu('refresh');
}

function fnTargetChange(ctrl, type) {
    //var key_number = (type == "Advertiser") ? 50 : 55;
    //var selected_val = $("#" + ctrl.id).val();
    //if (selected_val != "-1") {
    //    $.each(gDataTarget, function (key, val) {
    //        $.each(val, function (key_attrs, val_attrs) {
    //            if (parseInt(key) === key_number && val_attrs.JobNumber === parseInt(selected_val)) {
    //                $("#lbl" + type + "JobNumber").html(val_attrs.JobNumber);
    //                $("#lbl" + type + "ActiveBegin").html(val_attrs.ActiveBegin);
    //                $("#lbl" + type + "ActiveEnd").html(val_attrs.ActiveEnd);

    //                $("#dv" + type + "JobNumber").show();
    //                $("#dv" + type + "ActiveBegin").show();
    //                $("#dv" + type + "ActiveEnd").show();
    //            }
    //        });
    //    });
    //}
    //else {
    //    $("#lbl" + type + "JobNumber").empty();
    //    $("#lbl" + type + "ActiveBegin").empty();
    //    $("#lbl" + type + "ActiveEnd").empty();

    //    $("#dv" + type + "JobNumber").hide();
    //    $("#dv" + type + "ActiveBegin").hide();
    //    $("#dv" + type + "ActiveEnd").hide();
    //}

    var selected_val = $("#" + ctrl.id).val();
    if (selected_val != "-1") {
        $.each(gDataTarget, function (key, val) {
            $.each(val, function (key_attrs, val_attrs) {
                if (val_attrs.JobNumber === parseInt(selected_val)) {
                    $("#lblJobNumber").html(val_attrs.JobNumber);
                    $("#lblActiveBegin").html(val_attrs.ActiveBegin);
                    $("#lblActiveEnd").html(val_attrs.ActiveEnd);

                    $("#dvJobNumber").show();
                    $("#dvActiveBegin").show();
                    $("#dvActiveEnd").show();

                    if (type == "Monthly") {
                        var temp_date = new Date(val_attrs.ActiveBegin);
                        //sessionStorage.selectedTarget = $('#divSubHeader').html();
                        //$('#divSubHeader').html("Target: " + month[temp_date.getMonth()] + ", " + temp_date.getFullYear());
                        $('#divSubHeader').html("Target: " + val_attrs.description);
                    }
                    else {
                        //sessionStorage.selectedTarget = $('#divSubHeader').html();
                        $('#divSubHeader').html("Target: " + $("#" + ctrl.id).find('option:selected').text());

                    }
                }
            });
        });
    }
    else {
        $("#lblJobNumber").empty();
        $("#lblActiveBegin").empty();
        $("#lblActiveEnd").empty();

        $("#dvJobNumber").hide();
        $("#dvActiveBegin").hide();
        $("#dvActiveEnd").hide();
    }

    if (type == "Advertiser")
        $('#ddlMonthlyTarget').val('-1').selectmenu('refresh');
    else
        $('#ddlAdvertiserTarget').val('-1').selectmenu('refresh');
}

function fnTargetUploadReset() {
    $("#lblJobNumber").empty();
    $("#lblActiveBegin").empty();
    $("#lblActiveEnd").empty();

    $("#dvJobNumber").hide();
    $("#dvActiveBegin").hide();
    $("#dvActiveEnd").hide();

    $("#ddlAdvertiserTarget").val('-1');
    $("#ddlMonthlyTarget").val('-1');
    if (sessionStorage.selectedTarget != undefined && sessionStorage.selectedTarget != null && sessionStorage.selectedTarget != "") {
        var temp = sessionStorage.selectedTarget;
        $('#divSubHeader').html(temp);
        //sessionStorage.removeItem('selectedTarget');
    }

    $('#popupEditUploadTargets').popup('open');
    $('#popupEditUploadTargets').popup('close');
}

function fnTargetSubmit() {
    if (!fnTargetValidation())
        return false;

    sessionStorage.jobNumber = $("#lblJobNumber").text();
    //sessionStorage.removeItem('selectedTarget');
    sessionStorage.selectedTarget = $('#divSubHeader').html();

    fnTargetUploadReset();
    makeGData();

    //var post_url = serviceURLDomain + "api/FileUploadMw/" + getSessionData("publisherId") + "/" + $("#lblJobNumber").val();

    //postCORS(post_url, JSON.stringify(gDataTarget), function (response) {
    //    var msg = 'Data was saved.';
    //    if (response.replace(/"/g, "") != "success") {
    //        msg = "Data was not saved.";
    //    }
    //    else
    //        makeGData();
    //    $('#alertmsg').text(msg);
    //    $('#popupDialog').popup('open');
    //    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    //}, function (response_error) {
    //    $('#alertmsg').text(response_error.responseText);
    //    $('#popupDialog').popup('open');
    //    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    //});
}

function fnTargetValidation() {
    if ($("#ddlAdvertiserTarget").val() == "-1" && $("#ddlMonthlyTarget").val() == "-1") {
        $('#alertmsg').text("Please select at least one Target.");
        $('#popupEditUploadTargets').popup('close');
        $('#popupDialog').popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupEditUploadTargets').popup('open');");
        return false;
    }
    return true;
}

//******************** Public Functions End **************************
