﻿//var loadSubmitOrderInfo = function (self) {
//******************** View Model Start ******************************
var jobSubmitOrder = function () {
    var self = this;
    //******************** Global Variables Start **************************
    self.gData;
    self.jobNumber = getSessionData("jobNumber");
    self.facilityId = sessionStorage.facilityId;
    self.approversList = [];
    self.quantity;
    self.estQuantity;
    self.cost;
    self.gOutputData;
    self.postJobURL;
    self.PAGE_NAME = 'jobSubmitOrder.html';
    self.customerDetailsData = "";
    var gEditJobService = serviceURLDomain + "api/JobTicket/" + sessionStorage.facilityId + "/" + sessionStorage.jobNumber + '/' + sessionStorage.username;;
    //******************** Global Variables End **************************
    new makeSubmitOrder(self);
    var Order = function (order_data) {
        var self = this;
        ko.mapping.fromJS(order_data, [], self);
    }

    self.ordersData = ko.observableArray();
    self.headers = ko.observableArray();
    self.totalHeaders = ko.observableArray();
    self.loadSubmitOrder = function () {
        var displayEmail = "";
        var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
            return customer.jobCustomerNumber == jobCustomerNumber;
        });
        var tempList = {};
        self.jobCustomerNumber = jobCustomerNumber;
        self.displayEmail = customerDetails[0].email;

        if (customerDetails != null && customerDetails[0].gridCols != undefined && customerDetails[0].gridCols != null)
            self.headers(customerDetails[0].gridCols);

        if (customerDetails != null && customerDetails[0].gridColTotals != undefined && customerDetails[0].gridColTotals != null)
            self.totalHeaders(customerDetails[0].gridColTotals);
        var total_index = "";
        $.each(self.headers(), function (key, val) {
            if (val.toLowerCase().indexOf('Total') > -1) {
                total_index = key;
                return false;
            }
        });

        $.each(self.gData.checkOutGrid.checkOutGridStoreList, function (a, b) {
            var names = Object.getOwnPropertyNames(b);
            $.each(Object.getOwnPropertyNames(b), function (c, d) {
                if (customerDetails[0].gridColIds.indexOf(d) > -1)
                    tempList[d] = b[d];
            });
            //if (sessionStorage.userRole == 'admin' && (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && a == 0) {
            //    $.each(b.optionalChargesPretty, function (key, val) {
            //        if (key > 1) {
            //            self.headers().splice((total_index - 1), 0, val.expenseName);
            //            total_index = total_index + 1;
            //        }
            //    });
            //}
            self.ordersData.push(new Order(tempList));
        });
        self.finalInvoiceAmount = ko.observable(self.gData.checkOutGrid.finalInvoiceAmountPretty);
        self.finalEstdQuantity = ko.observable(self.estQuantity);
        self.finalQuantity = self.quantity;
        self.finalCost = self.cost;
        self.emails = ko.observable();
        self.approversViewersList = ko.observable({});
        self.approversViewersList(self.approversList);
        self.isEditable = ko.observable((sessionStorage.userRole == 'admin') ? 1 : 0);
        self.otherComments = ko.observable('');
        self.selectedStore = ko.observable({});
        self.selectedStoreForEdit = ko.observable({});
        self.editAdditionalCharges = function (data, event) {
            var temp_data = ko.mapping.toJS(data);
            self.selectedStoreForEdit(ko.mapping.toJS(data));
            var obs_temp_data = {};
            ko.mapping.fromJS(temp_data, {}, obs_temp_data);
            self.selectedStore(temp_data);

            //self.selectedStoreForEdit(obs_temp_data);
            $('#additionalChargesPopup div[data-role=collapsibleset] div fieldset').trigger('create');
            $('#additionalChargesPopup div[data-role=collapsibleset] div').trigger('create');
            $('#additionalChargesPopup div[data-role=collapsibleset]').collapsibleset('refresh');
            $('#additionalChargesPopup').popup('open');
        };

        self.addAdditionExpense = function (data, event) {
            //var selected_store_info = ko.mapping.toJS(data.selectedStoreForEdit);
            var selected_store_info = ko.mapping.toJS(data.selectedStore);
            selected_store_info.optionalChargesPretty.push({
                expenseName: "Additional",
                rateType: "",
                value: ""
            });
            var obs_temp_data = {};
            //ko.mapping.fromJS(selected_store_info, {}, obs_temp_data);
            self.selectedStore(selected_store_info);
            $('#additionalChargesPopup div[data-role=collapsibleset] div fieldset').trigger('create');
            $('#additionalChargesPopup div[data-role=collapsibleset] div').trigger('create');
            $('#additionalChargesPopup div[data-role=collapsibleset]').collapsibleset('refresh');
        };

        self.cancelAddExpenses = function (data, event) {
            data.selectedStoreForEdit({});
            $('#additionalChargesPopup').popup('close');
        };

        self.submitExpensesInfo = function (data, event) {
            var cnt_temp = "";
            var str_indx = "";
            //var selected_store_info = ko.mapping.toJS(data.selectedStoreForEdit);
            var selected_store_info = data.selectedStore();

            var apply_remove_status = [];
            var idx = "";
            var prev_idx = "";
            var is_apply_all = false;
            var is_remove_all = false;
            var chk_apply_to = $('input[name=rdoApplyTo]:checked').val();
            //var chk_remove_from=


            var temp_selected_store_info = self.selectedStoreForEdit();
            $.each(selected_store_info.optionalChargesPretty, function (add_key, add_val) {
                var chk_apply_list = $('#additionalChargesPopup div[data-role=collapsibleset] div fieldset input[name=rdoApplyTo' + add_key + ']:checked').val();
                var chk_remove_list = $('input[id=chkRemove_' + add_key + ']').is(':checked');
                var is_store_updated = false;
                $.each(self.ordersData(), function (key, val) {
                    var selected_store_edit = ko.utils.arrayFirst(self.selectedStoreForEdit().optionalChargesPretty, function (item, idx) {
                        //return (idx == add_key && item.expenseName.toLowerCase() != "additional" && item.expenseName !== add_val.expenseName);
                        return (idx == add_key && item.expenseName !== add_val.expenseName);
                    });

                    //if (selected_store_edit != null && selected_store_edit.expenseName.toLowerCase() == "additional" && selected_store_edit.rateType == "" && selected_store_edit.value == "")
                    //    selected_store_edit = null;

                    //var selected_expense = ko.utils.arrayFirst(val.optionalChargesPretty(), function (item) {
                    var selected_expense = ko.utils.arrayFirst(val.optionalChargesPretty(), function (item) {
                        return item.expenseName() === ((selected_store_edit != null) ? selected_store_edit.expenseName : add_val.expenseName);
                    });
                    if (selected_expense != null && selected_expense.expenseName().toLowerCase() == "additional" && selected_expense.rateType() == "" && selected_expense.value() == "") {
                        if ((selected_store_info.storeId == val.storeId() && chk_apply_list == "applyToSingle") || chk_apply_list == "applyToAll") {
                            val.optionalChargesPretty.remove(selected_expense);
                            selected_expense = null;
                        }
                    }

                    var temp_exp1 = ko.mapping.toJS(add_val);

                    if ((chk_remove_list && (chk_apply_list == "applyToSingle" || chk_apply_list == "applyToAll")) || (!chk_remove_list && chk_apply_list == "applyToSingle" && is_store_updated)) {
                        temp_exp1.rateType = "";
                        temp_exp1.value = "";
                    }

                    var temp_exp = {};
                    ko.mapping.fromJS(temp_exp1, {}, temp_exp);

                    if (selected_expense != null) {
                        if (selected_store_info.storeId != val.storeId()) {
                            selected_expense.expenseName(temp_exp1.expenseName);
                            if (chk_apply_list == "applyToAll") {
                                //if ((chk_remove_list && ((selected_store_info.optionalChargesPretty.length > 1 && chk_apply_list == "applyToSingle") || chk_apply_list == "applyToAll")) || chk_apply_list == "applyToAll") {
                                selected_expense.rateType(temp_exp1.rateType);
                                selected_expense.value(temp_exp1.value);
                            }
                        } else {
                            is_store_updated = true;
                            selected_expense.expenseName(temp_exp1.expenseName);
                            selected_expense.rateType(temp_exp1.rateType);
                            selected_expense.value(temp_exp1.value);
                        }
                    }
                    else {

                        if (selected_store_info.storeId != val.storeId()) {
                            if (chk_apply_list == "applyToAll") {
                                //selected_expense.expenseName(temp_exp1.expenseName);
                                //selected_expense.rateType(temp_exp1.rateType);
                                //selected_expense.value(temp_exp1.value);
                                val.optionalChargesPretty.push(temp_exp);
                            }
                            else {
                                temp_exp1.rateType = "";
                                temp_exp1.value = "";
                                var temp_exp2 = {};
                                ko.mapping.fromJS(temp_exp1, {}, temp_exp2);
                                val.optionalChargesPretty.push(temp_exp2);
                            }
                            //else {
                            //}
                        } else {
                            is_store_updated = true;
                            val.optionalChargesPretty.push(temp_exp);
                        }

                        //if (val.optionalChargesPretty().length == 1) {
                        //    if (val.optionalChargesPretty()[0].expenseName().toLowerCase() == "additional" && val.optionalChargesPretty()[0].rateType() == "" && val.optionalChargesPretty()[0].value() == "")
                        //        val.optionalChargesPretty([]);
                        //}

                        if (selected_store_info.storeId == val.storeId()) {
                            is_store_updated = true;
                        }
                    }

                    //if (selected_store_info.storeId != val.storeId()) {
                    //    if (val.optionalChargesPretty().length < max_len) {
                    //        for (var i = val.optionalChargesPretty().length; i < max_len; i++) {
                    //            val.optionalChargesPretty.push({
                    //                expenseName: "Additional",
                    //                rateType: "",
                    //                value: ""
                    //            });
                    //        }
                    //    }
                    //}
                });
            });
            //$.each(self.ordersData(), function (key, val) {
            //    if (val.storeId() == selected_store_info.storeId) {
            //        str_indx = key;
            //        val.optionalChargesPretty([]);
            //        //val.optionalChargesPretty(data.selectedStoreForEdit().optionalChargesPretty());
            //        var obs_temp_data = {};
            //        ko.mapping.fromJS(selected_store_info, {}, obs_temp_data);
            //        val.optionalChargesPretty(obs_temp_data.optionalChargesPretty());
            //        cnt_temp = val.optionalChargesPretty().length;
            //    }
            //});
            //var max_len = 0;
            //$.each(self.ordersData(), function (key, val) {
            //    if (max_len < val.optionalChargesPretty().length) {
            //        max_len = val.optionalChargesPretty().length;
            //    }
            //});

            var st_ind = "";
            if (self.headers().indexOf('Postage(CPP)') > -1)
                st_ind = self.headers.indexOf('Postage(CPP)') + 1;
            else if (self.headers().indexOf('Cost Per Piece') > -1)
                st_ind = self.headers().indexOf('Cost Per Piece') + 1;
            var end_ind = self.headers().indexOf('Total');
            self.headers.splice(st_ind, (end_ind - st_ind));

            $.each(self.ordersData(), function (a, b) {
                if (sessionStorage.userRole == 'admin' && a == 0) {
                    $.each(b.optionalChargesPretty(), function (key, val) {
                        self.headers.splice((self.headers.indexOf('Total')), 0, val.expenseName());
                    });
                }
            });

            $('#additionalChargesPopup').popup('close');
        };
        //to filter customer details based on logged in customer job number
        var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
            return customer.jobCustomerNumber == jobCustomerNumber;
        });
    };

    self.productionRelease = function () {
        var msg = "Orders cannot be Released to Production while the site is in Beta.";
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open', { positionTo: 'window' });
        return false;
    };

    self.submitOrder = function () {
        if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            var msg = "Orders cannot be Submitted to Production while the site is in Demo.";
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open', { positionTo: 'window' });
            return false;
        }
        var est_not_found = false;
        var actual_qty_not_found = false;
        var qty_msg = "";
        $.each($('#tblOrderGrid input[type="text"]'), function (key, val) {
            //$.each(val.find('td'), function (key1, val1) {
            if ($(val).val() == "") est_not_found = true;
            var act_qty = $(val).parent().parent().next().parent().find('span').text();
            if (act_qty == null || act_qty == "") actual_qty_not_found = true;
            if (est_not_found && actual_qty_not_found) {
                qty_msg = "The order cannot be submitted without a quantity for each location. Please enter an estimated quantity if you do not have actual quantity values.";
                return false;
            }
            //});
        });
        if (qty_msg != "") {
            $('#alertmsg').text(qty_msg);
            $('#popupDialog').popup('open', { positionTo: 'window' });
            return false;
        }

        if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
            if (!self.artListUploaded()) return false;
        }
        if ($("#chkNoArtApprove").is(":checked") || $("#chkNoListApprove").is(":checked"))
            $("#btnContinuePopup").text("Continue");
        else
            $("#btnContinuePopup").text("Submit Order");

        var is_list_uploaded = false;
        var is_art_uploaded = false;
        var is_needs_count = false;

        if (self.gOutputData.standardizeAction.standardizeFileList != undefined && self.gOutputData.standardizeAction.standardizeFileList.length > 0 && self.gOutputData.standardizeAction.standardizeFileList[0].fileName != "")
            is_list_uploaded = true;

        if (self.gOutputData.artworkAction.artworkFileList != undefined && Object.keys(self.gOutputData.artworkAction.artworkFileList).length > 0)
            is_art_uploaded = true;

        if (self.gOutputData != undefined && self.gOutputData.selectedLocationsAction != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
            $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                if (val.needsCount) {
                    is_needs_count = (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? false : true;
                    return false;
                }
            });
        }
        var msg = "";
        if ($("#ulJobLinks li[id*=liListSelection]").length > 0) {
            if ((jobCustomerNumber != AAG_CUSTOMER_NUMBER) || (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName != "user"))
                if (!is_art_uploaded || is_needs_count)
                    msg = "All artwork files should be upload and list selections made prior to submitting your order. Submitting your order will send Confirmation and Approval emails to the users listed on this page. Do you want to Continue to Submit this Order?";
        }
        else if (!is_list_uploaded || (jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && !is_art_uploaded)) {
            var art_work_msg = ((jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER) ? '' : 'and artwork files ');
            var approval_email_msg = ((jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) ? '' : 'and Approval ');
            msg = "All lists " + art_work_msg + "should be upload prior to submitting your order. Submitting your order will send Confirmation " + approval_email_msg + "emails to the users listed on this page. Do you want to Continue to Submit this Order?";
        }
        if (msg != "") {
            $('#dvMsg').html(msg);
            $('#confirmAdmin').popup('open');
        }
        else {
            self.getJobReadinessStatus("submit");
        }
    };

    self.getJobReadinessStatus = function (submit_flag) {
        diff = DiffObjects(jQuery.parseJSON(sessionStorage.jobSetupOutput), jQuery.parseJSON(sessionStorage.jobSetupOutputCompare));
        if (diff.length > 0) {
            $('#btnSubmitOrder').attr('disabled', '');
            $('#btnSubmitOrder')[0].disabled = false;
            if (submit_flag == "submit")
                self.fnJobFinal("submit");
        }
        else {
            var submit_log_info = serviceURLDomain + "api/Readiness_submit/" + self.facilityId + '/' + jobCustomerNumber + "/" + self.jobNumber + '/';
            //var submit_log_info = serviceURLDomain + "api/Readiness_submit/" + self.facilityId + '/6000420/602098/';
            getCORS(submit_log_info, null, function (data) {
                if (submit_flag == "submit") {
                    if (data.status.toLowerCase() != "ok") {
                        self.loadLogsInfo(data);
                        $('#postReminder').popup('open', { positionTo: 'window' });
                    }
                    else {
                        self.fnJobFinal("submit");
                    }
                }
                else {
                    if (data.status.toLowerCase() != "ok") {
                        $('#btnSubmitOrder').attr('disabled', 'disabled');
                        $('#btnSubmitOrder')[0].disabled = true;
                    }
                    else {
                        $('#btnSubmitOrder').attr('disabled', '');
                        $('#btnSubmitOrder')[0].disabled = false;
                    }

                }
            }, function (response_error) {
                showErrorResponseText(response_error, false);
            });
        }
    }

    self.listJobDetails = ko.observableArray([]);
    self.lastSubmittedDate = ko.observable({});
    self.loadLogsInfo = function (data) {
        //This function will display details info for selected logging
        self.listJobDetails([]);
        self.lastSubmittedDate(data.last_submitted_time);
        $.each(data.logList, function (a, b) {
            //for (i = 0; i < b.length; i++) {
            self.listJobDetails.push(b);
            //}
        });
    };

    self.fnJobFinal = function (action_type) {
        $('#confirmAdmin').popup('close');
        // post approvers info
        var approvers_info = {};
        $('#postReminder').popup('close');
        $.each(self.approversViewersList(), function (key, val) {
            if (val != null && val != "") {
                approvers_info["approvalType"] = key;
                //            if ((key.toLowerCase().indexOf('viewer') > -1))
                //                approvers_info["viewers"] = val;
                //            else
                approvers_info["approvers"] = val;
                (function (post_approvers_info) {
                    var my_timer = setInterval(function () {
                        var post_approval_url = serviceURLDomain + "api/Tagging_post_approvers/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + self.jobNumber
                        postCORS(post_approval_url, JSON.stringify(post_approvers_info), function (response) {
                            approvers_info = {};
                        }, function (response_error) {
                            showErrorResponseText(response_error, false);
                            ////var msg = 'Job creation failed.';
                            //var msg = response_error.responseText;
                            //$('#alertmsg').text(msg);
                            //$('#popupDialog').popup('open', { positionTo: 'window' });
                            //var error = response_error;
                        });
                        clearInterval(my_timer);
                    }, 1000);
                })(approvers_info);
                approvers_info = {};
            }
        });

        //submit job
        if ((appPrivileges.customerNumber === CW_CUSTOMER_NUMBER || appPrivileges.customerNumber === ALLIED_CUSTOMER_NUMBER || appPrivileges.customerNumber === DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber === KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber === SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER) && self.jobNumber != "-1") //Biz rules for needsBillingSave flag
            self.gOutputData = fnNeedsBillingSave(self.gOutputData);

        if (appPrivileges.customerNumber === OH_CUSTOMER_NUMBER)
            //api/oh_submit/{facilityId}/{customerNumber}/{jobNumber}/{userId}
            postJobURL = serviceURLDomain + 'api/JobTicket_ohEmail/' + self.facilityId + '/' + appPrivileges.customerNumber + '/' + self.jobNumber + '/' + sessionStorage.username;
        else
            postJobURL = serviceURLDomain + 'api/JobTicket/' + self.facilityId + '/' + self.jobNumber + '/' + sessionStorage.username + '/' + action_type + '/ticket';
        postCORS(postJobURL, JSON.stringify(self.gOutputData), function (response) {
            var msg = (sessionStorage.jobNumber == "-1") ? 'Your job was saved.' : 'Your job was updated.';
            msg = (action_type == "submit") ?
                            ((jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER)
                                            ? "Thanks for submitting your job to Gizmo. Once your files have processed, you’ll receive an automated notification from the system – usually within half an hour or less."
                                            : (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? "Thank you! Your order has been submitted and proofs will be sent for approval." : "Thank you! Your order has been submitted for execution.")
                  : msg;
            //
            if (response != "") {
                if (!isNaN(parseInt(response.replace(/"/g, "")))) {
                    if (response.replace(/"/g, "").toLowerCase() != "success")
                        self.jobNumber = parseInt(response.replace(/"/g, ""));
                    sessionStorage.jobNumber = self.jobNumber;
                    self.gOutputData.jobNumber = self.jobNumber;
                    sessionStorage.jobDesc = (self.gOutputData.jobName != undefined) ? self.gOutputData.jobName : "";
                    $('#liJobSummary').css('display', "block");
                    displayJobInfo();
                    sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
                    sessionStorage.jobSetupOutputCompare = JSON.stringify(self.gOutputData);
                    $('#btnSubmitOrder').attr('disabled', 'disabled');
                    $('#btnSubmitOrder')[0].disabled = true;
                }
                else
                    msg = response;
                $('#alertmsg').text(msg);
                //self.makeGData();
                window.setTimeout(function getDelay() {
                    if (msg == 'Your job was saved.' || msg == 'Your job was updated.')
                        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");

                    if (action_type == "submit" && (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER))
                        $("#okBut").bind("click", function endDemoJob() {
                            $('#popupDialog').popup('close');
                            if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && (is_valid_domain || appPrivileges.roleName == "admin"))
                                window.location.href = "../index.htm";
                        });

                    $('#popupDialog').popup('open', { positionTo: 'window' });
                }, 500);
            }
            else {
                var msg = 'Job creation failed.';
                $('#alertmsg').text(msg);
                $('#popupDialog').popup('open', { positionTo: 'window' });
            }
        }, function (response_error) {
            if (jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER) {
                var msg = 'Job creation failed.';
                msg = response_error.responseText;
                if (response_error.status == 409) {
                    msg = "Your job <b>" + self.gOutputData.jobName + "</b> could not be saved as there is already a job in the system with that name. Please rename your job and try again.";
                    $('#alertmsg').html(msg);
                }
                else
                    showErrorResponseText(response_error, false);
            }
            else {
                var msg = "Thank you! your order has been saved but will not be submitted for approval while in Beta.";
                $('#alertmsg').html(msg);
                $('#popupDialog').popup('open', { positionTo: 'window' });
            }
            //$('#alertmsg').text("Unknown Error: Contact a Site Administrator");
            //$('#popupDialog').popup('open', { positionTo: 'window' });
            //var error = response_error;
        });
    };

    self.displayOrderSummary = function () {
        var session_data = $.parseJSON(sessionStorage.jobSetupOutput)
        var order_summary = '';
        if (session_data.jobNumber == "-1") {
            order_summary = ' <h3>Order Summary</h3>';
            order_summary += '<div style="padding-bottom:12px;"><b>Job Name:</b> ' + session_data.jobName + ' <br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>In-Home Date:</b> ' + session_data.inHomeDate + '<br /></div>';
            if (jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER)
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded List Files: </b><br />NA<br /></div>';
            if (jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER) {
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded Art Files: </b><br />NA<br /></div>';
            }
        }
        else {
            //getCORS(gEditJobService, null, function (data) {
            var list_files_data = (session_data.standardizeAction.standardizeFileList == undefined || session_data.standardizeAction.standardizeFileList == null || session_data.standardizeAction.standardizeFileList == "" || Object.keys(self.gOutputData.standardizeAction.standardizeFileList).length == 0 || self.gOutputData.standardizeAction.standardizeFileList[0].fileName == "") ? [] : session_data.standardizeAction.standardizeFileList;
            var art_files_data = (session_data.artworkAction.artworkFileList == undefined || session_data.artworkAction.artworkFileList == null || session_data.artworkAction.artworkFileList == "" || Object.keys(session_data.artworkAction.artworkFileList).length == 0) ? [] : session_data.artworkAction.artworkFileList;
            order_summary = ' <h3>Order Summary</h3>';
            order_summary += '<div style="padding-bottom:12px;"><b>Job Name:</b> ' + session_data.jobName + ' <br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>In-Home Date:</b> ' + session_data.inHomeDate + '<br /></div>';
            //if (jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName == "admin")) {
            if (jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && (jobCustomerNumber == AAG_CUSTOMER_NUMBER && (is_valid_domain || appPrivileges.roleName == "admin"))) {
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded List Files: </b><br />';
                if (list_files_data.length > 0) {
                    $.each(list_files_data, function (key, val) {
                        order_summary += '<span>' + val.fileName + '</span><br />';
                    });
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';
            }
            if (jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER) {
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded Art Files: </b><br />';
                if (Object.keys(art_files_data).length > 0) {
                    $.each(art_files_data, function (key, val) {
                        order_summary += '<span>' + val.fileName + '</span><br />';
                    });
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';
            }
            if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                    order_summary += '<div style="padding-bottom:12px;"><b>Uploaded Source Files: </b><br />';
                    if (Object.keys(list_files_data).length > 0) {
                        var is_found = false;
                        $.each(list_files_data, function (key, val) {
                            if (val.sourceType == 1) {
                                order_summary += '<span>' + val.fileName + '</span><br />';
                                is_found = true;
                            }
                        });
                        if (!is_found)
                            order_summary += '<span>NA</span><br />';
                    }
                    else {
                        order_summary += '<span>NA</span><br />';
                    }
                    order_summary += '</div>';
                }

                order_summary += '<div style="padding-bottom:12px;"><b>Loaded Seed Files: </b><br />';
                if (Object.keys(list_files_data).length > 0) {
                    var is_found = false;
                    $.each(list_files_data, function (key, val) {
                        if (val.sourceType == 6) {
                            order_summary += '<span>' + val.fileName + '</span><br />';
                            is_found = true;
                        }
                    });
                    if (!is_found)
                        order_summary += '<span>NA</span><br />';
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';

                order_summary += '<div style="padding-bottom:12px;"><b>Loaded Do Not Mail Files: </b><br />';
                if (Object.keys(list_files_data).length > 0) {
                    var is_found = false;
                    $.each(list_files_data, function (key, val) {
                        if (val.sourceType == 2) {
                            order_summary += '<span>' + val.fileName + '</span><br />';
                            is_found = true;
                        }
                    });
                    if (!is_found)
                        order_summary += '<span>NA</span><br />';
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';
            }
            //}, function (error_response) {
            //   showErrorResponseText(error_response);
            //})
        }
        $('#dvOrderSummary').append(order_summary);
    };
    self.artListUploaded = function () {
        var msg = '';
        if (self.gOutputData.standardizeAction.standardizeFileList == undefined || self.gOutputData.standardizeAction.standardizeFileList == null || self.gOutputData.standardizeAction.standardizeFileList == "" || Object.keys(self.gOutputData.standardizeAction.standardizeFileList).length == 0) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select at least one List file.";
            else
                msg = "Please select at least one List file.";
        }
        else {
            var list_file_cnt = 0;
            $.each(self.gOutputData.standardizeAction.standardizeFileList, function (key, val) {
                if (val.fileName != "") {
                    var file_source_type = getFileType(val.sourceType);
                    if (file_source_type.toLowerCase() == "source") {
                        list_file_cnt++;
                    }
                }
            });
            if (list_file_cnt == 0) {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please select at least one List (Source) file.";
                else
                    msg = "Please select at least one List file.";
            }
        }
        if ((jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER) && (self.gOutputData.artworkAction.artworkFileList == undefined || self.gOutputData.artworkAction.artworkFileList == null || self.gOutputData.artworkAction.artworkFileList == "" || Object.keys(self.gOutputData.artworkAction.artworkFileList).length == 0)) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select at least one Art file";
            else
                msg = "Please select at least one Art file";
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupComments').popup('open');");
            return false;
        }
        return true;
    };
}
