﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var serviceURL = "../jobSubmitOrder/JSON/_submitOrder.JSON";
var customerEmailsURL = "../jobSubmitOrder/JSON/_customerEmails.JSON";
var gCheckoutServiceURL = serviceURLDomain + "api/CheckOut/";
//var gCheckoutServiceURL = serviceURLDomainInternal + "api/CheckOut/";
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var pageObj;
var pricingInfo = {};
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobSubmitOrder').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobSubmitOrder');
    loadingImg("_jobSubmitOrder");
    if (sessionStorage.pricingListInfo != undefined && sessionStorage.pricingListInfo != null && sessionStorage.pricingListInfo != "") {
        pricingInfo = $.parseJSON(sessionStorage.pricingListInfo);
    }
    pageObj = new jobSubmitOrder();
    pageObj.getApproversList();
    pageObj.makeEmails();
    pageObj.showExecuteConfirmationToAdmin();
    //pageObj.displayOrderSummary();
    if (sessionStorage.approvalCheckoutPrefs == undefined || sessionStorage.approvalCheckoutPrefs == null || sessionStorage.approvalCheckoutPrefs == "")
        if (appPrivileges.customerNumber == "203" || appPrivileges.customerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || appPrivileges.customerNumber == BBB_CUSTOMER_NUMBER || appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER || appPrivileges.customerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || appPrivileges.customerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
            getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, CW_CUSTOMER_NUMBER);
        else
            getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, appPrivileges.customerNumber);
    else
        managePagePrefs(jQuery.parseJSON(sessionStorage.approvalCheckoutPrefs));

    if (sessionStorage.userRole != 'admin') {
        $('#btnCountApprovalClear').hide();
        $('#btnArtApprovalClear').hide();
        $('#btnExecute').css('visibility', 'hidden');
        $('#btnExecute').css("display", "");        
    }
    if (sessionStorage.userRole == 'user' || (jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER)) {
        $('#btnProductionRelease').css('display', 'none');
    }
    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        $('#dvConfirmationEmailData').css('display', 'none');
        if ((jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) && appPrivileges.roleName == "user")
            $('#btnSubmitOrder').css('display', 'none');
        //$('#pSubmitInfoMsg').text('Please review your order below. If both artwork and list upload sections have been completed you are ready to submit your order. Click the Submit Order button below and a confirmation and approval email will be sent.');
        window.setTimeout(function loadHints() {
            createDemoHints("jobSubmitOrder");
        }, 50);
        setDemoHintsSliderValue();
        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
            //$('#dvOrderSummary').css('display', 'none');
            $('#dvConfirmationEmailData').css('display', 'none');
            if (sessionStorage.userRole == 'user')
                $('#dvDisplayEmail').css('display', 'none');
        }
    }
    else {
        $('#dvOrderSummary').css('display', 'none');
        if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
            $('#dvConfirmationEmailData').css('display', 'none');
        }

    }

    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });

    if (sessionStorage.userRole == 'admin') {
        $('#fsApprovalTypes').css("display", "block"); //display approval checkboxes only for admins
    }
});

$(document).on('pageshow', '#_jobSubmitOrder', function (event) {
    pageObj.getJobReadinessStatus("");
    pageObj.makeGData();
    persistNavPanelState();
    if (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) {// || jobCustomerNumber == SK_CUSTOMER_NUMBER
        $("#tblOrderGrid").css("display", "none");
        $('#tblOrderGrid thead').css('display', 'none');
        $("#tblOrderGridTotals").css("display", "none");
        $('#tblOrderGridTotals thead').css('display', 'none');
    }
    //if (sessionStorage.userRole != 'admin') {
    //    $('#btnSaveTop').addClass('ui-first-child');
    //}
    $('#btnSaveTop').addClass('ui-last-child');

    //Hidden continue for all users check later
    $('#btnContinueTop').css('display', 'none');
    $('#btnContinue').css('display', 'none');
    $('#btnPreviewMailing').css('display', 'none'); //PreviewMailing hidden for all customers
    if (appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER && sessionStorage.userRole == 'admin') {
        //Hidden PreviewMailing for admin aag
        $('#btnPreviewMailing').css('display', 'none');
        $('#btnSaveTop').addClass('ui-first-child');
    }
    var submit_order_msg = "Please review your order below. If both artwork and list upload sections have been completed you are ready to submit your order. Click the Submit Order button below and a confirmation and approval email will be sent.";
    if (jobCustomerNumber == OH_CUSTOMER_NUMBER) {
        submit_order_msg = 'Please review your order below. If the list and art upload sections have been completed, you are ready to submit your order. Click the Submit Order button and wait for the success message.';
    }
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER) {
        submit_order_msg = 'Please review your order below. If the list upload section has been completed you are ready to submit your order. Click the Submit Order button and wait for the success message. Once submitted you will receive a confirmation email.';
        if (appPrivileges.roleName == "user") {
            submit_order_msg = "Please review your order below. If all variable areas have been completed you are ready to submit your order. Click the Submit Order button below and a confirmation and approval email will be sent.";
        }
    }
    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        submit_order_msg = 'Please review your order below.  If the cost and quantity are correct for each location, click "Submit Order".  Confirmation and approval emails will be sent to the appropriate individuals.';
    }
    $('#pSubmitInfoMsg').html(submit_order_msg);
    //    var hint_text = 'When you submit your order, by clicking the "Submit Order" button, an email will be sent to the user placing the order. This will include a Job Summary Report';
    //    hint_text += (jobCustomerNumber == BBB_CUSTOMER_NUMBER) ? ' and link to the Proofing Module to review your artwork' : '.';
    //    $('#hintText').html(hint_text);

    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        $('#approveEmailDemoHints p a').text('Turn Hints Off');
    }

    if (!dontShowHintsAgain && (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isSubmitOrderDemoHintsDisplayed == undefined || sessionStorage.isSubmitOrderDemoHintsDisplayed == null || sessionStorage.isSubmitOrderDemoHintsDisplayed == "false")) {
        $('#approveEmailDemoHints').popup('open', { positionTo: '#txtEmails' });
        sessionStorage.isSubmitOrderDemoHintsDisplayed = true;
    }
    $('#sldrShowHints').slider("refresh");
    getNextPageInOrder();
});
//******************** Page Load Events End **************************