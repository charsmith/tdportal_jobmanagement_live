﻿var makeSubmitOrder = function (self) {
    self.getPricing = function (qty) {
        var price = "0.000";
        $.each(pricingInfo, function (key1, val1) {
            price = "0.000";
            $.each(val1, function (key2, val2) {
                if (parseFloat(qty) >= parseFloat(val2.minCount.replace(',', '')) && parseFloat(qty) <= parseFloat(val2.maxCount.replace(',', ''))) {
                    price = val2.value;
                    return false;
                }
                else if (val2.maxCount == "unbound" && parseFloat(qty) >= parseFloat(val2.minCount.replace(',', ''))) {
                    price = val2.value;
                    return false;
                }
            });
            if (parseFloat(price) > 0.000)
                return false;

        });
        return ((price.toString().indexOf('$') == -1) ? '$' : '') + price;
    }
    self.makeGData = function () {
        if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
            var temp_json = {
                "checkOutAction": {
                    "checkOutApproval": null,
                    "storePaymentInfoList": [],
                    "name": "checkout1",
                    "type": "checkout",
                    "isEnabled": true
                },
                "checkOutGrid": {
                    "finalInvoiceAmountPretty": "$0.00",
                    "totalPostageReceivedPretty": "$0.00",
                    "totalBillingReceivedPretty": "$0.00",
                    "remainingBalancePretty": "$00.00",
                    "checkOutGridStoreList": []
                }
            }
        }
        $.getJSON(serviceURL, function (data) {
            self.gData = data;
            if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != "" && sessionStorage.jobSetupOutput != null) {
                var output_json = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                getCORS(serviceURLDomain + "api/Locations_dropDown/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + output_json.jobNumber + "/0/2/" + output_json.jobTypeId, null, function (data) {
                    if (data.locationsSelected != undefined && data.locationsSelected != null && data.locationsSelected != "")
                        output_json.selectedLocationsAction.selectedLocationsList = data.locationsSelected;
                    sessionStorage.jobSetupOutput = JSON.stringify(output_json);
                }, function (error_response) {
                    showErrorResponseText(error_response, true);
                });
                var selected_stores = output_json.selectedLocationsAction.selectedLocationsList;
                var inhome_date = output_json.inHomeDate;
                var total_amt = 0;
                var total_qty = 0;
                var templates_list = jQuery.parseJSON(sessionStorage.templates);                
                var selected_template = $.grep(templates_list, function (obj) {
                    return obj.value === output_json.templateName;
                });
                if (selected_stores != undefined && selected_stores.length > 0) {
                    $.each(selected_stores, function (a, b) {
                        self.gData.selectedLocationsList.push({
                            "city": b.city,
                            "state": b.state,
                            "originalIndex": b.originalIndex,
                            "quantity": 10,//b.quantity,
                            "address1": b.address1,
                            "zip": b.zip,
                            "storeName": b.storeName,
                            "storeId": b.storeId,
                            "storePk": b.pk
                        });
                        //    //TO BE REMOVED LATER 
                        if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
                            temp_json.checkOutAction.storePaymentInfoList.push({
                                "postageReceivedPretty": "$0.00",
                                "billingReceivedPretty": "$0.00",
                                "billingCheck": 0,
                                "postageCheck": 0,
                                "city": b.city,
                                "state": b.state,
                                "storeName": null,
                                "storeId": b.storeId
                            });
                            temp_json.checkOutGrid.checkOutGridStoreList.push({
                                "templateName": selected_template[0].text,
                                "quantityPretty": "0",                                
                                "estQuantityPretty": "1000",
                                "productionCPPPretty": (output_json.templateName.toLowerCase().indexOf('5_5x11') > -1 ? "$0.235" : "$0.335"),
                                "postageCPPPretty": "$0.00",
                                "additionalStoreChargePretty": "$0.00",
                                "optionalChargesPretty": "$0.00",
                                "dataChargesPretty": "$0.00",
                                "totalPretty": "$240",
                                "storeName": "",
                                "storeId": b.storeId
                            });
                            temp_json.checkOutGrid.checkOutGridStoreList.push({
                                "templateName": selected_template[0].text + "<br/>(Shipped to Store Location)",
                                "quantityPretty": "500",
                                "estQuantityPretty": "0",
                                "productionCPPPretty": '$' + (output_json.templateName.toLowerCase().indexOf('5_5x11') > -1 ? parseFloat(57.50/500) : parseFloat(97.50/500)),
                                "postageCPPPretty": "$0.00",
                                "additionalStoreChargePretty": "$0.00",
                                "optionalChargesPretty": "$0.00",
                                "dataChargesPretty": "$0.00",
                                "totalPretty": (output_json.templateName.toLowerCase().indexOf('5_5x11') > -1 ? "$57.50" : "$97.50"),
                                "storeName": "",
                                "storeId": b.storeId
                            });
                        }
                        else if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                            temp_json.checkOutAction.storePaymentInfoList.push({
                                "postageReceivedPretty": "$0.00",
                                "billingReceivedPretty": "$0.00",
                                "billingCheck": 0,
                                "postageCheck": 0,
                                "city": b.city,
                                "state": b.state,
                                "storeId": b.storeId
                            });
                            var row_info =
                            {
                                "quantityPretty": (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? "3,000" : ((jobCustomerNumber == SK_CUSTOMER_NUMBER) ? b.targeted.replace(/\B(?=(\d{3})+(?!\d))/g, ",") : "4,256"),
                            };
                            total_qty += (row_info.quantityPretty != "") ? parseInt(row_info.quantityPretty.replace(',', '')) : 0;
                            //if (jobCustomerNumber != SK_CUSTOMER_NUMBER)
                            row_info["estQuantityPretty"] = (jobCustomerNumber != SK_CUSTOMER_NUMBER) ? "5000" : 0;
                            row_info["productionCPPPretty"] = (jobCustomerNumber == SK_CUSTOMER_NUMBER) ? "" : "$0.48";
                            row_info["totalPretty"] = (jobCustomerNumber != SK_CUSTOMER_NUMBER) ? "$2,057" : "";
                            if (jobCustomerNumber != SK_CUSTOMER_NUMBER)
                                total_amt += parseFloat(row_info["totalPretty"].replace(',', ''));
                            row_info["storeId"] = ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? ((b.storeName != undefined && b.storeName != "") ? b.storeName : b.location) : b.storeId)
                            //if (jobCustomerNumber != SK_CUSTOMER_NUMBER) {
                            row_info["postageCPPPretty"] = (jobCustomerNumber == SK_CUSTOMER_NUMBER) ? "$0.225" : "$0.00";
                            row_info["optionalChargesPretty"] = (sessionStorage.userRole != 'admin') ? '$0.00' : [{
                                expenseName: "Additional",
                                rateType: "",
                                value: ""
                            }];
                            //}
                            temp_json.checkOutGrid.checkOutGridStoreList.push(row_info);
                        }
                        //    //END
                    });
                    if (jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
                        var cost_per_price = "";
                        cost_per_price = self.getPricing(total_qty);
                        $.each(temp_json.checkOutGrid.checkOutGridStoreList, function (key, val) {
                            if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
                                val.productionCPPPretty = cost_per_price;
                            }
                            if (val.quantityPretty != "0") {
                                val.totalPretty = '$' + (val.quantityPretty.replace('$', '').replace(',', '') * (parseFloat(val.productionCPPPretty.replace('$', '')) + parseFloat(val.postageCPPPretty.replace('$', '')))).toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            }
                            else {
                                val.totalPretty = '$' + (val.estQuantityPretty.toString().replace('$', '').replace(',', '') * (parseFloat(val.productionCPPPretty.replace('$', '')) + parseFloat(val.postageCPPPretty.replace('$', '')))).toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            }

                            total_amt += parseFloat(val.totalPretty.replace(',', '').replace('$', ''));
                        });
                        //$('#spnPricingNote').css("display", "block");
                    }

                    if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                            var quantity = parseInt(temp_json.checkOutGrid.checkOutGridStoreList[0].quantityPretty.replace(',', ''));
                            self.quantity = (quantity * selected_stores.length).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            var estd_qty = (temp_json.checkOutGrid.checkOutGridStoreList[0].estQuantityPretty) ? parseInt(temp_json.checkOutGrid.checkOutGridStoreList[0].estQuantityPretty.replace(',', '')) : 0;
                            self.estQuantity = (estd_qty * selected_stores.length).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            temp_json.checkOutGrid.finalInvoiceAmountPretty = '$' + (selected_stores.length * 2057) + '.00';
                        }
                        else if (jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
                            temp_json.checkOutGrid.finalInvoiceAmountPretty = '$' + total_amt.toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        }
                        else {
                            temp_json.checkOutGrid.finalInvoiceAmountPretty = '$' + (selected_stores.length * 2057) + '.00';
                        }

                        var final_cost = "";
                        var final_quantity = 0;
                        var final_estd_qty = 0;
                        $.each(temp_json.checkOutGrid.checkOutGridStoreList, function (key, val) {
                            final_quantity += parseInt(val.quantityPretty.replace(',', ''));
                            final_estd_qty += parseInt(val.estQuantityPretty.toString().replace(',', ''));// (final_estd_qty != 0) ? (parseInt(final_estd_qty) + parseInt(val.estQuantityPretty.replace(',', ''))) : parseInt(val.estQuantityPretty.replace(',', ''));
                            final_cost = (final_cost != "") ? (parseFloat(final_cost) + parseFloat(val.productionCPPPretty.replace('$', ''))) : parseFloat(val.productionCPPPretty.replace('$', ''));
                        });
                        self.quantity = final_quantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        self.estQuantity = final_estd_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        self.cost = '$' + final_cost;
                    }
                }
                //gData.pricingId = output_json.pricingId;
                self.gData["jobTemplate"] = (output_json.templateName != undefined) ? output_json.templateName : ((output_json.template != undefined) ? output_json.template : '');
                self.gData["mailstreamAction"] = output_json.mailstreamAction;
                if (output_json.mailstreamAction != undefined) {
                    self.gData.isFirstClassMail = (output_json.mailstreamAction.mailClass != undefined && output_json.mailstreamAction.mailClass == "1") ? 1 : 0;
                }
                if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                    self.gData = temp_json;
                    self.loadSubmitOrder();
                    $("#tblOrderGrid").css("display", "block");
                    $("#tblOrderGrid").css("display", "");
                    ko.applyBindings(self);
                    //$("#tblOrderGrid").trigger('create');
                    $("#tblOrderGrid").table("refresh");
                    $("#tblOrderGridTotals").table("refresh");
                    self.makeGOutputData();
                    $("#tblOrderGrid").find('[type=text]').textinput();
                    //if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
                    //   $("#tblOrderGrid").css("display", "none");
                    //}
                }
                else {
                    postCORS(gCheckoutServiceURL + self.facilityId + '/' + ((appPrivileges.customerNumber == "203") ? CW_CUSTOMER_NUMBER : jobCustomerNumber) + '/' + self.jobNumber + '/asdf', JSON.stringify(self.gData), function (response) {

                        //TO BE removed once we get the udpated service - START
                        //if (sessionStorage.userRole == 'admin') {
                        //    $.each(response.checkOutGrid.checkOutGridStoreList, function (key, val) {
                        //        val.optionalChargesPretty = [];
                        //        val.optionalChargesPretty.push({
                        //            expenseName: "Additional",
                        //            rateType: "",
                        //            value: ""
                        //        });
                        //    });
                        //}
                        if (appPrivileges.customerNumber == CW_CUSTOMER_NUMBER) {
                            var final_cost = "";
                            var final_quantity = 0;
                            var final_estd_qty = "";
                            $.each(response.checkOutGrid.checkOutGridStoreList, function (key, val) {
                                final_quantity += parseInt(val.quantityPretty.replace(',', ''));
                                final_cost = (final_cost != "") ? (parseFloat(final_cost) + parseFloat(val.productionCPPPretty.replace('$', ''))) : parseFloat(val.productionCPPPretty.replace('$', ''));
                                final_estd_qty += parseInt(val.estQuantityPretty.replace(',', ''));
                            });
                            self.quantity = final_quantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            self.estQuantity = final_estd_qty.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            self.cost = '$' + final_cost;
                            response.checkOutGrid.checkOutGridStoreList
                        }
                        //$.each(response.checkOutGrid.checkOutGridStoreList, function (key, val) {
                        //    val.optionalChargesPretty = [];
                        //    val.optionalChargesPretty.push({
                        //        expenseName: "Additional",
                        //        rateType: "",
                        //        value: ""
                        //    });
                        //});
                        //END
                        self.gData = response;
                        self.loadSubmitOrder();
                        $("#tblOrderGrid").css("display", "block");
                        $("#tblOrderGrid").css("display", "");
                        ko.applyBindings(self);
                        $("#tblOrderGrid").table("refresh");
                        $("#tblOrderGrid").trigger('create');
                        self.makeGOutputData();
                        // window.setTimeout(function () {
                        $("td div.ui-input-text").css('margin-top', '0px');
                        $("td div.ui-input-text").css('margin-bottom', '0px');
                        // }, 1000);
                    }, function (error_response) {
                        //$('#divError').css('display', 'block');
                        showErrorResponseText(error_response, true);
                        $("#tblOrderGrid").css("display", "none");
                        var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
                            return customer.jobCustomerNumber == jobCustomerNumber;
                        });
                        var checkout_grid_stores = [];
                        var temp_checkout_grid_store_list = {};
                        $.each(customerDetails[0].gridColIds, function (key, val) {
                            if (val != "storeId")
                                temp_checkout_grid_store_list[val] = (val != "quantityPretty") ? "$0.00" : "0";
                            else
                                temp_checkout_grid_store_list[val] = "";
                        });
                        checkout_grid_stores.push(temp_checkout_grid_store_list);
                        var temp_checkout_info = {
                            "checkOutAction": {
                                "checkOutApproval": null,
                                "storePaymentInfoList": [],
                                "name": "checkout1",
                                "type": "checkout",
                                "isEnabled": true
                            },
                            "checkOutGrid": {
                                "finalInvoiceAmountPretty": "$0.00",
                                "totalPostageReceivedPretty": "$0.00",
                                "totalBillingReceivedPretty": "$0.00",
                                "remainingBalancePretty": "$0.00",
                                "checkOutGridStoreList": checkout_grid_stores
                            }
                        }
                        //temp_checkout_info = { "checkOutAction": { "checkOutApproval": null, "storePaymentInfoList": [{ "postageReceivedPretty": "$0.00", "billingReceivedPretty": "$0.00", "billingCheck": 0, "postageCheck": 0, "city": "Auburn", "state": "WA", "storeName": null, "storeId": ""}], "name": "checkout1", "type": "checkout", "isEnabled": true }, "checkOutGrid": { "finalInvoiceAmountPretty": "$684.00", "totalPostageReceivedPretty": "$0.00", "totalBillingReceivedPretty": "$0.00", "remainingBalancePretty": "$684.00", "checkOutGridStoreList": [{ "quantityPretty": "1,500", "productionCPPPretty": "$0.235", "postageCPPPretty": "$0.217", "additionalStoreChargePretty": "$0.00", "optionalChargesPretty": "$0.00", "dataChargesPretty": "$4.00", "totalPretty": "$684.00", "storeName": "Poulsbo RV", "storeId": ""}]} };
                        self.gData = temp_checkout_info;
                        self.loadSubmitOrder();
                        //if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
                        //    $("#tblOrderGrid").css("display", "block");
                        //    $("#tblOrderGrid").css("display", "");
                        //}
                        ko.applyBindings(self);
                        self.makeGOutputData();
                    });
                }
            }
        });
    };

    //Calculate final EstQty and final Invoice amount when changing estimated quantity.
    self.calculateSubTotal = function (data, event) {
        var ctrl = event.currentTarget || event.target;
        var context = ko.contextFor(ctrl).$parent;
        if (data.quantityPretty != undefined && data.quantityPretty != null && data.quantityPretty() == 0) {
            if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER)
                data.totalPretty('$' + (ctrl.value.replace('$', '').replace(',', '') * parseFloat(data.productionCPPPretty().replace('$', ''))).toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            else
                data.totalPretty('$' + (ctrl.value.replace('$', '').replace(',', '') * (parseFloat(data.productionCPPPretty().replace('$', '')) + parseFloat(data.postageCPPPretty().replace('$', '')))).toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            //calculate final est quantity
            var final_est_qty = 0.00;
            $.each($('#tbRowData input[type=text]'), function () {
                final_est_qty += parseFloat(($(this).val() != "" ? $(this).val() : 0).toString().replace('$', '').replace(',', ''));
            });
            context.finalEstdQuantity(final_est_qty.toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            //calculate final invoice amount
            var final_invoice_amount = 0.00;
            $.each($('#tbRowData tr'), function (key, val) {
                final_invoice_amount += parseFloat($(this).find('td:nth-child(' + parseInt($(val).find('td').length) + ')').attr('data-val').replace('$', '').replace(',', ''));
            });
            context.finalInvoiceAmount('$' + final_invoice_amount.toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }
    }

    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined)
            $.getJSON(gOutputServiceUrl, function (dataOutput) {
                self.gOutputData = dataOutput;
                self.gOutputData.checkOutAction = self.gData.checkOutAction;
            });
        else {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            var is_firsttime = false;
            if (self.gOutputData.checkOutAction == undefined) { self.gOutputData["checkOutAction"] = {}; is_firsttime = true; }
            self.gOutputData.checkOutAction = self.gData.checkOutAction;

            if (!is_firsttime) {
                updateStorePaymentInfoInSession();
                self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                self.gData.checkOutAction = self.gOutputData.checkOutAction;
                if (self.gData.checkOutAction.storePaymentInfoList != undefined && self.gData.checkOutAction.storePaymentInfoList != null && self.gData.checkOutAction.storePaymentInfoList != "") {
                    $.each(self.gData.checkOutAction.storePaymentInfoList, function (key, val) {
                        $.each(val, function (key1, val1) {
                            if (key1.indexOf('Pretty') > -1) {
                                if (val1.indexOf('$') == -1)
                                    self.gData.checkOutAction.storePaymentInfoList[key][key1] = '$' + val1;
                                if (val1.indexOf('.00') == -1)
                                    self.gData.checkOutAction.storePaymentInfoList[key][key1] = self.gData.checkOutAction.storePaymentInfoList[key][key1] + '.00';

                            }
                        });
                    });
                }
            }

            if (sessionStorage.jobSetupOutputCompare != undefined) {
                var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
                goutput_compare_data.checkOutAction = self.gOutputData.checkOutAction;
                sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
            }
        }
        delete self.gOutputData.checkoutAction; //TODO: to be discussed with Jim. We are getting this attribute in templateOnchange web service

        //if (self.gOutputData.modulesAction != undefined && self.gOutputData.modulesAction != null && self.gOutputData.modulesAction != "" && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != "") {
        //    //getCustNavLinks();
        //    //createNavLinks();
        //    displayNavLinks();
        //}
        //else {
        //    displayNavLinks();
        //}
        if (self.gOutputData != null) {
            getBubbleCounts(self.gOutputData);
        }
        self.displayOrderSummary();
    };

    //to get Customer email JSON
    self.makeEmails = function () {
        $.getJSON(customerEmailsURL, function (data) {
            self.customerDetailsData = data;
        });
    };

    self.previewMailing = function () {
        if (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER) {//Todo: Need to update pdf Kubota customer 
            encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
        }
        if (appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
        }
        getNextPageInOrder();
        var preview_proof = "../jobApproval/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    }

    self.getApproversList = function () {
        var approvers_list_url = serviceURLDomain + "api/Tagging_approvers/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + self.jobNumber + "/null";
        getCORS(approvers_list_url, null, function (response_data) {
            self.approversList = (response_data != undefined && response_data != null && response_data != "" && response_data.Approvers != undefined && response_data.Approvers != null && response_data.Approvers != "" && Object.keys(response_data.Approvers).length > 0) ? response_data.Approvers : [];
            if (self.approversList.length == 0) {
                self.approversList = { "artApprover": "", "listViewer": "", "artViewer": "", "listApprover": "" };
            }
            if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
                response_data.Approvers.artApprover = "admin_al";
                response_data.Approvers.artViewer = "admin_al";
            }
            if (self.approversList["artViewer"] == undefined) self.approversList["artViewer"] = "";
            if (self.approversList["listViewer"] == undefined) self.approversList["listViewer"] = "";
            if (self.approversList["artApprover"] == undefined) self.approversList["artApprover"] = "";
            if (self.approversList["listApprover"] == undefined) self.approversList["listApprover"] = "";
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };

    self.showExecuteConfirmationToAdmin = function () {
        var msg_box = '';
        msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="confirmAdmin" data-overlay-theme="d" data-history="false" data-theme="c" data-dismissible="false" style="max-width:520px;" class="ui-corner-all">' +
                    '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogHeader">' +
                    '	<h1>Warning!</h1>' +
                    '</div>' +
                    '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
                    '<div id="dvMsg"></div>' +
                    '	<div align="right"><a href="#" data-role="button" data-inline="true" data-mini="true" id="btnCancelPopup"  data-theme="a"  onclick="$(\'#confirmAdmin\').popup(\'close\') ">Cancel</a>' +
                    '	<a href="#" data-role="button" data-inline="true" data-mini="true"  id="btnContinuePopup" data-theme="a" data-transition="flow" onclick="ko.contextFor(this).$data.getJobReadinessStatus(\'submit\');" >Submit Order</a></div>' +
                    '</div>' +
                '</div>';
        $("#_jobSubmitOrder").append(msg_box);
    };
};