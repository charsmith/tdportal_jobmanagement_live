﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gEmailListsServiceUrl = "JSON/_emailLists.JSON";
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var gServiceEmailListsData;
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_emailLists').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayNavLinks();
    displayMessage('_emailLists');
    createConfirmMessage("_emailLists");

    getCORS(serviceURLDomain + "api/Customer/", null, function (data) {
        companyData = data;
        $('#companyTemplate').tmpl(companyData).appendTo('#ddlCustomer');
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
    $.getJSON(gEmailListsServiceUrl, function (data) {
        gServiceEmailListsData = data;
    });
});

$(document).on('pageshow', '#_emailLists', function (event) {
    window.setTimeout(function bindUsersInfo() {
        $('#ddlCustomer').val(jobCustomerNumber).selectmenu('refresh');
        if (appPrivileges.customerNumber != "1") {
            $('#ddlCustomer').attr('disabled', 'disabled');
        }
        pageObj = new loadEmailListsInfo();
        ko.applyBindings(pageObj);
        window.setTimeout(function () {
            $('#ulEmailLists div').trigger('create');
            $('#ulEmailLists').listview('refresh');
            //$('#ulMultipleJobsInvoice').listview('refresh');
        }, 50);
    }, 1000);


});

var emailInfo = function (email_group_name, email_group_type) {
    var self = this;
    self.emailGroupName = ko.observable({});
    self.emailGroupName(email_group_name);
    self.emailGroupType = ko.observable({});
    self.emailGroupType(email_group_type);
};

var loadEmailListsInfo = function () {
    var self = this;
    self.displayEmailListsInfo = ko.observableArray([]);
    var temp_data = {};
    $.each(gServiceEmailListsData, function (key, val) {
        var job_attrs = "";
        var counter = 0;
        var attr_val = "";
        $.each(val, function (key1, val1) {
            self.displayEmailListsInfo.push(new emailInfo(val1.emailGroupName, val1.emailGroupType));
        });
    });

    self.emailGroupClick = function (data) {
        sessionStorage.selectedEmailGroup = data.emailGroupName();
        window.location.href = " ../adminEmailLists/emailRecipients.html";
    };

    self.addNewListClick = function () {
        $('#popupNewList').popup('open');
    };

    self.addNewList = function (data, event) {
        var temp_list = new emailInfo($('#txtListName').val(), '');
        //Service Call goes here. In the success the below code has to written.
        data.displayEmailListsInfo.push(temp_list);
        $('#ulEmailLists').listview('refresh');
        sessionStorage.selectedEmailGroup = $('#txtListName').val();
        $('#popupNewList').popup('close');
        window.location.href = " ../adminEmailLists/emailRecipients.html";
    };

    self.showHomePage = function (type) {
        if (type == "home") {
            window.location.href = "../index.htm";
        }
        else {
            window.location.href = " ../adminEmailLists/emailLists.html";
        }
    };

    self.customerChanged = function () {
    };
};


