﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;


//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
gServiceUrl = '../JSON/_jobSetup.JSON';
gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
gTemplateUrlForAll = serviceURLDomain + 'api/Template';
var gVerifyJobNameUrl = serviceURLDomain + "api/JobTicket_conflicts/";
var orderPAckageServiceUrl = "JSON/orderingPackage.JSON";
var pricingListServiceUrl = "JSON/pricingList.JSON";
var packageInfo;
var pricingListInfo = {};
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobSelectTemplate').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobSelectTemplate');
    var dt_html = "<span id='spnInHome'>*</span>";
    $('#schedDate').html(dt_html + appPrivileges.label_scheduledDate.toLowerCase().replace("inhome", "In Home").replace("data", "Date").replace("date", "Date")); //coming from config.xml file dynamically

    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) {
        $.getJSON(pricingListServiceUrl, function (data) {
            pricingListInfo = data[jobCustomerNumber];
            sessionStorage.pricingListInfo = JSON.stringify(pricingListInfo);
        });
    }
    //    if (customerNumber == DATAWORKS_CUSTOMER_NUMBER || customerNumber == CASEYS_CUSTOMER_NUMBER) { //also for casey's //posted to page preferences
    //        $('#divEstQty').css("visibility", "hidden");
    //    }
    $('#divEstQty').css("display", "none");
    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) { //also for casey's show
        dt_html = "<span id='spnOfferExp'>*</span>";
        $('#lblOfferExp').html(dt_html + "Offer Expiration");
        $('#dvOfferExp').css("display", "block");  //posted to page preferences
    }

    if ((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) && sessionStorage.jobNumber != "-1") {
        $('#txtJobName').addClass('ui-disabled');
        $('#txtJobName').css('opacity', '0.7');

        $('#inHomeDate').addClass('ui-disabled');
        $('#inHomeDate').css('opacity', '0.7');
    }
    //    if (sessionStorage.selectTemplatePrefs == undefined || sessionStorage.selectTemplatePrefs == null || sessionStorage.selectTemplatePrefs == "")
    //        if (customerNumber == REGIS_CUSTOMER_NUMBER || customerNumber == "203")
    //            getPagePreferences('selectTemplate.html', sessionStorage.facilityId, CW_CUSTOMER_NUMBER);
    //        else
    //            getPagePreferences('selectTemplate.html', sessionStorage.facilityId, customerNumber);
    //    else
    //        managePagePrefs(jQuery.parseJSON(sessionStorage.selectTemplatePrefs));


    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
        $('#dvOrderType').css("display", "block");
        $('#schedDate').html(dt_html + "1st In Home Date:");
    }
    //test for mobility...
    //loadMobility();
    //$('#txtJobName').ForceAlphaNumeric();
    if ((jobCustomerNumber == REGIS_CUSTOMER_NUMBER)) {
        //$('#dvSalonBrand').attr("style", "display:block;");  //posted to page preferences
        //getSalonBrand();
        $('#dvSalonBrand').attr("style", "display:none;");
    }
    else {
        $('#dvSalonBrand').attr("style", "display:none;");
    }
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    pageObj = new selectTemplate();
    
    if ((jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)) {
        setDemoHintsSliderValue();
        window.setTimeout(function loadHints() {
            createDemoHints("jobSelectTemplate");
        }, 200);
    }

    $("navHints").bind({
        popupafterclose: function (event, ui) {
            $('#walkthroughSteps').popup('open', { positionTo: '#namingHint' });
        }
    });
});
function getTemplatePackageInfo() {
    $.getJSON(orderPAckageServiceUrl, function (data) {
        packageInfo = data;
    });
}
$(document).keydown(function (e) {
    if (e.keyCode === 8) {
        var d = e.srcElement || e.target;
        if (jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) {
            if ($(d).attr('id') == "inHomeDate" || e.target.nodeName == "DIV")
                return false;
        }
       else {
            if (($(d).attr('id') == "inHomeDate" || e.target.nodeName == "DIV") && ($(d).attr('id') != "_jobSelectTemplate"))
                return false;
        }
    }
});
$(document).on('pageshow', '#_jobSelectTemplate', function (event) {
    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        $('div[data-role="content"] h2').html("Templates");
    }
    jQuery("#inHomeDate").datepicker({
        minDate: new Date(2010, 0, 1),
        dateFormat: 'mm/dd/yy',
        constrainInput: true,
        beforeShowDay: function (date) {
            if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER)
                return displayBlockDays(date, 0);
            else if (sessionStorage.userRole != "admin")
                return displayBlockDays(date, ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? 15 : ((jobCustomerNumber == OH_CUSTOMER_NUMBER) ? 7 : 21)));
            else
                return displayBlockDays(date, 1);
        }
        //,
        //onSelect: function () {
        //    fnInhomeDate();
        //}
    });
    if (jobCustomerNumber == ALLIED_CUSTOMER_NUMBER) {
        jQuery("#inHomeDate2").datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: function (date) {
                if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER)
                    return displayBlockDays(date, 0);
                else if (sessionStorage.userRole != "admin")
                    return displayBlockDays(date, 21);
                else
                    return displayBlockDays(date, 1);
            }
            //,
            //onSelect: function () {
            //    fnInhomeDate();
            //}
        });
        jQuery("#inHomeDate3").datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: function (date) {
                if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER)
                    return displayBlockDays(date, 0);
                else if (sessionStorage.userRole != "admin")
                    return displayBlockDays(date, 21);
                else
                    return displayBlockDays(date, 1);
            }
            //,
            //onSelect: function () {
            //    fnInhomeDate();
            //}
        });
    }
    if (jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != SPORTSAUTHORITY_CUSTOMER_NUMBER && jobCustomerNumber != WALGREENS_CUSTOMER_NUMBER && jobCustomerNumber != REDPLUM_CUSTOMER_NUMBER && jobCustomerNumber != SAFEWAY_CUSTOMER_NUMBER && jobCustomerNumber != LOWES_CUSTOMER_NUMBER) {
        if (sessionStorage.userRole != "admin" && sessionStorage.jobNumber == "-1")
            $("#inHomeDate").datepicker('setDate', '+' + ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? 15 : 21) + 'd');
        else
            $("#inHomeDate").datepicker('setDate', '+1d');
    }
    else {
        $("#dvInHomeDate").css('display', 'none');
    }
    // if (sessionStorage.selectTemplatePrefs != undefined && sessionStorage.selectTemplatePrefs != null && sessionStorage.selectTemplatePrefs != "null" && sessionStorage.selectTemplatePrefs != "") {
    if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER)
        $("#spanPricingNote").css('display', 'block');

    if (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) {
        $('#navHints p a').text('Turn Hints Off');
        $('#walkthroughSteps p a').text('Turn Hints Off');
        $('#dvWelcomeDemoPopup p a').text('Turn Hints Off');
    }

    if (!dontShowHintsAgain && (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isTemplateDemoHintsDisplayed == undefined || sessionStorage.isTemplateDemoHintsDisplayed == null || sessionStorage.isTemplateDemoHintsDisplayed == "false")) {
        window.setTimeout(function () {
            $('#dvWelcomeDemoPopup').popup('open');
        }, 500);
        sessionStorage.isTemplateDemoHintsDisplayed = true;
        //$('#navHints').popup('open', { positionTo: '#navHint' });
        //$('#btnNavHints').trigger('click');
    }


    //show ONLY for TD.    
    //if ((customerNumber == "1")) {
    if ((jobCustomerNumber == "1")) {
        $('#divCustomer').attr("style", "display:block;");
        $('#divCustomer').removeAttr('disabled');  //posted to page preferences
        if (sessionStorage.jobNumber != "-1")
            $('#divCustomer').attr('disabled', 'disabled');
        // getCustomerInfo();
        pageObj.loadCustomerInfo();
    }
    pageObj.loadData();
    //}

    //if (!jQuery.browser.msie) {
    //    pagePrefs = jQuery.parseJSON(sessionStorage.selectTemplatePrefs);
    //    loadData();
    //}
    if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == "203" || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == TRIBUNEPUBLISHING_CUSTOMER_NUMBER || jobCustomerNumber == SANDIEGO_CUSTOMER_NUMBER || jobCustomerNumber == ACQUIREDHEALTH_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == ALLIED_CUSTOMER_NUMBER || jobCustomerNumber == REDPLUM_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) //posted to  page preferences
        $('#divEstQty').hide();

    if ((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) && sessionStorage.userRole != "admin") //for CGS and normal user should not get Save button in any of the pages
        $('#btnSaveTop').css('visibility', 'hidden'); //posted to  page preferences

    $('#inHomeDate').removeClass('ui-disabled'); //TODO: need to check the code while doing page preferences
    $('#inHomeDate').css('opacity', '1.0');
    $('#txtJobName').removeClass('ui-disabled');
    $('#txtJobName').css('opacity', '1.0');

    if (parseInt(sessionStorage.jobNumber) > -1) {
        $('#txtJobName').addClass('ui-disabled');
        $("#txtJobName").attr('disabled', true);
        $('#txtJobName').css('opacity', '0.7');
    }
    persistNavPanelState();
    
    //    var temp_default_json = (sessionStorage.defaultJobDetails != undefined && sessionStorage.defaultJobDetails != null && sessionStorage.defaultJobDetails != "") ? $.parseJSON(sessionStorage.defaultJobDetails) : {};
    //    if (Object.keys(temp_default_json).length > 0) {
    //        $('#txtJobName').addClass('ui-disabled');
    //        $("#txtJobName").attr('disabled', true);
    //        $('#txtJobName').css('opacity', '0.7');
    //        if (temp_default_json.defaultJobNumber == "-1") {
    //            $('#txtJobName').val(temp_default_json.defaultJobName);
    //        }
    //    }
    $('#sldrShowHints').slider("refresh");
});
function manageDemoHints() {
    $('#navHints').popup('close');
    $('#walkthroughSteps').popup('open', { positionTo: '#namingHint' });
}

//******************** Page Load Events End **************************
