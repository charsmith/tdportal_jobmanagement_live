﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gCompanyInfoServiceUrl = "../jobCompanyInfo/JSON/_companyInfo.JSON";
var gServiceCompanyData;
var gOutputData;
var pageObj;
var companyData;
var primaryFacilityData;
var loanOfficersList = [];
var jobLoInfo;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL Variables End **************************
//Sports KING Default JSON
var temp_cmp_info = {
    "companyInfo": {
        "locationHint": "LOCATED INSIDE HOMETOWN SQUARE", "address1": "123 Main Street", "address2": "", "city": "Anytown",
        "state": "US", "zip": "12345", "phone": "(888) 888-8888", "website": "SportsKing.com"
    },
    "variableDict": {
        "frontText": "We love baseball almost as much as you do.",
        "backHeader": "Bat a Thousand.\nShop SportsKing.",
        "backText": "We know how much you love baseball.\nWe love it too.\nthat's why we carry everything the avid baseball player needs — all at a great low price."
    },
    "isEnabled": true,
    "name": "companyInfo"
};
//******************** Page Load Events Start **************************
$('#_jobCustomText').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_jobCustomText');
    createConfirmMessage("_jobCustomText");
    loadingImg('_jobCustomText');

    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        $.getJSON("../JSON/_aagEnhancementsJobTicket.JSON", function (data) {
            jobLoInfo = data;
        });
    }
    setTimeout(function () {
        getCompanyInfo();
    }, (jobCustomerNumber == AAG_CUSTOMER_NUMBER) ? 1800 : 0);
    displayNavLinks();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
    createDemoHints("jobCustomText");
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER && appPrivileges.roleName == "user") {
        $('a[title="Loan Officers"]').hide();
        $('a[title="Ordered Locations"]').hide();
    } else if (jobCustomerNumber != AAG_CUSTOMER_NUMBER) {
        $('a[title="Loan Officers"]').hide();
    }
    else {
        $('a[title="Ordered Locations"]').hide();
    }
});

$(document).on('pageshow', '#_jobCustomText', function (event) {
    var pageObj_time = (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) ? 2300 : 300;
    window.setTimeout(function () {
        pageObj = new loadCompanyInfo();
        //pageObj.getSelectedInfo();
        ko.applyBindings(pageObj);
        //pageObj.loanOfficerChanged();
        $('#ddlLoanOfficers').trigger('change');
        $('input[type=checkbox]').trigger('create');
        $('#dvCmpInfo').trigger('create');
        $('#navbar').trigger('create');
        $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
        $('#chkReplicate').checkboxradio('refresh');
        $('#ddlLocations').selectmenu('refresh');
        //window.setTimeout(function () {
        $('input[type=text]').parent().css('margin', '0px');
        $('select').parent().parent().css('margin', '0px');
        //}, 1000);

        jQuery('input[data-fieldType=date]').datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: function (date) {
                return displayBlockDays(date, 0);
            }
        });

    }, pageObj_time);

    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
            $('#dvNavBar').css('display', 'block');
            $("label[for='chkReplicate']").text("Replicate Company Info For All Events");
        }
        else {
            $('#dvNavBar').css('display', 'none');
            $('#dvReplicate').css('display', 'none');
        }
        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
            $('a[data-icon="myapp-locations"]').css('display', 'none');
            $('a[data-icon="eye"]').addClass('ui-first-child');
        }
    }
    persistNavPanelState();
    if (!dontShowHintsAgain && (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
                          && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" ||
                              sessionStorage.showDemoHints == "on")
                          && (sessionStorage.isCustomTextDemoHintsDisplayed == undefined || sessionStorage.isCustomTextDemoHintsDisplayed == null ||
                              sessionStorage.isCustomTextDemoHintsDisplayed == "false")) {
        sessionStorage.isCustomTextDemoHintsDisplayed = true;
        $('#customTextHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#customTextHints').popup('open', { positionTo: '#dvCmpInfo' });
        }, 500);
    }
});

function validateInput(event) {
    if (event.id == 'txtAwarded_Amount:_$')
        event.value = event.value.replace(/[^0-9]+/g, '');
    
    return true;
}

var getCompanyInfo = function () {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);

        if (jobCustomerNumber == SK_CUSTOMER_NUMBER && self.gOutputData.variableTextAction != undefined) {
            if (self.gOutputData.variableTextAction.variableDict != undefined && self.gOutputData.variableTextAction.variableDict != null && Object.keys(self.gOutputData.variableTextAction.variableDict).length == 0)
                self.gOutputData.variableTextAction.variableDict = temp_cmp_info.variableDict;
           //Need to be updated based on the dictionary returned from service.
            self.gOutputData.variableTextAction.companyInfoDict = temp_cmp_info.companyInfo;
        }

        if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            if (gOutputData.variableTextAction != undefined && gOutputData.variableTextAction != null && gOutputData.variableTextAction != "") {
                gServiceCompanyData = gOutputData.variableTextAction;
            }
        }
        else {
            if (gOutputData.companyInfoAction != undefined && gOutputData.companyInfoAction != null && gOutputData.companyInfoAction != "") {
                gServiceCompanyData = gOutputData.companyInfoAction;
            }
        }
    }
    else if (sessionStorage.templateSetupConfigJSON != undefined && sessionStorage.templateSetupConfigJSON != null && sessionStorage.templateSetupConfigJSON != "") {
        gServiceCompanyData = $.parseJSON(sessionStorage.templateSetupConfigJSON);
    } else {
        $.getJSON(gCompanyInfoServiceUrl, function (data) {
            $.each(data, function (key, val) {
                if (key.indexOf(sessionStorage.customerNumber) > -1) {
                    gServiceCompanyData = val;
                }
            });
        });
    }
};

ko.bindingHandlers.fireChange = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var bindings = allBindingsAccessor();
        if (bindings.value != null) {
            if ($(element).val().length > 200 || $(element).val().indexOf('\n') > -1) {
                $(element).css("height", "180px");
            }
        }
    }
};

var locationVm = function (store_id, store_name, store_pk) {
    var self = this;
    self.locationId = store_pk;
    if (store_id != undefined && store_id != null && store_id != "")
        self.locationName = store_id + ' - ' + store_name;
    else
        self.locationName = store_name;
};

var fieldInfo = function (label, value, is_required, show_chkbox) {
    var self = this;
    self.label = ko.observable('');
    self.label(label);
    self.value = ko.observable('');
    self.value(value);
    self.isRequired = ko.observable('');
    self.isRequired(is_required);
    self.isEditable = ko.observable('');
    self.isEditable("true");
    //if (label.toLowerCase() == "toll-free number" && appPrivileges.roleName == "admin")
    //    self.isEditable("true");
    //else
    //if (label.toLowerCase() == "toll-free number" && appPrivileges.roleName == "user") {
    //    self.isEditable("false");
    //}

    //else if (label.toLowerCase() == "toll-free number" && appPrivileges.roleName != "admin")
    //self.isEditable(false);
};

var loanOfficer = function (user_id, first_name, last_name) {
    var self = this;
    self.loanOfficerId = user_id;
    self.loanOfficerName = first_name + ' ' + last_name;
};

var loadCompanyInfo = function () {
    var self = this;
    self.locationsList = ko.observableArray([]);
    self.varCompanyInfo = ko.observable({});
    self.prevVarCompanyInfo = ko.observable({});
    self.selectedInHomeStoreCompanyInfo = ko.observable({});
    self.selectedInhomeInfo = ko.observable({});
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.selectedLocation = ko.observable({});
    self.isFirstTime = ko.observable(true);
    self.selectedStore = ko.observable({});
    self.isReplicated = ko.observable();
    self.loanOffiersList = ko.observableArray([]);
    self.selectedLoanOfficer = ko.observable({});
    self.previousSelectedStore = ko.observable({});
    var temp_arr = [];
    var temp_arr1 = ko.observableArray([]);
    var temp_key = "";
    var index = 0;
    var temp_company_info = {};
    var show_chk = false;
    var is_object = false;
    var temp_key2 = "";
    var company_info = {};

    self.getPageDisplayName = function (page_name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var page_display_name = '';
        $.each(new String(page_name), function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                page_display_name += ((!page_display_name.endsWith(' ') && !page_display_name.endsWith('-')) ? ' ' : '') + value;
            }
            else {
                page_display_name += value;
            }
        });
        return page_display_name;
    };
    if (sessionStorage.loanOffiersList != undefined && sessionStorage.loanOffiersList != null && sessionStorage.loanOffiersList != "") {
        loanOfficersList = $.parseJSON(sessionStorage.loanOffiersList);
        $.each(loanOfficersList, function (key, val) {
            if (key == 0) {
                self.selectedLoanOfficer(val.userId);
            }
            self.loanOffiersList.push(new loanOfficer(val.userId, val.firstName, val.lastName));
        });
    }

    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
            $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                if (key == 0) {
                    self.selectedLocation(val.pk);
                    self.selectedStore(val.pk);
                    self.previousSelectedStore(self.selectedStore());
                }
                self.locationsList.push(new locationVm(val.storeId, val.storeName, val.pk));
            });
        }
    }

    if (gOutputData != undefined && gOutputData != null) {
        var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
        var cnt = -1;
        //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
        if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
            $.each(gOutputData.companyInfoAction, function (key, val) {
                if (key.toLowerCase().indexOf('event') > -1) {
                    var temp_list = {};
                    cnt++;
                    temp_list[key] = "Event " + (cnt + 1);
                    self.inHomeDatesList.push({
                        "showCheckIcon": false,
                        'dateValue': "Event " + (cnt + 1)
                    });

                    if (cnt == 0)
                        nav_bar_items += '<li><a href="#" data-icon="check">Event ' + (cnt + 1) + ' </a></li>';
                    else
                        nav_bar_items += '<li><a href="#" data-icon="edit">Event ' + (cnt + 1) + ' </a></li>';
                }
            });
        }
        else {
            if (jobCustomerNumber != SK_CUSTOMER_NUMBER) {
                $.each(gOutputData, function (key, val) {
                    if (key.toLowerCase().indexOf('inhome') > -1) {
                        var temp_list = {};
                        temp_list[key] = val;
                        self.inHomeDatesList.push({
                            "showCheckIcon": false,
                            'dateValue': val
                        });
                        cnt++;
                        if (cnt == 0)
                            nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                        else
                            nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
                    }
                });
            }
        }

        nav_bar_items += '</ul></div>';
        self.currentInHome(self.inHomeDatesList()[0]);
        if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
            //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
            //    var idx = 0;
            //    var main_idx = 0;
            //    var temp_id = {};
            //$.each(jobLoInfo.customText, function (key1, val1) {
            //    $.each(gOutputData.companyInfoAction, function (key2, val2) {
            //        $.each(val2, function (key3, val3) {
            //            //if (idx == main_idx) {
            //            temp_id[main_idx]=
            //            val3[main_idx].companyInfo = val1[0].companyInfo;
            //            idx++;
            //            // }
            //        });
            //        idx = 0;
            //    });
            //    main_idx++;
            //});


            $.each(gOutputData.companyInfoAction, function (key1, val1) {
                if (key1 != "isReplicated") {
                    gOutputData.companyInfoAction[key1] = {};
                    gOutputData.companyInfoAction[key1] = jobLoInfo.customText;
                    //$.each(val1, function (key2, val2) {
                    //    val2 = jobLoInfo.customText;
                    //});
                }
            });


            gServiceCompanyData = gOutputData.companyInfoAction;
            //} else {
            //    //gOutputData.companyInfoAction[self.currentInHome().dateValue][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
            //    //gServiceCompanyData[self.currentInHome().dateValue][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
            //    $.each(gOutputData.companyInfoAction, function (key1, val1) {
            //        if (key1 != "isReplicated") {
            //            gOutputData.companyInfoAction[key1] = {};
            //            gOutputData.companyInfoAction[key1] = jobLoInfo.customText;
            //            //$.each(val1, function (key2, val2) {
            //            //    val2 = jobLoInfo.customText;
            //            //});
            //        }
            //    });
            //}
        }
    }

    self.buildCompanyInfo = function () {
        if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            var temp_company_info = {};
            temp_company_info["isReplicated"] = false;
            $.each(gServiceCompanyData, function (key, val) {
                if (key.toLowerCase() != "isreplicated" && key.toLowerCase() != "isenabled" && key.toLowerCase() != "name" && key.toLowerCase() != "type") {
                    var temp_arr = [];
                    var lbl = "";
                    $.each(val, function (key1, val1) {
                        lbl = self.getPageDisplayName(key1).initCap();
                        temp_arr.push(new fieldInfo(lbl, val1, true, false));
                    });
                    //Need to be updated below based on the dictionary returned from service.
                    if (key == "companyInfoDict") {
                        //temp_company_info["companyInfo"] = temp_arr;
                    }
                    else
                        temp_company_info["companyInfo"] = temp_arr;
                        //temp_company_info["customInfo"] = temp_arr;
                }
            });
            var temp = [];
            temp.push(temp_company_info);
            company_info = temp;
        }
        else {
            $.each(gServiceCompanyData, function (key, val) {
                if (key.toLowerCase() != "isreplicated") {
                    company_info[key] = {};
                    $.each(val, function (key1, val1) {
                        $.each(val1, function (key2, val2) {
                            getDataFromJSON(val2, key, key1, val1, key2, val2);
                            //if (typeof (val1[key2]) == "object") {
                            //    if (temp_key == "")
                            //        temp_key = key2;
                            //    if (temp_key != key2) {
                            //        temp_company_info[temp_key] = temp_arr;
                            //        temp_key = key2;
                            //        temp_arr = [];
                            //    }
                            //    if (temp_key == "companyInfo")
                            //        show_chk = true;
                            //    else
                            //        show_chk = false;
                            //    if (key2 != "name" && key2 != "isEnabled") {
                            //        $.each(val2, function (inner_key1, inner_val1) {
                            //            temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
                            //        });
                            //        if (index == (Object.keys(val1).length - 1)) {
                            //            temp_company_info[temp_key] = temp_arr;
                            //            temp_key = key2;
                            //            temp_arr = [];
                            //        };
                            //    }
                            //    index++;
                            //}
                            //else {
                            //    if (temp_key == "")
                            //        temp_key = key2;
                            //    if (temp_key != key2) {
                            //        temp_company_info[temp_key] = temp_arr;
                            //        temp_key = key2;
                            //        temp_arr = [];
                            //    }
                            //    if (temp_key == "companyInfo")
                            //        show_chk = true;
                            //    else
                            //        show_chk = false;
                            //    if (key2 != "name" && key2 != "isEnabled") {
                            //        $.each(val2, function (inner_key1, inner_val1) {
                            //            temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
                            //        });
                            //        if (index == (Object.keys(val1).length - 1)) {
                            //            temp_company_info[temp_key] = temp_arr;
                            //            temp_key = key2;
                            //            temp_arr = [];
                            //        };
                            //    }
                            //    index++;
                            //}
                        });

                        if (!is_object) {
                            company_info[key][key1] = ko.observable({});
                            if (Object.keys(temp_company_info).length > 0) {
                                company_info[key][key1](temp_company_info);
                                temp_company_info = {};
                            }
                        }
                    });
                }
                else {
                    if (jobCustomerNumber != AAG_CUSTOMER_NUMBER)
                        if (val == true)
                            self.isReplicated(true);
                        else
                            self.isReplicated(false);
                }
            });
        }
        if (Object.keys(company_info).length > 0) {
            self.varCompanyInfo(company_info);
            var temp = ko.toJS(company_info);
            self.prevVarCompanyInfo(temp);
            company_info = {};
        }
    }

    self.buildCompanyInfo();

    function getDataFromJSON(data, key, key1, val1, key2, val2) {
        //if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        index = 0;
        if (key2 != "name" && key2 != "isEnabled") {
            is_object = true;
            temp_key2 = key2;
            var is_list = false;
            $.each(val2, function (key3, val3) {
                // $.each(val3, function (key4, val4) {
                if (temp_key == "")
                    temp_key = key3;
                if (temp_key != key3) {
                    temp_company_info[temp_key] = temp_arr;
                    temp_key = key3;
                    temp_arr = [];
                }
                if (temp_key == "companyInfo")
                    show_chk = true;
                else
                    show_chk = false;
                if (typeof (val3) == "object" && val3.length > 0)
                    is_list = true;
                else
                    is_list = false;
                temp_arr = (is_list) ? [] : {};
                if (key3 != "name" && key3 != "isEnabled") {
                    $.each(val3, function (inner_key1, inner_val1) {
                        temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
                    });
                    if (index == (Object.keys(val2).length - 1)) {
                        temp_company_info[temp_key] = temp_arr;
                        temp_key = key3;
                        temp_arr = (is_list) ? [] : {};
                    };
                }
                else {
                    temp_company_info[temp_key] = val3;
                }

                index++;
                //});
            });
            if (company_info[key][key1] == undefined) {
                company_info[key][key1] = [];
                //company_info[key][key1] = {};
            }
            //company_info[key][key1][temp_key2] = ko.observable({});
            if (Object.keys(temp_company_info).length > 0) {
                //company_info[key][key1][temp_key2](temp_company_info);
                //if(is_list)
                company_info[key][key1].push(temp_company_info);
                //else
                //                      company_info[key][key1][temp_key2](temp_company_info);
                //company_info[key][key1][temp_key2].push(temp_company_info);
                temp_company_info = {};
                is_list = false;
            }
            temp_key = "";
        }
        else {
            //if (temp_key == "")
            temp_key = key2;
            //if (temp_key != key2) {
            company_info[key][key1][temp_key] = temp_arr;
            temp_key = key2;
            temp_arr = [];
            //}
        }

        //}
        //else {
        //    if (temp_key == "")
        //        temp_key = key2;
        //    if (temp_key != key2) {
        //        temp_company_info[temp_key] = temp_arr;
        //        temp_key = key2;
        //        temp_arr = [];
        //    }
        //    if (temp_key == "companyInfo")
        //        show_chk = true;
        //    else
        //        show_chk = false;
        //    if (key2 != "name" && key2 != "isEnabled") {
        //        $.each(val2, function (inner_key1, inner_val1) {
        //            temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
        //        });
        //        if (index == (Object.keys(val1).length - 1)) {
        //            temp_company_info[temp_key] = temp_arr;
        //            temp_key = key2;
        //            temp_arr = [];
        //        };
        //    }
        //    index++;
        //}

    };

    self.confirmContinue = function (current_page, page_name, i_count, click_type) {
        var ctrls = $('#dvNavBar div ul li a.ui-icon-edit');
        if (ctrls.length > 0 && click_type == "onlycontinue" && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) {
            var tmp_list = "";
            $.each(ctrls, function (key, val) {
                tmp_list += (tmp_list != "") ? (key < (ctrls.length - 1)) ? ', ' : ' and ' + $(this).attr('data-inHome') : $(this).attr('data-inHome');
            });

            $('#confirmMsg').html('You have not completed setup for ' + tmp_list + '. Do you want to stay on this page to continue setup or go to the next page?');
            $('#okButConfirm').text('Go To Next Page');
            $('#cancelButConfirm').text('Continue Setup');

            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#popupConfirmDialog').popup('close');
            });
            $('#okButConfirm').bind('click', function () {
                $('#okButConfirm').unbind('click');
                $('#popupConfirmDialog').popup('close');
                goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
                self.updateCompanyInfo(current_page, page_name, i_count, click_type);
            });
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        else {
            //goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
            return self.updateCompanyInfo(current_page, page_name, i_count, click_type);
        }
    };
    self.updateCompanyInfo = function (current_page, page_name, i_count, click_type) {
        if (jobCustomerNumber != SK_CUSTOMER_NUMBER)
            self.validateForDataExistency();
        var is_valid = false;
        // Call validation function to validate the data in the fields.  If it returns true proceed further else stop further execution.
        var temp_json = ko.mapping.toJS(self.varCompanyInfo);
        if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            temp_json = (temp_json.length > 0) ? temp_json[0] : [];
            var tmp_json = {};
            var key_name = "";
            $.each(temp_json, function (key, val) {
                tmp_json = {};
                if (key != "isReplicated") {
                    $.each(val, function (key1, val1) {
                        key_name = val1.label.replace(' ', '').initLower();
                        tmp_json[key_name] = val1.value;
                    });
                    //if (key != "companyInfo") { //This should not be changed.
                    if (key == "companyInfo") { //This should not be changed.
                        gOutputData.variableTextAction.variableDict = tmp_json;
                    }
                    else {
                        //Need to be updated below based on the dictionary returned from service.
                        //gOutputData.variableTextAction["companyInfoDict"] = tmp_json;
                    }
                }
            });
        } else {
            gOutputData.companyInfoAction = temp_json;
        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        if (click_type == "onlycontinue")
            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
        return true;
    };
    self.navBarItemClick = function (data, event) {
        self.validateForDataExistency();
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var ele = event.target || event.targetElement;
        $('#dvNavBar div ul li a').removeClass('ui-btn-active');
        $(ele).addClass('ui-btn-active');
        if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 1)) {
            self.selectedInHomeStoreCompanyInfo({});
        }
        self.currentInHome(ko.dataFor(ele));
        self.getSelectedInfo();
    }

    self.locationChanged = function (data, event) {
        self.validateForDataExistency();
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        var selected_store = $(ele).val();
        self.selectedStore(self.selectedLocation());
        self.getSelectedInfo();

        var curr_info = ko.toJS(self.varCompanyInfo());
        var prev_info = ko.toJS(self.prevVarCompanyInfo());
        var diff_between_json_objects = [];
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            $.each(prev_info, function (key2, val2) {
                if (key1 == key2) {
                    $.each(val1, function (key3, val3) {
                        var curr_temp1 = val3;
                        var prev_temp2 = val2[key3];
                        diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                        if (diff_between_json_objects.length != 0) {
                            is_diff_found = true;
                            return false;
                        }
                    });
                }
                if (is_diff_found) {
                    return false;
                }
            });
            if (is_diff_found) {
                return false;
            }
        });

        if (is_diff_found) {
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
        }
        else {
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
        }
    };

    self.loanOfficerChanged = function (data, event) {
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        displayJobInfo();
        var header_text = $('#navHeader').text();
        header_text = header_text + (($("#ddlLoanOfficers option:selected").text() != '') ? ' (' + $("#ddlLoanOfficers option:selected").text() + ')' : '');
        $('#navHeader').text(header_text);
        var selected_lo = $(ele).val();
        self.selectedLoanOfficer(selected_lo);
        //self.prevVarCompanyInfo({});
        self.selectedInHomeStoreCompanyInfo({});
        self.getSelectedInfo();
    };

    self.loadSelectedLoanOfficerInfo = function () {
        //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
        //    gOutputData.companyInfoAction["Event 1"][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //    gOutputData.companyInfoAction["Event 2"][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //    gServiceCompanyData["Event 1"][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //    gServiceCompanyData["Event 2"][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //}
        //else {
        //    gOutputData.companyInfoAction[self.currentInHome().dateValue][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //    gServiceCompanyData[self.currentInHome().dateValue][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //}
        //self.varCompanyInfo()[self.currentInHome().dateValue][self.selectedStore()] = [];
        //self.prevVarCompanyInfo({});
        //self.selectedInHomeStoreCompanyInfo({});
        //self.buildCompanyInfo();
        //self.getSelectedInfo();
        $('#popUpLoanOfficers').popup('close');
    }

    self.getSelectedInfo = function () {
        //if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        //    self.getSeletedLoanOfficerInfo();
        //}
        var is_found = false;
        self.selectedInHomeStoreCompanyInfo({});
        $('div.ui-grid-a').find('label,input,textarea').remove();
        if (jobCustomerNumber != SK_CUSTOMER_NUMBER) {
            var selected_inhome = self.currentInHome().dateValue;

            $.each(self.varCompanyInfo(), function (key, val) {
                if (key == selected_inhome) {
                    self.selectedInhomeInfo(val);
                    $.each(val, function (key1, val1) {
                        //if (typeof (ko.toJS(val1)) == "object") {
                        //if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
                        // $.each(val1, function (key2, val2) {
                        //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
                        //    //var temp_arr = ko.observableArray([]);
                        //    //temp_arr.push(val1[self.selectedLoanOfficer() - 1]);
                        //    self.selectedInHomeStoreCompanyInfo([]);
                        //    self.selectedInHomeStoreCompanyInfo.push(val1[self.selectedLoanOfficer() - 1]);
                        //    is_found = true;
                        //    return false;
                        //}
                        //else
                        if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
                            if (key1 == self.selectedLoanOfficer()) {
                                self.selectedInHomeStoreCompanyInfo(val1);
                                is_found = true;
                                return false;
                            }
                        }
                        else {
                            if (key1 == self.selectedStore()) {
                                self.selectedInHomeStoreCompanyInfo(val1);
                                is_found = true;
                                return false;
                            }
                        }
                        //});
                        //}
                        //else {
                        //    if (key1 == self.selectedStore()) {
                        //        self.selectedInHomeStoreCompanyInfo(val1());
                        //        is_found = true;
                        //        return false;
                        //    }
                        //}
                    });
                    if (is_found)
                        return false;
                }
            });
            var temp = ko.toJS(self.varCompanyInfo());
            self.updateInHomeNavBarUI(temp);
            $('div').trigger('create');
        }
        else {
            self.selectedInHomeStoreCompanyInfo({});
            self.selectedInHomeStoreCompanyInfo(self.varCompanyInfo());
            $('#dvCmpInfo').trigger('create');
        }
    };

    self.updateInHomeNavBarUI = function (temp) {
        $.each(temp, function (key, val) {
            var is_valid = self.verifyData(val);
            if (is_valid) {
                $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
            }
            else {
                $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
            }
        });
    };

    self.replicateChanged = function (data, event) {
        var ele = event.target || event.targetElement;
        $('#confirmMsg').html('Replicating Setup Entries will overwrite any previous information entered.');
        $('#okButConfirm').text('Continue');

        var curr_info = ko.toJS(self.varCompanyInfo());
        var prev_info = ko.toJS(self.prevVarCompanyInfo());
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            $.each(prev_info, function (key2, val2) {
                if (key1 == key2) {
                    $.each(val1, function (key3, val3) {
                        var curr_temp1 = val3;
                        var prev_temp2 = val2[key3];
                        diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                        if (diff_between_json_objects.length != 0) {
                            is_diff_found = true;
                            return false;
                        }
                    });
                }
                if (is_diff_found) {
                    return false;
                }
            });
            if (is_diff_found) {
                return false;
            }
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.replicateData();
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        if ($(ele).is(':checked') && is_diff_found) {
            $('#popupConfirmDialog').popup('open');
        }
    };
    self.replicateData = function () {
        var selected_inhome = self.currentInHome().dateValue;
        $.each(self.selectedInhomeInfo(), function (key1, val1) {
            var temp_key = key1;
            $.each(self.varCompanyInfo(), function (key2, val2) {
                if (selected_inhome != key2) {
                    //val2[temp_key](val1());
                    val2[temp_key] = val1;
                }
            });
        });
        //self.varCompanyInfo().isReplicated(true);
        var temp = ko.toJS(self.varCompanyInfo());
        self.prevVarCompanyInfo(temp);
        $.each(temp, function (key, val) {
            if (key != "isReplicated") {
                var is_valid = self.verifyData(val);
                if (is_valid) {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
                }
                else {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
                }
            }
        });
    };

    self.validateForDataExistency = function () {
        var temp_info = ko.toJS(self.selectedInhomeInfo);
        var is_valid = self.verifyData(temp_info);
        var selected_inhome = self.currentInHome().dateValue;
        if (is_valid) {
            $('#dvNavBar div ul li a[data-inHome="' + selected_inhome + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
        }
        else {
            $('#dvNavBar div ul li a[data-inHome="' + selected_inhome + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
        }
    };

    self.verifyData = function (temp_info) {
        var is_data_found = false;
        $.each(temp_info, function (key1, val1) {
            $.each(val1, function (key2, val2) {
                $.each(val2, function (key3, val3) {
                    if (key3.toLowerCase() == "companyinfo" || key3.toLowerCase() == "custominfo") {
                        $.each(val3, function (key4, val4) {
                            if (val4.value == "") {
                                is_data_found = false;
                                return false;
                            }
                            else {
                                is_data_found = true;
                            }
                        });
                        if (!is_data_found) {
                            return false;
                        }
                    }
                });
            });
            if (!is_data_found) {
                return false;
            }
        });
        return is_data_found;
    };

    self.showOrderedLocations = function () {
        $('#popUpLocations').popup('open');
    };
    self.showLoanOfficers = function () {
        $('#popUpLoanOfficers').popup('open');
    };

    self.manageLocationsPopup = function (data, event) {
        var ele = event.target || event.targetElement;
        if ($(ele).attr('data-btnType') == "submit") {
            self.previousSelectedStore(self.selectedStore());
        }
        else {
            self.selectedStore(self.previousSelectedStore());
            self.selectedLocation(self.previousSelectedStore())
        }
        $('#ddlLocations').selectmenu('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        var sel_location = $('#ddlLocations option:selected').text();
        header_text = header_text + ' (' + sel_location + ')';
        $('#navHeader').text(header_text);
        $('#popUpLocations').popup('close');
    };
    self.previewMailing = function () {
        //Todo: Need to update pdf file for Kubota customer
        var encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
        if (appPrivileges.customerNumber == SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER) {
            previewMailing();
			return false;
        }
        if (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER) {
            encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
        }
        if (appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
        }
        getNextPageInOrder();
        //var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
        var preview_proof = "../jobApproval/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    }
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if ((type == "template")) {
        window.location.href = " ../adminTemplateSetup/templateSetup.html";
    }
    else if ((type == "vcc")) {
        window.location.href = " ../adminTemplateSetup/variableCompanyInfo.html";
    }
};
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
//Capitalizes the first letter in the given string.
String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};
String.prototype.initLower = function () {
    return this.replace(/(?:^|\s)[A-Z]/g, function (m) {
        return m.toLowerCase();
    });
};
