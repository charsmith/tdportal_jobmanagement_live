﻿var dragAndDropFiles = function (self) {

    self.ignoreDrag = function (e) {
        e.preventDefault();
    };

    self.makeDrop = function (e, is_multiple, ctrl) {
        var fileList = (!is_multiple) ? (e.originalEvent.dataTransfer.files) : $(ctrl)[0].files;
        e.preventDefault();
        if (!is_multiple) {
            isDragDrop = true;
        }
        var mode = $('#hidMode').val();
        if (mode == "edit") {
            if (temp_data.length > 0) temp_edit_file = temp_data;
            temp_data = [];
            if (fileList.length > 1) {
                $("#popupListDefinition").popup("close");
                $('#validationAlertmsg').html("Please select/drag and drop single file to replace the exising file.");
                window.setTimeout(function getDelay() {
                    $('#validationPopupDialog').popup('open');
                    return false;
                }, 200);
                return false;
            }
        }
        else if ((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && ($('#dvSelectedLocations input[type=checkbox]:not([value=all]):checked').length == 0 && $('#hidMode').val() != "replace")) {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html("Please select at least one location.");
            window.setTimeout(function getDelay() {
                self.resetFileInput();
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);
            return false;
        }
        else {
            var msg = "";
            if (self.uploadMultiplesValidation(true)) {
                //if ($('#fieldType').val() == "select")
                //    msg += "Please select List type.";

                //if (msg != "") {
                //    $("#popupListDefinition").popup("close");
                //    $('#validationAlertmsg').html(msg);
                //    window.setTimeout(function () {
                //        $('#validationPopupDialog').popup('open');
                //    }, 200);
                //    self.resetFileInput();
                //    return false;
                //}

                var source_files_to_upload = 0;
                $.each(self.filesToUpload, function (a, b) {
                    if (getFileType(b.sourceType) == "source")
                        source_files_to_upload++;
                });
                //var total_files_count = source_files_to_upload + temp_data.length + fileList.length;
                var total_files_count = source_files_to_upload + fileList.length;
                if ((total_files_count > self.maxNumberOfFiles) || (self.uploadedSourceFilesCount >= self.maxNumberOfFiles) || ((total_files_count + self.uploadedSourceFilesCount > self.maxNumberOfFiles))) {
                    $("#popupListDefinition").popup("close");
                    msg = "The maximum number of files to be uploaded per order has been reached. Please remove a previously uploaded file in order to upload a new file to this order. If this order requires more than " + self.maxNumberOfFiles + " files, please contact your Account Manager.";
                    $('#validationAlertmsg').html(msg);
                    window.setTimeout(function getDelay() {
                        $('#validationPopupDialog').popup('open');
                    }, 200);
                    self.resetFileInput();
                    return false;
                }
            }
            else
                return false;
        }
        self.generateDropFileList(fileList, is_multiple, ctrl);
    }

    self.generateDropFileList = function (fileList, is_multiple, ctrl) {
        if (fileList && fileList.length > 0) {
            illegalFilesCnt = 0;
            invalidFileTypesCnt = 0;
            existingFilesCnt = 0;
            for (var i = 0; i < fileList.length; i++) {
                if (self.validateFiles(fileList[i].name)) {
                    var is_file_found = $.grep(temp_data, function (obj) {
                        var file_type = ($("#fieldType").val() != "select") ? $("#fieldType").val() : "";
                        return (obj.name.toLowerCase() === fileList[i].name.toLowerCase() && (obj.type.toLowerCase() === file_type.toLowerCase()))
                    });
                    var is_file_found_in_selected = $.grep(self.filesToUpload, function (obj) {
                        var file_type = ($("#fieldType").val() != "select") ? $("#fieldType").val() : "";
                        var file_type_code = getFileTypeCode(file_type);
                        return (($('#hidMode').val() == "edit") ? (obj.fileName.toLowerCase() === temp_edit_file[0].name.toLowerCase() && obj.sourceType === getFileTypeCode(temp_edit_file[0].type)) : (obj.fileName.toLowerCase() === fileList[i].name.toLowerCase() && obj.sourceType === file_type_code));
                    });
                    if (is_file_found.length == 0) {
                        if (is_file_found_in_selected.length > 0) {
                            var file_found = $.grep(data, function (obj) {
                                var file_type = ($("#fieldType").val() != "select") ? $("#fieldType").val() : "";
                                //return (obj.name.toLowerCase() === fileList[i].name.toLowerCase() && (obj.type.toLowerCase() === file_type.toLowerCase()))
                                return (($('#hidMode').val() == "edit") ? (obj.name.toLowerCase() === temp_edit_file[0].name.toLowerCase() && obj.type === temp_edit_file[0].type) : (obj.name.toLowerCase() === fileList[i].name.toLowerCase() && (obj.type.toLowerCase() === file_type.toLowerCase())));
                            });
                            if (file_found.length > 0) {
                                file_found[0].type = ($("#fieldType").val() != "select") ? $("#fieldType").val() : "";
                                file_found[0].file = fileList[i];
                            }
                            else {
                                data.push({
                                    "name": fileList[i].name,
                                    "type": ($("#fieldType").val() != "select") ? $("#fieldType").val() : "",
                                    "fileMapperName": fileList[i].name.substring(fileList[i].name.lastIndexOf('\\') + 1, fileList[i].name.lastIndexOf('.')) + "_map.xml",
                                    "file": fileList[i]
                                });
                            }
                        }
                        else {
                            data.push({
                                "name": fileList[i].name,
                                "type": ($("#fieldType").val() != "select") ? $("#fieldType").val() : "",
                                "fileMapperName": fileList[i].name.substring(fileList[i].name.lastIndexOf('\\') + 1, fileList[i].name.lastIndexOf('.')) + "_map.xml",
                                "file": fileList[i]
                            });
                        }
                        temp_data.push({
                            "name": fileList[i].name,
                            "type": ($("#fieldType").val() != "select") ? $("#fieldType").val() : "",
                            "fileMapperName": fileList[i].name.substring(fileList[i].name.lastIndexOf('\\') + 1, fileList[i].name.lastIndexOf('.')) + "_map.xml",
                            "file": fileList[i]
                        });
                    }
                }
                self.resetFileInput();
            }
            if (temp_data.length > 0) {
                temp_edit_file = [];
                self.createSelectedFilesList();
                self.displayFileList();
            }
            var msg = "<ul>";
            if (illegalFilesCnt > 0) {
                //                if (illegalFilesCnt == 1)
                //                    msg += '<li>One of the selected file has ';
                //                else
                //                    msg += '<li>Few of the selected files have ';
                msg += '<li>Illegal characters exist(s) in file name. Cannot upload.</li>';
                self.resetFileInput();
            }
            if (invalidFileTypesCnt > 0) {
                //                if (invalidFileTypesCnt == 1)
                //                    msg += '<li>One of the selected file is ';
                //                else
                //                    msg += '<li>Few of the selected files are ';
                msg += '<li>Invalid file type selection(s). Only "TXT","CSV","DAT","DATA","LST","XLS","XLSX","DOC","GZ","ZIP" and "DOCX" types are accepted.</li>';
                self.resetFileInput();
            }
            if (existingFilesCnt > 0) {
                msg += '<li>The selected file(s) already exist(s) in the list. Please select other file(s).</li>';
                self.resetFileInput();
            }
            msg += "</ul>";
            if (msg != "<ul></ul>") {
                $("#popupListDefinition").popup("close");
                $('#validationAlertmsg').html(msg);
                window.setTimeout(function getDelay() {
                    $('#validationAlertmsg').addClass('wordwrap');
                    $('#validationPopupDialog').popup('open');
                    return false;
                }, 200);

            }
        }
    };

    self.createSelectedFilesList = function () {
        //self.resetFileUploadControl(self.filesToUpload);
        self.resetFileInput();
        $.each(temp_data, function (key, val) {
            file_name = val.name;
            source_type = ($('#fieldType').val() != "select") ? $("#fieldType").val() : "";
            var source_type_code = getFileTypeCode(source_type);
            var uploading_file_name = "";
            uploading_file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
            var is_file_found_in_selected = $.grep(self.filesToUpload, function (obj) {
                return (obj.fileName.toLowerCase() === file_name.toLowerCase() && obj.sourceType === source_type_code)
            });
            if (is_file_found_in_selected.length == 0) {
                self.uploadingFiles += (self.uploadingFiles != "") ? "|" + uploading_file_name + "^" + source_type_code : uploading_file_name + "^" + source_type_code;
                self.updateFileInfo(file_name, source_type);
            }
            $.each(temp_data, function (key, val) {
                $.each(data, function (key1, val1) {
                    if ((val.name.toLowerCase() == val1.name.toLowerCase()) && (val.type.toLowerCase() == val1.type.toLowerCase())) {
                        val.type = source_type;
                    }
                });
                val.type = source_type;
            });

        });
        //$("#popupListDefinition").popup("close");
        //self.makeFileListBeforePost(self.filesToUpload);
        temp_data = [];
        if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isListFileSelectedFirstTime == undefined || sessionStorage.isListFileSelectedFirstTime == null || sessionStorage.isListFileSelectedFirstTime == "" || sessionStorage.isListFileSelectedFirstTime == "false") && self.filesToUpload.length == 1 && (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == SPORTSAUTHORITY_CUSTOMER_NUMBER || jobCustomerNumber == WALGREENS_CUSTOMER_NUMBER || jobCustomerNumber == SAFEWAY_CUSTOMER_NUMBER || jobCustomerNumber == LOWES_CUSTOMER_NUMBER)) {
            sessionStorage.isListFileSelectedFirstTime = true;
            $('#uploadSelectedListDemoHints').popup('open', { positionTo: '#btnUploadFiles' });
        }
    };

    self.displayFileList = function () {
        var file_list = "<ul data-role='listview' data-theme='c'>";
        var file_type_n_list = '';
        var file_names = "";
        //for (var i = 0; i < temp_data.length; i++) {
        //    var type = getFileTypeCode(temp_data[i].type);
        //    file_list += '<li><a href="" title="' + temp_data[i].name + '">' + temp_data[i].name + '</a><a href="" data-icon="delete" onclick="pageObj.deleteDraggedFile(\'' + temp_data[i].name + '\',' + i + '\,' + type + ')"></a></li>';
        //}
        for (var i = 0; i < self.filesToUpload.length; i++) {
            //var type = getFileTypeCode(self.filesToUpload[i].sourceType);
            file_list += '<li><a href="" title="' + self.filesToUpload[i].fileName + '">' + self.filesToUpload[i].fileName + '</a><a href="" data-icon="delete" onclick="pageObj.deleteDraggedFile(\'' + self.filesToUpload[i].fileName + '\',' + i + '\,' + self.filesToUpload[i].sourceType + ')"></a></li>';
        }
        file_list += '</ul>';
        $('#drop-area').html(file_list);
        window.setTimeout(function () {
            $('#drop-area ul').listview();
        }, 50);
    };


    self.handleReaderOnLoadEnd = function () {
        return function (event) {
            $image.attr("src", this.result)
                .addClass("small")
                .appendTo("#drop-area");
        };
    }
    self.deleteDraggedFile = function (file_name, file_index, type) {
        temp_data = data;
        temp_edit_file.push(temp_data[file_index]);
        temp_data.splice(file_index, 1);
        var file_list = "<ul data-role='listview' data-theme='c'>";
        var file_type_n_list = '';
        var file_names = "";
        var del_temp_data = [];
        for (var i = 0; i < data.length; i++) {
            if (data[i].name != file_name) {
                del_temp_data.push({
                    "name": data[i].name,
                    "type": data[i].type,
                    "file": data[i].file
                });
            }
        }
        var selected_file_index = null;
        if (self.filesToUpload.length > 0) {
            $.each(self.filesToUpload, function (key, val) {
                if (val.fileName.toLowerCase().substring(val.fileName.lastIndexOf('\\') + 1, val.fileName.length) == file_name.toLowerCase() && val.sourceType == type) {
                    selected_file_index = key;
                    return false;
                }
            });
            if (selected_file_index != null) {
                self.filesToUpload.splice(selected_file_index, 1);
                var uploading_files = self.uploadingFiles.split('|');
                self.uploadingFiles = [];
                $.each(uploading_files, function (a, b) {
                    var file_info = b.split('^');
                    if (file_name.toLowerCase() == file_info[0].toLowerCase() && file_info[1] == type) {
                    }
                    else {
                        self.uploadingFiles += (self.uploadingFiles != "") ? '|' + b : b;
                    }
                });
                self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
            }
        }



        //for (var i = 0; i < temp_data.length; i++) {
        //    var type = getFileTypeCode(temp_data[i].type);
        //    file_list += '<li><a href="" title="' + temp_data[i].name + '">' + temp_data[i].name + '</a><a href="" data-icon="delete" onclick="pageObj.deleteDraggedFile(\'' + temp_data[i].name + '\',' + i + '\,' + type + ')"></a></li>';
        //}
        for (var i = 0; i < self.filesToUpload.length; i++) {
            //var type = getFileTypeCode(self.filesToUpload[i].sourceType);
            file_list += '<li><a href="" title="' + self.filesToUpload[i].fileName + '">' + self.filesToUpload[i].fileName + '</a><a href="" data-icon="delete" onclick="pageObj.deleteDraggedFile(\'' + self.filesToUpload[i].fileName + '\',' + i + '\,' + self.filesToUpload[i].sourceType + ')"></a></li>';
        }
        data = del_temp_data;
        file_list += '</ul>';
        $('#drop-area').empty();
        $('#drop-area').html(file_list);
        window.setTimeout(function createList() {
            $('#drop-area ul').listview();
        }, 50);
    };
    var illegalFilesCnt = 0;
    var invalidFileTypesCnt = 0;
    var existingFilesCnt = 0;
    self.validateFiles = function (file_name) {
        var file_name_type = "";
        var file_names = "";
        var check_file_name = (file_name.indexOf('\\') > -1) ? file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.length) : file_name;
        var illegal_filename_regexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;
        var allowed_extension = ["txt", "csv", "dat", "data", "lst", "xls", "xlsx", "doc", "docx", "gz", "zip"];
        if (!illegal_filename_regexp.test(check_file_name.substring(0, check_file_name.lastIndexOf('.')))) {
            illegalFilesCnt++;
            return false;
        }
        else {
            var extension = file_name.split('.');
            var extension = file_name.substring(file_name.lastIndexOf('.') + 1);
            var isValidFileFormat = false;
            var uploading_file_name = "";
            if (typeof (extension) != 'undefined' && extension.length > 1) {
                for (var i = 0; i < allowed_extension.length; i++) {
                    if (extension.toLowerCase() == allowed_extension[i]) {
                        isValidFileFormat = true;
                        break;
                    }
                }
                if (!isValidFileFormat) {
                    invalidFileTypesCnt++;
                    return false;
                }
                else if (self.verifyFileExistencyInUploads(file_name)) {
                    existingFilesCnt++;
                    return false;
                }
                else
                    return true;
            }
            //var filemappername = file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.lastIndexOf('.')) + "_map.xml";
        }
    };

    //Validates the file selection settings.
    self.uploadMultiplesValidation = function (is_browse_click) {
        var msg = "<ul>";
        if (sessionStorage.defaultJobDetails != undefined && sessionStorage.defaultJobDetails != null && sessionStorage.defaultJobDetails != "" && jobCustomerNumber != ACQUIREDHEALTH_CUSTOMER_NUMBER && jobCustomerNumber != TRIBUNEPUBLISHING_CUSTOMER_NUMBER && jobCustomerNumber != SANDIEGO_CUSTOMER_NUMBER)
            if ($('#ddlMailingType').val() == "select")// && (appPrivileges.roleName != "user" && sessionStorage.roleName != "power"))
                msg += "<li>Please select Mailing type.</li>";

        if ($('#fieldType').val() == "select")
            msg += "<li>Please select List type.</li>";

//        if ((appPrivileges.roleName == "admin" || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) && $('#select-choice-1').val() == "select")
            //msg += "<li>Please select File Delimiter.</li>";
        //if (temp_data.length == 0 && !is_browse_click) {
        if (self.filesToUpload.length == 0 && !is_browse_click) {
            if ($('#name').val() == "")
                msg += "<li>Please choose a file to upload.</li>";
        }
        msg += "</ul>";

        if (msg != "<ul></ul>") {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html(msg);
            window.setTimeout(function getDelay() {
                $('#validationAlertmsg').addClass('wordwrap');
                $('#validationPopupDialog').popup('open');
                self.resetFileInput();
                return false;
            }, 200);

        }
        else
            return true;
    };
    self.resetFileInput = function () {
        var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'); // document.getElementById('spnFileInput');
        var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
        var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
        if (fileInput.value != "") {
            // Create a new file input
            var newFileInput = fileInput.cloneNode(true);
            newFileInput.value = null;
            $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
            tdFileInputsTemp.appendChild(newFileInput);
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.navigate(e, $(e.target));
            });
        }

    }
};