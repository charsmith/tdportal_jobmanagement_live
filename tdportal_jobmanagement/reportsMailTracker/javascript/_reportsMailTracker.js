﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
//var reportMailTrackerURL = serviceURLDomain + 'api/FileUploadMw/' + getSessionData("publisherId");
var reportMailTrackerURL = 'JSON/_reportsMailTracker.JSON';
var dropDownJsonURL = 'JSON/_dropDowns.JSON';
var dropDownData;
var pubDialogType = '';
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_plurisMailTracker').live('pagebeforecreate', function (event) {
    //displayMessage('_plurisMailTracker');
    //fnConfirmSignout('_plurisMailTracker');
    
    $('#popupDialog').attr("data-rel", "dialog");
    $('#popupDialog').attr("data-dismissible", "false");
    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage 
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }

    if (sessionStorage.desktop) // desktop storage 
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    else if (localStorage.mobile) // mobile storage
        return true;

    if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
        alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
        window.open('', '_self', '');
        window.close();
        return false;
    }
    //    if (appPrivileges.roleName != "admin")
    //        $("#divFileMap").hide();

    if (sessionStorage["headerValue"] != undefined && sessionStorage["headerValue"] != null && sessionStorage["headerValue"] != "")
        var headerVal = sessionStorage.headerValue;
    $("#headerH2").text(headerVal);
    displayNavLinks();
});

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_plurisMailTracker', function (event) {
    // $('#divHeader').html("File Upload for " + appPrivileges.publisherName + " Publications <BR/");
    makeGData();
    //    $.getJSON(dropDownJsonURL, function (data) {
    //        dropDownData = data;
    //        loadDropDowns(dropDownData);
    //    });

});


//******************** Model Creation Start **************************

var ReportMailTracker = function (target_type_name, week_id, in_home_date, reports, drops) {
    this.targetName = target_type_name.replace(/ /g, '') + ' Week ' + week_id + ' (' + in_home_date + ')';
    this.id = target_type_name.replace(/ /g, '') + '_week' + week_id + '_' + in_home_date.replace(/\//g, '');
    this.reports = ko.observableArray(reports);
    this.drops = ko.observableArray(drops);
}

function createViewModel() {
    var mailtracker_model = [];
    $.each(gData[0], function (key, val) {
        $.each(val.weeks, function (a, b) {
            var report_mail_tracker = new ReportMailTracker(key, b.weekNo, b.inHomeDate, b.reports, b.drops);
            mailtracker_model.push(report_mail_tracker);
        });

    });
    return mailtracker_model;
}

//******************** Model Creation End **************************


//******************** Public Functions Start **************************

function makeGData() {
    //    getCORS(reportMailTrackerURL, null, function (data) {
    //        gData = data;
    //        //ko.applyBindings(createViewModel(), $('#divList')[0]);
    //        $('#divList').empty();
    //        ko.cleanNode($('#divList')[0]);
    //        var model = createViewModel();
    //        ko.applyBindings(model, $('#divList')[0]);
    //        fillPublicationDDL();
    //        //displayList();
    //        $.each($('#divList'), function (obj) {
    //            $(this).collapsibleset();
    //        });
    //        $("#divList ul").each(function (i) {/// <reference path="../JSON/_reports.JSON" />

    //            $(this).listview();
    //        });
    //    }, function (error_response) {
    //        showErrorResponseText(error_response);
    //    });
    $.getJSON(reportMailTrackerURL, function (data) {
        gData = data;
        //ko.applyBindings(createViewModel(), $('#divList')[0]);
        $('#divList').empty();
        ko.cleanNode($('#divList')[0]);
        var model = createViewModel();
        //alert('data' + model);
        ko.applyBindings(model, $('#divList')[0]);
        //alert('data2' + model);
        //fillPublicationDDL();
        //displayList();
        $.each($('#divList'), function (obj) {
            $(this).collapsibleset();
        });
        $.each($('div[data-role=collapsible]'), function (obj) {
            $(this).collapsible();
        });
        $("#divList ul").each(function (i) {
            $(this).listview();
        });
        var query_string = window.location.search.substring(1);
        var id = query_string.split("&");
        if (id)
            $('div.ui-collapsible-content', "#div" + id).trigger('expand');
    });
}