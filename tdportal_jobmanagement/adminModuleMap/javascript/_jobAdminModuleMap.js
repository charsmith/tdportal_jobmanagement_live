﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
//var gServiceUrl = 'http://localhost:53661/Gizmo_Js_110314/adminModuleMap/JSON/_adminModuleMap.JSON';
var gServiceUrl = 'JSON/_adminModuleMap.JSON';
/// <reference path="../JSON/_adminModule.JSON" />

var gData;
var gModulesData = [];
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_jobAdminModuleMap', function (event) {
});
$('#_jobAdminModuleMap').live('pagebeforecreate', function (event) {
    //displayMessage('_jobAdminModule');
    makeGData();
    loadMobility();
});

function makeGData() {
    $.getJSON(gServiceUrl, function (data) {
        gData = data;
        $.each(gData, function (key, val) {
            var a = val;
            gModulesData.push({
                "item": val.list,
                "children": ko.observableArray(val.items)
            });
        });
        $('#dvMapModules').empty();
        ko.cleanNode($('#dvMapModules')[0]);
        ko.applyBindings(gModulesData, $('#dvMapModules')[0]);
        $("#dvMapModules").each(function (i) {
            $(this).listview();
        });
        var index = 0;
        $("select").each(function (i) {
            if(index==0)
                $(this).slider();
            else
                $(this).selectmenu();
            index++;
        });
    });
}

