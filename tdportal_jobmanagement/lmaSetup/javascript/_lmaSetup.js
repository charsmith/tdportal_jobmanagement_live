﻿
$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var gStateServiceUrl = '../JSON/_listStates.JSON';
var gLocationsServiceUrl = 'JSON/_lmaLocations.JSON';
var gZipsServiceUrl = 'JSON/_lmaZips.JSON';
var gAssignedZipsServiceUrl = 'JSON/_lmaAssignedZips.JSON';
var gLocationSearchUrl = serviceURLDomain + 'api/Locations/';
var locationsServiceJobNumber = "-17847";
var totalAvblLocations = [];
var gLocationZipsUrl = serviceURLDomain + 'api/Locations_lma_store/';
var tempLocations = {};
var tempZips = {};
var tempLocationZips = {};
var tempAssignedZips = {};
var pageObj;

var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
var mapping = { 'ignore': ["__ko_mapping__"] };

$('#_lmaSetup').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_lmaSetup');
    loadingImg("_lmaSetup");
    createConfirmMessage("_lmaSetup");
    $.getJSON(gLocationsServiceUrl, function (result) {
        tempLocations = result;
    });
    $.getJSON(gZipsServiceUrl, function (result) {
        tempZips = result;
    });
    $.getJSON(gAssignedZipsServiceUrl, function (result) {
        tempLocationZips = result;
    });
    if (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        setDemoHintsSliderValue();
        window.setTimeout(function () {
            createDemoHints("lmaSetup");
        }, 300);
    }
    //var post_json = {};
    //post_json["state"] = "";
    //post_json["approvalStatusDesc"] = "Available,Opted-In,Pending";
    //post_json["locationListSize"] = 1200;
    //var search_filter_url = gLocationSearchUrl + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + locationsServiceJobNumber + "/0/0/52";
    //postCORS(search_filter_url, JSON.stringify(post_json), function (data) {
    //    totalAvblLocations = data.locationsAvailable;
    //    $.each(data.locationsSelected, function (key, val) {
    //        totalAvblLocations.push(val);
    //    });
    //}, function (error_response) {
    //    showErrorResponseText(error_response, false);
    //});
});

$(document).on('pageshow', '#_lmaSetup', function (event) {
    pageObj = new imaSetup();
    window.setTimeout(function () {
        ko.applyBindings(pageObj);
        $('#ddlState').selectmenu('refresh');
        $('#ddlLocations').selectmenu('refresh');
        window.setTimeout(function () {
            $('#dvPostalBoundarySelections div.ui-collapsible-content').css('padding-top', '0px');
        }, 500);
    }, 500);
    $('#spnUpdatedTimeStamp').text(formatAMPM())

    if (!dontShowHintsAgain && (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isLMADemoHintsDisplayed == undefined || sessionStorage.isLMADemoHintsDisplayed == null || sessionStorage.isLMADemoHintsDisplayed == "false")) {
        window.setTimeout(function () {
            $('#dvWelcomeDemoPopup').popup('open', { positionTo: '#map' });
        }, 500);
        sessionStorage.isLMADemoHintsDisplayed = true;
    }
    mapVerticalResize();
});

function formatAMPM() {
    var d = new Date(),
        minutes = d.getMinutes().toString().length == 1 ? '0' + d.getMinutes() : d.getMinutes(),
        seconds = d.getSeconds().toString().length == 1 ? '0' + d.getSeconds() : d.getSeconds(),
        hours = d.getHours().toString().length == 1 ? '0' + d.getHours() : d.getHours(),
        ampm = d.getHours() >= 12 ? 'PM' : 'AM',
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    return d.getMonth() + '/' + d.getDate() + '/' + d.getFullYear() + ' ' + hours + ':' + minutes + ':' + seconds + ' ' + ampm;
}

function openViewByCountyDemoHint() {
    if (!dontShowHintsAgain && (jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isViewByCountyFirstTime == undefined || sessionStorage.isViewByCountyFirstTime == null || sessionStorage.isViewByCountyFirstTime == "false")) {
        sessionStorage.isViewByCountyFirstTime = true;
        $('#dvViewByCountyPopup').popup('close');
        window.setTimeout(function () {
            $('#dvViewByCountyPopup').popup('open', { positionTo: '#map' });
        }, 1000);
    }
}

var stateVM = function (state) {
    var self = this;
    self.stateCode = state.stateValue;
    self.stateName = state.stateName;
};

var locationVM = function (location) {
    var self = this;
    ko.mapping.fromJS(location, {}, self);
    self.locationPk = ko.observable(location.pk);
    self.computedLocationName = ko.computed(function () {
        deferEvaluation: true
        var full_name = "";
        var store_id = '';
        if (location.storeId != undefined && location.storeId != null && location.storeId != "") {
            store_id = location.storeId + " - ";
        }
        if (location.location != undefined && location.location != null && location.location != "") {
            full_name = location.location.substring(0, 17) + "...";
        }
        if (location.city != undefined && location.city != null && location.city != "") {
            full_name += (full_name != "") ? ' (' + location.city + ')' : location.city;
        }

        full_name = store_id + full_name;
        full_name = ((full_name.trim().endsWith(',')) ? (full_name.trim().substring(0, full_name.trim().length - 1).trim()) : full_name);
        return full_name;
    });
    //self.isApproved = location.isApproved;
};

var zipVM = function (zip) {
    var self = this;
    self.zipCode = zip.zipCode;
    self.isChecked = zip.isChecked;
    self.county = zip.county;
    self.zipName = zip.zipName;
};

var storePercentCoverageVM = function (item) {
    var self = this;
    self.storeId = item.storeId;
    self.storePk = item.storePk;
    self.percentCoverageActual = item.percentCoverageActual;
    self.percentCoverageTarget = item.percentCoverageTarget;
};

var countyVM = function (county) {
    var self = this;
    self.countyName = county.countyName;
    self.percentCoverageActual = county.percentCoverageActual;
    self.percentCoverageTarget = county.percentCoverageTarget;
    self.storeId = county.storeId;
    self.dealersInfo = ko.observableArray([]);
    var temp_dealers_info = [];
    var item = {};
    var store_id = "";
    $.each(county.storePkCoverageDict, function (key, val) {
        store_id = "";
        // $.each(tempLocations, function (key1, val1) {
        $.each(county.zipViewList, function (key1, val1) {
            if (key == val1.storePk) {
                store_id = val1.storeId;
                return false;
            }
        });

        item["storePk"] = key;
        item["storeId"] = store_id;
        item["percentCoverageActual"] = val.percentCoverageActual;
        item["percentCoverageTarget"] = val.percentCoverageTarget;
        temp_dealers_info.push(new storePercentCoverageVM(item));
    });
    self.dealersInfo(temp_dealers_info);
    //self.dealersInfo(ko.mapping.fromJS(zip.dealersInfo, [], self));
    self.zipsInfo = ko.observableArray([]);
    var temp_zips_info = [];
    $.each(county.zipViewList, function (key, val) {
        temp_zips_info.push(ko.mapping.fromJS(val));
    });
    self.zipsInfo(temp_zips_info);
    //self.zipsInfo = ko.mapping.fromJS(zip.zips);
    //self.zipsInfo(ko.mapping.fromJS(zip.zips, [], self));
};

var dealerInfoVM = function (dealer) {
    var self = this;
    self.dealerId = dealer.storeId();
    self.dealerName = dealer.location();
    self.RSM = dealer.marketManager();
    self.address = ko.computed(function () {
        deferEvaluation: true
        return dealer.address() + " " + dealer.city() + ", " + dealer.state() + " " + dealer.zip();
    });
};

var imaSetup = function () {
    var self = this;
    new loadGoogleMap(self);
    self.selectedState = ko.observable();
    self.selectedLocation = ko.observable('');
    self.selectedLocationInfo = ko.observable({});
    self.states = [];
    self.locations = ko.observableArray([]);
    self.selectedLocationZipsInfo = ko.observable({});
    self.selectedDealerInfo = ko.observable({});
    self.selectedCounty = ko.observable('');
    self.selectedSubCoverage = ko.observable('');
    self.selectedSubCoverage('ag');
    //loads states from JSON --  START
    self.loadStates = function () {
        $.getJSON(gStateServiceUrl, function (result) {
            self.states = result;
        });
    };
    self.loadStates();
    //loads states from JSON --  END

    //Loads locations for the selected state. - START
    self.loadLocationsByState = function () {
        self.locations([]);
        self.selectedLocation('');
        map = null;
        $('#dvButtons').addClass('ui-disabled');
        $('#dvButtons a[data-role=button]').attr('disabled', true);
        $('div[data-role=collapsible-set]').css('display', 'none');
        $('#dvMapMarker').css('display', 'none');
        $('#map').css('display', 'none');
        var post_json = {};
        post_json["state"] = self.selectedState();
        post_json["approvalStatusDesc"] = "Available,Opted-In,Pending";
        post_json["locationListSize"] = 300;
        var search_filter_url = gLocationSearchUrl + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + locationsServiceJobNumber + "/0/0/52";
        postCORS(search_filter_url, JSON.stringify(post_json), function (data) {
            if (data.locationsAvailable.length == 0) {
                $('#alertmsg').html("No locations available for the selected state " + self.selectedState() + ".");
                $('#popupDialog').popup('open');
                return false;
            }
            tempLocations = data.locationsAvailable;
            $.each(data.locationsAvailable, function (key, val) {
                self.locations.push(new locationVM(val));
            });
            var appr_cnt = 0;
            var title = "";
            $.each(self.locations(), function (key, val) {
                title = "";
                if (val.isApproved) {
                    $('#ddlLocations').find('option[value=' + val.locationPk() + ']').css('color', 'white');
                    $('#ddlLocations').find('option[value=' + val.locationPk() + ']').css('background-color', 'green');
                    $('#ddlLocations').find('option[value=' + val.locationPk() + ']').css('font-weight', 'bold');
                    appr_cnt++;
                }
                title = val.storeId() + ' - ' + val.location() + ' (' + val.city() + ')';
                $('#ddlLocations option[value=' + val.locationPk() + ']').attr('title', title);
            });
            if (appr_cnt == self.locations().length) {
                $('#ddlState').find('option:selected').css('color', 'white');
                $('#ddlState').find('option:selected').css('background-color', 'green');
                $('#ddlState').find('option:selected').css('font-weight', 'bold');
            }

            $('#ddlLocations').val('').selectmenu('refresh');
            $('#ddlLocations').find('option').addClass('word_wrap');

        }, function (error_response) {
            showErrorResponseText(error_response, false);
        });
    };
    //Loads locations for the selected state. - END
    var temp_info = {};
    //Load/Get Zips by location - START
    self.loadZipsByLocation = function (data, event) {
        if (self.selectedLocation()) {
            self.selectedLocationZipsInfo({});
            self.selectedCounty('');
            temp_info = {};
            temp_info = $.extend(true, {}, tempLocationZips);
            map = null;
            openViewByCountyDemoHint();
            if (ctaLayer != null)
                ctaLayer.setMap(null);

            $.each(self.locations(), function (key, val) {
                if (val.locationPk() == self.selectedLocation()) {
                    self.selectedDealerInfo(new dealerInfoVM(val));
                    self.selectedLocationInfo(self.locations()[key]);
                    temp_info.storeId = val.storeId();
                    temp_info.latitude = val.latitude();
                    temp_info.longitude = val.longitude();
                    temp_info.storePk = val.locationPk();
                    temp_info.isApproved = 0;
                    temp_info["productGroup"] = $('#ddlProductGroup').val();
                    return false;
                }
            });

            var search_filter_url = gLocationZipsUrl + jobCustomerNumber + "/" + self.selectedLocation();
            getCORS(search_filter_url, null, function (data) {
                tempAssignedZips = data;
                if (Object.keys(data.countyDict).length > 0) {
                    $.each(data.countyDict, function (key1, val1) {
                        temp_info.zips.push(new countyVM(val1));
                        //temp_info.zips.push(ko.mapping.fromJS(val1));
                    });
                    self.selectedLocationZipsInfo(temp_info);
                    $('#dvPostalBoundarySelections div[data-role="collapsible-set"] div[data-role=collapsible]').find('[data-role=controlgroup]').controlgroup().controlgroup('refresh');
                    //$('#dvPostalBoundarySelections div[data-role="collapsible-set"] div[data-role=collapsible] input[type=checkbox]').checkboxradio().checkboxradio('refresh');
                    $('#dvPostalBoundarySelections div[data-role="collapsible-set"] div[data-role=collapsible]').collapsible().collapsible('refresh');
                    $('#dvPostalBoundarySelections div[data-role="collapsible-set"]').collapsibleset('refresh');
                    //$('#dvPostalBoundarySelections ul input[type=checkbox]').checkboxradio().checkboxradio('refresh');

                    $('#dvCounties').find('div[data-role=collapsible]').on('collapsibleexpand', function (event) {
                        if ($(this).find('fieldset').length > 0) {
                            ctaLayer.setMap(null);
                            ctaLayer = null;
                            self.selectedCounty($(this).attr('data-countyName'));
                            if ($('#chkViewLMA').is(':checked'))
                                self.makeKmlFileName();
                            var total_ctrls = $(this).find('fieldset input[type=checkbox]:not([id^=checkAll])');
                            if (total_ctrls.length > 0) {
                                var checked_ctrls = $(this).find('fieldset input[type=checkbox]:not([id^=checkAll]):checked');
                                if (total_ctrls.length == checked_ctrls.length) {
                                    $(this).find('fieldset input[type=checkbox][id^=checkAll]').attr('checked', true).checkboxradio('refresh');
                                }
                            }
                            else {
                                $('#alertmsg').html("Sorry, dealer " + self.selectedLocationInfo().storeId() + " does not have a LMA for " + self.selectedCounty() + " county.");
                                $('#popupDialog').popup('open');
                                $(this).collapsible('collapse');
                                return false;
                            }
                            $('#imgMapKey').attr('src', '../images/LMA_county_map_key.png');
                        }
                        else {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                    });
                    $('#dvCounties').find('div[data-role=collapsible]').on('collapsiblecollapse', function (event) {
                        var is_expanded = false;
                        $.each($('#dvCounties').find('div[data-role=collapsible]'), function () {
                            if (!$(this).hasClass('ui-collapsible-collapsed')) {
                                is_expanded = true;
                            }
                        });
                        if (!is_expanded) {
                            self.selectedCounty('');
                            ctaLayer.setMap(null);
                            ctaLayer = null;
                            if ($('#chkViewLMA').is(':checked'))
                                self.makeKmlFileName();
                            $('#imgMapKey').attr('src', '../images/LMA_overview_map_key.png');
                        }
                    });
                    //$('#dvPostalBoundarySelections ul input[type=checkbox]').checkboxradio().checkboxradio('refresh');
                    //$('#dvPostalBoundarySelections ul').listview().listview('refresh');
                    pageObj.makeGoogleMap();
                    $('#imgMapKey').attr('src', '../images/LMA_overview_map_key.png');
                    $('#dvButtons').removeClass('ui-disabled');
                    $('#dvButtons a[data-role=button]').removeAttr('disabled');
                    $('div[data-role=collapsible-set]').css('display', 'block');
                    $('#dvMapMarker').css('display', 'block');
                    //$('#mapHolder img').css('display', 'block');
                    return false;
                    //}
                    //});
                }
                else {
                    $('#alertmsg').html("Sorry, dealer " + self.selectedLocationInfo().storeId() + " does not have a LMA for " + self.selectedSubCoverage().toUpperCase() + ".");
                    $('#popupDialog').popup('open');
                    return false;
                }
            }, function (error_response) {
                showErrorResponseText(error_response, false);
            });
        }
    };

    self.addRemoveZipsOnMap = function (county_name, zip_code, is_checked) {
        var ctrl_county = $('div[data-countyName=' + county_name + ']').collapsible('expand');
        var ctrl = $('input[type=checkbox][id^=check_' + zip_code + "_" + county_name + ']').attr('checked', is_checked).checkboxradio('refresh').trigger('change');
    };

    //Load/Get Zips by location - END
    self.productGroupChange = function (data, event) {
        var ctrl = event.target || event.targetElement;
        if ($(ctrl).val() != 'ag') {
            $('#alertmsg').text("This option is not available yet.");
            $('#popupDialog').popup('open');
            $('#ddlProductGroup').val('ag').selectmenu('refresh');
        }
        else {
            self.selectedSubCoverage($(ctrl).val());
        }
    };
    self.makeKmlFileName = function () {
        var saved_users_info = {};
        if (sessionStorage.savedUsersInfo != undefined && sessionStorage.savedUsersInfo != null && sessionStorage.savedUsersInfo != "")
            saved_users_info = $.parseJSON(sessionStorage.savedUsersInfo);
        var junk_param = "";
        if (saved_users_info[sessionStorage.username])
            junk_param = "?junk=" + (new Date()).getTime();
        var county_selected = self.selectedCounty();
        var kml_file_name = self.selectedLocationInfo().storeId() + ((county_selected != "") ? '_' + county_selected.replace(/ /g, '').toUpperCase() : "");
        var kml_file = myProtocol + 'images.tribunedirect.com/lmaKml/_' + kml_file_name + '_01.kml';
        ctaLayer = new google.maps.KmlLayer(String(kml_file) + junk_param, { suppressInfoWindows: false, preserveViewport: true });
        ctaLayer.setMap(map);
    };

    self.saveClick = function (data, event) {
        $('#loadingText1').html('Saving...');
        $('#waitPopUp').popup('open', { positionTo: 'window' });
        var temp_Json = ko.mapping.toJS(self.selectedLocationZipsInfo(), mapping);
        $.each(temp_Json.zips, function (key1, val1) {
            var temp = val1;
            $.each(val1.zipsInfo, function (zips_key, zips_val) {
                //Update store Id , store pk in source object
                $.each(tempAssignedZips.countyDict[val1.countyName.toUpperCase()].zipViewList, function (source_key, source_val) {
                    if (zips_val.zip == source_val.zip) {
                        source_val.storeId = zips_val.storeId;
                        source_val.storePk = zips_val.storePk;
                        return false;
                    }
                });
            });
        });

        postCORS(serviceURLDomain + "api/Locations_lma_store_save/" + jobCustomerNumber + "/" + self.selectedLocationInfo().pk() + "/" + self.selectedLocationInfo().storeId(), JSON.stringify(tempAssignedZips), function (data) {
            var saved_users_info = {};
            if (sessionStorage.savedUsersInfo != undefined && sessionStorage.savedUsersInfo != null && sessionStorage.savedUsersInfo != "")
                saved_users_info = $.parseJSON(sessionStorage.savedUsersInfo);
            saved_users_info[sessionStorage.username] = true;
            sessionStorage.savedUsersInfo = JSON.stringify(saved_users_info);
            //if (self.selectedCounty() != "") {
            //    $('div[data-countyName=' + self.selectedCounty() + ']').collapsible('collapse');
            //    self.selectedCounty('');
            //}
            //if ($('#chkViewLMA').is(':checked'))
            //    self.makeKmlFileName();

            //var search_filter_url = gLocationZipsUrl + jobCustomerNumber + "/" + self.selectedLocation();
            //getCORS(search_filter_url, null, function (data) {
            //    tempAssignedZips = data;
            //}, function (error_response) {
            //    showErrorResponseText(error_response, false);
            //});
            $('#waitPopUp').popup('close');
            self.loadZipsByLocation();
            $('#alertmsg').html("Your selections have been saved for store " + self.selectedLocationInfo().storeId() + ".");
            $('#popupDialog').popup('open');

        }, function (error_response) {
            $('#waitPopUp').popup('close');
            showErrorResponseText(error_response, false);
        });
    };
    self.manageZips = function (data, event) {
        var ctrl = event.target || event.targetElement;
        var parent_context_data = ko.contextFor(ctrl).$parent;
        var total_zips = $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox]:not([id^=checkAll])');
        var selected_zips = $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox]:not([id^=checkAll]):checked');
        if ($(ctrl).is(':checked')) {
            data.storeId(self.selectedLocationInfo().storeId());
            data.storePk(self.selectedLocationInfo().pk());
            //parent_context_data
            if (total_zips.length == selected_zips.length) {
                $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox][id^=checkAll]').attr('checked', true).checkboxradio('refresh');
            }
            else {
                $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox][id^=checkAll]').attr('checked', false).checkboxradio('refresh');
            }
        }
        else {
            data.storeId(0);
            data.storePk(0);
            $.each(tempAssignedZips.countyDict[parent_context_data.countyName.toUpperCase()].zipViewList, function (key, val) {
                if (val.zip == data.zip() && val.storePk != self.selectedLocationInfo().pk()) {
                    data.storeId(val.storeId);
                    data.storePk(val.storePk);
                    return false;
                }
            });
            $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox][id^=checkAll]').attr('checked', false).checkboxradio('refresh');
        }
    };

    self.selectAllZipsInCounty = function (data, event) {
        var ctrl = event.target || event.targetElement;
        var parent_context_data = ko.contextFor(ctrl).$parent;
        var total_zips = $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox]:not([id^=checkAll])');
        var selected_zips = $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox]:not([id^=checkAll]):checked');
        if ($(ctrl).is(':checked')) {
            $.each(parent_context_data.zipsInfo(), function (zip_key, zip_val) {
                zip_val.storeId(self.selectedLocationInfo().storeId());
                zip_val.storePk(self.selectedLocationInfo().pk());
            });

            $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox]').attr('checked', true).checkboxradio('refresh');
        }
        else {
            $.each(parent_context_data.zipsInfo(), function (zip_key, zip_val) {
                zip_val.storeId(0);
                zip_val.storePk(0);
                $.each(tempAssignedZips.countyDict[parent_context_data.countyName.toUpperCase()].zipViewList, function (key, val) {
                    if (val.zip == zip_val.zip() && val.storePk != self.selectedLocationInfo().pk()) {
                        zip_val.storeId(val.storeId);
                        zip_val.storePk(val.storePk);
                        return false;
                    }
                });
            });

            $('div[data-countyName=' + parent_context_data.countyName + ']').find('input[type=checkbox]').attr('checked', false).checkboxradio('refresh');
        }
    };

    self.prefChanged = function (data, event) {
        var ctrl = event.target || event.targetElement;
        if ($(ctrl).is(':checked')) {
            self.makeKmlFileName();
        }
        else {
            ctaLayer.setMap(null);
        }
    };
};
function openMapPrefs() {
    $('#popupMapPrefs').panel('open', 'optionsHash');
}
function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else {
        window.location.href = " ../lmaSetup/lmaSetup.html";
    }
}

//Capitalizes the first letter in the given string.
String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

//Checks the given string ends with a specific (given character).
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
