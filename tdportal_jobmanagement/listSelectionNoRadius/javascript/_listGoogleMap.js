//------- START GLOBAL -------
var map;
var gCrrtMarkersA = [];
var gCrrtsA;
var ctaLayer;
var defaultMapZoom = 10;
var infowindow;
if (this.wantsCrrtMarkers) {
    //=this.mapLatLongExporter.makeCrrtMarkerJsArray() 
}

//------- END GLOBAL -------

function makeGoogleMap() {
    $('#map').show();
    //        var latitude = 40.776531000000;
    //        var longitude = -89.724655000000

    if (ctaLayer != null) {
        ctaLayer.setMap(null);
        gCrrtMarkersA = [];
    }
    var latitude = gSelectedZips.latitude;
    var longitude = gSelectedZips.longitude;

    var mapCenter = new google.maps.LatLng(latitude, longitude);
    map = new google.maps.Map(document.getElementById('map'), {
        'zoom': defaultMapZoom,
        'center': mapCenter,
        'mapTypeId': google.maps.MapTypeId.ROADMAP
    });
    var contentString = "Store Id: " + selectedStoreId

    //var infowindow = new google.maps.InfoWindow({
    infowindow = new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 100
    });
    var store_marker = addStoreMarker(mapCenter);
    //var image = '../images/customer_logos/steemer_marker.png';

    //show/hide crrt markers as needed...
    google.maps.event.addListener(map, 'zoom_changed', function () {
        var crrt_checkbox = document.getElementById('showHideCrrtCheck');
        //only have to worry if crrt is checked.
        //if (crrt_checkbox !=null && crrt_checkbox.checked){
        //...and if we're crossing defaultMapZoom "zone"
        if (map.getZoom() >= defaultMapZoom || map.getZoom() < defaultMapZoom) {
            if (map.getZoom() < defaultMapZoom) {
                for (i in gCrrtMarkersA) {
                    gCrrtMarkersA[i].setMap(null);
                }
            }
            if (map.getZoom() >= defaultMapZoom) {
                for (i in gCrrtMarkersA) {
                    gCrrtMarkersA[i].setMap(map);
                }
            }
        }
        //}
        oldZoom = map.getZoom();
    });



    // Add a Circle overlay to the map.
    /*
    var circle = new google.maps.Circle({
        map: map,
        radius: 0,
        strokeWeight: 2.5,
        strokeColor: "#CC0000",
        strokeOpacity: 1.0,
        zIndex: 500
    });

    circle.bindTo('center', store_marker, 'position');
    */

   //draw radius if required
   switch (appPrivileges.customerNumber) {
       case CASEYS_CUSTOMER_NUMBER:
           //Caseys Draw Radius at 12 miles
           var radius_value = 12
           var units = 'miles';
           drawCircle(map, mapCenter, radius_value, units);
           break;
        default:
            break;
    }

    postCORS(mapsUrl + 'api/Kml/' + appPrivileges.customerNumber + '/1/' + jobNumber + '/' + selectedStoreId + '/asdf', JSON.stringify(gSelectedZips), function (data) {
        gCrrtsA = eval(data.crrtA);
        var kml_file = myProtocol + 'images.tribunedirect.com/mapKml/' + data.kmlName;
        ctaLayer = new google.maps.KmlLayer(String(kml_file), { suppressInfoWindows: false, preserveViewport: true });
        ctaLayer.setMap(map);
        addCrrts();
        addLocationMarkers(eval(data.locationsA));
        ctaLayer.zIndex = 100;
    }, function (error_response) {
        showErrorResponseText(error_response);
    });

    
}
function addStoreMarker(mapCenter) {
    var marker = null;
    switch (appPrivileges.customerNumber) {
        case CASEYS_CUSTOMER_NUMBER:

            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155711));
            var image = new google.maps.MarkerImage('../images/maps/caseys_map_marker.png',
            // This marker is 59 pixels wide by 24 pixels tall.
            new google.maps.Size(41, 42),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 29,24.
            new google.maps.Point(20, 42));
            var shadow = new google.maps.MarkerImage('../images/maps/caseys_map_shadow.png',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(50, 42),
            new google.maps.Point(0, 0),
            new google.maps.Point(20, 42));
            var shape = {
                coord: [0, 17, 18, 7, 18, 1, 22, 0, 22, 7, 41, 17, 41, 18, 39, 18, 39, 39, 23, 39, 21, 42, 19, 42, 17, 39, 2, 39, 2, 18, 0, 18],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Caseys General Store'
            });
            break;
        case CW_CUSTOMER_NUMBER:

            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155370));

            var image = new google.maps.MarkerImage('../images/maps/camping_world_map_marker.png',
             new google.maps.Size(30, 41),
             new google.maps.Point(0, 0),
             new google.maps.Point(14, 41));
            var shadow = new google.maps.MarkerImage('../images/maps/camping_world_map_shadow.png',
             new google.maps.Size(47, 41),
             new google.maps.Point(0, 0),
             new google.maps.Point(14, 41));
            var shape = {
                coord: [14, 41, 10, 29, 0, 16, 0, 9, 9, 0, 20, 0, 30, 9, 30, 16, 21, 29, 15, 41],
                type: 'poly'
            };
            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Camping World'
            });
            break;
    }


    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
    return marker;
}
function addLocationMarkers(locationsA) {
    var locations_markers_A = [];
    var info_window = new google.maps.InfoWindow({
        content: "Store Id: " + selectedStoreId,
        maxWidth: 100
    });
    for (var i = 0; i < locationsA.length; i++) {
        var location = locationsA[i];
        var marker = null;
        switch (appPrivileges.customerNumber) {
            case CASEYS_CUSTOMER_NUMBER:
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addLocationMarkers155711));
                var image = new google.maps.MarkerImage('../images/maps/caseys_map_marker_small.png',
                new google.maps.Size(30, 31),
                new google.maps.Point(0, 0),
                new google.maps.Point(15, 31));
                var shadow = new google.maps.MarkerImage('../images/maps/caseys_map_shadow_small.png',
                new google.maps.Size(38, 31),
                new google.maps.Point(0, 0),
                new google.maps.Point(15, 31));
                var shape = {
                    coord: [0, 13, 0, 12, 13, 5, 13, 0, 17, 0, 17, 5, 30, 12, 30, 13, 29, 13, 29, 28, 18, 28, 16, 31, 15, 31, 13, 28, 2, 28, 2, 13],
                    type: 'poly'
                };
                var myLatLng = new google.maps.LatLng(location[1], location[2]);

                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + location[0],
                    title: 'Store Id: ' + location[0]
                });
                break;
            case CW_CUSTOMER_NUMBER:

                //eval(getURLDecode(pagePrefs.scriptBlockDict.addLocationMarkers155370));
                var image = new google.maps.MarkerImage('../images/maps/camping_world_map_marker.png',
                 new google.maps.Size(30, 41),
                 new google.maps.Point(0, 0),
                 new google.maps.Point(14, 41));
                var shadow = new google.maps.MarkerImage('../images/maps/camping_world_map_shadow.png',
                 new google.maps.Size(47, 41),
                 new google.maps.Point(0, 0),
                 new google.maps.Point(14, 41));
                var shape = {
                    coord: [14, 41, 10, 29, 0, 16, 0, 9, 9, 0, 20, 0, 30, 9, 30, 16, 21, 29, 15, 41],
                    type: 'poly'
                };
                var myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    title: 'Camping World'
                });
                break
        }

        locations_markers_A.push(marker);

        google.maps.event.addListener(marker, 'click', function () {
            info_window.setContent(this.contentString);
            info_window.open(map, this);
        });
    }
    for (i in locations_markers_A) {
        locations_markers_A[i].setMap(map);
    }

}
//------- START CRRT -------
function addCrrts() {
    // Add markers to the map
    // Marker sizes are expressed as a Size of X,Y
    // where the origin of the image (0,0) is located
    // in the top left of the image.

    // Origins, anchor positions and coordinates of the marker
    // increase in the X direction to the right and in
    // the Y direction down.
    var shape = {
        coord: [0, 0, 34, 0, 34, 14, 18, 15, 18, 20, 16, 20, 15, 14, 0, 14],
        type: 'poly'
    };
    for (var i = 0; i < gCrrtsA.length; i++) {
        var crrt = gCrrtsA[i];
        if (crrt != undefined) {
            //var the_category = makeCrrtCategory(crrt[4]);
            var the_category = "yellow";
            var image = new google.maps.MarkerImage(logoPath + 'cr_pins/' + the_category + '/' + crrt[0] + '.png',
                // This marker is 20 pixels wide by 32 pixels tall.
                new google.maps.Size(35, 21),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at 0,32.
                new google.maps.Point(17, 20));
            var shadow = new google.maps.MarkerImage(logoPath + 'cr_pins/cr_shadow.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(44, 21),
                new google.maps.Point(0, 0),
                new google.maps.Point(17, 20));
            // Shapes define the clickable region of the icon.
            // The type defines an HTML <area> element 'poly' which
            // traces out a polygon as a series of X,Y points. The final
            // coordinate closes the poly by connecting to the first
            // coordinate.
            var myLatLng = new google.maps.LatLng(crrt[1], crrt[2]);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                title: "Count: " + crrt[4],
                zIndex: crrt[3]
            });
            gCrrtMarkersA.push(marker);
        }
        for (i in gCrrtMarkersA) {
            gCrrtMarkersA[i].setMap(map);
        }

    }
}

function drawCircle(map,map_center,radius_value,units) {
    var my_circle;


    if (units == 'kilometer')
        radius_value = radius_value * 1000
    else
        radius_value = radius_value * 1609.344


    var circleOptions = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0,
        map: map,
        center: map_center,
        clickable: false,
        radius: radius_value
    };
    // Add the circle for this city to the map.
    my_circle = new google.maps.Circle(circleOptions);
}
/*
function makeCrrtCategory(the_count){
    var the_cat = "yellow";
    if (the_count <= 10){
        the_cat = "cyan";
    }else if (the_count <= 50){
        the_cat = "green";
    }else if (the_count <= 100){
        the_cat = "yellow";
    }else if (the_count <= 200){
        the_cat = "orange";
    }else{
        the_cat = "red";
    }
    return the_cat;
}
    */
/*
 function showHideCrrts(){
    //var crrt_checkbox = document.getElementById('showHideCrrtMarkers');
    addCrrts(map, gCrrtsA);
    if (gCrrtsA){
        for (i in gCrrtMarkersA){
            //if (crrt_checkbox.checked){
                gCrrtMarkersA[i].setMap(map);
            //}else{
              //  gCrrtMarkersA[i].setMap(null);
            //}
        }
    }
}
function showHideZips(){
    var zip_checkbox = document.getElementById('showHideZipCheck');
    if (ctaLayer){
        if (zip_checkbox.checked){
            ctaLayer.setMap(map);
        }else{
            ctaLayer.setMap(null);
        }
    }
}
*/
//------- END CRRT
// Register an event listener to fire when the page finishes loading.
//    google.maps.event.addDomListener(window, 'load', init);
