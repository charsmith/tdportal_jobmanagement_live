﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gServiceUrl = 'JSON/_adminModule.JSON';
var gData;
var gModulesData = [];
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_jobAdminModule', function (event) {
});

$('#_jobAdminModule').live('pagebeforecreate', function (event) {
    //displayMessage('_jobAdminModule');
    makeGData();
    loadMobility();
    });

function makeGData() {
    $.getJSON(gServiceUrl, function (data) {
        gData = data;
        $.each(gData, function (key, val) {
            var a = val;
            gModulesData.push({
                "item": val.item,
                "children": ko.observableArray(val.values)
            });

        });
        $('#dvModules').empty();
        ko.cleanNode($('#dvModules')[0]);
        ko.applyBindings(gModulesData, $('#dvModules')[0]);
        $("div ul").each(function (i) {
            $(this).listview();
        });
        $.each($('div[data-role=collapsible]'), function (a, b) {
            $(this).collapsible();
        });
        //$("#uploadList").collapsibleset('refresh');
    });
    //var view_model = createViewModel();
}

function fnAdminModule(ctrl_id) {
    if (ctrl_id == 'cr') {
        window.location.href = "../adminModuleMap/adminModuleMap.html"
    }
}
