﻿var loadFinanceInfo = function () {
    var self = this;

    self.gData;
    self.jobNumber = getSessionData("jobNumber");
    self.facilityId = sessionStorage.facilityId;
    self.gOutputData;
    self.postJobURL;
    self.PAGE_NAME = 'approvalCheckout.html';

    //Create store list that were ordered for that job.
    self.getStoreIdList = function () {
        var store_ids = '<label for="ddlStoreId">StoreID:</label><br\><select id="ddlStoreId" name="ddlStoreId" data-mini="true"  data-theme="f">';
        store_ids += '<option value="select">Select</option>';
        $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key, obj) {
            //store_ids += '<option value= "' + obj.storeId + '">' + obj.storeId + '</option>';
            store_ids += '<option value= "' + obj.pk + '">' + obj.storeId + '</option>';
        });
        store_ids += '</select>'
        $('#divSelectStoreId').append(store_ids);
        $('#divSelectStoreId').trigger('create');
    };

    //Gets selected store value
    self.getSelectedStoreVal = function () {
        var store_ddl_Val = $('#ddlStoreId').val();
    };

    //******************** Public Functions Start **************************
    //This function will build store list
    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined)
            $.getJSON(gOutputServiceUrl, function (dataOutput) {
                self.gOutputData = dataOutput;
            });
        else
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        getBubbleCounts(self.gOutputData);
        self.getStoreIdList();
        //delete gOutputData.checkoutAction; //TODO: to be discussed with Jim. We are getting this attribute in templateOnchange web service
    };
}
