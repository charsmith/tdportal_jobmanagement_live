﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************

var gServiceEmailList = "../jobEmail/JSON/_jobEmail.JSON";
var gServiceEmailListData;
var emailData;
var pageObj;
var gOutputData;
var gPageSize = 20;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobEmail').live('pagebeforecreate', function (event) {
    displayMessage('_jobEmail');
    createConfirmMessage('_jobEmail')
    displayNavLinks();
    getEmailRecipientsData();
    window.setTimeout(function () {
        //vm = new viewModel();
        vm = new makeEmailRecipients();
        //vm.person(new makeEmailRecipients());
        ko.applyBindings(vm);
        //ko.applyBindings(vm.person());
        window.setTimeout(function delay() {
            $.each($('#dvEmailRecipients div[data-role=collapsible]'), function () {
                $(this).on("collapsiblecollapse", function (e) {
                    $(this).data("previous-state", "collapsed");
                    //$(this).find('.ui-input-clear').unbind('click');
                });
                $(this).on("collapsibleexpand", function (e) {
                    $(this).data("previous-state", "expanded");
                    //$('.ui-content').on('click', '.ui-input-clear', function (e) {
                    vm.getData(this.id);
                });
                $(this).find('.ui-input-clear').bind('click', function (e) {
                    if (e.originalEvent) {
                        vm.search();
                    }
                });

            });
        }, 500);
        $('#dvEmailRecipients').collapsibleset('refresh');
        $('#dvEmailRecipients').trigger('create');
    }, 2000);
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });

    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        getBubbleCounts(gOutputData);
    }
    if (jobCustomerNumber == JETS_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
        setDemoHintsSliderValue();
    }
    createPlaceHolderforIE();
});

$(document).on('pageshow', '#_jobEmail', function (event) {
    persistNavPanelState();
    $('#sldrShowHints').slider("refresh");
});

function emailPopupGroups() {
    var group_list = '<fieldset data-role="controlgroup" class="ui-controlgroup" id="fldStEmailGroupsChk">';
    var rdo_group_list = '<fieldset data-role="controlgroup" class="ui-controlgroup" id="fldStEmailGroupsRdoSave">';
    var display_group_name = '';

    $.each(emailData, function (key, val) {
        display_group_name = '';
        display_group_name = getDisplayGroupName(key);
        group_list += '<label for="chk' + key.initCap() + '">' + display_group_name + ':</label>';
        group_list += '<input type="checkbox" name="chk' + key.initCap() + '" id="chk' + key.initCap() + '" value="' + display_group_name + '" data-mini="true" data-theme="c" />';

        rdo_group_list += '<label for="rdo' + key.initCap() + '">' + display_group_name + ':</label>';
        rdo_group_list += '<input type="radio" name="rdoSaveEmailGroups" id="rdo' + key.initCap() + '" value="' + display_group_name + '" data-mini="true" data-theme="c" />';

    });
    group_list += '</fieldset>';
    rdo_group_list += '</fieldset>';
    $('#dvGroups').html(group_list).trigger('create');
    $('#dvGroupsSave').html(rdo_group_list).trigger('create');
}
function getDisplayGroupName(group_name) {
    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
    var group_display_name = '';
    var prev_index = 0;
    $.each(new String(group_name), function (key, value) {
        if (key > 0 && value.match(PATTERN)) {
            if ((prev_index + 1) != key) {
                group_display_name += ' ' + value;
            }
            else
                group_display_name += value;
            prev_index = key;
        }
        else {
            group_display_name += (key == 0) ? value.initCap() : value;
            if (value == "-")
                prev_index = key;
        }
    });
    return group_display_name;
}
function getEmailRecipientsData() {
    $.getJSON(gServiceEmailList, function (data) {
        $.each(data, function (key, val) {
            if (key.indexOf(jobCustomerNumber) > -1) {
                emailData = val;
                emailPopupGroups();
            }
        });
    });
};

function createPagerObject1(total_pages, start, end) {
    var pager_length = Math.ceil(total_pages / gPageSize);
    var pager_object = [];
    if (total_pages > 1) {
        for (var j = 0; j < total_pages; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j) });
        }
    }
    return pager_object;
}

var emailRecipientGroup = function (name, email_list, selected_page_value, search_value) {
    var self = this;
    self.caption = ko.observable(name);
    self.emails = ko.observableArray([]);
    $.each(email_list, function (key, val) {
        self.emails.push(new emailRecipient(val));
    });
    self.pagingData = ko.observableArray([]).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 200 } });
    self.selectedPageValue = ko.observable();
    self.selectedPageValue(selected_page_value);
    self.showMoreOption = ko.observable();
    self.showSearchField = ko.observable();
    self.searchValue = ko.observable();
    self.prevSearchValue = ko.observable('');
    self.searchValue(((search_value != undefined && search_value != null) ? search_value : ""));
    self.selectedFilterOption = ko.observable();
    self.selectedFilterOption('emailAddress');
};

var emailRecipient = function (data_info) {
    var self = this;
    self.firstName = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.firstName : "");
    self.lastName = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.lastName : "");
    self.storeId = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.storeId : "");
    self.emailAddress = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.emailAddress : "");
    self.optInStatus = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.optInStatus : "");
    self.approvalStatus = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.approvalStatus : "");
    self.sent = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.sent : "");
    self.fieldsList = ko.computed(function () {
        var temp_fields = "";
        temp_fields = "First Name: " + self.firstName() + ' | ' + "Last Name: " + self.lastName();
        //$.each(data_info, function (key, val) {
        //    if (key.toLowerCase() != "emailaddress") {
        //        var field_name = "";
        //        field_name = getDisplayGroupName(key);
        //        temp_fields += (temp_fields != "") ? ' | ' + field_name + ': ' + val : field_name + ': ' + val;
        //    }
        //});
        return temp_fields;
    });
}

var makeEmailRecipients = function () {
    var self = this;
    self.emailsList = ko.observableArray([]);
    self.selectedEmailListItem = ko.observable({});
    self.prevSelectedEmailListItem = {};
    self.selectedEmailType = ko.observable();
    self.emailsQuantity = ko.observable();
    self.sendEmailType = ko.observable();
    self.emailTypeMode = ko.observable();
    self.sendEmailsList = ko.observableArray([]);
    self.getPageDisplayName = function (page_name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var page_display_name = '';
        $.each(new String(page_name), function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                page_display_name += ((!page_display_name.endsWith(' ') && !page_display_name.endsWith('-')) ? ' ' : '') + value;
            }
            else {
                page_display_name += value;
            }
        });
        return page_display_name;
    };
    var temp_key_email = "";
    var temp_email_list = [];
    $.each(emailData, function (key, val) {
        temp_key_email = self.getPageDisplayName(key.initCap());
        self.emailsList.push(new emailRecipientGroup(temp_key_email, val, 0, ''));
    });

    self.saveMasterListClick = function () {
        $('input[type=radio]').attr('checked', false).checkboxradio('refresh');
        $('#popupSaveMasterList').popup('open');
    }

    self.saveMasterListInfo = function () {
        //var selected_grps = $('#popupSaveMasterList input[type=radio]:checked');
        var selected_option = $("input[name=rdoSaveEmailGroups]:checked").val();
        if (selected_option != undefined && selected_option != null && selected_option != "") {
            var post_data = {};
            var template_name = "";
            if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != undefined != null && sessionStorage.jobSetupOutput != "") {
                var job_ticket_json = $.parseJSON(sessionStorage.jobSetupOutput);
                template_name = job_ticket_json.templateName;
            }
            post_data["customerNumber"] = sessionStorage.customerNumber;
            post_data["facilityId"] = sessionStorage.facilityId;
            post_data["jobNumber"] = sessionStorage.jobNumber;
            post_data["emailType"] = selected_option.toLowerCase();
            post_data["templateName"] = template_name;

            $('#popupSaveMasterList').popup('close');
            var master_list_service_url = serviceURLDomain + "api/GizmoEmailCustomer_updateMaster";
            postCORS(master_list_service_url, JSON.stringify(post_data), function (response_data) {
                if (response_data.toLowerCase() == "success") {
                    $('#alertmsg').html('Master list saved successfully.');
                    $('#popupDialog').popup('open');
                }
            },
            function (responce_error) {
                showErrorResponseText(error_response, false);
            });
        }
        else {
            $('#popupSaveMasterList').popup('close');
            $('#alertmsg').html('Please select an option to save.');
            $('#okBut').bind('click', function () {
                $('#okBut').unbind('click');
                $('#okBut').attr('onclick', '$("#popupDialog").popup("close")');
                window.setTimeout(function reopenPopup() {
                    $('#popupSaveMasterList').popup('open');
                }, 200);
            });
            $('#popupDialog').popup('open');
        }
    };
    self.editEmailClick = function (data, el) {
        var ctrl_id = (el.targetElement || el.target || el.currentTarget);
        //var ctrl = $('#' + e.target.id);
        var context_data = ko.contextFor(ctrl_id).$parents[1];

        self.selectedEmailListItem(data);
        var temp_selected_list_item = (ko.toJS(data));
        delete temp_selected_list_item["fieldsList"];
        self.prevSelectedEmailListItem = temp_selected_list_item;
        self.emailTypeMode('edit');
        self.selectedEmailType(context_data.caption());
        $('input[type=checkbox]').attr('checked', false).checkboxradio('refresh');
        $('input[type=checkbox][value="' + self.selectedEmailType() + '"]').attr('checked', true).checkboxradio('refresh');
        $('#popupEmailRecipient').popup('open', { positionTo: 'window' });
        $('input[type="checkbox"]').checkboxradio('refresh');
    };
    self.addEmailClick = function (data, el) {
        $('input[type=checkbox]').attr('checked', false).checkboxradio('refresh');
        //$('#chkOptInEmailtest').attr('checked', false).checkboxradio('refresh');
        //$('#chkOptInEmail').attr('checked', false).checkboxradio('refresh');
        //$('#chkApprovalEmailTest').attr('checked', false).checkboxradio('refresh');
        //$('#chkApprovalEmail').attr('checked', false).checkboxradio('refresh');
        self.selectedEmailListItem(new emailRecipient());
        self.emailTypeMode('new');
        $('#popupEmailRecipient').popup('open', { positionTo: 'window' });
    };

    self.addNewEmailRecipient = function (data, el) {
        if (!self.emailRecipientValidation()) return false;
        if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER) {
            var post_json = ko.toJS(self.selectedEmailListItem);
            delete post_json["fieldsList"];
            delete post_json["storeId"];
            delete post_json["optInStatus"];
            delete post_json["approvalStatus"];
            delete post_json["sent"];
            var insert_update_service_url = serviceURLDomain + "api/GizmoEmailCustomer_update/" + jobCustomerNumber + "/" + sessionStorage.facilityId + "/" + sessionStorage.jobNumber + "/";// + $('#hdnSelectedGroup').val();
            var selected_target = $('#hdnSelectedGroup').val();
            if (selected_target != "") {
                var page_index = 0;
                var context_data = "";
                //var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
                var ctrl_id = selected_target.replace(/ /g, '_');
                if (selected_target != "") {
                    //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
                    context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
                }
                var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
            }
            $("#popupEmailRecipient").popup('close');
            var temp_seleted_groups_cnt;
            $.each($('input[type=checkbox]:checked'), function (key, val) {
                temp_selected_groups_cnt = $('input[type=checkbox]:checked').length;
                postCORS(insert_update_service_url + $(val).val().toLowerCase(), JSON.stringify(post_json), function (response_data) {
                    if (key == (temp_selected_groups_cnt - 1)) {
                        if (selected_target != "") {
                            self.getSelectedEmailsList(ctrl_id, context_data, context_data.caption(), selected_page_value);
                        }
                        if (self.emailTypeMode() == "new") {// && response_data == "success"
                            $('#alertmsg').html('Email Recipient added successfully.')
                        }
                        else {
                            $('#alertmsg').html('Email Recipient updated successfully.')
                        }
                        $('#popupDialog').popup('open');
                    }
                }, function (error_response) {
                    showErrorResponseText(error_response, false);
                    $("#popupEmailRecipient").popup('close');
                });
            });
        }
        else {
            if (self.emailTypeMode() == "new") {
                $.each($('input[type=checkbox]'), function (key, val) {
                    if ($(this).is(":checked")) {
                        $.each(self.emailsList(), function (key1, val1) {
                            if (self.getPageDisplayName(val.value.initCap()) == val1.caption()) {
                                val1.emails.push(new emailRecipient(ko.toJS(self.selectedEmailListItem)));
                                return false;
                            }
                        });
                    }
                });
            }
            else if (self.emailTypeMode() == "edit") {
                $.each(self.emailsList(), function (key1, val1) {
                    if (self.selectedEmailType().toLowerCase() == val1.caption().toLowerCase() && !$('input[type=checkbox][value="' + self.selectedEmailType() + '"]').is(':checked')) {
                        if (self.selectedEmailType().toLowerCase() == val1.caption().toLowerCase()) {
                            val1.emails.remove(self.selectedEmailListItem());
                        }
                    }
                    else if (self.selectedEmailType().toLowerCase() != val1.caption().toLowerCase() && $('input[type=checkbox][value="' + val1.caption() + '"]').is(':checked')) {
                        val1.emails.push(new emailRecipient(ko.toJS(self.selectedEmailListItem)));
                    }
                });

                self.selectedEmailListItem({});
            }
            $("#popupEmailRecipient").popup('close');
        }
        $('#dvEmailRecipients ul').listview('refresh');

    }

    self.emailTypeChange = function (data, event) {
        if (event.target.checked)
            $('#dvEmailInfo').css('display', 'block');
        else
            $('#dvEmailInfo').css('display', 'none');
    };
    self.removeItem = function (data, event) {
        var ctrl_id = (event.targetElement || event.target || event.currentTarget);
        //var ctrl = $('#' + e.target.id);
        var context_data;
        var selected_target = $('#hdnSelectedGroup').val();
        if (selected_target != "") {
            var page_index = 0;
            context_data = "";
            //var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
            var ctrl_id = selected_target.replace(/ /g, '_');
            if (selected_target != "") {
                //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
                context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
            }
            var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
        }
        $('#confirmMsg').html('Are you sure you want remove this item?');
        $('#okButConfirm').unbind('click');
        if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
            $('#okButConfirm').bind('click', function () {
                $.each(self.emailsList(), function (key, val) {
                    if (context_data.caption() == val.caption()) {
                        val.emails.remove(data);
                        return false;
                    }
                });
                $('#popupConfirmDialog').popup('close');
            });
        }
        else if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER) {
            $('#okButConfirm').bind('click', function () {
                //{ "customerNumber": MOTIV8_CUSTOMER_NUMBER, "facilityId": 1, "jobNumber": -16279, "emailType": "test", "emailAddress": "wright_hinsdale@hotmail.com" }
                var post_data = {};
                post_data["customerNumber"] = sessionStorage.customerNumber;
                post_data["facilityId"] = sessionStorage.facilityId;
                post_data["jobNumber"] = sessionStorage.jobNumber;
                post_data["emailType"] = context_data.caption().toLowerCase();
                post_data["emailAddress"] = data.emailAddress();
                var deleteEmail_service_url = serviceURLDomain + "api/GizmoEmailCustomer_delete";
                $('#popupConfirmDialog').popup('close');
                postCORS(deleteEmail_service_url, JSON.stringify(post_data), function (response_data) {
                    if (response_data.toLowerCase() == "success") {
                        if (selected_target != "") {
                            self.getSelectedEmailsList(ctrl_id, context_data, context_data.caption(), selected_page_value);
                        }
                        $('#alertmsg').html('Email address has been deleted successfully.');
                    }
                    else if (response_data.toLowerCase() == "email_address_not_found") {
                        $('#alertmsg').html('Email address not found, unable to delete the selected email address.');
                    }
                    else if (response_data.toLowerCase() == "multiple_email_addresses") {
                        $('#alertmsg').html('Multiple email addresses found, unable to delete the selected email address.');
                    }
                    else if (response_data.toLowerCase() == "too_many_email_files") {
                        $('#alertmsg').html('Too many email files associated to this address, unable to delete the selected email address.');
                    }
                    window.setTimeout(function openPopup() {
                        $('#popupDialog').popup('open');
                    }, 200);
                },
                    function (responce_error) {
                        showErrorResponseText(error_response, false);
                    });

            });
        }
        $('#okButConfirm').text('Ok');
        $('#popupConfirmDialog').popup('open', { positionTo: 'window' });
    };
    self.emailCancel = function () {
        if (self.emailTypeMode() == "new")
            self.selectedEmailListItem({});
        else {
            $.each(self.prevSelectedEmailListItem, function (key, val) {
                self.selectedEmailListItem()[key](val);
            });
        }
        self.prevSelectedEmailListItem = {};
        self.selectedEmailListItem({});
        $('#popupEmailRecipient').popup('close');
    };

    self.sendEmailPopup = function (data, event) {
        var ctrl_id = (event.targetElement || event.target || event.currentTarget);
        //var ctrl = $('#' + e.target.id);
        var context_data = ko.contextFor(ctrl_id).$data;
        self.sendEmailsList.removeAll();
        self.sendEmailType(context_data.caption());
        self.emailsQuantity(context_data.emails().length);
		if (context_data.pagingData().length > 0) {
            self.emailsQuantity((gPageSize * context_data.pagingData().length));
        }
        if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
            $.each(context_data.emails(), function (key, val) {
                self.sendEmailsList.push(val.emailAddress());
            });
        }
        $('#emailList ul').listview('refresh');
        $('#btnSendEmail').bind('click', function () {
            if (!self.ValidateEmailConfirm()) return false;
            $('#okButConfirm').unbind('click');
            $('#confirmMsg').html('Are you sure you want to send <b>' + self.emailsQuantity() + '</b>  emails from the <b>' + self.sendEmailType() + '</b> list?');
            $('#okButConfirm').text('Send Email');
            $('#okButConfirm').bind('click', function () {
                $('#okButConfirm').unbind('click');
                self.sendEmail(data, event);
            });
            $("#popupSendEmailConfirm").popup('close');
            window.setTimeout(function showMsg() {
                $("#popupConfirmDialog").popup('open', { positionTo: 'window' });
            }, 1000);
        });
        $("#popupSendEmailConfirm").popup('open');
    }

    self.sendEmail = function (data, event) {
        $('#popupConfirmDialog').popup('close');
        if (jobCustomerNumber == JETS_CUSTOMER_NUMBER) {
            $("#alertmsg").html('Emails sent successfully.');
            window.setTimeout(function showMsg() {
                $("#popupDialog").popup('open', { positionTo: 'window' });
            }, 1000);
        } else {
            var send_email_url = serviceURLDomain + "api/GizmoEmailCustomer_send";
            var post_json = {};
            post_json["customerNumber"] = jobCustomerNumber;
            post_json["facilityId"] = parseInt(sessionStorage.facilityId);
            post_json["jobNumber"] = parseInt(sessionStorage.jobNumber);
            post_json["emailType"] = self.sendEmailType().toLowerCase();
            post_json["subject"] = $('#txtSubject').val();
            post_json["from"] = $('#txtFromEmail').val();

            postCORS(send_email_url, JSON.stringify(post_json), function (response_data) {
                if (response_data == "success") {
                    window.setTimeout(function showMsg() {
                        $('#alertmsg').html('Emails sent successfully.')
                        $('#popupDialog').popup('open');
                    }, 1000);
                }
            }, function (error_response) {
                showErrorResponseText(error_response, false);
            });
            $('#txtSubject').val('');
            $('#txtFromEmail').val('');
            createPlaceHolderforIE();
        }
        //  });
        // $('#emailList ul').listview('refresh');
        // $("#popupSendEmailConfirm").popup('open');
    };

    self.emailRecipientValidation = function () {
        var msg = "";
        var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var regFirstLast = /^[a-zA-Z ]*$/;
        var regxStore = /^[ A-Za-z0-9_.#-]*$/
        var is_chkd = $('#popupEmailRecipient').find('input[type="checkbox"]:checked')
        if (is_chkd.length == 0) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select at least one 'Add to' option.";
            else
                msg = "Please select at least one 'Add to' option.";

        }
        if ($("#txtEmail").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Email address.";
            else
                msg = "Please enter Email address.";
            $('#txtEmail').val('');
        }

        else if (!regEmail.test($("#txtEmail").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Email address.";
            else
                msg = "Please enter valid Email address.";
            $('#txtEmail').val('');
        }
        if ($("#txtFirstName").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter First Name";
            else
                msg = "Please enter First Name";
            $('#txtFirstName').val('');
        }
        else if (!regFirstLast.test($("#txtFirstName").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid First Name";
            else
                msg = "Please enter valid First Name";
            $('#txtFirstName').val('');
        }
        if ($("#txtLastName").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Last Name";
            else
                msg = "Please enter Last Name";
            $('#txtLastName').val('');
        }
        else if (!regFirstLast.test($("#txtLastName").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Last Name";
            else
                msg = "Please enter valid Last Name";
            $('#txtLastName').val('');
        }
        if ($("#txtStoreID").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Store Id";
            else
                msg = "Please enter Store Id";
            $('#txtStoreID').val('');
        }
        else if (!regxStore.test($("#txtStoreID").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Store Id";
            else
                msg = "Please enter valid Store Id";
            $('#txtStoreID').val('');
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $('#popupEmailRecipient').popup('close');
            $("#popupDialog").popup('open');
            $("#okBut").bind('click', function () {
                $('#popupDialog').popup('close');
                $("#okBut").unbind('click');
                $('#okBut').bind('click', function () {
                    $('#popupDialog').popup('close');
                });
                window.setTimeout(function setDelay() {
                    $('#popupEmailRecipient').popup('open');
                }, 300);
            });
            return false;
        }
        return true;

    };

    self.getData = function (id) {
        $('#hdnSelectedGroup').val(id);
        if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER) {
            var context_data = ko.dataFor($('#' + id.replace(/ /g, '_'))[0]);
            var page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            self.getSelectedEmailsList(id, context_data, context_data.caption, page_index);
        }
    };

    self.moreClick = function (el, e) {
        if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER) {
            if (e.originalEvent) {
                var ctrl_id = (e.targetElement || e.currentTarget || e.target).id;
                var ctrl = $('#' + ctrl_id);
                var context_data = ko.contextFor(ctrl[0]).$data;

                context_data.selectedPageValue(context_data.selectedPageValue() + 1)

                self.getSelectedEmailsList(ctrl_id, context_data, context_data.caption(), (context_data.selectedPageValue()));
                //window.setTimeout(function setUI() {
                $("#" + ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
                $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
                $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
                $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
                //}, 40);
            }
        }
    };

    self.pageChanged = function (el, e) {
        if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER) {
            if (e.originalEvent) {
                var ctrl_id = (e.targetElement || e.target || e.currentTarget).id;
                var ctrl = $('#' + e.target.id);
                var context_data = ko.contextFor(ctrl[0]).$data;
                self.getSelectedEmailsList(ctrl_id, context_data, context_data.caption(), ctrl.val());
                $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
                $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
                $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
            }
        }
    };

    self.searchFilterClick = function (el, e) {
        var selected_target = $('#hdnSelectedGroup').val();
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
            var selected_option = '';
            if (context_data.selectedFilterOption() != "")
                selected_option = context_data.selectedFilterOption();
            else {
                selected_option = "emailAddress";
                context_data.selectedFilterOption(selected_option);
            }
        }
        $('input:radio[name="rdoFilterList"]').filter('[value="' + context_data.selectedFilterOption() + '"]').parent().find("label[for].ui-btn").click();
        //$("input[name=rdoFilterList]").val(context_data.selectedFilterOption());
        //$('#rdo' + selected_option).attr('checked', true).checkboxradio('refresh');
        $('#popupSearchFilter').popup('open');
    };

    self.setFilter = function (el, e) {
        var selected_target = $('#hdnSelectedGroup').val();
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
            var selected_option = $("input[name=rdoFilterList]:checked").val();
            context_data.selectedFilterOption(selected_option);
            if ($("#txt" + selected_target.replace('dv', '')).val().toLowerCase() == $("#txt" + selected_target.replace('dv', '')).attr('placeholder').toLowerCase())
                $("#txt" + selected_target.replace('dv', '')).val('');

            switch (selected_option) {
                case "firstName":
                    $("#txt" + selected_target.replace('dv', '')).attr("placeholder", "Search by First Name");
                    break;
                case "lastName":
                    $("#txt" + selected_target.replace('dv', '')).attr("placeholder", "Search by Last Name");
                    break;
                case "emailAddress":
                    $("#txt" + selected_target.replace('dv', '')).attr("placeholder", "Search by Email address");
                    break;
            }
            //window.setTimeout(function setPlaceHo)
            createPlaceHolderforIE();
        }
        $('#popupSearchFilter').popup('close');
    }

    self.getSelectedEmailsList = function (id, context_data, caption, page_index) {
        var emails_list_service_url = serviceURLDomain + "api/GizmoEmailCustomer_find/" + jobCustomerNumber + "/" + sessionStorage.facilityId + "/" + sessionStorage.jobNumber + "/" + context_data.caption().toLowerCase()

        var post_json = {
            "pageIndex": (page_index != undefined && page_index != null && page_index != "") ? page_index : 0,
        };
        var search_term = context_data.selectedFilterOption();
        post_json[search_term] = "";

        post_json[search_term] = (context_data.searchValue() != "" && $('#' + id).find('input[ data-type="search"]').val() != $('#' + id).find('input[ data-type="search"]').attr('placeholder')) ? context_data.searchValue() : "";

        postCORS(emails_list_service_url, JSON.stringify(post_json), function (response_data) {
            context_data.emails.removeAll();
            $.each(response_data.emails, function (key, val) {
                context_data.emails.push(new emailRecipient(val))
            });
            context_data.pagingData(createPagerObject1(response_data.totalPages, 0, 0));
            context_data.prevSearchValue(context_data.searchValue());
            var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
            var is_more_visible = (parseInt(selected_page_value + 1) < parseInt(response_data.totalPages)) ? true : false;
            context_data.showMoreOption(is_more_visible);
            $("#" + context_data.caption()).find('ul').trigger("create").listview().listview('refresh');
            window.setTimeout(function setUI() {
                $("div[id='dv" + context_data.caption() + "']").find('ul').listview('refresh').trigger('create');
                $("div[id='dv" + context_data.caption() + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
                $("div[id='dv" + context_data.caption() + "']").find('ul').find("input[data-type=search]").trigger('create');
                createPlaceHolderforIE();
            }, 5);
        }, function (error_response) {
            showErrorResponseText(error_response, false);
        });
    };

    self.search = function (data, event) {
        if (jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER) {
            if (!self.searchValidation()) return false;
            var selected_target = $('#hdnSelectedGroup').val();
            var page_index = 0;
            var context_data = "";
            //var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
            var ctrl_id = selected_target.replace(/ /g, '_');
            if (selected_target != "") {
                //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
                context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
                page_index = context_data.selectedPageValue();
                page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
                page_index = (context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1) ? 0 : parseInt(page_index);
                if (event != undefined)
                    context_data.selectedPageValue(0);
                self.getSelectedEmailsList(ctrl_id, context_data, context_data.caption(), page_index);
                //(ctrl, context_data, title, service_url, index_list_name, page_index, sort_order, search_term)
            }
        }
    };

    self.searchWithEnterKey = function (data, event) {
        try {
            if (event.which == 13) {
                //ko.contextFor($('#dv' + $('#hdnSelectedGroup').val().replace(/ /g, '_'))[0]).$data.searchValue(event.target.value);
                ko.contextFor($('#' + $('#hdnSelectedGroup').val().replace(/ /g, '_'))[0]).$data.searchValue(event.target.value);
                self.search(data, event);
                return false;
            }
            return true;
        }
        catch (e) {
        }
    };

    self.ValidateEmailConfirm = function () {
        var msg = "";
        var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var regxSubject = /^[ A-Za-z0-9_./#&-]*$/;
        if (($("#txtSubject").val().trim() == $("#txtSubject").attr('placeholder')) || $("#txtSubject").val().trim() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Subject.";
            else
                msg = "Please enter Subject.";
            $("#txtSubject").val('');
        }
        else if (($("#txtSubject").val().trim() != $("#txtSubject").attr('placeholder')) && $("#txtSubject").val().trim() != "" && !regxSubject.test($("#txtSubject").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Subject.";
            else
                msg = "Please enter valid Subject.";
            $("#txtSubject").val('');
        }
        if ($("#txtFromEmail").val() == undefined || $("#txtFromEmail").val() == null || ($("#txtFromEmail").val().trim() == $("#txtFromEmail").attr('placeholder')) || $("#txtFromEmail").val().trim() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter From Email address.";
            else
                msg = "Please enter From Email address.";
            $("#txtFromEmail").val('');
        }

        else if (($("#txtFromEmail").val().trim() != $("#txtFromEmail").attr('placeholder')) && $("#txtFromEmail").val().trim() != "" && !regEmail.test($("#txtFromEmail").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid From Email address.";
            else
                msg = "Please enter valid From Email address.";
            $("#txtFromEmail").val('');
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $('#popupSendEmailConfirm').popup('close');
            $("#popupDialog").popup('open');
            $("#okBut").bind('click', function () {
                $('#popupDialog').popup('close');
                $("#okBut").unbind('click');
                $('#okBut').bind('click', function () {
                    $('#popupDialog').popup('close');
                });
                window.setTimeout(function setDelay() {
                    $('#popupSendEmailConfirm').popup('open');
                }, 300);
            });
            return false;
        }
        return true;
    };

    self.searchValidation = function () {
        var context_data = "";
        var regxSearch;
        var selected_target = $('#hdnSelectedGroup').val();
        //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
        context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
        var search_value = context_data.searchValue().toLowerCase();
        var selected_option = $("input[name=rdoFilterList]:checked").val();
        switch (selected_option) {
            case "firstName":
            case "lastName":
                regxSearch = /^[a-zA-Z ]*$/;
                break;
            case "emailAddress":
                regxSearch = /^[ A-Za-z0-9@_.-]*$/;
                break;
        }
        if (search_value != "" && !regxSearch.test(search_value)) {
            $('#alertmsg').html("Please enter valid search text");
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            return false;
        }
        return true;
    };
};

String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
