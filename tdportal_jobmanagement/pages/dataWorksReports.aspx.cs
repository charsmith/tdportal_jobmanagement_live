﻿using System;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class pages_dataWorksReports : System.Web.UI.Page
{
    #region -------------------- PUBLIC METHODS --------------------------------
    /// <summary>
    /// Calling loadDWReport on Page Load Method
    /// </summary>
    /// <param name=""></param>     
    protected void Page_Load(object sender, EventArgs e)
    {
        loadDataWorksReport();
    }

    protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.loadDataWorksReport();
    }

    /// <summary>
    /// To display the DataWorks "Water Fall" Report
    /// </summary>
    /// <param name=""></param>
    private void loadDataWorksReport()
    {        
        string sp_name = "";
        string sp_market_code_param = "";
        string sp_name_param = "774252";
        DataTable reports_data_table =null;
        ReportDataSource rpt_data_source = new ReportDataSource();
        rptViewer.ProcessingMode = ProcessingMode.Local;

        ReportParameter job_number = new ReportParameter("job_number", sp_name_param.ToString());
        ReportParameter report_type = new ReportParameter("report_type", ddlReportType.SelectedItem.Value.ToString());
        if (Request["type"].ToString() == "jobInformationRecap")
        {
            rptViewer.LocalReport.ReportPath = Server.MapPath("..\\reports\\rptJobInformationRecap.rdlc");
        }
        else if (Request.QueryString["type"].ToString() == "qc")
        {
            sp_name = "uspRptWaterfall";
            reports_data_table = getDataTable(sp_name, new SqlParameter("@jobNumber", sp_name_param));
            rpt_data_source.Value = reports_data_table;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(rpt_data_source);
            rpt_data_source.Name = "DataSet1_uspRptWaterfall";
            rptViewer.LocalReport.ReportPath = Server.MapPath("..\\reports\\rptWaterfall.rdlc");
            rptViewer.LocalReport.SetParameters(new ReportParameter[] { job_number});           
        }
        else if (Request.QueryString["type"].ToString() == "prizmQc")
        {
            sp_name = "uspRptWaterFall_PrizmQC";
            reports_data_table = getDataTable(sp_name, new SqlParameter("@jobNumber", sp_name_param));
            rpt_data_source.Value = reports_data_table;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(rpt_data_source);
            rpt_data_source.Name = "DataSet1_uspRptWaterfall_PrizmQC";
            rptViewer.LocalReport.ReportPath = Server.MapPath("..\\reports\\rptWaterFallPrizmQC.rdlc");
            rptViewer.LocalReport.SetParameters(new ReportParameter[] { job_number});
        }
        else if (Request.QueryString["type"].ToString() == "clusterQc")
        {
            sp_name = "uspRptPrizmCoreCluster";
            sp_market_code_param = "0";
            reports_data_table = getDataTable(sp_name, new SqlParameter("@jobNumber", sp_name_param), new SqlParameter("@marketCode", sp_market_code_param));
            rpt_data_source.Value = reports_data_table;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(rpt_data_source);
            rpt_data_source.Name = "DataSet1_uspRptPrizmCoreCluster";
            rptViewer.LocalReport.ReportPath = Server.MapPath("..\\reports\\rptPrizmCoreCluster.rdlc");
            rptViewer.LocalReport.SetParameters(new ReportParameter[] { job_number});
        }
        else if (Request.QueryString["type"].ToString() == "allPrizm")
        {
            sp_name = "uspRptAllPrizmSegments";
            sp_market_code_param = "1";
            reports_data_table = getDataTable(sp_name, new SqlParameter("@jobNumber", sp_name_param), new SqlParameter("@marketCode", sp_market_code_param));
            rpt_data_source.Value = reports_data_table;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(rpt_data_source);
            rpt_data_source.Name = "DataSet1_uspRptPrizmCoreCluster";
            rptViewer.LocalReport.ReportPath = Server.MapPath("..\\reports\\rptAllPrizmSegments.rdlc");
            rptViewer.LocalReport.SetParameters(new ReportParameter[] { job_number});
        }
        else if (Request.QueryString["type"].ToString() == "productStoreSummary")
        {
            this.ddlReportType.Visible = true;
            this.lblReportType.Visible = true;
            sp_name = "uspRptProductSummary";
            sp_market_code_param = "1";
            reports_data_table = getDataTable(sp_name, new SqlParameter("@jobNumber", sp_name_param), new SqlParameter("@marketCode", sp_market_code_param), new SqlParameter("@reportType", this.ddlReportType.SelectedItem.Value));
            rpt_data_source.Name = "DataSet1_uspRptPrizmCoreCluster";
            rpt_data_source.Value = reports_data_table;

            sp_name = "uspRptStoreSummary";
            sp_market_code_param = "1";
            reports_data_table = getDataTable(sp_name, new SqlParameter("@jobNumber", sp_name_param), new SqlParameter("@marketCode", sp_market_code_param), new SqlParameter("@reportType", this.ddlReportType.SelectedItem.Value));
            ReportDataSource rpt_data_source_summary = new ReportDataSource();
            rpt_data_source_summary.Name = "DataSet2_uspRptPrizmCoreCluster";
            rpt_data_source_summary.Value = reports_data_table;
           

            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(rpt_data_source);
            rptViewer.LocalReport.DataSources.Add(rpt_data_source_summary);
            
            rptViewer.LocalReport.ReportPath = Server.MapPath("..\\reports\\rptProductSummary.rdlc");
            rptViewer.LocalReport.SetParameters(new ReportParameter[] { job_number, report_type });
        }
    }
    #endregion ----------------------------------------------------------------------
    #region -------------------- PRIVATE METHODS --------------------------------
    /// <summary>
    /// getConnection
    /// Returns the connectionstring from the configuration file for the dataworks database.
    /// </summary>
    /// <returns>connectionstring</returns>     
    private string getConnection()
    {
        connection_string = ConfigurationManager.ConnectionStrings["tdDataWorksConnectionString"].ConnectionString;
        return connection_string;
    }
    /// <summary>
    /// getConnection
    /// Returns the connectionstring from the configuration file for the dataworks database.
    /// </summary>
    /// <returns>connectionstring</returns>  
    private DataTable getDataTable(string procedure_name, params SqlParameter[] arr_params)
    {
        DataTable data_table = new DataTable();
        //get the connectionstring
        connection_string = getConnection();
        using (SqlConnection conn_object = new SqlConnection(connection_string))
        {
            conn_object.Open();
            using (SqlCommand command_object = new SqlCommand())
            {
                command_object.Connection = conn_object;
                command_object.CommandType = CommandType.StoredProcedure;
                command_object.CommandText = procedure_name;
                if (arr_params != null)
                {
                    foreach (SqlParameter param in arr_params)
                        command_object.Parameters.Add(param);
                }
                using (SqlDataAdapter data_adapter = new SqlDataAdapter(command_object))
                {
                    data_adapter.Fill(data_table);
                }
            }
        }
        return data_table;
    }
    #endregion ----------------------------------------------------------------------
    #region -------------------- PUBLIC VARIABLES --------------------------------
    public string connection_string = string.Empty;
    #endregion --------------------------------------------------------------------
  
}