﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dataWorksReports.aspx.cs"
    Inherits="pages_dataWorksReports" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB">
<head>
    <title>Conversion Alliance</title>
</head>
 <body>
    <div style="text-align:right;">         
        <form runat="server" id="form1">
        <table width="75%">
            <tr>
                <td align="left"><asp:Label ID="lblReportType" runat="server" Visible="false">Report Type:</asp:Label>
                    <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="true" Visible="false"
                        onselectedindexchanged="ddlReportType_SelectedIndexChanged">
                        <asp:ListItem Text="Revenue" Value="Revenue"></asp:ListItem>
                        <asp:ListItem Text="Budget" Value="Budget"></asp:ListItem>
                        <asp:ListItem Text="Volume" Value="Volume"></asp:ListItem>
                        <asp:ListItem Text="Radius" Value="Radius"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <p>
                            <rsweb:ReportViewer ID="rptViewer" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="1050px" Height="550px" TabIndex="3">
                            </rsweb:ReportViewer>
                        </p>
                    </div>
                </td>
            </tr>
        </table>
        </form>
    </div>
</body>
</html>
