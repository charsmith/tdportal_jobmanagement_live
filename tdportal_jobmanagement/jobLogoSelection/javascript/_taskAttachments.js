﻿var taskAttachments = function (self) {
    new uploadFilesCommon(self);
    var illegalFilesCnt = 0;
    var invalidFileTypesCnt = 0;
    var existingFilesCnt = 0;
    var msg = "";
    if (window.location.href.toLowerCase().indexOf('logoselection') > -1)
        self.maxNumberOfFiles = 1;
    self.filesToAttach = ko.observableArray([]);
    self.fileToDropBox = ko.observableArray([]).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 0 } });
    self.filesUploaded = ko.observableArray([]).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 0 } });
    self.selectedFiles = [];
    self.validateFile = function (file_name, selected_type) {
        //var ctrl = e.target || e.currentTarget || e.targetElement || e.sourceElement;
        switch (selected_type) {
            case "0":
                msg += "<li>Please select List type.</li>";
                break;
            case "1":
            case "2":
            case "6":
                var allowed_extension = ["txt", "csv", "dat", "data", "lst", "xls", "xlsx", "doc", "docx", "gz", "zip"];
                return self.validateFiles(file_name, allowed_extension);
                break;
            case "25":
                var allowed_extension;
                if (window.location.href.toLowerCase().indexOf('summary') > -1) {
                    allowed_extension = ["jpg", "jpeg", "png", "pdf", "gif", "tif", "tiff", "svg", "html", "htm"];
                }
                else {
                    allowed_extension = ["jpg", "jpeg", "pdf", "tif", "tiff", "eps"];
                }
                return self.validateFiles(file_name, allowed_extension);
                break;
            case "9":
                var allowed_extension = ["pdf", "xls", "xlsx", "doc", "docx", "csv"];
                return self.validateFiles(file_name, allowed_extension);
                break;
        }
    };

    self.setValidationMsg = function (selected_type) {
        var mesg = "";
        if (invalidFileTypesCnt > 0)
            if (selected_type == "25")
                if (window.location.href.toLowerCase().indexOf('summary') > -1) {
                    mesg = '<li>Invalid file type selection(s). Only "JPG","JPEG","PNG","PDF","GIF","TIF","TIFF","SVG","HTML" and "HTM" types are accepted.</li>';
                }
                else {
                    mesg = '<li>Invalid file type selection(s). Only "JPG","JPEG","PDF","TIF","TIFF" and "EPS" types are accepted.</li>';
                }
            else if (selected_type == "9")
                mesg = '<li>Invalid file type selection(s). Only "PDF","XLS","XLSX","DOC","DOCX" and "CSV" types are accepted.</li>';
            else
                mesg = '<li>Invalid file type selection(s). Only "TXT","CSV","DAT","DATA","LST","XLS","XLSX","DOC","GZ","ZIP" and "DOCX" types are accepted.</li>';

        if (illegalFilesCnt > 0)
            mesg += '<li>Illegal characters exist(s) in file name. Cannot upload.</li>';
        return mesg;
    };

    self.setValidationMsgPopup = function () {
        window.setTimeout(function getDelay() {
            $('#okBut').bind('click', function () {
                $('#popupDialog').popup('close');
                $('#okBut').unbind('click');
                $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');')
                msg = "";
                window.setTimeout(function openFileWin() {
                    $("#popupSetTaskStatus").popup("open");
                }, 200);
            });
            $('#popupDialog').popup('open');
            return false;
        }, 200);
    };

    self.validateFiles = function (file_name, allowed_extensions) {
        var file_name_type = "";
        var file_names = "";
        var check_file_name = (file_name.name.indexOf('\\') > -1) ? file_name.name.substring(file_name.name.lastIndexOf('\\') + 1, file_name.name.length) : file_name.name;
        var illegal_filename_regexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;

        if (!illegal_filename_regexp.test(check_file_name.substring(0, check_file_name.lastIndexOf('.')))) {
            illegalFilesCnt++;
            return false;
        }
        else {
            var extension = file_name.name.split('.');
            var extension = file_name.name.substring(file_name.name.lastIndexOf('.') + 1);
            var isValidFileFormat = false;
            if (typeof (extension) != 'undefined' && extension.length > 1) {
                for (var i = 0; i < allowed_extensions.length; i++) {
                    if (extension.toLowerCase() == allowed_extensions[i]) {
                        isValidFileFormat = true;
                        break;
                    }
                }
                if (!isValidFileFormat) {
                    invalidFileTypesCnt++;
                    return false;
                }
                    //else if (self.verifyFileExistencyInUploads(file_name)) {
                    //    existingFilesCnt++;
                    //    return false;
                    //}
                else
                    return true;
            }
            //var filemappername = file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.lastIndexOf('.')) + "_map.xml";
        }
    };

    self.ignoreDrag = function (e) {
        e.preventDefault();
    };

    self.fileTypeChanged = function (data, e) {
        var type = $(e.target).val();
        if (type != "0") {
            var sel_type = $(e.target).find('option:selected').text();// getFileType(type);
            var selectedCategory = ko.utils.arrayFirst(self.fileToDropBox(), function (item) {
                return item.type() === sel_type;
            });
            if (selectedCategory == null) {
                self.fileToDropBox.push({
                    "type": ko.observable(sel_type),
                    "filesList": ko.observableArray([])
                });
            }
        }
    };

    self.makeDrop = function (e, is_multiple, ctrl) {
        var fileList = (!is_multiple) ? (e.originalEvent.dataTransfer.files) : $(ctrl)[0].files;
        if (!('draggable' in document.createElement('span'))) {
            fileList = [];
            fileList.push({ "name": e.target.value.substring(e.target.value.lastIndexOf('\\') + 1) });
        }
        e.preventDefault();
        fileExistenceCnt = 0;
        invalidFileTypesCnt = 0;
        illegalFilesCnt = 0;
        if (!is_multiple) {
            //    isDragDrop = true;
        }
        //    var mode = $('#hidMode').val();
        //    if (mode == "edit" || mode == "replace") {
        //        if (temp_data.length > 0) temp_edit_file = temp_data;
        //        temp_data = [];
        //        if (fileList.length > 1) {
        //            $("#popupListDefinition").popup("close");
        //            $('#validationAlertmsg').html("Please select/drag and drop single file to replace the exising file.");
        //            window.setTimeout(function getDelay() {
        //                $('#validationPopupDialog').popup('open');
        //                return false;
        //            }, 200);
        //            return false;
        //        }
        //    }
        //    else {

        var total_files_count = 0;
        if (window.location.href.toLowerCase().indexOf('summary') > -1)
            total_files_count = self.filesToAttach().length + fileList.length;
        else
            total_files_count = self.filesToAttach().length + self.uploadedFiles().length;

        if ((total_files_count > self.maxNumberOfFiles)) {
            var msg = "";
            if (window.location.href.toLowerCase().indexOf('summary') > -1) {
                msg = "The maximum number of artwork files to be uploaded per order has been reached. Please remove a previously uploaded file in order to upload a new file to this order. If this order requires more than " + self.maxNumberOfFiles + " artwork files, please contact your Account Manager.";
            }
            else {
                msg = "Only one artwork file is allowed to upload.";
            }
            $('#alertmsg').html(msg);
            $("#popupSetTaskStatus").popup("close");
            self.setValidationMsgPopup();
            return false;
        }
        else if ($('#ddlFileType').val() == "0") {
            $('#alertmsg').html("Please select List type.");
            $("#popupSetTaskStatus").popup("close");
            self.setValidationMsgPopup();
            return false;
        }
        else {
            if (fileList && fileList.length > 0) {
                if (self.filesToAttach().length == 0)
                    $('#drop-area ul li').remove();
                var selected_type = $('#ddlFileType').val();
                var selected_type_name = getFileType(selected_type);
                var file_info = {};
                var temp_file_list = [];
                var drop_box_files_list = ko.toJS(self.fileToDropBox);
                for (var i = 0; i < fileList.length; i++) {
                    var temp_obj = {};
                    var selectedCategory = ko.utils.arrayFirst(self.fileToDropBox(), function (item) {
                        return (item.type().toLowerCase().replace(/ /g, '') === selected_type_name);//
                    });
                    if (self.validateFile(fileList[i], selected_type)) {
                        self.selectedFiles.push(fileList[i].name + '^' + selected_type);
                        if (selectedCategory != undefined && selectedCategory != null) {
                            selectedCategory.filesList.push({
                                "fileName": fileList[i].name,
                                "fileType": selected_type,
                                "fileObj": (!('draggable' in document.createElement('span'))) ? null : fileList[i]
                            });
                        }
                        self.filesToAttach.push({
                            "fileName": fileList[i].name,
                            "fileType": selected_type
                        });
                    }
                }

                if (invalidFileTypesCnt > 0 || illegalFilesCnt > 0) {
                    msg = self.setValidationMsg($('#ddlFileType').val());
                    msg = "<ul>" + msg + "</ul>";
                    $('#alertmsg').html(msg);
                    $("#popupSetTaskStatus").popup("close");
                    self.setValidationMsgPopup();
                    return false;
                }
                window.setTimeout(function createList() {
                    $('#drop-area ul').listview().listview('refresh');
                }, 50);
            }
        }
        if (!('draggable' in document.createElement('span'))) {
            self.resetFileInput();
        }
    };
    self.removeSelectedFile = function (data, el) {
        var selectedCategory = ko.utils.arrayFirst(self.fileToDropBox(), function (item) {
            return item.type() === $(el.target).attr('type');
        });
        self.selectedFiles.pop(data.fileName + '^' + data.fileType);
        selectedCategory.filesList.remove(data);
    };
    self.resetFileInput = function () {
        var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'); // document.getElementById('spnFileInput');
        var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
        var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
        if (fileInput.value != "") {
            // Create a new file input
            var newFileInput = fileInput.cloneNode(true);
            newFileInput.value = null;
            $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
            tdFileInputsTemp.appendChild(newFileInput);
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.makeDrop(e, true, e.target)
            });
        }
    };
    self.uploadSelectedFiles = function (data, e) {
        if (self.selectedFiles.length == 0) {
            $('#alertmsg').html('Please select files to upload.');
            $('#okBut').bind('click', function () {
                $('#okBut').unbind('click');
                $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                if (window.location.href.toLowerCase().indexOf('summary') > -1) {
                    window.setTimeout(function setPopups() {
                        $('#popupSetTaskStatus').popup('open');
                    }, 200);
                }
            });
            $('#popupSetTaskStatus').popup('close');
            window.setTimeout(function setPopups() {
                $('#popupDialog').popup('open');
            }, 200);
            return false;
        }
        self.uploadingFiles = self.selectedFiles.join('|');
        self.uploadFiles(e);
        $.each(self.fileToDropBox(), function (key, val) {
            //var sel_type = getFileType(type);
            var selectedCategory = ko.utils.arrayFirst(self.filesUploaded(), function (item) {
                return item.type().toLowerCase().replace(/ /g, '') === val.type().toLowerCase().replace(/ /g, '');
            });
            if (selectedCategory != null && selectedCategory.length > 0) {
                $.each(val.filesList(), function (key1, val1) {
                    delete val1.fileObj;
                    selectedCategory[0].filesList.push(val1);
                });
            }
            else {
                var temp_list = [];
                $.each(val.filesList(), function (key1, val1) {
                    delete val1.fileObj;
                    temp_list.push(val1);
                });
                self.filesUploaded.push({
                    "type": val.type,
                    "filesList": ko.observableArray(temp_list)
                });
                temp_list = [];
            }
        });
        if (window.location.href.toLowerCase().indexOf('logoselection') == -1)
            self.selectedProcess().processAttachments(self.filesUploaded());
        else {
            var temp = ko.toJS(self.filesUploaded());
            $.each(temp, function (key, val) {
                $.each(val.filesList, function (key1, val1) {
                    self.selectedInHomeStoreLogoInfo().logoFile(new logoFileVM(self.selectedStore(), val1.fileName, "", "", ""));
                });
            });
            //self.uploadedFiles(self.filesUploaded());
            $('div').trigger('create');
        }
        self.filesUploaded([]);
        self.fileToDropBox([]);
        self.selectedFiles = [];
    };
    self.removeUploadedFile = function (data, e) {
        var file_delete_url = serviceURLDomain + "api/FileUpload/delete/" + self.facilityId + "/" + self.jobNumber + "/";
        file_delete_url = file_delete_url + data.fileType + "/" + data.fileName;
        getCORS(file_delete_url, null, function (prod_data) {
            if (prod_data != "" && $.parseJSON(prod_data).Message != undefined && $.parseJSON(prod_data).Message.indexOf('error') > -1) {
                $('#popupSetTaskStatus').popup('close');
                $('#alertmsg').html('An error occurred. Please contact Administrator.');
                $('#okBut').bind('click', function () {
                    $('#okBut').unbind('click');
                    $('#popupDialog').popup('close');
                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                    if (window.location.href.toLowerCase().indexOf('summary') > -1) {
                        window.setTimeout(function setPopups() {
                            $('#popupSetTaskStatus').popup('open');
                        }, 400);
                    }
                });
                window.setTimeout(function showDeleteMsg() {
                    $('#popupDialog').popup('open', { positionTo: 'window' });
                }, 200);
                //window.setTimeout(function displayErrorMsg() { $('#popupDialog').popup('open', { positionTo: 'window' }); }, 200);
                return false;
            }
            else {
                $('#popupSetTaskStatus').popup('close');
                var sel_type = getFileType(data.fileType);

                var selectedCategory;
                if (window.location.href.toLowerCase().indexOf('summary') > -1) {
                    selectedCategory = ko.utils.arrayFirst(self.filesUploaded(), function (item) {
                        return item.type().toLowerCase().replace(/ /g, '') === sel_type;
                    });
                }
                else {
                    selectedCategory = ko.utils.arrayFirst(self.uploadedFiles(), function (item) {
                        return item.type().toLowerCase().replace(/ /g, '') === sel_type;
                    });
                }

                if (selectedCategory != null) {
                    if (selectedCategory.filesList != undefined && selectedCategory.filesList().length > 0)
                        selectedCategory.filesList.remove(data);
                }
                $('#alertmsg').html('File deleted successfully.');
                $('#okBut').bind('click', function () {
                    $('#okBut').unbind('click');
                    $('#popupDialog').popup('close');
                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                    if (window.location.href.toLowerCase().indexOf('summary') > -1) {
                        window.setTimeout(function setPopups() {
                            $('#popupSetTaskStatus').popup('open');
                        }, 400);
                    }
                    else {
                        self.filesUploaded([]);
                        self.fileToDropBox([]);
                        self.filesToAttach([]);
                        $('#dvUploadedFilesList').css('display', 'none');
                    }
                });
                window.setTimeout(function showDeleteMsg() {
                    $('#popupDialog').popup('open', { positionTo: 'window' });
                }, 200);
            }
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };
};