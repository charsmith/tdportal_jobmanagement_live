﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gVarArtworkInfoServiceUrl = "../jobLogoSelection/JSON/_jobLogoSelection.JSON";
var gServiceData;
var gServiceOptionalLogos = [];
var pageObj;
var companyData;
var primaryFacilityData;
var gOutputData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobLogoSelection').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_jobLogoSelection');
    createConfirmMessage("_jobLogoSelection");
    displayNavLinks();
    getOptionalLogosInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
    //$('#sldrShowHints').val(sessionStorage.showDemoHints).slider('refresh');
});

$(document).on('pageshow', '#_jobLogoSelection', function (event) {
    //load IFRAME for IE to upload files.
    if ($.browser.msie) {
        $('#formUpload').remove();
    }
    loadUploadIFrame();

    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        pageObj.makeDrop(e, true, e.target)
    });

    window.setTimeout(function () {
        if (pageObj == undefined || pageObj == null) {
            pageObj = new loadCompanyInfo();
            var $dropArea = $("#drop-area");
            $dropArea.on({
                "drop": pageObj.makeDrop,
                "dragenter": pageObj.ignoreDrag,
                "dragover": pageObj.ignoreDrag
            });
            ko.applyBindings(pageObj);
        }
        //window.setTimeout(function () {
        $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
        $('div').trigger('create');
        $('#ddlLocations').selectmenu('refresh');
        pageObj.getSelectedInfo();
        $('#chkReplicate').checkboxradio('refresh');
        //}, 500);
    }, 500);
    persistNavPanelState();
});

var getOptionalLogosInfo = function () {

    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        $.getJSON(gVarArtworkInfoServiceUrl, function (data) {
            $.each(data, function (key, val) {
                //if (key == gOutputData.templateName.substring(1, 4).replace('_', 'X')) {
                if (gOutputData.templateName.indexOf(key) > -1) {
                    gServiceOptionalLogos = val;
                    return false;
                }
                //var selected_artwork_info = $.gServiceData.filter
            });
        });



        //$.getJSON(gVarArtworkInfoServiceUrl, function (data) {
        //    gServiceOptionalLogos = data;
        //});
    }
}

var logoFileVM = function (location_pk, file_name, file_size, file_status, file_pk) {
    var self = this;
    self.locationPk = ko.observable('');
    self.locationPk(location_pk);
    self.fileName = ko.observable('');
    self.fileName(file_name);
    self.fileSizePretty = ko.observable('');
    self.fileSizePretty(file_size);
    self.fileStatus = ko.observable('');
    self.fileStatus(file_status);
    self.filePk = ko.observable('');
    self.filePk(file_pk);
};

var logoFileInfo = function (logo_file, optional_logos_selected, is_enabled) {
    var self = this;
    self.logoFile = ko.observable({});
    self.logoFile(logo_file);
    self.optionalLogosSelected = ko.observableArray([]);
    self.optionalLogosSelected(optional_logos_selected);
    self.isEnabled = ko.observable('');
    self.isEnabled(is_enabled);
    self.name = "logoSelection";
};

var optionalLogosVM = function (logo_name, image_name, is_checked) {
    var self = this;
    self.logoName = ko.observable('');
    self.logoName(logo_name);
    self.logoFileName = ko.observable('');
    self.logoFileName(image_name);
    self.isChecked = ko.observable();
    self.isChecked(is_checked);
};

var selectedLogosListInfo = function (logo_list, is_replicated) {
    var self = this;
    self.isReplicated = ko.observable('');
    self.isReplicated(is_replicated);
    $.each(logo_list, function (key, val) {
        self[key] = ko.observable({});
        self[key](val);
    });
};

var locationVm = function (store_id, store_name, store_pk) {
    var self = this;
    self.locationId = store_pk;
    if (store_id != undefined && store_id != null && store_id != "")
        self.locationName = store_id + ' - ' + store_name;
    else
        self.locationName = store_name;
};

var loadCompanyInfo = function () {
    var self = this;
    new taskAttachments(self);
    self.uploadedFiles = ko.observableArray([]);
    self.optionalLogos = ko.observableArray([]);

    self.locationsList = ko.observableArray([]);
    self.selectedLocation = ko.observable({});

    self.logoSelectionInfo = ko.observable({});
    self.prevLogoSelectionInfo = ko.observable({});
    self.selectedInHomeStoreLogoInfo = ko.observable({});
    self.selectedInhomeInfo = ko.observable({});
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.selectedStore = ko.observable('');
    self.isReplicated = ko.observable();
    self.previousSelectedStore = ko.observable({});

    //Loading all the selected stores into drop down.
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
            if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
                $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                    if (key == 0) {
                        self.selectedLocation(val.pk);
                        self.selectedStore(val.pk);
                        self.previousSelectedStore(self.selectedStore());
                    }
                    self.locationsList.push(new locationVm(val.storeId, val.storeName, val.pk));
                });
            }
        }
        if ((gOutputData.logoSelectionAction != undefined && gOutputData.logoSelectionAction != null && gOutputData.logoSelectionAction != "")) {
            var temp_inhomewise_list = {};
            var is_replicated = '';
            var index = 0;
            var temp_bases_list = {};
            var uploaded_logos = [];
            var optional_logos = [];
            $.each(gOutputData.logoSelectionAction, function (key, val) {
                if (is_replicated == '' && key == 'isReplicated')
                    is_replicated = val;
                else {
                    temp_inhomewise_list[key] = {};
                    $.each(val, function (key1, val1) {
                        var temp_base_info = [];
                        var optional_logos_list = []
                        $.each(val1.optionalLogosSelected, function (key2, val2) {
                            optional_logos_list.push(new optionalLogosVM(val2.logoName, val2.logoFileName, val2.isChecked));
                        });
                        var logo_file = {}
                        if (Object.keys(val1.logoFile).length > 0) {
                            logo_file = new logoFileVM(val1.logoFile.locationPk, val1.logoFile.fileName, val1.logoFile.fileSizePretty, val1.logoFile.fileStatus, val1.logoFile.filePk);
                        }
                        temp_inhomewise_list[key][key1] = new logoFileInfo(logo_file, optional_logos_list, val1.isEnabled);
                    });
                }
            });
            if (is_replicated == true)
                self.isReplicated(true);
            else
                self.isReplicated(false);
            self.logoSelectionInfo(new selectedLogosListInfo(temp_inhomewise_list, is_replicated));
            self.prevLogoSelectionInfo(new selectedLogosListInfo(temp_inhomewise_list, is_replicated));
        }
    }

    //Loading Inhome dates nav bar.
    if (gOutputData != undefined && gOutputData != null) {
        var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
        var cnt = -1;
        $.each(gOutputData, function (key, val) {
            if (key.toLowerCase().indexOf('inhome') > -1) {
                var temp_list = {};
                temp_list[key] = val;
                self.inHomeDatesList.push({
                    "showCheckIcon": false,
                    'dateValue': val
                });
                cnt++;
                if (cnt == 0)
                    nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                else
                    nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
            }
        });
        nav_bar_items += '</ul></div>';
        self.currentInHome(self.inHomeDatesList()[0]);
    }

    $.each(gServiceOptionalLogos, function (key, val) {
        var optional_logo_path = logoPath;
        optional_logo_path += appPrivileges.companyName;
        var temp_name = gOutputData.templateName.substring(1);
        optional_logo_path += "/" + temp_name.substring(0, temp_name.indexOf('_'));
        optional_logo_path += "/" + "OptionalLogos";
        optional_logo_path += "/" + val.logoFileName;

        self.optionalLogos.push(new optionalLogosVM(val.logoName, optional_logo_path, val.isChecked));
    });

    self.selectArtworkClick = function (data, event) {
        var el = event.target || event.currentTarget;
        $('#ddlFileType').val('25').selectmenu('refresh');
        $('#ddlFileType').trigger('change');
        $('#popupSetTaskStatus').popup('open');
    };
    self.cancelSelectArtworkClick = function () {
        self.filesToAttach([]);
        self.fileToDropBox([]);
        self.selectedFiles = [];
        $('#popupSetTaskStatus').popup('close');
    };

    self.confirmContinue = function (current_page, page_name, i_count, click_type) {
        self.persistSelectedBaseInfo();
        var temp = ko.toJS(self.logoSelectionInfo());
        self.updateInHomeNavBarUI(temp);
        var ctrls = $('#dvNavBar div ul li a.ui-icon-edit');
        if (ctrls.length > 0 && click_type == "onlycontinue") {
            var tmp_list = "";
            $.each(ctrls, function (key, val) {
                tmp_list += (tmp_list != "") ? (key < (ctrls.length - 1)) ? ', ' : ' and ' + $(this).attr('data-inHome') : $(this).attr('data-inHome');
            });

            $('#confirmMsg').html('You have not completed setup for ' + tmp_list + '. Do you want to stay on this page to continue setup or go to the next page?');
            $('#okButConfirm').text('Go To Next Page');
            $('#cancelButConfirm').text('Continue Setup');

            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#cancelButConfirm').text('Cancel');
                $('#popupConfirmDialog').popup('close');
            });
            $('#okButConfirm').bind('click', function () {
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#cancelButConfirm').text('Cancel');
                $('#popupConfirmDialog').popup('close');
                goToNextPage("../jobOfferSetup/jobOfferSetup.html", "../jobOfferSetup/jobOfferSetup.html", 5, 'onlycontinue');
                self.updateLogoSelectionInfo(current_page, page_name, i_count, click_type);
            });
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        else {
            //goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
            return self.updateLogoSelectionInfo(current_page, page_name, i_count, click_type);
        }
    };

    self.updateLogoSelectionInfo = function (current_page, page_name, i_count, click_type) {
        var selected_inhome = self.currentInHome().dateValue;
        var temp_json = ko.toJS(self.logoSelectionInfo);
        gOutputData.logoSelectionAction = temp_json;
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        if (click_type == "onlycontinue")
        window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
        return true;
    };


    self.persistSelectedBaseInfo = function () {
        var selected_inhome = self.currentInHome().dateValue;

        if (selected_inhome != "" && self.selectedStore() != "") {
            var temp_optional_logos = ko.observableArray([]);
            $.each(self.optionalLogos(), function (key, val) {
                if (val.isChecked()) {
                    var temp = ko.toJS(val);
                    temp = new optionalLogosVM(temp.logoName, temp.logoFileName, temp.isChecked);
                    temp_optional_logos.push(temp);
                    val.isChecked(false);

                }
            });
            if (ko.toJS(temp_optional_logos).length > 0) {
                self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()].logoFile(self.selectedInHomeStoreLogoInfo().logoFile());
                self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()].optionalLogosSelected([]);
                self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()].optionalLogosSelected(temp_optional_logos);
            }
        }
        $('#dvUploadedFilesList ul').listview('refresh');
        $('#dvUploadedFilesList').collapsibleset('refresh');
    };

    //Loading the data based on the selected store in the current inhome.
    self.locationChanged = function (data, event) {
        //self.validateForDataExistency();
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        var selected_store = $(ele).val();
        var selected_inhome = self.currentInHome().dateValue;

        self.persistSelectedBaseInfo();

        self.filesUploaded([]);
        self.filesToAttach([]);
        self.fileToDropBox([]);
        self.selectedFiles = [];
        self.uploadingFiles = "";

        self.selectedStore(self.selectedLocation());
        self.getSelectedInfo();

        var curr_info = ko.toJS(self.logoSelectionInfo());
        var prev_info = ko.toJS(self.prevLogoSelectionInfo());
        var diff_between_json_objects = [];
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            if (key1.toLowerCase() != 'isreplicated') {
                $.each(prev_info, function (key2, val2) {
                    if (key2.toLowerCase() != 'isreplicated') {
                        $.each(val1, function (key3, val3) {
                            var curr_temp1 = val3;
                            var prev_temp2 = val2[key3];
                            diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                            if (diff_between_json_objects.length != 0) {
                                is_diff_found = true;
                                return false;
                            }
                        });
                    }
                    if (is_diff_found) {
                        return false;
                    }
                });
                if (is_diff_found) {
                    return false;
                }
            }
        });

        if (is_diff_found) {
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
        }
        else {
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
        }
    };

    //Load the base info for the selected location in the current inhome.
    self.getSelectedInfo = function () {
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var selected_inhome = self.currentInHome().dateValue;
        var is_found = false;
        if (self.logoSelectionInfo()[selected_inhome]() != undefined && self.logoSelectionInfo()[selected_inhome]() != null)
            self.selectedInhomeInfo(self.logoSelectionInfo()[selected_inhome]());

        if (self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()] != undefined && self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()] != null) {
            self.selectedInHomeStoreLogoInfo(self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()]);
            if (ko.toJS(self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()].optionalLogosSelected).length > 0) {
                var temp = ko.toJS(self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()].optionalLogosSelected);
                $.each(temp, function (key, val) {
                    var is_found = false;
                    $.each(self.optionalLogos(), function (key1, val1) {
                        if (val1.logoName() == val.logoName) {
                            val1.isChecked(true);
                            is_found = true;
                        }
                        if (is_found)
                            return false;
                    });
                });
            }
            self.uploadedFiles([]);
            if (Object.keys(ko.toJS(self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()]).logoFile).length > 0) {
                self.uploadedFiles.push(self.logoSelectionInfo()[selected_inhome]()[self.selectedStore()].logoFile);
            }
        }
        $('#dvLogoSetup').css('display', 'block');
        $('div').trigger('create');
        var temp = ko.toJS(self.logoSelectionInfo());
        self.updateInHomeNavBarUI(temp);
    };

    self.navBarItemClick = function (data, event) {
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var ele = event.target || event.targetElement;
        $('#dvNavBar div ul li a').removeClass('ui-btn-active');
        $(ele).addClass('ui-btn-active');
        self.persistSelectedBaseInfo();
        self.currentInHome(ko.dataFor(ele));
        self.getSelectedInfo();
    }

    self.replicateChanged = function (data, event) {
        self.persistSelectedBaseInfo();
        var ele = event.target || event.targetElement;
        $('#confirmMsg').html('Replicating Setup Entries will overwrite any previous information entered.');
        $('#okButConfirm').text('Continue');

        var curr_info = ko.toJS(self.logoSelectionInfo());
        var prev_info = ko.toJS(self.prevLogoSelectionInfo());
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            if (key1.toLowerCase() != 'isreplicated') {
                $.each(prev_info, function (key2, val2) {
                    if (key2.toLowerCase() != 'isreplicated') {
                        //if (key1 == key2) {
                        $.each(val1, function (key3, val3) {
                            var curr_temp1 = val3;
                            var prev_temp2 = val2[key3];
                            diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                            if (diff_between_json_objects.length != 0) {
                                is_diff_found = true;
                                return false;
                            }
                        });
                        //}
                        if (is_diff_found) {
                            return false;
                        }
                    }
                });
                if (is_diff_found) {
                    return false;
                }
            }
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            $('#okButConfirm').unbind('click');
            self.replicateData();
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        if ($(ele).is(':checked') && is_diff_found) {
            $('#popupConfirmDialog').popup('open');
        }
    };

    self.replicateData = function () {
        var selected_inhome = self.currentInHome().dateValue;
        var temp_info = {};
        if (self.logoSelectionInfo()[selected_inhome]() != undefined && self.logoSelectionInfo()[selected_inhome]() != null && self.logoSelectionInfo()[selected_inhome]() != "")
            temp_info = self.logoSelectionInfo()[selected_inhome]();
        
        $.each(self.inHomeDatesList(), function (key2, val2) {
            if (selected_inhome != val2.dateValue) {
                self.logoSelectionInfo()[val2.dateValue](temp_info);
            }
        });
        self.logoSelectionInfo().isReplicated(true);
        var temp = ko.toJS(self.logoSelectionInfo());
        if ((temp != undefined && temp != null && temp != "")) {
            var temp_inhomewise_list = {};
            var is_replicated = '';
            var index = 0;
            var temp_bases_list = {};
            $.each(temp, function (key, val) {
                if (is_replicated == '' && key == 'isReplicated')
                    is_replicated = val;
                else {
                    temp_inhomewise_list[key] = {};
                    $.each(val, function (key1, val1) {
                        var temp_base_info = [];
                        var optional_logos_list = []
                        $.each(val1.optionalLogosSelected, function (key2, val2) {
                            optional_logos_list.push(new optionalLogosVM(val2.logoName, val2.logoFileName, val2.isChecked));
                        });
                        var logo_file = {}
                        if (Object.keys(val1.logoFile).length > 0) {
                            logo_file = new logoFileVM(val1.logoFile.locationPk, val1.logoFile.fileName, val1.logoFile.fileSizePretty, val1.logoFile.fileStatus, val1.logoFile.filePk);
                        }
                        temp_inhomewise_list[key][key1] = new logoFileInfo(logo_file, optional_logos_list, val1.isEnabled);
                    });
                }
            });
            self.prevLogoSelectionInfo(new selectedLogosListInfo(temp_inhomewise_list, is_replicated));
        }
        self.updateInHomeNavBarUI(temp);
    };

    self.verifyData = function (temp_info) {
        var is_data_found = false;
        $.each(temp_info, function (key1, val1) {
            if (val1.optionalLogosSelected.length > 0 && Object.keys(val1.logoFile).length > 0)
                is_data_found = true;
            else
                is_data_found = false;
            if (!is_data_found) {
                return false;
            }
        });
        return is_data_found;
    };

    self.updateInHomeNavBarUI = function (temp) {
        $.each(temp, function (key, val) {
            if (key != "isReplicated") {
                var is_valid = self.verifyData(val);
                if (is_valid) {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
                }
                else {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
                }
            }
        });
    };

    self.showOrderedLocations = function () {
        $('#popUpLocations').popup('open');
    };

    self.manageLocationsPopup = function (data, event) {
        var ele = event.target || event.targetElement;
        if ($(ele).attr('data-btnType') == "submit") {
            self.previousSelectedStore(self.selectedStore());
        }
        else {
            self.selectedStore(self.previousSelectedStore());
            self.selectedLocation(self.previousSelectedStore())
        }
        $('#ddlLocations').selectmenu('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        var sel_location = $('#ddlLocations option:selected').text();
        header_text = header_text + ' (' + sel_location + ')';
        $('#navHeader').text(header_text);
        $('#popUpLocations').popup('close');
    };
    self.previewMailing = function () {
        getNextPageInOrder();
        var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    }

};
