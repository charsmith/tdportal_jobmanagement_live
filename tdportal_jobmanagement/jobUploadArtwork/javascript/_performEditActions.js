﻿//******************** Public Functions Start **************************
var performEditActions = function (self) {
    //Navigates to proofing page to open the uploaded artwork file in Crocodoc viewer to allow the users to write their comments.
    self.openProofingAndMarkup = function (file_name) {
        getNextPageInOrder();
        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
            window.location.href = "../jobApproval/jobApproval.html?fileName=" + file_name + "&jobNumber=" + self.jobNumber;
        }
        else {
            window.location.href = "../jobProofing/jobProofing.html?fileName=" + file_name + "&jobNumber=" + self.jobNumber;
        }
        
    }

    //Displays the Artwork preview popup.
    self.showPreviewPopup = function (file_name) {
        $('#btnPreview').attr('onclick', 'pageObj.displayArtwork("' + file_name + '")');
        $('#dvPreviewArtwork').popup('open');
    }

    //Displays Wait image while opening preview.
    self.displayOverlay = function () {
        $('#waitPopUp').popup('open', { positionTo: 'window' });
        $('#overlay').css({ top: '40%', left: '50%', margin: '-' + ($('#overlay').height() / 2) + 'px 0 0 -' + ($('#overlay').width() / 2) + 'px' });
    }

    //Drives to open the selected artwork to be opened in crocodoc viewer or in preview window based on the link user clicks.
    self.displayArtwork = function (event, file_name) {
        var inner_text = (event.target.innerText || event.target.innerHTML);
        if (event.target.tagName.toLowerCase() == "span" && (inner_text.toLowerCase() == "client proofing &amp; markup" || inner_text.toLowerCase() == "client proofing & markup")) {
            self.openProofingAndMarkup(file_name)
        }
        else {
            $('#divError').text('');
            self.displayOverlay();
            $('#dvPreviewArtwork').popup('close');
            $('#waitPopUp').popup('open', { positionTo: 'window' });
            var file_type = file_name.substring(file_name.lastIndexOf('.') + 1).toLowerCase();
            var content_type = null;
            switch (file_type) {
                case "pdf":
                    content_type = 'application/pdf';
                    break;
                case "jpg":
                case "jpeg":
                case "png":
                case "gif":
                case "tif":
                case "tiff":
                    content_type = 'image/' + file_type;
                    break;
                case "svg":
                    content_type = 'image/svg+xml';
                    break;
                case "html":
                case "htm":
                    content_type = "text/html";
                    break;
            }
            var preview_artwork_url = serviceURLDomain + "api/FileUpload_get/" + self.facilityId + "/" + self.jobNumber + "/" + escape(file_name) + "/" + escape(content_type.replace(/\//g, '|'));
            var htmlText = null;
            if (content_type == 'application/pdf') {
                htmlText = "<embed width=850 height=500 type='" + content_type + "' src='" + preview_artwork_url + "' id='selectedFile'></embed>";
            } else {
                htmlText = "<img src='" + preview_artwork_url + "' id='selectedFile'></img>";
            }

            $('#popupPDFContent').html(htmlText);

            window.setTimeout(function getDelayNDisplayFile() {
                if ($('#selectedFile')[0].clientWidth > 0 && $('#selectedFile')[0].clientHeight > 0)
                    var display = $("#popPDF");
                if (display != undefined) {
                    display.scroll(function () {

                        var ctrls = $("#popPDF img");
                        console.log("************************");
                        ctrls.each(function (index) {
                            console.log($(this).position().top);

                            if ($(this).position().top < 0)
                                $(this).css("visibility", "hidden")//.hide();
                            else {
                                $(this).css("visibility", "")//.show();
                            }
                        });
                    });
                }

                window.setTimeout(function getDelay() {
                    $('#waitPopUp').popup('close');
                    $('#popPDF').popup('open', { positionTo: 'window' });
                }, 200);
            }, 5000);
        }
    };

    //Updates the selected information of the selected file in edit mode.
    self.updateEditFileInfo = function (ctrl, file_name, source_type_code, update_type, e) {
        //if ($('#dvUploadedListFiles').html().toString().indexOf('There are no files available') > -1 && $('#ddlFileActions').val() == "select" || ((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == "205" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER) && $('#txtVersionCode').val() == "")) {
        if ($('#hidMode').val() != "" && $('#hidMode').val() != "delete") {
            if ((isMultiple || isDragDrop || ('draggable' in document.createElement('span')) && $('#hidMode').val() != "delete") && !self.uploadMultiplesValidation()) return false;
        }
        //if (update_type != "" || $('#hidMode').val() == "") {
        //    if (((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == "205" || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER) && ($('#txtVersionCode').val() == "" && $('#ddlFileActions').val() == "select")) ||
        //    ((jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != "205" && jobCustomerNumber != BRIGHTHOUSE_CUSTOMER_NUMBER && jobCustomerNumber != BBB_CUSTOMER_NUMBER && jobCustomerNumber != DATAWORKS_CUSTOMER_NUMBER) && $('#ddlFileActions').val() == "select")) {
        //        $('#popupListDefinition').popup('close');
        //        $('#alertmsg').text('Please select an action to update.');
        //        $('#popupDialog').popup('open');
        //        return false;
        //    }
        //}

        if ($('#ddlFileActions').val() == "delete")
            update_type = "delete";
        else if ($('#ddlFileActions').val() == "replace")
            update_type = "replace";

        if (update_type == "delete") {
            return self.removeArtworkFile(file_name, $('#hidEditUploadLocation').val());
        }
        else if (update_type == "replace") {
            self.replaceArtworkFile(e);
            $('#popupListDefinition').popup('close');
        }
        else if (update_type == "") {
            //if (!self.uploadArtValidation()) return false;

            if ((!isMultiple && !isDragDrop) && !self.uploadArtValidation()) return false;
            if ((isMultiple || isDragDrop) && !self.uploadMultiplesValidation()) return false;

            var file_name = "";

            if (!isMultiple && !isDragDrop) {
                file_name = $('#name').val();
            }
            else {
                if (temp_data.length > 0) {
                    file_name = temp_data[0].name;
                }
            }

            var file_type = file_name.split('.')[1];
            var linked_to = $('#ddlFilesUploaded').val();
            linked_to = (linked_to != undefined && linked_to != null && linked_to != "" && linked_to != "select") ? linked_to : "";
            var checked_ctrls = $('fieldset[data-role=controlgroup]').find('input[type=checkbox][checked=checked]');
            var selected_location = '';
            if (checked_ctrls.length > 0)
                $.each(checked_ctrls, function (a, b) {
                    selected_location += (selected_location != '') ? ',' + b.value : b.value;
                });
            var old_file_name = $('#hidEditUploadFile').val();
            var old_file_type = old_file_name.substring(old_file_name.lastIndexOf('.') + 1);
            var old_file_location = $('#hidEditUploadLocation').val();
            var file_info = jQuery.grep(self.artFilesToUpload, function (obj) {
                return (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER) ? obj.fileName === old_file_name && obj.fileName.substring(obj.fileName.lastIndexOf('.') + 1) === old_file_type && obj.locationId.toLowerCase() == old_file_location.toLowerCase() : obj.fileName === old_file_name && obj.fileName.substring(obj.fileName.lastIndexOf('.') + 1) === old_file_type;
            });
            if (file_info.length > 0) {
                file_info[0].fileName = file_name;
                file_info[0].locationId = selected_location;
                file_info[0].versionCode = $('#txtVersionCode').val();
                file_info[0].linkedTo = linked_to;
                $('#popupListDefinition').popup('close');
                if (!isMultiple && !isDragDrop)
                    self.resetFileUploadPopup();
            }
            if (isMultiple || isDragDrop) {
                var data_file_info = jQuery.grep(data, function (obj) {
                    return (obj.name.substring(obj.name.lastIndexOf("\\") + 1) === old_file_name);
                });

                if (data_file_info.length > 0 && temp_data.length > 0) {
                    data_file_info[0].type = file_type;
                    data_file_info[0].file = temp_data[0].file;
                }
            }

            var uploading_files = self.uploadingFiles.split('|');
            var temp_uploading_files = "";
            $.each(uploading_files, function (key, val) {
                var file_parts = val.split('^');
                if (file_parts[0].toLowerCase() == old_file_name.toLowerCase()) {
                    file_parts[0] = file_name;
                    val = file_parts[0] + '^' + file_parts[1];
                    temp_uploading_files = (temp_uploading_files != "") ? temp_uploading_files + '|' + val : val;
                    return false;
                }
                else {
                    temp_uploading_files = (temp_uploading_files != "") ? temp_uploading_files + '|' + val : val;
                }
            });
            self.uploadingFiles = temp_uploading_files;

            $('#hidEditUploadFile').val('');
            self.makeFileListBeforePost(self.artFilesToUpload);
            sessionStorage.artFilesToUpload = JSON.stringify(self.artFilesToUpload);
            $('#hidEditUploadLocation').val('');
            temp_data = [];
        }
        else {
            self.updatePostUploadFile('art', file_name, '', source_type_code);
            if (update_type == "editlink")
                postJobSetUpData('save');
        }
    };

    //Performs the selected action on the uploaded file - replace, delete etc.
    self.performFileAction = function (edit_action) {
        //if ($('#ddlFileActions').val() == "replace") {
        if (edit_action == "reassign") {
            $('#btnUpdate').text('Update');
            $('#dvBrowse').hide();
            $('#dvDragDrop').hide();
            $('#dvFileName').hide();
            if ('draggable' in document.createElement('span'))
                $('#dvText').hide();
            else {
                $('#dvText').hide();
            }
            $('#hidMode').val('reassign');
            $('#drop-area').empty();
        } else if (edit_action == "replace") {
            $('#btnUpdate').text('Update');
            $('#dvBrowse').show();
            $('#dvDragDrop').show();
            $('#dvFileName').hide();
            if ('draggable' in document.createElement('span'))
                $('#dvText').hide();
            else {
                $('#dvText').show();
                self.addFileInput();
            }
            $('#hidMode').val('replace');
            $('#drop-area').empty();
        }
            //else if ($('#ddlFileActions').val() == "delete") {
        else if (edit_action == "delete") {
            $('#hidMode').val('delete');
            var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
            var file_ctrls = $(upload_form).find('input[type=file]');
            if (file_ctrls.length > 0 && !('draggable' in document.createElement('span'))) {
                var newFileInput = file_ctrls[0].cloneNode(true);
                newFileInput.value = null;
                newFileInput.style.display = "";
                newFileInput.id = "mailFiles[]";
                newFileInput.name = "mailFiles[]";
                $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).find('input[type=file]').remove();
                $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).append(newFileInput);
                (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                    self.navigate(e, $(e.target))
                });
                self.fileCount = 0;
            }
            $('#dvDragDrop').hide();
            $('#dvFileName').hide();
            $('#dvApprovalType').hide();
            $('#dvBrowse').hide();
            $('#dvText').hide();
            $('#btnUpdate').text('Delete');
        }
        else if ($('#ddlFileActions').val() == "select") {
            $('#btnUpdate').text('Update');
        }
    }

    //Starts the replace artwork action when user selected replace option in edit mode.
    self.replaceArtworkFile = function (e) {
        var upload_type = $('#ddlFileActions').val();
        //if (!self.uploadArtValidation()) return false;
        if ((!isMultiple && !isDragDrop) && !self.uploadArtValidation()) return false;
        if ((isMultiple || isDragDrop || ('draggable' in document.createElement('span'))) && !self.uploadMultiplesValidation()) return false;

        var file_info = "";
        var file_name = ""
        var file_type = "";
        var linked_to = $('#ddlFilesUploaded').val();
        linked_to = (linked_to != undefined && linked_to != null && linked_to != "" && linked_to != "select") ? linked_to : "";
        if (!isMultiple && !isDragDrop) {
            file_info = $('#hidUploadFile').val();
            file_name = file_info.split('|')[0];
            file_type = file_info.split('|')[1];
        }
        else {
            //file_name = temp_data[0].name;
            //file_name = $('#hidEditUploadFile').val();
            file_name = self.artFilesToUpload[0].fileName;
            file_type = file_name.split('.')[1];
        }
        var check_file_name = (file_name.indexOf('\\') > -1) ? file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.length) : file_name;
        self.replaceClickEvent = e;
        if (!self.verifyFileExistence(check_file_name)) {
            self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
            $('#popupListDefinition').popup('close');
            $('#confirmMsg').text('A file by the same name has already been uploaded. Uploading this file will replace the previous version for production and become the current version in the Proofing History. Click Ok to continue or Cancel to Remove the Selected File if you do not want to replace the current file.');
            $('#okButConfirm').attr('onclick', '$("#popupConfirmDialog").popup("open");$("#popupConfirmDialog").popup("close");pageObj.uploadReplacingFile(\'' + file_name + '\',\'' + check_file_name + '\',\'' + linked_to + '\');');
            $('#cancelButConfirm').attr('onclick', '$("#popupConfirmDialog").popup("open");$("#popupConfirmDialog").popup("close");pageObj.popUpListDefinitionHide();pageObj.resetFileUploadPopup();pageObj.resetReplaceFileCtrl();');
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        else {
            self.uploadReplacingFile(check_file_name, check_file_name, linked_to)
        }
    }

    //Uploades the selected when the user selects the replace artwork in edit mode.
    self.uploadReplacingFile = function (file_name, check_file_name, linked_to) {
        var uploading_file_name = "";
        if (file_name != "") {
            uploading_file_name = check_file_name;
            self.uploadingFiles = "";
            self.uploadingFiles += (self.uploadingFiles != "") ? "|" + uploading_file_name + "^" + getFileTypeCode('artwork') : uploading_file_name + "^" + getFileTypeCode('artwork');

            // pull the file info from gOutputData and udpate with new file and update the same in session.
            var file_info;
            file_info = (self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};
            var file_info = (Object.keys(file_info).length > 0 && file_info[$('#hidEditUploadFile').val()] != undefined && file_info[$('#hidEditUploadFile').val()] != null && Object.keys(file_info[$('#hidEditUploadFile').val()]).length > 0) ? file_info[$('#hidEditUploadFile').val()] : {};
            //var temp_file_info = $.extend(true, [], file_info);
            var temp_file_info = $.extend(true, {}, file_info);
            //file_name = (file_name.indexOf('.jpg') || file_name.indexOf('.png')) ? file_name.replace('.png', '.pdf').replace('.jpg', '.pdf') : file_name;
            if (Object.keys(file_info).length > 0) {
                var replace_string = 'replace|' + file_info.fileName + '|' + file_info.filePk;
                $('#hidEditFileOperation').val(replace_string);
                //file_info[0].fileName = check_file_name;
                file_info.linkedTo = linked_to;
                file_info.fileStatus = "archive";

                temp_file_info.fileName = file_name; //  check_file_name;
                temp_file_info.linkedTo = linked_to;
                temp_file_info.versionCode = ($('#txtVersionCode').val() != "") ? $('#txtVersionCode').val() : temp_file_info.versionCode;
            }

            var linked_files_info = (self.gOutputData.standardizeAction.standardizeFileList != undefined) ? self.gOutputData.standardizeAction.standardizeFileList : [];
            if (linked_files_info.length > 0) {
                $.each(linked_files_info, function (key, val) {
                    if (val.linkedTo != undefined && val.linkedTo != null && val.linkedTo.toLowerCase() == $('#hidEditUploadFile').val().toLowerCase()) {
                        val.linkedTo = file_name;
                    }
                });
            }

            if (self.gOutputData.artworkAction.artworkFileList != undefined)
                self.gOutputData.artworkAction.artworkFileList[check_file_name] = temp_file_info;

            sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
            //Call uploadFiles(e) function to upload.
            $("#popupDialog").on({
                popupafteropen: function (event, ui) {
                    if (!isMultiple && !isDragDrop)
                        self.resetFileUploadPopup();
                    self.resetReplaceFileCtrl();
                    $("#popupDialog").off("popupafteropen");
                }
            });
            temp_data = [];
            $('#hidMode').val('');
            self.uploadFiles(self.replaceClickEvent);
        }
        else {
            $("#popupListDefinition").popup("close");
        }
    }

    //Resets the Upload control after the replace action is completed.
    self.resetReplaceFileCtrl = function () {
        //reset the fields...
        self.resetFields();
        if ('draggable' in document.createElement('span')) {
            var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
            var file_ctrls = $(upload_form).find('input[type=file]');
            if (file_ctrls.length > 0) {
                var newFileInput = file_ctrls[0].cloneNode(true);
                newFileInput.value = null;
                newFileInput.style.display = "";
                newFileInput.id = "mailFiles[]";
                newFileInput.name = "mailFiles[]";
                $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).find('input[type=file]').remove();
                $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).append(newFileInput);
                (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                    self.navigate(e, $(e.target))
                });
                self.fileCount = 0;
            }
        }
    }
    //Removes the artwork from the job when user selects "Delete" option in edit mode.
    self.removeArtworkFile = function (file_name, location_id) {
        $('#divError').text('');
        var file_delete_url = serviceURLDomain + "api/FileUpload/delete/" + self.facilityId + "/" + self.jobNumber + "/";
        var file_type = file_name.substring(file_name.lastIndexOf('.') + 1);
        file_delete_url = file_delete_url + "25/" + file_name;

        getCORS(file_delete_url, null, function (prod_data) {
            var file_info;
            file_info = (self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};

            var remove_files = [];
            $.each(file_info, function (key1, val1) {
                if ((jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER) && location_id != "null" && location_id != "") {
                    if (val1.fileName.substring(val1.fileName.lastIndexOf('.') + 1).toLowerCase() === file_type.toLowerCase() && val1.fileName.toLowerCase() === file_name.toLowerCase() && val1.locationId.toLowerCase() == location_id.toLowerCase()) {
                        remove_files.push(key1);
                    }
                } else {
                    if (val1.fileName.substring(val1.fileName.lastIndexOf('.') + 1).toLowerCase() === file_type.toLowerCase() && val1.fileName.toLowerCase() === file_name.toLowerCase()) {
                        remove_files.push(key1);
                    }
                }
            });

            var linked_files_info = (self.gOutputData.standardizeAction.standardizeFileList != undefined) ? self.gOutputData.standardizeAction.standardizeFileList : [];
            if (linked_files_info.length > 0) {
                $.each(linked_files_info, function (key, val) {
                    if (val.linkedTo != undefined && val.linkedTo != null && val.linkedTo.toLowerCase() == file_name) {
                        val.linkedTo = "";
                    }
                });
            }

            if (remove_files.length > 0) {
                $.each(remove_files, function (key2, val2) {
                    self.resetLinkToUploadedFiles('art', file_info[val2].linkedTo)
                    delete file_info[val2];
                    if (Object.keys(file_info).length == 0) {
                        file_info = {};
                        $('#tblFiles').hide();
                        $("#dvUploadList").hide();
                    }
                });
                $('#popupListDefinition').popup('open');
                $('#popupListDefinition').popup('close');
            }
            sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
            postJobSetUpData('save');
            self.buildOutputLists(true);
            if (self.gOutputData != null) {
                getBubbleCounts(self.gOutputData);
            }
            $('#hidEditUploadLocation').val('');
        },
            function (error_response) {
                $('#popupListDefinition').popup('open');
                $('#popupListDefinition').popup('close');
                showErrorResponseText(error_response, true);
            });
    }

    //Displays the Popup window settings when the user clicks on edit button from the list of uploaded files.
    self.displayPostUploadEditDialog = function (file_name, location_id, version_code, linked_to, update_type) {
        file_info = (self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};
        var file_info = (Object.keys(file_info).length > 0 && file_info[file_name] != undefined && file_info[file_name] != null && Object.keys(file_info[file_name]).length > 0) ? file_info[file_name] : {};

        if (Object.keys(file_info).length > 0 && file_info.overallStatus == 1 && appPrivileges.roleName == "user") {
            $('#alertmsg').html("The artwork has been approved. To edit/replace or remove artwork, please contact a site administrator if you have questions.");
            $('#popupListDefinition').popup('close');
            $("#popupDialog").popup("open");
            return false;
        }
        else {
            //openEditFileDialog(file_name, location_id, version_code, linked_to, update_type);


            //self.openEditFileDialog(file_name, location_id, version_code, linked_to, update_type);
            if (jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER && jobCustomerNumber != CASEYS_CUSTOMER_NUMBER)
                $('#popupEditArtowrk a[data-ActionType="editlocationassignment"]').parent().css('display', 'none');
            //if (jobCustomerNumber != CW_CUSTOMER_NUMBER && jobCustomerNumber != OH_CUSTOMER_NUMBER)
                $('#popupEditArtowrk a[data-ActionType="editlink"]').parent().css('display', 'none');
            $('#popupEditArtowrk a').bind('click', function (event) {
                var msg = "";
                $('#popupEditArtowrk').popup('close');
                if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
                    $('#confirmMsg').text('This assignment will affect previous artwork assignments. Please review all artwork assignments.');
                    //$('#okButConfirm').attr('onclick', '$("#popupConfirmDialog").popup("open");$("#popupConfirmDialog").popup("close");pageObj.uploadReplacingFile(\'' + file_name + '\',\'' + check_file_name + '\',\'' + linked_to + '\',true);');
                    $('#cancelButConfirm').attr('onclick', '$("#popupConfirmDialog").popup("open");$("#popupConfirmDialog").popup("close");pageObj.resetFileUploadPopup();pageObj.resetReplaceFileCtrl();');
                    //$('#popupConfirmDialog').popup('open');
                    $('#okButConfirm').unbind('click');
                    $('#okButConfirm').removeAttr('onclick');
                }
                var src = event.target || event.targetElement;
                var type = $(src).attr('data-ActionType');
                switch (type) {
                    case "editlink":
                        self.openEditFileDialog(file_name, location_id, version_code, linked_to, "editlink");
                        break;
                    case "editlocationassignment":
                        //Display confirmation msg and on user confirmation, update the assignments.
                        $('#okButConfirm').unbind('click');
                        $('#okButConfirm').bind('click', function () {
                            $('#popupConfirmDialog').popup('close');
                            $('#okButConfirm').unbind('click');
                            $('#okButConfirm').text('Ok');
                            window.setTimeout(function () {
                                $('#okButConfirm').unbind('click');
                                self.openEditFileDialog(file_name, location_id, version_code, linked_to, "reassign");
                                self.performFileAction('reassign');
                            }, 1000);
                        });

                        $('#okButConfirm').text('Continue');
                        $('#popupConfirmDialog').popup('open');
                        break;
                    case "replaceartwork":
                        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
                            $('#okButConfirm').text('Continue');
                            $('#popupConfirmDialog').popup('open');
                            $('#okButConfirm').unbind('click');
                            $('#okButConfirm').bind('click', function () {
                                $('#popupConfirmDialog').popup('close');
                                $('#okButConfirm').unbind('click');
                                $('#okButConfirm').text('Ok');
                                window.setTimeout(function () {
                                    $('#okButConfirm').unbind('click');
                                    self.openEditFileDialog(file_name, location_id, version_code, linked_to, "replace");
                                    self.performFileAction('replace');
                                }, 1000);
                            });
                        }
                        else {
                            self.openEditFileDialog(file_name, location_id, version_code, linked_to, "replace");
                            self.performFileAction('replace');
                        }

                        break;
                    case "deleteartwork":
                        self.performFileAction('delete');
                        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) {
                            $('#okButConfirm').unbind('click');
                            $('#okButConfirm').bind('click', function () {
                                $("#popupConfirmDialog").popup("close");
                                $('#okButConfirm').unbind('click');
                                $('#okButConfirm').text('Ok');
                                self.removeArtworkFile(file_name, $('#hidEditUploadLocation').val());
                            });
                            $('#okButConfirm').text('Continue');
                            $('#popupConfirmDialog').popup('open');
                        } else {
                            return self.removeArtworkFile(file_name, $('#hidEditUploadLocation').val());
                        }
                        break;
                };
                $('#popupEditArtowrk a').unbind('click');
            });

            $('#popupEditArtowrk').popup('open');
        }
    }

    //Reassigns the locations to selected artwork.
    self.reAssignLocations = function (file_name, location_id, version_code, linked_to, update_type) {
        var uploading_file_name = "";
        if (file_name != "") {
            // pull the file info from gOutputData and udpate with new file and update the same in session.
            var file_info;
            file_info = (self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};
            var file_info = (Object.keys(file_info).length > 0 && file_info[$('#hidEditUploadFile').val()] != undefined && file_info[$('#hidEditUploadFile').val()] != null && Object.keys(file_info[$('#hidEditUploadFile').val()]).length > 0) ? file_info[$('#hidEditUploadFile').val()] : {};

            var checked_ctrls = $('fieldset[data-role=controlgroup]').find('input[type=checkbox]:not([value=all]):checked');
            var selected_location = '';
            if (checked_ctrls.length > 0)
                $.each(checked_ctrls, function (a, b) {
                    selected_location += (selected_location != '') ? ',' + b.value : b.value;
                });

            //var temp_file_info = $.extend(true, [], file_info);
            //file_name = (file_name.indexOf('.jpg') || file_name.indexOf('.png')) ? file_name.replace('.png', '.pdf').replace('.jpg', '.pdf') : file_name;
            if (Object.keys(file_info).length > 0) {
                file_info.locationId = selected_location;  //Get all the locations as comma separated string.

            }

            sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
            self.reloadJobTicket();
            //Call uploadFiles(e) function to upload.
            $("#popupDialog").on({
                popupafteropen: function (event, ui) {
                    if (!isMultiple && !isDragDrop)
                        self.resetFileUploadPopup();
                    self.resetReplaceFileCtrl();
                    $("#popupDialog").off("popupafteropen");
                }
            });
        }
    };

    ////Validate the selected file with replace option
    //self.validateAssigningFile = function (file_name, location_id, version_code, linked_to, update_type) {
    //    var loc_list = [];
    //    if (location_id != "" && location_id.indexOf(',') > 0) {
    //        loc_list = location_id.split(',');
    //    } else if (location_id != "") {
    //        loc_list = new Array(location_id);
    //    }
    //    if (loc_list.length == $('#dvSelectedLocations input[type=checkbox]:not([value="all"])').length) {

    //    }
    //};

    //Sets the appropriate information of the selected file in the edit mode.
    self.openEditFileDialog = function (file_name, location_id, version_code, linked_to, update_type) {
        $('#dvText').hide();
        $('#dvBrowse').hide();
        $('#dvDragDrop').hide();
        $('#testButtonOk').hide();
        if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER)
            $('#dvUploadedListFiles').show();
        $('#dvPopupListDefinitionContent').show();
        $('#btnUpdate').text('Update');
        $('#btnUpdate').show();
        //$('#btnDelete').show();
        //$('#ddlFileActions').val('select').selectmenu('refresh');
        $('#dvFileActions').hide();
        if (appPrivileges.roleName == "user" || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER) {
            $('#dvVersionCode').hide();
        }
        else {
            $('#txtVersionCode').val((version_code != null && version_code != "null" && version_code != undefined) ? version_code : "");
        }
        if (jobCustomerNumber == OH_CUSTOMER_NUMBER) {
            $('#dvVersionCode').hide();
        }
        $('#hidEditUploadFile').val(file_name);
        $('#hidEditUploadLocation').val(location_id);
        if (update_type == "editlink") {
            $('#btnUpdate').attr('onclick', 'pageObj.updateEditFileInfo(this,\'' + file_name + '\',\'25\',\'' + update_type + '\',event)');
            $('#popupListDefinition').popup('close');
        }
        else if (update_type == "reassign") {
            $('#btnUpdate').unbind("click");
            $('#btnUpdate').bind('click', function () {
                var msg = "";
                if ($('#dvSelectedLocations input[type=checkbox]:checked').length == 0) {
                    msg += "<ul><li>Please select at least one location.</li></ul>";
                    if (msg != "") {
                        $("#popupListDefinition").popup("close");
                        $('#validationAlertmsg').html(msg);
                        window.setTimeout(function getDelay() {
                            $('#validationAlertmsg').addClass('wordwrap');
                            $('#validationPopupDialog').popup('open');
                        }, 200);
                        return false;
                    }
                }
                else {
                    var selected_stores = $('#dvSelectedLocations input[type=checkbox]:not([value="all"]):checked');
                    var selected_stores_info = [];
                    if (selected_stores.length > 0) {
                        var is_assigned = false;
                        $.each(selected_stores, function (key, val) {
                            is_assigned = self.checkAssignedLocationToRessign($(val).val());
                            if (is_assigned) {
                                selected_stores_info.push($('label[for="chk' + $(val).val() + '"]').text());
                            }
                        });
                    }
                    if (selected_stores_info.length > 0) {
                        var msg = "Artwork has already been assigned to these locations: <br /><b> " + selected_stores_info.join('<br />') + "</b>.  <br />Please select another locations.";
                        if (msg != "") {
                            $("#popupListDefinition").popup("close");
                            $('#validationAlertmsg').html(msg);
                            window.setTimeout(function getDelay() {
                                $('#validationAlertmsg').addClass('wordwrap');
                                $('#validationPopupDialog').popup('open');
                            }, 200);
                            return false;
                        }
                    } else {
                        //reAssignLocations
                        $('#btnUpdate').unbind("click");
                        self.reAssignLocations(file_name, location_id, version_code, linked_to, update_type);
                        $('#popupListDefinition').popup('close');
                        //self.reloadJobTicket();
                    }
                }
            });
            if ($('#dvSelectedLocations .ui-checkbox').length == 0)
                $('#dvSelectedLocations').trigger('create');
            $('#dvSelectedLocations input[type=checkbox]').attr('checked', false).checkboxradio('refresh');

            if (location_id != "" && location_id.indexOf('_') > -1) {
                var selected_locations = location_id.split('_');
                $.each(selected_locations, function (key, val) {
                    $('#dvSelectedLocations input[type=checkbox][value=' + val + ']').attr('checked', true);
                });
                if (selected_locations.length == $('#dvSelectedLocations input[type=checkbox]:not([value="all"])').length)
                    $('#dvSelectedLocations input[type=checkbox][value="all"]').attr('checked', true);
            }
            else if (location_id != "") {
                $('#dvSelectedLocations input[type=checkbox][value=' + location_id + ']').attr('checked', true);
                $('#dvSelectedLocations input[type=checkbox]:not([value=' + location_id + '])').attr('checked', false);
            }
            window.setTimeout(function () {
                if ($('#dvSelectedLocations .ui-checkbox') == 0)
                    $('#dvSelectedLocations').trigger('create');
                $('#dvSelectedLocations input[type=checkbox]').checkboxradio('refresh');
            }, 1000);
            $('#popupListDefinition h3').text('Edit Location Assignments');
            $('#dvSelectedLocations').parent().css('padding', '0px 1px 1px 1px');
            $('#dvLocations').show();
            $('#dvLocations').css('display', 'block');
        }
        else {
            $('#dvLocations').hide();
            $('#dvLocations').css('display', 'none');
            $('#btnUpdate').attr('onclick', 'pageObj.updateEditFileInfo(this,\'' + file_name + '\',\'25\',\'' + update_type + '\',event)');
            $('#popupListDefinition h3').text('Select Art Files');
        }
        if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == DATAWORKS_CUSTOMER_NUMBER || jobCustomerNumber == OH_CUSTOMER_NUMBER) {
            if (jobCustomerNumber != OH_CUSTOMER_NUMBER) {
                if ($('#dvUploadedListFiles select option:not([value=select])').length == 0)
                    $('#dvUploadedListFiles').html('There are no files available  <br />to link to.');
                else
                    if ($('#dvUploadedListFiles select option[value="' + linked_to + '"]').length > 0)
                        $('#ddlFilesUploaded').val(linked_to).selectmenu('refresh');
                    else
                        $('#ddlFilesUploaded').val('select').selectmenu('refresh');
            }
            if (update_type == "editlink")
                $('#popupListDefinition h3').text('Edit Link');
        }
        $('#dvFileName').hide();
        $('#dvApprovalType').hide();
        $('#popupListDefinition').popup('open', { positionTo: '#divpopupposition' });
    }
    //******************** Public Functions End **************************
};