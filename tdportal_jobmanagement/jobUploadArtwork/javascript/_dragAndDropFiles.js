﻿var dragAndDropFiles = function (self) {
    var illegalFilesCnt = 0;
    var invalidFileTypesCnt = 0;
    var fileExistenceCnt = 0;

    self.ignoreDrag = function (e) {
        e.preventDefault();
    };
    self.makeDrop = function (e, is_multiple, ctrl) {
        var fileList = (!is_multiple) ? (e.originalEvent.dataTransfer.files) : $(ctrl)[0].files;
        e.preventDefault();
        fileExistenceCnt = 0;
        invalidFileTypesCnt = 0;
        illegalFilesCnt = 0;
        if (!is_multiple) {
            isDragDrop = true;
        }
        var mode = $('#hidMode').val();
        if (mode == "edit" || mode == "replace") {
            if (temp_data.length > 0) temp_edit_file = temp_data;
            temp_data = [];
            if ((fileList.length + self.artFilesToUpload.length) > 1) {
                $("#popupListDefinition").popup("close");
                $('#validationAlertmsg').html("Please select/drag and drop single file to replace the existing file.");
                window.setTimeout(function getDelay() {
                    $('#validationPopupDialog').popup('open');
                    self.resetFileInput();
                    return false;
                }, 200);
                return false;
            }
        }
        else if ((fileList.length + self.artFilesToUpload.length) > 1 && ($('#dvLocations').css('display') == "block" || $('#dvLocations').css('display') == "")) {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html("Please select single file to assign and upload.");
            window.setTimeout(function getDelay() {
                $('#validationPopupDialog').popup('open');
                self.resetFileInput();
                return false;
            }, 200);
            return false;
        }
        else if ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) && ($('#dvSelectedLocations input[type=checkbox]:not([value=all]):checked').length == 0 && $('#hidMode').val() != "replace")) {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html("Please select at least one location.");
            window.setTimeout(function getDelay() {
                self.resetFileInput();
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);
            return false;
        }
        else {
            //var total_files_count = self.artFilesToUpload.length + temp_data.length + fileList.length;
            var total_files_count = self.artFilesToUpload.length + fileList.length;
            var total_files_uploaded_count = Object.keys(self.gOutputData.artworkAction.artworkFileList).length;
            var total_count = total_files_count + ((Object.keys(self.gOutputData.artworkAction.artworkFileList).length >= 1) ? Object.keys(self.gOutputData.artworkAction.artworkFileList).length : 0);
            if ((total_files_count > self.maxNumberOfFiles) || (total_files_uploaded_count > self.maxNumberOfFiles) || (total_count > self.maxNumberOfFiles)) {
                var msg = "The maximum number of artwork files to be uploaded per order has been reached. Please remove a previously uploaded file in order to upload a new file to this order. If this order requires more than " + self.maxNumberOfFiles + " artwork files, please contact your Account Manager.";
                $('#validationAlertmsg').html(msg);
                $("#popupListDefinition").popup("close");
                window.setTimeout(function getDelay() {
                    $('#validationPopupDialog').popup('open');
                    return false;
                }, 200);
                return false;
            }
            if (Object.keys(self.gOutputData.artworkAction.artworkFileList).length > 0 && fileList.length > 0) {
                var selected_stores = $('#dvSelectedLocations input[type=checkbox]:not([value="all"]):checked');
                var selected_stores_info = [];
                if (selected_stores.length > 0) {
                    var is_assigned = false;
                    $.each(selected_stores, function (key, val) {
                        is_assigned = self.checkAssignedLocations($(val).val(), "Art");
                        if (is_assigned) {
                            selected_stores_info.push($('label[for="chk' + $(val).val() + '"]').text());
                        }
                    });
                }
                if (selected_stores_info.length > 0) {
                    var msg = "Artwork has already been assigned to these locations: <br /><b> " + selected_stores_info.join('<br />') + "</b>.  <br />Please select another locations.";
                    $('#validationAlertmsg').html(msg);
                    $("#popupListDefinition").popup("close");
                    window.setTimeout(function getDelay() {
                        self.resetFileInput();
                        $('#validationPopupDialog').popup('open');
                        return false;
                    }, 200);
                    return false;
                }
            }
        }
        self.generateDropFileList(fileList, is_multiple, ctrl);
    }

    self.generateDropFileList = function (fileList, is_multiple, ctrl) {
        if (fileList && fileList.length > 0) {
            illegalFilesCnt = 0;
            invalidFileTypesCnt = 0;
            for (var i = 0; i < fileList.length; i++) {
                if (self.validateFiles(fileList[i].name)) {
                    var is_file_found = $.grep(temp_data, function (obj) {
                        return (obj.name.toLowerCase() === fileList[i].name.toLowerCase())
                    });
                    var is_file_found_in_selected = $.grep(self.artFilesToUpload, function (obj) {
                        return (($('#hidMode').val() == "edit" && temp_edit_file.length > 0) ? (obj.fileName.toLowerCase() === temp_edit_file[0].name.toLowerCase()) : (obj.fileName.toLowerCase() === fileList[i].name.toLowerCase()));
                    });
                    if (is_file_found.length == 0) {
                        if (is_file_found_in_selected.length > 0) {
                            var file_found = $.grep(data, function (obj) {
                                return (($('#hidMode').val() == "edit" && temp_edit_file.length > 0) ? (obj.name.toLowerCase() === temp_edit_file[0].name.toLowerCase()) : (obj.name.toLowerCase() === fileList[i].name.toLowerCase()));
                            });
                            if (file_found.length > 0) {
                                file_found[0].file = fileList[i];
                            }
                            else {
                                data.push({
                                    "name": fileList[i].name,
                                    "type": fileList[i].name.split('.')[1],
                                    "renamedFileName": fileList[i].name,
                                    "file": fileList[i]
                                });
                            }
                        }
                        else {
                            if ($('#hidMode').val() == "replace") data = [];
                            data.push({
                                "name": fileList[i].name,
                                "type": fileList[i].name.split('.')[1],
                                "renamedFileName": fileList[i].name,
                                "file": fileList[i]
                            });
                        }
                        temp_data.push({
                            "name": fileList[i].name,
                            "type": fileList[i].name.split('.')[1],
                            "renamedFileName": fileList[i].name,
                            "file": fileList[i]
                        });
                    }
                }
                self.resetFileInput();
            }
            if (temp_data.length > 0) {
                temp_edit_file = [];
                self.createSelectedFilesList();
                self.displayFileList();
            }
            var msg = "<ul>";
            if (illegalFilesCnt > 0) {
                //                if (illegalFilesCnt == 1)
                //                    msg += '<li>One of the selected file has ';
                //                else
                //                    msg += '<li>Few of the selected files have ';
                msg += '<li>Illegal characters exist(s) in file name. Cannot upload.</li>';
            }
            if (invalidFileTypesCnt > 0) {
                //                if (invalidFileTypesCnt == 1)
                //                    msg += '<li>One of the selected file is ';
                //                else
                //                    msg += '<li>Few of the selected files are ';
                msg += (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? 'Invalid file type selection. Please upload a PDF file.' : 'Invalid File Type...Only "JPG","JPEG","PNG","PDF","GIF","TIF","TIFF","ZIP","SVG","HTML" and "HTM" types are accepted.';
            }
            if (fileExistenceCnt > 0) {
                msg += '<li>A file with the same name as an uploaded file cannot be uploaded. Either delete the original file or replace it by clicking the edit button on the uploaded file.</li>';
            }
            msg += "</ul>";
            if (msg != "<ul></ul>") {
                $("#popupListDefinition").popup("close");
                $('#validationAlertmsg').html(msg);
                window.setTimeout(function getDelay() {
                    $('#validationAlertmsg').addClass('wordwrap');
                    //isMultiple = false;
                    isDragDrop = false;
                    if (fileExistenceCnt > 0) {
                        //self.resetFileUploadControl(self.artFilesToUpload);
                        self.resetFileInput();
                        self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                    }
                    fileExistenceCnt = 0;
                    invalidFileTypesCnt = 0;
                    illegalFilesCnt = 0;
                    $('#validationPopupDialog').popup('open');
                    return false;
                }, 200);

            }
        }
    };

    self.createSelectedFilesList = function () {
        self.resetFileInput();
        //self.resetFileUploadControl(self.artFilesToUpload);
        //var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'); // document.getElementById('spnFileInput');
        //var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
        //var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
        //if (fileInput.value != "") {
        //    //fileInput.value = null;
        //    // Create a new file input
        //    var newFileInput = fileInput.cloneNode(true);
        //    newFileInput.value = null;
        //    //var id = fileInput.id.replace('[]', self.fileCount);
        //    //id = id + '[]';
        //    //fileInput.id = id;
        //    //fileInput.name = id;
        //    newFileInput.id = "mailFiles[]";
        //    newFileInput.name = "mailFiles[]";
        //    //$(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1].style.display = "none";
        //    $(tdFileInputsTemp).find('input[type=file]').remove();
        //    var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
        //    spnFileInputs.appendChild(newFileInput);
        //    //self.resetFileUploadPopup();
        //    //self.fileCount = self.fileCount + 1;
        //    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        //        self.navigate(e, $(e.target));
        //    });
        //}
        $.each(data, function (key, val) {
            file_name = val.name;
            file_type = file_name.split('.')[1];
            var file_type_code = getFileTypeCode(file_type);
            var is_file_found_in_selected = $.grep(self.artFilesToUpload, function (obj) {
                return (obj.fileName.toLowerCase() === file_name.toLowerCase())
            });
            if (is_file_found_in_selected.length == 0) {
                var uploading_file_name = "";
                if (file_name != "") {
                    uploading_file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
                    self.uploadingFiles += (self.uploadingFiles != "") ? "|" + uploading_file_name + "^" + getFileTypeCode('artwork') : uploading_file_name + "^" + getFileTypeCode('artwork');
                    self.artApprovalTypes += ((self.artApprovalTypes != "") ? "^" : "") + (($('#ddlApprovalType').val() != "select") ? $('#ddlApprovalType').val() : "");
                    self.updateFileInfo(uploading_file_name, file_type);
                    if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isArtFileSelectedFirstTime == undefined || sessionStorage.isArtFileSelectedFirstTime == null || sessionStorage.isArtFileSelectedFirstTime == "" || sessionStorage.isArtFileSelectedFirstTime == "false") && self.artFilesToUpload.length == 1 && (jobCustomerNumber == BBB_CUSTOMER_NUMBER || jobCustomerNumber == MOTIV8_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER)) {
                        sessionStorage.isArtFileSelectedFirstTime = true
                        window.setTimeout(function displayHint() { $('#uploadSelectedArtDemoHints').popup('open', { positionTo: '#btnUploadFiles' }); }, 1000);
                    }
                }
            }
        });
        //$("#popupListDefinition").popup("close");
        //self.makeFileListBeforePost(self.artFilesToUpload);
        if ($('#hidMode').val() != "replace") {
            temp_data = [];
        }
    };

    self.displayFileList = function () {
        var file_list = "<ul data-role='listview' data-theme='c'>";
        var file_type_n_list = '';
        var file_names = "";
        //for (var i = 0; i < temp_data.length; i++) {
        //    var type = getFileTypeCode("artwork");
        //    file_list += '<li><a href="" title="' + temp_data[i].name + '">' + temp_data[i].name + '</a><a href="" data-icon="delete" onclick="pageObj.deleteDraggedFile(\'' + temp_data[i].name + '\',' + i + '\,' + type + ')"></a></li>';
        //}
        for (var i = 0; i < self.artFilesToUpload.length; i++) {
            var type = getFileTypeCode("artwork");
            file_list += '<li><a href="" title="' + self.artFilesToUpload[i].fileName + '">' + self.artFilesToUpload[i].fileName + '</a><a href="" data-icon="delete" onclick="pageObj.deleteDraggedFile(\'' + self.artFilesToUpload[i].fileName + '\',' + i + '\,' + self.artFilesToUpload[i].sourceType + ')"></a></li>';
        }
        file_list += '</ul>';
        $('#drop-area').empty();
        $('#drop-area').html(file_list);
        window.setTimeout(function createList() {
            $('#drop-area ul').listview();
        }, 50);
    };


    self.handleReaderOnLoadEnd = function () {
        return function (event) {
            $image.attr("src", this.result)
                .addClass("small")
                .appendTo("#drop-area");
        };
    }
    self.deleteDraggedFile = function (file_name, file_index, type) {
        temp_data = data;
        temp_edit_file.push(temp_data[file_index]);
        temp_data.splice(file_index, 1);
        var file_list = "<ul data-role='listview' data-theme='c'>";
        var file_type_n_list = '';
        var file_names = "";
        var del_temp_data = [];
        for (var i = 0; i < data.length; i++) {
            if (data[i].name != file_name) {
                del_temp_data.push({
                    "name": data[i].name,
                    "type": data[i].type,
                    "file": data[i].file
                });
            }
        }
        var selected_file_index = null;
        if (self.artFilesToUpload.length > 0) {
            $.each(self.artFilesToUpload, function (key, val) {
                if (val.fileName.toLowerCase().substring(val.fileName.lastIndexOf('\\') + 1, val.fileName.length) == file_name.toLowerCase()) {
                    selected_file_index = key;
                    return false;
                }
            });
            if (selected_file_index != null) {
                self.artFilesToUpload.splice(selected_file_index, 1);
                var uploading_files = self.uploadingFiles.split('|');
                self.uploadingFiles = [];
                $.each(uploading_files, function (a, b) {
                    var file_info = b.split('^');
                    if (file_name.toLowerCase() == file_info[0].toLowerCase() && file_info[1] == type) {
                    }
                    else {
                        self.uploadingFiles += (self.uploadingFiles != "") ? '|' + b : b;
                    }
                });
                self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
            }
        }
        //for (var i = 0; i < temp_data.length; i++) {
        //    var type = getFileTypeCode("artwork");
        //    file_list += '<li><a href="" title="' + temp_data[i].name + '">' + temp_data[i].name + '</a><a href="" data-icon="delete" onclick="pageObj.deleteDraggedFile(\'' + temp_data[i].name + '\',' + i + '\,' + type + ')"></a></li>';
        //}
        for (var i = 0; i < self.artFilesToUpload.length; i++) {
            var type = getFileTypeCode("artwork");
            file_list += '<li><a href="" title="' + self.artFilesToUpload[i].fileName + '">' + self.artFilesToUpload[i].fileName + '</a><a href="" data-icon="delete" onclick="pageObj.deleteDraggedFile(\'' + self.artFilesToUpload[i].fileName + '\',' + i + '\,' + self.artFilesToUpload[i].sourceType + ')"></a></li>';
        }
        data = del_temp_data;
        file_list += '</ul>';
        $('#drop-area').empty();
        $('#drop-area').html(file_list);
        window.setTimeout(function createList() {
            $('#drop-area ul').listview();
        }, 50);
    };

    self.validateFiles = function (file_name) {
        var file_name_type = "";
        var file_names = "";
        var check_file_name = (file_name.indexOf('\\') > -1) ? file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.length) : file_name;
        var illegal_filename_regexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;
        var allowed_extension = ["jpg", "jpeg", "png", "pdf", "gif", "tif", "tiff", "svg", "html", "htm", "zip"];
        if (!illegal_filename_regexp.test(check_file_name.substring(0, check_file_name.lastIndexOf('.')))) {
            illegalFilesCnt++;
            return false;
        }
        else if ($('#hidMode').val() != "replace" && !self.verifyFileExistence(check_file_name)) {
            fileExistenceCnt++;
            return false;
        }
        else {
            var extension = file_name.split('.');
            var extension = file_name.substring(file_name.lastIndexOf('.') + 1);
            var isValidFileFormat = false;
            var uploading_file_name = "";
            if (typeof (extension) != 'undefined' && extension.length > 1) {
                for (var i = 0; i < allowed_extension.length; i++) {
                    if (extension.toLowerCase() == allowed_extension[i]) {
                        isValidFileFormat = true;
                        break;
                    }
                }
                if (!isValidFileFormat) {
                    invalidFileTypesCnt++;
                    return false;
                }
                else return true;
            }
            //var filemappername = file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.lastIndexOf('.')) + "_map.xml";
        }
    };

    //Validates the file selection settings.
    self.uploadMultiplesValidation = function (is_browse_click) {
        var msg = "<ul>";
        if ($('#fieldType').val() == "select")
            msg += "<li>Please select List type.</li>";

        if (appPrivileges.roleName == "admin" && $('#select-choice-1').val() == "select")
            msg += "<li>Please select File Delimiter.</li>";
        //if (temp_data.length == 0) {
        if (self.artFilesToUpload.length == 0 && !is_browse_click) {
            //if ($('#name').val() == "")
            msg += "<li>Please choose/drag and drop a file to upload.</li>";
        }
        if ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || jobCustomerNumber == CASEYS_CUSTOMER_NUMBER) && ($('#dvSelectedLocations input[type=checkbox]:not([value=all]):checked').length == 0 && $('#hidMode').val() != "replace")) {
            msg += "<li>Please select at least one location.</li>";
        }

        msg += "</ul>";

        if (msg != "<ul></ul>") {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html(msg);
            window.setTimeout(function getDelay() {
                $('#validationAlertmsg').addClass('wordwrap');
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);

        }
        else
            return true;
    };
    self.resetFileInput = function () {
        var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'); // document.getElementById('spnFileInput');
        var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
        var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
        if (fileInput.value != "") {
            // Create a new file input
            var newFileInput = fileInput.cloneNode(true);
            newFileInput.value = null;
            $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
            tdFileInputsTemp.appendChild(newFileInput);
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.navigate(e, $(e.target));
            });
        }
    }
};