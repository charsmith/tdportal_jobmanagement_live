﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gVarOfferInfoServiceUrl = "../jobOfferSetup/JSON/_jobOfferSetup.JSON";
var gServiceData;
var gServiceOffers = [];
var pageObj;
var companyData;
var primaryFacilityData;
var gOutputData;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobOfferSetup').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_jobOfferSetup');
    createConfirmMessage("_jobOfferSetup");
    loadingImg('_jobOfferSetup');

    displayNavLinks();
    getOffersInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
    createDemoHints("jobOfferSetup");
    //$('#sldrShowHints').val(sessionStorage.showDemoHints).slider('refresh');
});

$(document).on('pageshow', '#_jobOfferSetup', function (event) {

    window.setTimeout(function () {
        //if (pageObj == undefined || pageObj == null) {
        pageObj = new loadOffersInfo();
        ko.applyBindings(pageObj);
        //}
        var curr_date = new Date();
        var min_date = new Date(curr_date.getDate() + 1, curr_date.getMonth(), curr_date.getFullYear());
        $.each($('input[data-fieldtype=expiryDate]'), function () {

            $(this).datepicker({
                minDate: new Date(),
                dateFormat: 'mm/dd/yy',
                constrainInput: true
            });
        });
        $('#ddlLocations').selectmenu('refresh');
        $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
        if (pageObj.selectedStore() != "") {
            $('#ddlLocations').trigger('change');
            // pageObj.getSelectedInfo();

        }

        //.datepicker();
        $('#dvOfferSetup').trigger('create');
        //window.setTimeout(function () {
        //$('input[data-fieldtype=expiryDate]').datepicker();
        $.each($('input[type=radio]:checked'), function (key, val) {
            $(val).val($(this).val()).trigger('change');
        });
        $('#chkReplicate').checkboxradio('refresh');
        //$('input[type=radio]:checked').trigger('change');//.checkboxradio();
        //}, 0);
    }, 500);
    if (jobCustomerNumber == AAG_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
        $('#dvReplicate').css('display', 'none');
        $('#dvNavBar').css('display', 'none');
        if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
            $('a[data-icon="myapp-locations"]').css('display', 'none');
            $('a[data-icon="eye"]').addClass('ui-first-child');
        }
    }
    persistNavPanelState();
    if (!dontShowHintsAgain && (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER)
                          && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" ||
                              sessionStorage.showDemoHints == "on")
                          && (sessionStorage.isOfferSetupDemoHintsDisplayed == undefined || sessionStorage.isOfferSetupDemoHintsDisplayed == null ||
                              sessionStorage.isOfferSetupDemoHintsDisplayed == "false")) {
        sessionStorage.isOfferSetupDemoHintsDisplayed = true;
        $('#offerSetupHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#offerSetupHints').popup('open', { positionTo: '#dvOfferSetup' });
        }, 500);
    }
});

var getOffersInfo = function () {
    //if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
    //    gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
    //    if ((gOutputData.offerSetupAction != undefined && gOutputData.offerSetupAction != null && gOutputData.offerSetupAction != "") && (gOutputData.offerSetupAction.offersList.length > 0)) {
    //        gServiceData = gOutputData.offerSetupAction;
    //    }
    //}
    $.getJSON(gVarOfferInfoServiceUrl, function (data) {
        //gServiceOffers = data;
        $.each(data, function (key, val) {
            if (key.indexOf(jobCustomerNumber) > -1) {
                gServiceOffers = val;
            }
        });
    });
}

var offerInfo = function (offer_name, offer_value, is_checked) {
    var self = this;
    self.offerName = ko.observable('');
    self.offerName(offer_name);
    self.offerValue = ko.observable('');
    self.offerValue(offer_value);
    self.isChecked = ko.observable('');
    self.isChecked(is_checked);
};

var selectedOfferInfo = function (offer_type, offer_name, offer_value, offer_expiry_date, offer_description, offer_amount, offer_own_text, percent, offer_redeem_at, online_code) {
    var self = this;
    self.offerType = ko.observable('');
    self.offerType(offer_type);
    self.offerName = ko.observable('');
    self.offerName(offer_name);
    self.offerValue = ko.observable('');
    self.offerValue(offer_value);
    self.offerAmount = ko.observable('');
    self.offerAmount(offer_amount);
    self.offerDescription = ko.observable('');
    self.offerDescription(offer_description);
    self.offerExpiryDate = ko.observable('');
    self.offerExpiryDate(offer_expiry_date);
    self.ownOfferText = ko.observable('');
    self.ownOfferText(offer_own_text);
    if (percent != undefined && percent != null) {
        self.offerPercent = ko.observable('');
        self.offerPercent(percent);
    }
    if (offer_redeem_at) {
        self.offerDisclaimer = ko.observable('');
        self.offerDisclaimer(offer_redeem_at);
    }
    if (online_code) {
        self.onlineCode = ko.observable('');
        self.onlineCode(online_code);
    }
};

var offersList = function (offer_type, offers_list, amount, offer_description, offer_exp_date, own_offer_text, selected_offer, percent, offer_redeem_at, online_code) {
    var self = this;
    self.offerType = ko.observable('');
    self.offerType(offer_type);
    //self.offersList = ko.observableArray([]);
    self.variableDict = ko.observableArray([]);
    //self.offersList(offers_list);
    self.variableDict(offers_list);
    self.offerAmount = ko.observable('');
    var temp_amount = '$' + amount;
    self.offerAmount(temp_amount);

    self.offerDisclaimer = ko.observable('');
    self.offerDisclaimer(offer_redeem_at);
    self.onlineCode = ko.observable('');
    self.onlineCode(online_code);
    self.offerDescription = ko.observable('');
    self.offerDescription(offer_description);
    if (offer_exp_date == undefined || offer_exp_date == null || offer_exp_date == "") {
        var tmp_date = new Date();
        tmp_date.setDate(tmp_date.getDate() + parseInt(30));
        offer_exp_date = tmp_date.getMonth() + 1 + "/" + tmp_date.getDate() + "/" + tmp_date.getFullYear();
    }
    if (offer_exp_date != undefined && offer_exp_date != null) {
        self.offerExpiryDate = ko.observable('');
        self.offerExpiryDate(offer_exp_date);
    }
    self.ownOfferText = ko.observable('');
    self.ownOfferText(own_offer_text);
    self.isOwnOfferText = ko.observable(false);
    self.isVariableAmount = ko.observable((amount != "") ? true : false);
    self.selectedOffer = ko.observable();
    self.selectedOffer(selected_offer);
    if (percent != undefined && percent != null) {
        self.offerPercent = ko.observable('');
        var temp_percent = percent + '%';
        self.offerPercent(temp_percent);
    }
};

var locationVm = function (store_id, store_name, store_pk) {
    var self = this;
    self.locationId = store_pk;
    if (store_id != undefined && store_id != null && store_id != "")
        self.locationName = store_id + ' - ' + store_name;
    else
        self.locationName = store_name;
};

var selectedOfersListInfo = function (offers_list, is_replicated) {
    var self = this;
    self.isReplicated = ko.observable('');
    self.isReplicated(is_replicated);
    $.each(offers_list, function (key, val) {
        self[key] = ko.observable({});
        self[key](val);
    });
};

var selectedOffersInfo = function (offers_selected, is_enabled) {
    var self = this;
    //self.offersList = ko.observableArray([]);
    self.variableDict = ko.observableArray([]);
    //self.offersList(offers_selected);
    self.variableDict(offers_selected);
    self.isEnabled = ko.observable('');
    self.isEnabled(is_enabled);
    self.name = "offerSetup";
};

var loadOffersInfo = function () {
    var self = this;
    //self.offersList = ko.observableArray([]);
    self.variableDict = ko.observableArray([]);
    self.locationsList = ko.observableArray([]);
    self.selectedLocation = ko.observable({});

    self.offerSetupInfo = ko.observable({});
    self.prevOfferSetupInfo = ko.observable({});
    self.selectedInHomeStoreOfferInfo = ko.observable({});
    self.selectedInhomeInfo = ko.observable({});
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.selectedStore = ko.observable('');
    self.isReplicated = ko.observable();
    self.previousSelectedStore = ko.observable({});

    //Loading all the default offers into the screen
    var temp_offers_list = [];
    $.each(gServiceOffers, function (key, val) {
        var selected_offer = "";
        $.each(val.offersList, function (offer_key, offer_val) {
            temp_offers_list.push(new offerInfo(offer_val.name, offer_val.value, offer_val.isChecked));
            if (offer_val.isChecked == "true")
                selected_offer = offer_val.value;
        });

        self.variableDict.push(new offersList(val.offerType, temp_offers_list, val.amount, val.description, val.expirationDate, val.ownOfferText, selected_offer, val.percent, val.offerDisclaimer, val.onlineCode));
        temp_offers_list = [];
    });


    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
            //Loading all the selected stores into drop down.
            if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
                $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                    if (key == 0) {
                        self.selectedLocation(val.pk);
                        self.selectedStore(val.pk);
                        self.previousSelectedStore(self.selectedStore());
                    }
                    self.locationsList.push(new locationVm(val.storeId, val.storeName, val.pk));
                });
            }
        }

        if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            if ((gOutputData.offerTextAction != undefined && gOutputData.offerTextAction != null && gOutputData.offerTextAction != "")) {
                if ((gOutputData.offerTextAction.variableDict != undefined && gOutputData.offerTextAction.variableDict != null && gOutputData.offerTextAction.variableDict != "")) {
                    var temp_list = ko.toJSON(self.variableDict);
                    var temp_inhomewise_list = {};
                    var is_replicated = '';
                    var index = 0;
                    var temp_bases_list = {};
                    var offers_list = [];
                    var temp_offers_list = []
                    var is_checked = false;
                    $.each(self.variableDict(), function (key, val) {
                        //if(val.offer)
                        $.each(val.variableDict(), function (key1, val1) {
                            is_checked = false;
                            if (val1.offerValue() == gOutputData.offerTextAction.variableDict.offerValue) {
                                //val.offer
                                val1.isChecked(true);
                                is_checked = true;
                                return false;
                            }
                        });
                        if (is_checked) {
                            val.offerAmount('$' + gOutputData.offerTextAction.variableDict.offerAmount);
                            val.offerDescription(gOutputData.offerTextAction.variableDict.offerDescription);
                            val.offerType(gOutputData.offerTextAction.variableDict.offerType);
                            val.ownOfferText(gOutputData.offerTextAction.variableDict.ownOfferText);
                            val.onlineCode(gOutputData.offerTextAction.variableDict.onlineCode);
                            val.offerExpiryDate(gOutputData.offerTextAction.variableDict.offerExpiryDate);
                            val.offerDisclaimer(gOutputData.offerTextAction.variableDict.offerDisclaimer);
                            val.offerPercent(gOutputData.offerTextAction.variableDict.offerPercent);
                            val.isVariableAmount((gOutputData.offerAmount != "") ? true : false);
                            val.selectedOffer(gOutputData.offerTextAction.variableDict.offerValue);
                        }
                    });

                    // $.each(gOutputData.offerTextAction.variableDict, function (key2, val2) {
                    temp_offers_list.push(new selectedOfferInfo(gOutputData.offerTextAction.variableDict.offerType, gOutputData.offerTextAction.variableDict.offerName, gOutputData.offerTextAction.variableDict.offerValue, gOutputData.offerTextAction.variableDict.offerExpiryDate, gOutputData.offerTextAction.variableDict.offerDescription, gOutputData.offerTextAction.variableDict.offerAmount, gOutputData.offerTextAction.variableDict.ownOfferText, gOutputData.offerTextAction.variableDict.offerPercent, gOutputData.offerTextAction.variableDict.offerDisclaimer, gOutputData.offerTextAction.variableDict.onlineCode));
                    //});
                    temp_inhomewise_list = new selectedOffersInfo(temp_offers_list, gOutputData.offerTextAction.variableDict.isEnabled);
                    if (is_replicated == true)
                        self.isReplicated(true);
                    else
                        self.isReplicated(false);
                    self.offerSetupInfo(new selectedOfersListInfo(temp_inhomewise_list, gOutputData.offerTextAction.isReplicated));
                    self.prevOfferSetupInfo(new selectedOfersListInfo(temp_inhomewise_list, gOutputData.offerTextAction.isReplicated));
                }
                //NEED TO UPDATE THE OBJECT WITH THE OFFER DATA FROM THE JOB TICKET JSON IN THE ABOVE CODE.
            }
        }
        else {
            if ((gOutputData.offerSetupAction != undefined && gOutputData.offerSetupAction != null && gOutputData.offerSetupAction != "")) {
                var temp_inhomewise_list = {};
                var is_replicated = '';
                var index = 0;
                var temp_bases_list = {};
                var offers_list = [];
                $.each(gOutputData.offerSetupAction, function (key, val) {
                    if (is_replicated == '' && key == 'isReplicated')
                        is_replicated = val;
                    else {
                        temp_inhomewise_list[key] = {};
                        $.each(val, function (key1, val1) {
                            var temp_offers_list = []
                            $.each(val1.offersList, function (key2, val2) {
                                temp_offers_list.push(new selectedOfferInfo(val2.offerType, val2.offerName, val2.offerValue, val2.offerExpiryDate, val2.offerDescription, val2.offerAmount, val2.ownOfferText, val2.offerPercent, val2.offerDisclaimer, val2.onlineCode));
                            });
                            temp_inhomewise_list[key][key1] = new selectedOffersInfo(temp_offers_list, val1.isEnabled);
                        });
                    }
                });
                if (is_replicated == true)
                    self.isReplicated(true);
                else
                    self.isReplicated(false);
                self.offerSetupInfo(new selectedOfersListInfo(temp_inhomewise_list, is_replicated));
                self.prevOfferSetupInfo(new selectedOfersListInfo(temp_inhomewise_list, is_replicated));
            }
        }
    }
    if (jobCustomerNumber != SK_CUSTOMER_NUMBER) {
        //Loading Inhome dates nav bar.
        if (gOutputData != undefined && gOutputData != null) {
            var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
            var cnt = -1;
            $.each(gOutputData, function (key, val) {
                if (key.toLowerCase().indexOf('inhome') > -1) {
                    var temp_list = {};
                    temp_list[key] = val;
                    self.inHomeDatesList.push({
                        "showCheckIcon": false,
                        'dateValue': val
                    });
                    cnt++;
                    if (cnt == 0)
                        nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                    else
                        nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
                }
            });
            nav_bar_items += '</ul></div>';
            self.currentInHome(self.inHomeDatesList()[0]);
        }
    }

    self.offerChanged = function (data, event) {
        var ele = event.target || event.targetElement;
        var parent_context = ko.contextFor(ele).$parent;
        //if ($(ele).is('checked')) {
        //data.isChecked(true);
        if ($(ele).val().toLowerCase().indexOf('variableamount') > -1 || $(ele).val().toLowerCase().indexOf('purchaseoffer') > -1) {
            parent_context.isVariableAmount(true);
            parent_context.ownOfferText('');
            parent_context.isOwnOfferText(false);
            if (appPrivileges.customerNumber == SK_CUSTOMER_NUMBER) {
                if (gOutputData.offerTextAction.variableDict != undefined && gOutputData.offerTextAction.variableDict != null && Object.keys(gOutputData.offerTextAction.variableDict).length == 0)
                    parent_context.offerDescription('A Special Offer from One Baseball Lover to Another');
            }
            parent_context.selectedOffer(ele.value);
        }
        else if ($(ele).val().toLowerCase().indexOf('ownoffer') > -1) {
            parent_context.isVariableAmount(false);
            //parent_context.offerAmount('');
            if (parent_context.offerAmount != undefined && parent_context.offerAmount != null)
                parent_context.offerAmount('');
            if (appPrivileges.customerNumber == SK_CUSTOMER_NUMBER) {
                if (gOutputData.offerTextAction.variableDict != undefined && gOutputData.offerTextAction.variableDict != null && Object.keys(gOutputData.offerTextAction.variableDict).length == 0)
                    parent_context.offerDescription('');
            }
            parent_context.isOwnOfferText(true);
            parent_context.selectedOffer(ele.value);
        }
        else {
            //parent_context.ownOfferText('');
            //parent_context.offerAmount('$');
            parent_context.isOwnOfferText(false);
            parent_context.isVariableAmount(false);
            //parent_context.offerDescription('');
            //if (parent_context.offerAmount != undefined && parent_context.offerAmount != null)
            //    parent_context.offerAmount('%');
            parent_context.selectedOffer('');
        }
        //}
        $('#dvOfferSetup').trigger('create');
    };

    //Load the base info for the selected location in the current inhome.
    self.getSelectedInfo = function () {
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var selected_inhome = (jobCustomerNumber != SK_CUSTOMER_NUMBER) ? self.currentInHome().dateValue : "";
        var is_found = false;
        if (self.offerSetupInfo()[selected_inhome] != undefined && self.offerSetupInfo()[selected_inhome]() != undefined && self.offerSetupInfo()[selected_inhome]() != null)
            self.selectedInhomeInfo(self.offerSetupInfo()[selected_inhome]());

        if (self.offerSetupInfo()[selected_inhome] != undefined && self.offerSetupInfo()[selected_inhome]()[self.selectedStore()] != undefined && self.offerSetupInfo()[selected_inhome]()[self.selectedStore()] != null) {
            self.selectedInHomeStoreOfferInfo(self.offerSetupInfo()[selected_inhome]()[self.selectedStore()]);
            if (ko.toJS(self.offerSetupInfo()[selected_inhome]()[self.selectedStore()].variableDict).length > 0) {
                var temp = ko.toJS(self.offerSetupInfo()[selected_inhome]()[self.selectedStore()].variableDict);
                $.each(temp, function (key, val) {
                    var is_found = false;
                    //$.each(val.offersList, function (key1, val1) {
                    $.each(self.variableDict(), function (key1, val1) {
                        if (val1.offerType() == val.offerType) {
                            $.each(val1.variableDict(), function (key2, val2) {
                                if (val2.offerName() == val.offerName) {
                                    val2.isChecked(true);
                                    val1.offerType(val.offerType);
                                    val1.offerDisclaimer(val.offerDisclaimer);
                                    val1.offerDescription(val.offerDescription);
                                    var amt = '$' + val.offerAmount;
                                    val1.offerAmount(amt);
                                    if (val1.offerExpiryDate != undefined && val1.offerExpiryDate != null)
                                        val1.offerExpiryDate(val.offerExpiryDate);
                                    val1.isVariableAmount(((val.offerAmount.substring(1) != "") ? true : false));
                                    val1.isOwnOfferText(((val.ownOfferText != "") ? true : false));
                                    val1.ownOfferText(val.ownOfferText);
                                    val1.selectedOffer(val.offerValue);
                                    if (val1.offerPercent != undefined && val1.offerPercent != null) {
                                        var pcnt = val.offerPercent + '%';
                                        val1.offerPercent(pcnt);
                                    }
                                    is_found = true;
                                }
                                if (is_found)
                                    return false;
                            });
                        }
                        if (is_found)
                            return false;
                    });
                    //});
                });
            }
        }
        $('#dvOfferSetup').css('display', 'block');
        $('#dvOfferSetup').trigger('create');
        var temp = ko.toJS(self.offerSetupInfo());
        self.updateInHomeNavBarUI(temp);
    };

    self.persistSelectedOfferInfo = function () {
        var selected_inhome = (jobCustomerNumber != SK_CUSTOMER_NUMBER) ? self.currentInHome().dateValue : gOutputData.inHomeDate;

        if (selected_inhome != "" && self.selectedStore() != "") {
            var temp_offers_list = ko.observableArray([]);
            var offers_list = ko.observableArray([]);
            var is_found = false;
            $.each(self.variableDict(), function (key, val) {
                $.each(val.variableDict(), function (key1, val1) {
                    //if (JSON.parse(val1.isChecked())) {
                    if (val1.offerValue() == val.selectedOffer()) {
                        var temp = ko.toJS(val1);
                        var temp_val = ko.toJS(val);
                        temp = new selectedOfferInfo(temp_val.offerType, temp.offerName, temp.offerValue, temp_val.offerExpiryDate, temp_val.offerDescription, ((temp_val.offerAmount.indexOf('$') > -1) ? temp_val.offerAmount.substring(1) : temp_val.offerAmount), temp_val.ownOfferText, temp_val.offerPercent.substring(0, temp_val.offerPercent.length - 1), temp_val.offerDisclaimer, temp_val.onlineCode);
                        temp_offers_list.push(temp);
                        val1.isChecked(true);
                        is_found = true;
                    }
                    if (is_found)
                        return false;
                });
                //var temp_amount = '$';
                if (is_found) {
                    //val.offerAmount('$');
                    //val.offerDescription('');
                    //if (val.offerExpiryDate != undefined && val.offerExpiryDate != null) val.offerExpiryDate('');
                    // if (val.offerPercent != undefined && val.offerPercent != null) val.offerPercent('%');
                    //val.ownOfferText('');
                    // val.isOwnOfferText(false);
                    // val.isVariableAmount(false);
                    //val.selectedOffer('');
                    is_found = false;
                }
            });
            if (ko.toJS(temp_offers_list).length > 0) {
                if (jobCustomerNumber != SK_CUSTOMER_NUMBER) {
                    self.offerSetupInfo()[selected_inhome]()[self.selectedStore()].variableDict([]);
                    self.offerSetupInfo()[selected_inhome]()[self.selectedStore()].variableDict(temp_offers_list);
                }
                else {
                    self.offerSetupInfo(temp_offers_list);
                }
            }

        }
    };

    //Loading the data based on the selected store in the current inhome.
    self.locationChanged = function (data, event) {
        //self.validateForDataExistency();
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        var selected_store = $(ele).val();
        var selected_inhome = (jobCustomerNumber != SK_CUSTOMER_NUMBER) ? self.currentInHome().dateValue : "";

        self.persistSelectedOfferInfo();

        self.selectedStore(self.selectedLocation());
        if (self.selectedLocation() != "") {
            self.getSelectedInfo();
            var curr_info = ko.toJS(self.offerSetupInfo());
            var prev_info = ko.toJS(self.prevOfferSetupInfo());
            var diff_between_json_objects = [];
            var is_diff_found = false;
            if (jobCustomerNumber != SK_CUSTOMER_NUMBER) {
                $.each(curr_info, function (key1, val1) {
                    if (key1.toLowerCase() != 'isreplicated') {
                        $.each(prev_info, function (key2, val2) {
                            if (key2.toLowerCase() != 'isreplicated') {
                                $.each(val1, function (key3, val3) {
                                    var curr_temp1 = val3;
                                    var prev_temp2 = val2[key3];
                                    diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                                    if (diff_between_json_objects.length != 0) {
                                        is_diff_found = true;
                                        return false;
                                    }
                                });
                            }
                            if (is_diff_found) {
                                return false;
                            }
                        });
                        if (is_diff_found) {
                            return false;
                        }
                    }
                });

                var is_valid = false;
                $.each(curr_info, function (key, val) {
                    if (key != "isReplicated") {
                        is_valid = self.verifyData(val);
                    }
                    if (!is_valid)
                        return false;
                });
            }
            if (is_diff_found && !is_valid) {
                self.isReplicated(false);
                $('#chkReplicate').checkboxradio('refresh');
            }
            else if (is_valid) {
                self.isReplicated(true);
                $('#chkReplicate').checkboxradio('refresh');
            }
        }
    };

    self.navBarItemClick = function (data, event) {
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var ele = event.target || event.targetElement;
        $('#dvNavBar div ul li a').removeClass('ui-btn-active');
        $(ele).addClass('ui-btn-active');
        self.persistSelectedOfferInfo();
        self.currentInHome(ko.dataFor(ele));
        self.getSelectedInfo();
    };

    self.replicateChanged = function (data, event) {
        self.persistSelectedOfferInfo();
        var ele = event.target || event.targetElement;
        $('#confirmMsg').html('Replicating Setup Entries will overwrite any previous information entered.');
        $('#okButConfirm').text('Continue');

        var curr_info = ko.toJS(self.offerSetupInfo());
        var prev_info = ko.toJS(self.prevOfferSetupInfo());
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            if (key1.toLowerCase() != 'isreplicated') {
                $.each(prev_info, function (key2, val2) {
                    if (key2.toLowerCase() != 'isreplicated') {
                        //if (key1 == key2) {
                        $.each(val1, function (key3, val3) {
                            var curr_temp1 = val3;
                            var prev_temp2 = val2[key3];
                            diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                            if (diff_between_json_objects.length != 0) {
                                is_diff_found = true;
                                return false;
                            }
                        });
                        //}
                        if (is_diff_found) {
                            return false;
                        }
                    }
                });
                if (is_diff_found) {
                    return false;
                }
            }
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            $('#okButConfirm').unbind('click');
            self.replicateData();
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        if ($(ele).is(':checked') && is_diff_found) {
            $('#popupConfirmDialog').popup('open');
        }
    };

    self.replicateData = function () {
        var selected_inhome = self.currentInHome().dateValue;
        var temp_info = {};
        if (self.offerSetupInfo()[selected_inhome]() != undefined && self.offerSetupInfo()[selected_inhome]() != null && self.offerSetupInfo()[selected_inhome]() != "")
            temp_info = self.offerSetupInfo()[selected_inhome]();

        $.each(self.inHomeDatesList(), function (key2, val2) {
            if (selected_inhome != val2.dateValue) {
                self.offerSetupInfo()[val2.dateValue](temp_info);
            }
        });
        self.offerSetupInfo().isReplicated(true);
        self.getSelectedInfo();

        var temp = ko.toJS(self.offerSetupInfo());
        if ((temp != undefined && temp != null && temp != "")) {
            var temp_inhomewise_list = {};
            var is_replicated = '';
            var index = 0;
            var temp_bases_list = {};
            $.each(temp, function (key, val) {
                if (is_replicated == '' && key == 'isReplicated')
                    is_replicated = val;
                else {
                    temp_inhomewise_list[key] = {};
                    $.each(val, function (key1, val1) {
                        var temp_offers_list = []
                        $.each(val1.variableDict, function (key2, val2) {
                            temp_offers_list.push(new selectedOfferInfo(val2.offerType, val2.offerName, val2.offerValue, val2.offerExpiryDate, val2.offerDescription, val2.offerAmount, val2.ownOfferText, val2.offerDisclaimer, val2.onlineCode));
                        });
                        temp_inhomewise_list[key][key1] = new selectedOffersInfo(temp_offers_list, val1.isEnabled);
                    });
                }
            });
            self.prevOfferSetupInfo(new selectedOfersListInfo(temp_inhomewise_list, is_replicated));
        }
        self.updateInHomeNavBarUI(temp);
    };

    self.verifyData = function (temp_info) {
        var is_data_found = false;
        $.each(temp_info, function (key1, val1) {
            if (val1.variableDict.length > 0)
                is_data_found = true;
            else
                is_data_found = false;
            if (!is_data_found) {
                return false;
            }
        });
        return is_data_found;
    };

    self.updateInHomeNavBarUI = function (temp) {
        if (jobCustomerNumber != SK_CUSTOMER_NUMBER) {
            $.each(temp, function (key, val) {
                if (key != "isReplicated") {
                    var is_valid = self.verifyData(val);
                    if (is_valid) {
                        $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
                    }
                    else {
                        $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
                    }
                }
            });
        }
    };

    self.confirmContinue = function (current_page, page_name, i_count, click_type) {
        self.persistSelectedOfferInfo();
        var temp = ko.toJS(self.offerSetupInfo());
        self.updateInHomeNavBarUI(temp);
        var ctrls = $('#dvNavBar div ul li a.ui-icon-minus');
        if (ctrls.length > 0 && click_type == "onlycontinue") {
            var tmp_list = "";
            $.each(ctrls, function (key, val) {
                tmp_list += (tmp_list != "") ? (key < (ctrls.length - 1)) ? ', ' : ' and ' + $(this).attr('data-inHome') : $(this).attr('data-inHome');
            });

            $('#confirmMsg').html('You have not completed setup for ' + tmp_list + '. Do you want to stay on this page to continue setup or go to the next page?');
            $('#okButConfirm').text('Go To Next Page');
            $('#cancelButConfirm').text('Continue Setup');

            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#cancelButConfirm').text('Cancel');
                $('#popupConfirmDialog').popup('close');
            });
            $('#okButConfirm').bind('click', function () {
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#cancelButConfirm').text('Cancel');
                $('#popupConfirmDialog').popup('close');
                goToNextPage("../jobSubmitOrder/jobSubmitOrder.html", "../jobSubmitOrder/jobSubmitOrder.html", 5, 'onlycontinue');
                self.updateOfferSelectionInfo(current_page, page_name, i_count, click_type);
            });
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        else {
            //goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
            return self.updateOfferSelectionInfo(current_page, page_name, i_count, click_type);
        }
    };

    self.updateOfferSelectionInfo = function (current_page, page_name, i_count, click_type) {
        var temp_json = ko.toJS(self.offerSetupInfo);
        if (jobCustomerNumber == SK_CUSTOMER_NUMBER) {
            gOutputData.offerTextAction.variableDict = temp_json[0];
        } else {
            var selected_inhome = self.currentInHome().dateValue;
            gOutputData.offerSetupAction = temp_json;
        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);

        if (click_type == "onlycontinue")
            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
        return true;
    };

    self.showOrderedLocations = function () {
        $('#popUpLocations').popup('open');
    };

    self.manageLocationsPopup = function (data, event) {
        var ele = event.target || event.targetElement;
        if ($(ele).attr('data-btnType') == "submit") {
            self.previousSelectedStore(self.selectedStore());
        }
        else {
            self.selectedStore(self.previousSelectedStore());
            self.selectedLocation(self.previousSelectedStore())
        }
        $('#ddlLocations').selectmenu('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        var sel_location = $('#ddlLocations option:selected').text();
        header_text = header_text + ' (' + sel_location + ')';
        $('#navHeader').text(header_text);
        $('#popUpLocations').popup('close');
    };
    //self.previewMailing = function () {
    //    getNextPageInOrder();
    //    var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
    //    window.location.href = preview_proof;
    //}
    self.previewMailing = function () {
        //Todo: Need to update pdf file for Kubota customer
        if (appPrivileges.customerNumber == SK_CUSTOMER_NUMBER || appPrivileges.customerNumber == KUBOTA_CUSTOMER_NUMBER || appPrivileges.customerNumber == GWA_CUSTOMER_NUMBER) {
            previewMailing();
			return false;
        }
        else if (appPrivileges.customerNumber == DCA_CUSTOMER_NUMBER) {
            encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
        }
        if (appPrivileges.customerNumber == AAG_CUSTOMER_NUMBER) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
        }
        getNextPageInOrder();
        //var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
        var preview_proof = "../jobApproval/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    }
};
