﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var gServiceAddOrderUrl = 'JSON/adOrder.JSON';
var gServiceAdOrderData = [];

//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobAdOrder').live('pagebeforecreate', function (event) {
    displayNavLinks();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    makeAddata();
});

$(document).on('pageshow', '#_jobAdOrder', function (event) {
    persistNavPanelState();

    window.setTimeout(function setDelay() {
        //$('#dvAdOrder ul').listview();
        $('#dvAdOrder').collapsible();
        $('div[data-role="collapsible-set"]').trigger('create');
        $('div[data-role="collapsible-set"]').collapsibleset().collapsibleset("refresh");
    }, 1000);
});

//******************** Page Load Events End **************************

//******************** Public Functions Start **************************
var adOrderViewModel = function (ad_order) {
    var self = this;
    self.publishName = ad_order.name;
    self.importListData = ad_order.importListDict;
}

var adOrderVM = function () {
    var self = this;
    self.adOrderDataList = ko.observableArray();
    $.each(gServiceAdOrderData.publicationNamesDict, function (a, b) {
        self.adOrderDataList.push(new adOrderViewModel(b));
    });
}

function makeAddata() {
    $.getJSON(gServiceAddOrderUrl, function (data) {
        gServiceAdOrderData = data;
        ko.applyBindings(new adOrderVM());
        //$("#dvAdOrder").listview("refresh");
        //$("#dvAdOrder").collapsibleset().collapsibleset("refresh");
    });
}
//******************** Public Functions End **************************