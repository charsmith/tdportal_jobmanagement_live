﻿var displayMapAndMarkers = function (self) {
    // Displays Markers for the given lat lng points and the type - e / o / n
    self.displayMarkers = function (lat_lng_pts, type) {
        if ($('#dvSearchLocationsMap').css('display') != 'none') {
            var map_center = self.map().getCenter();
            var current_markers = $.grep(self.markers_array, function (obj) {
                return obj.type === type;
            });
            var $mapDiv = $('#map-canvas');

            var mapDim = {
                height: $mapDiv.height(),
                width: $mapDiv.width()
            }
            $.each(lat_lng_pts, function (a, b) {
                var marker;
                var status = current_markers[0].addresses[a].substring(current_markers[0].addresses[a].lastIndexOf('^') + 1);
                var marker_number = current_markers[0].addresses[a].substring(0, current_markers[0].addresses[a].lastIndexOf('^'));
                marker_number = marker_number.substring(marker_number.lastIndexOf('^') + 1);
                //var marker_number = current_markers[0].addresses[a].substring(current_markers[0].addresses[a].lastIndexOf('^') + 1);

                if (b.lat != "" && b.lng != "")
                    marker = self.createMarkerForPoint(this, type, marker_number, status);
                else
                    marker = undefined;
                if (marker != undefined && marker != null && marker != "")
                    current_markers[0].markers.push({
                        marker: marker,
                        markerNo: marker_number
                    });
            });
            if (current_markers[0].markers.length > 0) {
                var bounds = (current_markers[0].markers.length > 0) ? self.createBoundsForMarkers(self.markers_array, type) : null;
                $.each(current_markers[0].markers, function (a, b) {
                    if (b.marker != undefined) {
                        if (type == 'o' && a <= 50) {
                            this.marker.setMap(self.map());
                            google.maps.event.addListener(b.marker, 'click', self.showInfoWindowExisting(a, type, current_markers[0].addresses[a]));
                        }
                        else if (type != 'o') {
                            this.marker.setMap(self.map());
                            google.maps.event.addListener(b.marker, 'click', self.showInfoWindowExisting(a, type, current_markers[0].addresses[a]));
                        }
                    }

                });
                window.setTimeout(function setMapBounds() {
                    self.map().setCenter(((bounds) ? bounds.getCenter() : new google.maps.LatLng(0, 0)));
                    var markers_not_found_count = 0;
                    $.each(self.markers_array, function (key, val) {
                        if (val.markers.length == 0)
                            markers_not_found_count++;
                    });
                    if (markers_not_found_count == 3 && current_markers[0].markers.length == 1)
                        self.map().setZoom(17);
                    else
                        self.map().setZoom(((bounds) ? self.getBoundsZoomLevel(bounds, mapDim) : 0));
                }, 2000);
            }
        }
    };

    //Clears the existing markers based on the type provided - e/o/n
    self.clearMarkers = function (type) {
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === type;
        });
        for (var i = 0; i < current_markers[0].markers.length; i++) {
            if (current_markers[0].markers[i].marker)
                current_markers[0].markers[i].marker.setMap(null);
        }
        current_markers[0].addresses = [];
        current_markers[0].markers = [];
    };

    //Shows the info window on the marker when the user clicks on the marker.
    self.showInfoWindowExisting = function (i, type) {
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === type;
        });
        var address = current_markers[0].addresses[i];
        return function (address) {
            if (self.infowindow()) {
                self.infowindow().close();
                self.infowindow(null);
            }

            self.infowindow(new google.maps.InfoWindow({
                content: self.getIWContentExisting(i, type, address),
                height: '100px'
            }));
            self.infowindow().open(self.map(), current_markers[0].markers[i].marker);
        }
    };
    //Gets the content required to display in the info window for a particular marker.
    self.getIWContentExisting = function (i, type, address) {
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === type;
        });
        var address_parts = current_markers[0].addresses[i].split('^');
        var content = '';
        content += '<table>';
        content += '<tr class="iw_table_row">';
        content += '<td colspan="2"><b>' + address_parts[1] + '</b></td></tr>';
        var address_contents = address_parts[0].split(',');
        content += '<tr class="iw_table_row"><td>' + address_contents[0] + '</td></tr>';
        content += '<tr class="iw_table_row"><td>' + $.trim(address_contents[1]) + '</td></tr>';
        content += '<tr class="iw_table_row"><td>' + $.trim(address_contents[2]) + '</td></tr>';
        content += (address_contents.length > 3) ? '<tr class="iw_table_row"><td>' + $.trim(address_contents[3]) + '</td></tr>' : '';
        if (type == 'e')
            content += '<tr class="iw_table_row"><td colspan ="2"><a href="#" data-clickType="existing" onclick="ko.contextFor(this).$root.addSelectedStore(ko.contextFor(this).$root.locations()[' + i + '],event);">Add to Order</a></td></tr>';
        else if (type == 'n')
            content += '<tr class="iw_table_row"><td colspan ="2"><a href="#" data-clickType="new" onclick="ko.contextFor(this).$root.editLocation(ko.contextFor(this).$root.searchedLocations()[' + i + '],' + i + ',ko.contextFor(this).$data,event);">Add to Order</a></td></tr>';
        content += '</table>';
        return content;
    };

    //Get the zoom level based on the provided bounds.
    self.getBoundsZoomLevel = function (bounds, mapDim) {
        //var WORLD_DIM = { height: 256, width: 256 };
        var WORLD_DIM = { height: 520, width: 256 };
        /*var WORLD_DIM ={};
        if ($.browser.msie) {
        WORLD_DIM = { height: 520, width: 520 };
        }
        else {
        WORLD_DIM = { height: 256, width: 256 };
        }*/
        var ZOOM_MAX = 21;

        function latRad(lat) {
            var sin = Math.sin(lat * Math.PI / 180);
            var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
            return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
        }

        function zoom(mapPx, worldPx, fraction) {
            return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
        }

        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();

        var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

        var lngDiff = ne.lng() - sw.lng();
        var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
        var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    };
    //Creates the bounds on the map for the displayed markers.
    self.createBoundsForMarkers = function (markers, type) {
        var bounds = new google.maps.LatLngBounds();
        $.each(markers, function (a, b) {
            $.each(b.markers, function (c, d) {
                if (d.marker != undefined)
                    if (type == 'o' && c <= 50)
                        bounds.extend(d.marker.getPosition());
                    else if (type != 'o')
                        bounds.extend(d.marker.getPosition());
            });
        });
        return bounds;
    };

    //Creates the marker on the map for the provided lat lng point.
    self.createMarkerForPoint = function (point, type, i, status) {
        var marker_no = 0;
        var temp_index = i.substring(0, i.indexOf('-'));
        var marker_color = "";
        switch (status.toLowerCase()) {
            case "available":
            case "open":
                marker_color = "blue";
                break;
            case "opted-out":
            case "declined":
                marker_color = "grey";
                break;
            case "opted-in":
                marker_color = "yellow";
                break;
            case "pending":
                marker_color = "orange";
                break;
            case "approved":
                marker_color = "red";
                break;
            case "":
                marker_color = "green";
                break;
        }

        if (type == 'o') marker_no = (((parseInt(temp_index) > 49) ? (parseInt(temp_index) % 50) : parseInt(temp_index)) + 1);
        else marker_no = parseInt(temp_index) + 1;

        var marker_folder = (status.toLowerCase() == "open" || status.toLowerCase() == "available" || status.toLowerCase() == "opted-out") ? "e" : ((status.toLowerCase() == "approved" || status.toLowerCase() == "opted-in" || status.toLowerCase() == "pending") ? "o" : 'n');
        //var icon = logoPath + 'mapMarkers/' + type + '/marker' + type.toUpperCase() + '_' + ((type == 'e') ? 'blue' : ((type == 'o') ? 'red' : 'green')) + marker_no + '.png';
        //var icon = logoPath + 'mapMarkers/' + type + '/marker' + type.toUpperCase() + '_' + marker_color + marker_no + '.png';
        var icon = logoPath + 'mapMarkers/' + marker_folder + '/marker' + marker_folder.toUpperCase() + '_' + marker_color + marker_no + '.png';
        if (type == "e")
            self.locations()[temp_index].locationMarker(icon);
        else if (type == "o")
            self.selectedLocationsAdded()[temp_index].locationMarker(icon);
        else if (type == "n")
            self.searchedLocations()[temp_index].locationMarker(icon);
        if (self.uIViewConfig.m() && self.uIViewConfig[type]())
            return new google.maps.Marker({
                position: new google.maps.LatLng(point.lat, point.lng),
                icon: icon
            });
        else
            return;
    };
}
