﻿var locationFiltersAndPaging = function () {
    var self = this;
    ko.utils.extend(self, new locationConfig(self)); // Extends the view model to integrate the locations configuration api.
    ko.utils.extend(self, new validateLocations(self)); // Extends the view model to integrate the api to validate the location fields.
    ko.utils.extend(self, new buildNewStores()); // Extends the view model to integrate the base view model (location setup).
    ko.utils.extend(self, new displayMapAndMarkers(self)); // Extends the view model to integrate the api to display markers etc.
    ko.utils.extend(self, new manageLocations(self)); // Extends the view model to integrate the api for manage location (add/remove/update etc).
    ko.utils.extend(self, new manageGroupedLocations(self));

    self.locationsExists = true;

    //Closes the popups - search
    self.closePopups = function () {
        $('#popupSearch').popup().popup('close');
    };

    self.initMapCanvas = function () {
        var latlng = new google.maps.LatLng(43.1462421, -104.9929722);
        geocoder = new google.maps.Geocoder();
        geocoder.geocode(/*{ 'address': 'US' }*/{ 'latLng': latlng }, function (results, status) {
            if (status == "OK") {
                var mapOptions = {
                    //zoom: 12,
                    zoom: 7,
                    //center: latlng,
                    center: results[0].geometry.location,
                    //mapTypeId: google.maps.MapTypeId.TERRAIN
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                if ($.parseJSON(self.uIViewConfig.m()))
                    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                else
                    map = new google.maps.Map(document.getElementById('tempMapCanvas'), mapOptions);
                gMapBoundsUSA = new google.maps.LatLngBounds(new google.maps.LatLng(28.70, -127.50), new google.maps.LatLng(48.85, -55.90));
                gMapCenter = map.getCenter();
                self.initializeMap();
            }
        });
    };

    self.viewPrefChanged = function (data, event) {
        //sessionStorage.firstTimeInLocations = "true";
        $('#waitPopUp').popup('open', { positionTo: '#dvWaitPopupRef' });
        $('#mapBlock').remove();
        $('#dvELocations').remove();
        $('#dvOLocations').remove();
        $('#dvNewSearchResults').remove();
        ko.cleanNode($('div.colmask')[0])

        ko.applyBindings(pageObj, $('div.colmask')[0]);
        self.isSwapping(true);
        self.initMapCanvas();
        window.setTimeout(function () {
            if (self.newSearchText() != "" && Object.keys(self.newSearchText()).length > 0) {
                var temp_json = ko.toJS(self.newSearchText);
                if (temp_json.locationName != "")
                    $('#txtLocationName').val(temp_json.locationName);
                if (temp_json.city != "")
                    $('#txtCity').val(temp_json.city);
                if (temp_json.address != "")
                    $('#txtAddress').val(temp_json.address);
                if (temp_json.state != "")
                    $('#ddlState').val(temp_json.state);
                if (temp_json.city != "")
                    $('#txtZipCode').val(temp_json.zip);
            }
            if (self.uIViewConfig.n()) {
                $('#dvSearchLocations').css('display', 'block');
                $('#dvSearchLocationsMap').css('display', 'block');
                $("#dvNewLocations").css('display', 'block');
                $('#dvNewSearchResults').css('display', 'block');
                if (self.searchedLocations().length > 4) {
                    $('#dvSearchLocationsContainer').css('height', ' 550px')
                    $('#dvSearchLocationsContainer').css('overflow', 'scroll-y');
                }
                else {
                    $('#dvSearchLocationsContainer').css('height', 'auto')
                    $('#dvSearchLocationsContainer').css('overflow', 'none');
                }
                $('#ulSearchLocations').listview().listview('refresh');
            }
            if (sessionStorage.tempSelectedStores != undefined && sessionStorage.tempSelectedStores != null && sessionStorage.tempSelectedStores != "") {
                var temp_selected_stores = $.parseJSON(sessionStorage.tempSelectedStores);
                var is_found = false;
                $.each(temp_selected_stores, function (key, val) {
                    $.each(self.gDataFiltered.locationsSelected, function (key1, val1) {
                        if (val1.storeId == val.storeId) { is_found = true; return false; }
                    });
                    if (!is_found)
                        self.gDataFiltered.locationsSelected.push(val);
                    is_found = false;
                });
            }

            //Inserts ordered locations into job ticket object.
            if (self.gDataFiltered != undefined) {
                var is_found = false;
                if (self.gDataFiltered.locationsSelected != undefined && self.gDataFiltered.locationsSelected != null && self.gDataFiltered.locationsSelected.length > 0) {
                    $.each(self.gDataFiltered.locationsSelected, function (loc_key1, loc_val1) {
                        $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (loc_key2, loc_val2) {
                            if (loc_val2.pk == loc_val1.pk) {
                                is_found = true;
                                return false;
                            }
                        });
                        if (!is_found) {
                            self.gOutputData.selectedLocationsAction.selectedLocationsList.push(loc_val1);
                        }
                        is_found = false;
                    });
                    //self.gOutputData.selectedLocationsAction.selectedLocationsList = [];
                    //self.gOutputData.selectedLocationsAction.selectedLocationsList = self.gDataFiltered.locationsSelected;
                }

                if (self.uIViewConfig.e() && self.gDataFiltered.locationsAvailable != undefined && self.gDataFiltered.locationsAvailable != null)
                    //Load Existing Locations
                    self.loadLocationsWithPaging("e", self.gDataFiltered.locationsAvailable, self.gDataFiltered.availableStartIndex, self.gDataFiltered.availableFinalIndex, self.gDataFiltered.availableTotalCount);

                if (self.uIViewConfig.o() && self.gDataFiltered.locationsSelected != undefined && self.gDataFiltered.locationsSelected != null)
                    //Load Ordered Locations
                    self.loadLocationsWithPaging("o", self.gDataFiltered.locationsSelected, self.gDataFiltered.selectedStartIndex, self.gDataFiltered.selectedFinalIndex, self.gDataFiltered.selectedTotalCount);
            }
            window.setTimeout(function () {
                self.isSwapping(false);
                $('#waitPopUp').popup('close');
            }, 1000);
        }, 1000);
        $('#dvE').trigger('create');
        $('#dvO').trigger('create');
        $('#dvM').trigger('create');
        $('#dvN').trigger('create');
    };

    self.viewClick = function () {
        $('#viewLocationsPref').panel('open');
    };

    self.findAllAvblLocByOIRecipientClick = function (data, event) {
        var ctrl = event.target || event.currentTarget || event.targetElement;
        if (ctrl.checked) {
            $('#dvOptInUsers').css('display', 'block');
        }
        else {
            $('#dvOptInUsers').css('display', 'none');
        }
    };

    self.userSelected = function (data, event) {
        var ctrl = event.currentTarget || event.target;
        if (ctrl.checked)
            self.selectedOptInUser.push(data.userName);
        else {
            var temp_idx;
            $.each(self.selectedOptInUser, function (key, val) {
                if (val == data.userName) {
                    temp_idx = key;
                    return false;
                }
            })
            if (temp_idx != undefined && temp_idx != null && temp_idx !== "")
                self.selectedOptInUser.splice(temp_idx, 1);
        }
    };

    self.clearSearchFields = function () {
        $('#txtLocationName').val('');
        $('#txtStoreId').val('');
        $('#txtCity').val('');
        $('#txtAddress').val('');
        $('#ddlState').val('');
        $('#txtState').val('');
        $('#txtZipCode').val('');
		$('#txtBrandName').val('');
        $('#txtBrandFilterLocation').val('');
        $('#txtParentIdFilterLocation').val('');
        $('#fldStOptInUsers input[type="checkbox"]').checkboxradio().attr('checked', false).checkboxradio('refresh');
        //$('#fldSetStatus input[type="checkbox"]').checkboxradio().attr('checked', false).checkboxradio('refresh');
        //self.newSearchText({});
    };

    self.findAllLocationsChanged = function () {
        self.clearSearchFields();
        createPlaceHolderforIE();
    };

    //Displays the locations search popup 
    //Fields would be displayed based on the type selected - existing/new.
    self.searchLocationsPopup = function (data, event) {
        self.searchText('');
        self.selectedOptInUser = [];
        self.clearSearchFields();
        $('#chkFindAllLocations').attr('checked', false).checkboxradio('refresh');
        $('#chkFindAllAvblLocByOIRecipient').attr('checked', false).checkboxradio('refresh').trigger('change');
        createPlaceHolderforIE();
        var partial_header = "";
        if ($(event.currentTarget).attr('mySearch') == "existing") {
            //Loads all the marketManagers.
            self.optInUsersList.removeAll();
            self.statusList.removeAll();
            partial_header = 'Search by partial or full terms, for example a search for "New" will return results such as "Newark" and "New York".';
            getCORS(gLocationSearchUrl + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + self.gOutputData.jobNumber + '/' + self.gOutputData.jobTypeId, null, function (data) {
                $.each(data, function (key, val) {
                    if (key == "marketManagers") {
                        var managers = [];
                        var temp_managers = {};
                        for (var key in val)
                            managers.push(key);
                        managers.sort();

                        for (var i = 0; i < managers.length; i++) {
                            temp_managers[managers[i]] = val[managers[i]];
                        }
                        $.each(temp_managers, function (manager_key, count_val) {
                            if (manager_key != "") {
                                self.optInUsersList.push(new optInUser(manager_key, count_val));
                            }
                        });
                    }
                    if (key == "statuses") {
                        $.each(val, function (status_key, count_val) {
                            self.statusList.push(new statusInfo(status_key, count_val));
                        });
                    }
                });
            }, function (error_response) {
                showErrorResponseText(error_response, true);
            });

            $('#headerSearch').text('Search Existing Locations');
            if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                $('#spnPartialHeader').text(partial_header);
            }
          
            if (self.storeId != undefined && self.storeId != null && $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == null)
                $('#txtStoreId').parent().css('display', 'block');
            else
                $('#txtStoreId').parent().css('display', 'none');
            $('#txtState').parent().css('display', 'block');
            $('#dvDdlState').css('display', 'none');
            $('#txtAddress').parent().css('display', 'block');
            $('#dvNewLocationLink').css('display', 'block');
            $('#txtLocationName').val('');
            $('#txtLocationName').parent().css('display', 'none');
            $('#fsRbtypes').css('display', 'none');
            $('#txtZipCode').parent().css('display', 'block');
            $('#txtZipCode').css('display', 'block');
            $('#dvFindAllLocations').css('display', 'block');
            $('#dvFilterByBrand').css('display', (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && self.gOutputData.jobTypeId == 73 ? 'none' : 'block'));
            $('#fldStOptInUsers input[type="checkbox"]').checkboxradio().checkboxradio('refresh');
            $('#txtBrandName').parent().css('display', ((jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER) ? 'block' : 'none'));
            $("#txtParentIdFilterLocation").parent().css('display', ((jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) ? 'block' : 'none'));
            $('#dvFindAllAvblLocByOIRecipient').css('display', (jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && self.gOutputData.jobTypeId == 73) ? 'none' : 'block'));
            $('#fldSetStatus div').trigger('create');
            $('#fldSetStatus').trigger('create');
            $('#dvStatusList').collapsibleset().collapsibleset('refresh');
            $('#fldSetStatus input[type="checkbox"]').attr('checked', true).checkboxradio('refresh');
            $('#dvStatusList').css('display', ((jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER || (jobCustomerNumber == CASEYS_CUSTOMER_NUMBER && self.gOutputData.jobTypeId == 73)) ? 'none' : 'block'));
            $('#dvSearchFilters').collapsibleset('refresh');
        }
        else if ($(event.currentTarget).attr('mySearch') == "new") {
            self.newSearchText({});
            if (jobCustomerNumber == DCA_CUSTOMER_NUMBER || jobCustomerNumber == KUBOTA_CUSTOMER_NUMBER || jobCustomerNumber == SK_CUSTOMER_NUMBER || jobCustomerNumber == GWA_CUSTOMER_NUMBER) {
                $('#spnPartialHeader').text(partial_header);
            }
            var default_search = "";
            if (jobCustomerNumber == CW_CUSTOMER_NUMBER || jobCustomerNumber == BRIGHTHOUSE_CUSTOMER_NUMBER || jobCustomerNumber == BBB_CUSTOMER_NUMBER)
                default_search = "RV Sales";
            else if (jobCustomerNumber == REGIS_CUSTOMER_NUMBER)
                default_search = "Haircut";
            self.newPosition('');
            $('#popupSearch').popup().popup('close');
            $('#txtStoreId').parent().css('display', 'none');
            $('#txtState').parent().css('display', 'none');
            $('#dvDdlState').css('display', 'block');
            $('#txtLocationName').parent().css('display', 'block');
            $('#txtLocationName').css('display', 'block');
            $('#dvNewLocationLink').css('display', 'none');
            $('#headerSearch').text('Search For New Locations');
            $('#txtLocationName').val(default_search);
            $('#fsRbtypes').css('display', 'block');
            $('#rbtBusinessName').trigger('click');
            $('#txtZipCode').css('display', 'none');
            $('#txtZipCode').parent().css('display', 'none');
            $('#dvFindAllLocations').css('display', 'none');
            $('#dvFindAllAvblLocByOIRecipient').css('display', 'none');
            $('#dvFilterByBrand').css('display', 'none');
            $('#dvStatusList').css('display', 'none');
            var search_type_fields = '<fieldset data-role="controlgroup" data-mini="true" id="fsRbtypes"><legend>Search By:</legend>';
            search_type_fields += '<input type="radio" id="rbtBusinessName" data-theme="c" name="rdoSearchGroup" onclick="ko.contextFor(this).$root.changeType(ko.contextFor(this).$data,event);" />';
            search_type_fields += '<label for="rbtBusinessName">Business Name or Type</label>';
            search_type_fields += '<input type="radio" id="rbtStreetAddr" data-theme="c" name="rdoSearchGroup" onclick="ko.contextFor(this).$root.changeType(ko.contextFor(this).$data,event);" />';
            search_type_fields += '<label for="rbtStreetAddr">Street Address</label></fieldset>';
            $('#dvRadioGrp').html(search_type_fields).trigger('create');
            $('#rbtBusinessName').attr('checked', true).checkboxradio('refresh').trigger('click');
            $('#dvSearchFilters').collapsibleset('refresh');
        }
        //$('#dvFilterByGeography').trigger('collapsibleexpand');
        $('#dvFilterByGeography').collapsible('expand');
        $('#ddlState').selectmenu().selectmenu('refresh');
        self.pageId(0);
        if (self.gOutputData.jobNumber == -1) {
            self.ordPageId(0);
        }
        $('#popupSearch').popup().popup('open');
        if (map == undefined) {
            self.initMapCanvas();
        }
    };

    //Event to manipulate fiels for the new search options.
    self.changeType = function (data, event) {
        if ((event.currentTarget || event.target).id.indexOf('Street') > -1) {
            $("input[type='radio']:last").trigger('create').attr("checked", "checked");
            $('#txtLocationName').parent().css('display', 'none');
            $('#txtAddress').parent().css('display', 'block');
        }
        else {
            $("input[type='radio']:first").trigger('create').attr("checked", "checked");
            $('#txtAddress').parent().css('display', 'none');
            $('#txtLocationName').parent().css('display', 'block');
        }
        $("input[type='radio']").checkboxradio("refresh");

    };

    //Performs the Search button operation.
    self.searchClick = function () {
        if ($('#popupSearch').parent().hasClass('ui-popup-active')) {
            $('#btnSearch').trigger('click');
        }
        else if ($('#popupDialog').parent().hasClass('ui-popup-active')) {
            $('#okBut').trigger('click');
        }
        else if ($('#popUpLocationProfile').parent().hasClass('ui-popup-active')) {
        }
        else if ($('#popUpConfirmChange').parent().hasClass('ui-popup-active')) {
        }
        else
            $('#btnContinue').trigger('onclick');
    };

    //Validates the search fields and searches the locations 
    self.locationsSearch = function () {
        var store_id = ($('#txtStoreId').val() != "") ? $('#txtStoreId').val() : "";
        store_id = (store_id != $('#txtStoreId').attr('placeholder')) ? store_id : "";
        var store_address = ($('#txtAddress').val() != "") ? $('#txtAddress').val() : "";
        store_address = (store_address != $('#txtAddress').attr('placeholder')) ? store_address : "";
        var search_state = ($('#txtState').val() != "") ? $('#txtState').val() : "";
        search_state = (search_state != $('#txtState').attr('placeholder')) ? search_state : "";
        var search_zip = ($('#txtZipCode').val() != "") ? $('#txtZipCode').val() : "";
        search_zip = (search_zip != $('#txtZipCode').attr('placeholder')) ? search_zip : "";

        var search_city = ($('#txtCity').val() != "") ? $('#txtCity').val() : "";
        search_city = (search_city != $('#txtCity').attr('placeholder')) ? search_city : "";

        //Filter by Brand/Location Name
        var search_brand = ($('#txtBrandName').val() != "") ? $('#txtBrandName').val() : "";
        search_brand = (search_brand != $('#txtBrandName').attr('placeholder')) ? search_brand : "";
        var search_location = ($('#txtBrandFilterLocation').val() != "") ? $('#txtBrandFilterLocation').val() : "";
        search_location = (search_location != $('#txtBrandFilterLocation').attr('placeholder')) ? search_location : "";

        var search_parent_id = ($('#txtParentIdFilterLocation').val() != "") ? $('#txtParentIdFilterLocation').val() : "";
        search_parent_id = (search_parent_id != $('#txtParentIdFilterLocation').attr('placeholder')) ? search_parent_id : "";

        var search_state = "";
        var search_business_name = "";
        if ($('#txtState').parent().css('display') == 'block') {
            search_state = ($('#txtState').val() != "") ? $('#txtState').val() : "";
            search_state = (search_state != $('#txtState').attr('placeholder')) ? search_state : "";
        }
        else {
            search_state = ($('#ddlState').val() != "") ? $('#ddlState').val() : "";
            search_business_name = ($('#txtLocationName').val() != "") ? $('#txtLocationName').val() : "";
            search_business_name = (search_business_name != $('#txtLocationName').attr('placeholder')) ? search_business_name : "";
        }
        var find_all = ($('#chkFindAllLocations').is(':checked')) ? "all" : "";

        var optIn_user = (self.selectedOptInUser.length > 0) ? self.selectedOptInUser.join(',') : "";
        var selected_status = "";
        $.each($('#fldSetStatus').find('input[type="checkbox"]:checked'), function () {
            selected_status += (selected_status != "") ? "," + $(this).val() : $(this).val();
        });

        var msg = "";
        var search_json = {};
        if ($('#txtState').parent().css('display') == 'block') {
            if ((sessionStorage.firstTimeInLocations != undefined && sessionStorage.firstTimeInLocations != null && sessionStorage.firstTimeInLocations != "" && sessionStorage.firstTimeInLocations == "true") && (store_id == "" && store_address == "" && search_city == "" && search_state == "" && search_zip == "" && find_all == "" && optIn_user == "" && selected_status == "" && search_brand != "" && search_location != "" && search_parent_id != ""))
                msg = "Please enter a value for at least one search field.";
            else {
                if (find_all != "") {
                    search_json["all"] = find_all;
                } else {
                    if (store_id != "")
                        search_json["storeId"] = (store_id.endsWith(',')) ? store_id.substring(0, store_id.length - 1) : store_id;
                    if (store_address != "")
                        search_json["address"] = (store_address.endsWith(',')) ? store_address.substring(0, store_address.length - 1) : store_address;
                    if (search_city != "")
                        search_json["city"] = (search_city.endsWith(',')) ? search_city.substring(0, search_city.length - 1) : search_city; //.replace(/, /g, ',');
                    if (search_state != "") {
                        search_state = (search_state.endsWith(',')) ? search_state.substring(0, search_state.length - 1) : search_state;
                        if (search_state.indexOf(',') > -1) {
                            var selected_states = search_state.split(',');
                            search_state = selected_states.join(',').replace(/ /g, '');
                        }
                        search_json["state"] = search_state; //.replace(/, /g, ',');
                    }
                    if (search_zip != "")
                        search_json["zip"] = (search_zip.endsWith(',')) ? search_zip.substring(0, search_zip.length - 1) : search_zip;

                    if (optIn_user != "")
                        search_json["marketManager"] = optIn_user;
                    if (search_brand != "") {
                        search_json["brandName"] = search_brand;
                    }
                    if (search_location != "") {
                        search_json["location"] = search_location;
                    }
                    if (search_parent_id != "") {
                        search_json["parentId"] = search_parent_id;
                    }
                }
                if (selected_status != "")
                    search_json["approvalStatusDesc"] = selected_status;
                self.searchTextExisting = JSON.stringify(search_json);
                self.isPagerDisplayed = false;
                self.pageId('0');
                if (self.gOutputData.jobNumber == -1) {
                    self.ordPageId('0');
                }
                self.getFilterdLocations(JSON.stringify(search_json));
            }
        }
        else if ($('#dvDdlState').css('display') == 'block') {
            if ($("input[type='radio']:first").length > 0 && $("input[type='radio']:first").is(':checked') && search_business_name == "")
                msg = 'Please enter business name or type';
            if (search_city == "")
                msg += '<br /> Please enter the city';
            if (store_address == "" && $("#rbtStreetAddr").is(":checked"))
                msg += '<br /> Please enter Street Address';
            if (search_state == "")
                msg += (msg != "") ? '<br /> Please enter state' : 'Please enter state';
        }
        return msg;
    };

    self.displaySearchTerms = function (search_json, tmp_json) {
        var geographic_search = "";
        var location_search = "";
        var search_term = "";
        $('#spnSearchTerms').html("");
        if (search_json != undefined && search_json != null && search_json != "") {
            if (tmp_json.all != undefined && tmp_json.all != null && tmp_json.all != '')
                search_term = 'All Available';
            if (tmp_json.storeId != undefined && tmp_json.storeId != null && tmp_json.storeId != '')
                search_term = tmp_json.storeId;
            if (tmp_json.address != undefined && tmp_json.address != null && tmp_json.address != '')
                search_term += ($.trim(search_term) != "") ? ", " + tmp_json.address : tmp_json.address;
            if (tmp_json.city != undefined && tmp_json.city != null && tmp_json.city != '')
                search_term += ($.trim(search_term) != "") ? ", " + tmp_json.city : tmp_json.city;
            if (tmp_json.state != undefined && tmp_json.state != null && tmp_json.state != '')
                search_term += ($.trim(search_term) != "") ? ", " + tmp_json.state : tmp_json.state;
            if (tmp_json.zip != undefined && tmp_json.zip != null && tmp_json.zip != '')
                search_term += ($.trim(search_term) != "") ? ", " + tmp_json.zip : tmp_json.zip;

            geographic_search = (search_term != "") ? '<b>Existing- Geography-</b>' + search_term : "";

            if (tmp_json.brandName != undefined && tmp_json.brandName != null && tmp_json.brandName != '')
                location_search += ($.trim(location_search) != "") ? ", " + tmp_json.brandName : tmp_json.brandName;
            if (tmp_json.location != undefined && tmp_json.location != null && tmp_json.location != '')
                location_search += ($.trim(location_search) != "") ? ", " + tmp_json.location : tmp_json.location;

            geographic_search = (location_search != "") ? ((geographic_search != "") ? geographic_search + ' ,<b>Existing- Location-</b>' + location_search : '<b>Existing- Location-</b>' + location_search) : geographic_search;

            if (tmp_json.marketManager != undefined && tmp_json.marketManager != null && tmp_json.marketManager != '')
                geographic_search = (geographic_search != "") ? geographic_search + ' ,<b> Market Managers-</b>' + tmp_json.marketManager : '<b> Market Managers-</b>' + tmp_json.marketManager;
            if (tmp_json.approvalStatusDesc != undefined && tmp_json.approvalStatusDesc != null && tmp_json.approvalStatusDesc != '')
                geographic_search = (geographic_search != "") ? geographic_search + ' ,<b> Status-</b>' + tmp_json.approvalStatusDesc : '<b> Status-</b>' + tmp_json.approvalStatusDesc;
        }
        if (geographic_search != "") {
            $('#spnSearchTerms').html("Search: " + geographic_search);
            $('#spnSearchTerms').css("display", "block");
        }
    };

    //Gets the existing locations based on the given search criteria.
    self.getFilterdLocations = function (search_json) {
        var tmp_json = $.parseJSON(search_json);
        self.displaySearchTerms(search_json, tmp_json);
        var existing_loc_page_id = (sessionStorage.firstTimeInLocations == undefined || sessionStorage.firstTimeInLocations == null || sessionStorage.firstTimeInLocations == "" || sessionStorage.firstTimeInLocations == "false") ? "-1" : self.pageId();
        var search_filter_url = gLocationSearchUrl + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + self.gOutputData.jobNumber + "/" + existing_loc_page_id + "/" + self.ordPageId() + '/' + self.gOutputData.jobTypeId;
        postCORS(search_filter_url, search_json, function (data) {
            //if (jobCustomerNumber == AAG_CUSTOMER_NUMBER) {
            //    data = {
            //        "locations": [{
            //            "pk": "",
            //            "storeId": "",
            //            "location": "Home",
            //            "storeType": "",
            //            "latitude": "42.266883330000",
            //            "longitude": "-88.179155310000",
            //            "address": "27794 N. Darrell Rd.",
            //            "address2": "",
            //            "city": "Wauconda",
            //            "state": "IL",
            //            "zip": "60064",
            //            "areaCode": "847",
            //            "phone": "487-8720",
            //            "firstName": "Mike",
            //            "lastName": "Stanley"
            //        }, {
            //            "pk": "",
            //            "storeId": "",
            //            "location": "Test1",
            //            "storeType": "",
            //            "latitude": "42.179972000000",
            //            "longitude": "-87.926806000000",
            //            "address": "250 Parkway Hiya",
            //            "address2": "Suite Hiya",
            //            "city": "Hiya",
            //            "state": "IL",
            //            "zip": "60069",
            //            "areaCode": "887",
            //            "phone": "626-7576",
            //            "firstName": "Mike",
            //            "lastName": "Stanley"
            //        }, {
            //            "pk": "",
            //            "storeId": "",
            //            "location": "Test2",
            //            "storeType": "",
            //            "latitude": "42.046063000000",
            //            "longitude": "-87.799863400000",
            //            "address": "9040 Waukegan Road",
            //            "address2": "",
            //            "city": "Morton Grove",
            //            "state": "IL",
            //            "zip": "60055",
            //            "areaCode": "",
            //            "phone": "",
            //            "firstName": "Mike",
            //            "lastName": "Stanley"
            //        }],
            //        "totalCount": 3,
            //        "startIndex": 0,
            //        "finalIndex": 3
            //    };

            //}
            self.gDataFiltered = data;

            if (sessionStorage.tempSelectedStores != undefined && sessionStorage.tempSelectedStores != null && sessionStorage.tempSelectedStores != "") {
                var temp_selected_stores = $.parseJSON(sessionStorage.tempSelectedStores);
                $.each(temp_selected_stores, function (key, val) {
                    self.gDataFiltered.locationsSelected.push(val);
                });
            }

            //Inserts ordered locations into job ticket object.
            if (self.gDataFiltered.locationsSelected != undefined && self.gDataFiltered.locationsSelected != null && self.gDataFiltered.locationsSelected.length > 0) {
                //self.gOutputData.selectedLocationsAction.selectedLocationsList = [];
                //self.gOutputData.selectedLocationsAction.selectedLocationsList = self.gDataFiltered.locationsSelected;
                var is_found = false;
                $.each(self.gDataFiltered.locationsSelected, function (loc_key1, loc_val1) {
                    $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (loc_key2, loc_val2) {
                        if (loc_val2.pk == loc_val1.pk) {
                            is_found = true;
                            return false;
                        }
                    });
                    if (!is_found) {
                        self.gOutputData.selectedLocationsAction.selectedLocationsList.push(loc_val1)
                    }
                    is_found = false;
                });
            }
            //self.loadOrderedLocations();

            self.locationsExists = ((self.gDataFiltered.locationsAvailable != undefined && self.gDataFiltered.locationsAvailable != null && self.gDataFiltered.locationsAvailable.length == 0) && self.gDataFiltered.locationsSelected.length == 0) ? true : false;

            //Load Existing Locations
            if (existing_loc_page_id > -1)
                self.loadLocationsWithPaging("e", self.gDataFiltered.locationsAvailable, self.gDataFiltered.availableStartIndex, self.gDataFiltered.availableFinalIndex, self.gDataFiltered.availableTotalCount);
            //else
            //    sessionStorage.firstTimeInLocations = "false";
            //Load Ordered Locations
            self.loadLocationsWithPaging("o", self.gDataFiltered.locationsSelected, self.gDataFiltered.selectedStartIndex, self.gDataFiltered.selectedFinalIndex, self.gDataFiltered.selectedTotalCount);

        }, function (error_response) {

            showErrorResponseText(error_response, false);
        });
    };

    //Initiates the Search/Filter the locations.
    self.filteredLocations = function () {
        if (sessionStorage.firstTimeInLocations == undefined || sessionStorage.firstTimeInLocations == null || sessionStorage.firstTimeInLocations == "" || sessionStorage.firstTimeInLocations == "false") {
            if (self.gOutputData.jobNumber != -1) {
                if ($('#headerSearch').text().toLowerCase().indexOf('new') == -1) {
                    sessionStorage.firstTimeInLocations = "true";
                }
                self.uIViewConfig.n(true);
                self.uIViewConfig.e(true);
                //self.initMapCanvas();
                self.viewPrefChanged();
                if ($('#headerSearch').text().toLowerCase().indexOf('new') > -1)
                    return false;
            }
        }
        window.setTimeout(function () {
            var msg = self.locationsSearch();
            if (msg != "") {
                $('#alertmsg').html(msg);
                $('#popupSearch').popup().popup('close');
                $("#okBut").unbind('click');
                $("#okBut").bind('click', function () {
                    $('#popupDialog').popup().popup('close');
                    window.setTimeout(function openPopSearch() {
                        $('#popupSearch').popup().popup('open');
                    }, 100);
                });
                //$("#okBut").attr("onclick", "$('#popupDialog').popup().popup('close');$('#popupSearch').popup().popup('open');");
                $('#popupDialog').popup().popup('open');
                return false;
            }
            else {
                if (self.map() == undefined || self.map() == null)
                    self.initializeMap();
                if ($('#headerSearch').text().toLowerCase().indexOf('new') > -1) {
                    if (self.isSwapping() == false)
                        self.search('n');
                }
                $('#popupSearch').popup().popup('close');
                $('#dvSearchLocationsMap').css('display', 'block');
                $('#map-canvas').css('display', 'block');
            }
            $('#dvLocationsPrefs input[type="checkbox"]').checkboxradio('refresh');
        }, 500);
    };

    //Gets the page wise existing locations 
    self.getPagingData = function (data, event) {
        var ctrl = event.currentTarget;
        if (!$('#' + ctrl.id).hasClass('ui-disabled')) {
            if (ctrl.id.indexOf("Previous") > -1) {
                if (parseInt(self.pageId()) > 0)
                    self.pageId(parseInt(self.pageId()) - 1);
            }
            else
                if (ctrl.id.indexOf("Next") > -1)
                    self.pageId(parseInt(self.pageId()) + 1);


            self.getFilterdLocations(self.searchTextExisting);
        }
    };

    self.getOrdPagingData = function (data, event) {
        var ctrl = event.currentTarget;
        if (!$('#' + ctrl.id).hasClass('ui-disabled')) {
            if (ctrl.id.indexOf("Previous") > -1) {
                if (parseInt(self.ordPageId()) > 0)
                    self.ordPageId(parseInt(self.ordPageId()) - 1);
            }
            else
                if (ctrl.id.indexOf("Next") > -1)
                    self.ordPageId(parseInt(self.ordPageId()) + 1);


            self.getFilterdLocations(self.searchTextExisting);
        }
    };

    //Gets the page wise ordered locations from session.
    self.getOrdPagingDataOld = function (data, event) {
        var ctrl = event.currentTarget;
        var start = 0;
        var end = 0;
        if (!$('#' + ctrl.id).hasClass('ui-disabled')) {
            start = (parseInt(self.ordPageId()) > 0) ? parseInt(self.ordPageId()) * 50 : 0;
            end = start + 50;
            if (ctrl.id.indexOf("Previous") > -1) {
                if (parseInt(self.ordPageId()) > 0)
                    self.ordPageId(parseInt(self.ordPageId()) - 1);
            }
            else
                if (ctrl.id.indexOf("Next") > -1)
                    self.ordPageId(parseInt(self.ordPageId()) + 1);

            var page_id = Math.floor(self.gOutputData.selectedLocationsAction.selectedLocationsList.length / 50);
            if ((self.gOutputData.selectedLocationsAction.selectedLocationsList.length % 50) == 0)
                page_id = page_id - 1;
            if (self.ordPageId() > page_id)
                self.ordPageId(page_id);

            //Get paging data for ordered locations  -- START
            self.getOrderedLocationsPaged();
            start = (parseInt(self.ordPageId()) > 0) ? parseInt(self.ordPageId()) * 50 : 0;
            end = start + 50;

            if (self.selectedLocationsPaged().length >= 6) {
                var total_locations = $('#spnTotalOrdLocations').text();
                $('#dvOrderedLocationsContainer').css("height", ((total_locations > 50) ? '470px' : '523px'));
                $('#dvOrderedLocationsContainer').css("overflow-y", "scroll");
            }
            else {
                $('#dvOrderedLocationsContainer').css('height', 'auto');
                $('#dvOrderedLocationsContainer').css('overflow-y', 'auto');
            }
            $('#ulSelectedLocations').listview().listview('refresh').trigger("create");
            //Get paging data for ordered locations  -- END
        }
    };

    //Checks the selected location is already exists in the target set or not.
    self.isStoreExistsInList = function (temp_selected_locations, temp_selected_location) {
        var is_exists = false;
        $.each(temp_selected_locations, function (a, b) {
            if ((b.pk != "") && (b.pk === temp_selected_location.pk)) {
                is_exists = true;
                return false;
            }
        });
        return is_exists;
    };

    //Gets the index of the location in the target set.
    self.getObjectIndex = function (temp_selected_locations, temp_selected_location) {
        var selected_index = "";
        $.each(temp_selected_locations, function (a, b) {
            if (b.pk === temp_selected_location.pk) {
                selected_index = a;
                return false;
            }
        });
        return selected_index;
    };

    //Cancels update operation and closes the location profile popup. 
    self.cancelPopUp = function () {
        var temp_location = ko.mapping.toJS(self.editLocationInfo(), mapping);
        var selected_index = $('#btnProfilePopupCancel').attr('selectedIndex');
        var temp_locations = ko.mapping.toJS(self.locations, mapping);
        if (selected_index == undefined || selected_index == null || selected_index == "") selected_index = self.getObjectIndex(temp_locations, temp_location);
        //self.editLocationInfo(new locationModel1(ko.mapping.toJS(self.locationPreviousInfo(), mapping), selected_index));
        var temp_location = ko.toJS(self.editLocationInfo);
        self.editLocationInfo(new locationModel1(ko.mapping.toJS(self.locationPreviousInfo(), mapping), self.editLocationInfo().locationIndex.substring(0, self.editLocationInfo().locationIndex.indexOf('-'))));
        self.editLocationInfo().locationMarker(temp_location.locationMarker);
        if ($('#btnPopUpAddUpdate').text().indexOf('Add') > -1)
            $.each(self.editLocationInfo(), function (key, val) {
                if (key.indexOf('mapping') == -1 && val.getDependenciesCount == undefined && val.getDependenciesCount == null)
                    if (key != 'locationIndex')
                        self.searchedLocations()[selected_index][key](val());
                    else
                        self.searchedLocations()[selected_index][key] = val;
            });
        else if ($('#btnPopUpAddUpdate').text().indexOf('Remove from Order') > -1) {
            $('#popUpLocationProfile').popup().popup('close');
            return;
        }
        else
            $.each(self.editLocationInfo(), function (key, val) {
                if (key.indexOf('mapping') == -1 && val.getDependenciesCount == undefined && val.getDependenciesCount == null)
                    if (key != 'locationIndex')
                        self.locations()[selected_index][key](val());
                    else
                        self.locations()[selected_index][key] = val;
            });
        $('#btnProfilePopupCancel').removeAttr('selectedIndex');
        $('#popUpLocationProfile').popup().popup('close');
    };

    //Gets the ordered locations based on the selected page in pager.
    self.getOrderedLocationsPaged = function () {
        var range_x = (parseInt(self.ordPageId()) == 0) ? 1 : ((parseInt(self.ordPageId()) * 50) + 1);
        //var ord_locations = self.selectedLocationsAdded().slice((range_x - 1), (range_x + 50) - 1);
        //self.selectedLocationsPaged.removeAll();
        //self.selectedLocationsPaged(ord_locations);
        var page_range = "";
        //var range_y = (parseInt(self.ordPageId()) == 0) ? (((self.selectedLocationsPaged().length < 50) ? self.selectedLocationsPaged().length : 50)) : ((parseInt(self.ordPageId()) * 50) + ((self.selectedLocationsPaged().length < 50) ? self.selectedLocationsPaged().length : 50));
        var range_y = self.selectedLocationsAdded().length;

        page_range = range_x + ' - ' + range_y;

        //self.clearMarkers('o');

        //var current_markers = $.grep(self.markers_array, function (obj) {
        //    return obj.type === 'o';
        //});

        var search_string = "";
        var latLngPts = [];
        //$.each(self.selectedLocationsPaged(), function (key, val) {
        //    search_string += (search_string != '') ? '|' + ((val.storeCompleteAddress() != "") ? val.storeCompleteAddress() : '') : (val.storeCompleteAddress() != "") ? val.storeCompleteAddress() : '';
        //    search_string += (val.computedStoreName() != "") ? '^' + val.computedStoreName() : '^';
        //    search_string += ((val.phone != undefined && val.phone != null && val.phone() != "") ? '^' + val.phone() : '^');
        //    search_string += '^' + key + '-' + ((val["storeId"] != undefined && val["storeId"] != null) ? val.storeId() : '');
        //    val.locationIndex = key + '-' + ((val["storeId"] != undefined && val["storeId"] != null) ? val.storeId() : '');
        //    latLngPts.push({ 'lat': val.latitude(), 'lng': val.longitude() });
        //    current_markers[0].addresses.push(search_string);
        //    search_string = "";
        //});
        //self.displayMarkers(latLngPts, 'o');

        if (self.selectedLocationsAdded().length <= 50 || self.selectedLocationsAdded().length == range_y)
            $('#btnOrdNext').addClass('ui-disabled');
        else
            $('#btnOrdNext').removeClass('ui-disabled');

        if (self.ordPageId() == 0)
            $('#btnOrdPrevious').addClass('ui-disabled');
        else
            $('#btnOrdPrevious').removeClass('ui-disabled');

        $('#spnOrdPageId').text(parseInt(self.ordPageId()) + 1);

        //var total_pages = Math.ceil(self.gOutputData.selectedLocationsAction.selectedLocationsList.length / 50);
        var total_pages = Math.ceil(((self.gDataFiltered.locationsSelected != undefined && self.gDataFiltered.locationsSelected != null && self.gDataFiltered.locationsSelected != "") ? self.gDataFiltered.locationsSelected.length : self.selectedLocationsAdded().length) / 50);

        total_pages = Math.ceil(((self.gDataFiltered.selectedTotalCount == undefined || self.gDataFiltered.selectedTotalCount == null || self.gDataFiltered.selectedTotalCount == "" || self.gDataFiltered.selectedTotalCount == 0) ? self.selectedLocationsAdded().length : self.gDataFiltered.selectedTotalCount) / 50);

        $('#spnOrdTotalPages').text(total_pages);
        //$('#spnOrdTotalLocations').text(self.gOutputData.selectedLocationsAction.selectedLocationsList.length);
        $('#spnOrdTotalLocations').text((self.gDataFiltered.selectedTotalCount == undefined || self.gDataFiltered.selectedTotalCount == null || self.gDataFiltered.selectedTotalCount == "" || self.gDataFiltered.selectedTotalCount == 0) ? self.selectedLocationsAdded().length : self.gDataFiltered.selectedTotalCount);
        //$('#spnTotalOrdLocations').text((self.gDataFiltered.selectedTotalCount == 0) ? self.gDataFiltered.locationsSelected.length : self.gDataFiltered.selectedTotalCount);
        $('#spnTotalOrdLocations').text(self.gDataFiltered.selectedTotalCount || self.selectedLocationsAdded().length);
        $('#spnOrdLocRange').text(page_range);
        if (self.selectedLocationsAdded().length > 50) {
            //$('#dvOrderedLocPaging').css('display', 'block');
            //self.isPagerDisplayed = true;
        }
        else {
            $('#dvOrderedLocPaging').css('display', 'none');
        }

        if (self.selectedLocationsAdded().length > 4) {
            var total_locations = $('#spnTotalOrdLocations').text();
            $('#dvOrderedLocationsContainer').css("height", ((total_locations > 50) ? '470px' : '523px'));
            $('#dvOrderedLocationsContainer').css("overflow-y", "scroll");
        }
        else {
            $('#dvOrderedLocationsContainer').css('height', 'auto');
            $('#dvOrderedLocationsContainer').css('overflow-y', 'auto');
        }
    };

    //Fires when the map tiles loaded.
    self.tilesLoaded = function () {
        if (self.isSwapping() == false) {
            self.isFirstTime(false);
            self.search('n'); // To be implemented
        }
        google.maps.event.clearListeners(map, 'tilesloaded');
    };

    //Initializes the map and loads the ordered locations from the session.
    self.initializeMap = function () {
        var service = new google.maps.places.PlacesService(map);
        self.map(map);
        self.service(service);
        self.geoCoder(geocoder);
        if (self.isSwapping() == false) google.maps.event.addListener(self.map(), 'tilesloaded', self.tilesLoaded);
        //self.isFirstTime(false);
        self.search('n'); // To be implemented


        google.maps.event.addListener(self.map(), 'mousedown', function getNewPosition(event) {
            self.newPosition(event.latLng);
        });

        $('#btnRefresh').on('dblclick', function (event) {
            event.preventDefault();
            event.preventDefault();
            return false;
        });

        $("#btnRefresh").on('click', function (event) {
            event.preventDefault();
            if (!$(this)[0].disabled) {
                $(this).prop('disabled', true);
                //do something
                window.setTimeout(function delaySearch() {
                    self.search('n');
                }, 800);
            }
        });
        if (self.locations().length > 0 || self.selectedLocationsAdded().length > 0 || self.searchedLocations().length > 0) {
            $('#mapBlock').css('display', 'block');
            $('#mapBlock').css('visibility', 'visible');
        }
        else {
            $('#mapBlock').css('display', 'block');
            $('#mapBlock').css('visibility', 'hidden');
        }
        //self.loadOrderedLocations();
        //Ordered Locations code - Previous
        //if (sessionStorage.firstTimeInLocations == undefined || sessionStorage.firstTimeInLocations == null || sessionStorage.firstTimeInLocations == "" || sessionStorage.firstTimeInLocations == "false")
        // pageObj.locationsSearch();
    };

    //We may discard
    self.loadOrderedLocations = function () {
        var temp_selected_locations = [];
        if (self.gOutputData != null && self.gOutputData.selectedLocationsAction != undefined) {
            selected_locations = (self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != null) ? self.gOutputData.selectedLocationsAction.selectedLocationsList : [];
            var search_string = "";
            if (selected_locations.length > 0) {
                var current_markers = $.grep(self.markers_array, function (obj) {
                    return obj.type === 'o';
                });
                var latLngPts = [];
                $.each(selected_locations, function (a, b) {
                    if (b["needsCount"] == undefined || b["needsCount"] == null)
                        b["needsCount"] = false;
                });
                $.each(selected_locations, function (a, b) {
                    var temp_loc = new locationModel1(self.constructLocationModelForExistingJobs(b), a);
                    temp_selected_locations.push(temp_loc);
                });

                $('#dvSearchLocationsMap').css('display', 'block');
                // this.displayMarkers(latLngPts, 'o');
                if (search_string != "") {
                    $('#dvSearchLocations').css('display', 'block');
                    $('#dvSearchLocationsMap').css('display', 'block');
                    $("#dvNewLocations").css('display', 'block');
                    $('#map-canvas').css('display', 'block');
                    google.maps.event.trigger(self.map(), 'resize');
                }
            }
            self.selectedLocationsAdded(temp_selected_locations);
            if (self.selectedLocationsAdded().length > 0) {
                if (self.gOutputData.jobTypeId == 71 || self.gOutputData.jobTypeId == 72)
                    $('#fldGrpButPanel').css('display', 'none');
                else
                    $('#fldGrpButPanel').css('display', 'block');
            }
            else {
                $('#fldGrpButPanel').css('display', 'none');
            }
            self.getOrderedLocationsPaged();
        }

        if (self.locations().length > 0 || self.selectedLocationsAdded().length > 0 || self.searchedLocations().length > 0) {
            $('#mapBlock').css('display', 'block');
            $('#mapBlock').css('visibility', 'visible');
        }
        else {
            $('#mapBlock').css('display', 'block');
            $('#mapBlock').css('visibility', 'hidden');
        }
    };

    self.loadLocationsWithPaging = function (location_type, locations_list, start_index, final_index, total_count) {
        if (locations_list.length > 0) {
            self.clearMarkers(location_type);
            if (location_type == "e")
                self.locations.removeAll();
            else if (location_type == "o")
                self.selectedLocationsAdded.removeAll();

            var search_string = "";
            if (locations_list != undefined && locations_list != null && locations_list.length > 0) {
                var index = 0;
                var latLngPts = [];
                var current_markers = $.grep(self.markers_array, function (obj) {
                    return obj.type === location_type;
                });
                $.each(locations_list, function (a, b) {
                    b["needsCount"] = (b["needsCount"] != undefined && b["needsCount"] != null && b["needsCount"] != "") ? b["needsCount"] : true;
                    b["isNewStore"] = false;
                    self.loc_lst.push(new locationModel1(b, a));
                    search_string += (search_string != '') ? '|' + ((self.loc_lst[a].storeCompleteAddress() != "") ? self.loc_lst[a].storeCompleteAddress() : '') : (self.loc_lst[a].storeCompleteAddress() != "") ? self.loc_lst[a].storeCompleteAddress() : '';
                    search_string += (self.loc_lst[a].computedStoreName() != "") ? '^' + self.loc_lst[a].computedStoreName() : '^';
                    search_string += ((self.loc_lst[a].phone != undefined && self.loc_lst[a].phone != null && self.loc_lst[a].phone() != "") ? '^' + self.loc_lst[a].phone() : '^');
                    search_string += '^' + self.loc_lst[a].locationIndex; // +'^';
                    search_string += '^' + self.loc_lst[a].status();
                    latLngPts.push({ 'lat': self.loc_lst[a].latitude(), 'lng': self.loc_lst[a].longitude() });
                    current_markers[0].addresses.push(search_string);
                    search_string = "";
                    if (location_type == 'e' && locations_list.length == self.loc_lst.length) {
                        self.locations(self.loc_lst);
                        self.makeExistingAndOrdLocationsUI("dvExistingLocationsContainer", location_type, latLngPts, total_count);

                    }
                    else if (location_type == 'o' && locations_list.length == self.loc_lst.length) {
                        self.selectedLocationsAdded(self.loc_lst);
                        self.makeExistingAndOrdLocationsUI("dvOrderedLocationsContainer", location_type, latLngPts, total_count);
                    }
                });
                self.loc_lst = [];
            }
            self.createExistingAndOrdLocationsPager(location_type, locations_list, start_index, final_index, total_count);
            manageDemoHintsPopups();
        }
        else {
            if (location_type == "e" && self.locationsExists) {
                self.isFirstTimeExisting(false);
                self.isPagerDisplayed = false;
                self.locations.removeAll();
                $('#dvPaging').css('display', 'none');
                $('#dvExistingLocationsContainer').css('height', 'auto');
                $('#dvExistingLocationsContainer').css('overflow-y', 'auto');
                $('#alertmsg').html('No available locations could be found. Click "Add New Locations" to load a new location to the site.');
                $("#okBut").unbind('click');
                $("#okBut").bind('click', function () {
                    $("#okBut").unbind('click');
                    $("#okBut").attr('onclick', '$(\'#popupDialog\').popup().popup(\'close\');')
                    $('#popupDialog').popup().popup('close');
                    window.setTimeout(function () {
                        $('#popupSearch').popup().popup('open');
                    }, 200);
                });
                // $("#okBut").attr("onclick", "$('#popupDialog').popup().popup('open');$('#popupDialog').popup().popup('close');");
                window.setTimeout(function () {
                    $('#popupDialog').popup().popup('open');
                }, 100);

            } else if (location_type == "e") {
                isPagerDisplayed = false;
                self.locations.removeAll();
                $('#dvPaging').css('display', 'none');
                $('#dvExistingLocationsContainer').css('height', 'auto');
                $('#dvExistingLocationsContainer').css('overflow-y', 'auto');
            }
            else if (location_type == "o") {
                isOrdPagerDisplayed = false;
                self.selectedLocationsAdded.removeAll();
                $('#dvOrderedLocationsContainer').css('height', 'auto');
                $('#dvOrderedLocationsContainer').css('overflow-y', 'auto');
                $('#dvOrderedLocPaging').css('display', 'none');
            }
            self.clearMarkers(location_type);
            $('#popupSearch').popup().popup('close');

        }
        if (location_type == "e")
            $('#ulAvailableLocations').listview().listview('refresh');
        else if (location_type == "o")
            $('#ulSelectedLocations').listview().listview('refresh');
        //self.loc_lst.removeAll();
    };

    self.makeExistingAndOrdLocationsUI = function (containter, location_type, latLngPts, total_count) {
        $('#dvSearchLocationsMap').css('display', 'block');
        $('#map-canvas').css('display', 'block');
        self.displayMarkers(latLngPts, location_type);
        if (self.loc_lst.length > 6) {
            $('#' + containter).css('height', ((total_count > 50) ? '470px' : '523px'));
            $('#' + containter).css('overflow-y', 'scroll');
        }
        else {
            $('#' + containter).css('height', 'auto');
            $('#' + containter).css('overflow-y', 'auto');
        }
        $('#' + containter).css('display', 'block');
        $('#mapBlock').css('visibility', 'visible');
        if (location_type == "e")
            self.isFirstTimeExisting(false);
        $('#popupSearch').popup().popup('close');
        $('#ulAvailableLocations').listview().listview('refresh').trigger("create");
        $('#ulSelectedLocations').listview().listview('refresh').trigger("create");
        self.searchText('');
        if (!self.isSwapping())
            self.clearSearchFields();
    };

    self.createExistingAndOrdLocationsPager = function (location_type, locations_list, start_index, final_index, total_count) {
        var page_id = (location_type == "e") ? self.pageId() : self.ordPageId();
        if ((final_index < total_count) || page_id > 0) {
            if (location_type == "e" && self.isPagerDisplayed == false) {
                $('#dvPaging').css('display', 'block');
                self.isPagerDisplayed = true;
            }
            else if (location_type == "o") {
                $('#dvOrderedLocPaging').css('display', 'block');
            }
        }
        else {
            if (location_type == "e" && self.isPagerDisplayed == false) {
                $('#dvPaging').css('display', 'none');
            }
            else if (location_type == "o") {
                $('#dvOrderedLocPaging').css('display', 'none');
            }
        }

        var page_range = "";
        if (location_type == "e") {
            page_range = (start_index + 1) + ' - ' + final_index;
        }
        else if (location_type == "o") {
            page_range = (start_index + 1) + ' - ' + ((final_index == 0) ? ((locations_list.length > 50) ? 50 : locations_list.length) : final_index);
        }
        var page_count = 0;
        if (location_type == "e") {
            page_count = Math.ceil(total_count / 50);
            $('#spnTotalPages').text(page_count);
            $('#spnTotalLocations').text(total_count);
        }
        else if (location_type == "o") {
            page_count = Math.ceil(((total_count == 0) ? locations_list.length : total_count) / 50);
            $('#spnOrdTotalPages').text(page_count);
            $('#spnOrdTotalLocations').text((total_count == 0) ? locations_list.length : total_count);
            //$('#spnTotalOrdLocations').text((total_count == 0) ? locations_list.length : total_count);
            $('#spnTotalOrdLocations').text(total_count);
        }
        if ((final_index == total_count) != 0) {
            if (location_type == "e") {
                $('#btnNext').addClass('ui-disabled');
                $('#spnPageId').text(parseInt(self.pageId()) + 1);
                $('#spnLocationsRange').text(page_range);
            }
            else if (location_type == "o") {
                $('#btnOrdNext').addClass('ui-disabled');
                $('#spnOrdPageId').text(parseInt(self.ordPageId()) + 1);
                $('#spnOrdLocRange').text(page_range);
            }
        }
        else {
            if (location_type == "e") {
                $('#btnNext').removeClass('ui-disabled');
                $('#spnLocationsRange').text(page_range);
                $('#spnPageId').text(parseInt(self.pageId()) + 1);
            }
            else if (location_type == "o") {
                $('#btnOrdNext').addClass('ui-disabled');
                $('#spnOrdPageId').text(parseInt(self.ordPageId()) + 1);
                $('#spnOrdLocRange').text(page_range);
            }
        }

        if (parseInt(self.pageId()) == 0 && location_type == "e") {
            $('#btnPrevious').addClass('ui-disabled');
            $('#spnPageId').text(parseInt(self.pageId()) + 1);
            $('#spnLocationsRange').text(page_range);
        }
        else if (parseInt(self.pageId()) > 0 && location_type == "e") {
            $('#btnPrevious').removeClass('ui-disabled');
            $('#spnPageId').text(parseInt(self.pageId()) + 1);
            $('#spnLocationsRange').text(page_range);
        }

        if (parseInt(self.ordPageId()) == 0 && location_type == "o") {
            $('#btnOrdPrevious').addClass('ui-disabled');
            if (page_count > 1 && (parseInt(self.ordPageId()) + 1) < page_count) $('#btnOrdNext').removeClass('ui-disabled');
            $('#spnOrdPageId').text(parseInt(self.ordPageId()) + 1);
            $('#spnOrdLocRange').text(page_range);
        }
        else if (parseInt(self.ordPageId()) > 0 && location_type == "o") {
            $('#btnOrdPrevious').removeClass('ui-disabled');
            if (page_count > 1 && (parseInt(self.ordPageId()) + 1) < page_count) $('#btnOrdNext').removeClass('ui-disabled');
            $('#spnOrdPageId').text(parseInt(self.ordPageId()) + 1);
            $('#spnOrdLocRange').text(page_range);
        }
    };
};