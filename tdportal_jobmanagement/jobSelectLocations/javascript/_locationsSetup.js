﻿var mapping = { 'ignore': ["__ko_mapping__"] }

//******************** Location Model Start **************************
//View model of a location
var locationModel1 = function (data, index) {
    var self = this;
    data.quantity = ((data.quantity != undefined && data.quantity != null && data.quantity != '') ? setDigitsForGrid(data.quantity) : '');
    $.each(data, function (a, b) {
        data[a] = $.trim(b);
    });
    ko.mapping.fromJS(data, {}, self);
    self.computedStoreName = ko.computed(function () {
        deferEvaluation: true
        var full_name = "";
        var store_type = '';
        var store_location = '';
        var store_id = '';
        if (self.storeType != undefined && self.storeType != null && $.parseJSON(sessionStorage.locationConfig).hidden["storeType"] == undefined || ($.parseJSON(sessionStorage.locationConfig).hidden["storeType"] != undefined && $.parseJSON(sessionStorage.locationConfig).hidden["storeType"] == null))
            store_type = (($.trim(self.storeType()) != "") ? self.storeType() + ' - ' : "");
        //else if (self.location != undefined && self.location != null && $.parseJSON(sessionStorage.locationConfig).hidden["location"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["location"] == null)
        if (self.location != undefined && self.location != null && $.parseJSON(sessionStorage.locationConfig).hidden["location"] == undefined || ($.parseJSON(sessionStorage.locationConfig).hidden["location"] != undefined && $.parseJSON(sessionStorage.locationConfig).hidden["location"] == null))
            store_location = (($.trim(self.location()) != "") ? self.location() : "");
        if (self.storeId != undefined && self.storeId != null && $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == undefined || ($.parseJSON(sessionStorage.locationConfig).hidden["storeId"] != undefined && $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == null)) {
            store_id = (self.storeId() != "") ? self.storeId() + ' - ' : "";
        }
        if (self.firstName != undefined && self.firstName != null && self.lastName != undefined && self.lastName != null) {
            store_id = self.firstName() + ' ' + self.lastName() + ' - ';
        }

        full_name = store_id + store_type + store_location
        full_name = ((full_name.trim().endsWith('-')) ? (full_name.trim().substring(0, full_name.trim().length - 1).trim()) : full_name);
        full_name = full_name + ((self.marketManager != undefined && self.marketManager != null && self.marketManager !="" && self.marketManager() != undefined && self.marketManager() != null && self.marketManager() != "") ? ' (' + self.marketManager() + ')' : '');
        return full_name;
    });

    self.storeCompleteAddress = ko.computed(function () {
        var full_address = "";
        if (self.address != undefined && self.address != null) {
            full_address = self.address();
        }
        if (self.address2 != undefined && self.address2 != null && self.address2() != "") {
            full_address = (full_address != "") ? (full_address + ', ' + self.address2()) : self.address2();
        }
        if (self.city != undefined && self.city != null) {
            full_address = (full_address != "") ? ((!full_address.endsWith(',') ? full_address + ', ' : full_address + '') + self.city()) : self.city();
        }
        if (self.state != undefined && self.state != null) {
            full_address = (full_address != "") ? ((!full_address.endsWith(',') ? full_address + ', ' : full_address + '') + self.state()) : self.state();
        }
        if (self.zip != undefined && self.zip != null) {
            full_address = (full_address != "") ? (full_address + ' ' + self.zip()) : self.zip();
        }
        return full_address;
    });
    self.quantity = ko.observable(((data.quantity != undefined && data.quantity != null && data.quantity != '') ? setDigitsForGrid(data.quantity) : ''));
    self.isQuantityRequired = ko.observable(((jobCustomerNumber != CASEYS_CUSTOMER_NUMBER && jobCustomerNumber != REGIS_CUSTOMER_NUMBER && jobCustomerNumber != JETS_CUSTOMER_NUMBER && jobCustomerNumber != ALLIED_CUSTOMER_NUMBER && jobCustomerNumber != AAG_CUSTOMER_NUMBER && jobCustomerNumber != DCA_CUSTOMER_NUMBER && jobCustomerNumber != KUBOTA_CUSTOMER_NUMBER && jobCustomerNumber != SK_CUSTOMER_NUMBER && jobCustomerNumber != GWA_CUSTOMER_NUMBER) ? true : false));
    self.locationIndex = index + '-' + data.storeId;
    self.locationMarker = ko.observable('');
}

var locationsGroupModel = function (group_name, group_locations) {
    var self = this;
    self.groupName = ko.observable(group_name);
    self.locations = ko.observableArray(group_locations);
};

var loanOfficer = function (user_id, first_name, last_name) {
    var self = this;
    self.loanOfficerId = user_id;
    self.loanOfficerName = first_name + ' ' + last_name;
};

var locationUpdateModel = function (store_id, store_pk, update_type) {
    var self = this;
    self.storeId = store_id;
    self.pk = store_pk;
    self.update = update_type;
};

var statusInfo = function (status_key, status_count)
{
    var self = this;
    self.statusKey = status_key;
    self.statusCount = status_count;
}

var optInUser = function (user_name, count) {
    var self = this;
    self.count = count;
    self.userName = user_name;
    self.computedUserNameWithCount = ko.computed(function () {
        return user_name + " (" + count + ")";
    });
};

//******************** Location Model End **************************

//******************** Locations View Model start **************************
function buildNewStores() {
    var self = this;
    //******************** Global Variables Start **************************
    self.gDataFiltered;
    self.gOutputData;
    self.selectedLocations = [];
    self.validationControls = [];
    self.loanOffiersList = ko.observableArray([]);
    self.selectedLoanOfficer = ko.observable({});
    self.optInUsersList = ko.observableArray([]);
    self.statusList = ko.observableArray([]);
    self.selectedOptInUser = [];
    self.grpLoc_lst = [];
    self.updateLocations = [];
    self.uIViewConfig = {};
    self.uIViewConfig["e"] = ko.observable(true);
    self.uIViewConfig["o"] = ko.observable(true);
    self.uIViewConfig["m"] = ko.observable(true);
    self.uIViewConfig["n"] = ko.observable(true);
    self.isSwapping = ko.observable(false);
    self.newSearchText = ko.observable({});
    //self.service;
    //******************** Global Variables End **************************
    self.loc_lst = [];
    var selected_locations = [];
    var edit_location = [];
    self.markers_array = [];
    var markers1 = [];
    self.markers_array.push({
        type: "e",
        markers: [],
        addresses: []
    });
    self.markers_array.push({
        type: "n",
        markers: [],
        addresses: []
    });
    self.markers_array.push({
        type: "o",
        markers: [],
        addresses: []
    });
    self.markers_array.push({
        type: "g",
        markers: [],
        addresses: []
    });

    if (sessionStorage.loanOffiersList != undefined && sessionStorage.loanOffiersList != null && sessionStorage.loanOffiersList != "") {
        var lo_list = [];
        lo_list = $.parseJSON(sessionStorage.loanOffiersList);
        $.each(lo_list, function (key, val) {
            if (key == 0) {
                self.selectedLoanOfficer(val.userId);
            }
            self.loanOffiersList.push(new loanOfficer(val.userId, val.firstName, val.lastName));
        });
    }

    //Loads the job ticket json into local object and display the counts of uploaded files (artwork/list) and count of locations selected/ordered.
    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput == undefined || sessionStorage.jobSetupOutput == null) {
            window.setTimeout(function delay() {
                $.getJSON(gOutputServiceUrl, function (dataOutput) {
                    self.gOutputData = dataOutput;
                    if (self.gOutputData["milestoneAction"] == undefined) self.gOutputData["milestoneAction"] = {};
                    self.gOutputData.milestoneAction["milestoneDict"] = {};
                });
            }, 500);
        }
        else {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            if (self.gOutputData.jobNumber != -1) {
                //if (sessionStorage.firstTimeInLocations == undefined || sessionStorage.firstTimeInLocations == null || sessionStorage.firstTimeInLocations == "" || sessionStorage.firstTimeInLocations == "false") {
                    self.uIViewConfig.n(false);
                    self.uIViewConfig.e(false);
                //}
            }
        }
        if (self.gOutputData != null) {
            getBubbleCounts(self.gOutputData);
            if (self.gOutputData.jobTypeId == 71 || self.gOutputData.jobTypeId == 72 || self.gOutputData.jobTypeId == 52) {
                $('#imgMapPaddleKey').css("visibility", "visible");
            }
        }
    };
    self.makeGOutputData();

    var is_map_move = false;
    self.map = ko.observable(map);
    self.infowindow = ko.observable();
    self.service = ko.observable();
    self.geoCoder = ko.observable();
    self.newPosition = ko.observable();
    self.editLocationInfo = ko.observable({});
    self.states = statesJSON;
    self.locations = ko.observableArray([]);
    self.gDataFilteredGroup = ko.observableArray([]);
    self.searchText = ko.observable('');
    self.locationPreviousInfo = ko.observable({});
    self.previousZoomLevel = "";
    self.locationsNeedToReview = [];
    self.searchTextExisting = "";
    self.pageId = ko.observable('0');
    self.isPagerDisplayed = false;
    self.isOrdPagerDisplayed = false;
    self.ordPageId = ko.observable('0');
    self.selectedLocationsAdded = ko.observableArray([]);
    self.selectedLocationsPaged = ko.observableArray([]);
    self.searchedLocations = ko.observableArray([]);
    self.isFirstTimeExisting = ko.observable(true);
    self.isFirstTime = ko.observable(false);
    self.headerText = ko.observable((jobCustomerNumber == CASEYS_CUSTOMER_NUMBER || jobCustomerNumber == JETS_CUSTOMER_NUMBER) ? "Open Locations" : "Available Locations");
    self.isEditable = ko.observable(self.gOutputData.jobTypeId != "70");

};
//******************** Locations View Model start **************************