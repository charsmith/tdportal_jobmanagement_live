﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
//var gServiceInstructionsUrl = 'http://localhost:49768/tdportal_jobmanagement_dev_29102013/mergePurge/JSON/_jobMergePurgeInstructions.JSON';
//var gServiceMergePurgeUrl = 'http://localhost:49768/tdportal_jobmanagement_dev_29102013/mergePurge/JSON/_jobMergePurge.JSON';
//var gServiceMergePopup = 'http://localhost:49768/tdportal_jobmanagement_dev_29102013/mergePurge/JSON/_jobMergePurgePopup.JSON';
var gServiceInstructionsUrl = '../mergePurge/JSON/_jobMergePurgeInstructions.JSON';
var gServiceMergePurgeUrl = '../mergePurge/JSON/_jobMergePurge.JSON';
var gServiceMergePopup = '../mergePurge/JSON/_jobMergePurgePopup.JSON';
var gData;
var gDataMergePurge;
var gDataPopup;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_jobMergePurge', function (event) {
    makeGData();
});

$('#_jobMergePurge').live('pagebeforecreate', function (event) {
    displayMessage('_jobMergePurge');
    displayNavLinks();
    confirmMessage();
    //test for mobility...
    loadMobility();

    if (getSessionData("userRole") == "user" || getSessionData("userRole") == "power")
        $("#dvMergePurgeSetup").hide();
    else
        $("#dvMergePurgeSetup").show();
});
//******************** Page Load Events End **************************

//******************** Public Functions Start **************************
function makeGData() {
    $.getJSON(gServiceInstructionsUrl, function (data) {
        gData = data;
        createTable();

        $.getJSON(gServiceMergePurgeUrl, function (data) {
            gDataMergePurge = data;
            buildLists();

            $.getJSON(gServiceMergePopup, function (data) {
                gDataPopup = data;
            });
        });
    });

}

function buildLists() {

    gDataMergePurge.steps = gDataMergePurge.steps.sort(function (a, b) {
        return parseInt(a.index) > parseInt(b.index) ? 1 : -1;
    });

    $("#ulContent").empty();
    var counter = 0;
    $.each(gDataMergePurge.steps, function (key_data, val_data) {
        //if (key_method == "steps") {
        //    $.each(val_method, function (key_data, val_data) {                
        var method = "";
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var display_name = '';
        $.each(val_data.method, function (key, value) {
            if (key > 0 && value.match(PATTERN))
                display_name += ' ' + value;
            else
                display_name += value;
        });
        method = display_name.substring(0, 1).toUpperCase() + display_name.substring(1);
        var li = '<li><a href="#" onclick="fnOpenPopup(\'' + val_data.method + '\', \'' + val_data.index + '\')"><h3>' + method + '</h3>'
        //if (val_data == "step")
        //{
        //$.each(val_data.step, function (key_col, val_col)
        //{
        //if (val_data.step["column"] != undefined && val_data.step["column"] != "") 
        // li += '<p>' + val_data.step["column"] + ' : ' + val_data.step["value"] + '</p>'
        //else
        //    li += '<p><b>Click to Setup Options</b></p>'
        li += (val_data.step["column"] != undefined && val_data.step["column"] != "") ? '<p>' + val_data.step["column"] + ' : ' + val_data.step["value"] + '</p>' : '<p><b>Click to Setup Options</b></p>';
        //});
        //}
        li += '<span class="ui-li-count"> Index' + val_data["index"] + '</span></a>';
        li += '<a href="#" onclick="fnRemoveMethod(\'' + counter + '\');">Remove Method</a></li>';

        $("#ulContent").append(li);
        $("#ulContent").listview("refresh");
        counter++;
        //    });            
        //}
    });
}

function createTable() {
    $("#instructionsHeader").show();
    var count = $("#ulInstructions").find('textarea[id^=taInstructionsDescription]').length;
    var data = '';
    count += 1;

    data += '<li data-role="fieldcontain" id="liInstructions' + count + '"><a>';
    data += '<div data-role="fieldcontain"><label for="selInstructionsFilter' + count + '" class="select">Filter:</label>';
    data += '<select name="selInstructionsFilter' + count + '" id="selInstructionsFilter' + count + '" data-mini="true"></select></div>';
    data += '<div data-role="fieldcontain"><label label for="listValues' + count + '" class="select">List:</label>';
    data += '<select name="listValues' + count + '" id="listValues' + count + '"  data-mini="true"></select></div>';
    data += '<div data-role="fieldcontain"><label for="taInstructionsDescription' + count + '">Description:</label>';
    data += '<textarea cols="40" rows="8" name="taInstructionsDescription' + count + '" id="taInstructionsDescription' + count + '" data-mini="true"></textarea></div>';
    data += '</a><a href="#" onclick="fnRemove(\'' + count + '\');">Remove</a></li>';

    $("#ulInstructions").append(data);
    $("#ulInstructions").listview('refresh');

    $("div select").each(function (i) {
        $(this).selectmenu();
    });
    $.each(gData, function (key, val) {
        switch (val.list) {
            case "InstructionsFilter":
                fillValues(val, 'selInstructionsFilter' + count);
                break;
            case "ListItems":
                fillValues(val, 'listValues' + count);
                break;
        }
    });
    $(".ui-page").trigger('create');
}

function fnRemove(count) {
    $("#liInstructions" + count).remove();

    var instructions_count = $("#ulInstructions").find('textarea[id^=taInstructionsDescription]').length;
    if(instructions_count ==0)
        $("#instructionsHeader").hide();
}

function fillValues(Obj, ctrl) {
    $.each(Obj.items, function (key, val) {
        $("#" + ctrl).append('<option  value="' + val.value + '">' + val.name + '</option>');
    });
    $("#" + ctrl).selectmenu('refresh');
}

function fnRemoveMethod(pk) {
    $("#hdnPk").val(pk);
    $('#confirmMsg').text("Are you sure you want to Remove this Method?");
    $('#popupConfirmDialog').popup('open');
    return false;
}

function fnOpenPopup(method, index) {
    switch (method.toLowerCase().replace(/ /g, '')) {
        case "deliverypoint":
            //$('#popupDeliveryPoint').popup('open');
            fnPopupDeliveryPoint();
            break;
        case "fieldlevel":
            //$('#popupFieldLevel').popup('open');
            fnFieldLevelOptions();
            break;
        case "filter":
            //$('#popupFilter').popup('open');
            fnFilterOptions();
            break;
        case "nth":
            //if (index == 3)
            //    //$('#popupNthField').popup('open');
            //    fnPopupNthField();
            //else
            fnPopupNth();
            break;
        case "custom":
            //$('#popupCustomMergePurge').popup('open');
            fnPopupCustom();
            break;
    }
}

function fnPopupDeliveryPoint() {
    $('#popupDeliveryPoint').empty();

    var delivery_point = '<div><h3>Delivery Point Options</h3><div data-role="fieldcontain">Delivery Point:';
    delivery_point += '<label for="ddlDeliveryPoint" class="select"></label><select name="ddlDeliveryPoint" id="ddlDeliveryPoint" data-mini="true" data-theme="b"></select></div>';

    delivery_point += '<div data-role="collapsible" data-theme="a" data-content-theme="c" data-mini="true" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d">';
    delivery_point += '<h2>Advanced Dedupe by File</h2><ul data-role="listview"><li><p data-mini="true"><label for="ddlDedupe" class="select">File:</label>';
    delivery_point += '<select name="ddlDedupe" id="ddlDedupe" data-mini="true">'
    delivery_point += '</select></p></li></ul></div>';

    delivery_point += '<div class="ui-grid-b" style="padding-top: 6px;"><div class="ui-block-a">Index:';
    delivery_point += '<input type="range" data-type="range" name="txtIndexDeliveryPoint" id="txtIndexDeliveryPoint"  min="0" max="5" data-highlight="true" data-mini="true" /></div>';
    delivery_point += '<div class="ui-block-b"><a data-role="button" data-inline="true" data-mini="true" data-theme="b" onclick="$(\'#popupDeliveryPoint\').popup(\'close\');">Cancel</a></div>';
    delivery_point += '<div class="ui-block-c"><a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="fnSubmit(\'delivery point\', \'popupDeliveryPoint\', \'txtIndexDeliveryPoint\', ' + gDataPopup.deliveryPointOptions[0].index + ');">Submit</a></div></div></div>';

    $("#popupDeliveryPoint").append(delivery_point);
    $(".ui-page").trigger('create');

    $("#dropDownTemplate").tmpl(gDataPopup.deliveryPointOptions[0].deliveryPoint).appendTo("#ddlDeliveryPoint");
    $('#ddlDeliveryPoint').val('-1').selectmenu('refresh');
    $("#dropDownTemplate").tmpl(gDataPopup.deliveryPointOptions[0].dedupe).appendTo("#ddlDedupe");
    $('#ddlDedupe').val('-1').selectmenu('refresh');
    $('#txtIndexDeliveryPoint').val(gDataPopup.deliveryPointOptions[0].index);

    $('#popupDeliveryPoint').popup('open');
}

function fnSubmit(merge_purge_line, popup_id, index_ctrl_id, current_index) {
    var count = 0;
    $.each(gDataMergePurge.steps, function (key_data, val_data) {
        if (parseInt($('#' + index_ctrl_id).val()) == count)
            count++;
        if (val_data.method.toLowerCase() != merge_purge_line) {
            val_data.index = count;
        }
        else if (val_data.method.toLowerCase() == merge_purge_line) {
            val_data.index = parseInt($('#' + index_ctrl_id).val());
            count--;
        }
        count++;
    });

    $('#' + popup_id).popup('close');
    buildLists();
}


function fnFieldLevelOptions() {
    $('#popupFieldLevel').empty();

    var field_level_options = '<div><h3 style="padding-bottom: 6px;">Field Level Options</h3>Selected Fields:';
    field_level_options += '<div class="content-primary" id="dvSelectedFields"></div>';

    field_level_options += '<div data-role="collapsible-set" data-theme="b" data-content-theme="d" style="padding-top: 6px;" id="dvSelectFields"></div>';
    //field_level_options += '<div  ></div>';

    field_level_options += '<div data-role="collapsible" data-theme="a" data-content-theme="c" data-mini="true" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d">';
    field_level_options += '<h3>Advanced Every Nth</h3>Select every <input type="text" name="txtSelectEvery" id="txtSelectEvery" value="" data-mini="true" />Out of';
    field_level_options += '<input type="text" name="txtOutOf" id="txtOutOf" value="" data-mini="true" />Records';
    field_level_options += '<fieldset data-role="controlgroup" data-mini="true">';
    field_level_options += '<input type="radio" name="rdoDistribute" id="rdoEvenlyDistribute" value="choice-1" />';
    field_level_options += '<label for="rdoEvenlyDistribute">Evenly Distribute (1 out of 2 means every other record)</label>';
    field_level_options += '<input type="radio" name="rdoDistribute" id="rdoBlockDistribute" value="choice-2" />';
    field_level_options += '<label for="rdoBlockDistribute">Block Distribute (1 out of 2 means top half)</label></fieldset></div>';

    field_level_options += '<div class="ui-grid-b" style="padding-top: 6px;"><div class="ui-block-a">Index:';
    field_level_options += '<input type="range" data-type="range" name="txtIndexFieldLevel" id="txtIndexFieldLevel" min="0" max="5" data-highlight="true" data-mini="true" /></div>';

    field_level_options += '<div class="ui-block-b"><a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="$(\'#popupFieldLevel\').popup(\'close\');">Cancel</a></div>';
    field_level_options += '<div class="ui-block-c"><a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="fnSubmit(\'field level\', \'popupFieldLevel\', \'txtIndexFieldLevel\', ' + gDataPopup.fieldLevelOptions[0].index + ');">Submit</a></div></div>';

    $("#popupFieldLevel").append(field_level_options);
    $(".ui-page").trigger('create');

    //selected fields table
    fnSelectedFields();

    //select fields table (below table)
    fnToBeSelectedFields();

    $('#txtSelectEvery').val(gDataPopup.fieldLevelOptions[0].selectEvery);
    $('#txtOutOf').val(gDataPopup.fieldLevelOptions[0].outOf);
    $("#rdoEvenlyDistribute").attr("checked", gDataPopup.fieldLevelOptions[0].evenlyDistribute).checkboxradio("refresh");
    $("#rdoBlockDistribute").attr("checked", !gDataPopup.fieldLevelOptions[0].evenlyDistribute).checkboxradio("refresh");
    $('#txtIndexFieldLevel').val(gDataPopup.fieldLevelOptions[0].index);

    $(".ui-page").trigger('create');

    $('#popupFieldLevel').popup('open');
}

function fnFilterOptions() {
    $('#popupFilter').empty();

    var filter_options = '<div><h3>Filter Options</h3>Filter selected records OUT of the Output List.<p/><p/>';
    filter_options += 'Selected Fields (Set Filter Options for Each):<div data-role="collapsible-set" data-theme="a" id="dvFilterSelectedFieldsFilter" data-content-theme="c" data-mini="true" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"></div>';
    filter_options += '</div>';

    filter_options += '<div data-role="collapsible-set" data-theme="b" id="dvFilterSelectFieldsFilter" data-content-theme="d" style="padding-top:6px;" >Select Fields to Filter On:</div>';

    filter_options += '<div class="ui-grid-b" style="padding-top:6px;"><div class="ui-block-a">Index:';
    filter_options += '<input type="range" data-type="range" name="txtFilterIndex" id="txtFilterIndex" min="0" max="5" data-highlight="true" data-mini="true"/></div>';
    filter_options += '<div class="ui-block-b"><a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="$(\'#popupFilter\').popup(\'close\');">Cancel</a></div>';
    filter_options += '<div class="ui-block-c"><a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="fnFilterValidations(\'Filter\');">Submit</a></div></div>';

    $("#popupFilter").append(filter_options);

    //selected fields table
    fnFilterSelectedFields('Filter');

    //select fields table (below table)
    fnToBeFilterSelectedFields('Filter');

    $('#txtFilterIndex').val(gDataPopup.filterOptions[0].index);
    $(".ui-page").trigger('create');

    $('#popupFilter').popup('open');
}

function fnFilterValidations(ctrl) {
    $.each($("#dvFilterSelectedFields" + ctrl).find('input[id^=txtFilterValue]'), function (a, b) {
        var ctrl = $(this)[0].name.split('~');
        if ($.trim($(this)[0].value) == "") {
            $('#alertmsg').text('Please enter Filter Value for "Column-' + ctrl[1] + '" : "table-' + ctrl[2] + '"');
            $("#popupFilter").popup('close');
            $("#popupDialog").popup('open');
            $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'open\');$(\'#popupDialog\').popup(\'close\');$(\'#popupFilter\').popup(\'open\');');
            return false;
        }
    });
    fnSubmit('filter', 'popupFilter', 'txtFilterIndex', gDataPopup.filterOptions[0].index);
}

function fnSelectedFields() {
    $("#dvSelectedFields").empty();
    var selected_fields = '<ul data-role="listview" data-inset="true" data-divider-theme="e">';
    var counter = 0;
    $.each(gDataPopup.fieldLevelOptions[0].selectedField, function (key, val) {
        if (val.table != undefined) {
            selected_fields += '<li data-role="list-divider" >' + val.table + '</li>';
            $.each(val.columns, function (key_column, val_column) {
                selected_fields += '<li data-icon="delete" data-theme="c"><a style="font-size:12px" onclick="fnRemoveField(\'' + val.table + '\', \'' + val_column + '\');">' + val_column + '</a></li>';
            });
            counter++;
        }
    });
    if (counter == 0)
        selected_fields += '<span style="color:#fceda7;">No Fields Selected</span>';
    selected_fields += '</ul>';
    $("#dvSelectedFields").append(selected_fields);

    $(".ui-page").trigger('create');
}

function fnToBeSelectedFields() {
    $("#dvSelectFields").empty();

    var select_fields = 'Select Fields:';
    $.each(gDataPopup.fieldLevelOptions[0].selectField, function (key, val) {
        select_fields += '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" id="dvTable' + val.table + '" >';
        select_fields += '<h3><span style="font-size:14px">' + val.table + '</span></h3>';
        select_fields += '<ul data-role="listview" data-divider-theme="d" >';

        $.each(val.columns, function (key_column, val_column) {
            select_fields += '<li data-icon="plus"><a style="font-size:12px" onclick="fnAddField(\'' + val.table + '\', \'' + val_column + '\');">' + val_column + '</a></li>';
        });
        select_fields += '</ul></div>';
    });
    $("#dvSelectFields").append(select_fields);

    $(".ui-page").trigger('create');
}

function fnRemoveField(table, column) {
    $.each(gDataPopup.fieldLevelOptions[0].selectedField, function (key, val) {
        if (table == val.table) {
            var column_index = 0;
            $.each(val.columns, function (key_column, val_column) {
                if (column == val_column) {
                    column_index = key_column;
                    return false;
                }
            });
            val.columns.splice(column_index, 1);

            if (val.columns.length == 0)
                delete val.table;
        }
    });

    $.each(gDataPopup.fieldLevelOptions[0].selectField, function (key, val) {
        if (table == val.table) {
            val.columns.push(column);
        }
    });

    fnSelectedFields();
    fnToBeSelectedFields();
}

function fnAddField(table, column) {
    $.each(gDataPopup.fieldLevelOptions[0].selectField, function (key, val) {
        if (table == val.table) {
            var column_index = 0;
            $.each(val.columns, function (key_column, val_column) {
                if (column == val_column) {
                    column_index = key_column;
                    return false;
                }
            });
            val.columns.splice(column_index, 1);
        }
    });

    var counter = 0;
    $.each(gDataPopup.fieldLevelOptions[0].selectedField, function (key, val) {
        if (table == val.table) {
            val.columns.push(column);
            counter++;
        }
    });

    if (counter == 0) {
        gDataPopup.fieldLevelOptions[0].selectedField.push({
            "table": table,
            "columns": []
        });
        $.each(gDataPopup.fieldLevelOptions[0].selectedField, function (key, val) {
            if (table == val.table) {
                val.columns.push(column);
            }
        });
    }

    fnSelectedFields();
    fnToBeSelectedFields();

    $("#dvTable" + table).trigger("expand");
}

function fnFilterSelectedFields(ctrl) {
    $("#dvFilterSelectedFields" + ctrl).empty();
    var selected_fields = '';
    var counter = 0;
    $.each(gDataPopup.filterOptions[0].selectedField, function (key, val) {
        selected_fields += '<div data-role="collapsible" data-theme="e"><h3>' + val.column + '</h3>';
        selected_fields += '<div data-role="fieldcontain" ><b>' + val.table + '</b><p />Filter Operator:';
        selected_fields += '<label for="ddlFilterOperators' + counter + '" class="select"></label><select name="ddlFilterOperators' + counter + '" id="ddlFilterOperators' + counter + '" data-mini="true" data-theme="b"></select>';
        selected_fields += '</div>Filter Value:<input type="text" name="txtFilterValue~' + val.column + '~' + val.table + '" id="txtFilterValue' + counter + '" value="' + val.filterValue + '" data-mini="true" /><br />';
        selected_fields += '<a data-role="button" data-inline="true" onclick="fnRemoveFilterField(\'' + val.table + '\', \'' + val.column + '\', \'' + ctrl + '\');" data-theme="d" data-mini="true" data-icon="delete"  data-iconpos="right">Remove Filter</a></div></div>';
        counter++;
    });
    if (counter == 0)
        selected_fields += '<span style="color:#fceda7;">No Fields Selected</span>';

    $("#dvFilterSelectedFields" + ctrl).append(selected_fields);

    $(".ui-page").trigger('create');

    for (var i = 0; i < counter; i++) {
        $("#dropDownTemplate").tmpl(gDataPopup.filterOptions[0].filterOperator).appendTo("#ddlFilterOperators" + i);
        $('#ddlFilterOperators' + i).val('-1').selectmenu('refresh');
    }
}

function fnToBeFilterSelectedFields(ctrl) {
    $("#dvFilterSelectFields" + ctrl).empty();

    var select_fields = 'Select Fields:';
    $.each(gDataPopup.filterOptions[0].selectField, function (key, val) {
        select_fields += '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" id="dvTableFilter' + val.table + ctrl + '" >';
        select_fields += '<h3><span style="font-size:14px">' + val.table + '</span></h3>';
        select_fields += '<ul data-role="listview" data-divider-theme="d" >';

        $.each(val.columns, function (key_column, val_column) {
            select_fields += '<li data-icon="plus"><a style="font-size:12px" onclick="fnAddFilterField(\'' + val.table + '\', \'' + val_column + '\', \'' + ctrl + '\');">' + val_column + '</a></li>';
        });
        select_fields += '</ul></div>';
    });
    $("#dvFilterSelectFields" + ctrl).append(select_fields);

    $(".ui-page").trigger('create');
}

function fnAddFilterField(table, column, ctrl) {
    $.each(gDataPopup.filterOptions[0].selectField, function (key, val) {
        if (table == val.table) {
            var column_index = 0;
            $.each(val.columns, function (key_column, val_column) {
                if (column == val_column) {
                    column_index = key_column;
                    return false;
                }
            });
            val.columns.splice(column_index, 1);
        }
    });
    gDataPopup.filterOptions[0].selectedField.push({
        "column": column,
        "table": table,
        "filterValue": ""
    });

    fnFilterSelectedFields(ctrl);
    fnToBeFilterSelectedFields(ctrl);

    window.setTimeout(function delay() { $("#dvTableFilter" + table + ctrl).trigger("expand"); }, 1);
}

function fnRemoveFilterField(table, column, ctrl) {
    var column_index = 0;
    $.each(gDataPopup.filterOptions[0].selectedField, function (key, val) {
        if (column == val.column && table == val.table) {
            column_index = key;
            return false;
        }
    });
    gDataPopup.filterOptions[0].selectedField.splice(column_index, 1);

    $.each(gDataPopup.filterOptions[0].selectField, function (key, val) {
        if (table == val.table) {
            val.columns.push(column);
        }
    });
    fnFilterSelectedFields(ctrl);
    fnToBeFilterSelectedFields(ctrl);
}

function fnPopupCustom() {
    $('#popupCustomMergePurge').empty();
    var custom_add = '<div style="width: 250px; padding: 10px 10px;"><h3>Custom Merge-Purge Setup</h3>';
    custom_add += '<div data-role="fieldcontain">Stored Procedure Type: <label  label for="ddlspTypeCustom" class="select"></label><select name="ddlspTypeCustom" id="ddlspTypeCustom"  data-mini="true" data-theme="b"> </select></div>';
    custom_add += '<div data-role="fieldcontain"> Stored Procedures: <label for="ddlSpCustom"></label><select name="ddlSpCustom" id="ddlSpCustom" data-mini="true" data-theme="b"></select></div>';
    custom_add += ' <h3>Paramaters</h3> Store Number:<label for="txtStore"></label><input type="text" id="txtStore" name="txtStore" data-mini="true" />Radius:<label for="txtRadius"></label><input type="text" id="txtRadius" name="txtRadius" data-mini="true" />';
    custom_add += '  Max Quantity:<label for="txtMaxQuantity"></label><input type="text" id="txtMaxQuantity" name="txtMaxQuantity" data-mini="true" /><br />';
    custom_add += '<div data-role="fieldcontain">Index:<label for="txtIndexCustom"></label><input type="range" data-type="range" id="txtIndexCustom" name="txtIndexCustom" min="0" max="5" data-highlight="true" data-mini="true" /> </div>';
    custom_add += '<div align="center"><a data-role="button" data-inline="true" data-mini="true" data-theme="b" onclick="$(\'#popupCustomMergePurge\').popup(\'close\');">Cancel</a>';
    custom_add += '<a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="fnSubmit(\'custom\', \'popupCustomMergePurge\', \'txtIndexCustom\', ' + gDataPopup.customMergePurge[0].index + ');">Submit</a></div></div>';

    $("#popupCustomMergePurge").append(custom_add);
    $(".ui-page").trigger('create');

    $("#dropDownTemplate").tmpl(gDataPopup.customMergePurge[0].storedProcedureType).appendTo("#ddlspTypeCustom");
    $('#ddlspTypeCustom').val('-1').selectmenu('refresh');
    $("#dropDownTemplate").tmpl(gDataPopup.customMergePurge[0].storedProcedures).appendTo("#ddlSpCustom");
    $('#ddlSpCustom').val('-1').selectmenu('refresh');
    $('#txtStore').val(gDataPopup.customMergePurge[0].storeNumber);
    $('#txtRadius').val(gDataPopup.customMergePurge[0].radius);
    $('#txtMaxQuantity').val(gDataPopup.customMergePurge[0].maxQuantity);
    $('#txtIndexCustom').val(gDataPopup.customMergePurge[0].index);

    $('#popupCustomMergePurge').popup('open');
}

function nthByDropdownChange(type) {
    if (type.value == "viewTop") {
        $('#dvRadius').css('display', 'none');
        $('#dvViewField').css('display', 'none');
        $('#dvRecord').css('display', 'block');
        $('#dvFinalListFilter').css('display', 'none');
        $('#dvSelectedRecordsFinalList').css('display', 'none');
    }
    else if (type.value == "viewRadius") {
        $('#dvRadius').css('display', 'block');
        $('#dvViewField').css('display', 'none');
        $('#dvRecord').css('display', 'none');
        $('#dvFinalListFilter').css('display', 'none');
        $('#dvSelectedRecordsFinalList').css('display', 'none');
    }
    else if (type.value == "viewField") {
        $('#dvRadius').css('display', 'none');
        $('#dvViewField').css('display', 'block');
        $('#dvRecord').css('display', 'none');
        $('#dvFinalListFilter').css('display', 'none');
        $('#dvSelectedRecordsFinalList').css('display', 'none');
    }
    else if (type.value == "finalListDownToThisFilter") {
        $('#dvRadius').css('display', 'none');
        $('#dvViewField').css('display', 'none');
        $('#dvRecord').css('display', 'none');
        $('#dvFinalListFilter').css('display', 'block');
        $('#dvSelectedRecordsFinalList').css('display', 'none');        
        //selected fields table
        fnFilterSelectedFields('nThFinalList');

        //select fields table (below table)
        fnToBeFilterSelectedFields('nThFinalList');
    }
    else if (type.value == "selectedRecordsWithinFinalList") {
        $('#dvRadius').css('display', 'none');
        $('#dvViewField').css('display', 'none');
        $('#dvRecord').css('display', 'none');
        $('#dvFinalListFilter').css('display', 'none');
        $('#dvSelectedRecordsFinalList').css('display', 'block');        
        //selected fields table
        fnFilterSelectedFields('nThSRFinalList');

        //select fields table (below table)
        fnToBeFilterSelectedFields('nThSRFinalList');
    }
    else {
        $('#dvRadius').css('display', 'none');
        $('#dvViewField').css('display', 'none');
        $('#dvRecord').css('display', 'none');
        $('#dvFinalListFilter').css('display', 'none');
        $('#dvSelectedRecordsFinalList').css('display', 'none');
    }
}

function fnPopupNth() {
    $('#popupNth').empty();
    var controln_to_add = "";
    controln_to_add = '<div style="width: 250px; padding: 10px 10px;"><h3>Nth Setup</h3>';
    controln_to_add += '<div data-role="fieldcontain">Nth By:<label for="ddlNthBy" class="select"></label><select name="ddlNthBy" id="ddlNthBy" data-mini="true" data-theme="b" onchange="nthByDropdownChange(this);"></select></div>';

    //View Top
    controln_to_add += '<div id="dvRecord">';
    controln_to_add += '<div data-role="collapsible" data-mini="true" data-collapsed-icon="info" data-expanded-icon="arrow-u" data-iconpos="right" data-theme="a" data-collapsed="true">';
    controln_to_add += '<h3><span style="font-weight:bold; font-size:14px; color:#9CF">Nth By viewTop</span></h3>';
    controln_to_add += '<span style="font-size:12px"><p>Keeps the first portion of each zip trying to keep all zips.</p>';
    controln_to_add += '<p>It will try to keep each zip at the same proportion as the original list</p>';
    controln_to_add += '<p>Enter a number in the field below to select the top Nth records in this dataset.</p></span></div>';
    controln_to_add += 'Number of Records:<label for="basic"></label><input type="text" name="txtRecords" id="txtRecords" data-mini="true" /><br /></div>';

    //View Radius
    controln_to_add += '<div id="dvRadius">';
    controln_to_add += '<div data-role="collapsible" data-mini="true" data-collapsed-icon="info" data-expanded-icon="arrow-u" data-iconpos="right" data-theme="a" data-collapsed="true">';
    controln_to_add += '<h3><span style="font-weight:bold; font-size:14px; color:#9CF">Nth By Radius</span></h3>';
    controln_to_add += '<span style="font-size:12px"><p>Assumes the distance field is populated.</p>';
    controln_to_add += '<p>Will keep records with the smallest value first (closest)</p>';
    controln_to_add += '<p>Does not care about zip codes.</p>';
    controln_to_add += '<p>Enter a distance (decimal miles) in the field below to Nth records in this dataset.This control assumes that the "distance" column in the database id populated with valid distance data.</p></span></div>';
    controln_to_add += 'Distance:<label for="txtDistance"></label><input type="text" name="txtDistance" id="txtDistance" data-mini="true" /><br /></div>';

    //View Field
    controln_to_add += '<div id=dvViewField>';
    //controln_to_add += '<span style="font-weight:bold; font-size:14px; color:#9CF">Nth By Field</span><br /><br />
    controln_to_add += '<div data-role="collapsible" data-mini="true" data-collapsed-icon="info" data-expanded-icon="arrow-u" data-iconpos="right" data-theme="a" data-collapsed="true">';
    controln_to_add += '<h3><span style="font-weight:bold; font-size:14px; color:#9CF">Nth By Field</span></h3>';
    controln_to_add += '<span style="font-size:12px"><p>Takes selected fields and gets all unique combinations of values.</p>';
    controln_to_add += '<p>Keeps entered count for each unique combination</p>';
    controln_to_add += '<p>For example if you select batch and entered count of 1000.  It will keep 1000 records for each unique batch</p>';
    controln_to_add += '<p>Works the same as view top for zips</p></span></div>';
    controln_to_add += 'Selected Fields: <div class="content-primary" id="dvNthSelectedFields"></div>';
    controln_to_add += '<div data-role="collapsible-set" data-theme="b" data-content-theme="d" style="padding-top: 6px;" id="dvNthSelectFields"></div></div>';

    //Nth final list down to this filter
    controln_to_add += '<div id="dvFinalListFilter">';
    controln_to_add += '<div data-role="collapsible" data-mini="true" data-collapsed-icon="info" data-expanded-icon="arrow-u" data-iconpos="right" data-theme="a" data-collapsed="true">';
    controln_to_add += '<h3><span style="font-weight:bold; font-size:14px; color:#9CF">Nth final list down to this filter</span></h3>';
    controln_to_add += '<span style="font-size:12px"><p>This will remove all records that are not within the entered values and then nth what is left to the entered quantity.</p>';
    controln_to_add += '<p>You should only have one of these per a job</p>';
    controln_to_add += '</span></div>';

    controln_to_add += '<div>Selected Fields (Set Filter Options for Each):<div data-role="collapsible-set" data-theme="a" id="dvFilterSelectedFieldsnThFinalList" data-content-theme="c" data-mini="true" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"></div>'; //
    controln_to_add += '</div>';
    controln_to_add += '<div data-role="collapsible-set" data-theme="b" id="dvFilterSelectFieldsnThFinalList" data-content-theme="d" style="padding-top:6px;" >Select Fields to Filter On:</div>'; //

    controln_to_add += '</div>';    

    //Nth to selected records within final list
    controln_to_add += '<div id="dvSelectedRecordsFinalList">';
    controln_to_add += '<div data-role="collapsible" data-mini="true" data-collapsed-icon="info" data-expanded-icon="arrow-u" data-iconpos="right" data-theme="a" data-collapsed="true">';
    controln_to_add += '<h3><span style="font-weight:bold; font-size:14px; color:#9CF">Nth to selected records within final list</span></h3>';
    controln_to_add += '<span style="font-size:12px"><p>This will flag all records that match entered values and are still good then Nth those records down to the entered quantity</p>';
    controln_to_add += '<p>You can have as many of these as you need.</p>';
    controln_to_add += '<p>Should not Nth to a quantity that is higher than what you have available</p>';
    controln_to_add += '</span></div>';

    controln_to_add += '<div>Selected Fields (Set Filter Options for Each):<div data-role="collapsible-set" data-theme="a" id="dvFilterSelectedFieldsnThSRFinalList" data-content-theme="c" data-mini="true" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"></div>'; //
    controln_to_add += '</div>';
    controln_to_add += '<div data-role="collapsible-set" data-theme="b" id="dvFilterSelectFieldsnThSRFinalList" data-content-theme="d" style="padding-top:6px;" >Select Fields to Filter On:</div>'; //

    controln_to_add += '</div>';


    controln_to_add += '<div id="dvcommonFields"><div class="ui-grid-b" style="padding-top: 6px;"><div class="ui-block-a">Index:<input type="range" data-type="range" id="txtNthIndex" name="txtNthIndex" min="0" max="5" data-highlight="true" data-mini="true" /></div>';
    controln_to_add += '<div class="ui-block-b" align="right"><a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="$(\'#popupNth\').popup(\'close\');">Cancel</a></div>';
    controln_to_add += '<div class="ui-block-c"><a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="fnSubmit(\'nth\', \'popupNth\', \'txtNthIndex\', ' + gDataPopup.nField[0].index + ');">Submit</a></div></div></div></div>';

    $("#popupNth").append(controln_to_add);

    $(".ui-page").trigger('create');

    $('#dropDownTemplate').tmpl(gDataPopup.nField[0].ntypePopup).appendTo("#ddlNthBy");
    $('#ddlNthBy').val('-1').selectmenu('refresh');
    $('#txtRecords').val(gDataPopup.nField[0].numberOfRecords);
    $('#txtDistance').val(gDataPopup.nField[0].distance);
    $('#txtNthIndex').val(gDataPopup.nField[0].index);

    fnSelectedNthFields();
    fnToBeSelectedNthFields();

    $('#popupNth').popup('open');
    $('#dvRadius').css('display', 'none');
    $('#dvViewField').css('display', 'none');
    $('#dvRecord').css('display', 'none');
    $('#dvFinalListFilter').css('display', 'none');
    $('#dvSelectedRecordsFinalList').css('display', 'none');

    $("#popupNth").popup('open');
}

//Popup Nth field
//function fnPopupNthField() {
//    $('#popupNthField').empty();
//    var controln_field = "";
//    controln_field += '<div style="width: 250px; padding: 10px 10px;"><h3>Nth Setup</h3><div data-role="fieldcontain">Nth By:';
//    controln_field += '<label for="nByfield"></label><select name="nByfield" id="nByfield" data-mini="true"></select></div>';

//    controln_field += '<div class="content-primary" id="dvNthSelectedFields"></div>';
//    controln_field += '<div data-role="collapsible-set" data-theme="b" data-content-theme="d" style="padding-top: 6px;" id="dvNthSelectFields"></div>';

//    controln_field += '<div class="ui-grid-b" style="padding-top: 6px;"><div class="ui-block-a">Index:<input type="range" id="txtIndexNfield" name="txtIndexNfield" min="0" max="5" data-highlight="true" data-mini="true"/></div>';
//    controln_field += '<div class="ui-block-b"><a data-role="button" data-inline="true" data-theme="b" data-mini="true" onclick="$(\'#popupNthField\').popup(\'close\');">Cancel</a></div>';
//    controln_field += '<div class="ui-block-c"><a data-role="button" data-inline="true" data-theme="b" data-mini="true">Submit</a></div></div></div>';

//    $("#popupNthField").append(controln_field);
//    $(".ui-page").trigger('create');

//    fnSelectedNthFields();
//    fnToBeSelectedNthFields();

//    $("#dropDownTemplate").tmpl(gDataPopup.nfieldOptions[0].nByfieldName).appendTo("#nByfield");
//    $('#nByfield').val('-1').selectmenu('refresh');
//    $('#txtIndexNfield').val(gDataPopup.nfieldOptions[0].index);

//    $('#popupNthField').popup('open');
//}

function fnSelectedNthFields() {
    $('#dvNthSelectedFields').empty();
    var selectedn_field = '<ul data-role="listview" data-inset="true" data-divider-theme="e">';
    var counter = 0;
    $.each(gDataPopup.nField[0].selectedField, function (key, val) {
        if (val.table != undefined) {
            selectedn_field += '<li data-role="list-divider">' + val.table + '</li>';
            $.each(val.columns, function (key_column, val_column) {
                selectedn_field += '<li data-icon="delete" data-theme="c"><a  href="#" style="font-size:12px" onclick="fnRemoveNthField(\'' + val.table + '\', \'' + val_column + '\');">' + val_column + '</a></li>';
            });
            counter++;
        }
    });
    if (counter == 0)
        selectedn_field += '<span style="color:#fceda7;">No Fields Selected</span>';
    selectedn_field += '</ul>';
    $("#dvNthSelectedFields").append(selectedn_field);

    $(".ui-page").trigger('create');
}

function fnToBeSelectedNthFields() {
    $("#dvNthSelectFields").empty();
    var selectedn_field = 'Select Fields:';
    $.each(gDataPopup.nField[0].selectField, function (key, val) {
        selectedn_field += '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" id="dvTable' + val.table + '" >';
        selectedn_field += '<h3><span style="font-size:14px">' + val.table + '</span></h3>';
        selectedn_field += '<ul data-role="listview" data-divider-theme="d" >';

        $.each(val.columns, function (key_column, val_column) {
            selectedn_field += '<li data-icon="plus"><a href="#" style="font-size:12px" onclick="fnAddNthField(\'' + val.table + '\', \'' + val_column + '\');">' + val_column + '</a></li>';
        });
        selectedn_field += '</ul></div>';
    });
    $("#dvNthSelectFields").append(selectedn_field);
    $(".ui-page").trigger('create');
}

function fnRemoveNthField(table, column) {
    $.each(gDataPopup.nField[0].selectedField, function (key, val) {
        if (table == val.table) {
            var column_index = 0;
            $.each(val.columns, function (key_column, val_column) {
                if (column == val_column) {
                    column_index = key_column;
                    return false;
                }
            });
            val.columns.splice(column_index, 1);
            if (val.columns.length == 0)
                delete val.table;
        }
    });

    $.each(gDataPopup.nField[0].selectField, function (key, val) {
        if (table == val.table) {
            val.columns.push(column);
        }
    });
    fnSelectedNthFields();
    fnToBeSelectedNthFields();
}

function fnAddNthField(table, column) {
    $.each(gDataPopup.nField[0].selectField, function (key, val) {
        if (table == val.table) {
            var column_index = 0;
            $.each(val.columns, function (key_column, val_column) {
                if (column == val_column) {
                    column_index = key_column;
                    return false;
                }
            });
            val.columns.splice(column_index, 1);
        }
    });

    var counter = 0;
    $.each(gDataPopup.nField[0].selectedField, function (key, val) {
        if (table == val.table) {
            val.columns.push(column);
            counter++;
        }
    });

    if (counter == 0) {
        gDataPopup.nField[0].selectedField.push({
            "table": table,
            "columns": []
        });
        $.each(gDataPopup.nField[0].selectedField, function (key, val) {
            if (table == val.table) {
                val.columns.push(column);
            }
        });
    }

    fnSelectedNthFields();
    fnToBeSelectedNthFields();

    $("#dvTable" + table).trigger("expand");
}
function confirmMessage() {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupConfirmDialog" data-overlay-theme="a" data-theme="c" style="max-width:400px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="a" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="b" id="cancelBut" onclick="$(\'#popupConfirmDialog\').popup(\'close\');">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="b" onclick="fnConfirmContinueClick()" id="continueBut">Continue</a>';
    msg_box += '</div></div>';
    $("#_jobMergePurge").append(msg_box);
}

function fnAddMethod() {    
    if ($('#ddlMethod').val() == "-1") {
        $('#popupAddMergePurge').popup('close');
        $('#alertmsg').text("Please select a Merge-Purge Method.");
        $('#popupDialog').popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close'); $('#popupAddMergePurge').popup('open');");
        return false;
    }
    var step = { "commandType": "", "name": "", "column": "", "value": "" };
    gDataMergePurge.steps.push({
        "index": gDataMergePurge.steps.length,
        "method": $("#ddlMethod").val(),
        "step": step
    });
    $('#popupAddMergePurge').popup('close');
    buildLists();

}

function fnConfirmContinueClick() {
    gDataMergePurge.steps.splice($("#hdnPk").val(), 1);

    var counter = 0;
    $.each(gDataMergePurge.steps, function (key_data, val_data) {
        val_data.index = counter;
        counter++;
    });
    $('#popupConfirmDialog').popup('close');
    buildLists();
}

function fnOpenAddPopup() {
    $('#popupAddMergePurge').popup('open');
    $("#ddlMethod").val('-1').selectmenu('refresh');
    //$('#ddlMethod').selectmenu('refresh');
}
//******************** Public Functions End **************************